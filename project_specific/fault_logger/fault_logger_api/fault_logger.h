#ifndef FAULT_LOGGER_H
#define FAULT_LOGGER_H

/**
 *  @file fault_logger.h
 *
 *  @copyright 2020 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential@n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup fault_logger_api Fault Logger Interface Documentation
 *  @{
 *      @details Module used to log the occurrence and duration of faults as reported
 *               by the Fault Manager
 *
 *      @page ABBR Abbreviations
 *        - NONE
 */
/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "fault_manager.h"
#include "fault_manager_types.h"
#include "uds_types.h"

/*===========================================================================*
 * Exported Preprocessor \#define Constants
 *===========================================================================*/
#define FLT_LOG_RECORDS_DID_SIZE (FLT_LOG_NUM_FAULTS * sizeof(FLT_Record_T))
#define FLTEL_CLEAR_LOG_DID_SIZE (1u)
#define FLT_CLEAR_LOG_RECORDS_DID_SIZE (1u)

#define FLT_LOG_NUM_DATA_BLOCKS (3u)

/*===========================================================================*
 * Exported Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/
typedef struct
{
    uint16_t Total_Duration_Sec;
    uint8_t  IDLE_State_Count;
    uint8_t  RUN_State_Count;
} FLT_Record_T;

typedef enum
{
    FLT_LOG_OVER_VOLTAGE,
    FLT_LOG_UNDER_VOLTAGE,
    FLT_LOG_UNDER_VOLT_SELF_PROTECT,
    FLT_LOG_OVER_CURRENT,
    FLT_LOG_UNDER_CURRENT,
    FLT_LOG_OVER_TEMPERATURE,
    FLT_LOG_OVER_SPEED,
    FLT_LOG_UNDER_SPEED,
    FLT_LOG_LIN_DATA_ERROR,
    FLT_LOG_LIN_BIT_FIELD_FRAMING_ERROR,
    FLT_LOG_LIN_ID_PARITY_ERROR,
    FLT_LOG_LIN_CHECKSUM_ERROR,
    FLT_LOG_LIN_MASTER_REQUEST_MISSING,
    FLT_LOG_LIN_MASTER_REQUEST_DENIED,
    FLT_LOG_BLOCKED_GEROTOR,
    FLT_LOG_SHORT_CIRCUIT,
    FLT_LOG_NUM_FAULTS,
    FLT_LOG_NO_ERROR
} FLT_LOG_Faults_T;
/*===========================================================================*
 * Exported Const Object Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Function Prototypes
 *===========================================================================*/
/**
 * Scans for any fault log blocks that need to be updated in permanent storage
 */
void FLT_LOG_Scan(void);

/**
 * Initializes the Fault Logger
 */
void FLT_LOG_Init(void);

/**
 * Enables fault logging
 */
void FLT_LOG_Enable(void);

/**
 * Disables fault logging
 */
void FLT_LOG_Disable(void);

/**
 * Indicates if fault logging is enabled
 *
 * @retval true - fault logging is enabled
 * @retval false - fault logging is not enabled
 */
bool_t FLT_LOG_Is_Enabled(void);

/**
 * Records a fault event in permanent storage
 *
 * @param Fault_ID
 */
void FLT_LOG_Record_Fault_Active(FLT_LOG_Faults_T Fault_ID);

/**
 * Records the end of a fault event in permanent storage
 * @param Fault_ID
 */
void FLT_LOG_Record_Fault_Inactive(FLT_LOG_Faults_T Fault_ID);

/**
 * Clears the Fault Environmental Log
 *
 * @return Status indicating the success or failure of the clear
 * @retval true - Fault Environmental Log cleared
 * @retval false - Failed to clear the Fault Environmental Log
 */
bool_t FLTEL_Clear_Environmental_Log(void);

/**
 * Clears all the Fault Records
 *
 * @return Indicates success or failure of the clear operation
 * @retval true - All Fault Records were cleared
 * @retval false - Not all Fault Records were cleared. Some may have been.
 */
bool_t FLT_LOG_Clear_Fault_Records(void);

/**
 * Serializes the Fault Logs for transmission via UDS
 *
 * @param [out] p_Resp_SDB - Serial Data Buffer into which the Fault Log will be serialized
 *
 * @return value indicating success or failure of the serialization
 * @retval UDS_RESP_POSITIVE - Fault log serialized
 * @retval UDS_RESP_RESPONSE_TOO_LONG - Unable to serialize the fault log
 */
UDS_Response_Code_T FLT_LOG_Get_Fault_Log_Records_DID(Serial_Data_Buffer_T * const p_Resp_SDB);

/**
 * Clears the fault record specified in the request
 *
 * @param p_Req_SDB [in] Serial Data Buffer with the single byte specifying the Fault Record to clear
 *
 * @return value indicating success or failure of the clearing of the Fault Record
 * @retval UDS_RESP_POSITIVE Fault Record cleared
 * @retval UDS_RESP_REQ_OUT_OF_RANGE requested Fault Record is out of range
 */
UDS_Response_Code_T FLT_LOG_Clear_Fault_Record_DID(Serial_Data_Buffer_T * const p_Req_SDB);

/**
 * A Write only DID that clears all the data for the Fault Environmental Log
 *
 * @param p_Req_SDB - Serial Data Buffer that contains the clear request
 *
 * @return indicating success or failure of the clear operation
 * @retval UDS_RESP_POSITIVE Fault Environmental Log has been cleared
 * @retval UDS_RESP_GENERAL_PROGRAMMING_FAILURE Fault Environmental Log failed to be cleared
 */
UDS_Response_Code_T FLTEL_Clear_Environmental_Log_DID(Serial_Data_Buffer_T * const p_Req_SDB);

/** @} doxygen end group */

#endif /* FAULT_LOGGER_H */
