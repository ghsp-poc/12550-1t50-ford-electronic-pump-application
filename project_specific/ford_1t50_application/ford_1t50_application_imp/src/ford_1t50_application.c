/**
 * @ingroup USER_APP
 *
 * @{
 */

/**********************************************************************/
/*! @file app_vda_blower.c
 * @brief top-level state machine
 *
 * @details
 * exemplary implementation of a blower module commanded via LIN
 *
 * module prefix: app_
 *
 * @author       JBER
 */
/**********************************************************************/
#include "52307_driver.h"
#include "auto_version.h"
#include <defines.h>
#include "fdl.h"
#include "foc.h"
#include "lin_vehicle_comm.h"
#include "math_utils.h"
#include "motor_cfg.h"
#include "motor_ctrl.h"
#include "motor_manager.h"
#include "nvm_mgr.h"
#include "speed_meas.h"
#include "system_signals.h"
#include "fault_manager.h"
#include "time_utils.h"
#include "types.h"
#include "usr_cfg.h"
#include "ford_1t50_application.h"

/*---------------
   local defines
   -------------- */

#define APP_MOTOR_POLEPAIRS ( 2u )
#define APP_I_D_REF_MIN ( -250 )

/**
 * 50 ms interval for Watchdog Timer servicing
 */
#define WDT_TASK_TIMEBASE_MS    (50u)

/*-------------------
   local declarations
   ------------------- */

/*! @brief startup configuration of the motor control module */
void static app_set_motor_configuration( void )
{
    /* motor configuration ( 8/6/2019 11:04:27 AM ) */
    /* PRQA S 0311 ++ */ /* justification: automatically generated code */
#pragma diag_suppress = Pm142 /* allow cast from const* to *  ->  disable MISRA C 2004 rule 11.5 */

    ((mc_data_t *) mc_get_data())->startup.i_q_ref = STARTUP_I_Q_TARGET;
    ((mc_data_t *) mc_get_data())->startup.current_slope = STARTUP_CURRENT_RAMP_SLOPE;
    ((mc_data_t *) mc_get_data())->startup.align_rotor_duration = STARTUP_ALIGNMENT_TIME; /* time in ticks*/
    ((mc_data_t *) mc_get_data())->startup.speed_ramp_start = STARTUP_SPEED_RAMP_START; /* rpm */
    ((mc_data_t *) mc_get_data())->startup.speed_slope = STARTUP_SPEED_RAMP_SLOPE;
    ((mc_data_t *) mc_get_data())->startup.speed_ramp_end = STARTUP_SPEED_RAMP_END; /* rpm */

    ((foc_ctrl_loop_data_t *) foc_get_data())->controller_i_d.Kp = I_CTRL_KP;
    ((foc_ctrl_loop_data_t *) foc_get_data())->controller_i_d.Ki = I_CTRL_KI;
    ((foc_ctrl_loop_data_t *) foc_get_data())->controller_i_q.Kp = I_CTRL_KP;
    ((foc_ctrl_loop_data_t *) foc_get_data())->controller_i_q.Ki = I_CTRL_KI;
    ((foc_ctrl_loop_data_t *) foc_get_data())->i_d_ref = I_D_TARGET;
    ((foc_ctrl_loop_data_t *) foc_get_data())->rotor_angle_meas_mode = FOC_ANGLE_SENSE_MODE;

    ((foc_ctrl_loop_data_t *) foc_get_data())->luenberger_data.current_comp_Kp = CURRENT_COMP_KP;
    ((foc_ctrl_loop_data_t *) foc_get_data())->luenberger_data.current_comp_Kd = CURRENT_COMP_KD;
    ((foc_ctrl_loop_data_t *) foc_get_data())->luenberger_data.current_comp_scaler = CURRENT_COMP_SCALER;

    /* Phase R [mOhm]: 126 / Phase L [uH]: 230 */
    ((foc_ctrl_loop_data_t *) foc_get_data())->luenberger_data.motor_model_coeff_a = MOTOR_MODEL_COEFF_A;
    ((foc_ctrl_loop_data_t *) foc_get_data())->luenberger_data.motor_model_coeff_b = MOTOR_MODEL_COEFF_B;
    ((foc_ctrl_loop_data_t *) foc_get_data())->luenberger_data.motor_model_coeff_scaler = MOTOR_MODEL_COEFF_SCALER;

    /* Shunt value [mOhm]: 10 / OPA gain: 9.71 / Voltage divider ratio: 0.083 */
    ((foc_ctrl_loop_data_t *) foc_get_data())->voltage_conversion_factor = SENS_PARAM_VOLTAGE_FACTOR;
    ((foc_ctrl_loop_data_t *) foc_get_data())->current_conversion_factor = SENS_PARAM_CURRENT_FACTOR;

    ((mc_data_t *) mc_get_data())->controller_rotor_speed.Kp = SPEED_CTRL_KP;
    ((mc_data_t *) mc_get_data())->controller_rotor_speed.Ki = SPEED_CTRL_KI;
    ((mc_data_t *) mc_get_data())->rotor_speed_ctrl_time_base = SPEED_CTRL_TIMEBASE;
    ((mc_data_t *) mc_get_data())->direction = SPEED_CTRL_MOTOR_DIRECTION;
    ((mc_data_t *) mc_get_data())->speed_ctrl_output_max = SPEED_CTRL_OUTPUT_MAX;
    ((mc_data_t *) mc_get_data())->speed_ctrl_output_min = SPEED_CTRL_OUTPUT_MIN;

    ((foc_ctrl_loop_data_t *) foc_get_data())->param_motor_constant = MOTOR_CONSTANT_UF;
    ((foc_ctrl_loop_data_t *) foc_get_data())->stall_det_min_rotor_speed = STALL_DET_MIN_SPEED;
    ((foc_ctrl_loop_data_t *) foc_get_data())->stall_det_thrshld = STALL_DET_THRESHOLD;

    ((mc_data_t *) mc_get_data())->sync_data.bemf_ampl_min = SYNC_PARAM_BEMF_AMPL_MIN;
    ((mc_data_t *) mc_get_data())->sync_data.sync_duration = SYNC_PARAM_SYNC_DURATION;
    ((mc_data_t *) mc_get_data())->sync_data.sync_timeout = SYNC_PARAM_SYNC_TIMEOUT;
    ((mc_data_t *) mc_get_data())->enable_sync_to_rotor = SYNC_PARAM_SYNC_ENABLE;

#if ( USR_EN_INITIAL_POSITION_DETECTION == 1 )
    ((mc_data_t *) mc_get_data())->pos_detect_data.test_pulse_length= 1200u;
    ((mc_data_t *) mc_get_data())->pos_detect_data.meas_time= 500u;
    ((mc_data_t *) mc_get_data())->pos_detect_data.meas_delay= 150u;
    ((mc_data_t *) mc_get_data())->enable_initial_pos_detect= False;
    ((mc_data_t *) mc_get_data())->pos_detect_data.offset_uv= 0;
    ((mc_data_t *) mc_get_data())->pos_detect_data.offset_uw= 0;
    ((mc_data_t *) mc_get_data())->pos_detect_data.offset_vw= 0;
#endif

#pragma diag_default = Pm142
    /* PRQA S 0311 -- */ /* justification: automatically generated code */

    _Pragma( "diag_default = Pm142" )
}

/*! @brief initialization routine, called from sys_init() in main.c */
 void app_init( void )
 {
     app_set_motor_configuration();
     FDL_Init();
     NVMMGR_Init();
     FLT_MGR_Init();
     VCOMM_Init();
     MMGR_Init();
     SYSIG_Init();
 }

/* PRQA S 3206 -- */ /* justification: all state machine handler functions should look the same -> argument not necessarily used */

/*! @brief handler function for FOC output voltage overflow
 *
 * @details
 * When the target speed can't be reached, at first field-weakening is applied (Minimum: APP_I_D_REF_MIN). <br>
 * If that still does not suffice, the same approach as in mc_output_voltage_overflow_handler() is applied (decreasing <br>
 * the target speed of motor control module). <br>
 * <br>
 * NOTE: this function is called on interrupt level!
 */
 void usr_output_voltage_overflow_handler( void )
 {
     if( foc_get_i_d_ref() > APP_I_D_REF_MIN )
     {
         foc_set_i_d_ref( foc_get_i_d_ref() - 1 );
     }
     else
     {
         /* we now have no other option than to decrease the speed ... */
         /* PRQA S 0311 ++ */ /* justification: I know what I am doing here... */
         _Pragma( "diag_suppress = Pm142" )  /* allow cast from const* to *  ->  disable MISRA C 2004 rule 11.5 */

         ((mc_data_t *) mc_get_data())->controller_rotor_speed.err_sum -= SHR_S32( mc_get_data()->controller_rotor_speed.err_sum, 8u );
         ((mc_data_t *) mc_get_data())->controller_rotor_speed.set_value -= SHR_S16( mc_get_data()->controller_rotor_speed.set_value, 8u );

         _Pragma( "diag_default = Pm142" )
         /* PRQA S 0311 -- */ /* justification: I know what I am doing here... */

         if( mc_get_direction() == FORWARD )
         {
             foc_set_i_q_ref( mc_get_data()->controller_rotor_speed.set_value );
         }
         else
         {
             foc_set_i_q_ref( -mc_get_data()->controller_rotor_speed.set_value );
         }
     }
 }

/*! @brief advance the main state machine one iteration */
void app_main( void )
{
#ifndef ENABLE_UNLOADED_CPU_CHRONOMETRICS
    static uint32_t lin_vcomm_last_timestamp = 0u;
    static uint32_t mmgr_last_timestamp = 0u;
    static uint32_t faults_last_timestamp = 0u;
    static uint32_t nvmmgr_last_timestamp = 0u;
#endif
    static uint32_t cpu_load_task_last_timestamp = 0u;

    Time_Ticks_T current_timestamp = TMUT_Get_Base_Time_Ticks();

#ifndef ENABLE_UNLOADED_CPU_CHRONOMETRICS
    /* LIN Vehicle Communications */
    if (TMUT_Has_Time_Elapsed_Ticks(lin_vcomm_last_timestamp, TMUT_MS_To_Ticks(LIN_VCOMM_TIMEBASE_MS)))
    {
        VCOMM_Task();
        lin_vcomm_last_timestamp = current_timestamp;
    }
    /* Motor Manager */
    else if (TMUT_Has_Time_Elapsed_Ticks(mmgr_last_timestamp, TMUT_MS_To_Ticks(MMGR_TIMEBASE_MS)))
    {
        MMGR_Task();
        mmgr_last_timestamp = current_timestamp;
    }
    /* Fault Manager*/
    else if (TMUT_Has_Time_Elapsed_Ticks(faults_last_timestamp, TMUT_MS_To_Ticks(FAULT_TASK_TIMEBASE_MS)))
    {
        FLT_MGR_Task();
        faults_last_timestamp = current_timestamp;
    }
    /* NVM Manager */
    else if (TMUT_Has_Time_Elapsed_Ticks(nvmmgr_last_timestamp, TMUT_MS_To_Ticks(NVMMGR_TASK_TIMEBASE_MS)))
    {
        NVMMGR_Task();
        nvmmgr_last_timestamp = current_timestamp;
    }
    else
#endif /* ENABLE_UNLOADED_CPU_CHRONOMETRICS */
    /* CPU Load Measurement */
    if (TMUT_Has_Time_Elapsed_Ticks(cpu_load_task_last_timestamp, TMUT_MS_To_Ticks(SYSIG_CPU_LOAD_TASK_INTERVAL_MS)))
    {
        SYSIG_CPU_Load_Task();
        cpu_load_task_last_timestamp = current_timestamp;
    }
    else
    {
        SYSIG_Idle_Count_Handler();
    }
}

/* }@
 */
