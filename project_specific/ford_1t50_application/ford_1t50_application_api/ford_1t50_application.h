#ifndef F1T50_APPLICATION_H
#define F1T50_APPLICATION_H

/**
 *  @file ford_1t50_application.h
 *
 *  @copyright 2020 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential@n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @{
 */
/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "global.h"

/*===========================================================================*
 * Exported Preprocessor \#define Constants
 *===========================================================================*/
/** Thread times in milliseconds @{ */
#define LIN_VCOMM_TIMEBASE_MS ( 10u )
#define MMGR_TIMEBASE_MS (10u)
#define NVMMGR_TASK_TIMEBASE_MS (10u)
#define FAULT_TASK_TIMEBASE_MS ( 10u )

/** @} */
/*===========================================================================*
 * Exported Preprocessor \#define MACROS
 *===========================================================================*/
/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/** @} doxygen end group */
#endif /* F1T50_APPLICATION_H */
