#ifndef MOTOR_MANAGER_DIAG_H
#define MOTOR_MANAGER_DIAG_H

/**
 *  @file motor_manager_diag.h
 *
 *  @copyright 2019 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential@n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup motor_manager_api Motor Manager Interface Documentation
 *  @{
 *      @details **Place Your Description Here**
 *
 *      @page ABBR Abbreviations
 *        - NONE
 */
/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "global.h"
#include "uds.h"

/*===========================================================================*
 * Exported Preprocessor \#define Constants
 *===========================================================================*/
#define MMGR_MOTOR_STATE_DID_SIZE (1u)
#define MMGR_MOTOR_MGR_SPEED_RAMP_RATE_SIZE (2u)

/*===========================================================================*
 * Exported Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Const Object Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Function Prototypes
 *===========================================================================*/
void MMGR_Override_Startup_Iq_Ref(int16_t Startup_Iq_Ref_Override);
void MMGR_Override_Startup_Current_Slope(int16_t Startup_Current_Slope_Override);
void MMGR_Override_Startup_Speed_Slope(int16_t Startup_Speed_Slope_Override);
void MMGR_Override_Startup_Speed_Ramp_End(uint16_t Startup_Speed_Ramp_End_Override);
void MMGR_Override_Speed_Control_Output_Max(int16_t Speed_Control_Output_Max);

/* Motor Manager DIDs */
UDS_Response_Code_T MMGR_Get_Diag_Simulate_Stall(Serial_Data_Buffer_T * const p_Resp_SDB);
UDS_Response_Code_T MMGR_Diag_Simulate_Stall(Serial_Data_Buffer_T * const p_Req_SDB);
UDS_Response_Code_T MMGR_Get_Speed_Ramp_Rate(Serial_Data_Buffer_T * const p_Resp_SDB);
UDS_Response_Code_T MMGR_Override_Speed_Ramp_Rate(Serial_Data_Buffer_T * const p_Req_SDB);


/** @} doxygen end group */

#endif /* MOTOR_MANAGER_H */
