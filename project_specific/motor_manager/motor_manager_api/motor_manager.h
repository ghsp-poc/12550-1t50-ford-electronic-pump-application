#ifndef MOTOR_MANAGER_H
#define MOTOR_MANAGER_H

/**
 *  @file motor_manager.h
 *
 *  @copyright 2019 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential@n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup motor_manager_api Motor Manager Interface Documentation
 *  @{
 *      @details **Place Your Description Here**
 *
 *      @page ABBR Abbreviations
 *        - NONE
 */
/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "global.h"

/*===========================================================================*
 * Exported Preprocessor \#define Constants
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Const Object Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Function Prototypes
 *===========================================================================*/

/**
 * Initializes the Motor Manager module.
 *
 * @return true - Motor Manager was initialized
 *         false - Motor Manager failed to initialize
 */
bool_t MMGR_Init(void);

/**
 * Core functionality of the Motor Manager. Function must be called regularly.
 */
void MMGR_Task(void);

/**
 * Requests the reset sequence for resetting the IRQSTAT fault bits. This requires
 * the target speed to be transitioned to 0, to force Elmos motor state machine
 * from MC_DRIVER_ERROR to MC_IDLE, then a target speed of 1 is requested which
 * moves the Elmos state machine to MC_CLEAR_DRIVER_ERROR, and then once the bits
 * are cleared, the motor manager transitions back to MMGR_IDLE. To verify that
 * the sequence is complete, you can poll on MMGR_Is_Irqstat_Actively_Clearing()
 */
void MMGR_Reset_Irqstat(void);
/**
 * Determines if the motor manager is currently in the process of resetting the
 * IRQSTAT bits via the Elmos state machine, which is a multi-step process.
 *
 * @return  bool_t
 * @retval      true Still actively clearing the bits
 * @retval      false clearing the bits is complete
 */
bool_t MMGR_Is_Irqstat_Actively_Clearing(void);

/**
 * Provides the Target Speed of the motor
 *
 * @return - Target Speed in RPM
 */
uint16_t MMGR_Get_Target_Speed(void);

/** @} doxygen end group */

#endif /* MOTOR_MANAGER_H */
