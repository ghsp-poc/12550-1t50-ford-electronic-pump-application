#ifndef MOTOR_MANAGER_IMP_DEFINES_H
#define MOTOR_MANAGER_IMP_DEFINES_H
/**
 *  @file motor_manager_imp_defines.h
 *
 *  @ref motor_manager_imp
 *
 *  @copyright 2018 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup motor_manager_imp 1T50 Motor Manager Implementation
 *  @{
 *
 *      @page motor_manager_imp_defines_api 1T50 Motor Manager defines Interface
 *
 *      Private API used internal to the implementation of the 1T50 Motor Manager
 *      module.
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "global.h"
#include "fault_manager.h"

/*===========================================================================*
 * Exported Preprocessor \#define Constants
 *===========================================================================*/
#define MMGR_TEMP_LIMITED_MAX_SPEED (2100u)
#define MMGR_LOSS_OF_COMM_SPEED (3360u)
#define MMGR_MIN_COMMANDABLE_SPEED (50u)
#define MMGR_STARTUP_RPM (500u)

#define MMGR_VOLTAGE_CONVERSION_FACTOR ((float32_t)13.69)
#define MMGR_CURRENT_CONVERSION_FACTOR ((uint16_t)51u)

#define MMGR_OVERTEMP_SET_LIMIT ((int16_t)(159))
#define MMGR_OVERTEMP_CLEAR_LIMIT ((int16_t)155)

#define MMGR_MAX_STARTUP_ATTEMPTS (2u)
#define MMGR_MAX_RETRY_ATTEMPTS   (11u)
#define MMGR_REVERSE_CLEARING_TIME_MS (500u)

/** Speed ramp rpms per second */
#define MMGR_SPEED_RAMP_RPMS_PER_SEC ((uint16_t)2500u)

/** Maximum step size in speed per motor manager thread */
#define MMGR_SPEED_RAMP_MAX_STEP_SIZE                                          \
    ((uint16_t)((MMGR_SPEED_RAMP_RPMS_PER_SEC * (uint16_t)MMGR_TIMEBASE_MS)    \
                / 1000u))

/*===========================================================================*
 * Exported Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/** @} doxygen end group */
#endif /* MOTOR_MANAGER_IMP_DEFINES_H */
