/**
 *  @file motor_manager.c
 *
 *  @ref motor_manager_api
 *
 *  @copyright 2018 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup motor_manager_imp 1T50 Motor Manager Implementation
 *  @{
 *
 *      System specific implementation of the @ref motor_manager_api
 *
 *      @page TRACE Traceability
 *        - Design Document(s):
 *          - None
 *
 *        - Applicable Standards (in order of precedence: highest first):
 *          - @http{sharepoint/sandbox/ghspdev/LaunchPad/EngineeringDepartment/Elec_proj_mgmt/Revision%20Controlled%20Library/Software%20Process/Work%20Instructions/Coding%20Standard.docx,
 *                  "GHSP Coding Standard"}
 *
 *      @page DFS Deviations from Standards
 *        - None
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "fault_logger.h"
#include "fault_manager.h"
#include "global.h"
#include "lin_vehicle_comm.h"
#include "motor_ctrl.h"
#include "motor_manager.h"
#include "motor_manager_diag.h"
#include "ford_1t50_application.h"
#include "motor_manager_imp_defines.h"
#include "motor_manager_imp_types.h"
#include "serial_data_buffer.h"
#include <stdlib.h>
#include "system_signals.h"
#include "time_utils.h"
#include "uds.h"



/*===========================================================================*
 * Local Preprocessor \#define Constants
 *===========================================================================*/
#define MMGR_NO_TEMP_LIMIT (INT16_MAX)
#define MMGR_OVER_CURRENT_CORECTION_MS (1000u)  /* ms | SWREQ_2079 SWREQ_2080 */

#define MMGR_CURRENT_LMT (150u)
/*===========================================================================*
 * Local Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Local Type Declarations
 *===========================================================================*/
typedef struct
{
    int16_t High;
    int16_t Low;
} mmgr_Temperature_Range_T;

typedef struct
{
    mmgr_Temperature_Range_T Temp_Range;
    int16_t Iq_Ref;          /* Referred to as i_q target in SWREQ_2081 */
    int16_t Current_Slope;   /* Referred to as Current ramp slope in SWREQ_2081 */
    int16_t Speed_Slope;     /* Referred to as Speed ramp slope in SWREQ_2081 */
    uint16_t Speed_Ramp_End; /* Referred to as Speed ramp end in SWREQ_2081 */
} mmgr_Startup_Parameters_T;

typedef struct
{
    mmgr_Temperature_Range_T Temp_Range;
    int16_t Speed_Max;
} mmgr_Speed_Max_T;

/*===========================================================================*
 * Exported Const Object Definitions
 *===========================================================================*/

/*===========================================================================*
 * Local Function Prototypes
 *===========================================================================*/
STATIC void mmgr_Ramp_Commanded_Speed(VCOMM_RPM_T Target_Commanded_Speed);
STATIC void mmgr_Limit_Target_Speed(VCOMM_RPM_T Commanded_Speed);
static void mmgr_Set_Startup_Configuration(VCOMM_Temperature_T oil_temp);
static void mmgr_Set_Max_Speed(VCOMM_Temperature_T oil_temp);
static void mmgr_Stall_Retry(mmgr_Retry_Reasons_T Reason);
static void mmgr_Exit_STALLED_RETRY(void);
static mc_motor_state_t mmgr_Get_Motor_State(void);

/* State Handlers */

static void mmgr_Stalled_State_Handler(void);

/*===========================================================================*
 * Local Object Definitions
 *===========================================================================*/
/**
 * Startup related parameters based on the received Sump Oil Temperature
 *
 * SWREQ_2081
 */
STATIC mmgr_Startup_Parameters_T const mmgr_Startup_Parameters[] =
{
    {
        .Temp_Range =
        {
            .High = -16,
            .Low = -51 /* not inclusive */
        },
        .Iq_Ref = 1000,
        .Current_Slope = 4,
        .Speed_Slope = -1500,
        .Speed_Ramp_End = 27u /* 500 RPM */
    },
    {
        .Temp_Range =
        {
            .High = MMGR_NO_TEMP_LIMIT,
            .Low = -16 /* not inclusive */
        },
        .Iq_Ref = 600,
        .Current_Slope = 2,
        .Speed_Slope = -600,
        .Speed_Ramp_End = 27u /* 500 RPM */
    },
};

/**
 * Maximum Speed values to set in the Elmos Motor Control based on the
 * received Oil Sump Temperature.
 *
 * SWREQ_2086
 */
STATIC mmgr_Speed_Max_T const mmgr_Speed_Max_Values[] =
{
    {
        .Temp_Range =
        {
            .High = -15,
            .Low = -51 /* Not inclusive */
        },
        .Speed_Max = 1400
    },
    {
        .Temp_Range =
        {
            .High = 19,
            .Low = -15 /* Not inclusive */
        },
        .Speed_Max = 900
    },
    {
        .Temp_Range =
        {
            .High = 69,
            .Low = 19 /* Not inclusive */
        },
        .Speed_Max = 800
    },
    {
        .Temp_Range =
        {
            .High = MMGR_NO_TEMP_LIMIT,
            .Low = 69 /* Not inclusive */
        },
        .Speed_Max = 700
    },
};

STATIC mmgr_State_T mmgr_State = MMGR_IDLE;
STATIC VCOMM_RPM_T mmgr_Target_Speed = 0u;
STATIC VCOMM_RPM_T mmgr_Max_Speed = MAX_RPM;
STATIC bool_t mmgr_CPU_Overtemp = false;
STATIC VCOMM_RPM_T mmgr_Previous_Commanded_Speed = 0u;
STATIC uint16_t mmgr_Speed_Ramp_Override_Step = MMGR_SPEED_RAMP_MAX_STEP_SIZE;
STATIC VCOMM_RPM_T mmgr_Commanded_Speed = 0u;

STATIC VCOMM_Temperature_T mmgr_Oil_Temperature = 0;

/** Number of times the motor manager has attempted to startup the motor from IDLE */
STATIC uint8_t mmgr_Startup_Attempts = 0u;

/** Number of times the motor manager has attempted to restart the motor after a stall */
STATIC uint8_t mmgr_Retry_Attempts = 0u;

/** Temperature of the oil at which the stall was detected */
STATIC VCOMM_Temperature_T mmgr_Stall_Temperature = 0;

/** Indicates the reason for a Retry */
STATIC mmgr_Retry_Reasons_T mmgr_Retry_Reason = MMGR_NO_RETRY_REASON;

STATIC mmgr_Stalled_Retry_State_T mmgr_Stalled_Retry_State = MMGR_STALLED_RETRY_FORWARD;

/** Time at which the STALLED->RETRY->REVERSE state was entered */
STATIC Time_Ticks_T mmgr_Stalled_Reverse_Basetime = 0u;

STATIC bool_t mmgr_OC_Speed_Change_Pending = false;
STATIC Time_Ticks_T mmgr_Overcurrent_Basetime = 0u;
STATIC bool_t mmgr_In_Current_Limit_Mode = false;

STATIC bool_t mmgr_Blockage_Detected = false;

STATIC bool_t mmgr_Enable_Speed_Reporting = false;

/*----------------------*\
 * Diagnostic Overrides *
\*----------------------*/
STATIC int16_t mmgr_Startup_Iq_Ref_Ovrd = 0;
STATIC bool_t mmgr_Startup_Iq_Ref_Ovrd_Enabled = false;
STATIC int16_t mmgr_Startup_Current_Slope_Ovrd = 0;
STATIC bool_t mmgr_Startup_Current_Slope_Ovrd_Enabled = false;
STATIC int16_t mmgr_Startup_Speed_Slope_Ovrd = 0;
STATIC bool_t mmgr_Startup_Speed_Slope_Ovrd_Enabled = false;
STATIC uint16_t mmgr_Startup_Speed_Ramp_End_Ovrd = 0u;
STATIC bool_t mmgr_Startup_Speed_Ramp_End_Ovrd_Enabled = false;
STATIC int16_t mmgr_Speed_Control_Output_Max_Ovrd = 0;
STATIC bool_t mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;
STATIC bool_t mmgr_Simulate_Stall_Ovrd_Enabled = false;

/*===========================================================================*
 * Function Definitions
 *===========================================================================*/
bool_t MMGR_Init(void)
{
    return true;
}

#pragma optimize=balanced
void MMGR_Task(void)
{
    int16_t CPU_Temperature = SYSIG_Get_CPU_Temp();
    bool_t Self_Protect_Active = FLT_MGR_Is_In_Self_Protect();
    VCOMM_RPM_T Commanded_Speed = VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq();
    VCOMM_Oil_Pump_Mode_T Commanded_Mode = VCOMM_Signal_Get_TrnOilPumpMde_D_Rq();
    VCOMM_TAOP_Status_T V_D_Stat = VCOMM_REQUEST_HONORED;
    bool_t Command_Is_Valid = true;

    if (VCOMM_Loss_Of_Comm())
    {
        Commanded_Speed = MMGR_LOSS_OF_COMM_SPEED;
        FLT_LOG_Record_Fault_Active(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
        FLT_LOG_Record_Fault_Inactive(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    }
    else
    {
        FLT_LOG_Record_Fault_Inactive(FLT_LOG_LIN_MASTER_REQUEST_MISSING);

        if (    (    (Commanded_Speed == 0u)
                  && (Commanded_Mode == VCOMM_MODE_RUN))
             || (    (Commanded_Speed != 0u)
                  && (Commanded_Mode == VCOMM_MODE_IDLE)))
        {
            /* Invalid DEMAND */
            V_D_Stat = VCOMM_REQUEST_DENIED;
            Commanded_Speed = 0u;
            Command_Is_Valid = false;
            FLT_LOG_Record_Fault_Active(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
        }
        else
        {
            FLT_LOG_Record_Fault_Inactive(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
        }
    }

    mmgr_Oil_Temperature = VCOMM_Signal_Get_TrnSumpOil_Te_Actl();

    mmgr_Set_Max_Speed(mmgr_Oil_Temperature);

    /* Check for self protect */
    if (    (mmgr_State != MMGR_SELF_PROTECT)
         && (Self_Protect_Active))
    {
        mmgr_State = MMGR_SELF_PROTECT;
    }

    if (CPU_Temperature >= MMGR_OVERTEMP_SET_LIMIT)
    {
        mmgr_CPU_Overtemp = true;
        mmgr_Max_Speed = MMGR_TEMP_LIMITED_MAX_SPEED;
    }
    else
    {
        if (mmgr_CPU_Overtemp)
        {
            if (CPU_Temperature < MMGR_OVERTEMP_CLEAR_LIMIT)
            {
                mmgr_CPU_Overtemp = false;
                mmgr_Max_Speed = MAX_RPM;
            }
        }
    }

    /* Cap commanded speed at max */
    if (Commanded_Speed > mmgr_Max_Speed)
    {
        Commanded_Speed = mmgr_Max_Speed;
    }

    switch (mmgr_State)
    {
        case MMGR_IDLE:
        {
            mmgr_Enable_Speed_Reporting = false;
            mc_set_target_rotor_speed(0u);
            mmgr_OC_Speed_Change_Pending = false;
            mmgr_In_Current_Limit_Mode = false;
            mmgr_Commanded_Speed = 0u;

            VCOMM_Signal_Set_TrnOilPumpMde_D_Stat(VCOMM_MODE_IDLE);
            VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl(0u);

            mmgr_Previous_Commanded_Speed = 0u;

            if (Commanded_Speed > 0u)
            {
                mmgr_Set_Startup_Configuration(mmgr_Oil_Temperature);
                mmgr_State = MMGR_STARTUP;
            }
        }
        break;

        case MMGR_STARTUP:
        {
            mc_motor_state_t Motor_State = mmgr_Get_Motor_State();
            VCOMM_RPM_T Actual_RPM = SYSIG_Get_Actual_RPM();

            if (MC_RAMP_SPEED == Motor_State)
            {
                mmgr_Enable_Speed_Reporting = true;
            }

            if (mmgr_Enable_Speed_Reporting)
            {
                VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl(Actual_RPM);
            }
            else
            {
                VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl(0u);
            }

            VCOMM_Signal_Set_TrnOilPumpMde_D_Stat(VCOMM_MODE_RUN);

            /* Check for stalls */
            if (MC_STALLED == Motor_State)
            {
                /* Setup the Stall Retry for initial startup */
                mmgr_Stall_Retry(MMGR_STARTUP_RETRY);
            }
            else
            {
                mmgr_Target_Speed = MMGR_STARTUP_RPM;

                mc_set_target_rotor_speed(mmgr_Target_Speed * SYSIG_NUM_POLE_PAIRS);

                if (0u == Commanded_Speed)
                {
                    mmgr_State = MMGR_IDLE;
                }
                else
                {
                    if (MC_CLOSED_LOOP == mc_get_comm_mode())
                    {
                        mmgr_State = MMGR_RUNNING;
                        mmgr_Commanded_Speed = Actual_RPM;
                    }
                }
            }
        }
        break;

        case MMGR_RUNNING:
        {
            VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl(SYSIG_Get_Actual_RPM());
            VCOMM_Signal_Set_TrnOilPumpMde_D_Stat(VCOMM_MODE_RUN);
            VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt(false);

            /* Check for stalls */
            if (MC_STALLED == mmgr_Get_Motor_State())
            {
                /* Setup the Stall Retry for a stall during normal running */
                mmgr_Stall_Retry(MMGR_RUNNING_RETRY);
            }
            else
            {
                if (    (Commanded_Speed < MMGR_MIN_COMMANDABLE_SPEED)
                     && (Commanded_Speed != 0u))
                {
                    Commanded_Speed = MMGR_MIN_COMMANDABLE_SPEED;
                }

                if (Commanded_Speed != mmgr_Previous_Commanded_Speed)
                {
                    mmgr_Previous_Commanded_Speed = Commanded_Speed;
                }

                mmgr_Ramp_Commanded_Speed(Commanded_Speed);

                mmgr_Limit_Target_Speed(mmgr_Commanded_Speed);

                mc_set_target_rotor_speed(mmgr_Target_Speed * SYSIG_NUM_POLE_PAIRS);

                if (0u == Commanded_Speed)
                {
                    mmgr_State = MMGR_IDLE;
                }
            }
        }
        break;

        case MMGR_SELF_PROTECT:
        {
            VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl(0u);
            VCOMM_Signal_Set_TrnOilPumpMde_D_Stat(VCOMM_MODE_IDLE);

            if (!Self_Protect_Active)
            {
                if (    (mmgr_Blockage_Detected)
                     && (Commanded_Speed != 0u))
                {
                    /* Stay in SELF_PROTECT and check for auto retry */
                    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt(true);

                    if (    (    (mmgr_Oil_Temperature <= -30)
                              && (mmgr_Oil_Temperature >= mmgr_Stall_Temperature + 1))
                         ||
                            (    (mmgr_Oil_Temperature > -30)
                              && (mmgr_Oil_Temperature <= -20)
                              && (mmgr_Oil_Temperature >= mmgr_Stall_Temperature + 2)
                            )
                       )
                    {
                        /* Retry the startup based on environmental triggers */
                        mmgr_Stall_Retry(MMGR_AUTO_RETRY);
                    }
                    else
                    {
                        mc_set_target_rotor_speed(0u);
                        V_D_Stat = VCOMM_REQUEST_DENIED;
                    }
                }
                else
                {
                    /* No blockage is detected OR the Target Speed is 0 */
                    if (Command_Is_Valid)
                    {
                        /* Received command is valid so exit Self Protect to IDLE */
                        VCOMM_Signal_Set_TrnOilPumpBlk_B_Falt(false);
                        VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt(false);
                        FLT_LOG_Record_Fault_Inactive(FLT_LOG_BLOCKED_GEROTOR);
                        mmgr_State = MMGR_IDLE;
                        mmgr_Blockage_Detected = false;
                    }
                    else
                    {
                        mc_set_target_rotor_speed(0u);
                        V_D_Stat = VCOMM_REQUEST_DENIED;
                    }
                }
            }
            else
            {
                /* Self Protect, No exit from Self Protect */
                VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt(true);
                mmgr_Target_Speed = 0u;
                mc_set_target_rotor_speed(0u);
                V_D_Stat = VCOMM_REQUEST_DENIED;
            }
        }
        break;

        case MMGR_IRQSTAT_CLEARING:
            /* check if clearing is complete, then transition to idle for restart normally (step 3)*/
            VCOMM_Signal_Set_TrnOilPumpMde_D_Stat(VCOMM_MODE_IDLE);
            VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt(true);

            if (0u == mc_get_irqstat())
            {
                mc_set_target_rotor_speed(0u);
                mmgr_State = MMGR_IDLE;
            }
            else if (mc_get_motor_state() == MC_DRIVER_ERROR)
            {
                /*
                 * to clear the error the Elmos code needs to exit MC_DRIVER_ERROR
                 * to MC_IDLE by setting target rotor speed to 0 (step 1)
                 */
                mc_set_target_rotor_speed(0u);
            }
            else /* Elmos is in MC_IDLE */
            {
                /*
                 * Once in MC_IDLE, a speed > 0 must be requested to have Elmos
                 * enter MC_CLEAR_DRIVER_ERROR, to actually clear the error. (step 2)
                 */
                mc_set_target_rotor_speed(1u);
            }
        break;   

        case MMGR_STALLED:
        {
            mmgr_Commanded_Speed = Commanded_Speed;
            mmgr_Enable_Speed_Reporting = false;
            mmgr_Stalled_State_Handler();
        }
        break;

        default:
        {
            /* Should never get here. If so, stop the motor, and return to IDLE */
            mc_set_target_rotor_speed(0u);
            mmgr_State = MMGR_IDLE;

        }
        break;
    } /* switch */

    VCOMM_Signal_Set_TrnOilPumpV_D_Stat(V_D_Stat);
}

static mc_motor_state_t mmgr_Get_Motor_State(void)
{
    mc_motor_state_t Reported_State = mc_get_motor_state();

    if (mmgr_Simulate_Stall_Ovrd_Enabled)
    {
        if (    (MC_RUNNING == Reported_State)
             || (MC_RAMP_SPEED == Reported_State))
        {
            Reported_State = MC_STALLED;
        }
    }

    return Reported_State;
}

#pragma optimize=balanced
void MMGR_Reset_Irqstat(void)
{
     mmgr_State = MMGR_IRQSTAT_CLEARING;
}

#pragma optimize=balanced
bool_t MMGR_Is_Irqstat_Actively_Clearing(void)
{
    return (bool_t)((MMGR_IRQSTAT_CLEARING == mmgr_State)
                    ? (bool_t)true
                    : (bool_t)false);
}

#pragma optimize=balanced

STATIC void mmgr_Ramp_Commanded_Speed(VCOMM_RPM_T Target_Commanded_Speed)
{
    if (    (Target_Commanded_Speed != mmgr_Commanded_Speed)
         && (mc_get_comm_mode() == MC_CLOSED_LOOP))
    {
        uint16_t const Error = (VCOMM_RPM_T)abs((int)Target_Commanded_Speed - (int)mmgr_Commanded_Speed);
        uint16_t Ramp_Step = MMGR_SPEED_RAMP_MAX_STEP_SIZE;
        uint16_t Change_Amount;

        if(mmgr_Speed_Ramp_Override_Step != MMGR_SPEED_RAMP_MAX_STEP_SIZE)
        {
            Ramp_Step = mmgr_Speed_Ramp_Override_Step;      /* override has been set by DID, use that ramp rate */
        }

        Change_Amount = ((Ramp_Step <= Error) ? Ramp_Step : Error);

        if (Target_Commanded_Speed > mmgr_Commanded_Speed)
        {
            mmgr_Commanded_Speed += Change_Amount;
        }
        else if (Target_Commanded_Speed < mmgr_Commanded_Speed)
        {
            mmgr_Commanded_Speed -= Change_Amount;
        }
        else
        {
            /* At the Target Commanded Speed. Do nothing */
        }
    }
}

#pragma optimize=balanced
STATIC void mmgr_Limit_Target_Speed(VCOMM_RPM_T Commanded_Speed)
{
    if (    (Commanded_Speed != 0u)
         && (mmgr_State != MMGR_SELF_PROTECT))
    {
        int16_t current = SYSIG_Get_Current();
        VCOMM_RPM_T current_adjust_amount = ((Commanded_Speed - MMGR_MIN_COMMANDABLE_SPEED) / 10u);;

        if(mmgr_OC_Speed_Change_Pending)
        {
            if (TMUT_Has_Time_Elapsed_Ticks(mmgr_Overcurrent_Basetime, TMUT_MS_To_Ticks(MMGR_OVER_CURRENT_CORECTION_MS)))
            {
                mmgr_OC_Speed_Change_Pending = false;  
                if(current > (int16_t)MMGR_CURRENT_LMT)
                {
                    /* need to slow down */
                    mmgr_In_Current_Limit_Mode = true;
                    if(mmgr_Target_Speed >= (current_adjust_amount + MMGR_MIN_COMMANDABLE_SPEED))
                    {
                        mmgr_Target_Speed -= current_adjust_amount;
                    }
                    else
                    {
                       mmgr_State = MMGR_SELF_PROTECT;
                       mmgr_Blockage_Detected = true;
                       VCOMM_Signal_Set_TrnOilPumpBlk_B_Falt(true);
                       FLT_LOG_Record_Fault_Active(FLT_LOG_BLOCKED_GEROTOR);
                       mmgr_Target_Speed = 0u;
                    }
                }
                else if( mmgr_Target_Speed < Commanded_Speed)
                {
                    /* need to speed up */
                    mmgr_Target_Speed += current_adjust_amount;
                    if(mmgr_Target_Speed >= Commanded_Speed)
                    {
                        mmgr_In_Current_Limit_Mode = false;  
                    }
                }
                else{;}
            }
        }
        else if(current > (int16_t)MMGR_CURRENT_LMT)
        {
            mmgr_Overcurrent_Basetime = TMUT_Get_Base_Time_Ticks();
            mmgr_OC_Speed_Change_Pending = true; /*  true only when speed change needed */
        }
        else if(mmgr_In_Current_Limit_Mode && (mmgr_Target_Speed < Commanded_Speed))
        {
            /* Must be in recovery from over current induced slow down */
            mmgr_Overcurrent_Basetime = TMUT_Get_Base_Time_Ticks();
            mmgr_OC_Speed_Change_Pending = true; /*  true only when speed change needed */
        }
        else
        {
            mmgr_Target_Speed = Commanded_Speed;
        }

    }
    else
    {
        mmgr_Target_Speed = 0u;
    }

    if(mmgr_Target_Speed > Commanded_Speed)
    {
        /* There is no reason target speed should be above commanded speed */
        mmgr_Target_Speed = Commanded_Speed;
    }
}

#pragma optimize=balanced
static void mmgr_Set_Startup_Configuration(VCOMM_Temperature_T oil_temp)
{
    size_t range = 0u;

    /* Find the proper temperature range */
    for (range = 0u; range < Num_Elems(mmgr_Startup_Parameters); range++)
    {
        if (    (oil_temp > mmgr_Startup_Parameters[range].Temp_Range.Low)
             && (oil_temp <= mmgr_Startup_Parameters[range].Temp_Range.High))
        {
            /* Set proper value for Startup Iq Target */
            if (mmgr_Startup_Iq_Ref_Ovrd_Enabled)
            {
                mc_set_startup_i_q_ref(mmgr_Startup_Iq_Ref_Ovrd);
            }
            else
            {
                mc_set_startup_i_q_ref(mmgr_Startup_Parameters[range].Iq_Ref);
            }

            /* Set proper value for Startup Current Slope */
            if (mmgr_Startup_Current_Slope_Ovrd_Enabled)
            {
                mc_set_startup_current_slope(mmgr_Startup_Current_Slope_Ovrd);
            }
            else
            {
                mc_set_startup_current_slope(mmgr_Startup_Parameters[range].Current_Slope);
            }

            /* Set proper value for Startup Speed Slop */
            if (mmgr_Startup_Speed_Slope_Ovrd_Enabled)
            {
                mc_set_startup_speed_slope(mmgr_Startup_Speed_Slope_Ovrd);
            }
            else
            {
                mc_set_startup_speed_slope(mmgr_Startup_Parameters[range].Speed_Slope);
            }

            /* Set proper value for Startup Speed Ramp End */
            if (mmgr_Startup_Speed_Ramp_End_Ovrd_Enabled)
            {
                mc_set_startup_speed_ramp_end(mmgr_Startup_Speed_Ramp_End_Ovrd);
            }
            else
            {
                mc_set_startup_speed_ramp_end(mmgr_Startup_Parameters[range].Speed_Ramp_End);
            }

            break;
        }
    }
}

static void mmgr_Set_Max_Speed(VCOMM_Temperature_T oil_temp)
{
    size_t range = 0u;

    /* Find the proper temperature range */
    for (range = 0u; range < Num_Elems(mmgr_Speed_Max_Values); range++)
    {
        if (    (oil_temp > mmgr_Speed_Max_Values[range].Temp_Range.Low)
             && (oil_temp <= mmgr_Speed_Max_Values[range].Temp_Range.High))
        {
            /* Set proper value for Speed Control Output Maximum */
            if (mmgr_Speed_Control_Output_Max_Ovrd_Enabled)
            {
                mc_set_speed_ctrl_output_max(mmgr_Speed_Control_Output_Max_Ovrd);
            }
            else
            {
                mc_set_speed_ctrl_output_max(mmgr_Speed_Max_Values[range].Speed_Max);
            }
            break;
        }
    }
}

static void mmgr_Stall_Retry(mmgr_Retry_Reasons_T Reason)
{
    mc_set_target_rotor_speed(0u);

    if (Reason != MMGR_NO_RETRY_REASON)
    {
        mmgr_State = MMGR_STALLED;
        mmgr_Startup_Attempts = 0u;
        mmgr_Retry_Attempts = 0u;
        mmgr_Blockage_Detected = false;

        mmgr_Retry_Reason = Reason;
    }
}

uint16_t MMGR_Get_Target_Speed(void)
{
    return mmgr_Target_Speed;
}

/* Motor Manager State handlers */
#pragma optimize=balanced
static void mmgr_Stalled_State_Handler(void)
{
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat(VCOMM_MODE_RUN);

    /* If commanded to IDLE, quit handling a Stall */
    if (0u == mmgr_Commanded_Speed)
    {
        mmgr_State = MMGR_IDLE;
        mmgr_Retry_Attempts = 0u;
        mmgr_Startup_Attempts = 0u;
    }
    else
    {
        switch (mmgr_Stalled_Retry_State)
        {
            case MMGR_STALLED_RETRY_FORWARD:
            {
                /* has the motor achieved closed loop? */
                if (MC_CLOSED_LOOP == mc_get_comm_mode())
                {
                    mmgr_State = MMGR_RUNNING;
                    mmgr_Retry_Attempts = 0u;
                    mmgr_Startup_Attempts = 0u;
                    mmgr_Commanded_Speed = SYSIG_Get_Actual_RPM();
                }
                else
                {
                    /* Check for another stall */
                    if (MC_STALLED == mmgr_Get_Motor_State())
                    {
                        mmgr_Retry_Attempts++;
                        mc_set_target_rotor_speed(0u);

                        if (mmgr_Retry_Attempts >= MMGR_MAX_RETRY_ATTEMPTS)
                        {
                            mmgr_Stalled_Retry_State = MMGR_STALLED_RETRY_REVERSE;
                            mmgr_Stalled_Reverse_Basetime = TMUT_Get_Base_Time_Ticks();
                        }
                        else
                        {
                            mmgr_Stalled_Retry_State = MMGR_STALLED_RETRY_FAILED;
                        }
                    }
                    else
                    {
                        mc_set_direction(FORWARD);
                        mc_set_target_rotor_speed(MMGR_STARTUP_RPM * SYSIG_NUM_POLE_PAIRS);
                    }
                }
            }
            break;

            case MMGR_STALLED_RETRY_REVERSE:
            {
                if (TMUT_Has_Time_Elapsed_Ticks(mmgr_Stalled_Reverse_Basetime, TMUT_MS_To_Ticks(MMGR_REVERSE_CLEARING_TIME_MS)))
                {
                    mmgr_Exit_STALLED_RETRY();
                }
                else
                {
                    mc_set_direction(BACKWARD);
                    mc_set_target_rotor_speed(MMGR_STARTUP_RPM * SYSIG_NUM_POLE_PAIRS);
                }
            }
            break;

            case MMGR_STALLED_RETRY_FAILED:
            {
                if (MC_IDLE == mmgr_Get_Motor_State())
                {
                    mmgr_Stalled_Retry_State = MMGR_STALLED_RETRY_FORWARD;
                }
            }
            break;

            default:
            {

            }
            break;
        }
    }
}

static void mmgr_Exit_STALLED_RETRY(void)
{
    mc_set_direction(FORWARD);
    mc_set_target_rotor_speed(0u);
    mmgr_Stalled_Retry_State = MMGR_STALLED_RETRY_FORWARD;
    mmgr_Stall_Temperature = mmgr_Oil_Temperature;
    mmgr_Blockage_Detected = true;

    if (MMGR_STARTUP_RETRY == mmgr_Retry_Reason)
    {
        mmgr_Startup_Attempts++;

        if (mmgr_Startup_Attempts < MMGR_MAX_STARTUP_ATTEMPTS)
        {
            mmgr_Retry_Attempts = 0u;
        }
        else
        {
            mmgr_State = MMGR_SELF_PROTECT;
            mmgr_Retry_Attempts = 0u;
            mmgr_Startup_Attempts = 0u;
            VCOMM_Signal_Set_TrnOilPumpBlk_B_Falt(true);
            FLT_LOG_Record_Fault_Active(FLT_LOG_BLOCKED_GEROTOR);
         }
    }
    else
    {
        mmgr_State = MMGR_SELF_PROTECT;
        mmgr_Retry_Attempts = 0u;
        mmgr_Startup_Attempts = 0u;
        VCOMM_Signal_Set_TrnOilPumpBlk_B_Falt(true);
        FLT_LOG_Record_Fault_Active(FLT_LOG_BLOCKED_GEROTOR);
    }
}
/*-------------------------------*\
 * Diagnostic Override functions *
\*-------------------------------*/
void MMGR_Override_Startup_Iq_Ref(int16_t Startup_Iq_Ref_Override)
{
    if (INT16_MIN != Startup_Iq_Ref_Override)
    {
        mmgr_Startup_Iq_Ref_Ovrd = Startup_Iq_Ref_Override;
        mmgr_Startup_Iq_Ref_Ovrd_Enabled = true;
    }
    else
    {
        /* Disable the override */
        mmgr_Startup_Iq_Ref_Ovrd_Enabled = false;
    }
}

void MMGR_Override_Startup_Current_Slope(int16_t Startup_Current_Slope_Override)
{
    if (INT16_MIN != Startup_Current_Slope_Override)
    {
        mmgr_Startup_Current_Slope_Ovrd = Startup_Current_Slope_Override;
        mmgr_Startup_Current_Slope_Ovrd_Enabled = true;
    }
    else
    {
        /* Disable the override */
        mmgr_Startup_Current_Slope_Ovrd_Enabled = false;
    }
}

void MMGR_Override_Startup_Speed_Slope(int16_t Startup_Speed_Slope_Override)
{
    if (INT16_MIN != Startup_Speed_Slope_Override)
    {
        mmgr_Startup_Speed_Slope_Ovrd = Startup_Speed_Slope_Override;
        mmgr_Startup_Speed_Slope_Ovrd_Enabled = true;
    }
    else
    {
        /* Disable the override */
        mmgr_Startup_Speed_Ramp_End_Ovrd_Enabled = false;
    }
}

void MMGR_Override_Startup_Speed_Ramp_End(uint16_t Startup_Speed_Ramp_End_Override)
{
    if (UINT16_MAX != Startup_Speed_Ramp_End_Override)
    {
        mmgr_Startup_Speed_Ramp_End_Ovrd = Startup_Speed_Ramp_End_Override;
        mmgr_Startup_Speed_Ramp_End_Ovrd_Enabled = true;
    }
    else
    {
        /* Disable the override */
        mmgr_Startup_Speed_Ramp_End_Ovrd_Enabled = false;
    }
}

void MMGR_Override_Speed_Control_Output_Max(int16_t Speed_Control_Output_Max)
{
    if (INT16_MIN != Speed_Control_Output_Max)
    {
        mmgr_Speed_Control_Output_Max_Ovrd = Speed_Control_Output_Max;
        mmgr_Speed_Control_Output_Max_Ovrd_Enabled = true;
    }
    else
    {
        /* Disable the override */
        mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;
    }
}

UDS_Response_Code_T MMGR_Get_Diag_Simulate_Stall(Serial_Data_Buffer_T * const p_Resp_SDB)
{
    SDB_Serialize_U8(p_Resp_SDB, mmgr_Simulate_Stall_Ovrd_Enabled ? 1u : 0u);

    return UDS_RESP_POSITIVE;
}

UDS_Response_Code_T MMGR_Diag_Simulate_Stall(Serial_Data_Buffer_T * const p_Req_SDB)
{
    bool_t Simulate_Stall = (bool_t)SDB_Deserialize_U8(p_Req_SDB);

    if (Simulate_Stall)
    {
        mmgr_Simulate_Stall_Ovrd_Enabled = true;
        mc_set_remain_in_open_loop(true);
    }
    else
    {
        mmgr_Simulate_Stall_Ovrd_Enabled = false;
        mc_set_remain_in_open_loop(false);
    }

    return UDS_RESP_POSITIVE;
}

UDS_Response_Code_T MMGR_Get_Speed_Ramp_Rate(Serial_Data_Buffer_T * const p_Resp_SDB)
{
    UDS_Response_Code_T Response = UDS_RESP_POSITIVE;
    /* convert ramp step to RPM per second */
    uint16_t Ramp_Rate = ((mmgr_Speed_Ramp_Override_Step * 1000u) / (uint16_t)MMGR_TIMEBASE_MS);

    SDB_Serialize_U16(p_Resp_SDB, Ramp_Rate);

    return Response;
}


UDS_Response_Code_T MMGR_Override_Speed_Ramp_Rate(Serial_Data_Buffer_T * const p_Req_SDB)
{
    UDS_Response_Code_T Response = UDS_RESP_POSITIVE;
    uint16_t Ramp_Rate_Override = SDB_Deserialize_U16(p_Req_SDB);

    if (UINT16_MAX != Ramp_Rate_Override)
    {
        Ramp_Rate_Override = Ramp_Rate_Override;
    }
    else
    {
        Ramp_Rate_Override = MMGR_SPEED_RAMP_RPMS_PER_SEC;
    }

   /* Convert RPM per second to max ramp step per iteration */
    mmgr_Speed_Ramp_Override_Step = ((uint16_t)((Ramp_Rate_Override * (uint16_t)MMGR_TIMEBASE_MS) / 1000u));

    return Response;
}

/** @} doxygen end group */
