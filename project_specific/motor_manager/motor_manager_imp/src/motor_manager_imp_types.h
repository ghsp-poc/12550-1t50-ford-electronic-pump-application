#ifndef MOTOR_MANAGER_IMP_TYPES_H
#define MOTOR_MANAGER_IMP_TYPES_H

/**
 *  @file motor_manager_imp_types.h
 *
 *  @copyright 2019 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential@n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup mmgr_imp_api Motor Manager Private Interface Documentation
 *  @{
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "global.h"

/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/
typedef enum
{
    MMGR_IDLE,
    MMGR_IRQSTAT_CLEARING,
    MMGR_STARTUP,
    MMGR_RUNNING,
    MMGR_STALLED,
    MMGR_SELF_PROTECT
} mmgr_State_T;

typedef enum
{
    MMGR_STALLED_RETRY_FORWARD,
    MMGR_STALLED_RETRY_REVERSE,
    MMGR_STALLED_RETRY_FAILED
} mmgr_Stalled_Retry_State_T;

typedef enum
{
    MMGR_NO_RETRY_REASON,
    MMGR_AUTO_RETRY,
    MMGR_STARTUP_RETRY,
    MMGR_RUNNING_RETRY,
    MMGR_NUM_RETRY_REASONS
} mmgr_Retry_Reasons_T;

/** @} doxygen end group */
#endif /* MOTOR_MANAGER_IMP_TYPES_H */
