/**
 *  @file test_Speed_Ramp_Rate_DID.c
 *
 */
#include "unity.h"
#include "global.h"
#include "serial_data_buffer.h"
#include "motor_manager_diag.h"
#include "motor_manager_imp_defines.h"
#include "ford_1t50_application.h"

/* MOCKS */
#include "mock_lin_vehicle_comm.h"
#include "mock_fault_logger.h"
#include "mock_fault_manager.h"
#include "mock_motor_ctrl.h"
#include "mock_serial_data_buffer.h"
#include "mock_speed_meas.h"
#include "mock_system_signals.h"
#include "mock_time_utils.h"

/* STATIC's */
uint16_t mmgr_Speed_Ramp_Override_Step;

void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * Verifies that the DID Get function returns the current value of the Ramp Rate
 *
 */
void test_MMGR_Get_Speed_Ramp_Rate_serializes_the_current_Speed_Ramp_Rate(void)
{
    Serial_Data_Buffer_T Test_SDB;

    /* Ensure known test state */
    mmgr_Speed_Ramp_Override_Step = 25;     /* 25 is the step when mmgr time base is 10ms and ramp slope is 2500 RPM per second.  */

    /* Setup expected call chain */
    SDB_Serialize_U16_ExpectAndReturn(&Test_SDB, 2500, SDB_EOK);

    /* Call function under test */
    MMGR_Get_Speed_Ramp_Rate(&Test_SDB);
    
    /* Verify test results */
}

/**
 * Verifies that the DID Override function will set mmgr_Speed_Ramp_Override_Step to
 * the requested value
 *
 */
void test_MMGR_Override_Speed_Ramp_Rate_sets_the_Speed_Ramp_Rate(void)
{
    Serial_Data_Buffer_T Test_SDB;

    /* Ensure known test state */
    mmgr_Speed_Ramp_Override_Step = 0;

    /* Setup expected call chain */
    SDB_Deserialize_U16_ExpectAndReturn(&Test_SDB, 2500);

    /* Call function under test */
    MMGR_Override_Speed_Ramp_Rate(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT16(25, mmgr_Speed_Ramp_Override_Step);
}

/**
 * Verifies that the DID Override function will set mmgr_Commanded_Speed_Ramp_Rate to
 * the requested value
 *
 */
void test_MMGR_Override_Speed_Ramp_Rate_restores_the_Speed_Ramp_Rate_to_MMGR_SPEED_RAMP_RATE(void)
{
    Serial_Data_Buffer_T Test_SDB;

    /* Ensure known test state */
    mmgr_Speed_Ramp_Override_Step = 0;

    /* Setup expected call chain */
    SDB_Deserialize_U16_ExpectAndReturn(&Test_SDB, UINT16_MAX);

    /* Call function under test */
    MMGR_Override_Speed_Ramp_Rate(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT16(MMGR_SPEED_RAMP_MAX_STEP_SIZE, mmgr_Speed_Ramp_Override_Step);
}
