/**
 *  @file test_MMGR_State_Machine.c
 *
 */
#include "unity.h"
#include "global.h"
#include "motor_manager.h"
#include "motor_manager_imp_defines.h"
#include "motor_manager_imp_types.h"
#include "system_signals.h"

/* MOCKS */
#include "mock_lin_vehicle_comm.h"
#include "mock_fault_logger.h"
#include "mock_fault_manager.h"
#include "mock_motor_ctrl.h"
#include "mock_serial_data_buffer.h"
#include "mock_system_signals.h"
#include "mock_time_utils.h"

/* STATIC's */
mmgr_State_T mmgr_State;
uint16_t mmgr_Target_Speed;
uint16_t mmgr_Max_Speed;
uint8_t mmgr_Startup_Attempts;
VCOMM_RPM_T mmgr_Commanded_Speed;
VCOMM_RPM_T mmgr_Previous_Commanded_Speed;

int16_t mmgr_Startup_Iq_Ref_Ovrd;
bool_t mmgr_Startup_Iq_Ref_Ovrd_Enabled;
int16_t mmgr_Startup_Current_Slope_Ovrd;
bool_t mmgr_Startup_Current_Slope_Ovrd_Enabled;
int16_t mmgr_Startup_Speed_Slope_Ovrd;
bool_t mmgr_Startup_Speed_Slope_Ovrd_Enabled;
uint16_t mmgr_Startup_Speed_Ramp_End_Ovrd;
bool_t mmgr_Startup_Speed_Ramp_End_Ovrd_Enabled;
int16_t mmgr_Speed_Control_Output_Max_Ovrd;
bool_t mmgr_Speed_Control_Output_Max_Ovrd_Enabled;

bool_t mmgr_Enable_Speed_Reporting;


void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * @test Verifies that Motor Manager will transition from the IDLE state to the
 * SELF_PROTECT state when any self protect fault is active.
 */
/* Reqs: SWREQ_1186, SWREQ_2052, SWREQ_2055, SWREQ_2086 */
void test_Motor_Manager_IDLE_to_SELF_PROTECT_When_a_self_protect_fault_is_Active(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_IDLE;
    mmgr_Target_Speed = 0;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(0);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_IDLE);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(true);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    mc_set_target_rotor_speed_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(true);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_DENIED);

    /* Call function under test */
    MMGR_Task();
    
    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_SELF_PROTECT, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(0, mmgr_Target_Speed);
    
}

/**
 * @test Verifies that Motor Manager will transition from the RUNNING state to the
 * SELF_PROTECT state when any self protect fault is active.
 */
/* Reqs: SWREQ_1186, SWREQ_2052, SWREQ_2055 */
void test_Motor_Manager_RUNNING_to_SELF_PROTECT_When_a_self_protect_fault_is_Active(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_RUNNING;
    mmgr_Target_Speed = 1000;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
      VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(true);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    mc_set_target_rotor_speed_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(true);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_DENIED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_SELF_PROTECT, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(0, mmgr_Target_Speed);

}

/**
 * @test Verifies that Motor Manager will stay in IDLE and report the proper
 * status while in that state and command the motor to stop.
 */
/* Reqs: SWREQ_1985 */
void test_Motor_Manager_IDLE_Reports_IDLE(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_IDLE;
    mmgr_Target_Speed = 0;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(0);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_IDLE);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    mc_set_target_rotor_speed_Expect(0); /* Command speed of 0 RPM */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_IDLE, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(0, mmgr_Target_Speed);

}

/**
 * @test Verifies that Motor Manager will transition to the STARTUP state from IDLE
 * when a valid speed command is received. This test case if for a reported temperature
 * of -50 degrees C
 */
/* Reqs: SWREQ_1985, SWREQ_2081, SWREQ_2086 */
void test_Motor_Manager_IDLE_Transitions_to_STARTUP_and_Sets_Commanded_Speed_neg_50_C_No_Override(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_IDLE;
    mmgr_Target_Speed = 0;

    mmgr_Startup_Iq_Ref_Ovrd_Enabled = false;
    mmgr_Startup_Current_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Ramp_End_Ovrd_Enabled = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    mc_set_target_rotor_speed_Expect(0); /* Command speed of 0 RPM */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);

    mc_set_startup_i_q_ref_Expect(1000);
    mc_set_startup_current_slope_Expect(4);
    mc_set_startup_speed_slope_Expect(-1500);
    mc_set_startup_speed_ramp_end_Expect(27);

    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_STARTUP, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(0, mmgr_Target_Speed);

}

/**
 * @test Verifies that Motor Manager will transition to the STARTUP state from IDLE
 * when a valid speed command is received. This test case Is for when all diagnostics overrides are
 * enabled for the motor startup parameters.
 */
/* Reqs: SWREQ_1985, SWREQ_2081, SWREQ_2086 */
void test_Motor_Manager_IDLE_Transitions_to_STARTUP_and_Sets_Commanded_Speed_neg_50_C_All_Overrides(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_IDLE;
    mmgr_Target_Speed = 0;

    mmgr_Startup_Iq_Ref_Ovrd_Enabled = true;
    mmgr_Startup_Current_Slope_Ovrd_Enabled = true;
    mmgr_Startup_Speed_Slope_Ovrd_Enabled = true;
    mmgr_Startup_Speed_Ramp_End_Ovrd_Enabled = true;

    mmgr_Startup_Iq_Ref_Ovrd = 300;
    mmgr_Startup_Current_Slope_Ovrd = -14;
    mmgr_Startup_Speed_Slope_Ovrd = -300;
    mmgr_Startup_Speed_Ramp_End_Ovrd = 30;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    mc_set_target_rotor_speed_Expect(0); /* Command speed of 0 RPM */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);

    mc_set_startup_i_q_ref_Expect(300);
    mc_set_startup_current_slope_Expect(-14);
    mc_set_startup_speed_slope_Expect(-300);
    mc_set_startup_speed_ramp_end_Expect(30);

    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_STARTUP, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(0, mmgr_Target_Speed);

}

/**
 * @test Verifies that Motor Manager will transition to the STARTUP state from IDLE
 * when a valid speed command is received. This test case if for a reported temperature
 * of -16 degrees C
 */
/* Reqs: SWREQ_1985, SWREQ_2081, SWREQ_2086 */
void test_Motor_Manager_IDLE_Transitions_to_STARTUP_and_Sets_Commanded_Speed_neg_16_C_No_Override(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_IDLE;
    mmgr_Target_Speed = 0;

    mmgr_Startup_Iq_Ref_Ovrd_Enabled = false;
    mmgr_Startup_Current_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Ramp_End_Ovrd_Enabled = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-16); /* -16 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    mc_set_target_rotor_speed_Expect(0); /* Command speed of 0 RPM */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);

    mc_set_startup_i_q_ref_Expect(1000);
    mc_set_startup_current_slope_Expect(4);
    mc_set_startup_speed_slope_Expect(-1500);
    mc_set_startup_speed_ramp_end_Expect(27);

    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_STARTUP, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(0, mmgr_Target_Speed);

}

/**
 * @test Verifies that Motor Manager will transition to the STARTUP state from IDLE
 * when a valid speed command is received. This test case if for a reported temperature
 * of -15 degree C
 */
/* Reqs: SWREQ_1985, SWREQ_2081, SWREQ_2086 */
void test_Motor_Manager_IDLE_Transitions_to_STARTUP_and_Sets_Commanded_Speed_neg_15_C_No_Override(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_IDLE;
    mmgr_Target_Speed = 0;

    mmgr_Startup_Iq_Ref_Ovrd_Enabled = false;
    mmgr_Startup_Current_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Ramp_End_Ovrd_Enabled = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-15); /* -15 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    mc_set_target_rotor_speed_Expect(0); /* Command speed of 0 RPM */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);

    mc_set_startup_i_q_ref_Expect(600);
    mc_set_startup_current_slope_Expect(2);
    mc_set_startup_speed_slope_Expect(-600);
    mc_set_startup_speed_ramp_end_Expect(27);

    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_STARTUP, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(0, mmgr_Target_Speed);

}

/**
 * @test Verifies that Motor Manager will transition to the STARTUP state from IDLE
 * when a valid speed command is received. This test case if for a reported temperature
 * of 25 degrees C
 */
/* Reqs: SWREQ_1985, SWREQ_2081, SWREQ_2086 */
void test_Motor_Manager_IDLE_Transitions_to_STARTUP_and_Sets_Commanded_Speed_25_C_No_Override(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_IDLE;
    mmgr_Target_Speed = 0;

    mmgr_Startup_Iq_Ref_Ovrd_Enabled = false;
    mmgr_Startup_Current_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Ramp_End_Ovrd_Enabled = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(25); /* 25 degrees C */
    mc_set_speed_ctrl_output_max_Expect(800); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    mc_set_target_rotor_speed_Expect(0); /* Command speed of 0 RPM */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);

    mc_set_startup_i_q_ref_Expect(600);
    mc_set_startup_current_slope_Expect(2);
    mc_set_startup_speed_slope_Expect(-600);
    mc_set_startup_speed_ramp_end_Expect(27);

    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_STARTUP, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(0, mmgr_Target_Speed);

}

/**
 * @test Verifies that Motor Manager will transition to the STARTUP state from IDLE
 * when a valid speed command is received. This test case if for a reported temperature
 * of 50 degrees C
 */
/* Reqs: SWREQ_1985, SWREQ_2081, SWREQ_2086 */
void test_Motor_Manager_IDLE_Transitions_to_STARTUP_and_Sets_Commanded_Speed_50_C_No_Override(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_IDLE;
    mmgr_Target_Speed = 0;

    mmgr_Startup_Iq_Ref_Ovrd_Enabled = false;
    mmgr_Startup_Current_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Ramp_End_Ovrd_Enabled = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(50); /* 50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(800); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    mc_set_target_rotor_speed_Expect(0); /* Command speed of 0 RPM */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);

    mc_set_startup_i_q_ref_Expect(600);
    mc_set_startup_current_slope_Expect(2);
    mc_set_startup_speed_slope_Expect(-600);
    mc_set_startup_speed_ramp_end_Expect(27);

    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_STARTUP, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(0, mmgr_Target_Speed);

}

/**
 * @test Verifies that Motor Manager will transition to the STARTUP state from IDLE
 * when a valid speed command is received. This test case if for a reported temperature
 * of 204 degrees C (the maximum value reported by this signal)
 */
/* Reqs: SWREQ_1985, SWREQ_2081, SWREQ_2086 */
void test_Motor_Manager_IDLE_Transitions_to_STARTUP_and_Sets_Commanded_Speed_204_C_No_Override(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_IDLE;
    mmgr_Target_Speed = 0;

    mmgr_Startup_Iq_Ref_Ovrd_Enabled = false;
    mmgr_Startup_Current_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Ramp_End_Ovrd_Enabled = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(204); /* 204 degrees C */
    mc_set_speed_ctrl_output_max_Expect(700); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    mc_set_target_rotor_speed_Expect(0); /* Command speed of 0 RPM */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);

    mc_set_startup_i_q_ref_Expect(600);
    mc_set_startup_current_slope_Expect(2);
    mc_set_startup_speed_slope_Expect(-600);
    mc_set_startup_speed_ramp_end_Expect(27);

    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_STARTUP, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(0, mmgr_Target_Speed);

}

/**
 * @test Verifies that Motor Manager will set the maximum speed output to 1900 when the
 * received oil temperature is -50 degrees C
 */
/* Reqs: SWREQ_2086 */
void test_Motor_Manager_IDLE_Sets_Max_Speed_to_1000_at_neg50_C_Oil_Temp(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_IDLE;
    mmgr_Target_Speed = 0;

    mmgr_Startup_Iq_Ref_Ovrd_Enabled = false;
    mmgr_Startup_Current_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Ramp_End_Ovrd_Enabled = false;
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(0);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_IDLE);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    mc_set_target_rotor_speed_Expect(0); /* Command speed of 0 RPM */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */

}

/**
 * @test Verifies that Motor Manager will set the maximum speed output to 400 when the
 * received oil temperature is -50 degrees C because of an override.
 */
/* Reqs: SWREQ_2086 */
void test_Motor_Manager_IDLE_Sets_Max_Speed_to_400_at_neg50_C_Oil_Temp_With_Override(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_IDLE;
    mmgr_Target_Speed = 0;

    mmgr_Startup_Iq_Ref_Ovrd_Enabled = false;
    mmgr_Startup_Current_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Ramp_End_Ovrd_Enabled = false;
    mmgr_Speed_Control_Output_Max_Ovrd = 400;
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = true;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(0);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_IDLE);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(400); /* Override */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    mc_set_target_rotor_speed_Expect(0); /* Command speed of 0 RPM */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */

}

/**
 * @test Verifies that Motor Manager will set the maximum speed output to 1400 when the
 * received oil temperature is -15 degrees C
 */
/* Reqs: SWREQ_2086 */
void test_Motor_Manager_IDLE_Sets_Max_Speed_to_1400_at_neg15_C_Oil_Temp(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_IDLE;
    mmgr_Target_Speed = 0;

    mmgr_Startup_Iq_Ref_Ovrd_Enabled = false;
    mmgr_Startup_Current_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Ramp_End_Ovrd_Enabled = false;
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(0);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_IDLE);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-15); /* -15 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    mc_set_target_rotor_speed_Expect(0); /* Command speed of 0 RPM */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */

}
/**
 * @test Verifies that Motor Manager will set the maximum speed output to 900 when the
 * received oil temperature is -14 degrees C
 */
/* Reqs: SWREQ_2086 */
void test_Motor_Manager_IDLE_Sets_Max_Speed_to_900_at_neg14_C_Oil_Temp(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_IDLE;
    mmgr_Target_Speed = 0;

    mmgr_Startup_Iq_Ref_Ovrd_Enabled = false;
    mmgr_Startup_Current_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Ramp_End_Ovrd_Enabled = false;
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(0);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_IDLE);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-14); /* -14 degrees C */
    mc_set_speed_ctrl_output_max_Expect(900); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    mc_set_target_rotor_speed_Expect(0); /* Command speed of 0 RPM */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */

}

/**
 * @test Verifies that Motor Manager will set the maximum speed output to 900 when the
 * received oil temperature is 19 degrees C
 */
/* Reqs: SWREQ_2086 */
void test_Motor_Manager_IDLE_Sets_Max_Speed_to_900_at_19_C_Oil_Temp(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_IDLE;
    mmgr_Target_Speed = 0;

    mmgr_Startup_Iq_Ref_Ovrd_Enabled = false;
    mmgr_Startup_Current_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Ramp_End_Ovrd_Enabled = false;
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(0);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_IDLE);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(19); /* 19 degrees C */
    mc_set_speed_ctrl_output_max_Expect(900); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    mc_set_target_rotor_speed_Expect(0); /* Command speed of 0 RPM */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */

}

/**
 * @test Verifies that Motor Manager will set the maximum speed output to 800 when the
 * received oil temperature is 20 degrees C
 */
/* Reqs: SWREQ_2086 */
void test_Motor_Manager_IDLE_Sets_Max_Speed_to_800_at_20_C_Oil_Temp(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_IDLE;
    mmgr_Target_Speed = 0;

    mmgr_Startup_Iq_Ref_Ovrd_Enabled = false;
    mmgr_Startup_Current_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Ramp_End_Ovrd_Enabled = false;
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(0);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_IDLE);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(20); /* 20 degrees C */
    mc_set_speed_ctrl_output_max_Expect(800); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    mc_set_target_rotor_speed_Expect(0); /* Command speed of 0 RPM */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */

}

/**
 * @test Verifies that Motor Manager will set the maximum speed output to 800 when the
 * received oil temperature is 69 degrees C
 */
/* Reqs: SWREQ_2086 */
void test_Motor_Manager_IDLE_Sets_Max_Speed_to_800_at_69_C_Oil_Temp(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_IDLE;
    mmgr_Target_Speed = 0;

    mmgr_Startup_Iq_Ref_Ovrd_Enabled = false;
    mmgr_Startup_Current_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Ramp_End_Ovrd_Enabled = false;
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(0);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_IDLE);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(69); /* 69 degrees C */
    mc_set_speed_ctrl_output_max_Expect(800); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    mc_set_target_rotor_speed_Expect(0); /* Command speed of 0 RPM */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */

}

/**
 * @test Verifies that Motor Manager will set the maximum speed output to 700 when the
 * received oil temperature is 70 degrees C
 */
/* Reqs: SWREQ_2086 */
void test_Motor_Manager_IDLE_Sets_Max_Speed_to_700_at_70_C_Oil_Temp(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_IDLE;
    mmgr_Target_Speed = 0;

    mmgr_Startup_Iq_Ref_Ovrd_Enabled = false;
    mmgr_Startup_Current_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Ramp_End_Ovrd_Enabled = false;
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(0);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_IDLE);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(70); /* 70 degrees C */
    mc_set_speed_ctrl_output_max_Expect(700); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    mc_set_target_rotor_speed_Expect(0); /* Command speed of 0 RPM */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */

}

/**
 * @test Verifies that Motor Manager will set the maximum speed output to 700 when the
 * received oil temperature is 204 degrees C
 */
/* Reqs: SWREQ_2086 */
void test_Motor_Manager_IDLE_Sets_Max_Speed_to_700_at_204_C_Oil_Temp(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_IDLE;
    mmgr_Target_Speed = 0;

    mmgr_Startup_Iq_Ref_Ovrd_Enabled = false;
    mmgr_Startup_Current_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Ramp_End_Ovrd_Enabled = false;
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(0);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_IDLE);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(204); /* 204 degrees C */
    mc_set_speed_ctrl_output_max_Expect(700); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    mc_set_target_rotor_speed_Expect(0); /* Command speed of 0 RPM */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */

}

/**
 * @test Verifies that Motor Manager will report the running state and command the
 * motor to run at the proper speed while in the STARTUP state. Until the SPEED_RAMP
 * state is achieved, it will report V_Pc_Actl as 0.
 */
/* Reqs: SWREQ_1985 */
void test_Motor_Manager_STARTUP_Commands_Speed_and_Reports_RUN_and_V_Pc_Actl_0_before_SPEED_RAMP(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_STARTUP;
    mmgr_Target_Speed = 0;
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;
    mmgr_Enable_Speed_Reporting = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    mc_set_target_rotor_speed_Expect(MMGR_STARTUP_RPM * SYSIG_NUM_POLE_PAIRS); /* Command speed of 1992 RPM */
    mc_get_motor_state_ExpectAndReturn(MC_RAMP_CURRENT);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(500);
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0); /* Set LIN signal for Actual Speed to 0 */
    mc_get_comm_mode_ExpectAndReturn(MC_OPEN_LOOP);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_RUN);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_STARTUP, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(MMGR_STARTUP_RPM, mmgr_Target_Speed);
    TEST_ASSERT_FALSE(mmgr_Enable_Speed_Reporting);
}

/**
 * @test Verifies that Motor Manager will report the running state and command the
 * motor to run at the proper speed while in the STARTUP state. Once the SPEED_RAMP
 * state is achieved, the actual speed is reported.
 */
/* Reqs: SWREQ_1985 */
void test_Motor_Manager_STARTUP_Commands_Speed_and_Reports_RUN_V_Pc_Actl_after_SPEED_RAMP(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_STARTUP;
    mmgr_Target_Speed = 0;
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;
    mmgr_Enable_Speed_Reporting = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    mc_set_target_rotor_speed_Expect(MMGR_STARTUP_RPM * SYSIG_NUM_POLE_PAIRS); /* Command speed of 1992 RPM */
    mc_get_motor_state_ExpectAndReturn(MC_RAMP_SPEED);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(500); 
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(500); /* Set LIN signal for Actual Speed to 500 */
    mc_get_comm_mode_ExpectAndReturn(MC_OPEN_LOOP);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_RUN);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_STARTUP, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(MMGR_STARTUP_RPM, mmgr_Target_Speed);
    TEST_ASSERT_TRUE(mmgr_Enable_Speed_Reporting);

}

/**
 * @test Verifies that Motor Manager while in the IDLE state will transition
 * to the STARTUP state when there is a loss of LIN detected.
 */
/* Reqs: SWREQ_1985 */
void test_Motor_Manager_IDLE_transitions_to_STARTUP_with_loss_of_LIN(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_IDLE;
    mmgr_Target_Speed = 0;
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(100); /* 100 degrees C */
    mc_set_speed_ctrl_output_max_Expect(700); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(true);
    mc_set_target_rotor_speed_Expect(0); /* Command speed of 0 RPM */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    mc_set_startup_i_q_ref_Expect(600);
    mc_set_startup_current_slope_Expect(2);
    mc_set_startup_speed_slope_Expect(-600);
    mc_set_startup_speed_ramp_end_Expect(27);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_STARTUP, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(0, mmgr_Target_Speed);

}

/**
 * @test Verifies that Motor Manager will report the running state and command the
 * motor to run at the proper speed while in the STARTUP state.
 */
/* Reqs: SWREQ_2059 */
void test_Motor_Manager_STARTUP_Commands_Speed_and_Reports_RUN_no_Stall(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_STARTUP;
    mmgr_Target_Speed = 0;
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;
    mmgr_Enable_Speed_Reporting = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    mc_set_target_rotor_speed_Expect(MMGR_STARTUP_RPM * SYSIG_NUM_POLE_PAIRS); /* Command speed of 1992 RPM */
    mc_get_motor_state_ExpectAndReturn(MC_RAMP_SPEED); /* No stall reported */
    SYSIG_Get_Actual_RPM_ExpectAndReturn(500);
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(500); /* Set LIN signal for Actual Speed to 0 until Speed Ramp*/
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_RUN);
    mc_get_comm_mode_ExpectAndReturn(MC_OPEN_LOOP);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_STARTUP, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(MMGR_STARTUP_RPM, mmgr_Target_Speed);
    TEST_ASSERT_TRUE(mmgr_Enable_Speed_Reporting); /* Speed Ramp has been reached, report speed */
}

/**
 * @test Verifies that Motor Manager will command the motor to spin at the
 * commanded speed and report the actual speed reported by the FOC module.
 */
/* Reqs: SWREQ_2059 */
void test_Motor_Manager_RUNNING_Sets_Commanded_Speed_Report_Actual(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_RUNNING;
    mmgr_Target_Speed = 1000;
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;
    mmgr_Commanded_Speed = 1992;
    mmgr_Previous_Commanded_Speed = 1992; /* Commanded speed has not changed */

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    mc_set_target_rotor_speed_Expect(1992 * SYSIG_NUM_POLE_PAIRS); /* Command speed of 1992 RPM */
    SYSIG_Get_Actual_RPM_ExpectAndReturn(500); 
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(500); /* Set LIN signal for Actual Speed to 500 */
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_RUN);
    mc_get_motor_state_ExpectAndReturn(MC_RUNNING); /* Motor Control is running normally. */
    SYSIG_Get_Current_ExpectAndReturn(0);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);
    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(false);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_RUNNING, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(1992, mmgr_Target_Speed);

}

/*--------------------------------------------------------*\
 * Temperature related speed limit test cases for RUNNING *
\*--------------------------------------------------------*/
/**
 * @test Verifies that Motor Manager will retain the normal max speed limit
 * when at 158 degrees C
 */
/* Reqs: SWREQ_1930 */
void test_Motor_Manager_RUNNING_Limits_MAX_Speed_to_4200_at_158(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_RUNNING;
    mmgr_Target_Speed = 3000;
    mmgr_Max_Speed = 4200; /* Start unlimited by temperature */
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;
    mmgr_Commanded_Speed = 3000;
    mmgr_Previous_Commanded_Speed = 3000; /* Commanded speed has not changed */

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(3000);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(158);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    mc_set_target_rotor_speed_Expect(3000 * SYSIG_NUM_POLE_PAIRS);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(500); 
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(500); /* Set LIN signal for Actual Speed to 500 */
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_RUN);
    mc_get_motor_state_ExpectAndReturn(MC_RUNNING); /* Motor Control is running normally. */
    SYSIG_Get_Current_ExpectAndReturn(50);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);
    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(false);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_RUNNING, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(4200, mmgr_Max_Speed);

}

/**
 * @test Verifies that Motor Manager will set the max speed limit to 2100
 * when a measured CPU temperature of 159 degrees C is reached
 */
/* Reqs: SWREQ_1930 */
void test_Motor_Manager_RUNNING_Limits_MAX_Speed_to_2100_at_159(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_RUNNING;
    mmgr_Target_Speed = 3000;
    mmgr_Max_Speed = 4200; /* Start unlimited by temperature */
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;
    mmgr_Commanded_Speed = 2100;
    mmgr_Previous_Commanded_Speed = 2100; /* Commanded speed has not changed */

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(3000);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(159);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    mc_set_target_rotor_speed_Expect(2100 * SYSIG_NUM_POLE_PAIRS); /* Target speed capped */
    SYSIG_Get_Actual_RPM_ExpectAndReturn(500); 
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(500); /* Set LIN signal for Actual Speed to 500 */
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_RUN);
    mc_get_motor_state_ExpectAndReturn(MC_RUNNING); /* Motor Control is running normally. */
    SYSIG_Get_Current_ExpectAndReturn(50);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);
    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(false);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(2100, mmgr_Target_Speed);
    TEST_ASSERT_EQUAL(MMGR_RUNNING, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(2100, mmgr_Max_Speed);

}

/**
 * @test Verifies that Motor Manager will retain the maximum speed limit of 2100 RPM
 * at 155 degrees C after it was previously limited.
 */
/* Reqs: SWREQ_1931 */
void test_Motor_Manager_RUNNING_Limits_MAX_Speed_To_2100_at_155_After_Being_Limited(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_RUNNING;
    mmgr_Target_Speed = 3000;
    mmgr_Max_Speed = 2100; /* Start limited by temperature */
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;
    mmgr_Commanded_Speed = 2100;
    mmgr_Previous_Commanded_Speed = 2100; /* Commanded speed has not changed */

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(3000);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(155);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    mc_set_target_rotor_speed_Expect(2100 * SYSIG_NUM_POLE_PAIRS); /* Target Speed capped */
    SYSIG_Get_Actual_RPM_ExpectAndReturn(500); 
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(500); /* Set LIN signal for Actual Speed to 500 */
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_RUN);
    mc_get_motor_state_ExpectAndReturn(MC_RUNNING); /* Motor Control is running normally. */
    SYSIG_Get_Current_ExpectAndReturn(50);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);
    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(false);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(2100, mmgr_Target_Speed);
    TEST_ASSERT_EQUAL(MMGR_RUNNING, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(2100, mmgr_Max_Speed);

}

/**
 * @test Verifies that Motor Manager will sets the maximum speed limit to 4200
 * after it was limited to 2100 when the temperature is less than 155 degrees C
 */
/* Reqs: SWREQ_1931 */
void test_Motor_Manager_RUNNING_Limits_MAX_Speed_To_4200_at_154_After_Being_Limited(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_RUNNING;
    mmgr_Target_Speed = 3000;
    mmgr_Max_Speed = 2100; /* Start limited by temperature */
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;
    mmgr_Commanded_Speed = 3000;
    mmgr_Previous_Commanded_Speed = 3000; /* Commanded speed has not changed */

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(3000);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(154);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    mc_set_target_rotor_speed_Expect(3000 * SYSIG_NUM_POLE_PAIRS); /* Target Speed not capped */
    SYSIG_Get_Actual_RPM_ExpectAndReturn(500);
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(500); /* Set LIN signal for Actual Speed to 500 */
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_RUN);
    mc_get_motor_state_ExpectAndReturn(MC_RUNNING); /* Motor Control is running normally. */
    SYSIG_Get_Current_ExpectAndReturn(50);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);
    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(false);    

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(3000, mmgr_Target_Speed);
    TEST_ASSERT_EQUAL(MMGR_RUNNING, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(4200, mmgr_Max_Speed);

}

/*--------------------------------------------------------*\
 * Temperature related speed limit test cases for IDLE *
\*--------------------------------------------------------*/
/**
 * @test Verifies that Motor Manager will retain the normal max speed limit
 * when at 158 degrees C
 */
/* Reqs: SWREQ_1930 */
void test_Motor_Manager_IDLE_Limits_MAX_Speed_to_4200_at_158(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_IDLE;
    mmgr_Target_Speed = 0;
    mmgr_Max_Speed = 4200; /* Start unlimited by temperature */
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(0);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_IDLE);
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(158);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    mc_set_target_rotor_speed_Expect(0); /* no commanded speed */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_IDLE, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(4200, mmgr_Max_Speed);

}

/**
 * @test Verifies that Motor Manager will set the max speed limit to 2100
 * when a measured CPU temperature of 159 degrees C is reached
 */
/* Reqs: SWREQ_1930 */
void test_Motor_Manager_IDLE_Limits_MAX_Speed_to_2100_at_159(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_IDLE;
    mmgr_Target_Speed = 0;
    mmgr_Max_Speed = 4200; /* Start unlimited by temperature */
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(0);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_IDLE);
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(159);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    mc_set_target_rotor_speed_Expect(0); /* no commanded speed */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);


    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_IDLE, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(2100, mmgr_Max_Speed);

}

/**
 * @test Verifies that Motor Manager will retain the maximum speed limit of 2100 RPM
 * at 155 degrees C after it had been limited. In IDLE with no commanded speed. Current
 * draw is not relevant.
 */
/* Reqs: SWREQ_1931 */
void test_Motor_Manager_IDLE_Limits_MAX_Speed_To_2100_at_155_After_Being_Limited(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_IDLE;
    mmgr_Target_Speed = 0;
    mmgr_Max_Speed = 2100; /* Start limited by temperature */
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(0);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_IDLE);
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(155);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    mc_set_target_rotor_speed_Expect(0); /* no commanded speed */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_IDLE, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(2100, mmgr_Max_Speed);

}

/**
 * @test Verifies that Motor Manager will sets the maximum speed limit to 4200
 * after it was limited to 2100 when the temperature is less than 155 degrees C
 */
/* Reqs: SWREQ_1931 */
void test_Motor_Manager_IDLE_Limits_MAX_Speed_To_4200_at_154_After_Being_Limited(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_IDLE;
    mmgr_Target_Speed = 0;
    mmgr_Max_Speed = 2100; /* Start limited by temperature */
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(0);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_IDLE);
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(154);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    mc_set_target_rotor_speed_Expect(0); /* no commanded speed */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_IDLE, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(4200, mmgr_Max_Speed);

}

/*------------------------------------*\
 * Loss of Communications test cases  *
\*------------------------------------*/
/**
 * @test Verifies that Motor Manager will set the target speed to 3360
 * when there is a loss of communications while in the IDLE State
 */
/* Reqs: SWREQ_1927 */
void test_Motor_Manager_IDLE_Sets_Target_Speed_to_3360_when_Loss_Of_Comms(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_IDLE;
    mmgr_Target_Speed = 0;
    mmgr_Max_Speed = 4200;
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(0);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_IDLE);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(100); /* 100 degrees C */
    mc_set_speed_ctrl_output_max_Expect(700); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(true);
    mc_set_target_rotor_speed_Expect(0); /* IDLE state */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    mc_set_startup_i_q_ref_Expect(600);
    mc_set_startup_current_slope_Expect(2);
    mc_set_startup_speed_slope_Expect(-600);
    mc_set_startup_speed_ramp_end_Expect(27);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_STARTUP, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(0, mmgr_Target_Speed);

}

/**
 * @test Verifies that Motor Manager will set the target speed to 3360
 * when there is a loss of communications while in the RUNNING State
 */
/* Reqs: SWREQ_1927 */
void test_Motor_Manager_RUNNING_Sets_Target_Speed_to_3360_when_Loss_Of_Comms(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_RUNNING;
    mmgr_Target_Speed = 1000;
    mmgr_Max_Speed = 4200;
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;
    mmgr_Commanded_Speed = 3360;
    mmgr_Previous_Commanded_Speed = 3360; /* Commanded speed has not changed */

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(0);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_IDLE);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(true);
    mc_set_target_rotor_speed_Expect(3360 * SYSIG_NUM_POLE_PAIRS); /* Set to Loss of Comm speed */
    SYSIG_Get_Actual_RPM_ExpectAndReturn(500); 
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(500); /* Set LIN signal for Actual Speed to 500 */
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_RUN);
    mc_get_motor_state_ExpectAndReturn(MC_RUNNING); /* Motor Control is running normally. */
    SYSIG_Get_Current_ExpectAndReturn(50);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);
    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(false);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_RUNNING, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(3360, mmgr_Target_Speed);

}

/**
 * @test Verifies that Motor Manager will set the target speed to 2100
 * when there is a loss of communications while in the IDLE State because
 * of a temperature limit
 */
/* Reqs: SWREQ_1927, SWREQ_1930 */
void test_Motor_Manager_IDLE_Sets_Target_Speed_to_2100_when_Loss_Of_Comms_and_Overtemp(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_IDLE;
    mmgr_Target_Speed = 0;
    mmgr_Max_Speed = 4200;
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(0);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_IDLE);
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(true);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(159);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(100); /* 100 degrees C */
    mc_set_speed_ctrl_output_max_Expect(700); /* SWREQ_2086 */
    mc_set_target_rotor_speed_Expect(0); /* IDLE state */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    mc_set_startup_i_q_ref_Expect(600);
    mc_set_startup_current_slope_Expect(2);
    mc_set_startup_speed_slope_Expect(-600);
    mc_set_startup_speed_ramp_end_Expect(27);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_STARTUP, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(0, mmgr_Target_Speed);
    TEST_ASSERT_EQUAL_UINT16(2100, mmgr_Max_Speed);

}

/**
 * @test Verifies that Motor Manager will set the target speed to 2100
 * when there is a loss of communications while in the RUNNING State because
 * of a temperature limit
 */
/* Reqs: SWREQ_1927, SWREQ_1930 */
void test_Motor_Manager_RUNNING_Sets_Target_Speed_to_2100_when_Loss_Of_Comms_and_Overtemp(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_RUNNING;
    mmgr_Target_Speed = 1000;
    mmgr_Max_Speed = 4200;
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;
    mmgr_Commanded_Speed = 2100;
    mmgr_Previous_Commanded_Speed = 2100; /* Commanded speed has not changed */

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(0);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_IDLE);
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(true);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(159);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    SYSIG_Get_Current_ExpectAndReturn(50); /* Reports 5.0 Amps. Less than maximum allowed */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    mc_set_target_rotor_speed_Expect(2100 * SYSIG_NUM_POLE_PAIRS); /* Speed limited due to temperature */
    SYSIG_Get_Actual_RPM_ExpectAndReturn(500); 
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(500); /* Set LIN signal for Actual Speed to 500 */
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_RUN);
    mc_get_motor_state_ExpectAndReturn(MC_RUNNING); /* Motor Control is running normally. */
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);
    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(false);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_RUNNING, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(2100, mmgr_Target_Speed);
    TEST_ASSERT_EQUAL_UINT16(2100, mmgr_Max_Speed);

}

/**
 * @test Verifies that Motor Manager will stay in the STARTUP state while the FOC motor
 * subsystem is reporting that it is operating in Open Loop mode
 */
/* Reqs: SWREQ_1382 */
void test_Motor_Manager_STARTUP_Stays_in_STARTUP_when_Open_Loop(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_STARTUP;
    mmgr_Target_Speed = 1000;
    mmgr_Max_Speed = 4200;
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;
    mmgr_Enable_Speed_Reporting = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1000);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    mc_set_target_rotor_speed_Expect(MMGR_STARTUP_RPM * SYSIG_NUM_POLE_PAIRS);
    mc_get_motor_state_ExpectAndReturn(MC_START_COMMUTATION); /* Has not made it to Speed Ramp yet*/
    SYSIG_Get_Actual_RPM_ExpectAndReturn(500);
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0); /* Set LIN signal for Actual Speed to 0 until Speed Ramp */
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_RUN);
    mc_get_comm_mode_ExpectAndReturn(MC_OPEN_LOOP);

    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_STARTUP, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(MMGR_STARTUP_RPM, mmgr_Target_Speed);
    TEST_ASSERT_EQUAL_UINT16(4200, mmgr_Max_Speed);

}

/**
 * @test Verifies that Motor Manager will transitions from the STARTUP state to the RUNNING state
 * when the FOC motor subsystem indicates that closed loop operation has been achieved
 */
/* Reqs: SWREQ_1382 */
void test_Motor_Manager_STARTUP_Transitions_to_RUNNING_when_Closed_Loop(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_STARTUP;
    mmgr_Target_Speed = 1000;
    mmgr_Max_Speed = 4200;
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;
    mmgr_Enable_Speed_Reporting = true; /* Has made it to Speed Ramp previously */

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1000);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    mc_get_motor_state_ExpectAndReturn(MC_RUNNING);
    mc_set_target_rotor_speed_Expect(MMGR_STARTUP_RPM * SYSIG_NUM_POLE_PAIRS);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(500); 
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(500); /* Set LIN signal for Actual Speed to 500 */
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_RUN);
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_RUNNING, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(MMGR_STARTUP_RPM, mmgr_Target_Speed);
    TEST_ASSERT_EQUAL_UINT16(4200, mmgr_Max_Speed);
    TEST_ASSERT_EQUAL(500, mmgr_Commanded_Speed); /* Start commanding the speed at the point where we are to achieve ramp */
}

/**
 * @test Verifies that Motor Manager will peg the target speed to 50 rpm
 * when the V_Pc_Rq is less than 50 RPM but not 0. In this test case,
 * the raw signal of V_Pc_Rq will be 2 detents.
 */
/* Reqs: SWREQ_1923 */
void test_Motor_Manager_RUNNING_V_Pc_Rq_2_Commands_50_RPM(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_RUNNING;
    mmgr_Target_Speed = 0;
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;
    mmgr_Commanded_Speed = 50;
    mmgr_Previous_Commanded_Speed = 50; /* Commanded speed has not changed */

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(49);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    SYSIG_Get_Current_ExpectAndReturn(50); /* Report 5.0 Amps. Less than max allowed */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    mc_set_target_rotor_speed_Expect(MMGR_MIN_COMMANDABLE_SPEED * SYSIG_NUM_POLE_PAIRS);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(50); 
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(50); /* Set LIN signal for Actual Speed to 50 */
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_RUN);
    mc_get_motor_state_ExpectAndReturn(MC_RUNNING); /* Motor Control is running normally. */
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);
    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(false);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_RUNNING, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(50, mmgr_Target_Speed);
}

/**
 * @test Verifies that Motor Manager will peg the target speed to 50 rpm
 * when the V_Pc_Rq is less than 50 RPM but not 0. In this test case,
 * the raw signal of V_Pc_Rq will be 1 detent.
 */
/* Reqs: SWREQ_1923 */
void test_Motor_Manager_RUNNING_V_Pc_Rq_1_Commands_50_RPM(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_RUNNING;
    mmgr_Target_Speed = 0;
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(15);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    SYSIG_Get_Current_ExpectAndReturn(50); /* Report 5.0 Amps. Less than max allowed */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    mc_set_target_rotor_speed_Expect(MMGR_MIN_COMMANDABLE_SPEED * SYSIG_NUM_POLE_PAIRS);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(50); 
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(50); /* Set LIN signal for Actual Speed to 50 */
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_RUN);
    mc_get_motor_state_ExpectAndReturn(MC_RUNNING); /* Motor Control is running normally. */
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);
    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(false);    

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_RUNNING, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(50, mmgr_Target_Speed);
}


/**
 * @test Verifies that Motor Manager will set the target speed to 0 
 * when there is a non zero IRQSTAT fault.  (Step 1)
 * 
 */
/* Reqs: */
void test_Motor_Manager_MMGR_IRQSTAT_CLEARING_sets_target_speed_to_0_when_IRQSTAT_fault(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_IRQSTAT_CLEARING;
    mmgr_Target_Speed = 300;
   

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(15);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); 
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(true);

    mc_get_irqstat_ExpectAndReturn(0x200); /* under voltage on gate fault */


    
    mc_get_motor_state_ExpectAndReturn(MC_DRIVER_ERROR); /* IRQSTAT non zero */
    mc_set_target_rotor_speed_Expect(0);                /* SWREQ TBD */
    
    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_IRQSTAT_CLEARING, mmgr_State);

}


/**
 * @test Verifies that Motor Manager will set the target speed to 1 
 * when there is a non zero IRQSTAT fault.  (Step 2)
 * 
 */
/* Reqs: */

void test_Motor_Manager_MMGR_IRQSTAT_CLEARING_sets_target_speed_to_1_when_mc_get_motor_state_returns_MMGR_IDLE(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_IRQSTAT_CLEARING;
    mmgr_Target_Speed = 0;
   
    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(15);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); 
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);

    mc_get_irqstat_ExpectAndReturn(0x200);       /* under voltage on gate fault */
    mc_get_motor_state_ExpectAndReturn(MC_IDLE);  
    mc_set_target_rotor_speed_Expect(1);                                /* SWREQ TBD */
    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(true);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_IRQSTAT_CLEARING, mmgr_State);

}

/**
 * @test Verifies that Motor Manager will set the target speed to 1 
 * when there mc_motor_state is MMGR_IDLE (Step 3, IRQSTAT cleared)
 * 
 */
/* Reqs: */
void test_Motor_Manager_MMGR_IRQSTAT_CLEARING_sets_mmgr_State_to_MMGR_IDLE_when_no_IRQSTAT_fault(void)

{
    /* Ensure known test state */
    mmgr_State = MMGR_IRQSTAT_CLEARING;
    mmgr_Target_Speed = 0;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(15);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); 

    mc_get_irqstat_ExpectAndReturn(0);   /* IRQSTAT non zeror */
    mc_set_target_rotor_speed_Expect(0);                                /* SWREQ TBD */
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
     VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(true);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_IDLE, mmgr_State);

}


