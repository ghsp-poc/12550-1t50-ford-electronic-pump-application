/**
 *  @file test_STALLED_state.c
 *
 */
#include "unity.h"
#include "global.h"
#include "motor_manager.h"
#include "motor_manager_imp_defines.h"
#include "motor_manager_imp_types.h"

/* MOCKS */
#include "mock_lin_vehicle_comm.h"
#include "mock_fault_logger.h"
#include "mock_fault_manager.h"
#include "mock_motor_ctrl.h"
#include "mock_serial_data_buffer.h"
#include "mock_system_signals.h"
#include "mock_time_utils.h"

/* STATIC's */
mmgr_State_T mmgr_State;
uint16_t mmgr_Target_Speed;
uint8_t mmgr_Startup_Attempts;
uint8_t mmgr_Retry_Attempts;
VCOMM_Temperature_T mmgr_Stall_Temperature;
mmgr_Retry_Reasons_T mmgr_Retry_Reason;
mmgr_Stalled_Retry_State_T mmgr_Stalled_Retry_State;
Time_MS_T mmgr_Stalled_Reverse_Basetime;
bool_t mmgr_Blockage_Detected;

int16_t mmgr_Startup_Iq_Ref_Ovrd;
bool_t mmgr_Startup_Iq_Ref_Ovrd_Enabled;
int16_t mmgr_Startup_Current_Slope_Ovrd;
bool_t mmgr_Startup_Current_Slope_Ovrd_Enabled;
int16_t mmgr_Startup_Speed_Slope_Ovrd;
bool_t mmgr_Startup_Speed_Slope_Ovrd_Enabled;
uint16_t mmgr_Startup_Speed_Ramp_End_Ovrd;
bool_t mmgr_Startup_Speed_Ramp_End_Ovrd_Enabled;
int16_t mmgr_Speed_Control_Output_Max_Ovrd;
bool_t mmgr_Speed_Control_Output_Max_Ovrd_Enabled;

bool_t mmgr_Enable_Speed_Reporting;

Time_Ticks_T mmgr_Commanded_Speed_Ramp_Basetime;
VCOMM_RPM_T mmgr_Commanded_Speed;

void setUp(void)
{
    mmgr_Startup_Iq_Ref_Ovrd_Enabled = false;
    mmgr_Startup_Current_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Slope_Ovrd_Enabled = false;
    mmgr_Startup_Speed_Ramp_End_Ovrd_Enabled = false;
    mmgr_Speed_Control_Output_Max_Ovrd_Enabled = false;
}

void tearDown(void)
{

}

/**
 * @test Verifies that Motor Manager while in the STARTUP state will transition to the STALLED
 * state when the motor control module reports that a stall occurred.
 */
/* Reqs: SWREQ_1381, SWREQ_1382*/
void test_Motor_Manager_STARTUP_transitions_to_STALLED_when_motor_control_reports_STALLED(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_STARTUP;
    mmgr_Target_Speed = MMGR_STARTUP_RPM;
    mmgr_Startup_Attempts = 0;
    mmgr_Enable_Speed_Reporting = true; /* Has achieved Speed Ramp, stalled after */

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(500); 
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(500); /* Set LIN signal for Actual Speed to 500 */
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_RUN);
    mc_set_target_rotor_speed_Expect(0); /* Command speed of 0 RPM because of stall */
    mc_get_motor_state_ExpectAndReturn(MC_STALLED); /* Stall reported */
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_STALLED, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(MMGR_STARTUP_RPM, mmgr_Target_Speed); /* Target speed should be retained. */
}

/**
 * @test Verifies that Motor Manager while in the RUNNING state will transition to
 * STALLED when the motor control module reports a stall
 */
/* Reqs: SWREQ_1929*/
void test_Motor_Manager_RUNNING_transitions_to_STALLED_when_motor_control_reports_STALLED(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_RUNNING;
    mmgr_Target_Speed = 1000;
    mmgr_Startup_Attempts = 0;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(500); 
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(500); /* Set LIN signal for Actual Speed to 500 */
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_RUN);
    mc_get_motor_state_ExpectAndReturn(MC_STALLED); /* Stall reported */
    mc_set_target_rotor_speed_Expect(0); /* Command speed of 0 RPM because of stall */
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);
    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(false);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_STALLED, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(1000, mmgr_Target_Speed); /* Target speed should be retained. */
    TEST_ASSERT_EQUAL(MMGR_RUNNING_RETRY, mmgr_Retry_Reason);

}

/**
 * @test Verifies that Motor Manager while in the SELF_PROTECT state will transition to
 * STALLED for an Auto Retry when the oil temperature is reported at -25 degrees and the
 * SELF_PROTECT Occurred at -27 degrees, an increase of 2.
 */
/* Reqs: SWREQ_2085 */
void test_Motor_Manager_SELF_PROTECT_transitions_to_STALLED_at_neg25_after_2_degree_increase(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_SELF_PROTECT;
    mmgr_Target_Speed = 0;
    mmgr_Startup_Attempts = 0;
    mmgr_Stall_Temperature = -27; /* Stall was recored at -27 degrees */
    mmgr_Blockage_Detected = true;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-25); /* -25 degrees C */
    mc_set_speed_ctrl_output_max_Ignore(); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(true);
    mc_set_target_rotor_speed_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_STALLED, mmgr_State);
    TEST_ASSERT_EQUAL(MMGR_STALLED_RETRY_FORWARD, mmgr_Stalled_Retry_State);
    TEST_ASSERT_EQUAL_UINT16(0, mmgr_Target_Speed); /* Self Protect target speed is always 0 */
    TEST_ASSERT_EQUAL(MMGR_AUTO_RETRY, mmgr_Retry_Reason);
    TEST_ASSERT_FALSE(mmgr_Blockage_Detected);
}

/**
 * @test Verifies that Motor Manager while in the SELF_PROTECT state will stay in SELF_PROTECT
 * when the oil temperature is reported at -26 degrees and the SELF_PROTECT Occurred at -27 degrees,
 * an increase of 1.
 */
/* Reqs: SWREQ_2085 */
void test_Motor_Manager_SELF_PROTECT_stays_in_SELF_PROTECT_at_neg26_after_1_degree_increase(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_SELF_PROTECT;
    mmgr_Target_Speed = 0;
    mmgr_Startup_Attempts = 0;
    mmgr_Stall_Temperature = -27; /* Stall was recored at -27 degrees */
    mmgr_Blockage_Detected = true;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-26); /* -26 degrees C */
    mc_set_speed_ctrl_output_max_Ignore(); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    mc_set_target_rotor_speed_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(true);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_DENIED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_SELF_PROTECT, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(0, mmgr_Target_Speed); /* Self Protect target speed is always 0 */
}

/**
 * @test Verifies that Motor Manager while in the SELF_PROTECT state will stay in SELF_PROTECT
 * when the oil temperature is reported at -27 degrees and the SELF_PROTECT Occurred at -27 degrees.
 */
/* Reqs: SWREQ_2085 */
void test_Motor_Manager_SELF_PROTECT_stays_in_SELF_PROTECT_at_neg27_no_increase(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_SELF_PROTECT;
    mmgr_Target_Speed = 0;
    mmgr_Startup_Attempts = 0;
    mmgr_Stall_Temperature = -27; /* Stall was recored at -27 degrees */
    mmgr_Blockage_Detected = true;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-27); /* -27 degrees C */
    mc_set_speed_ctrl_output_max_Ignore(); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    mc_set_target_rotor_speed_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(true);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_DENIED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_SELF_PROTECT, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(0, mmgr_Target_Speed); /* Self Protect target speed is always 0 */
}

/**
 * @test Verifies that Motor Manager while in the SELF_PROTECT state will transition to
 * STALLED for an Auto Retry when the oil temperature is reported at -35 degrees and the
 * SELF_PROTECT Occurred at -36 degrees, an increase of 1.
 */
/* Reqs: SWREQ_2104 */
void test_Motor_Manager_SELF_PROTECT_transitions_to_STALLED_at_neg35_after_1_degree_increase(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_SELF_PROTECT;
    mmgr_Target_Speed = 0;
    mmgr_Startup_Attempts = 0;
    mmgr_Stall_Temperature = -36; /* Stall was recored at -36 degrees */
    mmgr_Blockage_Detected = true;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-35); /* -35 degrees C */
    mc_set_speed_ctrl_output_max_Ignore(); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(true);
    mc_set_target_rotor_speed_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_STALLED, mmgr_State);
    TEST_ASSERT_EQUAL(MMGR_STALLED_RETRY_FORWARD, mmgr_Stalled_Retry_State);
    TEST_ASSERT_EQUAL_UINT16(0, mmgr_Target_Speed); /* Self Protect target speed is always 0 */
    TEST_ASSERT_EQUAL(MMGR_AUTO_RETRY, mmgr_Retry_Reason);
    TEST_ASSERT_FALSE(mmgr_Blockage_Detected);

}

/**
 * @test Verifies that Motor Manager while in the SELF_PROTECT state will stay in SELF_PROTECT
 * when the oil temperature is reported at -36 degrees and the SELF_PROTECT Occurred at -36 degrees.
 */
/* Reqs: SWREQ_2085 */
void test_Motor_Manager_SELF_PROTECT_stays_in_SELF_PROTECT_at_neg36_no_increase(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_SELF_PROTECT;
    mmgr_Target_Speed = 0;
    mmgr_Startup_Attempts = 0;
    mmgr_Stall_Temperature = -36; /* Stall was recored at -36 degrees */
    mmgr_Blockage_Detected = true;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-36); /* -36 degrees C */
    mc_set_speed_ctrl_output_max_Ignore(); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    mc_set_target_rotor_speed_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(true);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_DENIED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_SELF_PROTECT, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(0, mmgr_Target_Speed); /* Self Protect target speed is always 0 */
}

/**
 * @test Verifies that Motor Manager while in the STALLED state will perform the Retry Strategy a second time
 * when the initial stall was during a STARTUP state and it has completed the full retry strategy once.
 */
/* Reqs: SWREQ_1382 */
void test_Motor_Manager_STALLED_restarts_Retry_Strategy_when_Entered_from_STARTUP(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_STALLED;
    mmgr_Target_Speed = 1992;
    mmgr_Retry_Attempts = 11; /* Maximum retries */
    mmgr_Retry_Reason = MMGR_STARTUP_RETRY;
    mmgr_Stalled_Retry_State = MMGR_STALLED_RETRY_REVERSE;
    mmgr_Stalled_Reverse_Basetime = 5000; /* Number is arbitrary. Used to track expected value to mock */

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_RUN);
    TMUT_MS_To_Ticks_ExpectAndReturn(500, 724); /* Made up value for the number of ticks in 500 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(5000, 724, true);
    mc_set_direction_Expect(FORWARD);
    mc_set_target_rotor_speed_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_STALLED, mmgr_State);
    TEST_ASSERT_EQUAL(MMGR_STALLED_RETRY_FORWARD, mmgr_Stalled_Retry_State);
    TEST_ASSERT_EQUAL_UINT8(0, mmgr_Retry_Attempts);
    TEST_ASSERT_EQUAL_UINT8(1, mmgr_Startup_Attempts);
    TEST_ASSERT_EQUAL_UINT16(1992, mmgr_Target_Speed); /* Target speed should be retained. */
}

/**
 * @test Verifies that Motor Manager while in the STALLED state will transition to
 * the RUNNING state if the Motor Control module reports that the motor is running in closed loop
 * mode.
 */
/* Reqs: SWREQ_2071 */
void test_Motor_Manager_STALLED_transitions_to_RUNNING_when_motor_control_reports_closed_loop(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_STALLED;
    mmgr_Target_Speed = 1992;
    mmgr_Startup_Attempts = 5;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_RUN);
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(500);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_RUNNING, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(1992, mmgr_Target_Speed); /* Target speed should be retained. */
    TEST_ASSERT_EQUAL(500, mmgr_Commanded_Speed);
}

/**
 * @test Verifies that Motor Manager while in the STALLED state will transition to
 * the IDLE state if the commanded speed becomes 0.
 */
/* Reqs: SWREQ_2070 */
void test_Motor_Manager_STALLED_transitions_to_IDLE_when_Target_Speed_0(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_STALLED;
    mmgr_Target_Speed = 0;
    mmgr_Startup_Attempts = 5;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(0);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_IDLE);
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_RUN);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_IDLE, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(0, mmgr_Target_Speed); /* Target speed should be retained. */
}

/**
 * @test Verifies that Motor Manager while in the STALLED->RETRY->FORWARD will transition to the
 * STALLED->RETRY->FAILED state and reset the commanded speed to 0 when the motor control module
 * reports a new stall. It will also increment the number of retry attempts.
 */
/* Reqs: SWREQ_1382 */
void test_Motor_Manager_STALLED_resets_speed_to_0_when_new_Stall_detected_5_previous_attempts(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_STALLED;
    mmgr_Target_Speed = 1992;
    mmgr_Retry_Attempts = 5; /* less than maximum attempts */
    mmgr_Stalled_Retry_State = MMGR_STALLED_RETRY_FORWARD;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_RUN);
    mc_get_comm_mode_ExpectAndReturn(MC_OPEN_LOOP);
    mc_get_motor_state_ExpectAndReturn(MC_STALLED); /* Stall reported */
    mc_set_target_rotor_speed_Expect(0); /* Command speed of 0 RPM because of stall */
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_STALLED, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(1992, mmgr_Target_Speed);
    TEST_ASSERT_EQUAL_UINT8(6, mmgr_Retry_Attempts);
    TEST_ASSERT_EQUAL(MMGR_STALLED_RETRY_FAILED, mmgr_Stalled_Retry_State);

}

/**
 * @test Verifies that Motor Manager while in the STALLED->RETRY_FAILED state will stay in that state
 * until the motor control reports IDLE.
 */
/* Reqs: SWREQ_1382 */
void test_Motor_Manager_STALLED_RETRY_FAILED_stays_until_motor_control_IDLE(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_STALLED;
    mmgr_Target_Speed = 1992;
    mmgr_Stalled_Retry_State = MMGR_STALLED_RETRY_FAILED;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_RUN);
    mc_get_motor_state_ExpectAndReturn(MC_STALLED); /* Stall reported */
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_STALLED, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(1992, mmgr_Target_Speed);
    TEST_ASSERT_EQUAL(MMGR_STALLED_RETRY_FAILED, mmgr_Stalled_Retry_State);

}

/**
 * @test Verifies that Motor Manager while in the STALLED->RETRY_FAILED state will transition to the
 * STALLED->RETRY->FORWARD state once the motor control module reports IDLE.
 */
/* Reqs: SWREQ_1382 */
void test_Motor_Manager_STALLED_RETRY_FAILED_transitions_to_STALLED_RETRY_FORWARD_when_motor_control_IDLE(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_STALLED;
    mmgr_Target_Speed = 1992;
    mmgr_Stalled_Retry_State = MMGR_STALLED_RETRY_FAILED;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_RUN);
    mc_get_motor_state_ExpectAndReturn(MC_IDLE); /* Motor control IDLE reported */
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_STALLED, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(1992, mmgr_Target_Speed);
    TEST_ASSERT_EQUAL(MMGR_STALLED_RETRY_FORWARD, mmgr_Stalled_Retry_State);

}

/**
 * @test Verifies that Motor Manager while in the STALLED_RETRY_FORWARD will transition to the
 * STALLED_RETRY_REVERSE state and reset the commanded speed to 0 when the motor control module reports
 * and has made the maximum number of Retry Attempts.
 */
/* Reqs: SWREQ_1382, SWREQ_1929 */
void test_Motor_Manager_STALLED_resets_speed_to_0_when_new_Stall_detected_10_previous_attempts(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_STALLED;
    mmgr_Target_Speed = 1992;
    mmgr_Retry_Attempts = 10; /* less than maximum attempts */
    mmgr_Stalled_Retry_State = MMGR_STALLED_RETRY_FORWARD;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_RUN);
    mc_get_comm_mode_ExpectAndReturn(MC_OPEN_LOOP);
    mc_get_motor_state_ExpectAndReturn(MC_STALLED); /* Stall reported */
    mc_set_target_rotor_speed_Expect(0); /* Command speed of 0 RPM because of stall */
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(5100); /* Value is arbitrary. Must be tracked in basetime */
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_STALLED, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(1992, mmgr_Target_Speed);
    TEST_ASSERT_EQUAL_UINT8(11, mmgr_Retry_Attempts);
    TEST_ASSERT_EQUAL_UINT32(5100, mmgr_Stalled_Reverse_Basetime);
    TEST_ASSERT_EQUAL(MMGR_STALLED_RETRY_REVERSE, mmgr_Stalled_Retry_State);

}

/**
 * @test Verifies that Motor Manager while in the STALLED state will transition to
 * the SELF_PROTECT state when the initial stall was during a RUN state and it has completed the
 * full retry strategy
 */
/* Reqs: SWREQ_1929 */
void test_Motor_Manager_STALLED_transitions_to_SELF_PROTECT_after_failed_retry_strategy_entered_from_RUNNING(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_STALLED;
    mmgr_Target_Speed = 1992;
    mmgr_Retry_Attempts = 11; /* Maximum retries */
    mmgr_Retry_Reason = MMGR_RUNNING_RETRY;
    mmgr_Stalled_Retry_State = MMGR_STALLED_RETRY_REVERSE;
    mmgr_Stalled_Reverse_Basetime = 5000; /* Number is arbitrary. Used to track expected value to mock */
    mmgr_Stall_Temperature = 23; /* ~ room temperature */
    mmgr_Blockage_Detected = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_RUN);
    TMUT_MS_To_Ticks_ExpectAndReturn(500, 724); /* Made up value for the number of ticks in 500 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(5000, 724, true); /* Reverse timer expires */
    mc_set_direction_Expect(FORWARD);
    mc_set_target_rotor_speed_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpBlk_B_Falt_Expect(true);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_BLOCKED_GEROTOR);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_SELF_PROTECT, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(1992, mmgr_Target_Speed); /* Target speed should be retained. */
    TEST_ASSERT_EQUAL_INT16(-50, mmgr_Stall_Temperature); /* Stall temperature must be recorded as the reported oil temperature */
    TEST_ASSERT_TRUE(mmgr_Blockage_Detected);
}

/**
 * @test Verifies that Motor Manager while in the STALLED state will transition to
 * the SELF_PROTECT state when entered from the SELF_PROTECT state and it has completed the
 * full retry strategy
 */
/* Reqs: SWREQ_1929 */
void test_Motor_Manager_STALLED_transitions_to_SELF_PROTECT_after_failed_retry_strategy_entered_from_SELF_PROTECT(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_STALLED;
    mmgr_Target_Speed = 1992;
    mmgr_Retry_Attempts = 11; /* Maximum retries */
    mmgr_Retry_Reason = MMGR_AUTO_RETRY;
    mmgr_Stalled_Retry_State = MMGR_STALLED_RETRY_REVERSE;
    mmgr_Stalled_Reverse_Basetime = 5000; /* Number is arbitrary. Used to track expected value to mock */
    mmgr_Stall_Temperature = 23; /* ~ room temperature */
    mmgr_Blockage_Detected = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_RUN);
    TMUT_MS_To_Ticks_ExpectAndReturn(500, 724); /* Made up value for the number of ticks in 500 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(5000, 724, true); /* Reverse timer expires */
    mc_set_direction_Expect(FORWARD);
    mc_set_target_rotor_speed_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpBlk_B_Falt_Expect(true);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_BLOCKED_GEROTOR);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_SELF_PROTECT, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(1992, mmgr_Target_Speed); /* Target speed should be retained. */
    TEST_ASSERT_EQUAL_INT16(-50, mmgr_Stall_Temperature); /* Stall temperature must be recorded as the reported oil temperature */
    TEST_ASSERT_TRUE(mmgr_Blockage_Detected);
}

/**
 * @test Verifies that Motor Manager while in the STALLED state will transition to
 * the SELF_PROTECT state when the initial stall was during a STARTUP state and it has completed the
 * full retry strategy twice
 */
/* Reqs: SWREQ_1382 */
void test_Motor_Manager_STALLED_transitions_to_SELF_PROTECT_after_failed_retry_strategy_entered_from_STARTUP(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_STALLED;
    mmgr_Target_Speed = 1992;
    mmgr_Retry_Attempts = 11; /* Maximum retries */
    mmgr_Retry_Reason = MMGR_STARTUP_RETRY;
    mmgr_Startup_Attempts = 2; /* Maximum number of startup attempts */
    mmgr_Stalled_Retry_State = MMGR_STALLED_RETRY_REVERSE;
    mmgr_Stalled_Reverse_Basetime = 5000; /* Number is arbitrary. Used to track expected value to mock */
    mmgr_Stall_Temperature = 23; /* ~ room temperature */
    mmgr_Blockage_Detected = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1992);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_RUN);
    TMUT_MS_To_Ticks_ExpectAndReturn(500, 724); /* Made up value for the number of ticks in 500 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(5000, 724, true); /* Reverse timer expires */
    mc_set_direction_Expect(FORWARD);
    mc_set_target_rotor_speed_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpBlk_B_Falt_Expect(true);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_BLOCKED_GEROTOR);
     VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_SELF_PROTECT, mmgr_State);
    TEST_ASSERT_EQUAL_UINT16(1992, mmgr_Target_Speed); /* Target speed should be retained. */
    TEST_ASSERT_EQUAL_INT16(-50, mmgr_Stall_Temperature); /* Stall temperature must be recorded as the reported oil temperature */
    TEST_ASSERT_TRUE(mmgr_Blockage_Detected);
}
