/**
 *  @file test_mmgr_Limit_Target_speed.c
 *
 */
#include "unity.h"
#include "motor_manager_imp_defines.h"
#include "motor_manager_imp_types.h"

/* MOCKS */
#include "mock_lin_vehicle_comm.h"
#include "mock_fault_logger.h"
#include "mock_fault_manager.h"
#include "mock_motor_ctrl.h"
#include "mock_serial_data_buffer.h"
#include "mock_speed_meas.h"
#include "mock_system_signals.h"
#include "mock_time_utils.h"

/* STATIC's */
VCOMM_RPM_T mmgr_Target_Speed;
VCOMM_RPM_T mmgr_Max_Speed;
bool_t mmgr_OC_Speed_Change_Pending;
Time_MS_T mmgr_Overcurrent_Basetime;
bool_t mmgr_In_Current_Limit_Mode;
mmgr_State_T mmgr_State;
bool_t mmgr_Blockage_Detected;

void mmgr_Limit_Target_Speed(VCOMM_RPM_T Commanded_Speed);

void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * @test Verifies that mmgr_Limit_Target_Speed will not limit the the target speed to
 * MMGR_MIN_SPEED when the commanded speed is 0.
 *
 */
/* Reqs: SWREQ_2070 */
void test_mmgr_Limit_Target_Speed_does_not_limit_target_speed_when_commanded_speed_0(void)
{
    /* Ensure known test state */
    mmgr_In_Current_Limit_Mode = false;
    mmgr_Target_Speed = 0;
    mmgr_Max_Speed = MAX_RPM;

    /* Setup expected call chain */

    /* Call function under test */
    mmgr_Limit_Target_Speed(0); /* Received commanded speed is 0 RPM */

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(0, mmgr_Target_Speed);

}

/**
 * @test Verifies that mmgr_Limit_Target_Speed will detect an overcurrent condition and start
 * timing it. Target speed should not be limited.
 */
/* Reqs: SWREQ-2079 */
void test_mmgr_Limit_Target_Speed_detects_overcurrent_and_begins_timing_no_limiting_of_target_speed(void)
{
    /* Ensure known test state */
    mmgr_In_Current_Limit_Mode = false;
    mmgr_Target_Speed = 3000;
    mmgr_Max_Speed = MAX_RPM;
    mmgr_OC_Speed_Change_Pending = false; /* Overcurrent has not been detected previously */

    /* Setup expected call chain */
    SYSIG_Get_Current_ExpectAndReturn(151); /* 15.1 Amps reported in 10ths of an Amp */
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(5000); /* Number is arbitrary, just used to track setting of basetime */

    /* Call function under test */
    mmgr_Limit_Target_Speed(3000); /* Received commanded speed is 3000 RPM */

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(3000, mmgr_Target_Speed);
    TEST_ASSERT_TRUE(mmgr_OC_Speed_Change_Pending); /* Should mark that an overcurrent has been detected and is being tracked */
    TEST_ASSERT_EQUAL_INT(5000, mmgr_Overcurrent_Basetime);
    TEST_ASSERT_FALSE(mmgr_In_Current_Limit_Mode);
}

/**
 * @test Verifies that mmgr_Limit_Target_Speed will detect that the current has dropped below 15A after tracking
 * began and clear all tracking of that overcurrent to allow it to be detected again.
 */
/* Reqs: SWREQ-2079 */
void test_mmgr_Limit_Target_Speed_detects_normal_after_overcurrent_Resets_tracking(void)
{
    /* Ensure known test state */
    mmgr_Target_Speed = 3000;
    mmgr_Max_Speed = MAX_RPM;
    mmgr_OC_Speed_Change_Pending = true; /* Overcurrent has been detected previously */
    mmgr_In_Current_Limit_Mode = false;  /* Not over current, has not been over current previously */

    /* Setup expected call chain */
    SYSIG_Get_Current_ExpectAndReturn(130); /* 15.0 Amps reported in 10ths of an Amp */
    TMUT_MS_To_Ticks_ExpectAndReturn(1000, 5248); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(mmgr_Overcurrent_Basetime, 5248, false); /* Mock indicates that the time has NOT elapsed */

    /* Call function under test */
    mmgr_Limit_Target_Speed(3000); /* Received commanded speed is 3000 RPM */

    /* Verify test results */
    TEST_ASSERT_TRUE(mmgr_OC_Speed_Change_Pending); /*  System should allow the detection to occur again */
    TEST_ASSERT_EQUAL_INT(3000, mmgr_Target_Speed);
    TEST_ASSERT_FALSE(mmgr_In_Current_Limit_Mode);

}

/**
 * @test Verifies that mmgr_Limit_Target_Speed will reduce the target speed by 295 when
 * the current is above 15A for more than 200 ms and the commanded speed is 3000 RPM with the
 * current target speed unreduced. The reduction factor is calculated according to SWREQ_2079
 * thusly:
 *
 * (3000 - 50) * 10% = 295
 */
/* Reqs: SWREQ-2079 */
void test_mmgr_Limit_Target_Speed_reduces_target_speed_from_commanded_when_above_15A_for_longer_than_200ms(void)
{
    /* Ensure known test state */
    mmgr_Target_Speed = 3000;
    mmgr_Max_Speed = MAX_RPM;
    mmgr_Overcurrent_Basetime = 5000; /* Value is arbitrary, just detecting that the right value is being passed to mock */
    mmgr_OC_Speed_Change_Pending = true; /* Overcurrent has been detected previously */
    mmgr_In_Current_Limit_Mode = false;  /* Start out not limiting speed to reduce current */

    /* Setup expected call chain */
    SYSIG_Get_Current_ExpectAndReturn(151); /* 15.1 Amps reported in 10ths of an Amp */
    TMUT_MS_To_Ticks_ExpectAndReturn(1000, 5248); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(mmgr_Overcurrent_Basetime, 5248, true); /* Mock indicates that the time has elapsed */

    /* Call function under test */
    mmgr_Limit_Target_Speed(3000); /* Received commanded speed is 3000 RPM */

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(2705, mmgr_Target_Speed);
    TEST_ASSERT_FALSE(mmgr_OC_Speed_Change_Pending); /* System should allow the detection to occur again */
    TEST_ASSERT_TRUE(mmgr_In_Current_Limit_Mode); /* Still in the limit mode, have not yet acheived command speed with no overcurrents */
}

/**
 * @test Verifies that mmgr_Limit_Target_Speed will reduce the target speed by 295 when
 * the current is above 15A for more than 200 ms and the commanded speed is 3000 RPM with the target
 * speed already reduced to 2705. The reduction factor is calculated according to SWREQ_2079 thusly:
 *
 * (3000 - 50) * 10% = 295
 */
/* Reqs: SWREQ-2079 */
void test_mmgr_Limit_Target_Speed_reduces_target_speed_further_when_above_15A_for_longer_than_200ms(void)
{
    /* Ensure known test state */
    mmgr_Target_Speed = 2705;
    mmgr_Max_Speed = MAX_RPM;
    mmgr_Overcurrent_Basetime = 5000; /* Value is arbitrary, just detecting that the right value is being passed to mock */
    mmgr_OC_Speed_Change_Pending = true; /* Overcurrent has been detected previously */
    mmgr_In_Current_Limit_Mode = true;  /* Start out  in speed limiting mode due to over current */

    /* Setup expected call chain */
    SYSIG_Get_Current_ExpectAndReturn(151); /* 15.1 Amps reported in 10ths of an Amp */
    TMUT_MS_To_Ticks_ExpectAndReturn(1000, 5248); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(mmgr_Overcurrent_Basetime, 5248, true); /* Mock indicates that the time has elapsed */

    /* Call function under test */
    mmgr_Limit_Target_Speed(3000); /* Received commanded speed is 3000 RPM */

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(2410, mmgr_Target_Speed);
    TEST_ASSERT_FALSE(mmgr_OC_Speed_Change_Pending); /* System should allow the detection to occur again */
    TEST_ASSERT_TRUE(mmgr_In_Current_Limit_Mode);
}

/**
 * @test Verifies that mmgr_Limit_Target_Speed will reduce the target speed by 295 when
 * the current is above 15A for more than 200 ms and the commanded speed is 3000 RPM with the target
 * speed already reduced to 2705. The reduction factor is calculated according to SWREQ_2079 thusly:
 *
 * (3000 - 50) * 10% = 295
 */
/* Reqs: SWREQ-2079 */
void test_mmgr_Limit_Target_Speed_reduces_target_speed_repeatedly_when_above_15A_for_longer_than_200ms(void)
{
    int i = 0;

    /* Ensure known test state */
    mmgr_Target_Speed = 4200;
    mmgr_Max_Speed = MAX_RPM;
    mmgr_Overcurrent_Basetime = 5000; /* Value is arbitrary, just detecting that the right value is being passed to mock */

    for (i = 1; i <= 10; i++)
    {
        mmgr_OC_Speed_Change_Pending = true; /* Overcurrent has been detected previously */
        mmgr_In_Current_Limit_Mode = true;  /* Start out  in speed limiting mode due to over current */

        /* Setup expected call chain */
        SYSIG_Get_Current_ExpectAndReturn(151); /* 15.1 Amps reported in 10ths of an Amp */
        TMUT_MS_To_Ticks_ExpectAndReturn(1000, 5248); /* Made up value for the number of ticks in 1000 ms for test */
        TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(mmgr_Overcurrent_Basetime, 5248, true); /* Mock indicates that the time has elapsed */

        /* Call function under test */
        mmgr_Limit_Target_Speed(4200); /* Received commanded speed is 4200 RPM */

        /* Verify test results */
        TEST_ASSERT_EQUAL_INT(4200 - (415 * i), mmgr_Target_Speed);
        TEST_ASSERT_FALSE(mmgr_OC_Speed_Change_Pending); /* System should allow the detection to occur again */
        TEST_ASSERT_TRUE(mmgr_In_Current_Limit_Mode);
    }
}

/**
 * @test Verifies that mmgr_Limit_Target_Speed will reduce the target speed below MMGR_MIN_SPEED
 * if the reported current is still above 15A
 */
/* Reqs: SWREQ-2079 */
void test_mmgr_Limit_Target_Speed_does_not_reduce_target_speed_below_MMGR_MIN_SPEED(void)
{
    /* Ensure known test state */
    mmgr_Target_Speed = MMGR_MIN_COMMANDABLE_SPEED + 1;
    mmgr_Max_Speed = MAX_RPM;
    mmgr_Overcurrent_Basetime = 5000; /* Value is arbitrary, just detecting that the right value is being passed to mock */
    mmgr_OC_Speed_Change_Pending = true; /* Overcurrent has been detected previously */
    mmgr_In_Current_Limit_Mode = true;   /* Start out in speed limiting mode due to over current */

    /* Setup expected call chain */
    SYSIG_Get_Current_ExpectAndReturn(151); /* 15.1 Amps reported in 10ths of an Amp */
    TMUT_MS_To_Ticks_ExpectAndReturn(1000, 5248); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(mmgr_Overcurrent_Basetime, 5248, true); /* Mock indicates that the time has elapsed */
    VCOMM_Signal_Set_TrnOilPumpBlk_B_Falt_Expect(true);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_BLOCKED_GEROTOR);
 
    /* Call function under test */
    mmgr_Limit_Target_Speed(3000); /* Received commanded speed is 3000 RPM */

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(0, mmgr_Target_Speed);
    TEST_ASSERT_FALSE(mmgr_OC_Speed_Change_Pending); /* System should allow the detection to occur again */
    TEST_ASSERT_TRUE(mmgr_In_Current_Limit_Mode); /* Still in the mode, current is over limit */
}

/**
 * Verifies that mmgr_Limit_Target_Speed, while in the RUNNING state, will stop the motor
 * and report a blockage and self protect when no more speed reductions can be performed without stopping
 * the motor. This will transition to SELF_PROTECT
 */
/* Reqs:  SWREQ_2079 */
void test_mmgr_Limit_Target_Speed_enters_SELF_PROTECT_when_no_further_speed_reductions_can_be_performed(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_RUNNING;
    mmgr_Target_Speed = MMGR_MIN_COMMANDABLE_SPEED;
    mmgr_Max_Speed = MAX_RPM;
    mmgr_Overcurrent_Basetime = 5000; /* Value is arbitrary, just detecting that the right value is being passed to mock */
    mmgr_OC_Speed_Change_Pending = true; /* Overcurrent has been detected previously */
    mmgr_In_Current_Limit_Mode = true;   /* Start out in speed limiting mode due to over current */

    /* Setup expected call chain */
    SYSIG_Get_Current_ExpectAndReturn(151); /* 15.1 Amps reported in 10ths of an Amp */
    TMUT_MS_To_Ticks_ExpectAndReturn(1000, 5248); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(mmgr_Overcurrent_Basetime, 5248, true); /* Mock indicates that the time has elapsed */
    VCOMM_Signal_Set_TrnOilPumpBlk_B_Falt_Expect(true);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_BLOCKED_GEROTOR);

    /* Call function under test */
    mmgr_Limit_Target_Speed(4200); /* Received commanded speed is 4200 RPM */

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(0, mmgr_Target_Speed);
    TEST_ASSERT_FALSE(mmgr_OC_Speed_Change_Pending); /* System should allow the detection to occur again */
    TEST_ASSERT_TRUE(mmgr_In_Current_Limit_Mode); /* Still in the mode, current is over limit */
    TEST_ASSERT_TRUE(mmgr_Blockage_Detected);
    TEST_ASSERT_EQUAL(MMGR_SELF_PROTECT, mmgr_State);
}

/**
 * Verifies that mmgr_Limit_Target_Speed, while in the RUNNING state, will increase the target speed by 295 when
 * the current is below 15A for more than 200 ms and the commanded speed is 3000 RPM with the target
 * speed already reduced to 2410. After the increase, the new target still remains below the commanded
 * speed, so it should check again in 200 ms.
 *
 * The increase factor is calculated according to SWREQ_2080 thusly:
 *
 * (3000 - 50) * 10% = 295
 */
/* Reqs: SWREQ-2080 */
void test_mmgr_Limit_Target_Speed_increases_target_speed_when_below_15A_for_longer_than_200ms_continues_checking(void)
{

    /* Ensure known test state */
    mmgr_State = MMGR_RUNNING;
    mmgr_Target_Speed = 2410; /* Target speed is less than command, so a reduction has occured */
    mmgr_Max_Speed = MAX_RPM;
    mmgr_Overcurrent_Basetime = 5000; /* Value is arbitrary, just detecting that the right value is being passed to mock */
    mmgr_OC_Speed_Change_Pending = true; /* Overcurrent has been detected previously */
    mmgr_In_Current_Limit_Mode = true; /* Start out in speed limiting mode due to over current */

    /* Setup expected call chain */
    SYSIG_Get_Current_ExpectAndReturn(149); /* 14.9 Amps reported in 10ths of an Amp */
    TMUT_MS_To_Ticks_ExpectAndReturn(1000, 5248); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(mmgr_Overcurrent_Basetime, 5248, true); /* Mock indicates that the time has elapsed */

    /* Call function under test */
    mmgr_Limit_Target_Speed(3000); /* Received commanded speed is 3000 RPM */

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(2705, mmgr_Target_Speed); /* Target Speed increased to Commanded speed */
    TEST_ASSERT_FALSE(mmgr_OC_Speed_Change_Pending); /* System should allow the detection to occur again */
    TEST_ASSERT_TRUE(mmgr_In_Current_Limit_Mode); /* Still in limiting mode, commanded speed has not yet been reached  */
}

/**
 * Verifies that mmgr_Limit_Target_Speed, while in the RUNNING state, will increase the target speed by 295 when
 * the current is below 15A for more than 200 ms and the commanded speed is 3000 RPM with the target
 * speed already reduced to 2705. After the increase, the new target will meet the commanded speed
 * so it should stop checking for increases. It also set the boolean to indicate we are in this mode from true to false
 * because the speed has increased to 
 *
 * The increase factor is calculated according to SWREQ_2080 thusly:
 *
 * (3000 - 50) * 10% = 295
 */
/* Reqs: SWREQ-2080 */
void test_mmgr_Limit_Target_Speed_increases_target_speed_when_below_15A_for_longer_than_200ms_stops_checking(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_RUNNING;
    mmgr_Target_Speed = 2705; /* Target speed is less than command, so a reduction has occured */
    mmgr_Max_Speed = MAX_RPM;
    mmgr_Overcurrent_Basetime = 5000; /* Value is arbitrary, just detecting that the right value is being passed to mock */
    mmgr_OC_Speed_Change_Pending = true; /* Overcurrent has been detected previously */
    mmgr_In_Current_Limit_Mode = true;  /* Start out in speed limiting mode due to over current */

    /* Setup expected call chain */
    SYSIG_Get_Current_ExpectAndReturn(149); /* 14.9 Amps reported in 10ths of an Amp */
    TMUT_MS_To_Ticks_ExpectAndReturn(1000, 5248); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(mmgr_Overcurrent_Basetime, 5248, true); /* Mock indicates that the time has elapsed */
    /* Call function under test */
    mmgr_Limit_Target_Speed(3000); /* Received commanded speed is 3000 RPM */

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(3000, mmgr_Target_Speed); /* Target Speed increased to Commanded speed */
    TEST_ASSERT_FALSE(mmgr_OC_Speed_Change_Pending); /* System should allow the detection to occur again */
    TEST_ASSERT_FALSE(mmgr_In_Current_Limit_Mode); /* No longer in speed limiting mode due to over current */
}
