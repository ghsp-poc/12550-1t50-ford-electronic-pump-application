/**
 *  @file test_SELF_PROTECT_Recovery.c
 *
 */
#include "unity.h"
#include "global.h"
#include "motor_manager.h"
#include "motor_manager_imp_defines.h"
#include "motor_manager_imp_types.h"

/* MOCKS */
#include "mock_lin_vehicle_comm.h"
#include "mock_fault_logger.h"
#include "mock_fault_manager.h"
#include "mock_motor_ctrl.h"
#include "mock_serial_data_buffer.h"
#include "mock_speed_meas.h"
#include "mock_system_signals.h"
#include "mock_time_utils.h"

/* STATIC's */
mmgr_State_T mmgr_State;
uint16_t mmgr_Target_Speed;
bool_t mmgr_Blockage_Detected;

void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * @test Verifies that Motor Manager will transition from the SELF_PROTECT state
 * to the IDLE state when all self protects faults have cleared and no blockage is detected,
 * commanded speed = 1000.
 */
/* Reqs: SWREQ_1186, SWREQ_2052, SWREQ_2055 */
void test_Motor_Manager_SELF_PROTECT_to_IDLE_When_self_protect_faults_clear_no_blockage_speed_1000(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_SELF_PROTECT;
    mmgr_Target_Speed = 0;
    mmgr_Blockage_Detected = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1000);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpBlk_B_Falt_Expect(false);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_BLOCKED_GEROTOR);
    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(false);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_IDLE, mmgr_State);
    TEST_ASSERT_FALSE(mmgr_Blockage_Detected);
}

/**
 * @test Verifies that Motor Manager will transition from the SELF_PROTECT state
 * to the IDLE state when all self protects faults have cleared and no blockage is detected,
 * commanded speed = 0.
 */
/* Reqs: SWREQ_1186, SWREQ_2052, SWREQ_2055 */
void test_Motor_Manager_SELF_PROTECT_to_IDLE_When_self_protect_faults_clear_no_blockage_speed_0(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_SELF_PROTECT;
    mmgr_Target_Speed = 0;
    mmgr_Blockage_Detected = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(0);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_IDLE);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpBlk_B_Falt_Expect(false);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_BLOCKED_GEROTOR);
    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(false);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_IDLE, mmgr_State);
    TEST_ASSERT_FALSE(mmgr_Blockage_Detected);
}

/**
 * @test Verifies that Motor Manager will stay in the SELF_PROTECT state
 * when the fault manager does not report a self protect fault but a blockage
 * is present and the commanded speed is not zero
 */
/* Reqs: SWREQ_2050 */
void test_Motor_Manager_stays_in_SELF_PROTECT_with_blockage_commanded_speed_not_0_no_self_protect_fault(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_SELF_PROTECT;
    mmgr_Target_Speed = 0;
    mmgr_Blockage_Detected = true;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1000);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    mc_set_target_rotor_speed_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(true);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_DENIED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_SELF_PROTECT, mmgr_State);
}

/**
 * @test Verifies that Motor Manager will transition from the SELF_PROTECT state
 * to the IDLE state when there are no self protect faults active but there was a
 * blockage detected and the commanded speed is 0.
 */
/* Reqs: SWREQ_2050 */
void test_Motor_Manager_SELF_PROTECT_to_IDLE_When_no_self_protect_after_blockage_commanded_speed_0(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_SELF_PROTECT;
    mmgr_Target_Speed = 0;
    mmgr_Blockage_Detected = true;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(0);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_IDLE);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpBlk_B_Falt_Expect(false);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_BLOCKED_GEROTOR);
     VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(false);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_HONORED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_IDLE, mmgr_State);
    TEST_ASSERT_FALSE(mmgr_Blockage_Detected);
}

/**
 * Verifies that Motor Manager stay in SELF_PROTECT state after a blockage has been detected until
 * a valid speed command of 0 is received.
 */
/* Reqs: SWREQ_2050 */
void test_Motor_Manager_stays_in_SELF_PROTECT_until_valid_commanded_speed_of_0_after_blockage(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_SELF_PROTECT;
    mmgr_Target_Speed = 0;
    mmgr_Blockage_Detected = true;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1000);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    mc_set_target_rotor_speed_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(true);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_DENIED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_SELF_PROTECT, mmgr_State);
    TEST_ASSERT_TRUE(mmgr_Blockage_Detected);
}

/**
 * Verifies that Motor Manager will stay in the SELF_PROTECT state
 * when the fault manager still reports a self protect fault with no blockage
 * commanded speed is non zero
 */
/* Reqs: SWREQ_1186, SWREQ_2052, SWREQ_2055 */
void test_Motor_Manager_stays_in_SELF_PROTECT_when_self_protect_fault_still_active_no_blockage_speed_not_0(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_SELF_PROTECT;
    mmgr_Target_Speed = 0;
    mmgr_Blockage_Detected = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1000);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(true);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    mc_set_target_rotor_speed_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(true);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_DENIED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_SELF_PROTECT, mmgr_State);
}

/**
 * @test Verifies that Motor Manager will stay in the SELF_PROTECT state
 * when the fault manager still reports a self protect fault with no blockage
 * commanded speed is zero
 */
/* Reqs: SWREQ_1186, SWREQ_2052, SWREQ_2055 */
void test_Motor_Manager_stays_in_SELF_PROTECT_when_self_protect_fault_still_active_no_blockage_speed_0(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_SELF_PROTECT;
    mmgr_Target_Speed = 0;
    mmgr_Blockage_Detected = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(0);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_IDLE);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(true);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    mc_set_target_rotor_speed_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(true);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_DENIED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_SELF_PROTECT, mmgr_State);
}

/**
 * @test Verifies that Motor Manager will stay in the SELF_PROTECT state
 * when the fault manager still reports a self protect fault with a blockage
 * commanded speed is non zero
 */
/* Reqs: SWREQ_1186, SWREQ_2052, SWREQ_2055 */
void test_Motor_Manager_stays_in_SELF_PROTECT_when_self_protect_fault_still_active_blockage_speed_not_0(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_SELF_PROTECT;
    mmgr_Target_Speed = 0;
    mmgr_Blockage_Detected = true;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1000);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(true);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    mc_set_target_rotor_speed_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(true);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_DENIED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_SELF_PROTECT, mmgr_State);
}

/**
 * @test Verifies that Motor Manager will stay in the SELF_PROTECT state
 * when the fault manager still reports a self protect fault with a blockage
 * commanded speed is non zero
 */
/* Reqs: SWREQ_1186, SWREQ_2052, SWREQ_2055 */
void test_Motor_Manager_stays_in_SELF_PROTECT_when_self_protect_fault_still_active_blockage_speed_0(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_SELF_PROTECT;
    mmgr_Target_Speed = 0;
    mmgr_Blockage_Detected = true;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(0);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_IDLE);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(true);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    mc_set_target_rotor_speed_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt_Expect(true);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_DENIED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_SELF_PROTECT, mmgr_State);
}

/**
 * @test Verifies that Motor Manager will stay in the SELF_PROTECT state
 * when there is no self protect fault present and no blockage detected,
 * but the TAOP_ECM_DEMAND message is requesting a speed of 0 and a Mode
 * of RUN. The DEMAND message will be DENIED.
 */
/* Reqs: SWREQ_1929 */
void test_Motor_Manager_stays_in_SELF_PROTECT_when_all_clear_V_Pc_Rq_0_Mode_RUN(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_SELF_PROTECT;
    mmgr_Target_Speed = 0;
    mmgr_Blockage_Detected = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(0);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_RUN);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    mc_set_target_rotor_speed_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_DENIED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_SELF_PROTECT, mmgr_State);
}

/**
 * @test Verifies that Motor Manager will stay in the SELF_PROTECT state
 * when there is no self protect fault present and no blockage detected,
 * but the TAOP_ECM_DEMAND message is requesting a speed of 1000 and a Mode
 * of IDLE. The DEMAND message will be DENIED.
 */
/* Reqs: SWREQ_1929 */
void test_Motor_Manager_stays_in_SELF_PROTECT_when_all_clear_V_Pc_Rq_1000_Mode_IDLE(void)
{
    /* Ensure known test state */
    mmgr_State = MMGR_SELF_PROTECT;
    mmgr_Target_Speed = 0;
    mmgr_Blockage_Detected = false;

    /* Setup expected call chain */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1000);
    VCOMM_Signal_Get_TrnOilPumpMde_D_Rq_ExpectAndReturn(VCOMM_MODE_IDLE);
    SYSIG_Get_CPU_Temp_ExpectAndReturn(50);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_MASTER_REQUEST_MISSING);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_LIN_MASTER_REQUEST_DENIED);
    VCOMM_Signal_Get_TrnSumpOil_Te_Actl_ExpectAndReturn(-50); /* -50 degrees C */
    mc_set_speed_ctrl_output_max_Expect(1400); /* SWREQ_2086 */
    FLT_MGR_Is_In_Self_Protect_ExpectAndReturn(false);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    VCOMM_Signal_Set_TrnOilPumpMde_D_Stat_Expect(VCOMM_MODE_IDLE);
    mc_set_target_rotor_speed_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_Expect(0);
    VCOMM_Signal_Set_TrnOilPumpV_D_Stat_Expect(VCOMM_REQUEST_DENIED);

    /* Call function under test */
    MMGR_Task();

    /* Verify test results */
    TEST_ASSERT_EQUAL(MMGR_SELF_PROTECT, mmgr_State);
}
