/**
 *  @file test_Ramp_Commanded_Speed.c
 *
 */
#include "unity.h"
#include "global.h"
#include "ford_1t50_application.h"
#include "fault_manager.h"
#include "speed_meas.h"
#include "motor_manager.h"
#include "motor_manager_imp_defines.h"



#include <string.h>

/* MOCKS */
#include "mock_fault_logger.h"
#include "mock_motor_ctrl.h"
#include "mock_fault_manager.h"
#include "mock_lin_vehicle_comm.h"
#include "mock_system_signals.h"
#include "mock_time_utils.h"
#include "mock_serial_data_buffer.h"
#include "mock_speed_meas.h"


/* STATIC's */
VCOMM_RPM_T mmgr_Commanded_Speed;


uint16_t mmgr_Speed_Ramp_Override_Step;

void mmgr_Ramp_Commanded_Speed(VCOMM_RPM_T Commanded_Speed);

void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * Verifies the default ramp rate is 2500 
 */
 /*  Reqs: SWREQ_2118 */

void test_default_ramp_rate_matches_requirements(void)
{
    TEST_ASSERT_EQUAL_UINT16(2500, MMGR_SPEED_RAMP_RPMS_PER_SEC);
}


/**
 * Verifies the ramp rate at least 50, to avoid zeror step size due to rounding 
 */

void test_default_ramp_rate_at_least_50_to_avoid_zero_ramp_step(void)
{
    TEST_ASSERT_TRUE(MMGR_SPEED_RAMP_RPMS_PER_SEC >= 50);
}
/**
 * Verifies that the function will allow the Commanded speed to INCREASE by 27
 * when 11 ms has elapsed to maintain a near rate of 2500 RPM per second to achieve the
 * commanded speed of 4200.
 *
 */
void test_mmgr_Ramp_Commanded_Speed_Increases_mmgr_Target_Speed_by_27_after_11ms_elapsed_with_Ramp_Rate_of_2500(void)
{
    /* Ensure known test state */
    mmgr_Commanded_Speed = 1500;


    mmgr_Speed_Ramp_Override_Step = MMGR_SPEED_RAMP_MAX_STEP_SIZE; /* based on 2500 RPM per second and 10ms MMGR task rate */

    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);

    /* Call function under test */
    mmgr_Ramp_Commanded_Speed(4200);
    
    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT32(1525, mmgr_Commanded_Speed);
}

/**
 * Verifies that the function will allow the Commanded speed to DECREASE by 27
 * when 11 ms has elapsed to maintain a near rate of 2500 RPM per second to achieve the
 * commanded speed of 500.
 *
 */
void test_mmgr_Ramp_Commanded_Speed_Decreases_mmgr_Target_Speed_by_27_after_11ms_elapsed_with_Ramp_Rate_of_2500(void)
{
    /* Ensure known test state */
    mmgr_Commanded_Speed = 1500;
    mmgr_Speed_Ramp_Override_Step = MMGR_SPEED_RAMP_MAX_STEP_SIZE; /* based on 2500 RPM per second and 10ms MMGR task rate */

    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);

    /* Call function under test */
    mmgr_Ramp_Commanded_Speed(500);

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT32(1475, mmgr_Commanded_Speed);
}

/**
 * Verifies that the function will allow the Commanded speed to INCREASE by 10
 * 
 *
 */
void test_mmgr_Ramp_Commanded_Speed_Increases_mmgr_Target_Speed_by_15_after_15ms_elapsed_with_Ramp_Rate_of_1000(void)
{
    /* Ensure known test state */
    mmgr_Commanded_Speed = 1500;
    mmgr_Speed_Ramp_Override_Step = 10; /* based on 1000 RPM per second and 10ms MMGR task rate */

    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);

    /* Call function under test */
    mmgr_Ramp_Commanded_Speed(3000);

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT32(1510, mmgr_Commanded_Speed);
}

/**
 * Verifies that the function will allow the Commanded speed to DECREASE by 10
 * when 10 ms has elapsed to maintain a near rate of 2500 RPM per second to achieve the
 * commanded speed of 500.
 *
 */
void test_mmgr_Ramp_Commanded_Speed_Decreases_mmgr_Target_Speed_by_15_after_15ms_elapsed_with_Ramp_Rate_of_1000(void)
{
    /* Ensure known test state */
    mmgr_Commanded_Speed = 1500;
    mmgr_Speed_Ramp_Override_Step = 10;  /* based on 1000 RPM per second and 10ms MMGR task rate */

    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);

    /* Call function under test */
    mmgr_Ramp_Commanded_Speed(500);

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT32(1490, mmgr_Commanded_Speed);
}

/**
 * Verifies that the function will allow the Commanded speed to INCREASE by 10
 * when 10 ms has elapsed to get to the commanded speed of 3000 when there is only 10 RPM remaining to
 * achieve the commanded speed instead of 25
 *
 */
void test_mmgr_Ramp_Commanded_Speed_Increases_mmgr_Target_Speed_by_10_after_10ms_elapsed_with_Ramp_Rate_of_2500(void)
{
    /* Ensure known test state */
    mmgr_Commanded_Speed = 2990;
    mmgr_Speed_Ramp_Override_Step = MMGR_SPEED_RAMP_MAX_STEP_SIZE; /* based on 2500 RPM per second and 10ms MMGR task rate */

    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);

    /* Call function under test */
    mmgr_Ramp_Commanded_Speed(3000);

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT32(3000, mmgr_Commanded_Speed);
}

/**
 * Verifies that the function will allow the Target speed to DECREASE by 10
 * when 10 ms has elapsed to get to the commanded speed of 1000 when there is only 10 RPM remaining to
 * achieve the commanded speed instead of 25
 *
 */
void test_mmgr_Ramp_Commanded_Speed_Decreases_mmgr_Target_Speed_by_10_after_10ms_elapsed_with_Ramp_Rate_of_2500(void)
{
    /* Ensure known test state */
    mmgr_Commanded_Speed = 1010;
    mmgr_Speed_Ramp_Override_Step = MMGR_SPEED_RAMP_MAX_STEP_SIZE; /* based on 2500 RPM per second and 10ms MMGR task rate */

    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);

    /* Call function under test */
    mmgr_Ramp_Commanded_Speed(1000);

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT32(1000, mmgr_Commanded_Speed);
}

/**
 * Verifies that the function will do nothing when the Commanded Speed is the same as
 * the commanded speed
 *
 */
void test_mmgr_Ramp_Commanded_Speed_Does_Nothing_when_Target_Speed_equals_Commanded_Speed(void)
{
    /* Ensure known test state */
    mmgr_Commanded_Speed = 3000;
    mmgr_Speed_Ramp_Override_Step = MMGR_SPEED_RAMP_MAX_STEP_SIZE; /* based on 2500 RPM per second and 10ms MMGR task rate */

    /* Setup expected call chain */

    /* Call function under test */
    mmgr_Ramp_Commanded_Speed(3000);

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT32(3000, mmgr_Commanded_Speed);
}

/**
 * Verifies that the function will do nothing when the Commanded Speed is the same as
 * the commanded speed
 *
 */
void test_mmgr_Ramp_Commanded_Speed_Does_Nothing_when_Motor_Control_OPEN_LOOP(void)
{
    /* Ensure known test state */
    mmgr_Commanded_Speed = 1000;
    mmgr_Speed_Ramp_Override_Step = MMGR_SPEED_RAMP_MAX_STEP_SIZE; /* based on 2500 RPM per second and 10ms MMGR task rate */

    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_OPEN_LOOP);

    /* Call function under test */
    mmgr_Ramp_Commanded_Speed(3000);

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT32(1000, mmgr_Commanded_Speed);
}


/**
 * Verifies that the default ramp rate will not cause a speeed fault by being too slow
 * Insert the set time in ms 
 *
 */
void test_mmgr_Ramp_Rate_Will_Not_Cause_Speed_Fault(void)
{
    const uint16_t set_time = 4000;  /*  over / under speed set time in ms */
    /* Ensure known test state */
    mmgr_Commanded_Speed = 1000;

    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);

    /* Call function under test */
    mmgr_Ramp_Commanded_Speed(3000);

    /* Verify test results */
    TEST_ASSERT_TRUE (((MAX_RPM * 1000) /  MMGR_SPEED_RAMP_RPMS_PER_SEC) < set_time);
}

/**
 * Verifies that the default ramp rate will  cause a speeed fault if Someone sets the speed fault set time to < 1.68 seconds.
 * Insert the set time in ms 
 *
 */
void test_mmgr_Ramp_Rate_Will_Cause_Speed_Fault_If_Set_Time_too_short(void)
{
    const uint16_t set_time = 1600;  /*  over / under speed set time in ms */
    /* Ensure known test state */
    mmgr_Commanded_Speed = 1000;

    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);

    /* Call function under test */
    mmgr_Ramp_Commanded_Speed(3000);

    /* Verify test results */
    TEST_ASSERT_FALSE (((MAX_RPM * 1000) /  MMGR_SPEED_RAMP_RPMS_PER_SEC) < set_time);
}