include $(GHSP_MAKE_DIR)/pre_module.mak

####################################################
# BEGIN: User configurable portion of the makefile #
####################################################

# Module only supports the production build
#ifeq ($(_build_),production)
# Module only supports the IAR RH850 Toolchain
#ifeq ($($(_build_)_TOOLCHAIN),IAR-RH850)

#
# Create a whitespace separated list of source directories for this module
# All source files in these directories will be compiled and (probably) linked 
# into any resulting binary.
#
# The variable _module_path_ is available for use in this definition. This variable
# contains the directory where this module Makefile is located.
#
# Example: _src_dirs_ := $(_module_path_)/project_module_a_imp/_src
# Where the _src directory contains all of the source files for the implementation of 
# the module.
#
_src_dirs_ := $(_module_path_)/lin_vehicle_comm_imp/src

#
# Setup the creation of a useful artifact of a module.
#
# A module must produce either a binary or a library, but not both.
# All source files in the _src_dirs_ will be used to create either of these
# artifacts.
#
# Note: If a library is specified, it will take precedence over any binary specified

#
# If a library will be produced, provide a name for the library.
# This library name should be related to the module name.
#
_library_name_ := lin_vehicle_comm

#
# If a binary will be produced, provide the name of the binary.
# This binary name should be related to the module name.
#
#_binary_name_ :=

#
# If producing a library, a set of libraries may be used as part
# of the binary being produced. List the names of the libraries here.
#
# Note: These libraries must be produced by the contents of the enclosing project
#
#_binary_library_names_ :=

#
# If producing a binary, a linker file may be specified
#
#_binary_linker_file_ :=
 
####################################################
# END: user configurable section of the makefile   #
####################################################

include $(GHSP_MAKE_DIR)/post_module.mak

#endif #ifeq ($($(_build_)_TOOLCHAIN),IAR-RH850)
#endif #ifeq ($(_build_),production)
