/**
 *  @file lin_vehicle_comm.c
 *
 *  @ref lin_vehicle_comm_api
 *
 *  @copyright 2018 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup lin_vehicle_comm_imp 1T50 LIN Vehicle Communications Implementation
 *  @{
 *
 *      System specific implementation of the @ref template_api
 *
 *      @page TRACE Traceability
 *        - Design Document(s):
 *          - None
 *
 *        - Applicable Standards (in order of precedence: highest first):
 *          - @http{sharepoint/sandbox/ghspdev/LaunchPad/EngineeringDepartment/Elec_proj_mgmt/Revision%20Controlled%20Library/Software%20Process/Work%20Instructions/Coding%20Standard.docx,
 *                  "GHSP Coding Standard"}
 *
 *      @page DFS Deviations from Standards
 *        - None
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "adc.h"
#include "global.h"
#include "fault_manager.h"
#include "foc.h"
#include "lin_transport.h"
#include "lin_vehicle_comm.h"
#include "LinProto_Interface.h"
#include "motor_ctrl.h"
#include "serial_data_buffer.h"
#include "speed_meas.h"
#include <stdlib.h>
#include <string.h>
#include "time_utils.h"

/*===========================================================================*
 * Local Preprocessor \#define Constants
 *===========================================================================*/
#define PRCNT_2_COUNTS (39216ul)

#define VCOMM_SUMPOIL_TE_ACTL_OFFSET ((int8_t)50)
#define VCOMM_FULL_SCALE_VOLTAGE ((uint16_t)3493)
#define VCOMM_LOSS_OF_COMM_TIME_MS (10000u)
#define VCOMM_NO_DEMAND_TIMEOUT_MS (1000u)
#define VCOMM_NO_STATUS_TIMEOUT_MS (VCOMM_LOSS_OF_COMM_TIME_MS)


/* J2602 Status byte application bit defines */
#define APINF04                 (0x10u)
#define APINF03                 (0x08u)
#define APINF02                 (0x04u)
#define APINF01                 (0x02u)
#define APINF00                 (0x01u)

#define APINF_SLAVE_NOT_CONFIGURED            (APINF04)
#define APINF_OUTPUT_DISABLED_ENVIRONMENTAL   (APINF02)               /* Output Disabled due to environmental issue */
#define APINF_OUTPUT_DISABLED_SHORTED         (APINF03)               /* One or more outputs disabled temporarily due to a short circuit */
#define APINF_ECU_FAULT                       (APINF03 | APINF02)     /* ECU fault (can be any internal fault or an output driver fault) */
#define APINF_FAULTS_DETECTED                 (APINF01)               /* One or more input faults detected.  Note that this fault is unavailable when ECU fault is present */


/*===========================================================================*
 * Local Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Local Type Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Const Object Definitions
 *===========================================================================*/

/*===========================================================================*
 * Local Function Prototypes
 *===========================================================================*/

/*===========================================================================*
 * Local Object Definitions
 *===========================================================================*/
STATIC bool_t vcomm_Loss_Of_Status_Header = false; /**< Flag indicating the lack of a STATUS frame header for longer than VCOMM_NO_STATUS_TIMEOUT_MS */
STATIC bool_t vcomm_Loss_Of_Demand = false; /**< Flag indicating the lack of a DEMAND frame for longer than VCOMM_LOSS_OF_COMM_TIME_MS */
STATIC bool_t vcomm_Demand_Missing = false; /**< Flag indicating the presence of the DEMAND frame based on the VCOMM_NO_DEMAND_TIMEOUT_MS */

/* ECM_TAOP_DEMAND signals */
STATIC uint8_t vcomm_V_Pc_Rq = 0u;
STATIC uint8_t vcomm_SumpOil_Te_Actl = 0u;
STATIC VCOMM_Oil_Pump_Mode_T vcomm_Mde_D_Rq = VCOMM_NUM_OPERATION_MODES;

/* TAOP_ECM_STATUS signals */
STATIC uint8_t vcomm_V_Pc_Actl = 0u;
STATIC uint8_t vcomm_U_Meas = 0u;
STATIC uint8_t vcomm_I_Meas = 0u;
STATIC VCOMM_TAOP_Status_T vcomm_V_D_Stat = VCOMM_NO_REQUEST;
STATIC VCOMM_Oil_Pump_Mode_T vcomm_Mde_D_Stat = VCOMM_MODE_IDLE;
STATIC VCOMM_TAOP_Temp_Fault_T vcomm_Te_D_Falt = VCOMM_TEMP_SIGNAL_NOT_AVAIL;
STATIC VCOMM_TAOP_Current_Fault_T vcomm_I_D_Falt = VCOMM_CURRENT_SIGNAL_NOT_AVAIL;
STATIC VCOMM_TAOP_Voltage_Fault_T vcomm_U_D_Falt = VCOMM_VOLTAGE_SIGNAL_NOT_AVAIL;
STATIC VCOMM_TAOP_Speed_Fault_T vcomm_V_D_Falt = VCOMM_SPEED_SIGNAL_NOT_AVAIL;
STATIC bool_t vcomm_Blk_B_Falt = false;
STATIC bool_t vcomm_Prtct_B_Falt = false;
STATIC uint8_t sticky_lin_fault = 0u; /**< used to record the LIN faults so they will get transmitted at least once until fault is cleared */

STATIC Time_Ticks_T vcomm_ECM_TAOP_DEMAND_timestamp = 0u;          /* Tracks when a valid ECM_TAOP_DEMAND message is received */
STATIC Time_Ticks_T vcomm_ECM_TAOP_DEMAND_Received_timestamp = 0u; /* Tracks when any ECM_TAOP_DEMAND message is received */
STATIC Time_Ticks_T vcomm_ECM_TAOP_STATUS_Received_timestamp = 0u;   /* Tracks when any ECM_TAOP_STATUS header is received */

SDB_CREATE(LIN_Rx_Buffer);
SDB_CREATE(LIN_Tx_Buffer);

/*===========================================================================*
 * Function Definitions
 *===========================================================================*/

bool_t VCOMM_Init(void)
{
    return true;
}

void VCOMM_Task(void)
{
    /* Set the supply current signal */
    VCOMM_Signal_Set_TrnOilPump_I_Meas((uint16_t)abs(mc_get_supply_current()));

    /* Set the supply voltage signal */
    VCOMM_Signal_Set_TrnOilPump_U_Meas(adc_get_supply_voltage());

    /* Set the temperature fault signal */
    if (FLT_MGR_Is_Fault_Active(FLT_OVER_TEMPERATURE))
    {
        vcomm_Te_D_Falt = VCOMM_OVER_TEMP_FAULT;
    }
    else
    {
        vcomm_Te_D_Falt = VCOMM_TEMP_NO_FAULT;
    }

    /* Set the voltage fault signal */
    if (FLT_MGR_Is_Fault_Active(FLT_OVER_VOLTAGE))
    {
        vcomm_U_D_Falt = VCOMM_OVER_VOLTAGE_FAULT;
    }
    else if (FLT_MGR_Is_Fault_Active(FLT_UNDER_VOLTAGE))
    {
        vcomm_U_D_Falt = VCOMM_UNDER_VOLTAGE_FAULT;
    }
    else
    {
        vcomm_U_D_Falt = VCOMM_VOLTAGE_NO_FAULT;
    }

    /* Set the current fault signal */
    if (FLT_MGR_Is_Fault_Active(FLT_OVER_CURRENT))
    {
        vcomm_I_D_Falt = VCOMM_OVER_CURRENT_FAULT;
    }
    else if (FLT_MGR_Is_Fault_Active (FLT_UNDER_CURRENT))
    {
        vcomm_I_D_Falt = VCOMM_UNDER_CURRENT_FAULT;
    }
    else
    {
        vcomm_I_D_Falt = VCOMM_CURRENT_NO_FAULT;
    }

    if (FLT_MGR_Is_Fault_Active(FLT_OVER_SPEED))
    {
        vcomm_V_D_Falt = VCOMM_OVER_SPEED_FAULT;
    }
    else if (FLT_MGR_Is_Fault_Active(FLT_UNDER_SPEED))
    {
        vcomm_V_D_Falt = VCOMM_UNDER_SPEED_FAULT;
    }
    else
    {
        vcomm_V_D_Falt = VCOMM_SPEED_NO_FAULT;
    }

    /* Detect missing ECM_TAOP_DEMAND message for reporting */
    if (TMUT_Has_Time_Elapsed_Ticks(vcomm_ECM_TAOP_DEMAND_Received_timestamp, TMUT_MS_To_Ticks(VCOMM_NO_DEMAND_TIMEOUT_MS)))
    {
        vcomm_Demand_Missing = true;
    }
  
      /* Detect Loss of LIN status header based on missingRequest for Status. */
    if (TMUT_Has_Time_Elapsed_Ticks(vcomm_ECM_TAOP_STATUS_Received_timestamp, TMUT_MS_To_Ticks(VCOMM_NO_STATUS_TIMEOUT_MS)))
    {
        vcomm_Loss_Of_Status_Header = true;
        vcomm_SumpOil_Te_Actl = 0u; /* Set minimum temperature when Loss of LIN occurs */

    }
    /* Detect Loss of LIN safety state, based on missing Demand or Request for Status. */
    if (TMUT_Has_Time_Elapsed_Ticks(vcomm_ECM_TAOP_DEMAND_timestamp, TMUT_MS_To_Ticks(VCOMM_LOSS_OF_COMM_TIME_MS)))
    {
        vcomm_Loss_Of_Demand = true;
        vcomm_SumpOil_Te_Actl = 0u; /* Set minimum temperature when Loss of LIN occurs */
    }
}

/*-----------------------------------*\
 *   ECM_TAOP_DEMAND Signal Getters  *
\*-----------------------------------*/
bool_t VCOMM_Loss_Of_Comm(void)
{
    bool_t Return_Val = false;
    if(vcomm_Loss_Of_Demand || vcomm_Loss_Of_Status_Header)
    {
        Return_Val = true;   
    }
    return (Return_Val);
}

VCOMM_Oil_Pump_Mode_T VCOMM_Signal_Get_TrnOilPumpMde_D_Rq(void)
{
    return vcomm_Mde_D_Rq;
}

VCOMM_RPM_T VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq(void)
{
    return (VCOMM_RPM_T)(((uint32_t)vcomm_V_Pc_Rq * MAX_RPM) / (uint8_t)UINT8_MAX);
}

VCOMM_Temperature_T VCOMM_Signal_Get_TrnSumpOil_Te_Actl(void)
{
    return (VCOMM_Temperature_T)((int16_t)vcomm_SumpOil_Te_Actl - VCOMM_SUMPOIL_TE_ACTL_OFFSET);
}

uint8_t VCOMM_Signal_Get_TrnOilPumpV_Pc_Actl(void)
{
    return(vcomm_V_Pc_Actl);
}

uint8_t VCOMM_Signal_Get_TrnOilPumpV_Pc_Req(void)
{
    return(vcomm_V_Pc_Rq);
}

/*-----------------------------------*\
 *   TAOP_ECM_STATUS Signal Setters  *
\*-----------------------------------*/
void VCOMM_Signal_Set_TrnOilPump_I_Meas(uint16_t Measured_Current)
{
    vcomm_I_Meas = (uint8_t)((Measured_Current / 51u) & (uint16_t)0x00FF);
}

void VCOMM_Signal_Set_TrnOilPump_U_Meas(uint16_t Measured_Voltage)
{
    if (Measured_Voltage > VCOMM_FULL_SCALE_VOLTAGE)
    {
        Measured_Voltage = VCOMM_FULL_SCALE_VOLTAGE;
    }

    vcomm_U_Meas = (uint8_t)(((uint32_t)Measured_Voltage * (uint8_t)UINT8_MAX) / VCOMM_FULL_SCALE_VOLTAGE);
}

VCOMM_Oil_Pump_Mode_T VCOMM_Signal_Get_TrnOilPumpMde_D_Stat(void)
{
    return vcomm_Mde_D_Stat;
}

void VCOMM_Signal_Set_TrnOilPumpMde_D_Stat(VCOMM_Oil_Pump_Mode_T Mode)
{
    vcomm_Mde_D_Stat = Mode;
}

void VCOMM_Signal_Set_TrnOilPumpBlk_B_Falt(bool_t Blockage_Detected)
{
    vcomm_Blk_B_Falt = Blockage_Detected;
}

void VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt(bool_t Self_Protect_Fault_Is_Active)
{
    vcomm_Prtct_B_Falt = Self_Protect_Fault_Is_Active;
}

void VCOMM_Signal_Set_TrnOilPumpV_D_Stat(VCOMM_TAOP_Status_T Status)
{
    vcomm_V_D_Stat = Status;
}

void VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl(VCOMM_RPM_T Actual_Speed)
{
    if (Actual_Speed > MAX_RPM)
    {
        Actual_Speed = MAX_RPM;
    }
    vcomm_V_Pc_Actl = (uint8_t)(((uint32_t)Actual_Speed * (uint8_t)UINT8_MAX) / MAX_RPM);
}


/*---------------------------------------------*\
 *           LIN Frame Handlers                *
\*---------------------------------------------*/
LinBusIf_eFrameIdAction_t app_ECM_TAOP_DEMAND(LinProtoIf_pGenericEnvData_t    genericProtoEnvData,
                                              LinBusIf_FrameID_t              frameID,
                                              LinProtoIf_cpFrameDescription_t frameDesc,
                                              LinProtoIf_pData_t              msgBuf,
                                              LinProtoIf_BufLength_t          msgBufLen,
                                              LinProtoIf_pGenericCbCtxData_t  genericProtoCbCtxData)
{
    LinBusIf_eFrameIdAction_t retVal = LinBusIf_PIDACT_RECEIVE;
    uint8_t Rx_Data[3];
    Time_Ticks_T Now = TMUT_Get_Base_Time_Ticks();
    FLT_MGR_LIN_J2602_Err_T Lin_Faults = FLT_MGR_Get_Lin_Fault_J2602();

    if (FLT_MGR_J2602_NO_ERROR == Lin_Faults)
    {
        memcpy(Rx_Data, msgBuf, 3u);

        vcomm_V_Pc_Rq = Rx_Data[0];
        vcomm_SumpOil_Te_Actl = Rx_Data[1];
        vcomm_Mde_D_Rq = (VCOMM_Oil_Pump_Mode_T)(Rx_Data[2] & (uint8_t)0x03);

        if (    (    (VCOMM_MODE_IDLE == vcomm_Mde_D_Rq)
                  && (vcomm_V_Pc_Rq == 0u))
             || (    (VCOMM_MODE_RUN == vcomm_Mde_D_Rq)
                  && (0u != vcomm_V_Pc_Rq))
           )
        {
            vcomm_Loss_Of_Demand = false;

            vcomm_ECM_TAOP_DEMAND_timestamp = Now;
        }

        vcomm_Demand_Missing = false;
        vcomm_ECM_TAOP_DEMAND_Received_timestamp = Now;
    }
    else
    {
        /* vcomm_Mde_D_Rq and vcomm_V_Pc_Rq retain the last known value */ 
        vcomm_SumpOil_Te_Actl = 0u;
    }

    return retVal;
}

LinBusIf_eFrameIdAction_t app_TAOP_ECM_STATUS(LinProtoIf_pGenericEnvData_t    genericProtoEnvData,
                                              LinBusIf_FrameID_t              frameID,
                                              LinProtoIf_cpFrameDescription_t frameDesc,
                                              LinProtoIf_pData_t              msgBuf,
                                              LinProtoIf_BufLength_t          msgBufLen,
                                              LinProtoIf_pGenericCbCtxData_t  genericProtoCbCtxData)
{
    LinBusIf_eFrameIdAction_t retVal = LinBusIf_PIDACT_TRANSMIT;
    uint8_t Tx_Data[6] = {0u};
    FLT_MGR_LIN_J2602_Err_T LIN_Error = FLT_MGR_Get_Lin_Fault_J2602();
    VCOMM_TAOP_Status_T Tx_V_D_Stat = VCOMM_NUM_REQUEST_STATUS;
    vcomm_ECM_TAOP_STATUS_Received_timestamp = TMUT_Get_Base_Time_Ticks();

    /* Override the transmitted V_D_Stat value if there are appropriate error conditions */
    if (vcomm_Demand_Missing)
    {
        /* No DEMAND frame recevied for VCOMM_NO_DEMAND_TIMEOUT_MS */
        Tx_V_D_Stat = VCOMM_NO_REQUEST;
    }
    else if (vcomm_Loss_Of_Demand)
    {
        /* No valid DEMAND frame received for VCOMM_LOSS_OF_COMM_TIME_MS */
        Tx_V_D_Stat = VCOMM_REQUEST_DENIED;
    }
    else if (FLT_MGR_J2602_NO_ERROR != LIN_Error)
    {
        /* LIN error reported. Report Request Denied */
        Tx_V_D_Stat = VCOMM_REQUEST_DENIED;
    }
    else
    {
        /* Nothing preventing normal reporting */
        Tx_V_D_Stat = vcomm_V_D_Stat;
    }

    if(LIN_Error > 0u)
    {
        Tx_Data[0] = (uint8_t)LIN_Error;
        sticky_lin_fault = Tx_Data[0];
    }
    else if (sticky_lin_fault != 0u)
    {
        /* lin faults are "sticky", they are retained until they are successfully reported without any detected errors */
        /* sticky_lin_fault U8 variable keeps track of the fault until no errors are detected on lin bus */
        Tx_Data[0] = sticky_lin_fault;
        sticky_lin_fault = 0u;
    }
    else
    {
        vcomm_Loss_Of_Status_Header = false;
    }

    if(vcomm_Prtct_B_Falt)
    {
        Tx_Data[0] |= APINF_OUTPUT_DISABLED_ENVIRONMENTAL;
    }
    
    if (FLT_MGR_IRQSTAT_Fault_Present())
    {
        Tx_Data[0] |= APINF_OUTPUT_DISABLED_SHORTED;
    }

    if((FLT_MGR_Is_Any_Fault_Active()) || (vcomm_Prtct_B_Falt))
    {
        Tx_Data[0] |= APINF_FAULTS_DETECTED;
    }


    Tx_Data[1] = vcomm_V_Pc_Actl;
    Tx_Data[2] = vcomm_U_Meas;
    Tx_Data[3] = vcomm_I_Meas;
    Tx_Data[4] = ((uint8_t)Tx_V_D_Stat       & (uint8_t)0x03)       | /* Bits 0, 1 */
                 (((uint8_t)vcomm_Mde_D_Stat & (uint8_t)0x03) << 2) | /* Bits 2, 3 */
                 (((uint8_t)vcomm_Te_D_Falt  & (uint8_t)0x03) << 4) | /* Bits 4, 5 */
                 (((uint8_t)vcomm_I_D_Falt   & (uint8_t)0x03) << 6);  /* Bits 6, 7 */

    Tx_Data[5] = ((uint8_t)vcomm_U_D_Falt  & (uint8_t)0x03)       | /* Bits 0, 1 */
                 (((uint8_t)vcomm_V_D_Falt & (uint8_t)0x03) << 2) | /* Bits 2, 3 */
                 ((vcomm_Blk_B_Falt ? 1u : 0u)              << 4) | /* Bit 4 */
                 ((vcomm_Prtct_B_Falt ? 1u : 0u)            << 5);  /* Bit 5 */

    memcpy(msgBuf, Tx_Data, 6u);

    return retVal;
}

LinBusIf_eFrameIdAction_t app_Master_Diag_Request(LinProtoIf_pGenericEnvData_t    genericProtoEnvData,
                                                  LinBusIf_FrameID_t              frameID,
                                                  LinProtoIf_cpFrameDescription_t frameDesc,
                                                  LinProtoIf_pData_t              msgBuf,
                                                  LinProtoIf_BufLength_t          msgBufLen,
                                                  LinProtoIf_pGenericCbCtxData_t  genericProtoCbCtxData)
{
    LinBusIf_eFrameIdAction_t retVal = LinBusIf_PIDACT_RECEIVE;
    uint8_t Rx_Data[8] = {0u};

    memcpy(Rx_Data, msgBuf, msgBufLen);
    SDB_Load(&LIN_Rx_Buffer, Rx_Data, (size_t)msgBufLen, (size_t)msgBufLen);
    LIN_Diagnostic_Request(&LIN_Rx_Buffer, NULL);

    return retVal;
}

LinBusIf_eFrameIdAction_t app_Slave_Diag_Response(LinProtoIf_pGenericEnvData_t    genericProtoEnvData,
                                                  LinBusIf_FrameID_t              frameID,
                                                  LinProtoIf_cpFrameDescription_t frameDesc,
                                                  LinProtoIf_pData_t              msgBuf,
                                                  LinProtoIf_BufLength_t          msgBufLen,
                                                  LinProtoIf_pGenericCbCtxData_t  genericProtoCbCtxData)
{
    LinBusIf_eFrameIdAction_t retVal = LinBusIf_PIDACT_TRANSMIT;
    uint8_t Tx_Data[8] = {0u};

    SDB_Init(&LIN_Tx_Buffer, Tx_Data, 8u);

    LIN_Diagnostic_Response(NULL, &LIN_Tx_Buffer);

    memcpy(msgBuf, Tx_Data, SDB_Length(&LIN_Tx_Buffer));

    return retVal;
}

/** @} doxygen end group */
