/**
 *  @file test_app_ECM_TAOP_DEMAND.c
 *
 */
#include "unity.h"
#include "lin_vehicle_comm.h"
#include "LinBus_Types.h"
#include "time_utils.h"

/* MOCKS */
#include "mock_adc.h"
#include "mock_fault_manager.h"
#include "mock_foc_stubs.h"
#include "mock_lin_transport.h"
#include "mock_motor_ctrl.h"
#include "mock_serial_data_buffer.h"
#include "mock_speed_meas.h"
#include "mock_time_utils.h"

/* STATIC's */
uint8_t vcomm_V_Pc_Rq;
uint8_t vcomm_SumpOil_Te_Actl;
VCOMM_Oil_Pump_Mode_T vcomm_Mde_D_Rq;

VCOMM_TAOP_Status_T vcomm_V_D_Stat;

Time_MS_T vcomm_ECM_TAOP_DEMAND_timestamp;
Time_MS_T vcomm_ECM_TAOP_DEMAND_Received_timestamp;
bool_t vcomm_Blk_B_Falt;
bool_t vcomm_Prtct_B_Falt;

void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * Verifies that the function app_ECM_TAOP_DEMAND will extract the raw
 * values from the ECM_TAOP_DEMAND frame and place them in the correct internal
 * storage. The raw signals are as follows:
 *
 * V_Pc_Rq = 0
 * SumpOil_Te_Actl = 0
 * Mde_D_Rq = 0
 */
/* Reqs: SWREQ_2066, SWREQ_2078 */
void test_app_ECM_TAOP_DEMAND_V_Pc_Rq_0_SumpOil_Te_Actl_0_Mde_D_Rq_0(void)
{
    LinBusIf_eFrameIdAction_t result = LinBusIf_PIDACT_IGNORE;
    /* Ensure known test state */
    uint8_t test_rx_buffer[3] = {0x00, 0x00, 0x00};

    vcomm_V_Pc_Rq = 0xFF;
    vcomm_SumpOil_Te_Actl = 0xFF;
    vcomm_Mde_D_Rq = VCOMM_NUM_OPERATION_MODES;
    vcomm_ECM_TAOP_DEMAND_timestamp = 0;
    vcomm_ECM_TAOP_DEMAND_Received_timestamp = 0;
    vcomm_Blk_B_Falt = false;
    vcomm_Prtct_B_Falt = false;

    /* Setup expected call chain */
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(123456);
    FLT_MGR_Get_Lin_Fault_J2602_ExpectAndReturn(FLT_MGR_J2602_NO_ERROR);

    /* Call function under test */
    result = app_ECM_TAOP_DEMAND(NULL,           /* genericProtoEnvData, ignored */
                                 0x08,           /* frameID = 0x08 */
                                 NULL,           /* frameDesc, ignored */
                                 test_rx_buffer, /* msgBuf, contains message data */
                                 3,              /* msgBufLen */
                                 NULL);          /* genericProtoCbCtxData, ignored */
    

    /* Verify test results */
    TEST_ASSERT_EQUAL(LinBusIf_PIDACT_RECEIVE, result);
    TEST_ASSERT_EQUAL_UINT8(0, vcomm_V_Pc_Rq);
    TEST_ASSERT_EQUAL_INT8(0, vcomm_SumpOil_Te_Actl);
    TEST_ASSERT_EQUAL_UINT8(VCOMM_MODE_IDLE, vcomm_Mde_D_Rq);
    TEST_ASSERT_EQUAL_UINT16(123456, vcomm_ECM_TAOP_DEMAND_timestamp);
    TEST_ASSERT_EQUAL_UINT16(123456, vcomm_ECM_TAOP_DEMAND_Received_timestamp);
    
}

/**
 * Verifies that the function app_ECM_TAOP_DEMAND will extract the raw
 * values from the ECM_TAOP_DEMAND frame and place them in the correct internal
 * storage. The raw signals are as follows:
 *
 * V_Pc_Rq = 0
 * SumpOil_Te_Actl = -13
 * Mde_D_Rq = 0
 */
/* Reqs: SWREQ_2066, SWREQ_2078 */
void test_app_ECM_TAOP_DEMAND_V_Pc_Rq_0_SumpOil_Te_Actl_neg13_Mde_D_Rq_0(void)
{
    LinBusIf_eFrameIdAction_t result = LinBusIf_PIDACT_IGNORE;
    /* Ensure known test state */
    uint8_t test_rx_buffer[3] = {0x00, -13, 0x00};

    vcomm_V_Pc_Rq = 0xFF;
    vcomm_SumpOil_Te_Actl = 0xFF;
    vcomm_Mde_D_Rq = VCOMM_NUM_OPERATION_MODES;
    vcomm_ECM_TAOP_DEMAND_timestamp = 0;
    vcomm_ECM_TAOP_DEMAND_Received_timestamp = 0;
    vcomm_Blk_B_Falt = false;
    vcomm_Prtct_B_Falt = false;
	
    /* Setup expected call chain */
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(0xABCDEF);
    FLT_MGR_Get_Lin_Fault_J2602_ExpectAndReturn(FLT_MGR_J2602_NO_ERROR);

    /* Call function under test */
    result = app_ECM_TAOP_DEMAND(NULL,           /* genericProtoEnvData, ignored */
                                 0x08,           /* frameID = 0x08 */
                                 NULL,           /* frameDesc, ignored */
                                 test_rx_buffer, /* msgBuf, contains message data */
                                 3,              /* msgBufLen */
                                 NULL);          /* genericProtoCbCtxData, ignored */


    /* Verify test results */
    TEST_ASSERT_EQUAL(LinBusIf_PIDACT_RECEIVE, result);
    TEST_ASSERT_EQUAL_UINT8(0, vcomm_V_Pc_Rq);
    TEST_ASSERT_EQUAL_INT8(-13, vcomm_SumpOil_Te_Actl);
    TEST_ASSERT_EQUAL_UINT8(VCOMM_MODE_IDLE, vcomm_Mde_D_Rq);
    TEST_ASSERT_EQUAL_UINT16(0xABCDEF, vcomm_ECM_TAOP_DEMAND_timestamp);
    TEST_ASSERT_EQUAL_UINT16(0xABCDEF, vcomm_ECM_TAOP_DEMAND_Received_timestamp);
}

/**
 * Verifies that the function app_ECM_TAOP_DEMAND will extract the raw
 * values from the ECM_TAOP_DEMAND frame and place them in the correct internal
 * storage. The raw signals are as follows:
 *
 * V_Pc_Rq = 127
 * SumpOil_Te_Actl = 25
 * Mde_D_Rq = 1
 */
/* Reqs: SWREQ_2066, SWREQ_2078 */
void test_app_ECM_TAOP_DEMAND_V_Pc_Rq_127_SumpOil_Te_Actl_25_Mde_D_Rq_1(void)
{
    LinBusIf_eFrameIdAction_t result = LinBusIf_PIDACT_IGNORE;
    /* Ensure known test state */
    uint8_t test_rx_buffer[3] = {127, 25, 0x01};

    vcomm_V_Pc_Rq = 0xFF;
    vcomm_SumpOil_Te_Actl = 0xFF;
    vcomm_Mde_D_Rq = VCOMM_NUM_OPERATION_MODES;
    vcomm_ECM_TAOP_DEMAND_timestamp = 0;
    vcomm_ECM_TAOP_DEMAND_Received_timestamp = 0;
    vcomm_Blk_B_Falt = false;
    vcomm_Prtct_B_Falt = false;

    /* Setup expected call chain */
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(0xDEADBEEF);
    FLT_MGR_Get_Lin_Fault_J2602_ExpectAndReturn(FLT_MGR_J2602_NO_ERROR);

    /* Call function under test */
    result = app_ECM_TAOP_DEMAND(NULL,           /* genericProtoEnvData, ignored */
                                 0x08,           /* frameID = 0x08 */
                                 NULL,           /* frameDesc, ignored */
                                 test_rx_buffer, /* msgBuf, contains message data */
                                 3,              /* msgBufLen */
                                 NULL);          /* genericProtoCbCtxData, ignored */


    /* Verify test results */
    TEST_ASSERT_EQUAL(LinBusIf_PIDACT_RECEIVE, result);
    TEST_ASSERT_EQUAL_UINT8(127, vcomm_V_Pc_Rq);
    TEST_ASSERT_EQUAL_INT8(25, vcomm_SumpOil_Te_Actl);
    TEST_ASSERT_EQUAL_UINT8(VCOMM_MODE_RUN, vcomm_Mde_D_Rq);
    TEST_ASSERT_EQUAL_UINT16(0xDEADBEEF, vcomm_ECM_TAOP_DEMAND_timestamp);
    TEST_ASSERT_EQUAL_UINT16(0xDEADBEEF, vcomm_ECM_TAOP_DEMAND_Received_timestamp);

}

/**
 * Verifies that the function app_ECM_TAOP_DEMAND will NOT change  vcomm_V_Pc_Rq,
 *  or vcomm_Mde_D_Rq when there is a LIN Checksum error. vcomm_SumpOil_Te_Actl should revert 0
 * Raw signals for the received frame should remain the same, to maintain last known state
 * until the Loss of Lin timer expires. 
 */
/* Reqs: SWREQ_2066, SWREQ_2078  SYS-REQ-1513 */
void test_app_ECM_TAOP_DEMAND_Checksum_Error_should_set_oil_temp_signal_to_0_but_not_others(void)
{
    LinBusIf_eFrameIdAction_t result = LinBusIf_PIDACT_IGNORE;
    /* Ensure known test state */
    uint8_t test_rx_buffer[3] = {127, 25, 0x01};

    vcomm_V_Pc_Rq = 0xFF;
    vcomm_SumpOil_Te_Actl = 0xFF;
    vcomm_Mde_D_Rq = VCOMM_MODE_RUN;
    vcomm_ECM_TAOP_DEMAND_timestamp = 0;
    vcomm_ECM_TAOP_DEMAND_Received_timestamp = 0;
    vcomm_Blk_B_Falt = false;
    vcomm_Prtct_B_Falt = false;

    /* Setup expected call chain */
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(0xDEADBEEF);
    FLT_MGR_Get_Lin_Fault_J2602_ExpectAndReturn(FLT_MGR_J2602_CHECKSUM_ERROR);

    /* Call function under test */
    result = app_ECM_TAOP_DEMAND(NULL,           /* genericProtoEnvData, ignored */
                                 0x08,           /* frameID = 0x08 */
                                 NULL,           /* frameDesc, ignored */
                                 test_rx_buffer, /* msgBuf, contains message data */
                                 3,              /* msgBufLen */
                                 NULL);          /* genericProtoCbCtxData, ignored */


    /* Verify test results */
    TEST_ASSERT_EQUAL(LinBusIf_PIDACT_RECEIVE, result);
    TEST_ASSERT_EQUAL_UINT8(0xFF, vcomm_V_Pc_Rq);
    TEST_ASSERT_EQUAL_INT8( 0, vcomm_SumpOil_Te_Actl);
    TEST_ASSERT_EQUAL_UINT8(VCOMM_MODE_RUN, vcomm_Mde_D_Rq);
    TEST_ASSERT_EQUAL_UINT16(0, vcomm_ECM_TAOP_DEMAND_timestamp);
    TEST_ASSERT_EQUAL_UINT16(0, vcomm_ECM_TAOP_DEMAND_Received_timestamp);

}

/**
 * Verifies that the function app_ECM_TAOP_DEMAND will extract the raw
 * values from the ECM_TAOP_DEMAND frame. This test case verifies the behavior
 * when the frame commands a speed of 0% but a run mode while in IDLE.
 * The raw signals are as follows:
 *
 * V_Pc_Rq = 0
 * SumpOil_Te_Actl = 25
 * Mde_D_Rq = 1
 */
/* Reqs: SWREQ_2066, SWREQ_2078 */
void test_app_ECM_TAOP_DEMAND_In_IDLE_V_Pc_Rq_0_SumpOil_Te_Actl_25_Mde_D_Rq_1_Sets_Proper_Signals(void)
{
    LinBusIf_eFrameIdAction_t result = LinBusIf_PIDACT_IGNORE;
    /* Ensure known test state */
    uint8_t test_rx_buffer[3] = {0, 25, 0x01};

    vcomm_V_Pc_Rq = 0x25;
    vcomm_SumpOil_Te_Actl = 0xFF;
    vcomm_Mde_D_Rq = VCOMM_MODE_IDLE;
    vcomm_ECM_TAOP_DEMAND_timestamp = 0;
    vcomm_ECM_TAOP_DEMAND_Received_timestamp = 0;
	vcomm_Blk_B_Falt = false;
    vcomm_Prtct_B_Falt = false;

    vcomm_V_D_Stat = VCOMM_NUM_REQUEST_STATUS;

    /* Setup expected call chain */
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(0xDEADBEEF);
    FLT_MGR_Get_Lin_Fault_J2602_ExpectAndReturn(FLT_MGR_J2602_NO_ERROR);

    /* Call function under test */
    result = app_ECM_TAOP_DEMAND(NULL,           /* genericProtoEnvData, ignored */
                                 0x08,           /* frameID = 0x08 */
                                 NULL,           /* frameDesc, ignored */
                                 test_rx_buffer, /* msgBuf, contains message data */
                                 3,              /* msgBufLen */
                                 NULL);          /* genericProtoCbCtxData, ignored */


    /* Verify test results */
    TEST_ASSERT_EQUAL(LinBusIf_PIDACT_RECEIVE, result);
    TEST_ASSERT_EQUAL_UINT8(0, vcomm_V_Pc_Rq);
    TEST_ASSERT_EQUAL_INT8(25, vcomm_SumpOil_Te_Actl);
    TEST_ASSERT_EQUAL_UINT8(VCOMM_MODE_RUN, vcomm_Mde_D_Rq);
    TEST_ASSERT_EQUAL_UINT16(0, vcomm_ECM_TAOP_DEMAND_timestamp);
    TEST_ASSERT_EQUAL_UINT16(0xDEADBEEF, vcomm_ECM_TAOP_DEMAND_Received_timestamp);

}

/**
 * Verifies that the function app_ECM_TAOP_DEMAND will extract the raw
 * values from the ECM_TAOP_DEMAND frame. This test case verifies the behavior
 * when the frame commands a speed of 0% but a run mode while in RUN.
 * The raw signals are as follows:
 *
 * V_Pc_Rq = 127
 * SumpOil_Te_Actl = 25
 * Mde_D_Rq = 0
 */
/* Reqs: SWREQ_2066, SWREQ_2078 */
void test_app_ECM_TAOP_DEMAND_In_RUN_V_Pc_Rq_0_SumpOil_Te_Actl_25_Mde_D_Rq_0_Sets_Proper_Signals(void)
{
    LinBusIf_eFrameIdAction_t result = LinBusIf_PIDACT_IGNORE;
    /* Ensure known test state */
    uint8_t test_rx_buffer[3] = {127, 25, 0x00};

    vcomm_V_Pc_Rq = 0x25;
    vcomm_SumpOil_Te_Actl = 0xFF;
    vcomm_Mde_D_Rq = VCOMM_MODE_IDLE;
    vcomm_ECM_TAOP_DEMAND_timestamp = 0;
    vcomm_ECM_TAOP_DEMAND_Received_timestamp = 0;
	vcomm_Blk_B_Falt = false;
    vcomm_Prtct_B_Falt = false;

    vcomm_V_D_Stat = VCOMM_NUM_REQUEST_STATUS;

    /* Setup expected call chain */
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(0xDEADBEEF);
    FLT_MGR_Get_Lin_Fault_J2602_ExpectAndReturn(FLT_MGR_J2602_NO_ERROR);

    /* Call function under test */
    result = app_ECM_TAOP_DEMAND(NULL,           /* genericProtoEnvData, ignored */
                                 0x08,           /* frameID = 0x08 */
                                 NULL,           /* frameDesc, ignored */
                                 test_rx_buffer, /* msgBuf, contains message data */
                                 3,              /* msgBufLen */
                                 NULL);          /* genericProtoCbCtxData, ignored */


    /* Verify test results */
    TEST_ASSERT_EQUAL(LinBusIf_PIDACT_RECEIVE, result);
    TEST_ASSERT_EQUAL_UINT8(127, vcomm_V_Pc_Rq);
    TEST_ASSERT_EQUAL_INT8(25, vcomm_SumpOil_Te_Actl);
    TEST_ASSERT_EQUAL_UINT8(VCOMM_MODE_IDLE, vcomm_Mde_D_Rq);
    TEST_ASSERT_EQUAL_UINT16(0, vcomm_ECM_TAOP_DEMAND_timestamp);
    TEST_ASSERT_EQUAL_UINT16(0xDEADBEEF, vcomm_ECM_TAOP_DEMAND_Received_timestamp);

}
