/**
 *  @file test_VCOMM_Signal_Set_TrnOilPump_I_Meas.c
 *
 */
#include "unity.h"
#include "lin_vehicle_comm.h"

/* MOCKS */
#include "mock_adc.h"
#include "mock_fault_manager.h"
#include "mock_foc_stubs.h"
#include "mock_lin_transport.h"
#include "mock_motor_ctrl.h"
#include "mock_serial_data_buffer.h"
#include "mock_speed_meas.h"
#include "mock_time_utils.h"

/* STATIC's */
uint8_t vcomm_I_Meas;

void setUp(void)
{
    vcomm_I_Meas = 0;
}

void tearDown(void)
{

}

/**
 * @test Verifies that VCOMM_Signal_Set_TrnOilPump_I_Meas() will set the LIN signal
 * to 50 when the actual current is 2560 (10A)
 *
 */
/* Reqs: SWREQ_2079, SWREQ_2080 */
void test_VCOMM_Signal_Set_TrnOilPump_I_Meas_sets_signal_to_50_When_current_is_2560(void)
{
    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    VCOMM_Signal_Set_TrnOilPump_I_Meas(2560);
    
    /* Verify test results */
    TEST_ASSERT_UINT8_WITHIN(1, 50, vcomm_I_Meas);
}

/**
 * @test Verifies that VCOMM_Signal_Set_TrnOilPump_I_Meas() will set the LIN signal
 * to 5 when the actual current is 256 (1A)
 *
 */
/* Reqs: SWREQ_2079, SWREQ_2080 */
void test_VCOMM_Signal_Set_TrnOilPump_I_Meas_sets_signal_to_5_When_current_is_256(void)
{
    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    VCOMM_Signal_Set_TrnOilPump_I_Meas(256);

    /* Verify test results */
    TEST_ASSERT_UINT8_WITHIN(1, 5, vcomm_I_Meas);
}

/**
 * @test Verifies that VCOMM_Signal_Set_TrnOilPump_I_Meas() will set the LIN signal
 * to 1 when the actual current is 51 (0.2A)
 *
 */
/* Reqs: SWREQ_2079, SWREQ_2080 */
void test_VCOMM_Signal_Set_TrnOilPump_I_Meas_sets_signal_to_1_When_current_is_51(void)
{
    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    VCOMM_Signal_Set_TrnOilPump_I_Meas(51);

    /* Verify test results */
    TEST_ASSERT_UINT8_WITHIN(1, 1, vcomm_I_Meas);
}
