/**
 *  @file test_app_ECM_TAOP_DEMAND.c
 *
 */
#include "unity.h"
#include "lin_vehicle_comm.h"
#include "LinBus_Types.h"

/* MOCKS */
#include "mock_adc.h"
#include "mock_fault_manager.h"
#include "mock_foc_stubs.h"
#include "mock_lin_transport.h"
#include "mock_motor_ctrl.h"
#include "mock_serial_data_buffer.h"
#include "mock_speed_meas.h"
#include "mock_time_utils.h"

/* STATIC's */
uint8_t vcomm_V_Pc_Actl;
uint8_t vcomm_U_Meas;
uint8_t vcomm_I_Meas;
VCOMM_TAOP_Status_T vcomm_V_D_Stat;
VCOMM_Oil_Pump_Mode_T vcomm_Mde_D_Stat;
VCOMM_TAOP_Temp_Fault_T vcomm_Te_D_Falt;
VCOMM_TAOP_Current_Fault_T vcomm_I_D_Falt;
VCOMM_TAOP_Voltage_Fault_T vcomm_U_D_Falt;
VCOMM_TAOP_Speed_Fault_T vcomm_V_D_Falt;
bool_t vcomm_Blk_B_Falt;
bool_t vcomm_Prtct_B_Falt;
bool_t vcomm_Loss_Of_Status_Header;
bool_t vcomm_Demand_Missing;
uint8_t sticky_lin_fault;

void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * @test Verifies that the function app_TAOP_ECM_STATUS will create a data buffer
 * with the proper values when the associated signals for the TAOP_ECM_STATUS frame
 * are set as follows:
 *
 * LIN / APINF Status = 0x00
 * vcomm_V_Pc_Actl = 34
 * vcomm_U_Meas = 16
 * vcomm_I_Meas = -4
 * vcomm_V_D_Stat = VCOMM_REQUEST_HONORED
 * vcomm_Mde_D_Stat = VCOMM_MODE_IDLE
 * All Faults = NO Fault or equivalent
 */
/* Reqs: SWREQ_1936, SWREQ_1192 */
void test_app_TAOP_ECM_STATUS_V_Pc_Actl_34_U_Meas_16_I_Meas_neg4_IDLE_No_Fault(void)
{
    uint8_t actual_tx_buffer[6] = {0};
    uint8_t expected_tx_buffer[6] =
    {
        0x00,         /* LIN / APINF Status */
        34,           /* Actual Speed */
        16,           /* Actual Voltage */
        4,            /* Actual Current */
        0x01,         /* V_D_Stat (2 bits 0, 1), Mde_D_Stat (2 bits 2, 3), Te_D_Falt (2 bits 4, 5), I_D_Falt (2 bits 6, 7) */
        0x00          /* U_D_Falt (2 bits 0, 1), V_D_Falt (2 bits 2, 3), Blk_B_Falt (1 bit 4), Prtct_B_Falt (1 bit 5) */
    };
    LinBusIf_eFrameIdAction_t result = LinBusIf_PIDACT_IGNORE;

    /* Ensure known test state */
    vcomm_V_Pc_Actl = 34;
    vcomm_U_Meas = 16;
    vcomm_I_Meas = 4;
    vcomm_V_D_Stat = VCOMM_REQUEST_HONORED;
    vcomm_Mde_D_Stat = VCOMM_MODE_IDLE;
    vcomm_Te_D_Falt = VCOMM_TEMP_NO_FAULT;
    vcomm_I_D_Falt = VCOMM_CURRENT_NO_FAULT;
    vcomm_U_D_Falt = VCOMM_VOLTAGE_NO_FAULT;
    vcomm_V_D_Falt = VCOMM_SPEED_NO_FAULT;
    vcomm_Blk_B_Falt = false;
    vcomm_Prtct_B_Falt = false;
    sticky_lin_fault = 0;

    /* Setup expected call chain */
    FLT_MGR_Get_Lin_Fault_J2602_ExpectAndReturn(FLT_MGR_J2602_NO_ERROR);
    FLT_MGR_Is_Any_Fault_Active_ExpectAndReturn(false);
    FLT_MGR_IRQSTAT_Fault_Present_ExpectAndReturn(0);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(123456);

    /* Call function under test */
    result = app_TAOP_ECM_STATUS(NULL,           /* genericProtoEnvData, ignored */
                                 0x09,           /* frameID = 0x09 */
                                 NULL,           /* frameDesc, ignored */
                                 actual_tx_buffer, /* msgBuf, contains message data */
                                 6,              /* msgBufLen */
                                 NULL);          /* genericProtoCbCtxData, ignored */
    

    /* Verify test results */
    TEST_ASSERT_EQUAL(LinBusIf_PIDACT_TRANSMIT, result);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_tx_buffer, actual_tx_buffer, 6);
    TEST_ASSERT_EQUAL(VCOMM_REQUEST_HONORED, vcomm_V_D_Stat);
    
}

/**
 * @test Verifies that the function app_TAOP_ECM_STATUS will create a data buffer
 * with the proper values when the associated signals for the TAOP_ECM_STATUS frame
 * are set as follows:
 *
 * LIN / APINF Status = 0x00
 * vcomm_V_Pc_Actl = 34
 * vcomm_U_Meas = 16
 * vcomm_I_Meas = -4
 * vcomm_V_D_Stat = VCOMM_REQUEST_HONORED
 * vcomm_Mde_D_Stat = VCOMM_MODE_RUN
 * All Faults = NO Fault or equivalent
 */
/* Reqs: SWREQ_1936, SWREQ_1192 */
void test_app_TAOP_ECM_STATUS_V_Pc_Actl_34_U_Meas_16_I_Meas_neg4_RUN_No_Fault(void)
{
    uint8_t actual_tx_buffer[6] = {0};
    uint8_t expected_tx_buffer[6] =
    {
        0x00,         /* LIN / APINF Status */
        34,           /* Actual Speed */
        16,           /* Actual Voltage */
        4,            /* Actual Current */
        0x05,         /* V_D_Stat (2 bits), Mde_D_Stat (2 bits), Te_D_Falt (2 bits), I_D_Falt (2 bits) */
        0x00          /* U_D_Falt (2 bits), V_D_Falt (2 bits), Blk_B_Falt (1 bit), Prtct_B_Falt (1 bit) */
    };
    LinBusIf_eFrameIdAction_t result = LinBusIf_PIDACT_IGNORE;

    /* Ensure known test state */
    vcomm_V_Pc_Actl = 34;
    vcomm_U_Meas = 16;
    vcomm_I_Meas = 4;
    vcomm_V_D_Stat = VCOMM_REQUEST_HONORED;
    vcomm_Mde_D_Stat = VCOMM_MODE_RUN;
    vcomm_Te_D_Falt = VCOMM_TEMP_NO_FAULT;
    vcomm_I_D_Falt = VCOMM_CURRENT_NO_FAULT;
    vcomm_U_D_Falt = VCOMM_VOLTAGE_NO_FAULT;
    vcomm_V_D_Falt = VCOMM_SPEED_NO_FAULT;
    vcomm_Blk_B_Falt = false;
    vcomm_Prtct_B_Falt = false;
    sticky_lin_fault = 0;

    /* Setup expected call chain */
    FLT_MGR_Get_Lin_Fault_J2602_ExpectAndReturn(FLT_MGR_J2602_NO_ERROR);
    FLT_MGR_Is_Any_Fault_Active_ExpectAndReturn(false);
    FLT_MGR_IRQSTAT_Fault_Present_ExpectAndReturn(0);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(123456);

    /* Call function under test */
    result = app_TAOP_ECM_STATUS(NULL,           /* genericProtoEnvData, ignored */
                                 0x09,           /* frameID = 0x09 */
                                 NULL,           /* frameDesc, ignored */
                                 actual_tx_buffer, /* msgBuf, contains message data */
                                 6,              /* msgBufLen */
                                 NULL);          /* genericProtoCbCtxData, ignored */


    /* Verify test results */
    TEST_ASSERT_EQUAL(LinBusIf_PIDACT_TRANSMIT, result);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_tx_buffer, actual_tx_buffer, 6);
    TEST_ASSERT_EQUAL(VCOMM_REQUEST_HONORED, vcomm_V_D_Stat);

}

/**
 * @test Verifies that the function app_TAOP_ECM_STATUS will create a data buffer
 * with the proper values when the associated signals for the TAOP_ECM_STATUS frame
 * are set as follows:
 *
 * LIN / APINF Status = 0x00
 * vcomm_V_Pc_Actl = 34
 * vcomm_U_Meas = 16
 * vcomm_I_Meas = -4
 * vcomm_V_D_Stat = VCOMM_REQUEST_DENIED
 * vcomm_Mde_D_Stat = VCOMM_MODE_RUN
 * All Faults = NO Fault or equivalent
 */
/* Reqs: SWREQ_1936, SWREQ_1192, SWREQ_1986, SWREQ_1989 */
void test_app_TAOP_ECM_STATUS_V_Pc_Actl_34_U_Meas_16_I_Meas_neg4_RUN_No_Fault_VCOMM_REQUEST_DENIED(void)
{
    uint8_t actual_tx_buffer[6] = {0};
    uint8_t expected_tx_buffer[6] =
    {
        0x00,         /* LIN / APINF Status */
        34,           /* Actual Speed */
        16,           /* Actual Voltage */
        4,            /* Actual Current */
        0x06,         /* V_D_Stat (2 bits), Mde_D_Stat (2 bits), Te_D_Falt (2 bits), I_D_Falt (2 bits) */
        0x00          /* U_D_Falt (2 bits), V_D_Falt (2 bits), Blk_B_Falt (1 bit), Prtct_B_Falt (1 bit) */
    };
    LinBusIf_eFrameIdAction_t result = LinBusIf_PIDACT_IGNORE;

    /* Ensure known test state */
    vcomm_V_Pc_Actl = 34;
    vcomm_U_Meas = 16;
    vcomm_I_Meas = 4;
    vcomm_V_D_Stat = VCOMM_REQUEST_DENIED;
    vcomm_Mde_D_Stat = VCOMM_MODE_RUN;
    vcomm_Te_D_Falt = VCOMM_TEMP_NO_FAULT;
    vcomm_I_D_Falt = VCOMM_CURRENT_NO_FAULT;
    vcomm_U_D_Falt = VCOMM_VOLTAGE_NO_FAULT;
    vcomm_V_D_Falt = VCOMM_SPEED_NO_FAULT;
    vcomm_Blk_B_Falt = false;
    vcomm_Prtct_B_Falt = false;
    sticky_lin_fault = 0;

    /* Setup expected call chain */
    FLT_MGR_Get_Lin_Fault_J2602_ExpectAndReturn(FLT_MGR_J2602_NO_ERROR);
    FLT_MGR_Is_Any_Fault_Active_ExpectAndReturn(false);
    FLT_MGR_IRQSTAT_Fault_Present_ExpectAndReturn(0);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(123456);

    /* Call function under test */
    result = app_TAOP_ECM_STATUS(NULL,           /* genericProtoEnvData, ignored */
                                 0x09,           /* frameID = 0x09 */
                                 NULL,           /* frameDesc, ignored */
                                 actual_tx_buffer, /* msgBuf, contains message data */
                                 6,              /* msgBufLen */
                                 NULL);          /* genericProtoCbCtxData, ignored */


    /* Verify test results */
    TEST_ASSERT_EQUAL(LinBusIf_PIDACT_TRANSMIT, result);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_tx_buffer, actual_tx_buffer, 6);
    TEST_ASSERT_EQUAL(VCOMM_REQUEST_DENIED, vcomm_V_D_Stat);

}

/**
 * @test Verifies that the function app_TAOP_ECM_STATUS will create a data buffer
 * with the proper values when the associated signals for the TAOP_ECM_STATUS frame
 * are set as follows:
 *
 * LIN / APINF Status = 0x00
 * vcomm_V_Pc_Actl = 34
 * vcomm_U_Meas = 16
 * vcomm_I_Meas = -4
 * vcomm_V_D_Stat = VCOMM_NO_REQUEST
 * vcomm_Mde_D_Stat = VCOMM_MODE_RUN
 * All Faults = NO Fault or equivalent
 */
/* Reqs: SWREQ_1936, SWREQ_1192, SWREQ_1986, SWREQ_1989 */
void test_app_TAOP_ECM_STATUS_V_Pc_Actl_34_U_Meas_16_I_Meas_neg4_RUN_No_Fault_VCOMM_NO_REQUEST(void)
{
    uint8_t actual_tx_buffer[6] = {0};
    uint8_t expected_tx_buffer[6] =
    {
        0x00,         /* LIN / APINF Status */
        34,           /* Actual Speed */
        16,           /* Actual Voltage */
        4,            /* Actual Current */
        0x04,         /* V_D_Stat (2 bits), Mde_D_Stat (2 bits), Te_D_Falt (2 bits), I_D_Falt (2 bits) */
        0x00          /* U_D_Falt (2 bits), V_D_Falt (2 bits), Blk_B_Falt (1 bit), Prtct_B_Falt (1 bit) */
    };
    LinBusIf_eFrameIdAction_t result = LinBusIf_PIDACT_IGNORE;

    /* Ensure known test state */
    vcomm_V_Pc_Actl = 34;
    vcomm_U_Meas = 16;
    vcomm_I_Meas = 4;
    vcomm_V_D_Stat = VCOMM_NO_REQUEST;
    vcomm_Mde_D_Stat = VCOMM_MODE_RUN;
    vcomm_Te_D_Falt = VCOMM_TEMP_NO_FAULT;
    vcomm_I_D_Falt = VCOMM_CURRENT_NO_FAULT;
    vcomm_U_D_Falt = VCOMM_VOLTAGE_NO_FAULT;
    vcomm_V_D_Falt = VCOMM_SPEED_NO_FAULT;
    vcomm_Blk_B_Falt = false;
    vcomm_Prtct_B_Falt = false;
    sticky_lin_fault = 0;

    /* Setup expected call chain */
    FLT_MGR_Get_Lin_Fault_J2602_ExpectAndReturn(FLT_MGR_J2602_NO_ERROR);
    FLT_MGR_Is_Any_Fault_Active_ExpectAndReturn(false);
    FLT_MGR_IRQSTAT_Fault_Present_ExpectAndReturn(0);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(123456);

    /* Call function under test */
    result = app_TAOP_ECM_STATUS(NULL,           /* genericProtoEnvData, ignored */
                                 0x09,           /* frameID = 0x09 */
                                 NULL,           /* frameDesc, ignored */
                                 actual_tx_buffer, /* msgBuf, contains message data */
                                 6,              /* msgBufLen */
                                 NULL);          /* genericProtoCbCtxData, ignored */


    /* Verify test results */
    TEST_ASSERT_EQUAL(LinBusIf_PIDACT_TRANSMIT, result);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_tx_buffer, actual_tx_buffer, 6);
    TEST_ASSERT_EQUAL(VCOMM_NO_REQUEST, vcomm_V_D_Stat);

}

/**
 * @test Verifies that the function app_TAOP_ECM_STATUS will create a data buffer
 * with the proper values when the associated signals for the TAOP_ECM_STATUS frame
 * are set as follows:
 *
 * LIN / APINF Status = 0x00
 * vcomm_V_Pc_Actl = 34
 * vcomm_U_Meas = 16
 * vcomm_I_Meas = -4
 * vcomm_V_D_Stat = VCOMM_REQUEST_HONORED
 * vcomm_Mde_D_Stat = VCOMM_MODE_RUN
 * All Faults = only self protect fault
 */
/* Reqs: SWREQ_1936, SWREQ_1192, SWREQ_2048, SWREQ_2050, SWREQ_2055 */
void test_app_TAOP_ECM_STATUS_V_Pc_Actl_34_U_Meas_16_I_Meas_neg4_RUN_Self_Protect_Fault(void)
{
    uint8_t actual_tx_buffer[6] = {0};
    uint8_t expected_tx_buffer[6] =
    {
        0x06,         /* LIN / APINF Status, FLT_MGR_LIN_J2602_Err_T bits are "don't care", masked out below */
        34,           /* Actual Speed */
        16,           /* Actual Voltage */
        4,            /* Actual Current */
        0x05,         /* V_D_Stat (2 bits), Mde_D_Stat (2 bits), Te_D_Falt (2 bits), I_D_Falt (2 bits) */
        0x20          /* U_D_Falt (2 bits), V_D_Falt (2 bits), Blk_B_Falt (1 bit), Prtct_B_Falt (1 bit) */
    };
    LinBusIf_eFrameIdAction_t result = LinBusIf_PIDACT_IGNORE;

    /* Ensure known test state */
    vcomm_V_Pc_Actl = 34;
    vcomm_U_Meas = 16;
    vcomm_I_Meas = 4;
    vcomm_V_D_Stat = VCOMM_REQUEST_HONORED;
    vcomm_Mde_D_Stat = VCOMM_MODE_RUN;
    vcomm_Te_D_Falt = VCOMM_TEMP_NO_FAULT;
    vcomm_I_D_Falt = VCOMM_CURRENT_NO_FAULT;
    vcomm_U_D_Falt = VCOMM_VOLTAGE_NO_FAULT;
    vcomm_V_D_Falt = VCOMM_SPEED_NO_FAULT;
    vcomm_Blk_B_Falt = false;
    vcomm_Prtct_B_Falt = true;
    sticky_lin_fault = 0;

    /* Setup expected call chain */
    FLT_MGR_Get_Lin_Fault_J2602_ExpectAndReturn(FLT_MGR_J2602_NO_ERROR);
    FLT_MGR_Is_Any_Fault_Active_ExpectAndReturn(false);
    FLT_MGR_IRQSTAT_Fault_Present_ExpectAndReturn(0);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(123456);

    /* Call function under test */
    result = app_TAOP_ECM_STATUS(NULL,           /* genericProtoEnvData, ignored */
                                 0x09,           /* frameID = 0x09 */
                                 NULL,           /* frameDesc, ignored */
                                 actual_tx_buffer, /* msgBuf, contains message data */
                                 6,              /* msgBufLen */
                                 NULL);          /* genericProtoCbCtxData, ignored */


    /* Verify test results */
    actual_tx_buffer[0] &= 0x1Fu;  /* mask out FLT_MGR_LIN_J2602_Err_T, may be set from previous test, note that sticky_lin_fault is static in app_TAOP_ECM_STATUS() */
    TEST_ASSERT_EQUAL(LinBusIf_PIDACT_TRANSMIT, result);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_tx_buffer, actual_tx_buffer, 6);
    TEST_ASSERT_EQUAL(VCOMM_REQUEST_HONORED, vcomm_V_D_Stat);

}

/**
 * @test Verifies that the function app_TAOP_ECM_STATUS will create a data buffer
 * with the proper values when the associated signals for the TAOP_ECM_STATUS frame
 * are set as follows:
 *
 * LIN / APINF Status = 0x00
 * vcomm_V_Pc_Actl = 34
 * vcomm_U_Meas = 16
 * vcomm_I_Meas = -4
 * vcomm_V_D_Stat = VCOMM_REQUEST_HONORED
 * vcomm_Mde_D_Stat = VCOMM_MODE_RUN
 * All Faults = only blockage fault
 */
/* Reqs: SWREQ_1936, SWREQ_1192, SWREQ_2041, SWREQ_2043, SWREQ_2045 */
void test_app_TAOP_ECM_STATUS_V_Pc_Actl_34_U_Meas_16_I_Meas_neg4_RUN_Blockage(void)
{
    uint8_t actual_tx_buffer[6] = {0};
    uint8_t expected_tx_buffer[6] =
    {
        0x00,         /* LIN / APINF Status */
        34,           /* Actual Speed */
        16,           /* Actual Voltage */
        4,            /* Actual Current */
        0x05,         /* V_D_Stat (2 bits), Mde_D_Stat (2 bits), Te_D_Falt (2 bits), I_D_Falt (2 bits) */
        0x10          /* U_D_Falt (2 bits), V_D_Falt (2 bits), Blk_B_Falt (1 bit), Prtct_B_Falt (1 bit) */
    };
    LinBusIf_eFrameIdAction_t result = LinBusIf_PIDACT_IGNORE;

    /* Ensure known test state */
    vcomm_V_Pc_Actl = 34;
    vcomm_U_Meas = 16;
    vcomm_I_Meas = 4;
    vcomm_V_D_Stat = VCOMM_REQUEST_HONORED;
    vcomm_Mde_D_Stat = VCOMM_MODE_RUN;
    vcomm_Te_D_Falt = VCOMM_TEMP_NO_FAULT;
    vcomm_I_D_Falt = VCOMM_CURRENT_NO_FAULT;
    vcomm_U_D_Falt = VCOMM_VOLTAGE_NO_FAULT;
    vcomm_V_D_Falt = VCOMM_SPEED_NO_FAULT;
    vcomm_Blk_B_Falt = true;
    vcomm_Prtct_B_Falt = false;
    sticky_lin_fault = 0;

    /* Setup expected call chain */
    FLT_MGR_Get_Lin_Fault_J2602_ExpectAndReturn(FLT_MGR_J2602_NO_ERROR);
    FLT_MGR_Is_Any_Fault_Active_ExpectAndReturn(false);
    FLT_MGR_IRQSTAT_Fault_Present_ExpectAndReturn(0);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(123456);
    /* Call function under test */
    result = app_TAOP_ECM_STATUS(NULL,           /* genericProtoEnvData, ignored */
                                 0x09,           /* frameID = 0x09 */
                                 NULL,           /* frameDesc, ignored */
                                 actual_tx_buffer, /* msgBuf, contains message data */
                                 6,              /* msgBufLen */
                                 NULL);          /* genericProtoCbCtxData, ignored */


    /* Verify test results */
    TEST_ASSERT_EQUAL(LinBusIf_PIDACT_TRANSMIT, result);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_tx_buffer, actual_tx_buffer, 6);
    TEST_ASSERT_EQUAL(VCOMM_REQUEST_HONORED, vcomm_V_D_Stat);

}

/**
 * @test Verifies that the function app_TAOP_ECM_STATUS will create a data buffer
 * with the proper values when the associated signals for the TAOP_ECM_STATUS frame
 * are set as follows:
 *
 * LIN / APINF Status = 0x00
 * vcomm_V_Pc_Actl = 34
 * vcomm_U_Meas = 16
 * vcomm_I_Meas = -4
 * vcomm_V_D_Stat = VCOMM_REQUEST_HONORED
 * vcomm_Mde_D_Stat = VCOMM_MODE_RUN
 * All Faults = only overvoltage fault
 */
/* Reqs: SWREQ_1936, SWREQ_1192, SWREQ_2017, SWREQ_2019, SWREQ_2022*/
void test_app_TAOP_ECM_STATUS_V_Pc_Actl_34_U_Meas_16_I_Meas_neg4_RUN_Overvoltage(void)
{
    uint8_t actual_tx_buffer[6] = {0};
    uint8_t expected_tx_buffer[6] =
    {
        0x00,         /* LIN / APINF Status */
        34,           /* Actual Speed */
        16,           /* Actual Voltage */
        4,            /* Actual Current */
        0x05,         /* V_D_Stat (2 bits), Mde_D_Stat (2 bits), Te_D_Falt (2 bits), I_D_Falt (2 bits) */
        0x01          /* U_D_Falt (2 bits), V_D_Falt (2 bits), Blk_B_Falt (1 bit), Prtct_B_Falt (1 bit) */
    };
    LinBusIf_eFrameIdAction_t result = LinBusIf_PIDACT_IGNORE;

    /* Ensure known test state */
    vcomm_V_Pc_Actl = 34;
    vcomm_U_Meas = 16;
    vcomm_I_Meas = 4;
    vcomm_V_D_Stat = VCOMM_REQUEST_HONORED;
    vcomm_Mde_D_Stat = VCOMM_MODE_RUN;
    vcomm_Te_D_Falt = VCOMM_TEMP_NO_FAULT;
    vcomm_I_D_Falt = VCOMM_CURRENT_NO_FAULT;
    vcomm_U_D_Falt = VCOMM_OVER_VOLTAGE_FAULT;
    vcomm_V_D_Falt = VCOMM_SPEED_NO_FAULT;
    vcomm_Blk_B_Falt = false;
    vcomm_Prtct_B_Falt = false;
    sticky_lin_fault = 0;

    /* Setup expected call chain */
    FLT_MGR_Get_Lin_Fault_J2602_ExpectAndReturn(FLT_MGR_J2602_NO_ERROR);
    FLT_MGR_Is_Any_Fault_Active_ExpectAndReturn(false);
    FLT_MGR_IRQSTAT_Fault_Present_ExpectAndReturn(0);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(123456);

    /* Call function under test */
    result = app_TAOP_ECM_STATUS(NULL,           /* genericProtoEnvData, ignored */
                                 0x09,           /* frameID = 0x09 */
                                 NULL,           /* frameDesc, ignored */
                                 actual_tx_buffer, /* msgBuf, contains message data */
                                 6,              /* msgBufLen */
                                 NULL);          /* genericProtoCbCtxData, ignored */


    /* Verify test results */
    TEST_ASSERT_EQUAL(LinBusIf_PIDACT_TRANSMIT, result);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_tx_buffer, actual_tx_buffer, 6);
    TEST_ASSERT_EQUAL(VCOMM_REQUEST_HONORED, vcomm_V_D_Stat);

}

/**
 * @test Verifies that the function app_TAOP_ECM_STATUS will create a data buffer
 * with the proper values when the associated signals for the TAOP_ECM_STATUS frame
 * are set as follows:
 *
 * LIN / APINF Status = 0x00
 * vcomm_V_Pc_Actl = 34
 * vcomm_U_Meas = 16
 * vcomm_I_Meas = -4
 * vcomm_V_D_Stat = VCOMM_REQUEST_HONORED
 * vcomm_Mde_D_Stat = VCOMM_MODE_RUN
 * All Faults = only undervoltage fault
 */
/* Reqs: SWREQ_1936, SWREQ_1192, SWREQ_2022, SWREQ_2025, SWREQ_2028 */
void test_app_TAOP_ECM_STATUS_V_Pc_Actl_34_U_Meas_16_I_Meas_neg4_RUN_Undervoltage(void)
{
    uint8_t actual_tx_buffer[6] = {0};
    uint8_t expected_tx_buffer[6] =
    {
        0x00,         /* LIN / APINF Status */
        34,           /* Actual Speed */
        16,           /* Actual Voltage */
        4,            /* Actual Current */
        0x05,         /* V_D_Stat (2 bits), Mde_D_Stat (2 bits), Te_D_Falt (2 bits), I_D_Falt (2 bits) */
        0x02          /* U_D_Falt (2 bits), V_D_Falt (2 bits), Blk_B_Falt (1 bit), Prtct_B_Falt (1 bit) */
    };
    LinBusIf_eFrameIdAction_t result = LinBusIf_PIDACT_IGNORE;

    /* Ensure known test state */
    vcomm_V_Pc_Actl = 34;
    vcomm_U_Meas = 16;
    vcomm_I_Meas = 4;
    vcomm_V_D_Stat = VCOMM_REQUEST_HONORED;
    vcomm_Mde_D_Stat = VCOMM_MODE_RUN;
    vcomm_Te_D_Falt = VCOMM_TEMP_NO_FAULT;
    vcomm_I_D_Falt = VCOMM_CURRENT_NO_FAULT;
    vcomm_U_D_Falt = VCOMM_UNDER_VOLTAGE_FAULT;
    vcomm_V_D_Falt = VCOMM_SPEED_NO_FAULT;
    vcomm_Blk_B_Falt = false;
    vcomm_Prtct_B_Falt = false;
    sticky_lin_fault = 0;

    /* Setup expected call chain */
    FLT_MGR_Get_Lin_Fault_J2602_ExpectAndReturn(FLT_MGR_J2602_NO_ERROR);
    FLT_MGR_Is_Any_Fault_Active_ExpectAndReturn(false);
    FLT_MGR_IRQSTAT_Fault_Present_ExpectAndReturn(0);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(123456);

    /* Call function under test */
    result = app_TAOP_ECM_STATUS(NULL,           /* genericProtoEnvData, ignored */
                                 0x09,           /* frameID = 0x09 */
                                 NULL,           /* frameDesc, ignored */
                                 actual_tx_buffer, /* msgBuf, contains message data */
                                 6,              /* msgBufLen */
                                 NULL);          /* genericProtoCbCtxData, ignored */


    /* Verify test results */
    TEST_ASSERT_EQUAL(LinBusIf_PIDACT_TRANSMIT, result);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_tx_buffer, actual_tx_buffer, 6);
    TEST_ASSERT_EQUAL(VCOMM_REQUEST_HONORED, vcomm_V_D_Stat);

}

/**
 * @test Verifies that the function app_TAOP_ECM_STATUS will create a data buffer
 * with the proper values when the associated signals for the TAOP_ECM_STATUS frame
 * are set as follows:
 *
 * LIN / APINF Status = 0x00
 * vcomm_V_Pc_Actl = 34
 * vcomm_U_Meas = 16
 * vcomm_I_Meas = -4
 * vcomm_V_D_Stat = VCOMM_REQUEST_HONORED
 * vcomm_Mde_D_Stat = VCOMM_MODE_RUN
 * All Faults = only overtemperature fault
 */
/* Reqs: SWREQ_1936, SWREQ_1192, SWREQ_1994 SWREQ_1996 SWREQ_1999 */
void test_app_TAOP_ECM_STATUS_V_Pc_Actl_34_U_Meas_16_I_Meas_neg4_RUN_Overtemperature(void)
{
    uint8_t actual_tx_buffer[6] = {0};
    uint8_t expected_tx_buffer[6] =
    {
        0x00,         /* LIN / APINF Status */
        34,           /* Actual Speed */
        16,           /* Actual Voltage */
        4,            /* Actual Current */
        0x15,         /* V_D_Stat (2 bits), Mde_D_Stat (2 bits), Te_D_Falt (2 bits), I_D_Falt (2 bits) */
        0x00          /* U_D_Falt (2 bits), V_D_Falt (2 bits), Blk_B_Falt (1 bit), Prtct_B_Falt (1 bit) */
    };
    LinBusIf_eFrameIdAction_t result = LinBusIf_PIDACT_IGNORE;

    /* Ensure known test state */
    vcomm_V_Pc_Actl = 34;
    vcomm_U_Meas = 16;
    vcomm_I_Meas = 4;
    vcomm_V_D_Stat = VCOMM_REQUEST_HONORED;
    vcomm_Mde_D_Stat = VCOMM_MODE_RUN;
    vcomm_Te_D_Falt = VCOMM_OVER_TEMP_FAULT;
    vcomm_I_D_Falt = VCOMM_CURRENT_NO_FAULT;
    vcomm_U_D_Falt = VCOMM_VOLTAGE_NO_FAULT;
    vcomm_V_D_Falt = VCOMM_SPEED_NO_FAULT;
    vcomm_Blk_B_Falt = false;
    vcomm_Prtct_B_Falt = false;
    sticky_lin_fault = 0;

    /* Setup expected call chain */
    FLT_MGR_Get_Lin_Fault_J2602_ExpectAndReturn(FLT_MGR_J2602_NO_ERROR);
    FLT_MGR_Is_Any_Fault_Active_ExpectAndReturn(false);
    FLT_MGR_IRQSTAT_Fault_Present_ExpectAndReturn(false);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(123456);

    /* Call function under test */
    result = app_TAOP_ECM_STATUS(NULL,           /* genericProtoEnvData, ignored */
                                 0x09,           /* frameID = 0x09 */
                                 NULL,           /* frameDesc, ignored */
                                 actual_tx_buffer, /* msgBuf, contains message data */
                                 6,              /* msgBufLen */
                                 NULL);          /* genericProtoCbCtxData, ignored */


    /* Verify test results */
    TEST_ASSERT_EQUAL(LinBusIf_PIDACT_TRANSMIT, result);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_tx_buffer, actual_tx_buffer, 6);
    TEST_ASSERT_EQUAL(VCOMM_REQUEST_HONORED, vcomm_V_D_Stat);

}

/**
 * @test Verifies that the function app_TAOP_ECM_STATUS will create a data buffer
 * with the proper values when the associated signals for the TAOP_ECM_STATUS frame
 * are set as follows:
 *
 * LIN / APINF Status = 0x00
 * vcomm_V_Pc_Actl = 34
 * vcomm_U_Meas = 16
 * vcomm_I_Meas = -4
 * vcomm_V_D_Stat = VCOMM_REQUEST_HONORED
 * vcomm_Mde_D_Stat = VCOMM_MODE_RUN
 * All Faults = only undertemperature fault
 */
/* Reqs: SWREQ_1936, SWREQ_1192 */
void test_app_TAOP_ECM_STATUS_V_Pc_Actl_34_U_Meas_16_I_Meas_neg4_RUN_Undertemperature(void)
{
    uint8_t actual_tx_buffer[6] = {0};
    uint8_t expected_tx_buffer[6] =
    {
        0x00,         /* LIN / APINF Status */
        34,           /* Actual Speed */
        16,           /* Actual Voltage */
        4,            /* Actual Current */
        0x25,         /* V_D_Stat (2 bits), Mde_D_Stat (2 bits), Te_D_Falt (2 bits), I_D_Falt (2 bits) */
        0x00          /* U_D_Falt (2 bits), V_D_Falt (2 bits), Blk_B_Falt (1 bit), Prtct_B_Falt (1 bit) */
    };
    LinBusIf_eFrameIdAction_t result = LinBusIf_PIDACT_IGNORE;

    /* Ensure known test state */
    vcomm_V_Pc_Actl = 34;
    vcomm_U_Meas = 16;
    vcomm_I_Meas = 4;
    vcomm_V_D_Stat = VCOMM_REQUEST_HONORED;
    vcomm_Mde_D_Stat = VCOMM_MODE_RUN;
    vcomm_Te_D_Falt = VCOMM_UNDER_TEMP_FAULT;
    vcomm_I_D_Falt = VCOMM_CURRENT_NO_FAULT;
    vcomm_U_D_Falt = VCOMM_VOLTAGE_NO_FAULT;
    vcomm_V_D_Falt = VCOMM_SPEED_NO_FAULT;
    vcomm_Blk_B_Falt = false;
    vcomm_Prtct_B_Falt = false;
    sticky_lin_fault = 0;

    /* Setup expected call chain */
    FLT_MGR_Get_Lin_Fault_J2602_ExpectAndReturn(FLT_MGR_J2602_NO_ERROR);
    FLT_MGR_Is_Any_Fault_Active_ExpectAndReturn(false);
    FLT_MGR_IRQSTAT_Fault_Present_ExpectAndReturn(false);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(123456);

    /* Call function under test */
    result = app_TAOP_ECM_STATUS(NULL,           /* genericProtoEnvData, ignored */
                                 0x09,           /* frameID = 0x09 */
                                 NULL,           /* frameDesc, ignored */
                                 actual_tx_buffer, /* msgBuf, contains message data */
                                 6,              /* msgBufLen */
                                 NULL);          /* genericProtoCbCtxData, ignored */


    /* Verify test results */
    TEST_ASSERT_EQUAL(LinBusIf_PIDACT_TRANSMIT, result);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_tx_buffer, actual_tx_buffer, 6);
    TEST_ASSERT_EQUAL(VCOMM_REQUEST_HONORED, vcomm_V_D_Stat);

}

/**
 * @test Verifies that the function app_TAOP_ECM_STATUS will create a data buffer
 * with the proper values when the associated signals for the TAOP_ECM_STATUS frame
 * are set as follows:
 *
 * LIN / APINF Status = 0x04
 * vcomm_V_Pc_Actl = 34
 * vcomm_U_Meas = 16
 * vcomm_I_Meas = -4
 * vcomm_V_D_Stat = VCOMM_REQUEST_HONORED
 * vcomm_Mde_D_Stat = VCOMM_MODE_RUN
 * All Faults = only undertemperature fault
 */
/* Reqs: SWREQ_1936, SWREQ_1192 */
void test_app_TAOP_ECM_STATUS_Self_Protect_true(void)
{
    uint8_t actual_tx_buffer[6] = {0};
    uint8_t expected_tx_buffer[6] =
    {
        0x06,         /* LIN / APINF Status */
        34,           /* Actual Speed */
        16,           /* Actual Voltage */
        4,            /* Actual Current */
        0x25,         /* V_D_Stat (2 bits), Mde_D_Stat (2 bits), Te_D_Falt (2 bits), I_D_Falt (2 bits) */
        0x20          /* U_D_Falt (2 bits), V_D_Falt (2 bits), Blk_B_Falt (1 bit), Prtct_B_Falt (1 bit) */
    };
    LinBusIf_eFrameIdAction_t result = LinBusIf_PIDACT_IGNORE;

    /* Ensure known test state */
    vcomm_V_Pc_Actl = 34;
    vcomm_U_Meas = 16;
    vcomm_I_Meas = 4;
    vcomm_V_D_Stat = VCOMM_REQUEST_HONORED;
    vcomm_Mde_D_Stat = VCOMM_MODE_RUN;
    vcomm_Te_D_Falt = VCOMM_UNDER_TEMP_FAULT;
    vcomm_I_D_Falt = VCOMM_CURRENT_NO_FAULT;
    vcomm_U_D_Falt = VCOMM_VOLTAGE_NO_FAULT;
    vcomm_V_D_Falt = VCOMM_SPEED_NO_FAULT;
    vcomm_Blk_B_Falt = false;
    vcomm_Prtct_B_Falt = true;
    sticky_lin_fault = 0;

    /* Setup expected call chain */
    FLT_MGR_Get_Lin_Fault_J2602_ExpectAndReturn(FLT_MGR_J2602_NO_ERROR);
    FLT_MGR_Is_Any_Fault_Active_ExpectAndReturn(false);
    FLT_MGR_IRQSTAT_Fault_Present_ExpectAndReturn(false);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(123456);

    /* Call function under test */
    result = app_TAOP_ECM_STATUS(NULL,           /* genericProtoEnvData, ignored */
                                 0x09,           /* frameID = 0x09 */
                                 NULL,           /* frameDesc, ignored */
                                 actual_tx_buffer, /* msgBuf, contains message data */
                                 6,              /* msgBufLen */
                                 NULL);          /* genericProtoCbCtxData, ignored */


    /* Verify test results */
    TEST_ASSERT_EQUAL(LinBusIf_PIDACT_TRANSMIT, result);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_tx_buffer, actual_tx_buffer, 6);
    TEST_ASSERT_EQUAL(VCOMM_REQUEST_HONORED, vcomm_V_D_Stat);

}

/**
 * @test Verifies that the function app_TAOP_ECM_STATUS will create a data buffer
 * with the proper values when the associated signals for the TAOP_ECM_STATUS frame
 * are set as follows:
 *
 * LIN / APINF Status = 0x02
 * vcomm_V_Pc_Actl = 34
 * vcomm_U_Meas = 16
 * vcomm_I_Meas = -4
 * vcomm_V_D_Stat = VCOMM_REQUEST_HONORED
 * vcomm_Mde_D_Stat = VCOMM_MODE_RUN
 * All Faults = only undertemperature fault
 */
/* Reqs: SWREQ_1949 */
void test_app_TAOP_ECM_STATUS_A_fault_is_active_true(void)
{
    uint8_t actual_tx_buffer[6] = {0};
    uint8_t expected_tx_buffer[6] =
    {
        0x02,         /* LIN / APINF Status */
        34,           /* Actual Speed */
        16,           /* Actual Voltage */
        4,            /* Actual Current */
        0x25,         /* V_D_Stat (2 bits), Mde_D_Stat (2 bits), Te_D_Falt (2 bits), I_D_Falt (2 bits) */
        0x00          /* U_D_Falt (2 bits), V_D_Falt (2 bits), Blk_B_Falt (1 bit), Prtct_B_Falt (1 bit) */
    };
    LinBusIf_eFrameIdAction_t result = LinBusIf_PIDACT_IGNORE;

    /* Ensure known test state */
    vcomm_V_Pc_Actl = 34;
    vcomm_U_Meas = 16;
    vcomm_I_Meas = 4;
    vcomm_V_D_Stat = VCOMM_REQUEST_HONORED;
    vcomm_Mde_D_Stat = VCOMM_MODE_RUN;
    vcomm_Te_D_Falt = VCOMM_UNDER_TEMP_FAULT;
    vcomm_I_D_Falt = VCOMM_CURRENT_NO_FAULT;
    vcomm_U_D_Falt = VCOMM_VOLTAGE_NO_FAULT;
    vcomm_V_D_Falt = VCOMM_SPEED_NO_FAULT;
    vcomm_Blk_B_Falt = false;
    vcomm_Prtct_B_Falt = false;
    sticky_lin_fault = 0;

    /* Setup expected call chain */
    FLT_MGR_Get_Lin_Fault_J2602_ExpectAndReturn(FLT_MGR_J2602_NO_ERROR);
    FLT_MGR_Is_Any_Fault_Active_ExpectAndReturn(true);
    FLT_MGR_IRQSTAT_Fault_Present_ExpectAndReturn(false);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(123456);

    /* Call function under test */
    result = app_TAOP_ECM_STATUS(NULL,           /* genericProtoEnvData, ignored */
                                 0x09,           /* frameID = 0x09 */
                                 NULL,           /* frameDesc, ignored */
                                 actual_tx_buffer, /* msgBuf, contains message data */
                                 6,              /* msgBufLen */
                                 NULL);          /* genericProtoCbCtxData, ignored */


    /* Verify test results */
    TEST_ASSERT_EQUAL(LinBusIf_PIDACT_TRANSMIT, result);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_tx_buffer, actual_tx_buffer, 6);
    TEST_ASSERT_EQUAL(VCOMM_REQUEST_HONORED, vcomm_V_D_Stat);

}

/**
 * @test Verifies that the function app_TAOP_ECM_STATUS will create a data buffer
 * with the proper values when the associated signals for the TAOP_ECM_STATUS frame
 * are set as follows:
 *
 * LIN / APINF Status = 0x80
 * vcomm_V_Pc_Actl = 34
 * vcomm_U_Meas = 16
 * vcomm_I_Meas = -4
 * vcomm_V_D_Stat = VCOMM_REQUEST_HONORED
 * vcomm_Mde_D_Stat = VCOMM_MODE_RUN
 * All Faults = only undertemperature fault
 */
/* Reqs: SWREQ_1952 */
void test_app_TAOP_ECM_STATUS_Data_Error(void)
{
    uint8_t actual_tx_buffer[6] = {0};
    uint8_t expected_tx_buffer[6] =
    {
        0x80,         /* LIN Status */
        34,           /* Actual Speed */
        16,           /* Actual Voltage */
        4,            /* Actual Current */
        0x26,         /* V_D_Stat (2 bits), Mde_D_Stat (2 bits), Te_D_Falt (2 bits), I_D_Falt (2 bits) */
        0x00          /* U_D_Falt (2 bits), V_D_Falt (2 bits), Blk_B_Falt (1 bit), Prtct_B_Falt (1 bit) */
    };
    LinBusIf_eFrameIdAction_t result = LinBusIf_PIDACT_IGNORE;

    /* Ensure known test state */
    vcomm_V_Pc_Actl = 34;
    vcomm_U_Meas = 16;
    vcomm_I_Meas = 4;
    vcomm_V_D_Stat = VCOMM_REQUEST_HONORED;
    vcomm_Mde_D_Stat = VCOMM_MODE_RUN;
    vcomm_Te_D_Falt = VCOMM_UNDER_TEMP_FAULT;
    vcomm_I_D_Falt = VCOMM_CURRENT_NO_FAULT;
    vcomm_U_D_Falt = VCOMM_VOLTAGE_NO_FAULT;
    vcomm_V_D_Falt = VCOMM_SPEED_NO_FAULT;
    vcomm_Blk_B_Falt = false;
    vcomm_Prtct_B_Falt = false;
    sticky_lin_fault = 0;

    /* Setup expected call chain */
    FLT_MGR_Get_Lin_Fault_J2602_ExpectAndReturn(FLT_MGR_J2602_DATA_ERROR);
    FLT_MGR_Is_Any_Fault_Active_ExpectAndReturn(false);
    FLT_MGR_IRQSTAT_Fault_Present_ExpectAndReturn(false);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(123456);

    /* Call function under test */
    result = app_TAOP_ECM_STATUS(NULL,           /* genericProtoEnvData, ignored */
                                 0x09,           /* frameID = 0x09 */
                                 NULL,           /* frameDesc, ignored */
                                 actual_tx_buffer, /* msgBuf, contains message data */
                                 6,              /* msgBufLen */
                                 NULL);          /* genericProtoCbCtxData, ignored */


    /* Verify test results */
    TEST_ASSERT_EQUAL(LinBusIf_PIDACT_TRANSMIT, result);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_tx_buffer, actual_tx_buffer, 6);
    TEST_ASSERT_EQUAL(VCOMM_REQUEST_HONORED, vcomm_V_D_Stat);

}

/**
 * @test Verifies that the function app_TAOP_ECM_STATUS will create a data buffer
 * with the proper values when the associated signals for the TAOP_ECM_STATUS frame
 * are set as follows:
 *
 * LIN / APINF Status = 0xA0
 * vcomm_V_Pc_Actl = 34
 * vcomm_U_Meas = 16
 * vcomm_I_Meas = -4
 * vcomm_V_D_Stat = VCOMM_REQUEST_HONORED
 * vcomm_Mde_D_Stat = VCOMM_MODE_RUN
 * All Faults = only undertemperature fault
 */
/* Reqs: SWREQ_1958 */
void test_app_TAOP_ECM_STATUS_Checksum_Error(void)
{
    uint8_t actual_tx_buffer[6] = {0};
    uint8_t expected_tx_buffer[6] =
    {
        0xA0,         /* LIN / APINF Status */
        34,           /* Actual Speed */
        16,           /* Actual Voltage */
        4,            /* Actual Current */
        0x26,         /* V_D_Stat (2 bits), Mde_D_Stat (2 bits), Te_D_Falt (2 bits), I_D_Falt (2 bits) */
        0x00          /* U_D_Falt (2 bits), V_D_Falt (2 bits), Blk_B_Falt (1 bit), Prtct_B_Falt (1 bit) */
    };
    LinBusIf_eFrameIdAction_t result = LinBusIf_PIDACT_IGNORE;

    /* Ensure known test state */
    vcomm_V_Pc_Actl = 34;
    vcomm_U_Meas = 16;
    vcomm_I_Meas = 4;
    vcomm_V_D_Stat = VCOMM_REQUEST_HONORED;
    vcomm_Mde_D_Stat = VCOMM_MODE_RUN;
    vcomm_Te_D_Falt = VCOMM_UNDER_TEMP_FAULT;
    vcomm_I_D_Falt = VCOMM_CURRENT_NO_FAULT;
    vcomm_U_D_Falt = VCOMM_VOLTAGE_NO_FAULT;
    vcomm_V_D_Falt = VCOMM_SPEED_NO_FAULT;
    vcomm_Blk_B_Falt = false;
    vcomm_Prtct_B_Falt = false;
    sticky_lin_fault = 0;

    /* Setup expected call chain */
    FLT_MGR_Get_Lin_Fault_J2602_ExpectAndReturn(FLT_MGR_J2602_CHECKSUM_ERROR);
    FLT_MGR_Is_Any_Fault_Active_ExpectAndReturn(false);
    FLT_MGR_IRQSTAT_Fault_Present_ExpectAndReturn(false);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(123456);
    /* Call function under test */
    result = app_TAOP_ECM_STATUS(NULL,           /* genericProtoEnvData, ignored */
                                 0x09,           /* frameID = 0x09 */
                                 NULL,           /* frameDesc, ignored */
                                 actual_tx_buffer, /* msgBuf, contains message data */
                                 6,              /* msgBufLen */
                                 NULL);          /* genericProtoCbCtxData, ignored */


    /* Verify test results */
    TEST_ASSERT_EQUAL(LinBusIf_PIDACT_TRANSMIT, result);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_tx_buffer, actual_tx_buffer, 6);
    TEST_ASSERT_EQUAL(VCOMM_REQUEST_HONORED, vcomm_V_D_Stat);

}

/**
 * @test Verifies that the function app_TAOP_ECM_STATUS will create a data buffer
 * with the proper values when the associated signals for the TAOP_ECM_STATUS frame
 * are set as follows:
 *
 * LIN / APINF Status = 0xC0
 * vcomm_V_Pc_Actl = 34
 * vcomm_U_Meas = 16
 * vcomm_I_Meas = -4
 * vcomm_V_D_Stat = VCOMM_REQUEST_HONORED
 * vcomm_Mde_D_Stat = VCOMM_MODE_RUN
 * All Faults = only undertemperature fault
 */
/* Reqs: SWREQ_1964, SWREQ_1967 */
void test_app_TAOP_ECM_STATUS_Framing_Error(void)
{
    uint8_t actual_tx_buffer[6] = {0};
    uint8_t expected_tx_buffer[6] =
    {
        0xC0,         /* LIN / APINF Status */
        34,           /* Actual Speed */
        16,           /* Actual Voltage */
        4,            /* Actual Current */
        0x26,         /* V_D_Stat (2 bits), Mde_D_Stat (2 bits), Te_D_Falt (2 bits), I_D_Falt (2 bits) */
        0x00          /* U_D_Falt (2 bits), V_D_Falt (2 bits), Blk_B_Falt (1 bit), Prtct_B_Falt (1 bit) */
    };
    LinBusIf_eFrameIdAction_t result = LinBusIf_PIDACT_IGNORE;

    /* Ensure known test state */
    vcomm_V_Pc_Actl = 34;
    vcomm_U_Meas = 16;
    vcomm_I_Meas = 4;
    vcomm_V_D_Stat = VCOMM_REQUEST_HONORED;
    vcomm_Mde_D_Stat = VCOMM_MODE_RUN;
    vcomm_Te_D_Falt = VCOMM_UNDER_TEMP_FAULT;
    vcomm_I_D_Falt = VCOMM_CURRENT_NO_FAULT;
    vcomm_U_D_Falt = VCOMM_VOLTAGE_NO_FAULT;
    vcomm_V_D_Falt = VCOMM_SPEED_NO_FAULT;
    vcomm_Blk_B_Falt = false;
    vcomm_Prtct_B_Falt = false;
    sticky_lin_fault = 0;

    /* Setup expected call chain */
    FLT_MGR_Get_Lin_Fault_J2602_ExpectAndReturn(FLT_MGR_J2602_FRAMING_ERROR);
    FLT_MGR_Is_Any_Fault_Active_ExpectAndReturn(false);
    FLT_MGR_IRQSTAT_Fault_Present_ExpectAndReturn(false);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(123456);

    /* Call function under test */
    result = app_TAOP_ECM_STATUS(NULL,           /* genericProtoEnvData, ignored */
                                 0x09,           /* frameID = 0x09 */
                                 NULL,           /* frameDesc, ignored */
                                 actual_tx_buffer, /* msgBuf, contains message data */
                                 6,              /* msgBufLen */
                                 NULL);          /* genericProtoCbCtxData, ignored */


    /* Verify test results */
    TEST_ASSERT_EQUAL(LinBusIf_PIDACT_TRANSMIT, result);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_tx_buffer, actual_tx_buffer, 6);
    TEST_ASSERT_EQUAL(VCOMM_REQUEST_HONORED, vcomm_V_D_Stat);

}

/**
 * @test Verifies that the function app_TAOP_ECM_STATUS will create a data buffer
 * with the proper values when the associated signals for the TAOP_ECM_STATUS frame
 * are set as follows:
 *
 * LIN / APINF Status = 0xE0
 * vcomm_V_Pc_Actl = 34
 * vcomm_U_Meas = 16
 * vcomm_I_Meas = -4
 * vcomm_V_D_Stat = VCOMM_REQUEST_HONORED
 * vcomm_Mde_D_Stat = VCOMM_MODE_RUN
 * All Faults = only undertemperature fault
 */
/* Reqs: SWREQ_1970, SWREQ_1973 */
void test_app_TAOP_ECM_STATUS_Parity_Error(void)
{
    uint8_t actual_tx_buffer[6] = {0};
    uint8_t expected_tx_buffer[6] =
    {
        0xE0,         /* LIN / APINF Status */
        34,           /* Actual Speed */
        16,           /* Actual Voltage */
        4,            /* Actual Current */
        0x26,         /* V_D_Stat (2 bits), Mde_D_Stat (2 bits), Te_D_Falt (2 bits), I_D_Falt (2 bits) */
        0x00          /* U_D_Falt (2 bits), V_D_Falt (2 bits), Blk_B_Falt (1 bit), Prtct_B_Falt (1 bit) */
    };
    LinBusIf_eFrameIdAction_t result = LinBusIf_PIDACT_IGNORE;

    /* Ensure known test state */
    vcomm_V_Pc_Actl = 34;
    vcomm_U_Meas = 16;
    vcomm_I_Meas = 4;
    vcomm_V_D_Stat = VCOMM_REQUEST_HONORED;
    vcomm_Mde_D_Stat = VCOMM_MODE_RUN;
    vcomm_Te_D_Falt = VCOMM_UNDER_TEMP_FAULT;
    vcomm_I_D_Falt = VCOMM_CURRENT_NO_FAULT;
    vcomm_U_D_Falt = VCOMM_VOLTAGE_NO_FAULT;
    vcomm_V_D_Falt = VCOMM_SPEED_NO_FAULT;
    vcomm_Blk_B_Falt = false;
    vcomm_Prtct_B_Falt = false;
    sticky_lin_fault = 0;

    /* Setup expected call chain */
    FLT_MGR_Get_Lin_Fault_J2602_ExpectAndReturn(FLT_MGR_J2602_ID_PARITY_ERROR);
    FLT_MGR_Is_Any_Fault_Active_ExpectAndReturn(false);
    FLT_MGR_IRQSTAT_Fault_Present_ExpectAndReturn(false);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(123456);
    
    /* Call function under test */
    result = app_TAOP_ECM_STATUS(NULL,           /* genericProtoEnvData, ignored */
                                 0x09,           /* frameID = 0x09 */
                                 NULL,           /* frameDesc, ignored */
                                 actual_tx_buffer, /* msgBuf, contains message data */
                                 6,              /* msgBufLen */
                                 NULL);          /* genericProtoCbCtxData, ignored */


    /* Verify test results */
    TEST_ASSERT_EQUAL(LinBusIf_PIDACT_TRANSMIT, result);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_tx_buffer, actual_tx_buffer, 6);
    TEST_ASSERT_EQUAL(VCOMM_REQUEST_HONORED, vcomm_V_D_Stat);
}

/**
 * @test Verifies that the function app_TAOP_ECM_STATUS will create a data buffer
 * with the proper values when the associated signals for the TAOP_ECM_STATUS frame
 * are set as follows:
 *
 * LIN / APINF Status = 0xE0
 * vcomm_V_Pc_Actl = 0
 * vcomm_U_Meas = 16
 * vcomm_I_Meas = -4
 * vcomm_V_D_Stat = VCOMM_REQUEST_MISSING
 * vcomm_Mde_D_Stat = VCOMM_MODE_IDLE
 * All Faults = no faults
 *
 * This test case is specifically for the case where there is a missing DEMAND message and also a LIN
 * error present. In this case the V_D_Stat will reflect the missing DEMAND message since that is the
 * highest priority.
 *
 */
/* Reqs:  */
void test_app_TAOP_ECM_STATUS_overrides_V_D_Stat_to_NO_REQUEST_when_DEMAND_missing_and_LIN_error_present(void)
{
    uint8_t actual_tx_buffer[6] = {0};
    uint8_t expected_tx_buffer[6] =
    {
        0xE0,         /* LIN / APINF Status */
        0,            /* Actual Speed */
        16,           /* Actual Voltage */
        4,            /* Actual Current */
        0x00,         /* V_D_Stat (2 bits), Mde_D_Stat (2 bits), Te_D_Falt (2 bits), I_D_Falt (2 bits) */
        0x00          /* U_D_Falt (2 bits), V_D_Falt (2 bits), Blk_B_Falt (1 bit), Prtct_B_Falt (1 bit) */
    };
    LinBusIf_eFrameIdAction_t result = LinBusIf_PIDACT_IGNORE;

    /* Ensure known test state */
    vcomm_V_Pc_Actl = 0;
    vcomm_U_Meas = 16;
    vcomm_I_Meas = 4;
    vcomm_V_D_Stat = VCOMM_REQUEST_DENIED;
    vcomm_Mde_D_Stat = VCOMM_MODE_IDLE;
    vcomm_Te_D_Falt = VCOMM_TEMP_NO_FAULT;
    vcomm_I_D_Falt = VCOMM_CURRENT_NO_FAULT;
    vcomm_U_D_Falt = VCOMM_VOLTAGE_NO_FAULT;
    vcomm_V_D_Falt = VCOMM_SPEED_NO_FAULT;
    vcomm_Blk_B_Falt = false;
    vcomm_Prtct_B_Falt = false;
    sticky_lin_fault = 0;

    vcomm_Demand_Missing = true;

    /* Setup expected call chain */
    FLT_MGR_Get_Lin_Fault_J2602_ExpectAndReturn(FLT_MGR_J2602_ID_PARITY_ERROR);
    FLT_MGR_Is_Any_Fault_Active_ExpectAndReturn(false);
    FLT_MGR_IRQSTAT_Fault_Present_ExpectAndReturn(false);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(123456);

    /* Call function under test */
    result = app_TAOP_ECM_STATUS(NULL,           /* genericProtoEnvData, ignored */
                                 0x09,           /* frameID = 0x09 */
                                 NULL,           /* frameDesc, ignored */
                                 actual_tx_buffer, /* msgBuf, contains message data */
                                 6,              /* msgBufLen */
                                 NULL);          /* genericProtoCbCtxData, ignored */


    /* Verify test results */
    TEST_ASSERT_EQUAL(LinBusIf_PIDACT_TRANSMIT, result);
    TEST_ASSERT_EQUAL_HEX8_ARRAY(expected_tx_buffer, actual_tx_buffer, 6);
    TEST_ASSERT_EQUAL(VCOMM_REQUEST_DENIED, vcomm_V_D_Stat);

}

/**
 * @test Verifies that the function app_TAOP_ECM_STATUS will create a data buffer
 * with the proper values when the associated signals for the TAOP_ECM_STATUS frame
 * are set as follows:
 *
 * LIN / APINF Status = 0xE0
 * vcomm_V_Pc_Actl = 0
 * vcomm_U_Meas = 16
 * vcomm_I_Meas = -4
 * vcomm_V_D_Stat = VCOMM_REQUEST_MISSING
 * vcomm_Mde_D_Stat = VCOMM_MODE_IDLE
 * All Faults = no faults
 *
 * This test case is specifically for the case where there is a missing DEMAND message and no other error.
 * In this case the V_D_Stat will reflect the missing DEMAND message since that is the highest priority.
 *
 */
/* Reqs:  */
void test_app_TAOP_ECM_STATUS_overrides_V_D_Stat_to_NO_REQUEST_when_DEMAND_missing_and_no_other_errors(void)
{
    uint8_t actual_tx_buffer[6] = {0};
    uint8_t expected_tx_buffer[6] =
    {
        0x00,         /* LIN / APINF Status */
        0,            /* Actual Speed */
        16,           /* Actual Voltage */
        4,            /* Actual Current */
        0x00,         /* V_D_Stat (2 bits), Mde_D_Stat (2 bits), Te_D_Falt (2 bits), I_D_Falt (2 bits) */
        0x00          /* U_D_Falt (2 bits), V_D_Falt (2 bits), Blk_B_Falt (1 bit), Prtct_B_Falt (1 bit) */
    };
    LinBusIf_eFrameIdAction_t result = LinBusIf_PIDACT_IGNORE;

    /* Ensure known test state */
    vcomm_V_Pc_Actl = 0;
    vcomm_U_Meas = 16;
    vcomm_I_Meas = 4;
    vcomm_V_D_Stat = VCOMM_REQUEST_DENIED;
    vcomm_Mde_D_Stat = VCOMM_MODE_IDLE;
    vcomm_Te_D_Falt = VCOMM_TEMP_NO_FAULT;
    vcomm_I_D_Falt = VCOMM_CURRENT_NO_FAULT;
    vcomm_U_D_Falt = VCOMM_VOLTAGE_NO_FAULT;
    vcomm_V_D_Falt = VCOMM_SPEED_NO_FAULT;
    vcomm_Blk_B_Falt = false;
    vcomm_Prtct_B_Falt = false;
    sticky_lin_fault = 0;

    vcomm_Demand_Missing = true;

    /* Setup expected call chain */
    FLT_MGR_Get_Lin_Fault_J2602_ExpectAndReturn(FLT_MGR_J2602_NO_ERROR);
    FLT_MGR_Is_Any_Fault_Active_ExpectAndReturn(false);
    FLT_MGR_IRQSTAT_Fault_Present_ExpectAndReturn(false);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(123456);

    /* Call function under test */
    result = app_TAOP_ECM_STATUS(NULL,           /* genericProtoEnvData, ignored */
                                 0x09,           /* frameID = 0x09 */
                                 NULL,           /* frameDesc, ignored */
                                 actual_tx_buffer, /* msgBuf, contains message data */
                                 6,              /* msgBufLen */
                                 NULL);          /* genericProtoCbCtxData, ignored */


    /* Verify test results */
    TEST_ASSERT_EQUAL(LinBusIf_PIDACT_TRANSMIT, result);
    TEST_ASSERT_EQUAL_HEX8_ARRAY(expected_tx_buffer, actual_tx_buffer, 6);
    TEST_ASSERT_EQUAL(VCOMM_REQUEST_DENIED, vcomm_V_D_Stat);

}

/**
 * @test Verifies that the function app_TAOP_ECM_STATUS will create a data buffer
 * with the proper values when the associated signals for the TAOP_ECM_STATUS frame
 * are set as follows:
 *
 * LIN / APINF Status = 0xE0
 * vcomm_V_Pc_Actl = 0
 * vcomm_U_Meas = 16
 * vcomm_I_Meas = -4
 * vcomm_V_D_Stat = VCOMM_REQUEST_MISSING
 * vcomm_Mde_D_Stat = VCOMM_MODE_IDLE
 * All Faults = no faults
 *
 * This test case is specifically for the case where the DEMAND message is present but the request was denied.
 * In this case the V_D_Stat will reflect the normal V_D_Stat value as set by the application.
 *
 */
/* Reqs:  */
void test_app_TAOP_ECM_STATUS_sets_V_D_Stat_to_normal_V_D_Stat_when_DEMAND_present_and_no_LIN_fault(void)
{
    uint8_t actual_tx_buffer[6] = {0};
    uint8_t expected_tx_buffer[6] =
    {
        0x00,         /* LIN / APINF Status */
        0,            /* Actual Speed */
        16,           /* Actual Voltage */
        4,            /* Actual Current */
        0x02,         /* V_D_Stat (2 bits), Mde_D_Stat (2 bits), Te_D_Falt (2 bits), I_D_Falt (2 bits) */
        0x00          /* U_D_Falt (2 bits), V_D_Falt (2 bits), Blk_B_Falt (1 bit), Prtct_B_Falt (1 bit) */
    };
    LinBusIf_eFrameIdAction_t result = LinBusIf_PIDACT_IGNORE;

    /* Ensure known test state */
    vcomm_V_Pc_Actl = 0;
    vcomm_U_Meas = 16;
    vcomm_I_Meas = 4;
    vcomm_V_D_Stat = VCOMM_REQUEST_DENIED;
    vcomm_Mde_D_Stat = VCOMM_MODE_IDLE;
    vcomm_Te_D_Falt = VCOMM_TEMP_NO_FAULT;
    vcomm_I_D_Falt = VCOMM_CURRENT_NO_FAULT;
    vcomm_U_D_Falt = VCOMM_VOLTAGE_NO_FAULT;
    vcomm_V_D_Falt = VCOMM_SPEED_NO_FAULT;
    vcomm_Blk_B_Falt = false;
    vcomm_Prtct_B_Falt = false;
    sticky_lin_fault = 0;

    vcomm_Demand_Missing = false;

    /* Setup expected call chain */
    FLT_MGR_Get_Lin_Fault_J2602_ExpectAndReturn(FLT_MGR_J2602_NO_ERROR);
    FLT_MGR_Is_Any_Fault_Active_ExpectAndReturn(false);
    FLT_MGR_IRQSTAT_Fault_Present_ExpectAndReturn(false);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(123456);

    /* Call function under test */
    result = app_TAOP_ECM_STATUS(NULL,           /* genericProtoEnvData, ignored */
                                 0x09,           /* frameID = 0x09 */
                                 NULL,           /* frameDesc, ignored */
                                 actual_tx_buffer, /* msgBuf, contains message data */
                                 6,              /* msgBufLen */
                                 NULL);          /* genericProtoCbCtxData, ignored */


    /* Verify test results */
    TEST_ASSERT_EQUAL(LinBusIf_PIDACT_TRANSMIT, result);
    TEST_ASSERT_EQUAL_HEX8_ARRAY(expected_tx_buffer, actual_tx_buffer, 6);
    TEST_ASSERT_EQUAL(VCOMM_REQUEST_DENIED, vcomm_V_D_Stat);

}
