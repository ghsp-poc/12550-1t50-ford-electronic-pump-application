/**
 *  @file test_VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq.c
 *
 */
#include "unity.h"
#include "lin_vehicle_comm.h"

/* MOCKS */
#include "mock_adc.h"
#include "mock_fault_manager.h"
#include "mock_foc_stubs.h"
#include "mock_lin_transport.h"
#include "mock_motor_ctrl.h"
#include "mock_serial_data_buffer.h"
#include "mock_speed_meas.h"
#include "mock_time_utils.h"

/* STATIC's */
uint8_t vcomm_V_Pc_Rq;

void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * @test Verifies that VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq will return the full scale
 * value of 4200 RPM when the LIN signal value is 255 (100%).
 *
 */
/* Reqs: SWREQ_1924, SWREQ_2071*/
void test_VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_returns_4200_when_signal_equals_255(void)
{
    uint16_t result = 0;

    /* Ensure known test state */
    vcomm_V_Pc_Rq = 255;

    /* Setup expected call chain */

    /* Call function under test */
    result = VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq();
    
    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT16(4200, result);
}

/**
 * @test Verifies that VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq will return the value 1037 RPM
 * when the LIN signal value is 63 (24.7%)
 *
 */
/* Reqs: SWREQ_1924, SWREQ_2059 */
void test_VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_returns_1037_when_signal_equals_63(void)
{
    uint16_t result = 0;

    /* Ensure known test state */
    vcomm_V_Pc_Rq = 63;

    /* Setup expected call chain */

    /* Call function under test */
    result = VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq();

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT16(1037, result);
}

/**
 * @test Verifies that VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq will return the value 16 RPM
 * when the LIN signal value is 1 (0.3%)
 *
 */
/* Reqs: SWREQ_1924, SWREQ_2059 */
void test_VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_returns_16_when_signal_equals_1(void)
{
    uint16_t result = 0;

    /* Ensure known test state */
    vcomm_V_Pc_Rq = 1;

    /* Setup expected call chain */

    /* Call function under test */
    result = VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq();

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT16(16, result);
}

/**
 * @test Verifies that VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq will return the value 247 RPM
 * when the LIN signal value is 15 (5%)
 *
 */
/* Reqs: SWREQ_1924, SWREQ_2059 */
void test_VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_returns_247_when_signal_equals_15(void)
{
    uint16_t result = 0;

    /* Ensure known test state */
    vcomm_V_Pc_Rq = 15;

    /* Setup expected call chain */

    /* Call function under test */
    result = VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq();

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT16(247, result);
}

/**
 * @test Verifies that VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq will return the value 2091 RPM
 * when the LIN signal value is 127 (49%)
 *
 */
/* Reqs: SWREQ_1924, SWREQ_2059 */
void test_VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_returns_2091_when_signal_equals_127(void)
{
    uint16_t result = 0;

    /* Ensure known test state */
    vcomm_V_Pc_Rq = 127;

    /* Setup expected call chain */

    /* Call function under test */
    result = VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq();

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT16(2091, result);
}

/**
 * @test Verifies that VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq will return the value 3145 RPM
 * when the LIN signal value is 191 (74%)
 *
 */
/* Reqs: SWREQ_1924, SWREQ_2059 */
void test_VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_returns_3145_when_signal_equals_191(void)
{
    uint16_t result = 0;

    /* Ensure known test state */
    vcomm_V_Pc_Rq = 191;

    /* Setup expected call chain */

    /* Call function under test */
    result = VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq();

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT16(3145, result);
}
