/**
 *  @file test_app_ECM_TAOP_DEMAND.c
 *
 */
#include "unity.h"
#include "lin_vehicle_comm.h"

/* MOCKS */
#include "mock_adc.h"
#include "mock_fault_manager.h"
#include "mock_foc_stubs.h"
#include "mock_lin_transport.h"
#include "mock_motor_ctrl.h"
#include "mock_serial_data_buffer.h"
#include "mock_speed_meas.h"
#include "mock_time_utils.h"

/* STATIC's */
VCOMM_TAOP_Status_T vcomm_V_D_Stat;
bool_t vcomm_Loss_Of_Demand;
bool_t vcomm_Loss_Of_Status_Header;
bool_t vcomm_Demand_Missing;
Time_MS_T vcomm_ECM_TAOP_DEMAND_timestamp;
Time_MS_T vcomm_ECM_TAOP_STATUS_Received_timestamp;
Time_MS_T vcomm_ECM_TAOP_DEMAND_Received_timestamp;

void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * @test Verifies that the VCOMM_Loss_Of_Comm function will report a loss of
 * communications when one has been detected.
 */
/* Reqs: SWREQ_1925 */
void test_VCOMM_Loss_Of_Comm_reports_loss_of_communications(void)
{
    bool_t result = false;

    /* Ensure known test state */
    vcomm_Loss_Of_Demand = true;

    /* Setup expected call chain */

    /* Call function under test */
    result = VCOMM_Loss_Of_Comm();
    
    /* Verify test results */
    TEST_ASSERT_TRUE(result);
    
}

/**
 * @test Verifies that the VCOMM_Loss_Of_Comm function will not report a loss of
 * communications when one has not been detected.
 */
/* Reqs SWREQ_1926 */
void test_VCOMM_Loss_Of_Comm_reports_no_loss_of_communications(void)
{
    bool_t result = true;

    /* Ensure known test state */
    vcomm_Loss_Of_Demand = false;

    /* Setup expected call chain */

    /* Call function under test */
    result = VCOMM_Loss_Of_Comm();

    /* Verify test results */
    TEST_ASSERT_FALSE(result);

}

/**
 * @test Verifies that the VCOMM_Task function will set the V_D_Stat signal to VCOMM_NO_REQUEST
 * after 1 second of no received message. It will leave the vcomm_Loss_Of_Demand alone.
 */
/* Reqs: SWREQ_1926 */
void test_VCOMM_Task_sets_V_D_Stat_to_NO_REQUEST_after_1_sec(void)
{
    bool_t result = true;

    /* Ensure known test state */
    vcomm_V_D_Stat = VCOMM_REQUEST_HONORED;
    vcomm_Demand_Missing = false;
    vcomm_Loss_Of_Demand = false;
    vcomm_Loss_Of_Status_Header = false;
    vcomm_ECM_TAOP_DEMAND_timestamp = 0x12345678;
    vcomm_ECM_TAOP_STATUS_Received_timestamp = 0x12345678;
    vcomm_ECM_TAOP_DEMAND_Received_timestamp = 0x12345678;

    /* Setup expected call chain */
    spdmeas_get_cur_speed_IgnoreAndReturn(0);
    mc_get_supply_current_IgnoreAndReturn(0);
    adc_get_supply_voltage_IgnoreAndReturn(0);
    FLT_MGR_Is_Fault_Active_IgnoreAndReturn(false); /* This is not a test for the fault setting. Ignore the Fault Manager */
    FLT_MGR_Is_In_Self_Protect_IgnoreAndReturn(false);
    TMUT_MS_To_Ticks_ExpectAndReturn(1000, 5248); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(0x12345678, 5248, true); /* No message for at least 1 second */
    TMUT_MS_To_Ticks_ExpectAndReturn(10000, 9456); /* Made up value for the number of ticks in 10000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(0x12345678, 9456, false); /* For loss of status message, header for ID 9 */
    TMUT_MS_To_Ticks_ExpectAndReturn(10000, 9456); /* Made up value for the number of ticks in 10000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(0x12345678, 9456, false); /* 10 second loss of LIN has not occurred yet */

    /* Call function under test */
    VCOMM_Task();

    /* Verify test results */
    TEST_ASSERT_TRUE(vcomm_Demand_Missing);

}

/**
 * @test Verifies that the VCOMM_Task function will set the vcomm_Loss_Of_Demand to true
 * when the VCOMM_LOSS_OF_COMM_TIME_MS has expired.
 */
/* Reqs: SWREQ_1926 */
void test_VCOMM_Task_sets_vcomm_Loss_Of_Demand_when_VCOMM_LOSS_OF_COMM_TIME_MS_expires(void)
{
    bool_t result = true;

    /* Ensure known test state */
    vcomm_V_D_Stat = VCOMM_REQUEST_HONORED;
    vcomm_Loss_Of_Demand = false;
    vcomm_Loss_Of_Status_Header = false;
    vcomm_ECM_TAOP_DEMAND_timestamp = 0x12345678;
    vcomm_ECM_TAOP_STATUS_Received_timestamp = 0x12345678;
    vcomm_ECM_TAOP_DEMAND_Received_timestamp = 0x12345678;

    /* Setup expected call chain */
    spdmeas_get_cur_speed_IgnoreAndReturn(0);
    mc_get_supply_current_IgnoreAndReturn(0);
    adc_get_supply_voltage_IgnoreAndReturn(0);
    FLT_MGR_Is_Fault_Active_IgnoreAndReturn(false); /* This is not a test for the fault setting. Ignore the Fault Manager */
    FLT_MGR_Is_In_Self_Protect_IgnoreAndReturn(false);
    TMUT_MS_To_Ticks_ExpectAndReturn(1000, 5248); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(0x12345678, 5248, true); /* No message for at least 1 second */
    TMUT_MS_To_Ticks_ExpectAndReturn(10000, 9456); /* Made up value for the number of ticks in 10000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(0x12345678, 9456, false); /* For loss of status message, header for ID 9 */
    TMUT_MS_To_Ticks_ExpectAndReturn(10000, 9456); /* Made up value for the number of ticks in 10000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(0x12345678, 9456, true); /* 10 second loss of LIN has occurred */

    /* Call function under test */
    VCOMM_Task();

    /* Verify test results */
    TEST_ASSERT_TRUE(vcomm_Loss_Of_Demand);
    TEST_ASSERT_FALSE(vcomm_Loss_Of_Status_Header);
}


/**
 * @test Verifies that the VCOMM_Task function will set vcomm_Loss_Of_Demand to true
 * when the VCOMM_NO_STATUS_TIMEOUT_MS has expired.
 */
/* Reqs: SWREQ_1926 and SWREQ_1927 */
void test_VCOMM_Task_sets_vcomm_Loss_Of_Demand_when_VCOMM_NO_STATUS_TIMEOUT_MS_expires(void)
{
    /* Ensure known test state */
    vcomm_V_D_Stat = VCOMM_REQUEST_HONORED;
    vcomm_Loss_Of_Demand = false;
    vcomm_Loss_Of_Status_Header = false;
    vcomm_ECM_TAOP_DEMAND_timestamp = 0x12345678;
    vcomm_ECM_TAOP_STATUS_Received_timestamp = 0x12345678;
    vcomm_ECM_TAOP_DEMAND_Received_timestamp = 0x12345678;

    /* Setup expected call chain */
    spdmeas_get_cur_speed_IgnoreAndReturn(0);
    mc_get_supply_current_IgnoreAndReturn(0);
    adc_get_supply_voltage_IgnoreAndReturn(0);
    FLT_MGR_Is_Fault_Active_IgnoreAndReturn(false); /* This is not a test for the fault setting. Ignore the Fault Manager */
    FLT_MGR_Is_In_Self_Protect_IgnoreAndReturn(false);
    TMUT_MS_To_Ticks_ExpectAndReturn(1000, 5248); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(0x12345678, 5248, false); /* No timeout of Demand message, still coming in OK  */
    TMUT_MS_To_Ticks_ExpectAndReturn(10000, 9456); /* Made up value for the number of ticks in 10000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(0x12345678, 9456, true); /* For loss of status message, header for ID 9 */
    TMUT_MS_To_Ticks_ExpectAndReturn(10000, 9456); /* Made up value for the number of ticks in 10000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(0x12345678, 9456, false); /* No timeout of Demand message, still coming in OK */

    /* Call function under test */
    VCOMM_Task();

    /* Verify test results */
    TEST_ASSERT_FALSE(vcomm_Loss_Of_Demand);
    TEST_ASSERT_TRUE(vcomm_Loss_Of_Status_Header);
    TEST_ASSERT_EQUAL(vcomm_V_D_Stat, VCOMM_REQUEST_HONORED);
}

