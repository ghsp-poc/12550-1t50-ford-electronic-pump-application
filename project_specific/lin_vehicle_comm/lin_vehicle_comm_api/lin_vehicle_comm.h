#ifndef LIN_VEHICLE_COMM_H
#define LIN_VEHICLE_COMM_H

/**
 *  @file lin_vehicle_comm.h
 *
 *  @copyright 2019 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential@n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup lin_vehicle_comm_api LIN Vehicle Communication Interface Documentation
 *  @{
 *      @details **Place Your Description Here**
 *
 *      @page ABBR Abbreviations
 *        - NONE
 */
/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "global.h"
#include "LinProto_Interface.h"

/*===========================================================================*
 * Exported Preprocessor \#define Constants
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define MACROS
 *===========================================================================*/
#define MAX_RPM        ((uint16_t)4200u)

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/


/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/
typedef enum
{
    VCOMM_MODE_IDLE,
    VCOMM_MODE_RUN,
    VCOMM_NUM_OPERATION_MODES
} VCOMM_Oil_Pump_Mode_T;

typedef enum
{
    VCOMM_NO_REQUEST,
    VCOMM_REQUEST_HONORED,
    VCOMM_REQUEST_DENIED,
    VCOMM_NUM_REQUEST_STATUS
} VCOMM_TAOP_Status_T;

typedef enum
{
    VCOMM_TEMP_NO_FAULT,
    VCOMM_OVER_TEMP_FAULT,
    VCOMM_UNDER_TEMP_FAULT,
    VCOMM_TEMP_SIGNAL_NOT_AVAIL
} VCOMM_TAOP_Temp_Fault_T;

typedef enum
{
    VCOMM_CURRENT_NO_FAULT,
    VCOMM_OVER_CURRENT_FAULT,
    VCOMM_UNDER_CURRENT_FAULT,
    VCOMM_CURRENT_SIGNAL_NOT_AVAIL
} VCOMM_TAOP_Current_Fault_T;

typedef enum
{
    VCOMM_VOLTAGE_NO_FAULT,
    VCOMM_OVER_VOLTAGE_FAULT,
    VCOMM_UNDER_VOLTAGE_FAULT,
    VCOMM_VOLTAGE_SIGNAL_NOT_AVAIL
} VCOMM_TAOP_Voltage_Fault_T;

typedef enum
{
    VCOMM_SPEED_NO_FAULT,
    VCOMM_OVER_SPEED_FAULT,
    VCOMM_UNDER_SPEED_FAULT,
    VCOMM_SPEED_SIGNAL_NOT_AVAIL
} VCOMM_TAOP_Speed_Fault_T;


typedef uint16_t VCOMM_RPM_T;

typedef int16_t VCOMM_Temperature_T;


/*===========================================================================*
 * Exported Const Object Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Function Prototypes
 *===========================================================================*/
/**
 * Initializes the Vehicle Communications module
 *
 * @return - true = successful initialization
 *           false = failed initialization
 */
bool_t VCOMM_Init(void);

/**
 * Repeated processing related to the Vehicle Communication module. Must be called regularly.
 *
 */
void VCOMM_Task(void);

/**
 * Indicates the status of the communication link with the LIN Master
 *
 * @return - true = No LIN communications detected
 *           false = LIN communications has been detected
 */
bool_t VCOMM_Loss_Of_Comm(void);

/**
 * Provides the command mode of operation of the pump.
 *
 * @return Commanded operation mode
 */
VCOMM_Oil_Pump_Mode_T VCOMM_Signal_Get_TrnOilPumpMde_D_Rq(void);

/**
 * Provides the value of the TrnOilPumpV_Pc_Rq Signal, which is the current
 * commanded motor speed in counts
 *
 * @return Commanded motor speed in RPM
 */
VCOMM_RPM_T VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq(void);

/**
 * Provides the value of the TrnSumpOil_Te_Actl, which is the current temperature
 * of the oil in which the pump is operating
 *
 * @return Oil temperature in Degrees C
 */
VCOMM_Temperature_T VCOMM_Signal_Get_TrnSumpOil_Te_Actl(void);

/**
 * Sets the value of the TrnOilPump_I_Meas, which is the measured current.
 *
 * @param Measured_Current - 16 bit representation for the measured current
 */
void VCOMM_Signal_Set_TrnOilPump_I_Meas(uint16_t Measured_Current);

/**
 * Sets the value of the TrnOilPump_U_Meas signal which is the measured
 * voltage.
 *
 * @param Measured_Voltage - 16 bit representation of the measured voltage
 */
void VCOMM_Signal_Set_TrnOilPump_U_Meas(uint16_t Measured_Voltage);

/**
 * Supplies the current value of the signal TrnOilPumpMde_D_Stat
 *
 * @return TrnOilPumpMde_D_Stat
 * @retval VCOMM_MODE_IDLE - Device is reporting an IDLE state
 * @retval VCOMM_MODE_RUN - Device is reporting a RUN state
 */
VCOMM_Oil_Pump_Mode_T VCOMM_Signal_Get_TrnOilPumpMde_D_Stat(void);

/**
 * Sets the value of the TrnOilPumpMde_D_Stat signal, which is the current
 * mode of operation of the motor
 *
 * @param Mode IDLE or RUN
 */
void VCOMM_Signal_Set_TrnOilPumpMde_D_Stat(VCOMM_Oil_Pump_Mode_T Mode);

/**
 * Sets the value of the TrnOilPumpBlk_B_Falt, which indicates that there
 * is a blockage in the gerotor.
 *
 * @param Blockage_Detected - true = Blockage detected
 *                          - false = No blockage detected
 */
void VCOMM_Signal_Set_TrnOilPumpBlk_B_Falt(bool_t Blockage_Detected);

/**
 * Sets the value of the TrnOilPumpPrtct_B_Falt signal, which indicates
 * if there is a fault that results in the system preventing normal operation
 * because of a fault that could result in damage.
 *
 * @param Self_Protect_Fault_Is_Present - true = Self protect fault is active
 *                                      - false = Self protect fault is not active
 */
void VCOMM_Signal_Set_TrnOilPumpPrtct_B_Falt(bool_t Self_Protect_Fault_Is_Active);

/**
 * Sets the value of the TrnOilPumpV_D_Stat whcih is the status of handling a request.
 *
 * @param Status indicates the status of the request
 */
void VCOMM_Signal_Set_TrnOilPumpV_D_Stat(VCOMM_TAOP_Status_T Status);

/**
 * Sets the value of the TrnOilPumpV_Pc_Actl signal, which is the current
 * speed of the pump in counts
 *
 * @param Actual_Speed in RPM
 */
void VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl(VCOMM_RPM_T Actual_Speed);

/**
 * Gets the value of the TrnOilPumpV_Pc_Actl signal, which is the current
 * speed of the pump in counts
 *
 * @return TrnOilPumpV_Pc_Actl 
 */

uint8_t VCOMM_Signal_Get_TrnOilPumpV_Pc_Actl(void);

/**
 * Gets the value of the TrnOilPumpV_Pc_Req signal, which is the commanded
 * speed of the pump in counts
 *
 * @return TrnOilPumpV_Pc_Req
 */

uint8_t VCOMM_Signal_Get_TrnOilPumpV_Pc_Req(void);


/*---------------------------------------*\
 *      ELMOS LIN Frame Handlers         *
\*---------------------------------------*/
LinBusIf_eFrameIdAction_t app_ECM_TAOP_DEMAND(LinProtoIf_pGenericEnvData_t    genericProtoEnvData,
                                              LinBusIf_FrameID_t              frameID,
                                              LinProtoIf_cpFrameDescription_t frameDesc,
                                              LinProtoIf_pData_t              msgBuf,
                                              LinProtoIf_BufLength_t          msgBufLen,
                                              LinProtoIf_pGenericCbCtxData_t  genericProtoCbCtxData);

LinBusIf_eFrameIdAction_t app_TAOP_ECM_STATUS(LinProtoIf_pGenericEnvData_t    genericProtoEnvData,
                                              LinBusIf_FrameID_t              frameID,
                                              LinProtoIf_cpFrameDescription_t frameDesc,
                                              LinProtoIf_pData_t              msgBuf,
                                              LinProtoIf_BufLength_t          msgBufLen,
                                              LinProtoIf_pGenericCbCtxData_t  genericProtoCbCtxData);

LinBusIf_eFrameIdAction_t app_Master_Diag_Request(LinProtoIf_pGenericEnvData_t    genericProtoEnvData,
                                                  LinBusIf_FrameID_t              frameID,
                                                  LinProtoIf_cpFrameDescription_t frameDesc,
                                                  LinProtoIf_pData_t              msgBuf,
                                                  LinProtoIf_BufLength_t          msgBufLen,
                                                  LinProtoIf_pGenericCbCtxData_t  genericProtoCbCtxData);

LinBusIf_eFrameIdAction_t app_Slave_Diag_Response(LinProtoIf_pGenericEnvData_t    genericProtoEnvData,
                                                  LinBusIf_FrameID_t              frameID,
                                                  LinProtoIf_cpFrameDescription_t frameDesc,
                                                  LinProtoIf_pData_t              msgBuf,
                                                  LinProtoIf_BufLength_t          msgBufLen,
                                                  LinProtoIf_pGenericCbCtxData_t  genericProtoCbCtxData);
/** @} doxygen end group */

#endif /* LIN_VEHICLE_COMM_H */
