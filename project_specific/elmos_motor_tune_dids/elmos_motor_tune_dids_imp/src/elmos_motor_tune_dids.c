/**
 *  @file elmos_motor_tune_dids.c
 *
 *  @ref elmos_motor_tune_dids_api
 *
 *  @copyright 2019 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup elmos_motor_tune_dids_imp Elmos Motor Tune DIDs Implementation
 *  @{
 *
 *      System specific implementation of the @ref elmos_motor_tune_dids_api
 *
 *      @page TRACE Traceability
 *        - Design Document(s):
 *          - None
 *
 *        - Applicable Standards (in order of precedence: highest first):
 *          - @http{sharepoint/sandbox/ghspdev/LaunchPad/EngineeringDepartment/Elec_proj_mgmt/Revision%20Controlled%20Library/Software%20Process/Work%20Instructions/Coding%20Standard.docx,
 *                  "GHSP Coding Standard"}
 *
 *      @page DFS Deviations from Standards
 *        - None
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "52307_driver.h"
#include "foc.h"
#include "global.h"
#include "motor_ctrl.h"
#include "motor_manager_diag.h"
#include "uds_types.h"

#ifdef _TEST_
#    include "52307_test_regs.h"
#endif

/*===========================================================================*
 * Local Preprocessor \#define Constants
 *===========================================================================*/
/*===========================================================================*
 * Local Preprocessor \#define MACROS
 *===========================================================================*/
/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/
/*===========================================================================*
 * Local Type Declarations
 *===========================================================================*/
/*===========================================================================*
 * Exported Const Object Definitions
 *===========================================================================*/
/*===========================================================================*
 * Local Function Prototypes
 *===========================================================================*/
/*===========================================================================*
 * Local Object Definitions
 *===========================================================================*/
/*===========================================================================*
 * Function Definitions
 *===========================================================================*/

UDS_Response_Code_T MTDIDS_Get_Startup_Iq_Target(Serial_Data_Buffer_T * const p_Resp_SDB)
{
    UDS_Response_Code_T result = UDS_RESP_POSITIVE;
    mc_data_t const * Motor_Control_Data = mc_get_data();

    if (NULL != Motor_Control_Data)
    {
        SDB_Serialize_I16(p_Resp_SDB, Motor_Control_Data->startup.i_q_ref);
    }
    else
    {
        result = UDS_RESP_GENERAL_REJECT;
    }

    return result;
}

UDS_Response_Code_T MTDIDS_Set_Startup_Iq_Target(Serial_Data_Buffer_T * const p_Req_SDB)
{
    int16_t Startup_Iq_Target = SDB_Deserialize_I16(p_Req_SDB);

    MMGR_Override_Startup_Iq_Ref(Startup_Iq_Target);

    return UDS_RESP_POSITIVE;
}

UDS_Response_Code_T MTDIDS_Get_Startup_Current_Slope(Serial_Data_Buffer_T * const p_Resp_SDB)
{
    UDS_Response_Code_T result = UDS_RESP_POSITIVE;
    mc_data_t const * Motor_Control_Data = mc_get_data();

    if (NULL != Motor_Control_Data)
    {
        SDB_Serialize_I16(p_Resp_SDB, Motor_Control_Data->startup.current_slope);
    }
    else
    {
        result = UDS_RESP_GENERAL_REJECT;
    }

    return result;
}

UDS_Response_Code_T MTDIDS_Set_Startup_Current_Slope(Serial_Data_Buffer_T * const p_Req_SDB)
{
    int16_t Startup_Current_Slope = SDB_Deserialize_I16(p_Req_SDB);

    MMGR_Override_Startup_Current_Slope(Startup_Current_Slope);

    return UDS_RESP_POSITIVE;
}

UDS_Response_Code_T MTDIDS_Get_Startup_Alignment_Time(Serial_Data_Buffer_T * const p_Resp_SDB)
{
    UDS_Response_Code_T result = UDS_RESP_POSITIVE;
    mc_data_t const * Motor_Control_Data = mc_get_data();

    if (NULL != Motor_Control_Data)
    {
        SDB_Serialize_U16(p_Resp_SDB, Motor_Control_Data->startup.align_rotor_duration);
    }
    else
    {
        result = UDS_RESP_GENERAL_REJECT;
    }

    return result;
}

UDS_Response_Code_T MTDIDS_Set_Startup_Alignment_Time(Serial_Data_Buffer_T * const p_Req_SDB)
{
    static uint16_t Original_Startup_Current_Slope = 0u;
    static bool_t Startup_Current_Slope_Override_Enabled = false;
    UDS_Response_Code_T result = UDS_RESP_POSITIVE;
    uint16_t Startup_Current_Slope = SDB_Deserialize_U16(p_Req_SDB);
    mc_data_t const * Motor_Control_Data = mc_get_data();

    if (    (Startup_Current_Slope_Override_Enabled)
         && (UINT16_MAX == Startup_Current_Slope))
    {
        /* Clear override and restore Original value */
        Startup_Current_Slope_Override_Enabled = false;
        mc_set_startup_align_rotor_duration(Original_Startup_Current_Slope);
    }
    else
    {
        if (    (NULL != Motor_Control_Data)
             && (UINT16_MAX != Startup_Current_Slope))
        {
            /* Enabled override and store Original value */
            Startup_Current_Slope_Override_Enabled = true;
            Original_Startup_Current_Slope = Motor_Control_Data->startup.align_rotor_duration;
            mc_set_startup_align_rotor_duration(Startup_Current_Slope);
        }
        else
        {
            result = UDS_RESP_GENERAL_REJECT;
        }
    }

    return result;
}

UDS_Response_Code_T MTDIDS_Get_Startup_Speed_Ramp_Start(Serial_Data_Buffer_T * const p_Resp_SDB)
{
    UDS_Response_Code_T result = UDS_RESP_POSITIVE;
    const mc_data_t * Motor_Control_Data = mc_get_data();

    if (NULL != Motor_Control_Data)
    {
        SDB_Serialize_U16(p_Resp_SDB, Motor_Control_Data->startup.speed_ramp_start);
    }
    else
    {
        result = UDS_RESP_GENERAL_REJECT;
    }

    return result;
}

UDS_Response_Code_T MTDIDS_Set_Startup_Speed_Ramp_Start(Serial_Data_Buffer_T * const p_Req_SDB)
{
    static uint16_t Original_Startup_Speed_Ramp_Start = 0u;
    static bool_t Startup_Speed_Ramp_Start_Override_Enabled = false;
    UDS_Response_Code_T result = UDS_RESP_POSITIVE;
    uint16_t Startup_Speed_Ramp_Start = SDB_Deserialize_U16(p_Req_SDB);
    mc_data_t const * Motor_Control_Data = mc_get_data();

    if (    (Startup_Speed_Ramp_Start_Override_Enabled)
         && (UINT16_MAX == Startup_Speed_Ramp_Start))
    {
        /* Clear override and restore Original value */
        Startup_Speed_Ramp_Start_Override_Enabled = false;
        mc_set_startup_align_rotor_duration(Original_Startup_Speed_Ramp_Start);
    }
    else
    {
        if (    (NULL != Motor_Control_Data)
             && (UINT16_MAX != Startup_Speed_Ramp_Start))
        {
            /* Enabled override and store Original value */
            Startup_Speed_Ramp_Start_Override_Enabled = true;
            Original_Startup_Speed_Ramp_Start = Motor_Control_Data->startup.align_rotor_duration;
            mc_set_startup_align_rotor_duration(Startup_Speed_Ramp_Start);
        }
        else
        {
            result = UDS_RESP_GENERAL_REJECT;
        }
    }

    return result;
}

UDS_Response_Code_T MTDIDS_Get_Startup_Speed_Slope(Serial_Data_Buffer_T * const p_Resp_SDB)
{
    UDS_Response_Code_T result = UDS_RESP_POSITIVE;
    const mc_data_t * Motor_Control_Data = mc_get_data();

    if (NULL != Motor_Control_Data)
    {
        SDB_Serialize_I16(p_Resp_SDB, Motor_Control_Data->startup.speed_slope);
    }
    else
    {
        result = UDS_RESP_GENERAL_REJECT;
    }

    return result;
}

UDS_Response_Code_T MTDIDS_Set_Startup_Speed_Slope(Serial_Data_Buffer_T * const p_Req_SDB)
{
    int16_t Startup_Speed_Slope = SDB_Deserialize_I16(p_Req_SDB);

    MMGR_Override_Startup_Speed_Slope(Startup_Speed_Slope);

    return UDS_RESP_POSITIVE;
}

UDS_Response_Code_T MTDIDS_Get_Startup_Speed_Ramp_End(Serial_Data_Buffer_T * const p_Resp_SDB)
{
    UDS_Response_Code_T result = UDS_RESP_POSITIVE;
    const mc_data_t * Motor_Control_Data = mc_get_data();

    if (NULL != Motor_Control_Data)
    {
        SDB_Serialize_U16(p_Resp_SDB, Motor_Control_Data->startup.speed_ramp_end);
    }
    else
    {
        result = UDS_RESP_GENERAL_REJECT;
    }

    return result;
}

UDS_Response_Code_T MTDIDS_Set_Startup_Speed_Ramp_End(Serial_Data_Buffer_T * const p_Req_SDB)
{
    uint16_t Startup_Speed_Ramp_End = SDB_Deserialize_U16(p_Req_SDB);

    MMGR_Override_Startup_Speed_Ramp_End(Startup_Speed_Ramp_End);

    return UDS_RESP_POSITIVE;
}

UDS_Response_Code_T MTDIDS_Get_Speed_Control_Kp(Serial_Data_Buffer_T * const p_Resp_SDB)
{
    UDS_Response_Code_T result = UDS_RESP_POSITIVE;
    const mc_data_t * Motor_Control_Data = mc_get_data();

    if (NULL != Motor_Control_Data)
    {
        SDB_Serialize_I16(p_Resp_SDB, Motor_Control_Data->controller_rotor_speed.Kp);
    }
    else
    {
        result = UDS_RESP_GENERAL_REJECT;
    }

    return result;
}

UDS_Response_Code_T MTDIDS_Set_Speed_Control_Kp(Serial_Data_Buffer_T * const p_Req_SDB)
{
    static uint16_t Original_Speed_Control_Kp = 0u;
    static bool_t Speed_Control_Kp_Override_Enabled = false;
    UDS_Response_Code_T result = UDS_RESP_POSITIVE;
    uint16_t Speed_Control_Kp = SDB_Deserialize_U16(p_Req_SDB);
    mc_data_t const * Motor_Control_Data = mc_get_data();

    if (    (Speed_Control_Kp_Override_Enabled)
         && (UINT16_MAX == Speed_Control_Kp))
    {
        /* Clear override and restore Original value */
        Speed_Control_Kp_Override_Enabled = false;
        mc_set_startup_align_rotor_duration(Original_Speed_Control_Kp);
    }
    else
    {
        if (    (NULL != Motor_Control_Data)
             && (UINT16_MAX != Speed_Control_Kp))
        {
            /* Enabled override and store Original value */
            Speed_Control_Kp_Override_Enabled = true;
            Original_Speed_Control_Kp = Motor_Control_Data->startup.align_rotor_duration;
            mc_set_startup_align_rotor_duration(Speed_Control_Kp);
        }
        else
        {
            result = UDS_RESP_GENERAL_REJECT;
        }
    }

    return result;
}

UDS_Response_Code_T MTDIDS_Get_Speed_Control_Ki(Serial_Data_Buffer_T * const p_Resp_SDB)
{
    UDS_Response_Code_T result = UDS_RESP_POSITIVE;
    const mc_data_t * Motor_Control_Data = mc_get_data();

    if (NULL != Motor_Control_Data)
    {
        SDB_Serialize_U16(p_Resp_SDB, Motor_Control_Data->controller_rotor_speed.Ki);
    }
    else
    {
        result = UDS_RESP_GENERAL_REJECT;
    }

    return result;
}

STATIC uint16_t Original_Speed_Control_Ki = 0u;
STATIC bool_t Speed_Control_Ki_Override_Enabled = false;

UDS_Response_Code_T MTDIDS_Set_Speed_Control_Ki(Serial_Data_Buffer_T * const p_Req_SDB)
{
    UDS_Response_Code_T result = UDS_RESP_POSITIVE;
    uint16_t Speed_Control_Ki = SDB_Deserialize_U16(p_Req_SDB);

    if (    (Speed_Control_Ki_Override_Enabled)
         && (UINT16_MAX == Speed_Control_Ki))
    {
        /* Clear override and restore Original value */
        Speed_Control_Ki_Override_Enabled = false;
        mc_set_speed_ctrl_ki(Original_Speed_Control_Ki);
    }
    else
    {
        mc_data_t const * Motor_Control_Data = mc_get_data();

        if (NULL != Motor_Control_Data)
        {
             if (UINT16_MAX != Speed_Control_Ki)
             {
                 /* Enabled override and store Original value */
                 Speed_Control_Ki_Override_Enabled = true;
                 Original_Speed_Control_Ki = Motor_Control_Data->startup.align_rotor_duration;
                 mc_set_speed_ctrl_ki(Speed_Control_Ki);
             }
        }
        else
        {
            result = UDS_RESP_GENERAL_REJECT;
        }
    }

    return result;
}

UDS_Response_Code_T MTDIDS_Get_Motor_Constant_Uf(Serial_Data_Buffer_T * const p_Resp_SDB)
{
    UDS_Response_Code_T result = UDS_RESP_POSITIVE;
    foc_ctrl_loop_data_t const * FOC_Data = foc_get_data();

    if (NULL != FOC_Data)
    {
        SDB_Serialize_U16(p_Resp_SDB, FOC_Data->param_motor_constant);
    }
    else
    {
        result = UDS_RESP_GENERAL_REJECT;
    }

    return result;
}

STATIC uint16_t Original_Motor_Constant_Uf = 0u;
STATIC bool_t Motor_Constant_Uf_Override_Enabled = false;

UDS_Response_Code_T MTDIDS_Set_Motor_Constant_Uf(Serial_Data_Buffer_T * const p_Req_SDB)
{
    UDS_Response_Code_T result = UDS_RESP_POSITIVE;
    uint16_t Motor_Constant_Uf = SDB_Deserialize_U16(p_Req_SDB);

    if (    (Motor_Constant_Uf_Override_Enabled)
         && (UINT16_MAX == Motor_Constant_Uf))
    {
        /* Clear override and restore Original value */
        Motor_Constant_Uf_Override_Enabled = false;
        foc_set_motor_constant(Original_Motor_Constant_Uf);
    }
    else
    {
        foc_ctrl_loop_data_t const * FOC_Data = foc_get_data();

        if (NULL != FOC_Data)
        {
             if (UINT16_MAX != Motor_Constant_Uf)
             {
                 /* Enabled override and store Original value */
                 Motor_Constant_Uf_Override_Enabled = true;
                 Original_Motor_Constant_Uf = FOC_Data->param_motor_constant;
                 foc_set_motor_constant(Motor_Constant_Uf);
             }
        }
        else
        {
            result = UDS_RESP_GENERAL_REJECT;
        }
    }

    return result;
}

UDS_Response_Code_T MTDIDS_Get_Stall_Detect_Min_Speed(Serial_Data_Buffer_T * const p_Resp_SDB)
{
    UDS_Response_Code_T result = UDS_RESP_POSITIVE;
    foc_ctrl_loop_data_t const * FOC_Data = foc_get_data();

    if (NULL != FOC_Data)
    {
        SDB_Serialize_U16(p_Resp_SDB, FOC_Data->stall_det_min_rotor_speed);
    }
    else
    {
        result = UDS_RESP_GENERAL_REJECT;
    }

    return result;
}

STATIC uint16_t Original_Stall_Detect_Min_Speed = 0u;
STATIC bool_t Stall_Detect_Min_Speed_Override_Enabled = false;

UDS_Response_Code_T MTDIDS_Set_Stall_Detect_Min_Speed(Serial_Data_Buffer_T * const p_Req_SDB)
{
    UDS_Response_Code_T result = UDS_RESP_POSITIVE;
    uint16_t Stall_Detect_Min_Speed = SDB_Deserialize_U16(p_Req_SDB);

    if (    (Stall_Detect_Min_Speed_Override_Enabled)
         && (UINT16_MAX == Stall_Detect_Min_Speed))
    {
        /* Clear override and restore Original value */
        Stall_Detect_Min_Speed_Override_Enabled = false;
        foc_set_stall_detect_min_rotor_speed(Original_Stall_Detect_Min_Speed);
    }
    else
    {
        foc_ctrl_loop_data_t const * FOC_Data = foc_get_data();

        if (NULL != FOC_Data)
        {
             if (UINT16_MAX != Stall_Detect_Min_Speed)
             {
                 /* Enabled override and store Original value */
                 Stall_Detect_Min_Speed_Override_Enabled = true;
                 Original_Stall_Detect_Min_Speed = FOC_Data->stall_det_min_rotor_speed;
                 foc_set_stall_detect_min_rotor_speed(Stall_Detect_Min_Speed);
             }
        }
        else
        {
            result = UDS_RESP_GENERAL_REJECT;
        }
    }

    return result;
}

UDS_Response_Code_T MTDIDS_Get_Speed_Control_Output_Max(Serial_Data_Buffer_T * const p_Resp_SDB)
{
    UDS_Response_Code_T result = UDS_RESP_POSITIVE;
    const mc_data_t * Motor_Control_Data = mc_get_data();

    if (NULL != Motor_Control_Data)
    {
        SDB_Serialize_I16(p_Resp_SDB, Motor_Control_Data->speed_ctrl_output_max);
    }
    else
    {
        result = UDS_RESP_GENERAL_REJECT;
    }

    return result;
}

UDS_Response_Code_T MTDIDS_Set_Speed_Control_Output_Max(Serial_Data_Buffer_T * const p_Req_SDB)
{
    int16_t Speed_Control_Output_Max = SDB_Deserialize_I16(p_Req_SDB);

    MMGR_Override_Speed_Control_Output_Max(Speed_Control_Output_Max);

    return UDS_RESP_POSITIVE;
}

UDS_Response_Code_T MTDIDS_Get_Short_Circuit_HS_Thresh(Serial_Data_Buffer_T * const p_Resp_SDB)
{
    uint8_t Short_Circuit_HS_Threshold = 0u;

    d52307_read_reg(SCTH_HS, &Short_Circuit_HS_Threshold);

    SDB_Serialize_U8(p_Resp_SDB, Short_Circuit_HS_Threshold);

    return UDS_RESP_POSITIVE;
}

UDS_Response_Code_T MTDIDS_Set_Short_Circuit_HS_Thresh(Serial_Data_Buffer_T * const p_Req_SDB)
{
    uint8_t Short_Circuit_HS_Threshold = SDB_Deserialize_U8(p_Req_SDB);

    d52307_write_reg(SCTH_HS, Short_Circuit_HS_Threshold);

    return UDS_RESP_POSITIVE;
}

UDS_Response_Code_T MTDIDS_Get_Short_Circuit_LS_Thresh(Serial_Data_Buffer_T * const p_Resp_SDB)
{
    uint8_t Short_Circuit_LS_Threshold = 0u;

    d52307_read_reg(SCTH_LS, &Short_Circuit_LS_Threshold);

    SDB_Serialize_U8(p_Resp_SDB, Short_Circuit_LS_Threshold);

    return UDS_RESP_POSITIVE;
}

UDS_Response_Code_T MTDIDS_Set_Short_Circuit_LS_Thresh(Serial_Data_Buffer_T * const p_Req_SDB)
{
    uint8_t Short_Circuit_LS_Threshold = SDB_Deserialize_U8(p_Req_SDB);

    d52307_write_reg(SCTH_LS, Short_Circuit_LS_Threshold);

    return UDS_RESP_POSITIVE;
}

UDS_Response_Code_T MTDIDS_Get_SC_Masking_Time(Serial_Data_Buffer_T * const p_Resp_SDB)
{
    uint8_t Short_Circuit_Masking_Time_Threshold = 0u;

    d52307_read_reg(SCMT, &Short_Circuit_Masking_Time_Threshold);

    SDB_Serialize_U8(p_Resp_SDB, Short_Circuit_Masking_Time_Threshold);

    return UDS_RESP_POSITIVE;
}

UDS_Response_Code_T MTDIDS_Set_SC_Masking_Time(Serial_Data_Buffer_T * const p_Req_SDB)
{
    uint8_t Short_Circuit_Masking_Time_Threshold = SDB_Deserialize_U8(p_Req_SDB);

    d52307_write_reg(SCMT, Short_Circuit_Masking_Time_Threshold);

    return UDS_RESP_POSITIVE;
}

UDS_Response_Code_T MTDIDS_Get_SC_Debounce_Time(Serial_Data_Buffer_T * const p_Resp_SDB)
{
    uint8_t Short_Circuit_Debounce_Time_Threshold = 0u;

    d52307_read_reg(SCDEB, &Short_Circuit_Debounce_Time_Threshold);

    SDB_Serialize_U8(p_Resp_SDB, Short_Circuit_Debounce_Time_Threshold);

    return UDS_RESP_POSITIVE;
}

UDS_Response_Code_T MTDIDS_Set_SC_Debounce_Time(Serial_Data_Buffer_T * const p_Req_SDB)
{
    uint8_t Short_Circuit_Debounce_Time_Threshold = SDB_Deserialize_U8(p_Req_SDB);

    d52307_write_reg(SCDEB, Short_Circuit_Debounce_Time_Threshold);

    return UDS_RESP_POSITIVE;
}

UDS_Response_Code_T MTDIDS_Get_PWM_Deadtime(Serial_Data_Buffer_T * const p_Req_SDB)
{
    uint16_t Dead_Time = PWMN_DEAD_TIME;

    SDB_Serialize_U16(p_Req_SDB, Dead_Time);
    return UDS_RESP_POSITIVE;
}

UDS_Response_Code_T MTDIDS_Set_PWM_Deadtime(Serial_Data_Buffer_T * const p_Req_SDB)
{
    uint16_t PWM_Deadtime = SDB_Deserialize_U16(p_Req_SDB);

    PWMN_DEAD_TIME_RELOAD = PWM_Deadtime;
    PWMN_DEAD_TIME = PWM_Deadtime;

    return UDS_RESP_POSITIVE;
}

UDS_Response_Code_T MTDIDS_Get_IRQSTAT(Serial_Data_Buffer_T * const p_Req_SDB)
{
    SDB_Serialize_U16(p_Req_SDB, mc_get_irqstat());

    return UDS_RESP_POSITIVE;
}

UDS_Response_Code_T MTDIDS_Get_OSC_CONFIG(Serial_Data_Buffer_T * const p_Req_SDB)
{
    SDB_Serialize_U16(p_Req_SDB, OSC_CTRL_OSC_CONFIG);

    return UDS_RESP_POSITIVE;
}

UDS_Response_Code_T MTDIDS_Set_OSC_CONFIG(Serial_Data_Buffer_T * const p_Req_SDB)
{
    uint16_t New_OSC_CONFIG = SDB_Deserialize_U16(p_Req_SDB);

    OSC_CTRL_OSC_CONFIG = New_OSC_CONFIG;

    return UDS_RESP_POSITIVE;
}
/** @} doxygen end group */
