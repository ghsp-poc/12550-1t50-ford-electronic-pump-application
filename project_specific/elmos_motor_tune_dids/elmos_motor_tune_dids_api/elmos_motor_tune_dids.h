#ifndef ELMOS_MOTOR_TUNE_DIDS_H
#define ELMOS_MOTOR_TUNE_DIDS_H

/**
 *  @file elmos_motor_tune_dids.h
 *
 *  @copyright 2019 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential@n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup elmos_motor_tune_dids_api Elmos Motor Tune DIDs Interface Documentation
 *  @{
 *      @details **Place Your Description Here**
 *
 *      @page ABBR Abbreviations
 *        - NONE
 */
/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "serial_data_buffer.h"
#include "uds_types.h"

/*===========================================================================*
 * Exported Preprocessor \#define Constants
 *===========================================================================*/
#define MTDIDS_STARTUP_IQ_DID_SIZE               (2u)
#define MTDIDS_STARTUP_I_SLOPE_DID_SIZE          (2u)
#define MTDIDS_STARTUP_ALIGN_TIME_DID_SIZE       (2u)
#define MTDIDS_STARTUP_SPEED_RAMP_START_DID_SIZE (2u)
#define MTDIDS_STARTUP_SPEED_SLOPE_DID_SIZE      (2u)
#define MTDIDS_STARTUP_SPEED_RAMP_END_DID_SIZE   (2u)
#define MTDIDS_SPEED_CTRL_KP_DID_SIZE            (2u)
#define MTDIDS_SPEED_CTRL_KI_DID_SIZE            (2u)
#define MTDIDS_MOTOR_CONSTANT_UF_DID_SIZE        (2u)
#define MTDIDS_STALL_DETECT_MIN_SPEED_DID_SIZE   (2u)
#define MTDIDS_SPEED_CTRL_OUT_MAX_DID_SIZE       (2u)
#define MTDIDS_SCTH_HS_DID_SIZE                  (1u)
#define MTDIDS_SCTH_LS_DID_SIZE                  (1u)
#define MTDIDS_SC_MASKING_TIME_DID_SIZE          (1u)
#define MTDIDS_SC_DEBOUNCE_TIME_DID_SIZE         (1u)
#define MTDIDS_PWM_DEADTIME_DID_SIZE             (2u)
#define MTDIDS_IRQSTAT_DID_SIZE                  (2u)
#define MTDIDS_OSC_CONFIG_DID_SIZE               (2u)

/*===========================================================================*
 * Exported Preprocessor \#define MACROS
 *===========================================================================*/
/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/
/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/
/*===========================================================================*
 * Exported Const Object Declarations
 *===========================================================================*/
/*===========================================================================*
 * Exported Function Prototypes
 *===========================================================================*/

/**
 * Read DID accessor for the Elmos Startup Iq Target
 *
 * @param[out] p_Resp_SDB Buffer in which to provide the serialized data.
 *
 * @return UDS_Response_Code_T indicates status of operation
 * @retval UDS_RESP_POSITIVE - Successful
 * @retval UDS_RESP_GENERAL_REJECT - Unknown error occurred retrieving data
 */
UDS_Response_Code_T MTDIDS_Get_Startup_Iq_Target(Serial_Data_Buffer_T * const p_Resp_SDB);

/**
 * Write DID accessor for the Elmos Startup Iq Target
 *
 * @param[out] p_Resp_SDB Buffer which contains the serialized data.
 *
 * @return UDS_Response_Code_T UDS_RESP_POSITIVE
 */
UDS_Response_Code_T MTDIDS_Set_Startup_Iq_Target(Serial_Data_Buffer_T * const p_Req_SDB);

/**
 * Read DID accessor for the Elmos Startup Current Slope
 *
 * @param[out] p_Resp_SDB Buffer in which to provide the serialized data.
 *
 * @return UDS_Response_Code_T indicates status of operation
 * @retval UDS_RESP_POSITIVE - Successful
 * @retval UDS_RESP_GENERAL_REJECT - Unknown error occurred retrieving data
 */
UDS_Response_Code_T MTDIDS_Get_Startup_Current_Slope(Serial_Data_Buffer_T * const p_Resp_SDB);

/**
 * Write DID accessor for the Elmos Startup Current Slope
 *
 * @param[out] p_Resp_SDB Buffer which contains the serialized data.
 *
 * @return UDS_Response_Code_T UDS_RESP_POSITIVE
 */
UDS_Response_Code_T MTDIDS_Set_Startup_Current_Slope(Serial_Data_Buffer_T * const p_Req_SDB);

/**
 * Read DID accessor for the Elmos Startup Alignment Time
 *
 * @param[out] p_Resp_SDB Buffer in which to provide the serialized data.
 *
 * @return UDS_Response_Code_T indicates status of operation
 * @retval UDS_RESP_POSITIVE - Successful
 * @retval UDS_RESP_GENERAL_REJECT - Unknown error occurred retrieving data
 */
UDS_Response_Code_T MTDIDS_Get_Startup_Alignment_Time(Serial_Data_Buffer_T * const p_Resp_SDB);

/**
 * Write DID accessor for the Elmos Startup Alignment Time
 *
 * @param[out] p_Resp_SDB Buffer which contains the serialized data.
 *
 * @return UDS_Response_Code_T UDS_RESP_POSITIVE
 */
UDS_Response_Code_T MTDIDS_Set_Startup_Alignment_Time(Serial_Data_Buffer_T * const p_Req_SDB);

/**
 * Read DID accessor for the Elmos Startup Speed Ramp Start
 *
 * @param[out] p_Resp_SDB Buffer in which to provide the serialized data.
 *
 * @return UDS_Response_Code_T indicates status of operation
 * @retval UDS_RESP_POSITIVE - Successful
 * @retval UDS_RESP_GENERAL_REJECT - Unknown error occurred retrieving data
 */
UDS_Response_Code_T MTDIDS_Get_Startup_Speed_Ramp_Start(Serial_Data_Buffer_T * const p_Resp_SDB);

/**
 * Write DID accessor for the Elmos Startup Speed Ramp
 *
 * @param[out] p_Resp_SDB Buffer which contains the serialized data.
 *
 * @return UDS_Response_Code_T UDS_RESP_POSITIVE
 */
UDS_Response_Code_T MTDIDS_Set_Startup_Speed_Ramp_Start(Serial_Data_Buffer_T * const p_Req_SDB);

/**
 * Read DID accessor for the Elmos Startup Speed Slope
 *
 * @param[out] p_Resp_SDB Buffer in which to provide the serialized data.
 *
 * @return UDS_Response_Code_T indicates status of operation
 * @retval UDS_RESP_POSITIVE - Successful
 * @retval UDS_RESP_GENERAL_REJECT - Unknown error occurred retrieving data
 */
UDS_Response_Code_T MTDIDS_Get_Startup_Speed_Slope(Serial_Data_Buffer_T * const p_Resp_SDB);

/**
 * Write DID accessor for the Elmos Startup Speed Slope
 *
 * @param[out] p_Resp_SDB Buffer which contains the serialized data.
 *
 * @return UDS_Response_Code_T UDS_RESP_POSITIVE
 */
UDS_Response_Code_T MTDIDS_Set_Startup_Speed_Slope(Serial_Data_Buffer_T * const p_Req_SDB);

/**
 * Read DID accessor for the Elmos Startup Speed Ramp End
 *
 * @param[out] p_Resp_SDB Buffer in which to provide the serialized data.
 *
 * @return UDS_Response_Code_T indicates status of operation
 * @retval UDS_RESP_POSITIVE - Successful
 * @retval UDS_RESP_GENERAL_REJECT - Unknown error occurred retrieving data
 */
UDS_Response_Code_T MTDIDS_Get_Startup_Speed_Ramp_End(Serial_Data_Buffer_T * const p_Resp_SDB);

/**
 * Write DID accessor for the Elmos Startup Speed Ramp End
 *
 * @param[out] p_Resp_SDB Buffer which contains the serialized data.
 *
 * @return UDS_Response_Code_T UDS_RESP_POSITIVE
 */
UDS_Response_Code_T MTDIDS_Set_Startup_Speed_Ramp_End(Serial_Data_Buffer_T * const p_Req_SDB);

/**
 * Read DID accessor for the Elmos Speed Control Kp
 *
 * @param[out] p_Resp_SDB Buffer in which to provide the serialized data.
 *
 * @return UDS_Response_Code_T indicates status of operation
 * @retval UDS_RESP_POSITIVE - Successful
 * @retval UDS_RESP_GENERAL_REJECT - Unknown error occurred retrieving data
 */
UDS_Response_Code_T MTDIDS_Get_Speed_Control_Kp(Serial_Data_Buffer_T * const p_Resp_SDB);

/**
 * Write DID accessor for the Elmos Speed Control Kp
 *
 * @param[out] p_Resp_SDB Buffer which contains the serialized data.
 *
 * @return UDS_Response_Code_T UDS_RESP_POSITIVE
 */
UDS_Response_Code_T MTDIDS_Set_Speed_Control_Kp(Serial_Data_Buffer_T * const p_Req_SDB);

/**
 * Read DID accessor for the Elmos Speed Control Ki
 *
 * @param[out] p_Resp_SDB Buffer in which to provide the serialized data.
 *
 * @return UDS_Response_Code_T indicates status of operation
 * @retval UDS_RESP_POSITIVE - Successful
 * @retval UDS_RESP_GENERAL_REJECT - Unknown error occurred retrieving data
 */
UDS_Response_Code_T MTDIDS_Get_Speed_Control_Ki(Serial_Data_Buffer_T * const p_Resp_SDB);

/**
 * Write DID accessor for the Elmos Speed Control Ki
 *
 * @param[out] p_Resp_SDB Buffer which contains the serialized data.
 *
 * @return UDS_Response_Code_T UDS_RESP_POSITIVE
 */
UDS_Response_Code_T MTDIDS_Set_Speed_Control_Ki(Serial_Data_Buffer_T * const p_Req_SDB);

/**
 * Read DID accessor for the Elmos Motor Constant Uf
 *
 * @param[out] p_Resp_SDB Buffer in which to provide the serialized data.
 *
 * @return UDS_Response_Code_T indicates status of operation
 * @retval UDS_RESP_POSITIVE - Successful
 * @retval UDS_RESP_GENERAL_REJECT - Unknown error occurred retrieving data
 */
UDS_Response_Code_T MTDIDS_Get_Motor_Constant_Uf(Serial_Data_Buffer_T * const p_Resp_SDB);

/**
 * Write DID accessor for the Elmos Motor Constant Uf
 *
 * @param[out] p_Resp_SDB Buffer which contains the serialized data.
 *
 * @return UDS_Response_Code_T UDS_RESP_POSITIVE
 */
UDS_Response_Code_T MTDIDS_Set_Motor_Constant_Uf(Serial_Data_Buffer_T * const p_Req_SDB);

/**
 * Read DID accessor for the Elmos Stall Detection Minimum Speed
 *
 * @param[out] p_Resp_SDB Buffer in which to provide the serialized data.
 *
 * @return UDS_Response_Code_T indicates status of operation
 * @retval UDS_RESP_POSITIVE - Successful
 * @retval UDS_RESP_GENERAL_REJECT - Unknown error occurred retrieving data
 */
UDS_Response_Code_T MTDIDS_Get_Stall_Detect_Min_Speed(Serial_Data_Buffer_T * const p_Resp_SDB);

/**
 * Write DID accessor for the Stall Detection Minimum Speed
 *
 * @param[out] p_Resp_SDB Buffer which contains the serialized data.
 *
 * @return UDS_Response_Code_T UDS_RESP_POSITIVE
 */
UDS_Response_Code_T MTDIDS_Set_Stall_Detect_Min_Speed(Serial_Data_Buffer_T * const p_Req_SDB);

/**
 * Read DID accessor for the Elmos Speed Control Output Maximum value
 *
 * @param[out] p_Resp_SDB Buffer in which to provide the serialized data.
 *
 * @return UDS_Response_Code_T indicates status of operation
 * @retval UDS_RESP_POSITIVE - Successful
 * @retval UDS_RESP_GENERAL_REJECT - Unknown error occurred retrieving data
 */
UDS_Response_Code_T MTDIDS_Get_Speed_Control_Output_Max(Serial_Data_Buffer_T * const p_Resp_SDB);

/**
 * Write DID accessor for the Elmos Speed Control Output Maximum value
 *
 * @param[out] p_Resp_SDB Buffer which contains the serialized data.
 *
 * @return UDS_Response_Code_T UDS_RESP_POSITIVE
 */
UDS_Response_Code_T MTDIDS_Set_Speed_Control_Output_Max(Serial_Data_Buffer_T * const p_Req_SDB);

/**
 * Read DID accessor for the Elmos Short Circuit High Side Detection Threshold
 *
 * @param[out] p_Resp_SDB Buffer in which to provide the serialized data.
 *
 * @return UDS_Response_Code_T indicates status of operation
 * @retval UDS_RESP_POSITIVE - Successful
 * @retval UDS_RESP_GENERAL_REJECT - Unknown error occurred retrieving data
 */
UDS_Response_Code_T MTDIDS_Get_Short_Circuit_HS_Thresh(Serial_Data_Buffer_T * const p_Resp_SDB);

/**
 * Write DID accessor for the Elmos Short Circuit High Side Detection Threshold
 *
 * @param[out] p_Resp_SDB Buffer which contains the serialized data.
 *
 * @return UDS_Response_Code_T UDS_RESP_POSITIVE
 */
UDS_Response_Code_T MTDIDS_Set_Short_Circuit_HS_Thresh(Serial_Data_Buffer_T * const p_Req_SDB);

/**
 * Read DID accessor for the Elmos Short Circuit Low Side Detection Threshold
 *
 * @param[out] p_Resp_SDB Buffer in which to provide the serialized data.
 *
 * @return UDS_Response_Code_T indicates status of operation
 * @retval UDS_RESP_POSITIVE - Successful
 * @retval UDS_RESP_GENERAL_REJECT - Unknown error occurred retrieving data
 */
UDS_Response_Code_T MTDIDS_Get_Short_Circuit_LS_Thresh(Serial_Data_Buffer_T * const p_Resp_SDB);

/**
 * Write DID accessor for the Elmos Short Circuit Low Side Detection Threshold
 *
 * @param[out] p_Resp_SDB Buffer which contains the serialized data.
 *
 * @return UDS_Response_Code_T UDS_RESP_POSITIVE
 */
UDS_Response_Code_T MTDIDS_Set_Short_Circuit_LS_Thresh(Serial_Data_Buffer_T * const p_Req_SDB);

/**
 * Read DID accessor for the Elmos Short Circuit Masking Time
 *
 * @param[out] p_Resp_SDB Buffer in which to provide the serialized data.
 *
 * @return UDS_Response_Code_T indicates status of operation
 * @retval UDS_RESP_POSITIVE - Successful
 * @retval UDS_RESP_GENERAL_REJECT - Unknown error occurred retrieving data
 */
UDS_Response_Code_T MTDIDS_Get_SC_Masking_Time(Serial_Data_Buffer_T * const p_Resp_SDB);

/**
 * Write DID accessor for the Elmos Short Circuit Masking Time
 *
 * @param[out] p_Resp_SDB Buffer which contains the serialized data.
 *
 * @return UDS_Response_Code_T UDS_RESP_POSITIVE
 */
UDS_Response_Code_T MTDIDS_Set_SC_Masking_Time(Serial_Data_Buffer_T * const p_Req_SDB);

/**
 * Read DID accessor for the Elmos Short Circuit Debounce Time
 *
 * @param[out] p_Resp_SDB Buffer in which to provide the serialized data.
 *
 * @return UDS_Response_Code_T indicates status of operation
 * @retval UDS_RESP_POSITIVE - Successful
 * @retval UDS_RESP_GENERAL_REJECT - Unknown error occurred retrieving data
 */
UDS_Response_Code_T MTDIDS_Get_SC_Debounce_Time(Serial_Data_Buffer_T * const p_Resp_SDB);

/**
 * Write DID accessor for the Elmos Short Circuit Debounce Time
 *
 * @param[out] p_Resp_SDB Buffer which contains the serialized data.
 *
 * @return UDS_Response_Code_T UDS_RESP_POSITIVE
 */
UDS_Response_Code_T MTDIDS_Set_SC_Debounce_Time(Serial_Data_Buffer_T * const p_Req_SDB);

/**
 * Read DID accessor for the Elmos PWM Deadtime
 *
 * @param[out] p_Resp_SDB Buffer in which to provide the serialized data.
 *
 * @return UDS_Response_Code_T indicates status of operation
 * @retval UDS_RESP_POSITIVE - Successful
 */
UDS_Response_Code_T MTDIDS_Get_PWM_Deadtime(Serial_Data_Buffer_T * const p_Req_SDB);

/**
 * Write DID accessor for the Elmos PWM Deadtime
 *
 * @param[out] p_Resp_SDB Buffer which contains the serialized data.
 *
 * @return UDS_Response_Code_T UDS_RESP_POSITIVE
 */
UDS_Response_Code_T MTDIDS_Set_PWM_Deadtime(Serial_Data_Buffer_T * const p_Resp_SDB);

/**
 * Read DID accessor for the Elmos IRQSTAT registers (IRQSTAT1 and IRQSTAT2)
 *
 * @param[out] p_Resp_SDB Buffer in which to provide the serialized data.
 *
 * @return UDS_Response_Code_T indicates status of operation
 * @retval UDS_RESP_POSITIVE - Successful
 */
UDS_Response_Code_T MTDIDS_Get_IRQSTAT(Serial_Data_Buffer_T * const p_Req_SDB);

/**
 * Read DID accessor for the Elmos OSC_CONFIG register
 *
 * @param[out] p_Resp_SDB Buffer in which to provide the serialized data.
 *
 * @return UDS_Response_Code_T indicates status of operation
 * @retval UDS_RESP_POSITIVE - Successful
 */
UDS_Response_Code_T MTDIDS_Get_OSC_CONFIG(Serial_Data_Buffer_T * const p_Req_SDB);

/**
 * Write DID accessor for the Elmos OSC_CONFIG Register
 *
 * @param[out] p_Resp_SDB Buffer which contains the serialized data.
 *
 * @return UDS_Response_Code_T UDS_RESP_POSITIVE
 */

UDS_Response_Code_T MTDIDS_Set_OSC_CONFIG(Serial_Data_Buffer_T * const p_Resp_SDB);

/** @} doxygen end group */

#endif /* ELMOS_MOTOR_TUNE_DIDS_H */
