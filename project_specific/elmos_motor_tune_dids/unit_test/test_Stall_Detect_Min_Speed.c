/**
 *  @file test_Speed_Control_Ki_DID.c
 *
 */
#include "unity.h"
#include "52307_test_regs.h"
#include "elmos_motor_tune_dids.h"
#include <string.h>

/* MOCKS */
#include "mock_52307_driver_stubs.h"
#include "mock_foc.h"
#include "mock_motor_ctrl.h"
#include "mock_motor_manager_diag.h"
#include "mock_serial_data_buffer.h"

uint16_t Test_Regs[TEST_REG_END] = {0};

/* STATIC's */
uint16_t Original_Stall_Detect_Min_Speed;
bool_t Stall_Detect_Min_Speed_Override_Enabled;

static foc_ctrl_loop_data_t test_foc_data = {0};
static Serial_Data_Buffer_T Test_SDB = {0};

void setUp(void)
{
    memset(&test_foc_data, 0, sizeof(test_foc_data));
    memset(&Test_SDB, 0, sizeof(Test_SDB));
}

void tearDown(void)
{

}

/**
 * @test Verifies that MTDIDS_Get_Stall_Detect_Min_Speed will serialize the value supplied for the
 * Stall Detection Minimum Speed by the Elmos FOC library
 */
void test_MTDIDS_Get_Stall_Detect_Min_Speed_retrieves_value_and_serializes(void)
{
    UDS_Response_Code_T result = 0xFF;

    /* Ensure known test state */
    test_foc_data.stall_det_min_rotor_speed = 0x0600;

    /* Setup expected call chain */
    foc_get_data_ExpectAndReturn(&test_foc_data);
    SDB_Serialize_U16_ExpectAndReturn(&Test_SDB, 0x0600, SDB_EOK);

    /* Call function under test */
    result = MTDIDS_Get_Stall_Detect_Min_Speed(&Test_SDB);
    
    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, result);
}

/**
 * @test Verifies that MTDIDS_Get_Stall_Detect_Min_Speed does nothing and returns UDS_RESP_GENERAL_REJECT
 * when the Elmos FOC library returns a NULL pointer
 */
void test_MTDIDS_Get_Stall_Detect_Min_Speed_returns_UDS_RESP_GENERAL_REJECT_when_foc_get_data_returns_NULL(void)
{
    UDS_Response_Code_T result = 0xFF;

    /* Ensure known test state */

    /* Setup expected call chain */
    foc_get_data_ExpectAndReturn(NULL);

    /* Call function under test */
    result = MTDIDS_Get_Stall_Detect_Min_Speed(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_GENERAL_REJECT, result);
}

/**
 * @test Verifies that MTDIDS_Set_Stall_Detect_Min_Speed will deserialize the value supplied for the
 * Stall Detection Minimum Speed and set it.
 */
void test_MTDIDS_Set_Stall_Detect_Min_Speed_deserializes_value_and_sets(void)
{
    UDS_Response_Code_T result = 0xFF;

    /* Ensure known test state */
    Stall_Detect_Min_Speed_Override_Enabled = false;
    test_foc_data.stall_det_min_rotor_speed = 0x0600;

    /* Setup expected call chain */
    SDB_Deserialize_U16_ExpectAndReturn(&Test_SDB, 0x451);
    foc_get_data_ExpectAndReturn(&test_foc_data);
    foc_set_stall_detect_min_rotor_speed_Expect(0x451);

    /* Call function under test */
    result = MTDIDS_Set_Stall_Detect_Min_Speed(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, result);
    TEST_ASSERT_EQUAL_UINT16(0x600, Original_Stall_Detect_Min_Speed);
    TEST_ASSERT_TRUE(Stall_Detect_Min_Speed_Override_Enabled);
}

/**
 * @test Verifies that MTDIDS_Set_Stall_Detect_Min_Speed will deserialize the clear value for the
 * Stall Detection Minimum Speed and restore the previous value.
 */
void test_MTDIDS_Set_Stall_Detect_Min_Speed_deserializes_clear_value_and_restores_original(void)
{
    UDS_Response_Code_T result = 0xFF;

    /* Ensure known test state */
    Stall_Detect_Min_Speed_Override_Enabled = true;
    Original_Stall_Detect_Min_Speed = 0x0600;

    /* Setup expected call chain */
    SDB_Deserialize_U16_ExpectAndReturn(&Test_SDB, UINT16_MAX);
    foc_set_stall_detect_min_rotor_speed_Expect(0x600); /* Restore original value */

    /* Call function under test */
    result = MTDIDS_Set_Stall_Detect_Min_Speed(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, result);
    TEST_ASSERT_FALSE(Stall_Detect_Min_Speed_Override_Enabled);
}

/**
 * @test Verifies that MTDIDS_Set_Stall_Detect_Min_Speed does nothing and returns UDS_RESP_GENERAL_REJECT
 * when the Elmos FOC library returns a NULL pointer
 */
void test_MTDIDS_Set_Stall_Detect_Min_Speed_returns_UDS_RESP_GENERAL_REJECT_when_foc_get_data_returns_NULL(void)
{
    UDS_Response_Code_T result = 0xFF;

    /* Ensure known test state */
    Stall_Detect_Min_Speed_Override_Enabled = false;

    /* Setup expected call chain */
    SDB_Deserialize_U16_ExpectAndReturn(&Test_SDB, 0x600);
    foc_get_data_ExpectAndReturn(NULL);

    /* Call function under test */
    result = MTDIDS_Set_Stall_Detect_Min_Speed(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_GENERAL_REJECT, result);
}
