/**
 *  @file test_Speed_Control_Ki_DID.c
 *
 */
#include "unity.h"
#include "52307_test_regs.h"
#include "elmos_motor_tune_dids.h"
#include <string.h>

/* MOCKS */
#include "mock_52307_driver_stubs.h"
#include "mock_foc.h"
#include "mock_motor_ctrl.h"
#include "mock_motor_manager_diag.h"
#include "mock_serial_data_buffer.h"

uint16_t Test_Regs[TEST_REG_END] = {0};

/* STATIC's */
uint16_t Original_Speed_Control_Ki;
bool_t Speed_Control_Ki_Override_Enabled;

static mc_data_t test_mc_data = {0};
static Serial_Data_Buffer_T Test_SDB = {0};

void setUp(void)
{
    memset(&test_mc_data, 0, sizeof(test_mc_data));
    memset(&Test_SDB, 0, sizeof(Test_SDB));
}

void tearDown(void)
{

}

/**
 * @test Verifies that MTDIDS_Get_Speed_Control_Ki will serialize the value supplied for the
 * speed control Ki by the Motor Control library
 *
 * @req
 */
void test_MTDIDS_Get_Speed_Control_Ki_retrieves_value_and_serializes(void)
{
    UDS_Response_Code_T result = 0xFF;

    /* Ensure known test state */
    test_mc_data.controller_rotor_speed.Ki = 0x0600;

    /* Setup expected call chain */
    mc_get_data_ExpectAndReturn(&test_mc_data);
    SDB_Serialize_U16_ExpectAndReturn(&Test_SDB, 0x0600, SDB_EOK);

    /* Call function under test */
    result = MTDIDS_Get_Speed_Control_Ki(&Test_SDB);
    
    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, result);
}

/**
 * @test Verifies that MTDIDS_Get_Speed_Control_Ki does nothing and returns UDS_RESP_GENERAL_REJECT
 * when the motor control library returns a NULL pointer
 *
 * @req
 */
void test_MTDIDS_Get_Speed_Control_Ki_returns_UDS_RESP_GENERAL_REJECT_when_mc_get_Data_returns_NULL(void)
{
    UDS_Response_Code_T result = 0xFF;

    /* Ensure known test state */

    /* Setup expected call chain */
    mc_get_data_ExpectAndReturn(NULL);

    /* Call function under test */
    result = MTDIDS_Get_Speed_Control_Ki(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_GENERAL_REJECT, result);
}

/**
 * @test Verifies that MTDIDS_Set_Speed_Control_Ki will deserialize the value supplied for the
 * speed control Ki and set it.
 *
 * @req
 */
void test_MTDIDS_Set_Speed_Control_Ki_deserializes_value_and_sets(void)
{
    UDS_Response_Code_T result = 0xFF;

    /* Ensure known test state */
    Speed_Control_Ki_Override_Enabled = false;
    test_mc_data.controller_rotor_speed.Ki = 0x0600;

    /* Setup expected call chain */
    SDB_Deserialize_U16_ExpectAndReturn(&Test_SDB, 0x451);
    mc_get_data_ExpectAndReturn(&test_mc_data);
    mc_set_speed_ctrl_ki_Expect(0x451);

    /* Call function under test */
    result = MTDIDS_Set_Speed_Control_Ki(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, result);
}

/**
 * @test Verifies that MTDIDS_Set_Speed_Control_Ki will deserialize the clear value for the
 * speed control Ki and restore the previous value.
 *
 * @req
 */
void test_MTDIDS_Set_Speed_Control_Ki_deserializes_clear_value_and_restores_original(void)
{
    UDS_Response_Code_T result = 0xFF;

    /* Ensure known test state */
    Speed_Control_Ki_Override_Enabled = true;
    Original_Speed_Control_Ki = 0x0600;

    /* Setup expected call chain */
    SDB_Deserialize_U16_ExpectAndReturn(&Test_SDB, UINT16_MAX);
    mc_set_speed_ctrl_ki_Expect(0x600); /* Restore original value */

    /* Call function under test */
    result = MTDIDS_Set_Speed_Control_Ki(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, result);
}

/**
 * @test Verifies that MTDIDS_Set_Speed_Control_Ki does nothing and returns UDS_RESP_GENERAL_REJECT
 * when the motor control library returns a NULL pointer
 *
 * @req
 */
void test_MTDIDS_Set_Speed_Control_Ki_returns_UDS_RESP_GENERAL_REJECT_when_mc_get_data_returns_NULL(void)
{
    UDS_Response_Code_T result = 0xFF;

    /* Ensure known test state */
    Speed_Control_Ki_Override_Enabled = false;

    /* Setup expected call chain */
    SDB_Deserialize_U16_ExpectAndReturn(&Test_SDB, UINT16_MAX);
    mc_get_data_ExpectAndReturn(NULL);

    /* Call function under test */
    result = MTDIDS_Set_Speed_Control_Ki(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_GENERAL_REJECT, result);
}
