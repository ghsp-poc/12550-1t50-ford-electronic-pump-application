/**
 *  @file test_Speed_Control_Ki_DID.c
 *
 */
#include "unity.h"
#include "52307_test_regs.h"
#include "elmos_motor_tune_dids.h"
#include <string.h>

/* MOCKS */
#include "mock_52307_driver_stubs.h"
#include "mock_foc.h"
#include "mock_motor_ctrl.h"
#include "mock_motor_manager_diag.h"
#include "mock_serial_data_buffer.h"

uint16_t Test_Regs[TEST_REG_END] = {0};

/* STATIC's */
static Serial_Data_Buffer_T Test_SDB = {0};

void setUp(void)
{
    memset(&Test_SDB, 0, sizeof(Test_SDB));
    memset(&Test_Regs, 0, sizeof(Test_Regs));
}

void tearDown(void)
{

}

/**
 * @test Verifies that MTDIDS_Get_PWM_Deadtime will serialize the value of the PWMN
 * Deadtime register
 */
void test_MTDIDS_Get_PWM_Deadtime_retrieves_value_and_serializes(void)
{
    UDS_Response_Code_T result = 0xFF;

    /* Ensure known test state */
    PWMN_DEAD_TIME = 0x55AA;

    /* Setup expected call chain */
    SDB_Serialize_U16_ExpectAndReturn(&Test_SDB, 0x55AA, SDB_EOK);

    /* Call function under test */
    result = MTDIDS_Get_PWM_Deadtime(&Test_SDB);
    
    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, result);
}

/**
 * @test Verifies that MTDIDS_Get_PWM_Deadtime will serialize the value of the PWMN
 * Deadtime register and PWMN Deadtime Releoad register
 */
void test_MTDIDS_Set_PWM_Deadtime_deserializes_and_writes_register(void)
{
    UDS_Response_Code_T result = 0xFF;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U16_ExpectAndReturn(&Test_SDB, 0xAA55);

    /* Call function under test */
    result = MTDIDS_Set_PWM_Deadtime(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, result);
    TEST_ASSERT_EQUAL_HEX16(0xAA55, PWMN_DEAD_TIME);
    TEST_ASSERT_EQUAL_HEX16(0xAA55, PWMN_DEAD_TIME_RELOAD);
}
