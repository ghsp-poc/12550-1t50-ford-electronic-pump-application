#ifndef MFG_DATA_DIDS_H
#define MFG_DATA_DIDS_H

/**
 *  @file mfg_data_dids.h
 *
 *  @copyright 2019 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential@n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup mfg_data_dids_api mfg_data_dids Interface Documentation
 *  @{
 *      @details **Place Your Description Here**
 *
 *      @page ABBR Abbreviations
 *        - NONE
 */
/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "serial_data_buffer.h"
#include "uds_types.h"

/*===========================================================================*
 * Exported Preprocessor \#define Constants
 *===========================================================================*/
/*===========================================================================*
 * Exported Preprocessor \#define MACROS
 *===========================================================================*/
/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/
/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/
/*===========================================================================*
 * Exported Const Object Declarations
 *===========================================================================*/
/*===========================================================================*
 * Exported Function Prototypes
 *===========================================================================*/

/**
 * Read DID accessor for the Software Part Number DID
 *
 * @param[out] p_Resp_SDB Buffer where version data is written to.
 *
 * @return UDS_Response_Code_T UDS response code positive or negative
 */
UDS_Response_Code_T MFDIDS_Get_Sw_Part_Number(Serial_Data_Buffer_T * const p_Resp_SDB);

/**
 * Read DID accessor for the Software Version
 *
 * @param [out] p_Resp_SDB - Buffer in which to place the response data
 * @return UDS_Response_Code_T UDS response code positive or negative
 */
UDS_Response_Code_T MFDIDS_Get_SW_Version(Serial_Data_Buffer_T * const p_Resp_SDB);

/**
 * Read DID accessor for the Hardware Part Number
 *
 * @param [out] p_Resp_SDB - Buffer in which to place the response data
 * @return UDS_Response_Code_T UDS response code positive or negative
 */
UDS_Response_Code_T MFDIDS_Get_HW_Part_Number(Serial_Data_Buffer_T * const p_Resp_SDB);

/**
 * Resets the contents of EEPROM to factory defaults
 *
 * @param p_Req_SDB [in] - buffer containing the flag indicating if EEPROM should be cleared
 *
 * @return UDS_RESP_POSITIVE
 */
UDS_Response_Code_T MFDIDS_Reset_EEPROM(Serial_Data_Buffer_T * const p_Req_SDB);

UDS_Response_Code_T MFDIDS_Corrupt_EEPROM_Block(Serial_Data_Buffer_T * const p_Req_SDB);

/** @} doxygen end group */

#endif /* MFG_DATA_DIDS_H */
