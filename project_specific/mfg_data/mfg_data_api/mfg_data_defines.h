#ifndef MFG_DATA_DEFINES_H
#define MFG_DATA_DEFINES_H

/**
 *  @file mfg_data_defines.h
 *
 *  @copyright 2019 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential@n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup mfg_data_api mfg_data Interface Documentation
 *  @{
 */
/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "auto_version.h"
#include "global.h"

/*===========================================================================*
 * Exported Preprocessor \#define Constants
 *===========================================================================*/
/**
 * Configure the Software Part number. This change is generally manual and should
 * change on every release
 */
#define MFD_SW_APPLICATION_PART_NUMBER "125500980-03RC3"

/**
 * Software Version String made up of the build date, git commit and git branch
 */
#define MFD_SW_VERSION_STRING VERSION_BUILD_DATE_CODE "-" VERSION_GIT_BRANCH "-" VERSION_GIT_COMMIT

/**
 * Hardware Version number first 5 digits is project ID, next for is the HW ID
 * then the -XX is the version of the hardware.
 */
#define MFD_HW_PART_NUMBER "125500961-02"

/**
 * Provide the number of characters present in the System's Part Number
 */
#define MFD_SW_PART_NUM_LEN (sizeof(MFD_SW_APPLICATION_PART_NUMBER) - 1u)

/**
 * Provide the number of characters present in the Software Version String
 */
#define MFD_SW_VERSION_LEN (sizeof(MFD_SW_VERSION_STRING))

/**
 * Provide the number of characters present in the Hardware Part Number String
 */
#define MFD_HW_PART_NUM_LEN (sizeof(MFD_HW_PART_NUMBER) - 1u)

/**
 * Size of the DID used to corrupt EEPROM Pages for testing purposes
 */
#define MFD_EEPROM_CORRUPT_DID_SIZE (2u)

/**
 * Size of the DID to clear the EEPROM to factory defaults
 */
#define MFD_EEPROM_FACTORY_RESET_DID_SIZE (1u)

/** DID number for SW Part Number */
#define MFD_SW_PART_NUMBER_DID (0x010Au)

/** DID number for the SW Version */
#define MFD_SW_VERSION_DID (0x010Bu)

/** DID number for the HW Part Number */
#define MFD_HW_PART_NUMBER_DID (0x010Cu)

/** DID number for corrupting a page of EEPROM for testing purposes */
#define MFD_EEPROM_CORRUPT_DID (0x01FEu)

/** DID number for resetting the contents of EEPROM to factory defaults */
#define MFD_EEPROM_FACTORY_RESET (0x01FFu)

/*===========================================================================*
 * Exported Preprocessor \#define MACROS
 *===========================================================================*/
/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/** @} doxygen end group */
#endif /* MFG_DATA_DEFINES_H */
