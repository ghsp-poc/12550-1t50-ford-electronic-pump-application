/**
 *  @file test_MFDIDS_Corrupt_EEPROM_Block.c
 *
 */
#include "unity.h"
#include "mfg_data_dids.h"

/* MOCKS */
#include "mock_fault_logger.h"
#include "mock_fdl.h"
#include "mock_serial_data_buffer.h"

/* STATIC's */

void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x0000 in the EEPROM when requested to corrupt Page 0, Block 0
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_0000(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 0); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 0); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x0000, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);
    
    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x0020 in the EEPROM when requested to corrupt Page 1, Block 0
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_0020(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 1); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 0); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x0020, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x0040 in the EEPROM when requested to corrupt Page 2, Block 0
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_0040(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 2); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 0); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x0040, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x0060 in the EEPROM when requested to corrupt Page 3, Block 0
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_0060(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 3); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 0); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x0060, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x0080 in the EEPROM when requested to corrupt Page 4, Block 0
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_0080(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 4); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 0); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x0080, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x00A0 in the EEPROM when requested to corrupt Page 5, Block 0
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_00A0(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 5); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 0); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x00A0, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x00C0 in the EEPROM when requested to corrupt Page 6, Block 0
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_00C0(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 6); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 0); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x00C0, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x00E0 in the EEPROM when requested to corrupt Page 7, Block 0
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_00E0(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 7); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 0); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x00E0, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x0100 in the EEPROM when requested to corrupt Page 8, Block 0
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_0100(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 8); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 0); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x0100, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x0120 in the EEPROM when requested to corrupt Page 9, Block 0
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_0120(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 9); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 0); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x0120, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x0140 in the EEPROM when requested to corrupt Page 10, Block 0
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_0140(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 10); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 0); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x0140, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x0160 in the EEPROM when requested to corrupt Page 11, Block 0
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_0160(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 11); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 0); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x0160, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x0180 in the EEPROM when requested to corrupt Page 12, Block 0
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_0180(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 12); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 0); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x0180, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x01A0 in the EEPROM when requested to corrupt Page 13, Block 0
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_01A0(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 13); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 0); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x01A0, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x01C0 in the EEPROM when requested to corrupt Page 14, Block 0
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_01C0(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 14); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 0); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x01C0, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x01E0 in the EEPROM when requested to corrupt Page 15, Block 0
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_01E0(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 15); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 0); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x01E0, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x0200 in the EEPROM when requested to corrupt Page 0, Block 1
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_0200(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 0); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 1); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x0200, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x0220 in the EEPROM when requested to corrupt Page 1, Block 1
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_0220(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 1); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 1); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x0220, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x0240 in the EEPROM when requested to corrupt Page 2, Block 1
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_0240(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 2); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 1); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x0240, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x0260 in the EEPROM when requested to corrupt Page 3, Block 1
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_0260(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 3); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 1); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x0260, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x0280 in the EEPROM when requested to corrupt Page 4, Block 1
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_0280(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 4); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 1); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x0280, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x02A0 in the EEPROM when requested to corrupt Page 5, Block 1
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_02A0(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 5); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 1); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x02A0, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x02C0 in the EEPROM when requested to corrupt Page 6, Block 1
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_02C0(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 6); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 1); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x02C0, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x02E0 in the EEPROM when requested to corrupt Page 7, Block 1
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_02E0(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 7); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 1); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x02E0, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x0300 in the EEPROM when requested to corrupt Page 8, Block 1
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_0300(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 8); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 1); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x0300, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x0320 in the EEPROM when requested to corrupt Page 9, Block 1
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_0320(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 9); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 1); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x0320, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x0340 in the EEPROM when requested to corrupt Page 10, Block 1
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_0340(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 10); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 1); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x0340, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x0360 in the EEPROM when requested to corrupt Page 11, Block 1
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_0360(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 11); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 1); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x0360, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x0380 in the EEPROM when requested to corrupt Page 12, Block 1
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_0380(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 12); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 1); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x0380, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x03A0 in the EEPROM when requested to corrupt Page 13, Block 1
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_03A0(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 13); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 1); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x03A0, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x03C0 in the EEPROM when requested to corrupt Page 14, Block 1
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_03C0(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 14); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 1); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x03C0, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block writes the proper pattern of 32 bytes to
 * Address 0x03E0 in the EEPROM when requested to corrupt Page 15, Block 1
 */
void test_MFDIDS_Corrupt_EEPROM_Block_Corrupts_address_0_03E0(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 15); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 1); /* Block ID 0 */
    FDL_Write_ExpectAndReturn(NULL, 0x03E0, 32, FDL_STATUS_BUSY);
    FDL_Write_IgnoreArg_p_src_buffer();

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block returns UDS_RESP_REQ_OUT_OF_RANGE
 * when Page ID > 15
 */
void test_MFDIDS_Corrupt_EEPROM_Block_returns_UDS_RESP_REQ_OUT_OF_RANGE_Page_ID_16(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 16); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 1); /* Block ID 0 */

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_REQ_OUT_OF_RANGE, Response);
}

/**
 * Verifies that MFDIDS_Corrupt_EEPROM_Block returns UDS_RESP_REQ_OUT_OF_RANGE
 * when Block ID > 1
 */
void test_MFDIDS_Corrupt_EEPROM_Block_returns_UDS_RESP_REQ_OUT_OF_RANGE_Block_ID_2(void)
{
    Serial_Data_Buffer_T Test_SDB;
    UDS_Response_Code_T Response = UDS_RESP_GENERAL_REJECT;

    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 15); /* Page ID 0 */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 2); /* Block ID 0 */

    /* Call function under test */
    Response =  MFDIDS_Corrupt_EEPROM_Block(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_REQ_OUT_OF_RANGE, Response);
}
