/**
 *  @file mfg_data_dids.c
 *
 *  @ref mfg_data_dids_api
 *
 *  @copyright 2019 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup mfg_data_dids_imp mfg_data_dids Implementation
 *  @{
 *
 *      System specific implementation of the @ref mfg_data_dids_api
 *
 *      @page TRACE Traceability
 *        - Design Document(s):
 *          - None
 *
 *        - Applicable Standards (in order of precedence: highest first):
 *          - @http{sharepoint/sandbox/ghspdev/LaunchPad/EngineeringDepartment/Elec_proj_mgmt/Revision%20Controlled%20Library/Software%20Process/Work%20Instructions/Coding%20Standard.docx,
 *                  "GHSP Coding Standard"}
 *
 *      @page DFS Deviations from Standards
 *        - None
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "fault_logger.h"
#include "fdl.h"
#include "global.h"
#include "mfg_data_dids.h"
#include "mfg_data_defines.h"
#include "uds_types.h"

/*===========================================================================*
 * Local Preprocessor \#define Constants
 *===========================================================================*/
/*===========================================================================*
 * Local Preprocessor \#define MACROS
 *===========================================================================*/
/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/
/*===========================================================================*
 * Local Type Declarations
 *===========================================================================*/
/*===========================================================================*
 * Exported Const Object Definitions
 *===========================================================================*/
/*===========================================================================*
 * Local Function Prototypes
 *===========================================================================*/
/*===========================================================================*
 * Local Object Definitions
 *===========================================================================*/

/**
 * Native NV25080 EEPROM addresses for each 32 byte Page managed
 */
static uint16_t const EEPROM_Page_Addresses[2][16] =
{
    /* Block 0 */
    {
        0x0000u, /* Page 0 */
        0x0020u, /* Page 1 */
        0x0040u, /* Page 2 */
        0x0060u, /* Page 3 */
        0x0080u, /* Page 4 */
        0x00A0u, /* Page 5 */
        0x00C0u, /* Page 6 */
        0x00E0u, /* Page 7 */
        0x0100u, /* Page 8 */
        0x0120u, /* Page 9 */
        0x0140u, /* Page 10 */
        0x0160u, /* Page 11 */
        0x0180u, /* Page 12 */
        0x01A0u, /* Page 13 */
        0x01C0u, /* Page 14 */
        0x01E0u, /* Page 15 */
    },
    /* Block 1 */
    {
        0x0200u, /* Page 0 */
        0x0220u, /* Page 1 */
        0x0240u, /* Page 2 */
        0x0260u, /* Page 3 */
        0x0280u, /* Page 4 */
        0x02A0u, /* Page 5 */
        0x02C0u, /* Page 6 */
        0x02E0u, /* Page 7 */
        0x0300u, /* Page 8 */
        0x0320u, /* Page 9 */
        0x0340u, /* Page 10 */
        0x0360u, /* Page 11 */
        0x0380u, /* Page 12 */
        0x03A0u, /* Page 13 */
        0x03C0u, /* Page 14 */
        0x03E0u, /* Page 15 */
    }
};

/*===========================================================================*
 * Function Definitions
 *===========================================================================*/

UDS_Response_Code_T MFDIDS_Get_Sw_Part_Number(Serial_Data_Buffer_T * const p_Resp_SDB)
{
    size_t const num_bytes_serialized = SDB_Serialize_Bytes(p_Resp_SDB,
                                                            (void const *)MFD_SW_APPLICATION_PART_NUMBER,
                                                            MFD_SW_PART_NUM_LEN);

    return (num_bytes_serialized == (size_t)MFD_SW_PART_NUM_LEN) ? UDS_RESP_POSITIVE : UDS_RESP_RESPONSE_TOO_LONG;
}

UDS_Response_Code_T MFDIDS_Get_SW_Version(Serial_Data_Buffer_T * const p_Resp_SDB)
{
    size_t const num_bytes_serialized = SDB_Serialize_Bytes(p_Resp_SDB,
                                                            (void const *)MFD_SW_VERSION_STRING,
                                                            MFD_SW_VERSION_LEN);

    return (num_bytes_serialized == (size_t)MFD_SW_VERSION_LEN) ? UDS_RESP_POSITIVE : UDS_RESP_RESPONSE_TOO_LONG;
}

UDS_Response_Code_T MFDIDS_Get_HW_Part_Number(Serial_Data_Buffer_T * const p_Resp_SDB)
{
    size_t const num_bytes_serialized = SDB_Serialize_Bytes(p_Resp_SDB,
                                                            (void const *)MFD_HW_PART_NUMBER,
                                                            MFD_HW_PART_NUM_LEN);

    return (num_bytes_serialized == (size_t)MFD_HW_PART_NUM_LEN) ? UDS_RESP_POSITIVE : UDS_RESP_RESPONSE_TOO_LONG;
}

UDS_Response_Code_T MFDIDS_Reset_EEPROM(Serial_Data_Buffer_T * const p_Req_SDB)
{
    /*uint8_t Clear_Serial_Number_Value[16] = {0u};*/

    bool_t Do_Erase = (bool_t)SDB_Deserialize_U8(p_Req_SDB);

    if (Do_Erase)
    {
        FLT_LOG_Clear_Fault_Records();
        FLTEL_Clear_Environmental_Log();
        /*FLTEL_Clear_Lifetime_Device_Data();*/
        /*mfdids_Set_Serial_Number(Clear_Serial_Number_Value);*/
    }

    return UDS_RESP_POSITIVE;
}

UDS_Response_Code_T MFDIDS_Corrupt_EEPROM_Block(Serial_Data_Buffer_T * const p_Req_SDB)
{
    UDS_Response_Code_T Response = UDS_RESP_POSITIVE;
    uint8_t Page_ID = SDB_Deserialize_U8(p_Req_SDB);
    uint8_t Block_ID = SDB_Deserialize_U8(p_Req_SDB);
    uint8_t Corruption_Data[32u] =
    { /* 0      1      2      3      4      5      6      7      8      9      A      B      C      D      E      F */
        0xAAu, 0x55u, 0xAAu, 0x55u, 0xAAu, 0x55u, 0xAAu, 0x55u, 0xAAu, 0x55u, 0xAAu, 0x55u, 0xAAu, 0x55u, 0xAAu, 0x55u,
        0xAAu, 0x55u, 0xAAu, 0x55u, 0xAAu, 0x55u, 0xAAu, 0x55u, 0xAAu, 0x55u, 0xAAu, 0x55u, 0xAAu, 0x55u, 0xAAu, 0x55u,
    };

    if (    (Block_ID < 2u)
         && (Page_ID < 16u))
    {
        FDL_Write(Corruption_Data, (uint32_t)EEPROM_Page_Addresses[Block_ID][Page_ID], Num_Elems(Corruption_Data));
    }
    else
    {
        Response = UDS_RESP_REQ_OUT_OF_RANGE;
    }

    return Response;
}

/** @} doxygen end group */
