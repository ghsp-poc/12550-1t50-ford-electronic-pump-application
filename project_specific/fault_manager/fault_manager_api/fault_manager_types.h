#ifndef FAULT_MANAGER_TYPES_H
#define FAULT_MANAGER_TYPES_H

/**
 *  @file fault_manager_types.h
 *
 *  @copyright 2020 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential@n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup fault_manager_api Fault Manager Interface Documentation
 *  @{
 *      @details **Place Your Description Here**
 *
 *      @page ABBR Abbreviations
 *        - NONE
 */
/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "global.h"

/*===========================================================================*
 * Exported Preprocessor \#define Constants
 *===========================================================================*/
  #define ENABLE_UNDER_VOLTAGE_IRQ_RETRY_BOOST 

/*===========================================================================*
 * Exported Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/
typedef enum
{
    FLT_OVER_VOLTAGE,
    FLT_UNDER_VOLTAGE,
    FLT_UNDER_VOLT_SELF_PROTECT,
    FLT_OVER_CURRENT,
    FLT_UNDER_CURRENT,
    FLT_OVER_TEMPERATURE,
    FLT_OVER_SPEED,
    FLT_UNDER_SPEED,
 #ifdef  ENABLE_UNDER_VOLTAGE_IRQ_RETRY_BOOST    
    FLT_UNDER_VOLT_IRQSTAT,
 #endif    
    FLT_MGR_NUM_FAULTS
} FLT_MGR_General_Faults_T;

/* J2602 Status byte 0 lin error defines */
typedef enum
{
    FLT_MGR_J2602_NO_ERROR = 0u,
    FLT_MGR_J2602_RESET = 0x20u,
    FLT_MGR_J2602_RESERVED_1 = 0x01u,
    FLT_MGR_J2602_RESERVED_2 = 0x02u,
    FLT_MGR_J2602_DATA_ERROR = 0x80u,
    FLT_MGR_J2602_CHECKSUM_ERROR = 0xA0u,
    FLT_MGR_J2602_FRAMING_ERROR = 0xC0u,
    FLT_MGR_J2602_ID_PARITY_ERROR = 0xE0u
} FLT_MGR_LIN_J2602_Err_T;

/** @} doxygen end group */

#endif /* FAULT_MANAGER_TYPES_H */
