/**
 *  @file test_FLT_MGR_any_faults_active.c
 *
 */
#include "unity.h"
#include "global.h"
#include "fault_manager.h"
#include "fault_manager_imp_defines.h"
#include "fault_manager_imp_types.h"
#include "speed_meas.h"
#include "motor_manager.h"
#include "fault_logger.h"

#include <string.h>


/* MOCKS */
#include "mock_fault_logger.h"
#include "mock_motor_ctrl.h"
#include "mock_lin_vehicle_comm.h"
#include "mock_system_signals.h"
#include "mock_time_utils.h"
#include "mock_serial_data_buffer.h"
#include "mock_speed_meas.h"
#include "mock_motor_manager.h"
#include "mock_nvm_mgr.h"


/* STATIC's */
#define TICKS_IN_1000MS (20000u)
#define TICKS_IN_4000MS (80000u)

Time_MS_T FLT_Speed_Timestamp;

bool_t detected_over_speed;
bool_t clearing_over_speed;
bool_t detected_under_speed;
bool_t clearing_under_speed;

bool_t Fault_Active[FLT_MGR_NUM_FAULTS];
uint8_t FLT_MGR_irqstat_fault_counter; 
uint8_t Faults_Check_Index;
void FLT_MGR_Check_Speed(void);

void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * @test Verifies that FLT_MGR_Is_Any_Fault_Active() will return false if no fault is active
 * 
 * Fault_Active[] = all false
 */
/* Reqs: SWREQ_1950 */
void test_FLT_MGR_Is_Any_Fault_Active_When_no_faults_present(void)
{
    bool_t actual_response = true;


    /* Ensure known test state */
 
    /* Setup expected call chain */
 
    /* Call function under test */
    actual_response = FLT_MGR_Is_Any_Fault_Active();

    /* Verify test results */
    TEST_ASSERT_FALSE(actual_response);
}

/**
 * @test Verifies that FLT_MGR_Is_Any_Fault_Active() will return true if one or more faults are active
 * Fault_Active[1] = true
 */
/* Reqs: SWREQ_1950 */
void test_FLT_MGR_Is_Any_Fault_Active_When_faults_present(void)
{
    bool_t actual_response = false;

    /* Ensure known test state */
    Fault_Active[1] = true;

    /* Setup expected call chain */
 

    /* Call function under test */
    actual_response = FLT_MGR_Is_Any_Fault_Active();

    /* Verify test results */
    TEST_ASSERT_TRUE(actual_response);
}


/**
 * @test Verifies that FLT_MGR_Is_Any_Fault_Active() will return true if one or more faults are active
 * Fault_Active[0] = true
 * Fault_Active[FLT_MGR_NUM_FAULTS - 1] = true
 */
/* Reqs: SWREQ_1950 */
void test_FLT_MGR_Is_Any_Fault_Active_When_more_than_one_faults_present(void)
{
    bool_t actual_response = false;

    /* Ensure known test state */
    Fault_Active[0] = true;
    Fault_Active[FLT_MGR_NUM_FAULTS - 1] = true;
    /* Setup expected call chain */
 

    /* Call function under test */
    actual_response = FLT_MGR_Is_Any_Fault_Active();

    /* Verify test results */
    TEST_ASSERT_TRUE(actual_response);
}


/**
 * @test Verifies that FLT_MGR_irqstat_fault_counter will return to 0 after FLT_MGR_MAX_FAULT_CHECK_INDEX
 * FLT_MGR_irqstat_fault_counter = 5 (FLT_MGR_MAX_FAULT_CHECK_INDEX) and should call case case 5: FLT_MGR_Check_Speed()
 * 
/* Reqs:  */

void test_FLT_MGR_Task_Fault_Index(void)
{
    /* Ensure known test state */
    Faults_Check_Index = FLT_MGR_MAX_FAULT_CHECK_INDEX;
    detected_over_speed = true;
    detected_under_speed = false;
    clearing_over_speed = false;
    Fault_Active[FLT_OVER_SPEED] = false;
 
    /* Setup expected call chain */
    FLT_LOG_Is_Enabled_ExpectAndReturn(true); 
    FLT_LOG_Scan_Expect();
    /* FLT_MGR_Check_Speed_Expect();  */

    /* Should execute speed fault task, the last one in the cycle */

    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(3964);
    MMGR_Get_Target_Speed_ExpectAndReturn(3000);
    TMUT_MS_To_Ticks_ExpectAndReturn(4000, TICKS_IN_4000MS); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(FLT_Speed_Timestamp, TICKS_IN_4000MS, true);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_OVER_SPEED);

    /* Call function under test */
    FLT_MGR_Task();
    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT8(0, Faults_Check_Index);
}



