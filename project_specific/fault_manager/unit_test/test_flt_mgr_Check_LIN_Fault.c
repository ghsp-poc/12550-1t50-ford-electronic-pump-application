/**
 *  @file test_flt_mgr_Check_LIN_Fault.c
 *
 */
#include "unity.h"
#include "fault_logger.h"
#include "LinBus_Types.h"

/* MOCKS */
#include "mock_fault_logger.h"
#include "mock_motor_ctrl.h"
#include "mock_lin_vehicle_comm.h"
#include "mock_system_signals.h"
#include "mock_time_utils.h"
#include "mock_serial_data_buffer.h"
#include "mock_speed_meas.h"
#include "mock_motor_manager.h"
#include "mock_nvm_mgr.h"

/* STATIC's */
FLT_MGR_LIN_J2602_Err_T Lin_Fault_J2602;
Time_MS_T FLT_LIN_Fault_Timestamp;

#define TICKS_IN_1000MS (20000u)
#define TICKS_IN_500MS (10000u)

void flt_mgr_Check_LIN_Fault(void);

void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * Verifies that the function flt_mgr_Check_LIN_Fault does nothing when there is no
 * LIN fault currently active
 */
void test_flt_mgr_Check_LIN_Fault_does_nothing_when_no_fault_active(void)
{
    Lin_Fault_J2602 = FLT_MGR_J2602_NO_ERROR;

    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    flt_mgr_Check_LIN_Fault();
    
    /* Verify test results */
    TEST_ASSERT_EQUAL(FLT_MGR_J2602_NO_ERROR, Lin_Fault_J2602);
}

/**
 * Verifies that the function flt_mgr_Check_LIN_Fault will check for timer expiration when the
 * FLT_MGR_J2602_CHECKSUM_ERROR is active.
 */
void test_flt_mgr_Check_LIN_Fault_checks_for_timer_expiration_When_FLT_MGR_J2602_CHECKSUM_ERROR_active(void)
{

    /* Ensure known test state */
    Lin_Fault_J2602 = FLT_MGR_J2602_CHECKSUM_ERROR;
    FLT_LIN_Fault_Timestamp = 5000;

    /* Setup expected call chain */
    TMUT_MS_To_Ticks_ExpectAndReturn(1000, TICKS_IN_1000MS); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(5000, TICKS_IN_1000MS, false);

    /* Call function under test */
    flt_mgr_Check_LIN_Fault();

    /* Verify test results */
    TEST_ASSERT_EQUAL(FLT_MGR_J2602_CHECKSUM_ERROR, Lin_Fault_J2602);
}

/**
 * Verifies that the function flt_mgr_Check_LIN_Fault will check for timer expiration when the
 * FLT_MGR_J2602_CHECKSUM_ERROR is active and clear that fault when the timer expires
 */
void test_flt_mgr_Check_LIN_Fault_clears_FLT_MGR_J2602_CHECKSUM_ERROR_when_timer_expires(void)
{
    Lin_Fault_J2602 = FLT_MGR_J2602_CHECKSUM_ERROR;
    FLT_LIN_Fault_Timestamp = 5000;

    /* Ensure known test state */
    TMUT_MS_To_Ticks_ExpectAndReturn(1000, TICKS_IN_1000MS); 
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(5000, TICKS_IN_1000MS, true);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_LIN_CHECKSUM_ERROR);

    /* Setup expected call chain */

    /* Call function under test */
    flt_mgr_Check_LIN_Fault();

    /* Verify test results */
    TEST_ASSERT_EQUAL(FLT_MGR_J2602_NO_ERROR, Lin_Fault_J2602);
}
