/**
 *  @file test_FLT_MGR_current_faults.c
 *
 */
#include "unity.h"
#include "global.h"
#include "fault_manager.h"
#include "fault_manager_imp_defines.h"
#include "fault_manager_imp_types.h"
#include "speed_meas.h"
#include "motor_manager.h"

#include <string.h>

/* MOCKS */
#include "mock_fault_logger.h"
#include "mock_motor_ctrl.h"
#include "mock_lin_vehicle_comm.h"
#include "mock_system_signals.h"
#include "mock_time_utils.h"
#include "mock_serial_data_buffer.h"
#include "mock_speed_meas.h"
#include "mock_motor_manager.h"
#include "mock_nvm_mgr.h"


/* STATIC's */
#define TICKS_IN_1000MS (20000u)

uint32_t FLT_Current_Timestamp;

 bool_t detected_over_current;
 bool_t clearing_over_current;
 bool_t detected_under_current;
 bool_t clearing_under_current;


const flt_mgr_Fault_Params_T Fault_Parameters[FLT_MGR_NUM_FAULTS];
bool_t Fault_Active[FLT_MGR_NUM_FAULTS];

void FLT_MGR_Check_Current(void);

void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * @test Verifies that FLT_MGR_ Over current will set the detected flag and begin timing
 *  I =  15A
 */
/* Reqs: SWREQ_2010, SWREQ_2013 */
void test_FLT_MGR_Over_current_sets_DETECTED_flag(void)
{
    /* Ensure known test state */
    detected_over_current = false;
    clearing_over_current = false;
    Fault_Active[FLT_OVER_CURRENT] = false;
 
    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);
    SYSIG_Get_Current_ExpectAndReturn(150);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(3000);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);   /* Arbitrary value of system timer */
 
    /* Call function under test */
    FLT_MGR_Check_Current();

    /* Verify test results */
    TEST_ASSERT_TRUE(detected_over_current);
}

/**
 * @test Verifies that FLT_MGR_ Over voltage() will clear the detected flag if voltage drops below set point 
 *  before timer is elapsed
 *  I =  14.9
 */
/* Reqs: SWREQ_2010, SWREQ_2013 */
void test_FLT_MGR_Over_Current_when_current_drops_before_timer_done(void)
{
    /* Ensure known test state */
    detected_over_current = true;
    clearing_over_current = false;
 
    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);
    SYSIG_Get_Current_ExpectAndReturn(149);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(3000);

    /* Call function under test */
    FLT_MGR_Check_Current();

    /* Verify test results */
    TEST_ASSERT_FALSE(detected_over_current);   /* this is what we're looking for, ensure get base time next time current is over the limit */
    TEST_ASSERT_FALSE( Fault_Active[FLT_OVER_CURRENT]);
}

/**
 * @test Verifies that FLT_MGR_ Over current will set the fault when current over measurement stays long 
 *  enough
 *  I =  15A
 */
/* Reqs: SWREQ_2010, SWREQ_2013 */
void test_FLT_MGR_Over_Current_sets_fault_flag(void)
{
    /* Ensure known test state */
    detected_over_current = true;
    clearing_over_current = false;
    Fault_Active[FLT_OVER_CURRENT] = false;
    FLT_Current_Timestamp = 5000;
 
    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);
    SYSIG_Get_Current_ExpectAndReturn(150);
    TMUT_MS_To_Ticks_ExpectAndReturn(1000, TICKS_IN_1000MS); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(FLT_Current_Timestamp, TICKS_IN_1000MS, true);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(3000);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_OVER_CURRENT);

    /* Call function under test */
    FLT_MGR_Check_Current();

    /* Verify test results */
    TEST_ASSERT_FALSE(clearing_over_current);
    TEST_ASSERT_TRUE(detected_over_current);
    TEST_ASSERT_TRUE( Fault_Active[FLT_OVER_CURRENT]);
}


/**
 * @test Verifies that FLT_MGR_ Over Current will load base time into variable to clear fault
 *  enough
 *  I =  14.5A
 */
/* Reqs: SWREQ_2010, SWREQ_2013 */
void test_FLT_MGR_Over_Current_starts_timer_to_clear_over_current_fault_flag(void)
{
    /* Ensure known test state */
    detected_over_current = true;
    clearing_over_current = false;
    Fault_Active[FLT_OVER_CURRENT] = true;
    FLT_Current_Timestamp = 5000;
 
    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);
    SYSIG_Get_Current_ExpectAndReturn(145);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(3000);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);  /* Arbitrary value of system time */

    /* Call function under test */
    FLT_MGR_Check_Current();

    /* Verify test results */
    TEST_ASSERT_TRUE(clearing_over_current);
    TEST_ASSERT_TRUE( Fault_Active[FLT_OVER_CURRENT]);
}


/**
 * @test Verifies that FLT_MGR_ Over current() will clear over current fualt when current below recover limit
 *  enough time, 500ms expired
 *  I =  14.5A
 */
/* Reqs: SWREQ_2010, SWREQ_2013 */
void test_FLT_MGR_Over_Current_clears_over_voltage_fault_flag_timer_expired(void)
{
    /* Ensure known test state */
    detected_over_current = true;
    clearing_over_current = true;
    Fault_Active[FLT_OVER_CURRENT] = true;
    FLT_Current_Timestamp = 5000;
 
    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);
    SYSIG_Get_Current_ExpectAndReturn(145);
    TMUT_MS_To_Ticks_ExpectAndReturn(1000, TICKS_IN_1000MS); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(FLT_Current_Timestamp, TICKS_IN_1000MS, true);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(3000);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_OVER_CURRENT);

    /* Call function under test */
    FLT_MGR_Check_Current();

    /* Verify test results */
    TEST_ASSERT_TRUE(clearing_over_current);
    TEST_ASSERT_FALSE( Fault_Active[FLT_OVER_CURRENT]);
    TEST_ASSERT_FALSE(detected_over_current); /* make sure this is cleared so the next time V > limit, loads timer */

}

/**
 * @test Verifies that FLT_MGR_  Under Current will set the detected flag and begin timing
 *  I =  low limit, currently TBD
 */
/* Reqs: SWREQ_2010, SWREQ_2013 */
void test_FLT_MGR_Under_Current_When_under_limit(void)
{
    /* Ensure known test state */
    detected_over_current = false;
    detected_under_current = false;
    clearing_under_current = false;
    Fault_Active[FLT_UNDER_CURRENT] = false;
    Fault_Active[FLT_OVER_CURRENT] = false;

 
    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);
    SYSIG_Get_Current_ExpectAndReturn(FLT_UNDER_CURRENT_LMT); /* the spec is <= this value is a fault */
    SYSIG_Get_Actual_RPM_ExpectAndReturn(2940);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);  /* Arbitrary value of system time */

    /* Call function under test */
    FLT_MGR_Check_Current();

    /* Verify test results */
    TEST_ASSERT_TRUE(detected_under_current);
    TEST_ASSERT_FALSE(Fault_Active[FLT_UNDER_CURRENT]);
}


/**
 * @test Verifies that FLT_MGR_ Under current() will clear the detected flag if current drops below set point 
 *  before timer is elapsed
 *  I =  low limit + 1, currently TBD  
 */
/* Reqs: SWREQ_2010, SWREQ_2013 */
void test_FLT_MGR_UNDER_Current_when_in_range_before_timer_done(void)
{
    /* Ensure known test state */
    detected_under_current = true;
    clearing_under_current = false;
    Fault_Active[FLT_UNDER_CURRENT] = false;
 
    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);
    SYSIG_Get_Current_ExpectAndReturn(FLT_UNDER_CURRENT_LMT + 1); /* the spec is <= this value is a fault */
    SYSIG_Get_Actual_RPM_ExpectAndReturn(2940);

    /* Call function under test */
    FLT_MGR_Check_Current();

    /* Verify test results */
    TEST_ASSERT_FALSE(detected_under_current);
    TEST_ASSERT_FALSE( Fault_Active[FLT_UNDER_CURRENT]);
    TEST_ASSERT_FALSE(clearing_under_current);
}

/**
 * @test Verifies that FLT_MGR_ Under Current() will set the fault when  stays long 
 *  enough
 *  I =  low limit, currently TBD 
 */
/* Reqs: SWREQ_2010, SWREQ_2013 */
void test_FLT_MGR_Under_Current_sets_fault_flag(void)
{
    /* Ensure known test state */
    detected_under_current = true;
    clearing_under_current = false;
    Fault_Active[FLT_UNDER_CURRENT] = false;
    FLT_Current_Timestamp = 5000;
 
    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);
    SYSIG_Get_Current_ExpectAndReturn(FLT_UNDER_CURRENT_LMT); /* the spec is <= this value is a fault */
    TMUT_MS_To_Ticks_ExpectAndReturn(1000, TICKS_IN_1000MS); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(FLT_Current_Timestamp, TICKS_IN_1000MS, true);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(2940);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_UNDER_CURRENT);

    /* Call function under test */
    FLT_MGR_Check_Current();

    /* Verify test results */
    TEST_ASSERT_FALSE(clearing_under_current);
    TEST_ASSERT_TRUE(detected_under_current);
    TEST_ASSERT_TRUE( Fault_Active[FLT_UNDER_CURRENT]);
}

/**
 * @test Verifies that FLT_MGR_ Under Current() will set the clearing when current is in recovery 
 *  enough
 *  I =  low limit, currently TBD 
 */
/* Reqs: SWREQ_2010, SWREQ_2013 */
void test_FLT_MGR_Under_Current_sets_clearing_flag(void)
{
    /* Ensure known test state */
    detected_under_current = true;
    clearing_under_current = false;
    Fault_Active[FLT_UNDER_CURRENT] = true;
    FLT_Current_Timestamp = 5000;
 
    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);
    SYSIG_Get_Current_ExpectAndReturn(FLT_UNDER_CURRENT_CLR); /* the spec is <= this value is a fault */
    SYSIG_Get_Actual_RPM_ExpectAndReturn(2940);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);  /* Arbitrary value of system time */

    /* Call function under test */
    FLT_MGR_Check_Current();

    /* Verify test results */

    TEST_ASSERT_TRUE(clearing_under_current);
    TEST_ASSERT_TRUE(Fault_Active[FLT_UNDER_CURRENT]);
}

/**
 * @test Verifies that FLT_MGR_ Under Current() will CLEAR the UC FAULT  is in recovery 
 *  enough
 *  I =  low limit, currently TBD 
 */
/* Reqs: SWREQ_2010, SWREQ_2013 */
void test_FLT_MGR_Under_Current_CLEAR_FAULT_flag(void)
{
    /* Ensure known test state */
    detected_under_current = false;
    clearing_under_current = true;
    Fault_Active[FLT_UNDER_CURRENT] = true;
    FLT_Current_Timestamp = 5000;
 
    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);
    SYSIG_Get_Current_ExpectAndReturn(FLT_UNDER_CURRENT_CLR); /* the spec is <= this value is a fault */
    TMUT_MS_To_Ticks_ExpectAndReturn(1000, TICKS_IN_1000MS); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(FLT_Current_Timestamp, TICKS_IN_1000MS, true);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(2940);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_UNDER_CURRENT);

    /* Call function under test */
    FLT_MGR_Check_Current();

    /* Verify test results */

    TEST_ASSERT_FALSE(detected_under_current);
    TEST_ASSERT_FALSE(Fault_Active[FLT_UNDER_CURRENT]);
}

/**
 * @test Verifies that FLT_MGR_ Under Current() will not set the DETECTED flag when speed is too low
 *  
 *  I =  low limit, currently TBD 
 */
/* Reqs: SWREQ_2010, SWREQ_2013 */
void test_FLT_MGR_Under_Current_does_not_set_under_current_detected_flag_if_speed_below_enable_threshold(void)
{
    /* Ensure known test state */
    detected_under_current = false;
    clearing_under_current = false;
    Fault_Active[FLT_UNDER_CURRENT] = false;
    FLT_Current_Timestamp = 5000;
 
    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);
    SYSIG_Get_Current_ExpectAndReturn(FLT_UNDER_CURRENT_LMT - 1); /* this value is a fault, if speed (RPM) is high enough */
    SYSIG_Get_Actual_RPM_ExpectAndReturn(2938);    /* not enough speed for under current fault to be enabled */

    /* Call function under test */
    FLT_MGR_Check_Current();

    /* Verify test results */

    TEST_ASSERT_FALSE(detected_under_current);
    TEST_ASSERT_FALSE(Fault_Active[FLT_UNDER_CURRENT]);
}

/**
 * @test Verifies that FLT_MGR_ Under Current() will CLEAR the UC FAULT  is in recovery 
 *  enough
 *  I =  low limit, currently TBD 
 */
/* Reqs: SWREQ_2010, SWREQ_2013 */
void test_FLT_MGR_Under_Current_CLEAR_FAULT_flag_due_to_below_speed_threshold(void)
{
    /* Ensure known test state */
    detected_under_current = false;
    clearing_under_current = true;
    Fault_Active[FLT_UNDER_CURRENT] = true;
    FLT_Current_Timestamp = 5000;
 
    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);
    SYSIG_Get_Current_ExpectAndReturn(15); 
    TMUT_MS_To_Ticks_ExpectAndReturn(1000, TICKS_IN_1000MS); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(FLT_Current_Timestamp, TICKS_IN_1000MS, true);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(2939);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_UNDER_CURRENT);

    /* Call function under test */
    FLT_MGR_Check_Current();

    /* Verify test results */

    TEST_ASSERT_FALSE(detected_under_current);
    TEST_ASSERT_FALSE(Fault_Active[FLT_UNDER_CURRENT]);
}
