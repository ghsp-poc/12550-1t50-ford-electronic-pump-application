/**
 *  @file test_FLT_MGR_speed_faults.c
 *
 */
#include "unity.h"
#include "global.h"
#include "fault_manager.h"
#include "fault_manager_imp_defines.h"
#include "fault_manager_imp_types.h"
#include "motor_manager.h"
#include "system_signals.h"

#include <string.h>


/* MOCKS */
#include "mock_fault_logger.h"
#include "mock_motor_ctrl.h"
#include "mock_lin_vehicle_comm.h"
#include "mock_system_signals.h"
#include "mock_time_utils.h"
#include "mock_motor_manager.h"
#include "mock_serial_data_buffer.h"
#include "mock_system_signals.h"
#include "mock_nvm_mgr.h"
#include "mock_systime.h"

#define TICKS_IN_1000MS (20000u)
#define TICKS_IN_4000MS (80000u)

/* STATIC's */
Time_MS_T FLT_Speed_Timestamp;

bool_t detected_over_speed;
bool_t clearing_over_speed;
bool_t detected_under_speed;
bool_t clearing_under_speed;


const flt_mgr_Fault_Params_T Fault_Parameters[FLT_MGR_NUM_FAULTS];
bool_t Fault_Active[FLT_MGR_NUM_FAULTS];

void FLT_MGR_Check_Speed(void);

void setUp(void)
{
    FLT_MGR_General_Faults_T Fault_ID = 0;

    for (Fault_ID = 0; Fault_ID < FLT_MGR_NUM_FAULTS; Fault_ID++)
    {
        Fault_Active[Fault_ID] = false;
    }
}

void tearDown(void)
{

}

/**
 * @test Verifies that FLT_MGR_ Over speed will set the detected flag and begin timing
 *  difference is > 10%
 */
/* Reqs: SWREQ_2037 */
void test_FLT_MGR_Over_speed_sets_DETECTED_flag(void)
{
    /* Ensure known test state */
    detected_over_speed = false;
    detected_under_speed = false;
    clearing_over_speed = false;
    Fault_Active[FLT_OVER_SPEED] = false;
 
    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(3961);
    MMGR_Get_Target_Speed_ExpectAndReturn(3000);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);  /* Arbitrary value of system time */

    /* Call function under test */
    FLT_MGR_Check_Speed();

    /* Verify test results */
    TEST_ASSERT_TRUE(detected_over_speed);
    TEST_ASSERT_FALSE(detected_under_speed);
}

/**
 * @test Verifies that FLT_MGR_ Over speed will clear the detected flag when goes back to in range before timer expires
 *  difference is < 10%
 */
/* Reqs: SWREQ_2037 */
void test_FLT_MGR_Over_speed_clears_DETECTED_flag_clears_when_goes_back_in_range(void)
{
    /* Ensure known test state */
    detected_over_speed = true;
    clearing_over_speed = false;
    Fault_Active[FLT_OVER_SPEED] = false;
 
    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(3959);
    MMGR_Get_Target_Speed_ExpectAndReturn(3600);
    /* Call function under test */
    FLT_MGR_Check_Speed();

    /* Verify test results */
    TEST_ASSERT_FALSE(detected_over_speed);
    TEST_ASSERT_FALSE(clearing_over_speed);
    TEST_ASSERT_FALSE(Fault_Active[FLT_OVER_SPEED]);
}

/**
 * @test Verifies that FLT_MGR_ Over speed  will set the fault flag and begin timing
 *  Diff = 10.1%
 */
/* Reqs: SWREQ_2037 */
void test_FLT_MGR_Over_speed_sets_FAULT_flag(void)
{
    /* Ensure known test state */
    detected_over_speed = true;
    detected_under_speed = false;
    clearing_over_speed = false;
    Fault_Active[FLT_OVER_SPEED] = false;
 
    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(3964);
    MMGR_Get_Target_Speed_ExpectAndReturn(3000);
    TMUT_MS_To_Ticks_ExpectAndReturn(4000, TICKS_IN_4000MS); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(FLT_Speed_Timestamp, TICKS_IN_4000MS, true);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_OVER_SPEED);


    /* Call function under test */
    FLT_MGR_Check_Speed();

    /* Verify test results */
    TEST_ASSERT_TRUE(Fault_Active[FLT_OVER_SPEED]);
    TEST_ASSERT_FALSE(Fault_Active[FLT_UNDER_SPEED]);
    TEST_ASSERT_FALSE(detected_under_speed);
}

/**
 * @test Verifies that FLT_MGR_ Over speed  will clear the fault flag and begin timing
 *  Diff < 10%
 */
/* Reqs: SWREQ_2037 */
void test_FLT_MGR_Over_speed_clears_FAULT_flag(void)
{
    /* Ensure known test state */
    detected_over_speed = true;
    detected_under_speed = false;
    clearing_over_speed = true;
    Fault_Active[FLT_OVER_SPEED] = true;
 
    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(3956);
    MMGR_Get_Target_Speed_ExpectAndReturn(3600);
    TMUT_MS_To_Ticks_ExpectAndReturn(1000, TICKS_IN_1000MS); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(FLT_Speed_Timestamp, TICKS_IN_1000MS, true);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_OVER_SPEED);

    /* Call function under test */
    FLT_MGR_Check_Speed();

    /* Verify test results */
    TEST_ASSERT_FALSE(Fault_Active[FLT_OVER_SPEED]);
    TEST_ASSERT_FALSE(Fault_Active[FLT_UNDER_SPEED]);
    TEST_ASSERT_FALSE(detected_under_speed);
}

/**
 * @test Verifies that FLT_MGR_ Under speed will set the detected flag and begin timing
 *  difference is > 10%
 */
/* Reqs: SWREQ_2040 */
void test_FLT_MGR_Under_speed_sets_DETECTED_flag(void)
{
    /* Ensure known test state */
    detected_under_speed = false;
    detected_over_speed = false;
    clearing_under_speed = false;
    Fault_Active[FLT_UNDER_SPEED] = false;
 
    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(3235);
    MMGR_Get_Target_Speed_ExpectAndReturn(3600);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);  /* Arbitrary value of system time */

    /* Call function under test */
    FLT_MGR_Check_Speed();

    /* Verify test results */
    TEST_ASSERT_TRUE(detected_under_speed);
    TEST_ASSERT_FALSE(detected_over_speed);
}

/**
 * @test Verifies that FLT_MGR_ Under speed will clear the detected flag when goes back to in range before timer expires
 *  difference is < 10%
 */
/* Reqs: SWREQ_2040 */
void test_FLT_MGR_Under_speed_clears_DETECTED_flag_clears_when_goes_back_in_range(void)
{
    /* Ensure known test state */
    detected_under_speed = true;
    clearing_under_speed = false;
    Fault_Active[FLT_UNDER_SPEED] = false;
    Fault_Active[FLT_OVER_SPEED] = false;
 
    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(3244);
    MMGR_Get_Target_Speed_ExpectAndReturn(3600);
    /* Call function under test */
    FLT_MGR_Check_Speed();

    /* Verify test results */
    TEST_ASSERT_FALSE(detected_under_speed);
    TEST_ASSERT_FALSE(clearing_under_speed);
    TEST_ASSERT_FALSE(Fault_Active[FLT_UNDER_SPEED]);
}

/**
 * @test Verifies that FLT_MGR_ Under speed  will set the fault flag and begin timing
 *  Diff = 10.5%
 */
/* Reqs: SWREQ_2040 */
void test_FLT_MGR_Under_speed_sets_FAULT_flag(void)
{
    /* Ensure known test state */
    detected_under_speed = true;
    clearing_under_speed = false;
    detected_over_speed = false;
    Fault_Active[FLT_UNDER_SPEED] = false;
 
    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(3235);
    MMGR_Get_Target_Speed_ExpectAndReturn(3600);
    TMUT_MS_To_Ticks_ExpectAndReturn(4000, TICKS_IN_4000MS); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(FLT_Speed_Timestamp, TICKS_IN_4000MS, true);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_UNDER_SPEED);

    /* Call function under test */
    FLT_MGR_Check_Speed();

    /* Verify test results */
    TEST_ASSERT_TRUE(Fault_Active[FLT_UNDER_SPEED]);
    TEST_ASSERT_FALSE(detected_over_speed);
}

/**
 * @test Verifies that FLT_MGR_ Under speed  will clear the fault flag and begin timing
 *  Diff = 10.5%
 */
/* Reqs: SWREQ_2040 */
void test_FLT_MGR_Under_speed_clears_FAULT_flag(void)
{
    /* Ensure known test state */
    detected_under_speed = false;
    clearing_under_speed = true;
    detected_over_speed = false;
    Fault_Active[FLT_UNDER_SPEED] = true;
 
    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(3244);
    MMGR_Get_Target_Speed_ExpectAndReturn(3600);
    TMUT_MS_To_Ticks_ExpectAndReturn(1000, TICKS_IN_1000MS); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(FLT_Speed_Timestamp, TICKS_IN_1000MS, true);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_UNDER_SPEED);

    /* Call function under test */
    FLT_MGR_Check_Speed();

    /* Verify test results */
    TEST_ASSERT_FALSE(Fault_Active[FLT_UNDER_SPEED]);
    TEST_ASSERT_FALSE(detected_over_speed);
    TEST_ASSERT_FALSE(detected_under_speed);
}

/**
 * @test Verifies that FLT_MGR_ target and actual are equal will clear the fault flag set
 *  Diff = 0%
 */
/* Reqs: SWREQ_2040 */
void test_FLT_MGR_Under_speed_set_clearing_underspeedflag_when_zero_error(void)
{
    /* Ensure known test state */
    detected_under_speed = false;
    clearing_under_speed = false;
    detected_over_speed = false;
    Fault_Active[FLT_UNDER_SPEED] = true;
 
    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(3200);
    MMGR_Get_Target_Speed_ExpectAndReturn(3200);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);  /* Arbitrary value of system time */

    /* Call function under test */
    FLT_MGR_Check_Speed();

    /* Verify test results */
    TEST_ASSERT_TRUE(Fault_Active[FLT_UNDER_SPEED]);
    TEST_ASSERT_FALSE(detected_under_speed);
    TEST_ASSERT_TRUE(clearing_under_speed);
    TEST_ASSERT_FALSE(detected_under_speed);
}

/**
 * @test Verifies that FLT_MGR_ target and actual are equal will clear the fault flag set
 *  Diff = 0%
 */
/* Reqs: SWREQ_2040 */
void test_FLT_MGR_Under_speed_clears_under_speed_FAULT_flag_when_zero_error(void)
{
    /* Ensure known test state */
    clearing_under_speed = true;
    detected_over_speed = false;
    Fault_Active[FLT_UNDER_SPEED] = true;
 
    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(3200);
    MMGR_Get_Target_Speed_ExpectAndReturn(3200);
    TMUT_MS_To_Ticks_ExpectAndReturn(1000, TICKS_IN_1000MS); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(FLT_Speed_Timestamp, TICKS_IN_1000MS, true);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_UNDER_SPEED);

    /* Call function under test */
    FLT_MGR_Check_Speed();

    /* Verify test results */
    TEST_ASSERT_FALSE(Fault_Active[FLT_UNDER_SPEED]);
    TEST_ASSERT_FALSE(detected_under_speed);
    TEST_ASSERT_FALSE(clearing_under_speed);
    TEST_ASSERT_FALSE(detected_under_speed);
}

/**
 * @test Verifies that FLT_MGR_ target and actual are equal will clear the fault flag set
 *  Diff = 0%
 */
/* Reqs: SWREQ_2040 */
void test_FLT_MGR_over_speed_set_clearing_underspeedflag_when_zero_error(void)
{
    /* Ensure known test state */
    detected_under_speed = false;
    clearing_under_speed = false;
    detected_over_speed = false;
    Fault_Active[FLT_OVER_SPEED] = true;
 
    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(3200);
    MMGR_Get_Target_Speed_ExpectAndReturn(3200);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);  /* Arbitrary value of system time */

    /* Call function under test */
    FLT_MGR_Check_Speed();

    /* Verify test results */
    TEST_ASSERT_TRUE(Fault_Active[FLT_OVER_SPEED]);
    TEST_ASSERT_FALSE(detected_under_speed);
    TEST_ASSERT_TRUE(clearing_over_speed);
    TEST_ASSERT_FALSE(detected_over_speed);
}

/**
 * @test Verifies that FLT_MGR_ target and actual are equal will clear the fault flag set
 *  Diff = 0%
 */
/* Reqs: SWREQ_2037, SWREQ_2040 */
void test_FLT_MGR_over_speed_clears_under_speed_FAULT_flag_when_zero_error(void)
{
    /* Ensure known test state */
    clearing_over_speed = true;
    detected_over_speed = false;
    Fault_Active[FLT_OVER_SPEED] = true;
 
    /* Setup expected call chain */
    mc_get_comm_mode_ExpectAndReturn(MC_CLOSED_LOOP);
    SYSIG_Get_Actual_RPM_ExpectAndReturn(3200);
    MMGR_Get_Target_Speed_ExpectAndReturn(3200);
    TMUT_MS_To_Ticks_ExpectAndReturn(1000, TICKS_IN_1000MS); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(FLT_Speed_Timestamp, TICKS_IN_1000MS, true);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_OVER_SPEED);

    /* Call function under test */
    FLT_MGR_Check_Speed();

    /* Verify test results */
    TEST_ASSERT_FALSE(Fault_Active[FLT_OVER_SPEED]);
    TEST_ASSERT_FALSE(detected_under_speed);
    TEST_ASSERT_FALSE(clearing_under_speed);
    TEST_ASSERT_FALSE(detected_under_speed);
}

