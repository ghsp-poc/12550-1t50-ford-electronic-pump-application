/**
 *  @file test_FLT_MGR_Is_In_Self_Protect.c
 *
 */
#include "unity.h"
#include "global.h"
#include "fault_manager.h"
#include "fault_manager_imp_defines.h"
#include "fault_manager_imp_types.h"
#include "speed_meas.h"
#include "motor_manager.h"

#include <string.h>


/* MOCKS */
#include "mock_fault_logger.h"
#include "mock_system_signals.h"
#include "mock_lin_vehicle_comm.h"
#include "mock_motor_ctrl.h"
#include "mock_time_utils.h"
#include "mock_serial_data_buffer.h"
#include "mock_speed_meas.h"
#include "mock_motor_manager.h"
#include "mock_nvm_mgr.h"

/* STATIC's */
void FLT_MGR_Check_Irqstat( void );

bool_t FLT_MGR_Irqstat_Fault;
bool_t Fault_Active[FLT_MGR_NUM_FAULTS];
uint8_t FLT_MGR_irqstat_fault_counter;
bool_t higher_voltage_IRQSTAT_retry_active;
FLT_MGR_LIN_J2602_Err_T Lin_Fault_J2602;

void setUp(void)
{
    memset(Fault_Active, 0, sizeof(Fault_Active));
}

void tearDown(void)
{

}


/**
 * @test Verifies that FLT_MGR_Is_In_Self_Protect() will return true if voltage is over the limit
 * required to be in self protect
 */
/* Reqs: SWREQ_1941, SWREQ_1944, SWREQ_2049, SWREQ_2051, SWREQ_2053 */
void test_FLT_MGR_Is_In_Self_Protect_returns_true_when_Overvoltage_Fault_Is_Present(void)
{
    bool_t self_protect_state = false;

    /* Ensure known test state */
    Fault_Active[FLT_OVER_VOLTAGE] = true;

    /* Setup expected call chain */

    /* Call function under test */
    self_protect_state = FLT_MGR_Is_In_Self_Protect();

    /* Verify test results */
    TEST_ASSERT_EQUAL(true, self_protect_state);
 }

/**
 * @test Verifies that FLT_MGR_Is_In_Self_Protect() will return true when both temperature and voltage
 * are over the limit required to be in self protect
 */
/* Reqs: SWREQ_1941, SWREQ_1944, SWREQ_1997, SWREQ_2000, SWREQ_2003, SWREQ_2049, SWREQ_2051, SWREQ_2053 */
void test_FLT_MGR_Is_In_Self_Protect_returns_true_when_Overtemp_And_Overvoltage_Faults_Are_Present(void)
{
    bool_t self_protect_state = false;

    /* Ensure known test state */
    Fault_Active[FLT_OVER_VOLTAGE] = true;
    Fault_Active[FLT_OVER_TEMPERATURE] = true;

    /* Setup expected call chain */

    /* Call function under test */
    self_protect_state = FLT_MGR_Is_In_Self_Protect();

    /* Verify test results */
    TEST_ASSERT_EQUAL(true, self_protect_state);
 }

/**
 * @test Verifies that FLT_MGR_Is_In_Self_Protect() will return false if temperature and
 * voltage is within operating range
 */
/* Reqs: SWREQ_2048, SWREQ_2050 */
void test_FLT_MGR_Is_In_Self_Protect_returns_false_when_No_Fault_Present(void)
{
    bool_t self_protect_state = true;

    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    self_protect_state = FLT_MGR_Is_In_Self_Protect();

    /* Verify test results */
    TEST_ASSERT_EQUAL(false, self_protect_state);
}

/**
 * @test Verifies that FLT_MGR_Is_In_Self_Protect() will return false if the temperature is too high
 */
/* Reqs: SWREQ_1998, SWREQ_2001, SWREQ_2048, SWREQ_2050 */
void test_FLT_MGR_Is_In_Self_Protect_returns_false_when_only_FLT_OVER_TEMPERATURE_present(void)
{
    bool_t self_protect_state = true;

    /* Ensure known test state */
    Fault_Active[FLT_OVER_TEMPERATURE] = true;

    /* Setup expected call chain */

    /* Call function under test */
    self_protect_state = FLT_MGR_Is_In_Self_Protect();

    /* Verify test results */
    TEST_ASSERT_EQUAL(false, self_protect_state);
}

/**
 * @test Verifies that FLT_MGR_Is_In_Self_Protect() will return true if the voltage is too low
 */
/* Reqs: SWREQ_2055 */
void test_FLT_MGR_Is_In_Self_Protect_returns_true_when_under_voltage_present(void)
{
    bool_t self_protect_state = false;


    /* Ensure known test state */
    memset(Fault_Active, 0, sizeof(Fault_Active));
    Fault_Active[FLT_UNDER_VOLT_SELF_PROTECT] = true;

    /* Setup expected call chain */

    /* Call function under test */
    self_protect_state = FLT_MGR_Is_In_Self_Protect();

    /* Verify test results */
    TEST_ASSERT_TRUE(self_protect_state);
}




/**
 * Verifies that FLT_MGR_Check_Irqstat() will set FLT_MGR_Irqstat_Fault to true if a short is detected in IRQSTAT1
 * When retries are exhasted
 */
/* Reqs: SWREQ_1948 */
void test_FLT_MGR_Check_Irqstat_sets_FLT_MGR_Irqstat_Fault_true_when_short_in_IRQSTAT1(void)
{
    /* Ensure known test state */
    FLT_MGR_Irqstat_Fault = false;
    higher_voltage_IRQSTAT_retry_active = true;
    Fault_Active[FLT_UNDER_VOLT_IRQSTAT] = false;
    FLT_MGR_irqstat_fault_counter = 3u;

    /* Setup expected call chain */
    mc_get_irqstat_ExpectAndReturn(1u);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_SHORT_CIRCUIT);
    MMGR_Is_Irqstat_Actively_Clearing_ExpectAndReturn(false);
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Req_ExpectAndReturn(1000);

    /* Call function under test */
    FLT_MGR_Check_Irqstat();


    /* Verify test results */
    TEST_ASSERT_TRUE(FLT_MGR_Irqstat_Fault);
    TEST_ASSERT_EQUAL_UINT(3,IRQSTAT_FAULT_RETRIES);        /* Can use IRQSTAT_FAULT_RETRIES now, it's been tested against the spec */  
}

/**
 * Verifies that FLT_MGR_Check_Irqstat() will set FLT_MGR_Irqstat_Fault to true when no short is indicated
 * in IRQSTAT but other faults are.
 */
/* Reqs: SWREQ_1948 */
void test_FLT_MGR_Check_Irqstat_sets_FLT_MGR_Irqstat_Fault_true_when_no_short_in_IRQSTAT1_but_other_faults(void)
{
    /* Ensure known test state */
    FLT_MGR_Irqstat_Fault = false;
    higher_voltage_IRQSTAT_retry_active = true;
    Fault_Active[FLT_UNDER_VOLT_IRQSTAT] = false;
    FLT_MGR_irqstat_fault_counter = 3u;

    /* Setup expected call chain */
    mc_get_irqstat_ExpectAndReturn(0xFFC0u); /* all bits are set in IRQSTAT1 and IRQSTAT2 except the short circuit bits */
    MMGR_Is_Irqstat_Actively_Clearing_ExpectAndReturn(false);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_SHORT_CIRCUIT);
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Req_ExpectAndReturn(1000);

    /* Call function under test */
    FLT_MGR_Check_Irqstat();

    /* Verify test results */
    TEST_ASSERT_TRUE(FLT_MGR_Irqstat_Fault);
}

/**
 * Verifies that FLT_MGR_Check_Irqstat() will set FLT_MGR_Irqstat_Fault to false if no fault is detected in IRQSTAT1
 */
/* Reqs: SWREQ_1948 */
void test_FLT_MGR_Check_Irqstat_sets_FLT_MGR_Irqstat_Fault_false_when_no_fault_in_IRQSTAT1(void)
{
    /* Ensure known test state */
    FLT_MGR_Irqstat_Fault = true;

    /* Setup expected call chain */
    mc_get_irqstat_ExpectAndReturn(0u); /* no fault bits are set in IRQSTAT */
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_SHORT_CIRCUIT);
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Req_ExpectAndReturn(1000);
    mc_get_motor_state_ExpectAndReturn(MC_IDLE);

    /* Call function under test */
    FLT_MGR_Check_Irqstat();


    /* Verify test results */
    TEST_ASSERT_FALSE(FLT_MGR_Irqstat_Fault);
}

/**
 * Verifies that FLT_MGR_Check_Irqstat() will clear IRQSTAT counter and flag indicating in higher voltage retry mode
 *  when motor is running and no IRQSTAT faults
 */
/* Reqs: SWREQ_1948 */
void test_FLT_MGR_Check_Irqstat_clears_counter_for_IRQSTAT_retry_and_flag_indicating_higher_V_retry(void)
{
    /* Ensure known test state */
    FLT_MGR_Irqstat_Fault = true;
    higher_voltage_IRQSTAT_retry_active = true;
    FLT_MGR_irqstat_fault_counter = 3u;

    /* Setup expected call chain */
    mc_get_irqstat_ExpectAndReturn(0u); /* no fault bits are set in IRQSTAT */
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_SHORT_CIRCUIT);
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Req_ExpectAndReturn(1000);
    mc_get_motor_state_ExpectAndReturn(MC_RUNNING);
    /* Call function under test */
    FLT_MGR_Check_Irqstat();


    /* Verify test results */
    TEST_ASSERT_FALSE(FLT_MGR_Irqstat_Fault);
    TEST_ASSERT_FALSE(higher_voltage_IRQSTAT_retry_active);
    TEST_ASSERT_EQUAL_UINT(0,FLT_MGR_irqstat_fault_counter); 
}



/**
 * Verifies that FLT_MGR_Check_Irqstat() will set FLT_MGR_Irqstat_Fault to false if no fault is detected in IRQSTAT1
 */
/* Reqs: SWREQ_1948 */
void test_FLT_MGR_Check_Irqstat_retries_once_at_low_V(void)
{
    /* Ensure known test state */
    FLT_MGR_Irqstat_Fault = false;
    FLT_MGR_irqstat_fault_counter = 0;
    higher_voltage_IRQSTAT_retry_active = false;
    Fault_Active[FLT_UNDER_VOLT_SELF_PROTECT] = false;

    /* Setup expected call chain */
    mc_get_irqstat_ExpectAndReturn(0x200u); /* no fault bits are set in IRQSTAT */
    MMGR_Is_Irqstat_Actively_Clearing_ExpectAndReturn(false);
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Req_ExpectAndReturn(1000);
    MMGR_Reset_Irqstat_Expect();
    /* Call function under test */
    FLT_MGR_Check_Irqstat();

    /* Verify test results */
    TEST_ASSERT_FALSE(FLT_MGR_Irqstat_Fault);
    TEST_ASSERT_EQUAL_UINT(1,FLT_MGR_irqstat_fault_counter);
}

/**
 * Verifies that FLT_MGR_Check_Irqstat() will increment the FLT_MGR_irqstat_fault_counter when IRQSTAT != 0 and no self protect fault 
 */
/* Reqs: SWREQ_1948 */
void test_FLT_MGR_Check_Irqstat_retries_once_at_High_V(void)
{
    /* Ensure known test state */
    FLT_MGR_Irqstat_Fault = false;
    FLT_MGR_irqstat_fault_counter = 0;
    higher_voltage_IRQSTAT_retry_active = true;
    Fault_Active[FLT_UNDER_VOLT_SELF_PROTECT] = false;

    /* Setup expected call chain */
    mc_get_irqstat_ExpectAndReturn(0x200u); /* no fault bits are set in IRQSTAT */
    MMGR_Is_Irqstat_Actively_Clearing_ExpectAndReturn(false);
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Req_ExpectAndReturn(1000);
    MMGR_Reset_Irqstat_Expect();
    /* Call function under test */
    FLT_MGR_Check_Irqstat();

    /* Verify test results */
    TEST_ASSERT_FALSE(FLT_MGR_Irqstat_Fault);
    TEST_ASSERT_EQUAL_UINT(1,FLT_MGR_irqstat_fault_counter);
}

/**
 * Verifies that FLT_MGR_Check_Irqstat() will NOT increment the FLT_MGR_irqstat_fault_counter when IRQSTAT != 0 and no self protect fault 
 */
/* Reqs: SWREQ_1948 */
void test_FLT_MGR_Check_Irqstat_no_retries_when_self_protect_low_V(void)
{
    /* Ensure known test state */
    FLT_MGR_Irqstat_Fault = false;
    FLT_MGR_irqstat_fault_counter = 0;
    higher_voltage_IRQSTAT_retry_active = false;
    Fault_Active[FLT_UNDER_VOLT_SELF_PROTECT] = true;

    /* Setup expected call chain */
    mc_get_irqstat_ExpectAndReturn(0x200u); /* no fault bits are set in IRQSTAT */
    MMGR_Is_Irqstat_Actively_Clearing_ExpectAndReturn(false);
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Req_ExpectAndReturn(1000);
    /* Call function under test */
    FLT_MGR_Check_Irqstat();

    /* Verify test results */
    TEST_ASSERT_FALSE(FLT_MGR_Irqstat_Fault);
    TEST_ASSERT_EQUAL_UINT(0,FLT_MGR_irqstat_fault_counter);
}

/**
 * Verifies that FLT_MGR_Check_Irqstat() will clear all IRQSTAT counters and flags when commanded off not due to lin failure
 */
/* Reqs: SWREQ_1948 */
void test_FLT_MGR_Check_Irqstat_clears_irqstat_counters_when_commanded_off(void)
{
    /* Ensure known test state */
    FLT_MGR_Irqstat_Fault = true;
    FLT_MGR_irqstat_fault_counter = 2;
    higher_voltage_IRQSTAT_retry_active = true;
    Fault_Active[FLT_UNDER_VOLT_SELF_PROTECT] = false;
    Lin_Fault_J2602 = FLT_MGR_J2602_NO_ERROR;

    /* Setup expected call chain */
    mc_get_irqstat_ExpectAndReturn(0x200u); /* no fault bits are set in IRQSTAT */
    MMGR_Is_Irqstat_Actively_Clearing_ExpectAndReturn(false);
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Req_ExpectAndReturn(0);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
    /* Call function under test */
    FLT_MGR_Check_Irqstat();

    /* Verify test results */
    TEST_ASSERT_FALSE(FLT_MGR_Irqstat_Fault);
    TEST_ASSERT_FALSE(higher_voltage_IRQSTAT_retry_active);
    TEST_ASSERT_EQUAL_UINT(0,FLT_MGR_irqstat_fault_counter);
}

/**
 * Verifies that FLT_MGR_Check_Irqstat() will NOT clear all IRQSTAT counters and flags when commanded off but there is a lin failure
 */
/* Reqs: SWREQ_1948 */
void test_FLT_MGR_Check_Irqstat_does_not_clear_irqstat_counters_when_commanded_off_by_lin_fault(void)
{
    /* Ensure known test state */
    FLT_MGR_Irqstat_Fault = true;
    FLT_MGR_irqstat_fault_counter = 2;
    higher_voltage_IRQSTAT_retry_active = true;
    Fault_Active[FLT_UNDER_VOLT_SELF_PROTECT] = false;
    Lin_Fault_J2602 = FLT_MGR_J2602_CHECKSUM_ERROR;

    /* Setup expected call chain */
    mc_get_irqstat_ExpectAndReturn(0x200u); /* no fault bits are set in IRQSTAT */
    MMGR_Is_Irqstat_Actively_Clearing_ExpectAndReturn(false);
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Req_ExpectAndReturn(0);
    VCOMM_Loss_Of_Comm_ExpectAndReturn(false);
     MMGR_Reset_Irqstat_Expect();

    /* Call function under test */
    FLT_MGR_Check_Irqstat();

    /* Verify test results */
    TEST_ASSERT_TRUE(FLT_MGR_Irqstat_Fault);
    TEST_ASSERT_TRUE(higher_voltage_IRQSTAT_retry_active);
    TEST_ASSERT_EQUAL_UINT(3,FLT_MGR_irqstat_fault_counter);
}

/**
 * Verifies that FLT_MGR_Check_Irqstat() does nothing if not commanded off and motor manager is actively clearing IRQstat register
 */
/* Reqs: SWREQ_1948 */
void test_FLT_MGR_Check_Irqstat_does_nothing_if_actively_clearing_irqstat(void)
{
    /* Ensure known test state */
    FLT_MGR_Irqstat_Fault = false;
    FLT_MGR_irqstat_fault_counter = 2;
    higher_voltage_IRQSTAT_retry_active = true;
    Fault_Active[FLT_UNDER_VOLT_SELF_PROTECT] = false;
    Lin_Fault_J2602 = FLT_MGR_J2602_NO_ERROR;

    /* Setup expected call chain */
    mc_get_irqstat_ExpectAndReturn(0x200u); /* no fault bits are set in IRQSTAT */
    MMGR_Is_Irqstat_Actively_Clearing_ExpectAndReturn(true);
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Req_ExpectAndReturn(1000);
   

    /* Call function under test */
    FLT_MGR_Check_Irqstat();

    /* Verify test results */
    TEST_ASSERT_FALSE(FLT_MGR_Irqstat_Fault);
    TEST_ASSERT_TRUE(higher_voltage_IRQSTAT_retry_active);
    TEST_ASSERT_EQUAL_UINT(2,FLT_MGR_irqstat_fault_counter);
}