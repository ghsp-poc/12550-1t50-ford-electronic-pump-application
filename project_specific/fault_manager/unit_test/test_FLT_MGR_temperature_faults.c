/**
 *  @file test_FLT_MGR_temperature_faults.c
 *
 */
#include "unity.h"
#include "global.h"
#include "fault_manager.h"
#include "fault_manager_imp_defines.h"
#include "fault_manager_imp_types.h"
#include "speed_meas.h"
#include "motor_manager.h"

#include <string.h>

#define TICKS_IN_1000MS (20000u)
/* MOCKS */
#include "mock_fault_logger.h"
#include "mock_motor_ctrl.h"
#include "mock_lin_vehicle_comm.h"
#include "mock_system_signals.h"
#include "mock_time_utils.h"
#include "mock_serial_data_buffer.h"
#include "mock_speed_meas.h"
#include "mock_motor_manager.h"
#include "mock_nvm_mgr.h"


/* STATIC's */
bool_t detected_over_temp;
bool_t clearing_over_temp;
bool_t detected_over_temp_self_p;
bool_t clearing_over_temp_self_p;

Time_MS_T FLT_Temperature_Timestamp;
Time_MS_T FLT_Temperature_Self_Protect_Timestamp;

const flt_mgr_Fault_Params_T Fault_Parameters[FLT_MGR_NUM_FAULTS];
bool_t Fault_Active[FLT_MGR_NUM_FAULTS];

void FLT_MGR_Check_Temperature(void);

void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * Verifies that FLT_MGR_Check_Temperature() will set the detected_over_temp flag if
 * Micro T > 155C per fault ID 31 and 32 of Rev 5 of the faults spreadsheet.
 */
/* Reqs: SWREQ_2025 and SWREQ_2028 */
void test_FLT_MGR_Over_Temperature(void)
{
    /* Ensure known test state */
    detected_over_temp = false;
 
    /* Setup expected call chain */
    SYSIG_Get_CPU_Temp_ExpectAndReturn(155);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);   /* Arbitrary value of system timer */

    /* Call function under test */
    FLT_MGR_Check_Temperature();

    /* Verify test results */
    TEST_ASSERT_TRUE(detected_over_temp);
}

/**
 * Verifies that FLT_MGR_Check_Temperature() will clear the detected_over_temp flag if
 * Micro T < 150C per fault ID 31 and 32 of Rev 5 of the faults spreadsheet.
 */
/* Reqs: SWREQ_2025 and SWREQ_2028 */
void test_FLT_MGR_Over_Temperature_clears_detected_flag_when_voltage_drops_below_set_limit(void)
{
    /* Ensure known test state */
    detected_over_temp = false;
 
    /* Setup expected call chain */
    SYSIG_Get_CPU_Temp_ExpectAndReturn(149);

    /* Call function under test */
    FLT_MGR_Check_Temperature();

    /* Verify test results */
    TEST_ASSERT_FALSE(detected_over_temp);
}

/**
 * Verifies that FLT_MGR_Check_Temperature() will set the over temp fault flag if
 * Micro T > 155C per fault ID 31 and 32 when timer expires
 */
/* Reqs: SWREQ_2025 and SWREQ_2028 */
void test_FLT_MGR_Over_Temperature_sets_over_temp_fault_flag(void)
{
    /* Ensure known test state */
    detected_over_temp = true;
    FLT_Temperature_Timestamp = 1500;
 
    /* Setup expected call chain */
    SYSIG_Get_CPU_Temp_ExpectAndReturn(155);
    TMUT_MS_To_Ticks_ExpectAndReturn(1000, TICKS_IN_1000MS); /* Made up value for the number of ticks in 1000 ms for test */    
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(FLT_Temperature_Timestamp, TICKS_IN_1000MS, true);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_OVER_TEMPERATURE);

    /* Call function under test */
    FLT_MGR_Check_Temperature();

    /* Verify test results */
    TEST_ASSERT_TRUE(Fault_Active[FLT_OVER_TEMPERATURE]);
}

/**
 * @test Verifies that FLT_MGR_Check_Temperature() will set the clearing flag when  
 *   Micro T == 149C per fault ID 31 and 32 when timer expires
 */
/* Reqs: SWREQ_1993, SWREQ_1995 */
void test_FLT_MGR_Over_Temperature_sets_clearing_flag_when_drops_to_clear_limit(void)
{
    /* Ensure known test state */
    detected_over_temp = true;
    Fault_Active[FLT_OVER_TEMPERATURE] = true;
    clearing_over_temp = false;
 
    /* Setup expected call chain */
    SYSIG_Get_CPU_Temp_ExpectAndReturn(149);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);   /* Arbitrary value of system timer */

    /* Call function under test */
    FLT_MGR_Check_Temperature();

    /* Verify test results */
    TEST_ASSERT_TRUE(Fault_Active[FLT_OVER_TEMPERATURE]);
    TEST_ASSERT_TRUE(clearing_over_temp);
}

/**
 * @test Verifies that FLT_MGR_Check_Temperature() will clear the over temp fault flage
 *   Micro T == 149C per fault ID 31 and 32 when timer expires
 */
/* Reqs: SWREQ_1993, SWREQ_1995 */
void test_FLT_MGR_Over_Temperature_clears_fault_flag(void)
{
    /* Ensure known test state */
    detected_over_temp = true;
    Fault_Active[FLT_OVER_TEMPERATURE] = true;
    clearing_over_temp = true;
    FLT_Temperature_Timestamp = 2000;
 
    /* Setup expected call chain */
    SYSIG_Get_CPU_Temp_ExpectAndReturn(149);
    TMUT_MS_To_Ticks_ExpectAndReturn(1000, TICKS_IN_1000MS); /* Made up value for the number of ticks in 1000 ms for test */        
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(FLT_Temperature_Timestamp, TICKS_IN_1000MS, true);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_OVER_TEMPERATURE);

    /* Call function under test */
    FLT_MGR_Check_Temperature();

    /* Verify test results */
    TEST_ASSERT_FALSE(Fault_Active[FLT_OVER_TEMPERATURE]);

}
