/**
 *  @file test_FLT_MGR_Set_Lin_Fault_Callback
 *
 */
#include "unity.h"
#include "global.h"
#include "fault_manager.h"
#include "fault_manager_imp_defines.h"
#include "fault_manager_imp_types.h"
#include "LinBus_Types.h"
#include "speed_meas.h"
#include "motor_manager.h"

#include <string.h>


/* MOCKS */
#include "mock_fault_logger.h"
#include "mock_motor_ctrl.h"
#include "mock_lin_vehicle_comm.h"
#include "mock_system_signals.h"
#include "mock_time_utils.h"
#include "mock_serial_data_buffer.h"
#include "mock_speed_meas.h"
#include "mock_motor_manager.h"
#include "mock_nvm_mgr.h"

/* STATIC's */
FLT_MGR_LIN_J2602_Err_T Lin_Fault_J2602;
Time_MS_T FLT_LIN_Fault_Timestamp;


void setUp(void)
{
    Lin_Fault_J2602 = FLT_MGR_J2602_NO_ERROR;
    FLT_LIN_Fault_Timestamp = 0;
}

void tearDown(void)
{

}




/**
 * @test Verifies that test_FLT_MGR_Set_Lin_Callback_checksum_error() will return 0xA0 when 
 * LinBusIf_ERR_CRC
 */
/* Reqs: SWREQ_1960, SWREQ_1963 */
void test_FLT_MGR_Set_Lin_Callback_checksum_error(void)
{
  
    /* Ensure known test state */
    Lin_Fault_J2602 = FLT_MGR_J2602_NO_ERROR;

    /* Setup expected call chain */
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_NO_ERROR);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_LIN_CHECKSUM_ERROR);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);  /* Arbitrary value of system time */

    /* Call function under test */
    FLT_MGR_Set_Lin_Fault_Callback(LinBusIf_ERR_CRC);

    /* Verify test results */
    TEST_ASSERT_EQUAL( 0xA0u, Lin_Fault_J2602);
    TEST_ASSERT_EQUAL_UINT32(3456, FLT_LIN_Fault_Timestamp);
}

/**
 * @test Verifies that test_FLT_MGR_Set_Lin_Callback_checksum_error() will return 0x80 when
 * LinBusIf_ERR_BUS_COLLISION 
 */
/* Reqs: SWREQ_1954, SWREQ_1957 */
void test_FLT_MGR_Set_Lin_Callback_data_error(void)
{
  
    /* Ensure known test state */
    Lin_Fault_J2602 = FLT_MGR_J2602_NO_ERROR;

    /* Setup expected call chain */
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_NO_ERROR);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_LIN_DATA_ERROR);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);  /* Arbitrary value of system time */

    /* Call function under test */
    FLT_MGR_Set_Lin_Fault_Callback(LinBusIf_ERR_BUS_COLLISION);

    /* Verify test results */
    TEST_ASSERT_EQUAL( 0x80u, Lin_Fault_J2602);
    TEST_ASSERT_EQUAL_UINT32(3456, FLT_LIN_Fault_Timestamp);
}

/**
 * @test Verifies that test_FLT_MGR_Set_Lin_Callback_parity_error() will return 0xE0 when
 * LinBusIf_ERR_PID_PARITY 
 */
/* Reqs: SWREQ_1972, SWREQ_1975 */
void test_FLT_MGR_Set_Lin_Callback_parity_error(void)
{
  
    /* Ensure known test state */
    Lin_Fault_J2602 = FLT_MGR_J2602_NO_ERROR;

    /* Setup expected call chain */
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_NO_ERROR);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_LIN_ID_PARITY_ERROR);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);  /* Arbitrary value of system time */

    /* Call function under test */
    FLT_MGR_Set_Lin_Fault_Callback(LinBusIf_ERR_PID_PARITY);

    /* Verify test results */
    TEST_ASSERT_EQUAL( 0xE0u, Lin_Fault_J2602);
}

/**
 * @test Verifies that test_FLT_MGR_Set_Lin_Callback_framing_error() will return  0xC0 when
 * LinBusIf_ERR_FRAMING_ERROR
 */
/* Reqs: SWREQ_1966, SWREQ_1969 */
void test_FLT_MGR_Set_Lin_Callback_framing_error(void)
{
  
    /* Ensure known test state */
    Lin_Fault_J2602 = FLT_MGR_J2602_NO_ERROR;

    /* Setup expected call chain */
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_NO_ERROR);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_LIN_BIT_FIELD_FRAMING_ERROR);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);  /* Arbitrary value of system time */

    /* Call function under test */
    FLT_MGR_Set_Lin_Fault_Callback(LinBusIf_ERR_FRAMING_ERROR);

    /* Verify test results */
    TEST_ASSERT_EQUAL( 0xC0u, Lin_Fault_J2602);
}

/**
 * @test Verifies that test_FLT_MGR_Set_Lin_Callback_reset_error() will return  0x20 when
 * LinBusIf_ERR_INIT
 * 
 */
void test_FLT_MGR_Set_Lin_Callback_reset_error(void)
{
  
    /* Ensure known test state */
    Lin_Fault_J2602 = FLT_MGR_J2602_NO_ERROR;

    /* Setup expected call chain */
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_NO_ERROR);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_NO_ERROR);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);  /* Arbitrary value of system time */

    /* Call function under test */
    FLT_MGR_Set_Lin_Fault_Callback(LinBusIf_ERR_INIT);

    /* Verify test results */
    TEST_ASSERT_EQUAL(  0x20u, Lin_Fault_J2602);
}


/**
 * @test Verifies that test_FLT_MGR_Set_Lin_Callback_reset_error() will return  0x80 when
 * LinBusIf_ERR_SYNC_FAIL
 *
 *   5.4.1.4 of J2602-2
 */
void test_FLT_MGR_Set_Lin_Callback_sync_feild_error(void)
{
  
    /* Ensure known test state */
    Lin_Fault_J2602 = FLT_MGR_J2602_NO_ERROR;

    /* Setup expected call chain */
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_NO_ERROR);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_LIN_DATA_ERROR);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);  /* Arbitrary value of system time */

    /* Call function under test */
    FLT_MGR_Set_Lin_Fault_Callback(LinBusIf_ERR_SYNC_FAIL);

    /* Verify test results */
    TEST_ASSERT_EQUAL(  0x80u, Lin_Fault_J2602);
}
