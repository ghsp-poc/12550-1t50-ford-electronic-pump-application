/**
 *  @file test_FLT_MGR_voltage_faults.c
 *
 */
#include "unity.h"
#include "global.h"
#include "fault_logger.h"
#include "fault_manager.h"
#include "fault_manager_imp_defines.h"
#include "fault_manager_imp_types.h"
#include "speed_meas.h"
#include "motor_manager.h"

#include <string.h>

/* MOCKS */
#include "mock_fault_logger.h"
#include "mock_motor_ctrl.h"
#include "mock_lin_vehicle_comm.h"
#include "mock_system_signals.h"
#include "mock_time_utils.h"
#include "mock_serial_data_buffer.h"
#include "mock_speed_meas.h"
#include "mock_motor_manager.h"
#include "mock_nvm_mgr.h"

/* STATIC's */

#define TICKS_IN_1000MS (20000u)
#define TICKS_IN_500MS (10000u)


Time_Ticks_T FLT_Current_Timestamp;
Time_Ticks_T FLT_Voltage_Timestamp;
Time_Ticks_T FLT_Under_Voltage_Self_Protect_Timestamp;
Time_MS_T FLT_Under_Voltage_IRQSTAT_Retry_Timestamp;

bool_t detected_over_voltage;
bool_t clearing_over_voltage;
bool_t detected_under_voltage;
bool_t clearing_under_voltage;
bool_t detected_under_voltage_self_p;
bool_t clearing_under_voltage_self_p;
bool_t clearing_under_voltage_IRQ;


const flt_mgr_Fault_Params_T Fault_Parameters[FLT_MGR_NUM_FAULTS];
bool_t Fault_Active[FLT_MGR_NUM_FAULTS];

void FLT_MGR_Check_Voltage(void);

void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * @test Verifies that FLT_MGR_ Over voltage() will set the detected flag and begin timing
 *  V =  18.6V 
 */
/* Reqs: SWREQ_2019, SWREQ_2022 */
void test_FLT_MGR_Over_Voltage_When_voltage_over_limit(void)
{
    /* Ensure known test state */
    detected_over_voltage = false;
    clearing_over_voltage = false;
    clearing_under_voltage_IRQ = false;
    FLT_Current_Timestamp = 1000;
    FLT_Voltage_Timestamp = FLT_Current_Timestamp;
 
    /* Setup expected call chain */
    SYSIG_Get_Voltage_ExpectAndReturn(186);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);   /* Arbitrary value of system timer */
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);   /* Arbitrary value of system timer */
    /* Call function under test */
    FLT_MGR_Check_Voltage();

    /* Verify test results */
    TEST_ASSERT_TRUE(detected_over_voltage);
}

/**
 * @test Verifies that FLT_MGR_ Over voltage() will clear the detected flag if voltage drops below set point 
 *  before timer is elapsed
 *  V =  17.0 V 
 */
/* Reqs: SWREQ_2015, SWREQ_2017, SWREQ_2019, SWREQ_2022 */
void test_FLT_MGR_Over_Voltage_when_voltage_drops_before_timer_done(void)
{
    /* Ensure known test state */
    detected_over_voltage = true;
    clearing_over_voltage = false;
    clearing_under_voltage_IRQ = false;
 
    /* Setup expected call chain */
    SYSIG_Get_Voltage_ExpectAndReturn(170);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);   /* Arbitrary value of system timer */


    /* Call function under test */
    FLT_MGR_Check_Voltage();

    /* Verify test results */
    TEST_ASSERT_FALSE(detected_over_voltage);
    TEST_ASSERT_FALSE( Fault_Active[FLT_OVER_VOLTAGE]);
}

/**
 * @test Verifies that FLT_MGR_ Over voltage() will set the fault when voltage stays long 
 *  enough
 *  V =  18.6V
 */
/* Reqs: SWREQ_2019, SWREQ_2022 */
void test_FLT_MGR_Over_Voltage_sets_fault_flag(void)
{
    /* Ensure known test state */
    detected_over_voltage = true;
    clearing_over_voltage = false;
    clearing_under_voltage_IRQ = false;
    Fault_Active[FLT_OVER_VOLTAGE] = false;
    FLT_Voltage_Timestamp = 5000;
    FLT_Voltage_Timestamp = 1000;
    FLT_Current_Timestamp = 2000;
 
    /* Setup expected call chain */
    SYSIG_Get_Voltage_ExpectAndReturn(186);
    TMUT_MS_To_Ticks_ExpectAndReturn(1000, TICKS_IN_1000MS); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(FLT_Voltage_Timestamp, TICKS_IN_1000MS, true);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_OVER_VOLTAGE);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);   /* Arbitrary value of system timer */   

    /* Call function under test */
    FLT_MGR_Check_Voltage();

    /* Verify test results */
    TEST_ASSERT_FALSE(clearing_over_voltage);
    TEST_ASSERT_TRUE(detected_over_voltage);
    TEST_ASSERT_TRUE(Fault_Active[FLT_OVER_VOLTAGE]);
}


/**
 * @test Verifies that FLT_MGR_ Over voltage() will load base time into variable to clear fault
 *  enough
 *  V =  16.4V
 */
/* Reqs: SWREQ_2015, SWREQ_2017, SWREQ_2019, SWREQ_2022 */
void test_FLT_MGR_Over_Voltage_starts_timer_to_clear_over_voltage_fault_flag(void)
{
    /* Ensure known test state */
    detected_over_voltage = true;
    clearing_over_voltage = false;
    clearing_under_voltage_IRQ = false;
    Fault_Active[FLT_OVER_VOLTAGE] = true;
    FLT_Voltage_Timestamp = 5000;
 
    /* Setup expected call chain */
    SYSIG_Get_Voltage_ExpectAndReturn(164);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);   /* Arbitrary value of system timer */
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);   /* Arbitrary value of system timer */   

    /* Call function under test */
    FLT_MGR_Check_Voltage();

    /* Verify test results */
    TEST_ASSERT_TRUE(clearing_over_voltage);
    TEST_ASSERT_TRUE( Fault_Active[FLT_OVER_VOLTAGE]);
}


/**
 * @test Verifies that FLT_MGR_ Over voltage() will clear over voltage fualt when voltage below recover limit
 *  enough time, 500ms expired
 *  V =  16.4V
 */
/* Reqs: SWREQ_2015, SWREQ_2017*/
void test_FLT_MGR_Over_Voltage_clears_over_voltage_fault_flag_timer_expired(void)
{
    /* Ensure known test state */
    detected_over_voltage = true;
    clearing_over_voltage = true;
    clearing_under_voltage_IRQ = false;
    Fault_Active[FLT_OVER_VOLTAGE] = true;
    FLT_Voltage_Timestamp = 5000;
 
    /* Setup expected call chain */
    SYSIG_Get_Voltage_ExpectAndReturn(164);
    TMUT_MS_To_Ticks_ExpectAndReturn(500, TICKS_IN_500MS); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(FLT_Voltage_Timestamp, TICKS_IN_500MS, true);
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_OVER_VOLTAGE);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);   /* Arbitrary value of system timer */   

    /* Call function under test */
    FLT_MGR_Check_Voltage();

    /* Verify test results */
    TEST_ASSERT_TRUE(clearing_over_voltage);
    TEST_ASSERT_FALSE(Fault_Active[FLT_OVER_VOLTAGE]);
    TEST_ASSERT_FALSE(detected_over_voltage); /* make sure this is cleared so the next time V > limit, loads timer */
}

/**
 * @test Verifies that FLT_MGR_  Under voltage will set the detected flag and begin timing
 *  V =  9.4V 
 */
/* Reqs: SWREQ_2025, SWREQ_2028 */
void test_FLT_MGR_Under_Voltage_When_voltage_under_limit(void)
{
    /* Ensure known test state */
    detected_under_voltage = false;
    clearing_under_voltage_IRQ = false;
    Fault_Active[FLT_UNDER_VOLTAGE] = false;

 
    /* Setup expected call chain */
    SYSIG_Get_Voltage_ExpectAndReturn(94);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);  /* Arbitrary value of system time */

    /* Call function under test */
    FLT_MGR_Check_Voltage();

    /* Verify test results */
    TEST_ASSERT_TRUE(detected_under_voltage);
    TEST_ASSERT_FALSE(Fault_Active[FLT_UNDER_VOLTAGE]);
}


/**
 * @test Verifies that FLT_MGR_ Under voltage() will clear the detected flag if voltage drops below set point 
 *  before timer is elapsed
 *  V =  10.0 V 
 */
/* Reqs: SWREQ_2025, SWREQ_2028 */
void test_FLT_MGR_UNDER_Voltage_when_voltage_drops_before_timer_done(void)
{
    /* Ensure known test state */
    detected_under_voltage = true;
    clearing_under_voltage = false;
    clearing_under_voltage_IRQ = false;
    Fault_Active[FLT_UNDER_VOLTAGE] = false;
 
    /* Setup expected call chain */
    SYSIG_Get_Voltage_ExpectAndReturn(100);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);  /* Arbitrary value of system time */

    /* Call function under test */
    FLT_MGR_Check_Voltage();

    /* Verify test results */
    TEST_ASSERT_FALSE(detected_under_voltage);
    TEST_ASSERT_FALSE( Fault_Active[FLT_UNDER_VOLTAGE]);
    TEST_ASSERT_FALSE(clearing_under_voltage);
}

/**
 * @test Verifies that FLT_MGR_ Undervoltage() will set the fault when voltage stays long 
 *  enough
 *  V =  9.4V
 */
/* Reqs: SWREQ_2025, SWREQ_2028 */
void test_FLT_MGR_Under_Voltage_sets_fault_flag(void)
{
    /* Ensure known test state */
    detected_under_voltage = true;
    clearing_under_voltage = false;
    clearing_under_voltage_IRQ = false;    
    Fault_Active[FLT_UNDER_VOLTAGE] = false;
    FLT_Voltage_Timestamp = 5000;
 
    /* Setup expected call chain */
    SYSIG_Get_Voltage_ExpectAndReturn(94);
    TMUT_MS_To_Ticks_ExpectAndReturn(1000, TICKS_IN_1000MS); /* Made up value for the number of ticks in 1000 ms for test */    
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(FLT_Voltage_Timestamp, TICKS_IN_1000MS, true);
    FLT_LOG_Record_Fault_Active_Expect(FLT_UNDER_VOLTAGE);

    /* Call function under test */
    FLT_MGR_Check_Voltage();

    /* Verify test results */
    
    TEST_ASSERT_TRUE(detected_under_voltage);
    TEST_ASSERT_TRUE( Fault_Active[FLT_UNDER_VOLTAGE]);
}


/**
 * @test Verifies that FLT_MGR_ Undervoltage() Self protect will set the detected flage when 
 *    Voltage =  6.9V
 */
/* Reqs: SWREQ_2052, SWREQ_2055 */
void test_FLT_MGR_Under_Voltage_Self_Protect_sets_DETECTED_flag(void)
{
    /* Ensure known test state */
    detected_under_voltage = false;
    detected_under_voltage_self_p = false;
    clearing_under_voltage_self_p = false;
    clearing_under_voltage_IRQ = false;
    Fault_Active[FLT_UNDER_VOLT_SELF_PROTECT] = false;
    Fault_Active[FLT_OVER_VOLTAGE] = false;
    Fault_Active[FLT_UNDER_VOLTAGE] = false;
 
    /* Setup expected call chain */
    SYSIG_Get_Voltage_ExpectAndReturn(69);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);  /* Arbitrary value of system time */
    
    /* Call function under test */
    FLT_MGR_Check_Voltage();

    /* Verify test results */
    TEST_ASSERT_FALSE(clearing_under_voltage_self_p);
    TEST_ASSERT_TRUE(detected_under_voltage_self_p);
    TEST_ASSERT_FALSE( Fault_Active[FLT_UNDER_VOLT_SELF_PROTECT]);
}

/**
 * @test Verifies that FLT_MGR_ Undervoltage() Self protect will set the fault flag when 
 *    Voltage =  6.9V and after being detected 
 */
/* Reqs: SWREQ_2025, SWREQ_2028, SWREQ_2052, SWREQ_2055 */
void test_FLT_MGR_Under_Voltage_Self_Protect_sets_FAULT_flag(void)
{
    /* Ensure known test state */
    detected_under_voltage_self_p = true;
    clearing_under_voltage_self_p = false;
    clearing_under_voltage_IRQ = false;    

    Fault_Active[FLT_UNDER_VOLT_SELF_PROTECT] = false;
    Fault_Active[FLT_OVER_VOLTAGE] = false;
    Fault_Active[FLT_UNDER_VOLTAGE] = false;

    FLT_Under_Voltage_Self_Protect_Timestamp = 5000;
 
    /* Setup expected call chain */
    SYSIG_Get_Voltage_ExpectAndReturn(69);
    TMUT_MS_To_Ticks_ExpectAndReturn(1000, TICKS_IN_1000MS); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(FLT_Under_Voltage_Self_Protect_Timestamp, TICKS_IN_1000MS, true);
    FLT_LOG_Record_Fault_Active_Expect(FLT_UNDER_VOLT_SELF_PROTECT);

    /* Call function under test */
    FLT_MGR_Check_Voltage();

    /* Verify test results */
    
    TEST_ASSERT_TRUE(detected_under_voltage_self_p);
    TEST_ASSERT_TRUE( Fault_Active[FLT_UNDER_VOLT_SELF_PROTECT]);
}

/**
 * @test Verifies that FLT_MGR_ Undervoltage() Self protect will set the clearing flag when 
 *    Voltage =  7.6V 
 */
/* Reqs: SWREQ_2025, SWREQ_2028, SWREQ_2052, SWREQ_2055 */
void test_FLT_MGR_Under_Voltage_Self_Protect_sets_clearing_flag(void)
{
    /* Ensure known test state */
    detected_under_voltage = false;
    detected_under_voltage_self_p = true;
    clearing_under_voltage_self_p = false;
    clearing_under_voltage_IRQ = false;    
    Fault_Active[FLT_UNDER_VOLT_SELF_PROTECT] = true;
    Fault_Active[FLT_OVER_VOLTAGE] = false;
    FLT_Under_Voltage_Self_Protect_Timestamp = 5000;
    Fault_Active[FLT_UNDER_VOLTAGE] = false;

 
    /* Setup expected call chain */
    SYSIG_Get_Voltage_ExpectAndReturn(76);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);  /* Arbitrary value of system time */
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);  /* Arbitrary value of system time */

    /* Call function under test */
    FLT_MGR_Check_Voltage();

    /* Verify test results */
    TEST_ASSERT_TRUE(clearing_under_voltage_self_p);
    TEST_ASSERT_TRUE(detected_under_voltage);
    TEST_ASSERT_TRUE( Fault_Active[FLT_UNDER_VOLT_SELF_PROTECT]);

}

/**
 * @test Verifies that FLT_MGR_ Undervoltage() Self protect will clear the fault flag
 *    Voltage =  7.6V 
 */
/* Reqs: SWREQ_2025, SWREQ_2028, SWREQ_2052, SWREQ_2055 */
void test_FLT_MGR_Under_Voltage_Self_Protect_clears_FAULT_flag(void)
{
    /* Ensure known test state */
    detected_under_voltage = false;
    detected_under_voltage_self_p = true;
    clearing_under_voltage_self_p = true;
    clearing_under_voltage_IRQ = false;    
    Fault_Active[FLT_UNDER_VOLT_SELF_PROTECT] = true;
    Fault_Active[FLT_OVER_VOLTAGE] = false;
    FLT_Under_Voltage_Self_Protect_Timestamp = 5000;
    Fault_Active[FLT_UNDER_VOLTAGE] = false;
 
    /* Setup expected call chain */
    SYSIG_Get_Voltage_ExpectAndReturn(76);

    TMUT_MS_To_Ticks_ExpectAndReturn(500, TICKS_IN_500MS); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(FLT_Under_Voltage_Self_Protect_Timestamp, TICKS_IN_500MS, true); /* under voltage begines to detect */
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_UNDER_VOLT_SELF_PROTECT);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);  /* Arbitrary value of system time */

    /* Call function under test */
    FLT_MGR_Check_Voltage();

    /* Verify test results */
    TEST_ASSERT_TRUE(clearing_under_voltage_self_p);
    TEST_ASSERT_TRUE(detected_under_voltage);
}

/**
 * @test Verifies that FLT_MGR_ Undervoltage will be detected
 *    When Voltage is at the borderline between under voltage, and under voltage self protect
 *    A voltage below this should go into under voltate self protect
 */
/* Reqs: SWREQ_2025, SWREQ_2028, SWREQ_2052, SWREQ_2055 */
void test_FLT_MGR_Under_Voltage_at_limit_SP(void)
{
     /* Ensure known test state */
    detected_under_voltage = false;
    clearing_under_voltage_IRQ = false;    
    Fault_Active[FLT_UNDER_VOLTAGE] = false;

 
    /* Setup expected call chain */
    SYSIG_Get_Voltage_ExpectAndReturn(94);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);  /* Arbitrary value of system time */

    /* Call function under test */
    FLT_MGR_Check_Voltage();

    /* Verify test results */
    TEST_ASSERT_TRUE(detected_under_voltage);
    TEST_ASSERT_FALSE(Fault_Active[FLT_UNDER_VOLTAGE]);

}

/**
 * @test Verifies that FLT_MGR_ Undervoltage or undervoltage Self protect will be detected
 *    When Voltage is at the borderline
 */
/* Reqs: SWREQ_2025, SWREQ_2028, SWREQ_2052, SWREQ_2055 */
void test_FLT_MGR_Under_Voltage_at_limit(void)
{
    /* Ensure known test state */
    detected_under_voltage = false;
    detected_under_voltage_self_p = false;
    clearing_under_voltage_self_p = false;
    clearing_under_voltage = false;
    clearing_under_voltage_IRQ = false;    
    Fault_Active[FLT_UNDER_VOLT_SELF_PROTECT] = false;
    Fault_Active[FLT_OVER_VOLTAGE] = false;
    FLT_Under_Voltage_Self_Protect_Timestamp = 5000;
    Fault_Active[FLT_UNDER_VOLTAGE] = false;

 
    /* Setup expected call chain */
    SYSIG_Get_Voltage_ExpectAndReturn(FLT_UNDER_VOLT_SELF_P_LMT + 1);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);  /* Arbitrary value of system time */

    /* Call function under test */
    FLT_MGR_Check_Voltage();

    /* Verify test results */
    TEST_ASSERT_FALSE(detected_under_voltage_self_p);
    TEST_ASSERT_TRUE(detected_under_voltage);
    TEST_ASSERT_FALSE( Fault_Active[FLT_UNDER_VOLT_SELF_PROTECT]);
}


/**
 * @test Verifies that IRQSTAT fault caused by undervoltage be cleared 
 *    
 */
/* Reqs: SWREQ_2025, SWREQ_2028, SWREQ_2052, SWREQ_2055 */
void test_FLT_MGR_IRQ_Under_Voltage_Disabled_sets_when_voltage_lower(void)
{
    /* Ensure known test state */
    detected_under_voltage = false;
    detected_under_voltage_self_p = false;
    clearing_under_voltage_IRQ = false;    
    
    Fault_Active[FLT_UNDER_VOLTAGE] = false;
    Fault_Active[FLT_UNDER_VOLT_IRQSTAT] = false;
    STATIC bool_t clearing_under_voltage_IRQ = false;
 
    /* Setup expected call chain */
    SYSIG_Get_Voltage_ExpectAndReturn(98);


    /* Call function under test */
    FLT_MGR_Check_Voltage();

    /* Verify test results */
    TEST_ASSERT_FALSE(clearing_under_voltage_IRQ);
    TEST_ASSERT_TRUE(Fault_Active[FLT_UNDER_VOLT_IRQSTAT]);
}

/**
 * @test Verifies that IRQSTAT fault caused by undervoltage
 *    When Voltage is at the borderline
 */
/* Reqs: SWREQ_2025, SWREQ_2028, SWREQ_2052, SWREQ_2055 */
void test_FLT_MGR_IRQ_Under_Voltage_Disabled_clears_when_voltage_higher_clearing_under_voltage_IRQ_true(void)
{
    /* Ensure known test state */
    detected_under_voltage = false;
    detected_under_voltage_self_p = false;
    clearing_under_voltage_IRQ = false;    
    
    Fault_Active[FLT_UNDER_VOLTAGE] = false;
    Fault_Active[FLT_UNDER_VOLT_IRQSTAT] = true;
 
    /* Setup expected call chain */
    SYSIG_Get_Voltage_ExpectAndReturn(101);
    TMUT_Get_Base_Time_Ticks_ExpectAndReturn(3456);  /* Arbitrary value of system time */


    /* Call function under test */
    FLT_MGR_Check_Voltage();

    /* Verify test results */
    TEST_ASSERT_TRUE(clearing_under_voltage_IRQ);
    TEST_ASSERT_TRUE(Fault_Active[FLT_UNDER_VOLT_IRQSTAT]);
}


/**
 * @test Verifies that IRQSTAT fault clears when timer is elapsed  
 *    When Voltage is at the borderline
 */
/* Reqs: SWREQ_2025, SWREQ_2028, SWREQ_2052, SWREQ_2055 */
void test_FLT_MGR_IRQ_Under_Voltage_Disabled_clears_when_timer_elapsed(void)
{
    /* Ensure known test state */
    detected_under_voltage = false;
    detected_under_voltage_self_p = false;
    clearing_under_voltage_IRQ = true;
    
    Fault_Active[FLT_UNDER_VOLT_IRQSTAT] = true;

    FLT_Under_Voltage_IRQSTAT_Retry_Timestamp = 5000;

    /* Setup expected call chain */
    SYSIG_Get_Voltage_ExpectAndReturn(101);
    TMUT_MS_To_Ticks_ExpectAndReturn(500, TICKS_IN_500MS); /* Made up value for the number of ticks in 1000 ms for test */
    TMUT_Has_Time_Elapsed_Ticks_ExpectAndReturn(FLT_Under_Voltage_IRQSTAT_Retry_Timestamp, TICKS_IN_500MS, true); /* under voltage begines to detect */

    /* Call function under test */
    FLT_MGR_Check_Voltage();

    /* Verify test results */
    TEST_ASSERT_FALSE(Fault_Active[FLT_UNDER_VOLT_IRQSTAT]);
}