#ifndef FAULT_MANAGER_IMP_TYPES_H
#define FAULT_MANAGER_IMP_TYPES_H

#include "time_utils_types.h"

/**
 *  @file fault_manager_imp_types.h
 *
 *  @copyright 2019 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential@n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup fault_manager_imp 1T50 Motor Manager Implementation
 *  @{
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "global.h"

/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/
typedef struct
{
    uint8_t         index;
    Time_Ticks_T    set_ms;
    Time_Ticks_T    clear_ms;
} flt_mgr_Fault_Params_T;

/** @} doxygen end group */
#endif /* FAULT_MANAGER_IMP_TYPES_H */
