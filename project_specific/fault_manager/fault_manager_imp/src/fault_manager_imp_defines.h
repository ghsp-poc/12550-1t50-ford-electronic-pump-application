#ifndef MOTOR_MANAGER_IMP_DEFINES_H
#define MOTOR_MANAGER_IMP_DEFINES_H
/**
 *  @file fault_manager_imp_defines.h
 *
 *  @ref fault_manager_imp
 *
 *  @copyright 2019 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup fault_manager_imp 1T50 Motor Manager Implementation
 *  @{
 *
 *      @page motor_manager_imp_defines_api 1T50 Motor Manager defines Interface
 *
 *      Private API used internal to the implementation of the 1T50 Motor Manager
 *      module.
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "global.h"

/*===========================================================================*
 * Exported Preprocessor \#define Constants
 *===========================================================================*/

#define FLT_OVER_TEMP_LMT                       ((int16_t)155)  /* FAULTS_0342  ID 22, 23 */
#define FLT_OVER_TEMP_LMTCLR                    ((int16_t)150)  /* FAULTS_0342  ID 22, 23 */

#define FLT_OVER_VOLTAGE_LMT                    (185u)  /* tenth volt units, upper limit with HW tolerance */
#define FLT_OVER_VOLTAGE_CLEAR_LMT              (165u)  /* tenth volt units */

#define FLT_UNDER_VOLTAGE_LMT                   (95u)   /* Set when below this value, in tenth volt units */
#define FLT_UNDER_VOLTAGE_CLR                   (100u)  /* tenth volt units */
#define FLT_UNDER_VOLT_SELF_P_LMT               (70u)   /* Must be > this to set undervoltage fault, and causes self protect mode */
#define FLT_UNDER_VOLT_SELF_P_CLR               (75u)   /* Must be > this to set undervoltage fault, and causes self protect mode */
#define FLT_UNDER_VOLT_SELF_P_CLR_BOOST         (10u)   /* Must be > this to set undervoltage fault, after failed restart retries at FLT_UNDER_VOLT_SELF_P_CLR */
#define FLT_UNDER_VOLT_IRQ_BOOST_V              (100u)   /* Must be >= this to clear undervoltage self protect, if IRQ stat retries at FLT_UNDER_VOLT_SELF_P_CLR failed to start after IRQSTAT_FAULT_RETRIES attempts */

#define FLT_OVER_CURRENT_LMT                    (150u)  /* FAULTS_0348 tenth A units */
#define FLT_OVER_CURRENT_CLR                    (145u)  /* FAULTS_0348 tenth A units */

#define FLT_UNDER_CURRENT_LMT                   (25u)   /* tenth A units */
#define FLT_UNDER_CURRENT_CLR                   (27u)   /* tenth A units */
#define FLT_UNDER_CURRENT_ENABLE_SPEED          (2940u)  /* RPM */

#define FLT_SPEED_PCT_TOLERANCE                 (100u)  /* Diagnostic speed error limit (.1 % units) */

#define FLT_MGR_LIN_FAULT_TIMEOUT_MS            (1000u) /* Time without a reported LIN fault before clearing the fault */

#define IRQSTAT_FAULT_RETRIES  (3u)

#define FLT_MGR_MAX_FAULT_CHECK_INDEX (5u)

/*===========================================================================*
 * Exported Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/** @} doxygen end group */
#endif /* MOTOR_MANAGER_IMP_DEFINES_H */
