/**
 *  @file system_signals.c
 *
 *  @ref system_signals_api
 *
 *  @copyright 2019 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup system_signals_imp System Signals Implementation
 *  @{
 *
 *      System specific implementation of the @ref system_signals_api
 *
 *      @page TRACE Traceability
 *        - Design Document(s):
 *          - None
 *
 *        - Applicable Standards (in order of precedence: highest first):
 *          - @http{sharepoint/sandbox/ghspdev/LaunchPad/EngineeringDepartment/Elec_proj_mgmt/Revision%20Controlled%20Library/Software%20Process/Work%20Instructions/Coding%20Standard.docx,
 *                  "GHSP Coding Standard"}
 *
 *      @page DFS Deviations from Standards
 *        - None
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "adc.h"
#include "adc_lists.h"
#include "global.h"
#include "motor_ctrl.h"
#include "system_signals.h"
#include "uds.h"
#include "usr_cfg.h"
#include "speed_meas.h"
#include "lin_vehicle_comm.h"

/*===========================================================================*
 * Local Preprocessor \#define Constants
 *===========================================================================*/


/* Temperature conversion defines */
#define COUNTS_AT_25C       ((int32_t)2540)
#define TEMPERATURE_SLOPE   ((int32_t)31)
#define TEMP_25_X_1000      ((int32_t)25000)

#define SYSIG_CPU_TEMP_OVERRIDE_CLEAR (INT16_MIN)
#define SYSIG_CURRENT_OVERRIDE_CLEAR  (INT16_MIN)

/* Voltage conversion defines */
#define VSUPPLY_DIVIDER  (((uint32_t)12))

/** Experimentally determined value for the number of times IDLE is "active" per interval on a completely unloaded system */
#define IDLE_UNLOADED_MAX_COUNTS ((uint32_t)3219u)

/*===========================================================================*
 * Local Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Local Type Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Const Object Definitions
 *===========================================================================*/

/*===========================================================================*
 * Local Function Prototypes
 *===========================================================================*/

/*===========================================================================*
 * Local Object Definitions
 *===========================================================================*/
STATIC bool_t sysig_Use_CPU_Temp_Override = false;
STATIC int16_t sysig_Override_CPU_Temp = 0;

STATIC bool_t sysig_Use_Current_Override = false;
STATIC int16_t sysig_Override_Current = 0;

STATIC uint32_t idle_loop_counts = 0u;
STATIC uint8_t idle_percent = 0u;

#ifdef ENABLE_UNLOADED_CPU_CHRONOMETRICS
STATIC uint32_t average_idle_loop_count = 0u;
STATIC bool_t first_idle_measurement = true;
#endif

/*===========================================================================*
 * Function Definitions
 *===========================================================================*/
bool_t SYSIG_Init(void)
{
    adc_lists_set_user_channel(0u,ADC_CHNO_TEMP);

    return true;
}


int16_t SYSIG_Get_CPU_Temp(void)
{
    int16_t temperature_deg_C;

    if (sysig_Use_CPU_Temp_Override)
    {
        temperature_deg_C = sysig_Override_CPU_Temp;
    }
    else
    {
        int32_t local_var_int_32;
        uint16_t temperature_a2d_counts;

        temperature_a2d_counts = adc_get_user_data((uint8_t)0 );

        local_var_int_32 = COUNTS_AT_25C - (int32_t)temperature_a2d_counts;
        local_var_int_32 = local_var_int_32 * (int32_t)A2D_V_PER_COUNT * (int32_t)100;
        local_var_int_32 = local_var_int_32 / TEMPERATURE_SLOPE;
        local_var_int_32 = local_var_int_32 + TEMP_25_X_1000;
        local_var_int_32 += (int32_t)500;   /* round to the nearest deg. C */

        local_var_int_32 /=  (int32_t)1000;


        temperature_deg_C = (int16_t)local_var_int_32;
    }

    return (temperature_deg_C);    /* in deg. C Units */
}

int16_t SYSIG_Get_Current(void)
{
    int16_t current;

    if (sysig_Use_Current_Override)
    {
        current = sysig_Override_Current;
    }
    else
    {
        int32_t temp_32;

        current = mc_get_supply_current();
        temp_32 = (int32_t) current;
        temp_32 *= (int32_t)100;  /* to get into .1A units */
        temp_32 /= (int32_t)256;  /* 256 counts from A-D = 1A. */
        temp_32 += (int32_t)5;    /* for rounding */
        temp_32 /= (int32_t)10;

        current = (int16_t)temp_32;
    }

    return (current);               /* in .1A units */
}

uint16_t SYSIG_Get_Voltage(void)
{
    uint16_t volts;
    uint32_t temp_U32;

    volts = adc_get_supply_voltage();
    temp_U32 = (uint32_t)volts * (uint32_t)A2D_V_PER_COUNT;
    temp_U32 *= VSUPPLY_DIVIDER;
    temp_U32 /= (uint32_t)1000;
    temp_U32 += (uint32_t)5;        /* for rounding to nearest 10th Volt */ 
    temp_U32 /= (uint32_t)10;

    volts = (uint16_t)temp_U32;

    return (volts);                 /* in .1V units. */
}

uint16_t SYSIG_Get_Actual_RPM(void)
{
    return((VCOMM_RPM_T)spdmeas_get_cur_speed() / SYSIG_NUM_POLE_PAIRS);
}

/*-----------------------*\
 * Diagnostics functions *
\*-----------------------*/
UDS_Response_Code_T SYSIG_Diag_Get_CPU_Temp(Serial_Data_Buffer_T * const p_Resp_SDB)
{
    UDS_Response_Code_T result = UDS_RESP_POSITIVE;

    SDB_Serialize_I16(p_Resp_SDB, SYSIG_Get_CPU_Temp());

    return result;
}

UDS_Response_Code_T SYSIG_Diag_Override_CPU_Temp(Serial_Data_Buffer_T * const p_Req_SDB)
{
    int16_t override_temp = SDB_Deserialize_I16(p_Req_SDB);
    UDS_Response_Code_T result = UDS_RESP_POSITIVE;

    if (SYSIG_CPU_TEMP_OVERRIDE_CLEAR == override_temp)
    {
        sysig_Use_CPU_Temp_Override = false;
        sysig_Override_CPU_Temp = 0;
    }
    else
    {
        sysig_Use_CPU_Temp_Override = true;
        sysig_Override_CPU_Temp = override_temp;
    }

    return result;
}

UDS_Response_Code_T SYSIG_Diag_Get_Current(Serial_Data_Buffer_T * const p_Resp_SDB)
{
    UDS_Response_Code_T result = UDS_RESP_POSITIVE;

    SDB_Serialize_I16(p_Resp_SDB, SYSIG_Get_Current());

    return result;
}

UDS_Response_Code_T SYSIG_Diag_Override_Current(Serial_Data_Buffer_T * const p_Req_SDB)
{
    int16_t override_temp = SDB_Deserialize_I16(p_Req_SDB);
    UDS_Response_Code_T result = UDS_RESP_POSITIVE;

    if (SYSIG_CURRENT_OVERRIDE_CLEAR == override_temp)
    {
        sysig_Use_Current_Override = false;
        sysig_Override_Current = 0;
    }
    else
    {
        sysig_Use_Current_Override = true;
        sysig_Override_Current = override_temp;
    }

    return result;
}

void SYSIG_Idle_Count_Handler(void)
{
    idle_loop_counts++;
}

void SYSIG_CPU_Load_Task(void)
{
    static uint32_t prev_idle_loop_count = 0u;
    static uint32_t delta_idle_loop_count;

    delta_idle_loop_count = idle_loop_counts - prev_idle_loop_count;
    prev_idle_loop_count = idle_loop_counts;

#ifndef ENABLE_UNLOADED_CPU_CHRONOMETRICS
    if ( delta_idle_loop_count > IDLE_UNLOADED_MAX_COUNTS )
    {
        delta_idle_loop_count = IDLE_UNLOADED_MAX_COUNTS;
    }

    idle_percent = (uint8_t)(((uint32_t)UINT8_MAX * delta_idle_loop_count) / IDLE_UNLOADED_MAX_COUNTS);
#else /* ENABLE_UNLOADED_CPU_CHRONOMETRICS */
    if (first_idle_measurement)
    {
        average_idle_loop_count = delta_idle_loop_count;
        first_idle_measurement = false;
    }
    else
    {
        average_idle_loop_count = (average_idle_loop_count + delta_idle_loop_count) / 2u;
    }
#endif /* ENABLE_UNLOADED_CPU_CHRONOMETRICS */
}

UDS_Response_Code_T SYSIG_Diag_Get_CPU_Idle(Serial_Data_Buffer_T * const p_Resp_SDB)
{
    UDS_Response_Code_T result = UDS_RESP_POSITIVE;

    SDB_Serialize_U8(p_Resp_SDB, idle_percent);

    return result;
}

/** @} doxygen end group */
