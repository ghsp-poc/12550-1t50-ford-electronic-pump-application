/**
 *  @file test_SYS_SIG_Get_CPU_Temp.c
 *
 */
#include "unity.h"
#include "global.h"
#include "system_signals.h"


/* MOCKS */
#include "mock_adc.h"
#include "mock_adc_lists.h"
#include "mock_motor_ctrl.h"
#include "mock_serial_data_buffer.h"
#include "mock_speed_meas.h"

/* STATIC's */
STATIC bool_t sysig_Use_CPU_Temp_Override;
STATIC int16_t sysig_Override_CPU_Temp;

void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * @test Verifies that the function SYSIG_Get_CPU_Temp will return the temperature
 * of -40 when the ADC reads the value of 2869
 */
/* Reqs: SWREQ_1930, SWREQ_1931 */
void test_SYSIG_Get_CPU_Temp_returns_neg40_when_adc_reads_2869(void)
{
    int16_t result = 0;

    /* Ensure known test state */
    sysig_Use_CPU_Temp_Override = false;

    /* Setup expected call chain */
    adc_get_user_data_ExpectAndReturn(0, 2869); /* User data 0 is the CPU temperature */

    /* Call function under test */
    result = SYSIG_Get_CPU_Temp();

    /* Verify test results */
    TEST_ASSERT_INT16_WITHIN(1, -40, result);
}

/**
 * @test Verifies that the function SYSIG_Get_CPU_Temp will return the temperature
 * of -25 when the ADC reads the value of 2793
 */
/* Reqs: SWREQ_1930, SWREQ_1931 */
void test_SYSIG_Get_CPU_Temp_returns_neg25_when_adc_reads_2793(void)
{
    int16_t result = 0;

    /* Ensure known test state */
    sysig_Use_CPU_Temp_Override = false;

    /* Setup expected call chain */
    adc_get_user_data_ExpectAndReturn(0, 2793); /* User data 0 is the CPU temperature */

    /* Call function under test */
    result = SYSIG_Get_CPU_Temp();

    /* Verify test results */
    TEST_ASSERT_INT16_WITHIN(1, -25, result);
}

/**
 * @test Verifies that the function SYSIG_Get_CPU_Temp will return the temperature
 * of -24 when the ADC reads the value of 2788
 */
/* Reqs: SWREQ_1930, SWREQ_1931 */
void test_SYSIG_Get_CPU_Temp_returns_neg24_when_adc_reads_2788(void)
{
    int16_t result = 0;

    /* Ensure known test state */
    sysig_Use_CPU_Temp_Override = false;

    /* Setup expected call chain */
    adc_get_user_data_ExpectAndReturn(0, 2788); /* User data 0 is the CPU temperature */

    /* Call function under test */
    result = SYSIG_Get_CPU_Temp();

    /* Verify test results */
    TEST_ASSERT_INT16_WITHIN(1, -24, result);
}

/**
 * @test Verifies that the function SYSIG_Get_CPU_Temp will return the temperature
 * of 0 when the ADC reads the value of 2666
 */
/* Reqs: SWREQ_1930, SWREQ_1931 */
void test_SYSIG_Get_CPU_Temp_returns_0_when_adc_reads_2666(void)
{
    int16_t result = 0;

    /* Ensure known test state */
    sysig_Use_CPU_Temp_Override = false;

    /* Setup expected call chain */
    adc_get_user_data_ExpectAndReturn(0, 2666); /* User data 0 is the CPU temperature */

    /* Call function under test */
    result = SYSIG_Get_CPU_Temp();

    /* Verify test results */
    TEST_ASSERT_INT16_WITHIN(1, 0, result);
}

/**
 * @test Verifies that the function SYSIG_Get_CPU_Temp will return the temperature
 * of 0 when the ADC reads the value of 2661
 */
/* Reqs: SWREQ_1930, SWREQ_1931 */
void test_SYSIG_Get_CPU_Temp_returns_1_when_adc_reads_2661(void)
{
    int16_t result = 0;

    /* Ensure known test state */
    sysig_Use_CPU_Temp_Override = false;

    /* Setup expected call chain */
    adc_get_user_data_ExpectAndReturn(0, 2661); /* User data 0 is the CPU temperature */

    /* Call function under test */
    result = SYSIG_Get_CPU_Temp();

    /* Verify test results */
    TEST_ASSERT_INT16_WITHIN(1, 1, result);
}

/**
 * @test Verifies that the function SYSIG_Get_CPU_Temp will return 25 degrees C when the
 * ADC reds a value of 2539 counts
 */
/* Reqs: SWREQ_1930, SWREQ_1931 */
void test_SYSIG_Get_CPU_Temp_returns_25_when_adc_reads_2539(void)
{
    int16_t result = 0;

    /* Ensure known test state */
    sysig_Use_CPU_Temp_Override = false;

    /* Setup expected call chain */
    adc_get_user_data_ExpectAndReturn(0, 2539); /* User data 0 is the CPU temperature */

    /* Call function under test */
    result = SYSIG_Get_CPU_Temp();

    /* Verify test results */
    TEST_ASSERT_INT16_WITHIN(1, 25, result);
}

/**
 * @test Verifies that the function SYSIG_Get_CPU_Temp will return the temperature
 * of 100 when the ADC reads the value of 2158
 */
/* Reqs: SWREQ_1930, SWREQ_1931 */
void test_SYSIG_Get_CPU_Temp_returns_100_when_adc_reads_2158(void)
{
    int16_t result = 0;

    /* Ensure known test state */
    sysig_Use_CPU_Temp_Override = false;

    /* Setup expected call chain */
    adc_get_user_data_ExpectAndReturn(0, 2158); /* User data 0 is the CPU temperature */

    /* Call function under test */
    result = SYSIG_Get_CPU_Temp();

    /* Verify test results */
    TEST_ASSERT_INT16_WITHIN(1, 100, result);
}

/**
 * @test Verifies that the function SYSIG_Get_CPU_Temp will return the temperature
 * of 130 when the ADC reads the value of 2006
 */
/* Reqs: SWREQ_1930, SWREQ_1931 */
void test_SYSIG_Get_CPU_Temp_returns_130_when_adc_reads_2006(void)
{
    int16_t result = 0;

    /* Ensure known test state */
    sysig_Use_CPU_Temp_Override = false;

    /* Setup expected call chain */
    adc_get_user_data_ExpectAndReturn(0, 2006); /* User data 0 is the CPU temperature */

    /* Call function under test */
    result = SYSIG_Get_CPU_Temp();

    /* Verify test results */
    TEST_ASSERT_INT16_WITHIN(1, 130, result);
}

/**
 * @test Verifies that the function SYSIG_Get_CPU_Temp will return the temperature
 * of 135 when the ADC reads the value of 1980
 */
/* Reqs: SWREQ_1930, SWREQ_1931 */
void test_SYSIG_Get_CPU_Temp_returns_135_when_adc_reads_1980(void)
{
    int16_t result = 0;

    /* Ensure known test state */
    sysig_Use_CPU_Temp_Override = false;

    /* Setup expected call chain */
    adc_get_user_data_ExpectAndReturn(0, 1980); /* User data 0 is the CPU temperature */

    /* Call function under test */
    result = SYSIG_Get_CPU_Temp();

    /* Verify test results */
    TEST_ASSERT_INT16_WITHIN(1, 135, result);
}

/**
 * @test Verifies that the function SYSIG_Get_CPU_Temp will return the temperature
 * of 140 when the ADC reads the value of 1955
 */
/* Reqs: SWREQ_1930, SWREQ_1931 */
void test_SYSIG_Get_CPU_Temp_returns_140_when_adc_reads_1955(void)
{
    int16_t result = 0;

    /* Ensure known test state */
    sysig_Use_CPU_Temp_Override = false;

    /* Setup expected call chain */
    adc_get_user_data_ExpectAndReturn(0, 1955); /* User data 0 is the CPU temperature */

    /* Call function under test */
    result = SYSIG_Get_CPU_Temp();

    /* Verify test results */
    TEST_ASSERT_INT16_WITHIN(1, 140, result);
}

/**
 * @test Verifies that the function SYSIG_Get_CPU_Temp will return the override
 * CPU temperature when there is an override present.
 */
/* Reqs: SWREQ_1930, SWREQ_1931 */
void test_SYSIG_Get_CPU_Temp_returns_override_when_override_present(void)
{
    int16_t result = 0;

    /* Ensure known test state */
    sysig_Use_CPU_Temp_Override = true;
    sysig_Override_CPU_Temp = 122;

    /* Setup expected call chain */

    /* Call function under test */
    result = SYSIG_Get_CPU_Temp();

    /* Verify test results */
    TEST_ASSERT_INT16_WITHIN(1, 122, result);
}
