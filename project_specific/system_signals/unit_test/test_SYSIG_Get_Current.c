/**
 *  @file test_SYSIG_Get_Current.c
 *
 */
#include "unity.h"
#include "global.h"
#include "system_signals.h"

/* MOCKS */
#include "mock_adc.h"
#include "mock_adc_lists.h"
#include "mock_motor_ctrl.h"
#include "mock_serial_data_buffer.h"
#include "mock_speed_meas.h"

/* STATIC's */

void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * @test Verifies that SYSIG_Get_Current() will return 0 when the current
 * is reported as 0.
 */
/* Reqs: SWREQ_2009, SWREQ_2012 */
void test_SYSIG_Get_Current_returns_0_when_currnt_0(void)
{
    uint16_t result = UINT16_MAX;

    /* Ensure known test state */

    /* Setup expected call chain */
    mc_get_supply_current_ExpectAndReturn(0);

    /* Call function under test */
    result = SYSIG_Get_Current();
    
    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT16(0, result);
}

/**
 * @test Verifies that SYSIG_Get_Current() will return 4 when the current
 * is reported as 115. Rounds down from 4.49  
 */
/* Reqs: SWREQ_2009, SWREQ_2012 */
void test_SYSIG_Get_Current_returns_4_when_currnt_115(void)
{
    uint16_t result = UINT16_MAX;

    /* Ensure known test state */

    /* Setup expected call chain */
    mc_get_supply_current_ExpectAndReturn(115);

    /* Call function under test */
    result = SYSIG_Get_Current();
    
    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT16(4, result);
}


/**
 * @test Verifies that SYSIG_Get_Current() will return 5  when the current
 * is reported as 140. Rounds up from 4.7 to the nearest Ampre. 
 */
/* Reqs: SWREQ_2009, SWREQ_2012 */
void test_SYSIG_Get_Current_returns_5_when_currnt_140(void)
{
    uint16_t result = UINT16_MAX;

    /* Ensure known test state */

    /* Setup expected call chain */
    mc_get_supply_current_ExpectAndReturn(140);

    /* Call function under test */
    result = SYSIG_Get_Current();
    
    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT16(5, result);
}


/**
 * @test Verifies that SYSIG_Get_Current() will return 30  when the current
 * is reported as 780. Rounds up from 4.7 to the nearest Ampre. 
 */
/* Reqs: SWREQ_2009, SWREQ_2012 */
void test_SYSIG_Get_Current_returns_30_when_currnt_780(void)
{
    uint16_t result = UINT16_MAX;

    /* Ensure known test state */

    /* Setup expected call chain */
    mc_get_supply_current_ExpectAndReturn(780);

    /* Call function under test */
    result = SYSIG_Get_Current();
    
    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT16(30, result);
}
