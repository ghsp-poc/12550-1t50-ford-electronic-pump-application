CANoe.LIN configuration for demonstration of stress functionality:
-----------------------------------------------------------------------------------
WARNING: this configuration cannot be run in simulated mode

Stressing via LIN Stress Interactive Generator
============================
Architecture:
 - Stress functionality can be controlled via GUI of LIN Stress IG on the 
   "Stress via LIN Stress IG" desktop
 - Test setup contains LIN Stress IG and LIN Disturbance blocks 

Demonstrated features:
- Transmitting header without dominant part of Break field (Header Error)
- Transmitting frame with an illegal checksum (Frame Error)
- Transmitting valid frame not defined in LDF (Frame with Id=2)

Stressing via LIN Disturbance Block
============================
Architecture:
 - Stress functionality can be controlled via GUI of LIN Disturbance Block on the 
   "Stress via LIN Disturbance Block" desktop
 - Test setup contains LIN Disturbance Block 

Demonstrated features:
- Inverting first data bit of Sync byte in the next LIN header  (Header disturbance)
- Inverting stop bit of first data byte for Frame with Id=0x33  (Response disturbance)

Stressing via CAPL
===========
Architecture:
 - Stress functionality can be controlled via panels on the "Stress via CAPL" desktop
 - Simulation Setup contains nodes of the modeled LIN network (LIN channel 1 is used).
 - Test Setup contains one CAPL program where event handlers for control panels are      implemented. 
   From the event handlers results the actual calls to LIN Stress CAPL API. The name of    called CAPL function(s) and used parameters are shown in Write window. 
 
Demonstrated features:
 - Inverting single bits in LIN header ("Invert Header Bits" panel)
 - Inverting single bits in LIN response ("Invert Response Bits" panel)
 - Transmitting LIN header with invalid parameters ("Set Header Error" panel)
 - Transmitting LIN response with an invalid number of bytes ("Set Response Length/Count" panel)
 - Transmitting LIN response a limited number of times ("Set Response Length/Count" panel)
 - Transmitting LIN response with an invalid checksum value
 - Applying dominant or recessive disturbance on the LIN bus ("Set Bus Disturbance" panel)
 - Transmitting manually composed LIN frame as a bit stream ("Send Frame as Bit Stream" panel)
 - Transmitting arbitrary byte values ("Send Bytes as Bit Stream" panel)

*********************************************************************
A detailed description can be found in the online help (see example configurations
in the table of contents).
*********************************************************************

-------------------------------------------------------------------------------------------------------
Copyright (c) by Vector Informatik GmbH Dep. Networks and Distributed Systems 2009
-------------------------------------------------------------------------------------------------------