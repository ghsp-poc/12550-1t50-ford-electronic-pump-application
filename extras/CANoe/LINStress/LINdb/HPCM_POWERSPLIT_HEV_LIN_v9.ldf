/* LIN Description file for entire LIN bus      */
/* File name: HPCM_POWERSPLIT_HEV_LIN_v8.ldf    */
/* Created by: Brian Morton, bmorto15@ford.com  */
/* Date Created: 10th Aug 2016                  */
/* Modified by: Brian Morton, bmorto16@ford.com */
/* Date Modified: 19th Aug 2019                 */

LIN_description_file;
LIN_protocol_version = "J2602_1_1.0";
LIN_language_version = "J2602_3_1.0";
LIN_speed = 10.417 kbps;

Nodes {
  Master: SOBDMC_HPCM, 8 ms, 0.1 ms, 48 bits, 40 %;
  Slaves: HV_PTC, PV, TAOP, PV2 ;
}

Signals {
  HV_PTC_heating_rq: 7, 127, SOBDMC_HPCM, HV_PTC ;
  HV_PTC_On_Rq: 2, 3, SOBDMC_HPCM, HV_PTC ;
  HV_PTC_Reset_Rq: 2, 3, SOBDMC_HPCM, HV_PTC ;
  HV_PTC_Emg_Off_Rq: 1, 0, SOBDMC_HPCM, HV_PTC ;
  HV_PTC_Discharge_Rq: 2, 3, SOBDMC_HPCM, HV_PTC ;
  HV_PTC_HV_DC_curr: 8, 255, HV_PTC, SOBDMC_HPCM ;
  HV_PTC_HV_voltage: 8, 255, HV_PTC, SOBDMC_HPCM ;
  HV_PTC_InternalWaterTemp1: 8, 255, HV_PTC, SOBDMC_HPCM ;
  HV_PTC_InternalWaterTemp2: 8, 255, HV_PTC, SOBDMC_HPCM ;
  HV_PTC_Status: 3, 7, HV_PTC, SOBDMC_HPCM ;
  HV_PTC_defectHeaterCircuits: 3, 7, HV_PTC, SOBDMC_HPCM ;
  HV_PTC_HV_Voltage_Valid: 2, 3, HV_PTC, SOBDMC_HPCM ;
  HV_PTC_TempSens_Flt: 1, 1, HV_PTC, SOBDMC_HPCM ;
  HV_PTC_TempLim: 2, 3, HV_PTC, SOBDMC_HPCM ;
  HV_PTC_LIN_Flt: 1, 1, HV_PTC, SOBDMC_HPCM ;
  HV_PTC_ExtPwrSup_Flt: 1, 1, HV_PTC, SOBDMC_HPCM ;
  HV_PTC_Off_ShrtCurr: 1, 0, HV_PTC, SOBDMC_HPCM ;
  HV_PTC_InternalPCBTemp: 8, 255, HV_PTC, SOBDMC_HPCM ;
  HV_PTC_CurrSens_Flt: 5, 7, HV_PTC, SOBDMC_HPCM ;
  RsErr_HV_PTC: 1, 1, HV_PTC, SOBDMC_HPCM ;
  ECM_NAD: 8, 1, SOBDMC_HPCM, PV ;
  ECM_PositionCommand: 8, 255, SOBDMC_HPCM, PV ;
  ECM_MoveEnable: 1, 1, SOBDMC_HPCM, PV ;
  ECM_TorqueCommand: 4, 1, SOBDMC_HPCM, PV ;
  ECM_Coils: 1, 0, SOBDMC_HPCM, PV ;
  PV_NAD: 8, 1, PV, SOBDMC_HPCM ;
  PV_ActuatorFaulted: 2, 0, PV, SOBDMC_HPCM ;
  PV_CommunicationError: 1, 0, PV, SOBDMC_HPCM ;
  PV_ReferencingStatus: 2, 0, PV, SOBDMC_HPCM ;
  PV_ARCStatus: 2, 0, PV, SOBDMC_HPCM ;
  PV_MoveState: 1, 0, PV, SOBDMC_HPCM ;
  PV_PositionReported: 8, 0, PV, SOBDMC_HPCM ;
  PV_TorqueReported: 4, 0, PV, SOBDMC_HPCM ;
  PV_ResetEvent: 3, 0, PV, SOBDMC_HPCM ;
  PV_UnderVoltage: 1, 0, PV, SOBDMC_HPCM ;
  PV_OverVoltage: 1, 0, PV, SOBDMC_HPCM ;
  PV_ThermalWarning: 1, 0, PV, SOBDMC_HPCM ;
  PV_ThermalShutdown: 1, 0, PV, SOBDMC_HPCM ;
  PV_OpenLoad: 1, 0, PV, SOBDMC_HPCM ;
  PV_OverCurrent: 1, 0, PV, SOBDMC_HPCM ;
  PV_UnexpectedBlockingPoint: 1, 0, PV, SOBDMC_HPCM ;
  PV_MechanicalBreak: 1, 0, PV, SOBDMC_HPCM ;
  CFCV_Err_Com: 1, 0, PV2, SOBDMC_HPCM ;
  CFCV_mode: 4, 15, PV2, SOBDMC_HPCM ;
  CFCV_Err_PsVolt_Out_of_range: 1, 0, PV2, SOBDMC_HPCM ;
  CFCV_Err_OverTemp_Stat: 1, 0, PV2, SOBDMC_HPCM ;
  CFCV_Err_PCTR_Time_Out: 1, 0, PV2, SOBDMC_HPCM ;
  CFCV_Err_PCTR_Blocking: 1, 0, PV2, SOBDMC_HPCM ;
  CFCV_Err_Learning: 1, 0, PV2, SOBDMC_HPCM ;
  CFCV_PCB_Temperature: 8, 255, PV2, SOBDMC_HPCM ;
  CFCV_Power_Supply_Voltage: 16, 65535, PV2, SOBDMC_HPCM ;
  CFCV_Posn_Rq: 16, 65535, SOBDMC_HPCM, PV2 ;
  CFCV_RefDrive_Enbl_Rq: 2, 3, SOBDMC_HPCM, PV2 ;
  CFCV_HighSpd_Rq: 2, 0, SOBDMC_HPCM, PV2 ;
  CFCV_ACTIVE_SAVE: 4, 15, SOBDMC_HPCM, PV2 ;
  CFCV_Position_Sensor_Out: 10, 1023, PV2, SOBDMC_HPCM ;
  CFCV_Err_MDR: 4, 15, PV2, SOBDMC_HPCM ;
  CFCV_Err_LowTemp_Stat: 1, 0, PV2, SOBDMC_HPCM ;
  CFCV_Err_Pos_Sens: 1, 0, PV2, SOBDMC_HPCM ;
  CFCV_Err_Setpoint: 1, 0, PV2, SOBDMC_HPCM ;
  CFCV_Err_position: 1, 0, PV2, SOBDMC_HPCM ;
  CFCV_Position_Perc: 10, 1023, PV2, SOBDMC_HPCM ;
  TAOPCtlFrm_Status: 8, 0, TAOP, SOBDMC_HPCM ;
  TrnOilPump_I_Meas: 8, 0, TAOP, SOBDMC_HPCM ;
  TrnOilPump_U_Meas: 8, 0, TAOP, SOBDMC_HPCM ;
  TrnOilPumpBlk_B_Falt: 1, 0, TAOP, SOBDMC_HPCM ;
  TrnOilPumpI_D_Falt: 2, 0, TAOP, SOBDMC_HPCM ;
  TrnOilPumpMde_D_Rq: 2, 0, SOBDMC_HPCM, TAOP ;
  TrnOilPumpMde_D_Stat: 2, 0, TAOP, SOBDMC_HPCM ;
  TrnOilPumpPrtct_B_Falt: 1, 0, TAOP, SOBDMC_HPCM ;
  TrnOilPumpTe_D_Falt: 2, 0, TAOP, SOBDMC_HPCM ;
  TrnOilPumpU_D_Falt: 2, 0, TAOP, SOBDMC_HPCM ;
  TrnOilPumpV_D_Falt: 2, 0, TAOP, SOBDMC_HPCM ;
  TrnOilPumpV_D_Stat: 2, 0, TAOP, SOBDMC_HPCM ;
  TrnOilPumpV_Pc_Actl: 8, 0, TAOP, SOBDMC_HPCM ;
  TrnOilPumpV_Pc_Rq: 8, 0, SOBDMC_HPCM, TAOP ;
  TrnSumpOil_Te_Actl: 8, 0, SOBDMC_HPCM, TAOP ;
}

Diagnostic_signals {
  MasterReqB0: 8, 0 ;
  MasterReqB1: 8, 0 ;
  MasterReqB2: 8, 0 ;
  MasterReqB3: 8, 0 ;
  MasterReqB4: 8, 0 ;
  MasterReqB5: 8, 0 ;
  MasterReqB6: 8, 0 ;
  MasterReqB7: 8, 0 ;
  SlaveRespB0: 8, 0 ;
  SlaveRespB1: 8, 0 ;
  SlaveRespB2: 8, 0 ;
  SlaveRespB3: 8, 0 ;
  SlaveRespB4: 8, 0 ;
  SlaveRespB5: 8, 0 ;
  SlaveRespB6: 8, 0 ;
  SlaveRespB7: 8, 0 ;
}



Frames {
  ECM_TAOP_DEMAND: 8, SOBDMC_HPCM, 3 {
    TrnOilPumpV_Pc_Rq, 0 ;
    TrnSumpOil_Te_Actl, 8 ;
    TrnOilPumpMde_D_Rq, 16 ;
  }
  TAOP_ECM_STATUS: 9, TAOP, 6 {
    TAOPCtlFrm_Status, 0 ;
    TrnOilPumpV_Pc_Actl, 8 ;
    TrnOilPump_U_Meas, 16 ;
    TrnOilPump_I_Meas, 24 ;
    TrnOilPumpV_D_Stat, 32 ;
    TrnOilPumpMde_D_Stat, 34 ;
    TrnOilPumpTe_D_Falt, 36 ;
    TrnOilPumpI_D_Falt, 38 ;
    TrnOilPumpU_D_Falt, 40 ;
    TrnOilPumpV_D_Falt, 42 ;
    TrnOilPumpBlk_B_Falt, 44 ;
    TrnOilPumpPrtct_B_Falt, 45 ;
  }
}



Diagnostic_frames {
  MasterReq: 0x3c {
    MasterReqB0, 0 ;
    MasterReqB1, 8 ;
    MasterReqB2, 16 ;
    MasterReqB3, 24 ;
    MasterReqB4, 32 ;
    MasterReqB5, 40 ;
    MasterReqB6, 48 ;
    MasterReqB7, 56 ;
  }
  SlaveResp: 0x3d {
    SlaveRespB0, 0 ;
    SlaveRespB1, 8 ;
    SlaveRespB2, 16 ;
    SlaveRespB3, 24 ;
    SlaveRespB4, 32 ;
    SlaveRespB5, 40 ;
    SlaveRespB6, 48 ;
    SlaveRespB7, 56 ;
  }
}

Node_attributes {
  HV_PTC{
    LIN_protocol = "2.1" ;
    configured_NAD = 0x4D ;
    initial_NAD = 0x4D ;
    product_id = 0x74, 0x3, 0 ;
    response_error = RsErr_HV_PTC ;
    P2_min = 50 ms ;
    ST_min = 0 ms ;
    N_As_timeout = 1000 ms ;
    N_Cr_timeout = 1000 ms ;
  }
  PV{
    LIN_protocol = "2.1" ;
    configured_NAD = 0x1 ;
    initial_NAD = 0x1 ;
    product_id = 0x76, 0x0, 0 ;
    response_error = PV_CommunicationError ;
    P2_min = 50 ms ;
    ST_min = 0 ms ;
    N_As_timeout = 1000 ms ;
    N_Cr_timeout = 1000 ms ;
  }
  TAOP{
    LIN_protocol = "J2602_1_1.0" ;
    configured_NAD = 0x62 ;
    product_id = 0x0, 0x0, 0 ;
    response_error = TAOPCtlFrm_Status ;
    P2_min = 0 ms ;
    ST_min = 0 ms ;
    configurable_frames {
      ECM_TAOP_DEMAND = 0x8 ;
      TAOP_ECM_STATUS = 0x9 ;
    }
    response_tolerance = 40 % ;
    wakeup_time = 100 ms;
    poweron_time = 100 ms;
  }
  PV2{
    LIN_protocol = "2.2" ;
    configured_NAD = 0xD ;
    initial_NAD = 0xD ;
    product_id = 0x37, 0x1, 0 ;
    response_error = CFCV_Err_Com ;
    P2_min = 50 ms ;
    ST_min = 0 ms ;
    N_As_timeout = 1000 ms ;
    N_Cr_timeout = 1000 ms ;
  }
}

Schedule_tables {
 Mst_Schedule {
    ECM_TAOP_DEMAND delay 64 ms ;
    TAOP_ECM_STATUS delay 64 ms ;
  }
 Lin_Diag_Schedule {
    MasterReq delay 56 ms ;
    SlaveResp delay 56 ms ;
  }
}


Signal_encoding_types {
  ARC {
    physical_value, 0, 3, 1, 0, "Counts" ;
    logical_value, 0, "ARC value = 0" ;
    logical_value, 1, "ARC value = 1" ;
    logical_value, 2, "ARC value = 2" ;
    logical_value, 3, "ARC value = 3" ;
  }
  Bit_01_Encoding {
    logical_value, 0, "False" ;
    logical_value, 1, "True" ;
  }
  BooleanCoding {
    logical_value, 0, "FALSE" ;
    logical_value, 1, "TRUE" ;
  }
  c02_HV_PTC_Discharge_Rq {
    logical_value, 0, "NO_DISCHRG" ;
    logical_value, 1, "DISCHRG_RQ" ;
    logical_value, 2, "NDEF_2" ;
    logical_value, 3, "SNA" ;
  }
  c02_HV_PTC_On_Rq {
    logical_value, 0, "PTC_OFF" ;
    logical_value, 1, "PTC_ON" ;
    logical_value, 2, "RES" ;
    logical_value, 3, "SNA" ;
  }
  c02_Reset_Rq {
    logical_value, 0, "NO_RESET" ;
    logical_value, 1, "RESET" ;
    logical_value, 2, "NDEF2" ;
    logical_value, 3, "SNA" ;
  }
  c03_Counter {
    physical_value, 0, 6, 1, 0 ;
    logical_value, 7, "SNA" ;
  }
  c03_HV_PTC_CurrSens_Flt {
    logical_value, 0, "DIAG_PENDING" ;
    logical_value, 1, "DIAG_PASS" ;
    logical_value, 2, "SHRT_TO_GND" ;
    logical_value, 3, "SHRT_TO_VCC" ;
    logical_value, 4, "OFFSET" ;
    logical_value, 5, "OUT_OF_RNG" ;
    logical_value, 6, "NDEF_6" ;
    logical_value, 7, "SNA" ;
  }
  c03_HV_PTC_Stat {
    logical_value, 0, "HV_PTC_OFF" ;
    logical_value, 1, "HV_PTC_ON" ;
    logical_value, 2, "HV_PTC_DEG" ;
    logical_value, 3, "HV_PTC_STOP" ;
    logical_value, 4, "HV_PTC_SHUTDOWN" ;
    logical_value, 5, "HV_PTC_SHORTED" ;
    logical_value, 6, "HV_PTC_NO_RESET" ;
    logical_value, 7, "SNA" ;
  }
  c07_perc_0_100_1 {
    physical_value, 0, 100, 1, 0, "%" ;
    logical_value, 127, "SNA" ;
  }
  c08_A_0_63k5_0k25 {
    physical_value, 0, 254, 0.25, 0, "A" ;
    logical_value, 255, "SNA" ;
  }
  c08_degC_m50_204_1 {
    physical_value, 0, 254, 1, -50, "C" ;
    logical_value, 255, "SNA" ;
  }
  c08_V_0_508_2 {
    physical_value, 0, 254, 2.5, 0, "V" ;
    logical_value, 255, "SNA" ;
  }
  Coils {
    logical_value, 0, "No Holding Current in Coils (Default)" ;
    logical_value, 1, "Holding Current in Coils" ;
  }
  Comm_Error {
    logical_value, 0, "No Communication Error" ;
    logical_value, 1, "Communication Error Detected" ;
  }
  Fault {
    logical_value, 0, "Actuator Not Faulted" ;
    logical_value, 1, "Actuator Faulted" ;
    logical_value, 2, "Actuator Fault Status Indeterminate" ;
    logical_value, 3, "Unused and Reserved" ;
  }
  HV_PTC_HV_Voltage_Valid_Encoding {
    logical_value, 0, "SNA" ;
    logical_value, 1, "In Range" ;
    logical_value, 2, "Undervoltage" ;
    logical_value, 3, "Overvoltage" ;
  }
  HV_PTC_TempLim_Encoding {
    logical_value, 0, "SNA" ;
    logical_value, 1, "Normal operation" ;
    logical_value, 2, "Temperature warning" ;
    logical_value, 3, "Over temperature shutdown" ;
  }
  LINStatus {
    physical_value, 0, 7, 1, 0, "LIN Status" ;
  }
  Move_Enable {
    logical_value, 0, "Ignore Commanded Position" ;
    logical_value, 1, "Move to Commanded Position" ;
  }
  Move_State {
    logical_value, 0, "Motor is Stopped" ;
    logical_value, 1, "Motor is Moving" ;
  }
  NAD {
    logical_value, 1, "NAD_Cmd" ;
  }
  NAD_Rsp {
    logical_value, 1, "NAD_Rsp" ;
  }
  Position_Cmd {
    physical_value, 0, 255, 0.3921569, 0, "Percent of Range" ;
    logical_value, 0, "Port B" ;
    logical_value, 255, "Port C" ;
  }
  Position_Rsp {
    physical_value, 0, 255, 0.3921569, 0, "Percent of Range" ;
    logical_value, 0, "Port B" ;
    logical_value, 255, "Port C" ;
  }
  Ref_Status {
    logical_value, 0, "Not Referenced" ;
    logical_value, 1, "Referencing in Process" ;
    logical_value, 2, "Referenced" ;
    logical_value, 3, "Unused and Reserved" ;
  }
  RestEvent_Encoding {
    logical_value, 0, "Indeterminate / UV at VDDx" ;
    logical_value, 1, "POR" ;
    logical_value, 2, "Wake Up after Sleep Mode" ;
    logical_value, 3, "SW Reset Command" ;
    logical_value, 4, "Watchdog / TXD Timeout" ;
    logical_value, 5, "Unused and Reserved" ;
    logical_value, 6, "Unused and Reserved" ;
    logical_value, 7, "Flag Successuly Reported" ;
  }
  StatusTAOPModeCoding {
    logical_value, 0, "No request" ;
    logical_value, 1, "Request honored" ;
    logical_value, 2, "Request denied" ;
    logical_value, 3, "Request out of range" ;
  }
  TAOPCurrentCoding {
    physical_value, 0, 255, 0.2, 0, "Amp" ;
  }
  TAOPCurrFltCoding {
    logical_value, 0, "No fault" ;
    logical_value, 1, "Over current fault" ;
    logical_value, 2, "Under current fault" ;
    logical_value, 3, "Signal not available" ;
  }
  TAOPModeCoding {
    logical_value, 0, "Idle Mode" ;
    logical_value, 1, "Run Mode" ;
    logical_value, 2, "Undefined state" ;
    logical_value, 3, "Undefined state" ;
  }
  TAOPSpeedFltCoding {
    logical_value, 0, "No fault" ;
    logical_value, 1, "Over speed fault" ;
    logical_value, 2, "Under speed fault" ;
    logical_value, 3, "Signal not available" ;
  }
  TAOPTempFltCoding {
    logical_value, 0, "No fault" ;
    logical_value, 1, "Over temperature fault" ;
    logical_value, 2, "Under temperature fault" ;
    logical_value, 3, "Signal not available" ;
  }
  TAOPVoltageCoding {
    physical_value, 0, 255, 0.1, 0, "Volt" ;
  }
  TAOPVoltFltCoding {
    logical_value, 0, "No fault" ;
    logical_value, 1, "Over voltage fault" ;
    logical_value, 2, "Under voltage fault" ;
    logical_value, 3, "Signal not available" ;
  }
  Torque_Cmd {
    logical_value, 0, "No Torque Value Requested" ;
    logical_value, 1, "Nominal Torque" ;
    logical_value, 2, "10% Boost" ;
    logical_value, 3, "20% Boost" ;
    logical_value, 4, "30% Boost" ;
    logical_value, 5, "40% Boost" ;
    logical_value, 6, "50% Boost" ;
    logical_value, 7, "60% Boost" ;
    logical_value, 8, "70% Boost" ;
    logical_value, 9, "80% Boost" ;
    logical_value, 10, "90% Boost" ;
    logical_value, 11, "100% Boost" ;
    logical_value, 12, "Unused and Reserved" ;
    logical_value, 13, "Unused and Reserved" ;
    logical_value, 14, "Unused and Reserved" ;
    logical_value, 15, "Unused and Reserved" ;
  }
  Torque_Rsp {
    logical_value, 0, "Torque Setting Indeterminate" ;
    logical_value, 1, "Nominal Torque Set" ;
    logical_value, 2, "10% Boost Set" ;
    logical_value, 3, "20% Boost Set" ;
    logical_value, 4, "30% Boost Set" ;
    logical_value, 5, "40% Boost Set" ;
    logical_value, 6, "50% Boost Set" ;
    logical_value, 7, "60% Boost Set" ;
    logical_value, 8, "70% Boost Set" ;
    logical_value, 9, "80% Boost Set" ;
    logical_value, 10, "90% Boost Set" ;
    logical_value, 11, "100% Boost Set" ;
    logical_value, 12, "Unused and Reserved" ;
    logical_value, 13, "Unused and Reserved" ;
    logical_value, 14, "Unused and Reserved" ;
    logical_value, 15, "Unused and Reserved" ;
  }
  TranSumpOilTempCoding {
    physical_value, 0, 254, 1, -50, "C" ;
    logical_value, 255, "Fault" ;
  }
  VaneCtrlReqCoding {
    physical_value, 0, 255, 0.392157, 0, "%" ;
  }
  CFCV_HighSpd_Rq_Encoding {
    logical_value, 0, "slow" ;
    logical_value, 1, "normal" ;
    logical_value, 2, "fast" ;
    logical_value, 3, "SNA" ;
  }
  CFCV_MDR_Err_Encoding {
    logical_value, 0, "NOERROR" ;
    logical_value, 1, "MDR_Short_Circuit_Err" ;
    logical_value, 2, "MDR_Open_Load_Err" ;
    logical_value, 15, "SNA" ;
  }
  CFCV_Position_Sensor_Out_Encoding {
    physical_value, 0, 1023, 0.1, 0, "%" ;
    logical_value, 65535, "SNA" ;
  }
  CFCV_Position_Sensor_Requ_Encoding {
    physical_value, 0, 1023, 0.1, 0, "%" ;
    logical_value, 65535, "SNA" ;
  }
  CFCV_Power_Supply_Voltage_Encoding {
    physical_value, 0, 32000, 0.01, 0, "V" ;
    logical_value, 65534, "ERROR" ;
    logical_value, 65535, "SNA" ;
  }
  CFCV_mode_Encoding {
    logical_value, 0, "Normal Operation" ;
    logical_value, 1, "Motor_Off" ;
    logical_value, 2, "System_Blind_Fail_Safe" ;
    logical_value, 3, "Learning" ;
    logical_value, 4, "Break_Free" ;
    logical_value, 5, "Cleaning" ;
    logical_value, 6, "active_Sleep" ;
    logical_value, 7, "Reset" ;
    logical_value, 8, "SYST_FAIL_SAFE" ;
    logical_value, 9, "STATIC" ;
    logical_value, 15, "SNA" ;
  }
  c01_NoError_Error {
    logical_value, 0, "NOERROR" ;
    logical_value, 1, "ERROR" ;
  }
  c10_step_0_1023 {
    physical_value, 0, 100, 0.1, 0, "%" ;
    logical_value, 1023, "SNA" ;
  }
  c_01_temperature_0_150 {
    physical_value, 0, 254, 1, -40, "�C" ;
    logical_value, 255, "SNA" ;
  }
  c_02_TJA_RefDriveAllow_Stat {
    logical_value, 0, "no_Learning_Requ" ;
    logical_value, 1, "Learning_Requ" ;
    logical_value, 2, "ERROR" ;
    logical_value, 3, "SNA" ;
  }
  CFCV_Pos_Sens_Err_Encoding {
    logical_value, 0, "NOERROR" ;
    logical_value, 1, "ERROR" ;
    logical_value, 2, "AGC error" ;
    logical_value, 3, "Magnitude error" ;
    logical_value, 4, "Calib error" ;
    logical_value, 7, "SNA" ;
  }
  CFCV_Warn_OverTemp_Stat_Encoding {
    logical_value, 0, "No_OverTemp_Warning" ;
    logical_value, 1, "OverTemp_Warning" ;
  }
  c01_Inactive_Active {
    logical_value, 0, "INACTIVE" ;
    logical_value, 1, "ACTIVE" ;
  }
  c02_WakeUp_Stat {
    logical_value, 0, "NO_WUP" ;
    logical_value, 1, "ACTIVE_WUP" ;
    logical_value, 2, "NDEF2" ;
    logical_value, 3, "SNA" ;
  }
  c04_RSV_Move_Stat {
    logical_value, 0, "IDLE" ;
    logical_value, 1, "CW" ;
    logical_value, 2, "CCW" ;
    logical_value, 3, "REF" ;
    logical_value, 4, "RES" ;
    logical_value, 5, "RES2" ;
    logical_value, 6, "RES3" ;
    logical_value, 7, "RES4" ;
    logical_value, 8, "RES5" ;
    logical_value, 9, "RES6" ;
    logical_value, 10, "RES7" ;
    logical_value, 11, "RES8" ;
    logical_value, 12, "RES9" ;
    logical_value, 13, "RES10" ;
    logical_value, 14, "ERROR" ;
    logical_value, 15, "SNA" ;
  }
  c12_step_0_4095 {
    physical_value, 0, 4094, 1, 0, "incr" ;
    logical_value, 4095, "SNA" ;
  }
  c16_Steps_0_65534 {
    physical_value, 0, 65534, 1, 0, "Steps" ;
    logical_value, 65535, "SNA" ;
  }
  c_RB_Err_Stat_TJA {
    logical_value, 0, "NO_ERROR" ;
    logical_value, 1, "position_Err" ;
    logical_value, 2, "PS_Voltg_Err" ;
    logical_value, 3, "LowTemp_Err" ;
    logical_value, 4, "OverTemp_Err" ;
    logical_value, 5, "PCTR_TimeOutErr" ;
    logical_value, 6, "PCTR_Blocking_Err" ;
    logical_value, 7, "Pos_Sens_Err" ;
    logical_value, 8, "Setpoint_Err" ;
    logical_value, 9, "Learning_Err" ;
    logical_value, 10, "Comm_Err" ;
    logical_value, 11, "RES1" ;
    logical_value, 12, "RES2" ;
    logical_value, 13, "RES3" ;
    logical_value, 14, "ERROR" ;
    logical_value, 15, "SNA" ;
  }
}

Signal_representation {
  ARC: PV_ARCStatus ;
  Bit_01_Encoding: PV_UnderVoltage, PV_OverVoltage, PV_ThermalWarning, PV_ThermalShutdown, PV_OpenLoad, PV_OverCurrent, PV_UnexpectedBlockingPoint, PV_MechanicalBreak ;
  BooleanCoding: TrnOilPumpBlk_B_Falt, TrnOilPumpPrtct_B_Falt ;
  CFCV_HighSpd_Rq_Encoding: CFCV_HighSpd_Rq ;
  CFCV_MDR_Err_Encoding: CFCV_Err_MDR ;
  CFCV_Position_Sensor_Out_Encoding: CFCV_Position_Sensor_Out ;
  CFCV_Position_Sensor_Requ_Encoding: CFCV_Posn_Rq ;
  CFCV_Power_Supply_Voltage_Encoding: CFCV_Power_Supply_Voltage ;
  CFCV_mode_Encoding: CFCV_mode ;
  Coils: ECM_Coils ;
  Comm_Error: PV_CommunicationError ;
  Fault: PV_ActuatorFaulted ;
  HV_PTC_HV_Voltage_Valid_Encoding: HV_PTC_HV_Voltage_Valid ;
  HV_PTC_TempLim_Encoding: HV_PTC_TempLim ;
  LINStatus: TAOPCtlFrm_Status ;
  Move_Enable: ECM_MoveEnable ;
  Move_State: PV_MoveState ;
  NAD: ECM_NAD ;
  NAD_Rsp: PV_NAD ;
  Position_Cmd: ECM_PositionCommand ;
  Position_Rsp: PV_PositionReported ;
  Ref_Status: PV_ReferencingStatus ;
  RestEvent_Encoding: PV_ResetEvent ;
  StatusTAOPModeCoding: TrnOilPumpV_D_Stat ;
  TAOPCurrFltCoding: TrnOilPumpI_D_Falt ;
  TAOPCurrentCoding: TrnOilPump_I_Meas ;
  TAOPModeCoding: TrnOilPumpMde_D_Rq, TrnOilPumpMde_D_Stat ;
  TAOPSpeedFltCoding: TrnOilPumpV_D_Falt ;
  TAOPTempFltCoding: TrnOilPumpTe_D_Falt ;
  TAOPVoltFltCoding: TrnOilPumpU_D_Falt ;
  TAOPVoltageCoding: TrnOilPump_U_Meas ;
  Torque_Cmd: ECM_TorqueCommand ;
  Torque_Rsp: PV_TorqueReported ;
  TranSumpOilTempCoding: TrnSumpOil_Te_Actl ;
  VaneCtrlReqCoding: TrnOilPumpV_Pc_Actl, TrnOilPumpV_Pc_Rq ;
  c01_NoError_Error: CFCV_Err_Com, CFCV_Err_PsVolt_Out_of_range, CFCV_Err_OverTemp_Stat, CFCV_Err_PCTR_Time_Out, CFCV_Err_PCTR_Blocking, CFCV_Err_Learning, CFCV_Err_LowTemp_Stat, CFCV_Err_Pos_Sens, CFCV_Err_Setpoint, CFCV_Err_position ;
  c02_HV_PTC_Discharge_Rq: HV_PTC_Discharge_Rq ;
  c02_HV_PTC_On_Rq: HV_PTC_On_Rq ;
  c02_Reset_Rq: HV_PTC_Reset_Rq ;
  c03_Counter: HV_PTC_defectHeaterCircuits ;
  c03_HV_PTC_CurrSens_Flt: HV_PTC_CurrSens_Flt ;
  c03_HV_PTC_Stat: HV_PTC_Status ;
  c07_perc_0_100_1: HV_PTC_heating_rq ;
  c08_A_0_63k5_0k25: HV_PTC_HV_DC_curr ;
  c08_V_0_508_2: HV_PTC_HV_voltage ;
  c08_degC_m50_204_1: HV_PTC_InternalWaterTemp1, HV_PTC_InternalWaterTemp2, HV_PTC_InternalPCBTemp ;
  c10_step_0_1023: CFCV_Position_Perc ;
  c_01_temperature_0_150: CFCV_PCB_Temperature ;
  c_02_TJA_RefDriveAllow_Stat: CFCV_RefDrive_Enbl_Rq ;
}
