#ifndef NVM_MGR_CFG_H
#   define NVM_MGR_CFG_H
/*===========================================================================*/
/**
 * @file nvm_mgr_cfg.h
 *
 *   Template file for configuring the NVM manager and submodules.
 *
 *------------------------------------------------------------------------------
 *
 * Copyright 2014 GHSP, Inc., All Rights Reserved.
 * GHSP Confidential
 * 
 * NOTICE:  
 * All information contained herein is, and remains the property of GHSP, Inc. 
 * The intellectual and technical concepts contained herein are proprietary to 
 * GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in 
 * process, and are protected by trade secret or copyright law. Dissemination 
 * of this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from GHSP, Inc.
 *
 *------------------------------------------------------------------------------
 *
 * @section DESC DESCRIPTION:
 *
 * Template file for configuring the NVM manager and submodules.
 *
 * @section ABBR ABBREVIATIONS:
 *   - None
 *
 * @section TRACE TRACEABILITY INFO:
 *   - Design Document(s):
 *     - None
 *
 *   - Requirements Document(s):
 *     - None
 *
 *   - Applicable Standards (in order of precedence: highest first):
 *     - <a href="http://sharepoint/sandbox/ghspdev/LaunchPad/EngineeringDepartment/Elec_proj_mgmt/Revision%20Controlled%20Library/Software%20Process/Work%20Instructions/Coding%20Standard.docx">
 *       "GHSP Coding Standard" [12-Mar-2006]</a>
 *
 * @section DFS DEVIATIONS FROM STANDARDS:
 *   - None
 *
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "global.h"
#include "fdl_cfg.h"

/*===========================================================================*
 * #define Constants
 *===========================================================================*/
/*
 * Storage is redundant. We only need half as many shadows as the number of
 * blocks that we actually have.
 */
#define NVMCFG_MAX_NUM_SHADOWS          (FDL_CFG_EEPROM_NUM_BLOCKS / 2u)

/* the max number of queued operations allowed in the nvm manager. */
#define NVMCFG_OPERATION_QUEUE_SIZE     (5u)

/*===========================================================================*\
 * #define MACROS
\*===========================================================================*/

/*===========================================================================*\
 * Custom Type Declarations
\*===========================================================================*/


#endif                          /* NVM_MGR_CFG_H */
