/**
 * @ingroup CFG
 *
 * @{
 */

/**********************************************************************/

#ifndef USR_CONFIGURATION_H
#define USR_CONFIGURATION_H

/**********************************************************************/

/* ----------------------------------------------------------------------------
   ---------------------------------------------------------------------------- */

#define  IC_MAJOR_ID      523
#define  IC_MINOR_ID      6
#define  IC_MAJOR_VERSION 1
#define  IC_MINOR_VERSION 0

#define  PCB_MAJOR_ID      0
#define  PCB_MINOR_ID      0
#define  PCB_MAJOR_VERSION 1
#define  PCB_MINOR_VERSION 0

#define SCI_USAGE_TYPE SCI_ELMOS_LIN                          /**< functionality of the MCU's SCI: SCI_UNUSED, SCI_DATALOGGER or SCI_ELMOS_LIN */

#define USR_SHUNT_MEASUREMENT_TYPE USR_SHUNT_MEAS_TYPE_SINGLE /**< configure the measurement of the phase currents */

/* ----------------------------------------------------------------------------
   FOC angle and speed measurement
   ---------------------------------------------------------------------------- */
#define FOC_EN_HALL_SENS 0                                    /**< if set to 1, the hall module is compiled into the firmware */
#define FOC_EN_OBSERVER  1                                    /**< if set to 1, the luenberger observer is compiled into the firmware (required for sensorless operation) */

#define FOC_EN_VBAT_COMPENSATION 0                            /**< if set to 1, the output of the i_d and i_q controller is scaled to the momentary supply voltage, so that changes in VBAT do not affect the controller's dynamic behaviour */
#if ( FOC_EN_VBAT_COMPENSATION == 1 )
  #define FOC_VBAT_NOMINAL_MV 13500u
#endif

#if ( FOC_EN_HALL_SENS == 1 )
  #define USR_USE_HALL_SPEED_CALC 0                           /**< if set to 1, module motor control (more precisely: the speed controller) uses the speed calculated by the hall module for lower speeds (see USR_HALL_SPEED_CALC_THRSHLD_HYS_LOWER and USR_HALL_SPEED_CALC_THRSHLD_HYS_UPPER) */

  #define USR_HALL_SPEED_CALC_THRSHLD_HYS_LOWER 12000u        /**< only regarded when USR_USE_HALL_SPEED_CALC == 1; lower hysteresis value for switching between hall speed calculation and SPDMEAS calculation [eRPM] */
  #define USR_HALL_SPEED_CALC_THRSHLD_HYS_UPPER 15000u        /**< only regarded when USR_USE_HALL_SPEED_CALC == 1; upper hysteresis value for switching between hall speed calculation and SPDMEAS calculation [eRPM] */
#endif

/* ----------------------------------------------------------------------------
   motor control state machine behaviour
   ---------------------------------------------------------------------------- */
#define USR_OVERRIDE_VOLTAGE_OVERFLOW_HANDLER             1   /**< if set to 1, the user overrides the output overflow handler mc_output_voltage_overflow_handler() - strongly recommended! */
#define USR_OVERRIDE_SYNC_TO_TURNING_ROTOR_FAILED_HANDLER 0   /**< if set to 1, the user overrides the sync failed handler mc_sync_to_turning_rotor_failed_handler() for deciding how to proceed in case of a failed sync attempt */

#define USR_EN_INITIAL_POSITION_DETECTION 0                   /**< if set to 1, the rotor position detection module is compiled into the firmware (required for rotor position detection at standstill) */

#define USR_EN_SINGLE_SHUNT_SILENT_MODE 0                     /**< if set to 1, the single shunt operation is implemented in a way that is absolutely noiseless (strongly recommended) */

/* ----------------------------------------------------------------------------
   PWM diagnostic interface
   ---------------------------------------------------------------------------- */
#define USE_PWM_DIAG_INPUT 0                                  /* if set to 1, the PWM diagnostic input module is copmiled into the firmware (for measuring an arbitrary input PWM at a GPIO pin) */

#if ( USE_PWM_DIAG_INPUT == 1 )
  #define PWM_DIAG_INPUT_PORT GPIO_C
  #define PWM_DIAG_INPUT_PIN  GPIO_IO_4

  #define PWM_DIAG_IN_PRESCALER ((u16) 100 )
#endif

#define USE_PWM_DIAG_OUTPUT 0                                 /* if set to 1, the PWM diagnostic output module is compiled into the firmware (for generating an arbitrary PWM signal at a GPIO pin) */

#if ( USE_PWM_DIAG_OUTPUT == 1 )
  #define PWM_DIAG_OUTPUT_PORT GPIO_C
  #define PWM_DIAG_OUTPUT_PIN  GPIO_IO_6

  #define PWM_DIAG_OUT_PRESCALER ((u16) 100 )
#endif

/* ----------------------------------------------------------------------------
   EEPROM emulation and real eeprom on SPI_1
   ---------------------------------------------------------------------------- */
#define ENABLE_EEPROM_EMULATION_DEMO 0                        /**< if set to 1, an example of how to use the program memory for an EEPROM emulation is compiled into the firmware */
#define ENABLE_SPI_1_EEPROM 1                                 /**< if set to 1, a Real SPI eeprom is attached to SPI_1 */
/* ----------------------------------------------------------------------------
   ADC measurement configuration
   ---------------------------------------------------------------------------- */
#if ( USR_SHUNT_MEASUREMENT_TYPE == USR_SHUNT_MEAS_TYPE_SINGLE )
  #define ADC_SHUNT_SUM ( ADC_CHNO_B5 )                       /** channel of the sum shunt */
#elif ( USR_SHUNT_MEASUREMENT_TYPE == USR_SHUNT_MEAS_TYPE_DUAL_U_V )
  #define ADC_SHUNT_1 ( ADC_CHNO_C2 )                         /** channel of the phase U shunt */
  #define ADC_SHUNT_2 ( ADC_CHNO_C6 )                         /** channel of the phase V shunt */
#elif ( USR_SHUNT_MEASUREMENT_TYPE == USR_SHUNT_MEAS_TYPE_DUAL_V_W )
  #define ADC_SHUNT_1 ( ADC_CHNO_C4 )                         /** channel of the phase V shunt */
  #define ADC_SHUNT_2 ( ADC_CHNO_C2 )                         /** channel of the phase W shunt */
#else
 #error Unknown USR_SHUNT_MEASUREMENT_TYPE!
#endif


#if( ENABLE_SPI_1_EEPROM == 1 )
  #define ADC_PHASE_U ( ADC_CHNO_B7 )                           /** channel of phase voltage divider U */
  #define ADC_PHASE_V ( ADC_CHNO_C6 )                           /** channel of phase voltage divider V */
  #define ADC_PHASE_W ( ADC_CHNO_B6 )                           /** channel of phase voltage divider W */
#else
  #define ADC_PHASE_U ( ADC_CHNO_C3 )                           /** channel of phase voltage divider U */
  #define ADC_PHASE_V ( ADC_CHNO_C4 )                           /** channel of phase voltage divider V */
  #define ADC_PHASE_W ( ADC_CHNO_C5 )                           /** channel of phase voltage divider W */
#endif 

#define ADC_VBAT_MEASURE_CH ( ADC_CHNO_C7 )                   /** channel for measuring VBAT */

#define ADC_POTI ( ADC_CHNO_C2 )                              /** channel of potentiometer voltage */

/* ----------------------------------------------------------------------------
   HALL sensor interface
   ---------------------------------------------------------------------------- */
#if ( FOC_EN_HALL_SENS == 1 )
  #define HALL_PORT            GPIO_C
  #define HALL_PORT_IE_REG     GPIO_C_DATA_IE
  #define HALL_PORT_OE_REG     GPIO_C_DATA_OE
  #define HALL_PORT_INPUT_REG  GPIO_C_DATA_IN
  #define HALL_PORT_OUTPUT_REG GPIO_C_DATA_OUT

  #define HALL_U_PIN   GPIO_IO_3
  #define HALL_U_IOMUX IOMUX_CTRL_PC0123_IO_SEL_bit.sel_3
  #define HALL_U_STATE ( HALL_PORT_INPUT_REG & HALL_U_PIN )

  #define HALL_V_PIN   GPIO_IO_4
  #define HALL_V_IOMUX IOMUX_CTRL_PC4567_IO_SEL_bit.sel_4
  #define HALL_V_STATE ( HALL_PORT_INPUT_REG & HALL_V_PIN )

  #define HALL_W_PIN   GPIO_IO_5
  #define HALL_W_IOMUX IOMUX_CTRL_PC4567_IO_SEL_bit.sel_5
  #define HALL_W_STATE ( HALL_PORT_INPUT_REG & HALL_W_PIN )
#endif

/* ---------------------------------------
 * CPU Chronometrics baseline measurement
 -----------------------------------------*/
/**
 * Define the following config to enable the baseline measurements of the CPU performance.
 * Enabling this will disable all functionality but the minimum in order to determine the maximum
 * idle time of the CPU.
 */
/*#define ENABLE_UNLOADED_CPU_CHRONOMETRICS*/

#endif /* "#ifndef USR_CONFIGURATION_H" */

/* }@
 */
