#ifndef UDS_CFG_H
#define UDS_CFG_H
/**
 *  @file uds_cfg.h
 *
 *  @copyright 2019 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *
 *  @addtogroup uds_imp_cfg uds Implementation Configuration
 *  @{
 *      Configuration of uds_did Implementation
 *      The UDS module is capable of supported messages of any size. Various physical layers
 *      and logical or transport layers can support messages of various sizes. Since this
 *      module is implemented using no dynamic memory allocation, the maximum size of a
 *      message that can be either sent or received on the underlying communication layer
 *      must be specified.
 *
 *      This configuration is supplied by setting @ref UDS_MAX_RESPONSE_SIZE to the value
 *      that the underlying communication layer supports.
 *
 *      @subsection uds_did_cfg UDS DID Configuration
 *
 *      This is the configuration file for the Customer and Supplier Data Identifiers supported
 *      by this system. Each DID must have a unique name, a unique 16 bit id, the size in bytes of the data
 *      associated with that did and a function that is used to read the value of the DID.
 *      Each DID may optionally have a function that can be used to write the value of that DID.
 *
 *      There are limited ranges for valid DIDs. These ranges are labeled as VehicleManufacturerSpecific
 *      in Table C.1 of the ISO 14229-1 specification, however, this implementation restricts the
 *      valid values of DIDs to the range 0x0100-0xA5FF.
 *
 *      To create a DID, a DID record must be added to the table UDS_DID_TABLE using the following format:
 *
 *      @code
 *         UDS_DID(name, id, size, Read Function, Write Function)\
 *      @endcode
 *
 *      Each of the fields above is described in the following section.
 *
 *      <b> name </b> - The logical name of the DID. This should be something meaningful for the
 *                      developers and maintainers of the project. This will become an element in
 *                      an enumerated type so the choice of this name should follow the naming
 *                      convention for an enumerated type.
 *
 *      <b> id </b> - The numerical value of the DID. This must be a value in the range 0x0100-0xA5FF
 *
 *      <b> size </b> - the actual data size of the underlying data associated with the DID. For example
 *                           if this DID contains an unsigned 16 bit integer, the size would be 2.
 *
 *      <b> Read Function </b> - Name of the function used to read the DID value. This value must not be NULL.
 *                               @see @ref did_read_func
 *
 *      <b> Write function </b> - Name of the function used to write the DID value. This value may be NULL.
 *                                Setting this value to NULL results in a read-only DID.
 *
 *      @b Example:
 *
 *      @code
 *        UDS_DID(UDS_SYSTEM_VOLTAGE,       0x0100, sizeof(uint16_t),    Test_Read_System_Voltage,       NULL)\
 *        UDS_DID(UDS_SYSTEM_CURRENT,       0x0101, sizeof(uint16_t),    Test_Read_System_Current,       NULL)\
 *        UDS_DID(UDS_INTERNAL_TEMPERATURE, 0x0102, sizeof(uint16_t),    Test_Read_Internal_Temperature, NULL)\
 *        UDS_DID(UDS_SERIAL_NUMBER,        0x0405, sizeof(char[16]),    Test_Read_Serial_Number,        NULL)\
 *      @endcode
 *
 *      @subsubsection did_read_func DID Read Functions
 *
 *      For each defined DID configured for this system, the integrator must provide a Read Function.
 *      This function must be implemented by the project and is responsible for writing the the data
 *      associated with the requested DID to the provided response buffer. This function must provide this
 *      data in a serialized format in network order ready for transmission on a data bus. The amount of
 *      data provided by this function must be exactly equal to the size of the specified data type for
 *      the associated DID.
 *
 *      The read function must adhere to the following prototype:
 *
 *      @code
 *      UDS_Response_Code_T read_function(Serial_Data_Buffer_T *      const p_Resp_SDB);
 *      @endcode

 *
 *      @warning The implementation must write data of the correct type to the provided response object and it must
 *      provide a number of bytes exactly equal to the size of the data type specified for the associated
 *      DID. Writing more data than the buffer can hold can be detected by the return
 *      of the serial data buffer response object serialized function calls.
 *
 *      @warning The configuration of every DID must provide a Read Function. NULL Read Functions are not
 *      supported.
 *
 *      The write function (which is optional per DID) must adhere to the following prototype:
 *
 *      @code
 *      UDS_Response_Code_T write_function(Serial_Data_Buffer_T *      const p_Req_SDB);
 *      @endcode
 *
 *      Writing data to the actual memory in the system is done by the writer handlers, but it is important
 *      that the entirety of the request buffer is read using the deserialize functions of the SDB object.
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "elmos_motor_tune_dids.h"
#include "fault_logger.h"
#include "fault_manager_diag.h"
#include "mfg_data_dids.h"
#include "mfg_data_defines.h"
#include "motor_manager_diag.h"
#include "system_signals.h"

/*===========================================================================*
 * \#define Constants
 *===========================================================================*/
/** Add support for routine control service 0x31 */
/*#define UDS_ENABLE_ROUTINE_CONTROL_SERVICE*/

/**
 * Configure this item for the maximum size of a response on the underlying
 * messaging layer. Keep in mind that this underlying messaging layer could
 * be a logical transport layer or a physical layer.
 */
#define UDS_MAX_RESPONSE_SIZE (128u)
/*===========================================================================*
 * \#define MACROS
 *===========================================================================*/
#define UDS_DID_TABLE \
    /*      DID Name                                ID                      size                                      Read Function                        Write Function */\
    UDS_DID(UDS_MFD_SW_PART_NUM_DID,                0x010Au,                MFD_SW_PART_NUM_LEN,                      MFDIDS_Get_Sw_Part_Number,           NULL)            \
    UDS_DID(UDS_MFD_SW_VERSION_DID,                 0x010Bu,                MFD_SW_VERSION_LEN,                       MFDIDS_Get_SW_Version,               NULL)            \
    UDS_DID(UDS_MFD_HW_PART_NUMBER_DID,             0x010Cu,                MFD_HW_PART_NUM_LEN,                      MFDIDS_Get_HW_Part_Number,           NULL)            \
    UDS_DID(UDS_MFD_EEPROM_CORRUPT_DID,             0x01FEu,                MFD_EEPROM_CORRUPT_DID_SIZE,              NULL,                                MFDIDS_Corrupt_EEPROM_Block)\
    UDS_DID(UDS_MFD_EEPROM_FACTORY_RESET_DID,       0x01FFu,                MFD_EEPROM_FACTORY_RESET_DID_SIZE,        NULL,                                MFDIDS_Reset_EEPROM)\
	\
	UDS_DID(UDS_SYSIG_CPU_TEMP_DID,                 0x0200,                 SYSIG_DIAG_CPU_TEMP_SIZE,                 SYSIG_Diag_Get_CPU_Temp,             SYSIG_Diag_Override_CPU_Temp)\
	UDS_DID(UDS_SYSIG_CURRENT_DID,                  0x0201,                 SYSIG_DIAG_CURRENT_SIZE,                  SYSIG_Diag_Get_Current,              SYSIG_Diag_Override_Current)\
	UDS_DID(UDS_SYSIG_CPU_IDLE_DID,                 0x02FF,                 SYSIG_DIAG_CPU_IDLE_SIZE,                 SYSIG_Diag_Get_CPU_Idle,             NULL)\
    \
    UDS_DID(UDS_MOTOR_STARTUP_IQ_DID,               0x0400,                 MTDIDS_STARTUP_IQ_DID_SIZE,               MTDIDS_Get_Startup_Iq_Target,        MTDIDS_Set_Startup_Iq_Target)\
    UDS_DID(UDS_MOTOR_STARTUP_I_SLOPE_DID,          0x0401,                 MTDIDS_STARTUP_I_SLOPE_DID_SIZE,          MTDIDS_Get_Startup_Current_Slope,    MTDIDS_Set_Startup_Current_Slope)\
    UDS_DID(UDS_MOTOR_STARTUP_ALIGNMENT_TIME_DID,   0x0402,                 MTDIDS_STARTUP_ALIGN_TIME_DID_SIZE,       MTDIDS_Get_Startup_Alignment_Time,   MTDIDS_Set_Startup_Alignment_Time)\
    UDS_DID(UDS_MOTOR_STARTUP_SPEED_RAMP_START_DID, 0x0403,                 MTDIDS_STARTUP_SPEED_RAMP_START_DID_SIZE, MTDIDS_Get_Startup_Speed_Ramp_Start, MTDIDS_Set_Startup_Speed_Ramp_Start)\
    UDS_DID(UDS_MOTOR_STARTUP_SPEED_SLOPE_DID,      0x0404,                 MTDIDS_STARTUP_SPEED_SLOPE_DID_SIZE,      MTDIDS_Get_Startup_Speed_Slope,      MTDIDS_Set_Startup_Speed_Slope)\
    UDS_DID(UDS_MOTOR_STARTUP_SPEED_RAMP_END_DID,   0x0405,                 MTDIDS_STARTUP_SPEED_RAMP_END_DID_SIZE,   MTDIDS_Get_Startup_Speed_Ramp_End,   MTDIDS_Set_Startup_Speed_Ramp_End)\
    UDS_DID(UDS_MOTOR_SPEED_CTRL_KP_DID,            0x0406,                 MTDIDS_SPEED_CTRL_KP_DID_SIZE,            MTDIDS_Get_Speed_Control_Kp,         MTDIDS_Set_Speed_Control_Kp)\
    UDS_DID(UDS_MOTOR_SPEED_CTRL_KI_DID,            0x0407,                 MTDIDS_SPEED_CTRL_KI_DID_SIZE,            MTDIDS_Get_Speed_Control_Ki,         MTDIDS_Set_Speed_Control_Ki)\
    UDS_DID(UDS_MOTOR_CONSTANT_UF,                  0x040B,                 MTDIDS_MOTOR_CONSTANT_UF_DID_SIZE,        MTDIDS_Get_Motor_Constant_Uf,        MTDIDS_Set_Motor_Constant_Uf)\
    UDS_DID(UDS_STALL_DETECT_MIN_SPEED,             0x040C,                 MTDIDS_STALL_DETECT_MIN_SPEED_DID_SIZE,   MTDIDS_Get_Stall_Detect_Min_Speed,   MTDIDS_Set_Stall_Detect_Min_Speed)\
    UDS_DID(UDS_MOTOR_SPEED_CTRL_OUT_MAX_DID,       0x040E,                 MTDIDS_SPEED_CTRL_OUT_MAX_DID_SIZE,       MTDIDS_Get_Speed_Control_Output_Max, MTDIDS_Set_Speed_Control_Output_Max)\
    UDS_DID(UDS_SHORT_CIRCUIT_THRESH_HS_DID,        0x0420,                 MTDIDS_SCTH_HS_DID_SIZE,                  MTDIDS_Get_Short_Circuit_HS_Thresh,  MTDIDS_Set_Short_Circuit_HS_Thresh)\
    UDS_DID(UDS_SHORT_CIRCUIT_THRESH_LS_DID,        0x0421,                 MTDIDS_SCTH_LS_DID_SIZE,                  MTDIDS_Get_Short_Circuit_LS_Thresh,  MTDIDS_Set_Short_Circuit_LS_Thresh)\
    UDS_DID(UDS_SHORT_CIRCUIT_MASKING_TIME_DID,     0x0422,                 MTDIDS_SC_MASKING_TIME_DID_SIZE,          MTDIDS_Get_SC_Masking_Time,          MTDIDS_Set_SC_Masking_Time)\
    UDS_DID(UDS_SHORT_CIRCUIT_DEBOUNCE_TIME_DID,    0x0423,                 MTDIDS_SC_DEBOUNCE_TIME_DID_SIZE,         MTDIDS_Get_SC_Debounce_Time,         MTDIDS_Set_SC_Debounce_Time)\
    UDS_DID(UDS_PWM_DEADTIME_DID,                   0x0430,                 MTDIDS_PWM_DEADTIME_DID_SIZE,             MTDIDS_Get_PWM_Deadtime,             MTDIDS_Set_PWM_Deadtime)\
    UDS_DID(UDS_IRQSTAT_DID,                        0x0440,                 MTDIDS_IRQSTAT_DID_SIZE,                  MTDIDS_Get_IRQSTAT,                  NULL)\
    UDS_DID(UDS_OSC_CONFIG_DID,                     0x0450,                 MTDIDS_OSC_CONFIG_DID_SIZE,               MTDIDS_Get_OSC_CONFIG,               MTDIDS_Set_OSC_CONFIG)\
    \
    UDS_DID(UDS_MOTOR_MGR_MOTOR_STATE_DID,          0x0500,                 MMGR_MOTOR_STATE_DID_SIZE,                MMGR_Get_Diag_Simulate_Stall,        MMGR_Diag_Simulate_Stall)\
    UDS_DID(UDS_MOTOR_MGR_SPEED_RAMP_RATE_DID,      0x0501,                 MMGR_MOTOR_MGR_SPEED_RAMP_RATE_SIZE,      MMGR_Get_Speed_Ramp_Rate,            MMGR_Override_Speed_Ramp_Rate)\
    \
    UDS_DID(UDS_FAULT_LOG_RECORDS_DID,              0x0600,                 FLT_LOG_RECORDS_DID_SIZE,                 FLT_LOG_Get_Fault_Log_Records_DID,   NULL)\
    UDS_DID(UDS_CLEAR_FAULT_ENVIRONMENTAL_LOG_DID,  0x0601,                 FLTEL_CLEAR_LOG_DID_SIZE,                 NULL,                                FLTEL_Clear_Environmental_Log_DID)\
    UDS_DID(UDS_CLEAR_FAULT_LOG_RECORDS_DID,        0x0602,                 FLT_CLEAR_LOG_RECORDS_DID_SIZE,           NULL,                                FLT_LOG_Clear_Fault_Record_DID)\
    /* !!! Do not remove this line !!! */
/*===========================================================================*
 * Custom Type Declarations
 *===========================================================================*/

/** @} doxygen end group */
#endif /* UDS_CFG_H */
