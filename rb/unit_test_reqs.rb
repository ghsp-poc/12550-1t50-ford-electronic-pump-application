require_relative 'unit_test_reqs/sw_requirements'
require_relative 'unit_test_reqs/coverage_report'
require_relative 'unit_test_reqs/parser'
require_relative 'unit_test_reqs/version'

require 'fileutils'

raise "CSV file must be included as arg 0" if ARGV[0].nil?
raise "Search directory must be included as arg 1" if ARGV[1].nil?
raise "Output directory must be included as arg 2" if ARGV[2].nil?

# csv file must be first argument
csv_file = ARGV[0]
# directory to search for files with test_*.c naming convention
directory = ARGV[1]
# output directory to write coverage_report.html to
out_directory = ARGV[2]


coverage_report = File.join(out_directory, "coverage_report.html")

FileUtils.mkdir_p(out_directory)

# find all test files
files = Dir.glob("#{directory}/**/test_*.c")

# parse all test files for sw requirements
unit_test_tags = UnitTestReqs::Parser.parse(files)

# capture all software requirements
sw_reqs = UnitTestReqs::SwRequirements.new(csv_file)

# set coverage for each test tag found
unit_test_tags.each do |utt|
  sw_reqs.set_covered_by_id(utt)
end

# create coverage report object used to generate html report
cr = UnitTestReqs::CoverageReport.new(sw_reqs)

# write the html report to file
File.open(coverage_report, "w") do |f|
  f.puts cr.to_html
end
