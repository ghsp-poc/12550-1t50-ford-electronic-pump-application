require 'csv'
require 'date'

class UnitTestReqs
  class SwRequirement

    # @!attribute [r]
    #   @return [String] Id of requirement
    attr_reader :id
    # @!attribute [r]
    #   @return [String] Project Id of requirement
    attr_reader :project_id
    # @!attribute [r]
    #   @return [String] Baseline of requirement
    attr_reader :baseline
    # @!attribute [r]
    #   @return [String] Text of requirement
    attr_reader :text
    # @!attribute [r]
    #   @return [DateTime] Creation date of requirement
    attr_reader :creation_date_time
    # @!attribute [r]
    #   @return [DateTime] Last modification date of requirement
    attr_reader :modified_date_time
    # @!attribute [r]
    #   @return [String] Category type of requirement
    attr_reader :category
    # @!attribute [r]
    #   @return [String] Key of requirement (table of contents number)
    attr_reader :key
    # @!attribute [r]
    #   @return [Array<String>] Verification methods for requirement
    attr_reader :verification_method

    def initialize(csv_row)
      @id = csv_row["Number"]
      @project_id = csv_row["Project ID (PID)"]
      @baseline = csv_row["Baseline"]
      @text = csv_row["Frame: TEXT"]
      @creation_date_time = DateTime.parse(csv_row["Creation Date"] + csv_row["Creation Time"])
      @modified_date_time = DateTime.parse(csv_row["Modification Date"] + csv_row["Modification Time"])
      @rejected = csv_row["Cat 01:"].include?("Rejected")
      @under_review = csv_row["Cat 01:"].include?("Under Review")
      @category = csv_row["Cat 02:"]
      if csv_row["Cat 09:"].nil?
        @verification_method = ["NA"]
      else
        @verification_method = csv_row["Cat 09:"].split('|')
      end
      @key = csv_row["Key"]
      @text ||= ""
      @key ||= "0"
    end

    # @return [TrueClass, FalseClass] whether the requirement is rejected or not.
    def rejected?
      @rejected
    end

    # @return [TrueClass, FalseClass] whether the requirement is rejected or not.
    def under_review?
      @under_review
    end

    # Sets the requirement as covered by a unit test (or not)
    # @param setting [TrueClass, FalseClass] is it covered or not
    def covered_by_unit_test=(setting)
      @covered = setting
    end

    # Checks if the requirement is covered by a unit test
    # @return [TrueClass, FalseClass] is it covered or not
    def has_unit_test?
      @covered
    end

    def is_covered?
      @covered || !@verification_method.include?("Unit Test")
    end
    # converts the data to a string for textual summary
    # @return [String] the stringified version of the requirement
    def to_s
      text =<<EOS
id:       #{@id}
category: #{@category}
key:      #{@key}
text:     #{@text}
EOS
    end

  end

  # class used to store the the individual software requirements parsed from a CSV dump of cradle.
  class SwRequirements
    
    # @!attribute [r]
    #   @return Array<SwRequirement> list of all requirements
    attr_reader :requirements

    # @!attribute [r]
    #   @return Array<SwRequirement> list of all under review requirements
    attr_reader :under_review_requirements

    # Creates a new SwRequirements object and parses all the requirements form a CSV file dump of cradle
    #
    # @param csv_file [String] the path to the 
    def initialize(csv_file)
      @requirements = []
      @under_review_requirements = []

      CSV.foreach(csv_file, headers: true) do |row|
        this_row = SwRequirement.new(row)
        unless this_row.rejected?
          if this_row.under_review?
            @under_review_requirements << this_row
          else
            @requirements << this_row
          end
        end
      end

      #sort the array by splitting key index and comparing
      [@requirements, @under_review_requirements].each do |reqs|
        reqs.sort! {|a,b| a.key.split('.').map {|n| n.to_i} <=> b.key.split('.').map {|n| n.to_i}}
      end
    end

    # Percentage of requirements that have been covered by a unit test
    # @param sig_digits Number of significant digits after the decimal for the returned Float
    #
    # @return [Float] Percentage
    def coverage_percentage(sig_digits = 2)
      (100.0 * (covered_requirements.length.to_f / @requirements.length.to_f)).round(sig_digits)
    end

    # @return [Fixnum] Number of requirements that have unit test coverage
    def covered_requirements_count
      covered_requirements.length
    end

    # @return Array<UnitTestReqs::SwRequirement> The covered software requirements
    def covered_requirements
      @requirements.select {|r| r.is_covered? }
    end

    # @return Array<UnitTestReqs::SwRequirement> The un-covered software requirements
    def uncovered_requirements
      @requirements.reject {|r| r.is_covered? }
    end

    # @return Array<UnitTestReqs::SwRequirement> The under review software requirements
    def under_review_requirements
      @under_review_requirements
    end

    # Sets a requirement that has the pass in id as covered
    # @param id [String] The id of the requirement
    def set_covered_by_id(id)
      req = get_by_id(id)
      unless req.nil?
        req.covered_by_unit_test = true;
      end
    end

    # Gets a requirement by id
    # @param id [String] The id of the requirement
    #
    # @return [UnitTestReqs::SwRequirement, nil] The requirement found by id (can be nil if not found)
    def get_by_id(id)
      found_index = @requirements.index {|x| x.id == id}
      if found_index.nil?
        nil
      else
        @requirements[found_index]
      end
    end
  end
end



