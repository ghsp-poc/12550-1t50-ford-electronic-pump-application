#ifndef TIME_UTILS_TYPES_H
#define TIME_UTILS_TYPES_H
/**
 *  @file time_utils_types.h
 *
 *  @copyright 2019 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *
 *  @addtogroup time_utils_api Time Utilities API
 *  @{
 *
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/

#include "global.h"

/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/
/**
 * Type used for millisecond unit times
 */
typedef uint32_t Time_MS_T;

/**
 * Type used for the system specific time counting units
 */
typedef uint32_t Time_Ticks_T;

/** @} doxygen end group */

#endif /* TIME_UTILS_TYPES_H */
