#ifndef TIME_UTILS_H
#define TIME_UTILS_H
/**
 *  @file time_utils.h
 *
 *  @copyright 2018 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential@n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup time_utils_api Time Utilities API
 *  @{
 *      @details Time Utility module. Provides a common interface for other libraries to use
 *      as a means to get time related information.
 *
 *      @page ABBR Abbreviations
 *        - TMUT - Time Utilities
 *        - MS   - Milliseconds
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "time_utils_types.h"

/*===========================================================================*
 * Exported Function Prototypes
 *===========================================================================*/
/**
 *  Accessor for the local base-time as defined by the internal ticker storage.
 *  Timestamp does not necessarily relate to any duration of time.
 *
 *  @return Time_MS_T The current base time in milliseconds
 */
Time_MS_T TMUT_Get_Base_Time_MS(void);

/**
 *  Accessor for the local base-time as defined by the internal ticker storage in system
 *  specific time units. Timestamp does not necessarily relate to any duration of time.
 *
 * @return Time_Ticks_T raw system specific time units
 */
Time_Ticks_T TMUT_Get_Base_Time_Ticks(void);

/**
 *  Utility for getting the elapsed time since a specific base time.
 *
 *  @warning The possibility of rollover exists, and is handled for the case of
 *      a single rollover event. Elapsed times that go beyond max value of
 *      Time_MS_T twice will not return a correct value.
 *      
 *  @param  base_time        Base time stamp
 *
 *  @return Time_MS_T The elapsed time in milliseconds
 */
Time_MS_T TMUT_Get_Elapsed_Time_MS(Time_MS_T const base_time);

/**
 *  Utility for getting the elapsed time since a specific base time in system specific time units.
 *
 *  @warning The possibility of rollover exists, and is handled for the case of
 *      a single rollover event. Elapsed times that go beyond max value of
 *      Time_Ticks_T twice will not return a correct value.
 *
 *  @param  base_time        Base time stamp in Time_Ticks_T
 *
 *  @return Time_Ticks_T The elapsed time in system specific time units
 */
Time_Ticks_T TMUT_Get_Elapsed_Time_Ticks(Time_Ticks_T const base_time_ticks);

/**
 * Utility for checking if a specific time has elapsed since a specific base time.
 *
 * @warning The possibility of rollover exists, and is handled for the case of
 *      a single rollover event. Elapsed times that go beyond max value of
 *      Time_MS_T twice may not return a correct value.
 *
 * @param  base_time        Base time stamp
 * @param  max_elapsed_time The elapsed time to evaluate (actual elapsed >= max_elapsed_time)
 *
 * @return                  bool_t
 * @retval                  true when actual elapsed time >= max_elapsed_time
 * @retval                  false when not enough time has elapsed
 */
bool_t TMUT_Has_Time_Elapsed_MS(Time_MS_T const base_time, Time_MS_T const max_elapsed_time);

/**
 * Utility for checking if a specific time has elapsed since a specific base time in system specific time units.
 *
 * @warning The possibility of rollover exists, and is handled for the case of
 *      a single rollover event. Elapsed times that go beyond max value of
 *      Time_Ticks_T twice may not return a correct value.
 *
 * @param  base_time        Base time stamp in Time_Ticks_T
 * @param  max_elapsed_time The elapsed time to evaluate (actual elapsed >= max_elapsed_time) in Time_Ticks_T
 *
 * @return                  bool_t
 * @retval                  true when actual elapsed time >= max_elapsed_time
 * @retval                  false when not enough time has elapsed
 */
bool_t TMUT_Has_Time_Elapsed_Ticks(Time_Ticks_T const base_time, Time_Ticks_T const max_elapsed_time);

/**
 * Converts time in milliseconds to the system native clock ticks
 *
 * @param time_ms - time in milliseconds to convert to system native clock ticks
 *
 * @return time in system native clock ticks
 */
INLINE Time_Ticks_T TMUT_MS_To_Ticks(Time_MS_T time_ms);

/**
 * Converts time in system native clock ticks to milliseconds
 *
 * @param time_ticks - time in system native clock ticks to convert to milliseconds
 *
 * @return time in milliseconds
 */
INLINE Time_MS_T TMUT_Ticks_To_MS(Time_Ticks_T time_ticks);

/** @} doxygen end group */

#endif /* TIME_UTILS_H */
