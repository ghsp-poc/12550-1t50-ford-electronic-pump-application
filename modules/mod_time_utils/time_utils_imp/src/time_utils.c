/**
 *  @file time_utils.c
 *
 *  @ref time_utils_imp
 *
 *  @copyright 2018 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup time_utils_imp Time Utilities Implementation
 *  @{
 *
 *      System specific implementation of the @ref time_utils_api
 *
 *      @page TRACE Traceability
 *        - Design Document(s):
 *          - None
 *
 *        - Requirements Document(s):
 *          - None
 *
 *        - Applicable Standards (in order of precedence: highest first):
 *          - @http{sharepoint/sandbox/ghspdev/LaunchPad/EngineeringDepartment/Elec_proj_mgmt/Revision%20Controlled%20Library/Software%20Process/Work%20Instructions/Coding%20Standard.docx,
 *                  "GHSP Coding Standard" [12-Mar-2006]}
 *
 *      @page DFS Deviations from Standards
 *        - None
 */
#include "global.h"
#include "time_utils.h"
#include "systime.h"

/*===========================================================================*
 * Local Object Definitions
 *===========================================================================*/

/*===========================================================================*
 * Local Function Prototypes
 *===========================================================================*/

/*===========================================================================*
 * Function Definitions
 *===========================================================================*/

Time_MS_T TMUT_Get_Base_Time_MS(void)
{
    return SYSTIME_TICKS_TO_SYSTIME_MS(systime_getU32());
}

Time_Ticks_T TMUT_Get_Base_Time_Ticks(void)
{
    return (Time_Ticks_T)systime_getU32();
}

Time_MS_T TMUT_Get_Elapsed_Time_MS(Time_MS_T const base_time)
{
    return SYSTIME_TICKS_TO_SYSTIME_MS(systime_getU32()) - base_time;
}

Time_Ticks_T TMUT_Get_Elapsed_Time_Ticks(Time_Ticks_T const base_time_ticks)
{
    return (Time_Ticks_T)systime_getU32() - base_time_ticks;
}

bool_t TMUT_Has_Time_Elapsed_MS(Time_MS_T const base_time, Time_MS_T const max_elapsed_time)
{
    Time_MS_T const elapsed_time = (SYSTIME_TICKS_TO_SYSTIME_MS(systime_getU32()) - base_time);
    bool_t has_elapsed = false;

    if (max_elapsed_time <= elapsed_time)
    {
        has_elapsed = true;
    }

    return (has_elapsed);
}

bool_t TMUT_Has_Time_Elapsed_Ticks(Time_Ticks_T const base_time, Time_Ticks_T const max_elapsed_time)
{
    Time_Ticks_T const elapsed_time = ((Time_Ticks_T)systime_getU32() - base_time);
    bool_t has_elapsed = false;

    if (max_elapsed_time <= elapsed_time)
    {
        has_elapsed = true;
    }

    return (has_elapsed);
}

INLINE Time_Ticks_T TMUT_MS_To_Ticks(Time_MS_T time_ms)
{
    return SYSTIME_MS_TO_SYSTIME_TICKS((uint16_t)time_ms);
}

INLINE Time_MS_T TMUT_Ticks_To_MS(Time_Ticks_T time_ticks)
{
    return SYSTIME_TICKS_TO_SYSTIME_MS(time_ticks);
}

/** @} doxygen end group */
