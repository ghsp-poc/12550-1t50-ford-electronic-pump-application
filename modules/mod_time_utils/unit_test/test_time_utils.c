#include "unity.h"
#include "time_utils.h"

/* Mocks */
#include "mock_systime.h"

/* STATIC's */

void setUp(void)
{

}

void tearDown(void)
{
}

void test_TMUT_Get_Base_Time_MS_should_return_the_current_value_of_systime(void)
{
    Time_MS_T const Expected_Ticker = (Time_MS_T)0x12345678u;
    Time_MS_T Actual_Ticker = 0u;

    /* Ensure known test state */

    /* Setup expected call chain */
    systime_getU32_ExpectAndReturn(UINT32_MAX); /* Number returned from this is not important since it'll be converted to MS */
    SYSTIME_TICKS_TO_SYSTIME_MS_ExpectAndReturn(UINT32_MAX, 0x12345678u);

    /* Call function under test */
    Actual_Ticker = TMUT_Get_Base_Time_MS();

    /* Verify test results */
    TEST_ASSERT_EQUAL(Expected_Ticker, Actual_Ticker);
}

void test_TMUT_Get_Elapsed_Time_MS_should_return_the_elapsed_time_in_ms_when_no_rollover(void)
{
    Time_MS_T Actual_Elasped_Time = (Time_MS_T)0u;

    /* Ensure known test state */

    /* Setup expected call chain */
    systime_getU32_ExpectAndReturn(UINT32_MAX); /* Number returned from this is not important since it'll be converted to MS */
    SYSTIME_TICKS_TO_SYSTIME_MS_ExpectAndReturn(UINT32_MAX, 994568u);

    /* Call function under test */
    Actual_Elasped_Time = TMUT_Get_Elapsed_Time_MS(4705u);

    /* Verify test results */
    TEST_ASSERT_EQUAL((Time_MS_T)989863u, Actual_Elasped_Time);
}

void test_TMUT_Get_Elapsed_Time_MS_should_return_the_elapsed_time_in_ms_when_rollover(void)
{
    Time_MS_T Actual_Elasped_Time = (Time_MS_T)0u;
    /* Ensure known test state */

    /* Setup expected call chain */
    systime_getU32_ExpectAndReturn(UINT32_MAX); /* Number returned from this is not important since it'll be converted to MS */
    SYSTIME_TICKS_TO_SYSTIME_MS_ExpectAndReturn(UINT32_MAX, 784568u);

    /* Call function under test */
    Actual_Elasped_Time = TMUT_Get_Elapsed_Time_MS((Time_MS_T)657894453u);

    /* Verify test results */
    TEST_ASSERT_EQUAL((Time_MS_T)3637857411u, Actual_Elasped_Time);
}

void test_TMUT_Has_Time_Elapsed_MS_should_return_true_when_elapsed_time_equal_to_max(void)
{
    bool_t actual_time_has_elapsed = false;
    Time_MS_T const expected_base_time = (Time_MS_T)0x12345678u;
    Time_MS_T const expected_max_elapsed_time = (Time_MS_T)200u;

    /* Ensure known test state */

    /* Setup expected call chain */
    systime_getU32_ExpectAndReturn(UINT32_MAX); /* Number returned from this is not important since it'll be converted to MS */
    SYSTIME_TICKS_TO_SYSTIME_MS_ExpectAndReturn(UINT32_MAX, 0x12345740u);

    /* Call function under test */
    actual_time_has_elapsed = TMUT_Has_Time_Elapsed_MS(expected_base_time, expected_max_elapsed_time);

    /* Verify test results */
    TEST_ASSERT_TRUE(actual_time_has_elapsed);
}

void test_TMUT_Has_Time_Elapsed_MS_should_return_true_when_elapsed_time_greater_than_max(void)
{
    bool_t actual_time_has_elapsed = false;
    Time_MS_T const expected_base_time = (Time_MS_T)0x12345678u;
    Time_MS_T const expected_max_elapsed_time = (Time_MS_T)200u;

    /* Ensure known test state */

    /* Setup expected call chain */
    systime_getU32_ExpectAndReturn(UINT32_MAX); /* Number returned from this is not important since it'll be converted to MS */
    SYSTIME_TICKS_TO_SYSTIME_MS_ExpectAndReturn(UINT32_MAX, 0x12345741u);

    /* Call function under test */
    actual_time_has_elapsed = TMUT_Has_Time_Elapsed_MS(expected_base_time, expected_max_elapsed_time);

    /* Verify test results */
    TEST_ASSERT_TRUE(actual_time_has_elapsed);
}

void test_TMUT_Has_Time_Elapsed_MS_should_return_false_when_elapsed_time_less_than_max(void)
{
    bool_t actual_time_has_elapsed = true;
    Time_MS_T const expected_base_time = (Time_MS_T)0x12345678u;
    Time_MS_T const expected_max_elapsed_time = (Time_MS_T)200u;

    /* Ensure known test state */

    /* Setup expected call chain */
    systime_getU32_ExpectAndReturn(UINT32_MAX); /* Number returned from this is not important since it'll be converted to MS */
    SYSTIME_TICKS_TO_SYSTIME_MS_ExpectAndReturn(UINT32_MAX, 0x1234573Fu);

    /* Call function under test */
    actual_time_has_elapsed = TMUT_Has_Time_Elapsed_MS(expected_base_time, expected_max_elapsed_time);

    /* Verify test results */
    TEST_ASSERT_FALSE(actual_time_has_elapsed);
}

/*------------------*\
 * Ticks test cases *
\*------------------*/


void test_TMUT_Get_Base_Time_Ticks_should_return_the_current_value_of_systime(void)
{
    Time_Ticks_T const Expected_Ticker = (Time_Ticks_T)0x12345678u;
    Time_Ticks_T Actual_Ticker = 0u;

    /* Ensure known test state */

    /* Setup expected call chain */
    systime_getU32_ExpectAndReturn(0x12345678);

    /* Call function under test */
    Actual_Ticker = TMUT_Get_Base_Time_Ticks();

    /* Verify test results */
    TEST_ASSERT_EQUAL(Expected_Ticker, Actual_Ticker);
}

void test_TMUT_Get_Elapsed_Time_Ticks_should_return_the_elapsed_time_in_Ticks_when_no_rollover(void)
{
    Time_Ticks_T Actual_Elasped_Time = (Time_Ticks_T)0u;

    /* Ensure known test state */

    /* Setup expected call chain */
    systime_getU32_ExpectAndReturn(4500);

    /* Call function under test */
    Actual_Elasped_Time = TMUT_Get_Elapsed_Time_Ticks(3000);

    /* Verify test results */
    TEST_ASSERT_EQUAL(1500, Actual_Elasped_Time);
}

void test_TMUT_Get_Elapsed_Time_Ticks_should_return_the_elapsed_time_in_Ticks_when_rollover(void)
{
    Time_Ticks_T Actual_Elasped_Time = (Time_Ticks_T)0u;
    /* Ensure known test state */

    /* Setup expected call chain */
    systime_getU32_ExpectAndReturn(0x000000FF);

    /* Call function under test */
    Actual_Elasped_Time = TMUT_Get_Elapsed_Time_Ticks((Time_Ticks_T)0xFFFFFF00);

    /* Verify test results */
    TEST_ASSERT_EQUAL((Time_Ticks_T)0x1FF, Actual_Elasped_Time);
}

void test_TMUT_Has_Time_Elapsed_Ticks_should_return_true_when_elapsed_time_equal_to_max(void)
{
    bool_t actual_time_has_elapsed = false;
    Time_Ticks_T const expected_base_time = (Time_Ticks_T)1000;
    Time_Ticks_T const expected_max_elapsed_time = (Time_Ticks_T)200u;

    /* Ensure known test state */

    /* Setup expected call chain */
    systime_getU32_ExpectAndReturn(1200);

    /* Call function under test */
    actual_time_has_elapsed = TMUT_Has_Time_Elapsed_Ticks(expected_base_time, expected_max_elapsed_time);

    /* Verify test results */
    TEST_ASSERT_TRUE(actual_time_has_elapsed);
}

void test_TMUT_Has_Time_Elapsed_Ticks_should_return_true_when_elapsed_time_greater_than_max(void)
{
    bool_t actual_time_has_elapsed = false;
    Time_Ticks_T const expected_base_time = (Time_Ticks_T)1000;
    Time_Ticks_T const expected_max_elapsed_time = (Time_Ticks_T)200u;

    /* Ensure known test state */

    /* Setup expected call chain */
    systime_getU32_ExpectAndReturn(1201);

    /* Call function under test */
    actual_time_has_elapsed = TMUT_Has_Time_Elapsed_Ticks(expected_base_time, expected_max_elapsed_time);

    /* Verify test results */
    TEST_ASSERT_TRUE(actual_time_has_elapsed);
}

void test_TMUT_Has_Time_Elapsed_Ticks_should_return_false_when_elapsed_time_less_than_max(void)
{
    bool_t actual_time_has_elapsed = true;
    Time_Ticks_T const expected_base_time = (Time_Ticks_T)1000;
    Time_Ticks_T const expected_max_elapsed_time = (Time_Ticks_T)200u;

    /* Ensure known test state */

    /* Setup expected call chain */
    systime_getU32_ExpectAndReturn(1199);

    /* Call function under test */
    actual_time_has_elapsed = TMUT_Has_Time_Elapsed_Ticks(expected_base_time, expected_max_elapsed_time);

    /* Verify test results */
    TEST_ASSERT_FALSE(actual_time_has_elapsed);
}

void test_TMUT_MS_To_Ticks_passes_value_on_to_systime(void)
{
    Time_Ticks_T result = 0u;

    /* Ensure known test state */

    /* Setup expected call chain */
    SYSTIME_MS_TO_SYSTIME_TICKS_ExpectAndReturn(0x1234, 0x567890);

    /* Call function under test */
    result = TMUT_MS_To_Ticks(0x1234);

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT32(0x567890, result);
}

void test_TMUT_Ticks_To_MS_passes_value_on_to_systime(void)
{
    Time_Ticks_T result = 0u;

    /* Ensure known test state */

    /* Setup expected call chain */
    SYSTIME_TICKS_TO_SYSTIME_MS_ExpectAndReturn(0x1234, 0x567890);

    /* Call function under test */
    result = TMUT_Ticks_To_MS(0x1234);

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT32(0x567890, result);
}
