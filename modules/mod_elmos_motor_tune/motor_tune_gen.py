import argparse
import sys
import os

from sys import version_info
if version_info.major == 2:
    from StringIO import StringIO ## for Python 2
elif version_info.major == 3:
    from io import StringIO ## for Python 3
import motor_tune_header_template

UINT16_MAX = int(0xFFFF)
UINT16_MIN = 0
INT16_MAX  = int(0x7fff)
INT16_MIN  = (-int(0x7fff) - 1)

arg_parser = argparse.ArgumentParser(description = 'Create a C header file with based on the provided Elmos motor configuration file')
arg_parser.add_argument('elmos_cfg_file', help='File produced by the Elmos motor tuning GUI')
arg_parser.add_argument('-o', '--output',nargs='?', type=argparse.FileType('wt'), default=sys.stdout, help='header file to write')
args = arg_parser.parse_args()

outfilename = os.path.basename(args.output.name)
include_guard = outfilename.upper().replace(".","_")
defines_string = ""

with open (args.elmos_cfg_file, 'rt') as cfg_file:
    config_dict = {}
    for line in cfg_file:
       key,value = line.split("=")
       config_dict[key] = value.strip()

    # Output the config file
    defines_string_io = StringIO()
    
    defines_string_io.write("#define STARTUP_I_Q_TARGET ({})\n".format(config_dict["startup_i_q_target"]))
    defines_string_io.write("#define STARTUP_CURRENT_RAMP_SLOPE ({})\n".format(config_dict["startupCurrentRampSlope"]))
    
    output_startup_alignment_time = int(config_dict["startupAlignmentTime"]) * 20
    defines_string_io.write("#define STARTUP_ALIGNMENT_TIME ({}u)\n".format(output_startup_alignment_time))
    
    #
    # Calcuation Provided by Elmos:
    #
    #                    UINT16_MAX
    #                    ----------
    # Speed Ramp Start x     60
    #                    ----------
    #                       20000
    #
    output_startup_speed_ramp_start = int((float(config_dict["startupSpeedRampStart"]) * (float(UINT16_MAX) / 60.0 / 20000.0)) + 0.5)
    defines_string_io.write("#define STARTUP_SPEED_RAMP_START ({}u)\n".format(output_startup_speed_ramp_start))
    
    defines_string_io.write("#define STARTUP_SPEED_RAMP_SLOPE ({})\n".format(config_dict["startupSpeedRampSlope"]))
    
    #
    # Calcuation Provided by Elmos:
    #
    #                  UINT16_MAX
    #                  ----------
    # Speed Ramp End x     60
    #                  ----------
    #                    20000
    #
    output_startup_speed_ramp_end = int((float(config_dict["startupSpeedRampEnd"]) * (float(UINT16_MAX) / 60.0 / 20000.0)) + 0.5)
    defines_string_io.write("#define STARTUP_SPEED_RAMP_END ({}u)\n".format(output_startup_speed_ramp_end))
    
    defines_string_io.write("#define SPEED_CTRL_KP ({})\n".format(config_dict["speedCtrlKP"]))
    
    defines_string_io.write("#define SPEED_CTRL_KI ({}u)\n".format(config_dict["speedCtrlKI"]))
    
    output_speed_control_timebase = int(config_dict["speedCtrlTimebase"]) * 20
    defines_string_io.write("#define SPEED_CTRL_TIMEBASE ({}u)\n".format(output_speed_control_timebase))
    
    defines_string_io.write("#define SPEED_CTRL_OUTPUT_MAX ({})\n".format(config_dict["speedCtrlOutputMax"]))
    
    defines_string_io.write("#define SPEED_CTRL_OUTPUT_MIN ({})\n".format(config_dict["speedCtrlOutputMin"]))
    
    if (int(config_dict["speedCtrlMotorDirection"]) == 0):
        defines_string_io.write("#define SPEED_CTRL_MOTOR_DIRECTION (FORWARD)\n")
    elif (int(config_dict["speedCtrlMotorDirection"]) == 1):
        defines_string_io.write("#define SPEED_CTRL_MOTOR_DIRECTION (BACKWARD)\n")
    else:
        defines_string_io.write("Invalid value for speedCtrlMotorDirection in input file")
    
    defines_string_io.write("#define SYNC_PARAM_BEMF_AMPL_MIN ({})\n".format(config_dict["syncParamBemfAmplMin"]))
    
    output_sync_param_sync_duration = int(config_dict["syncParamSyncDuration"]) * 20
    defines_string_io.write("#define SYNC_PARAM_SYNC_DURATION ({}u)\n".format(output_sync_param_sync_duration))
    
    output_sync_param_sync_timeout = int(config_dict["syncParamSyncTimeout"]) * 20
    defines_string_io.write("#define SYNC_PARAM_SYNC_TIMEOUT ({}u)\n".format(output_sync_param_sync_timeout))
    
    if (int(config_dict["syncParamSyncEnable"]) == 0):
        defines_string_io.write("#define SYNC_PARAM_SYNC_ENABLE (false)\n")
    else:
        defines_string_io.write("#define SYNC_PARAM_SYNC_ENABLE (true)\n")

    motor_phase_R = float(config_dict["motorPhaseR"])
    motor_phase_L = float(config_dict["motorPhaseL"])
    motor_coeff_scaler = 9

    #
    # Calculation provided by Elmos:
    # 
    #                     R ohm
    #                     -----
    #                      1000
    # 1 - (-------------------- ) x 512
    #                   L henry
    #          20 kHz * -------
    #                   1000000
    #
    # Simplifies to:
    #
    #          R ohm
    #          -----
    #          1000
    # 1 - (------------) x 512
    #         L henry
    #         -------
    #           50
    #
    motor_model_coeff_a = int(((1 - (((motor_phase_R + 0.5) / 1000.0) / ((motor_phase_L + 0.5) / 50.0))) * (1 << motor_coeff_scaler)) + 1)
    
    #
    # Calculation provided by Elmos:
    #          1
    # (----------------) x 512
    #           L henry
    #  20 kHz * -------
    #           1000000
    #
    # Simplifies to:
    #
    #       50
    # (------------) x 512
    #    L henry
    #    
    
    #motor_model_coeff_b = int((1 / (motor_phase_L / 50.0)) * (1 << motor_coeff_scaler))
    motor_model_coeff_b = int(((50.0 / motor_phase_L) * (1 << motor_coeff_scaler)) + 1)
    defines_string_io.write("#define MOTOR_MODEL_COEFF_A ({})\n".format(motor_model_coeff_a))
    defines_string_io.write("#define MOTOR_MODEL_COEFF_B ({})\n".format(motor_model_coeff_b))
    defines_string_io.write("#define MOTOR_MODEL_COEFF_SCALER ({}u)\n".format(motor_coeff_scaler))
    
    if (int(config_dict["focAngleSensMode"]) == 0):
        defines_string_io.write("#define FOC_ANGLE_SENSE_MODE (FOC_SENSORLESS)\n")
    elif (int(config_dict["focAngleSensMode"]) == 1):
        defines_string_io.write("#define FOC_ANGLE_SENSE_MODE (FOC_HALL_SENSOR)\n")
    else:
        defines_string_io.write("Invalid value for focAngleSensMode in input file\n")
    
    defines_string_io.write("#define I_CTRL_KP ({})\n".format(config_dict["i_d_ctrlKP"]))
    
    defines_string_io.write("#define I_CTRL_KI ({}u)\n".format(config_dict["i_d_ctrlKI"]))
    
    defines_string_io.write("#define I_D_TARGET ({})\n".format(config_dict["i_d_target"]))
    
    defines_string_io.write("#define CURRENT_COMP_KP ({})\n".format(config_dict["current_comp_Kp"]))
    
    defines_string_io.write("#define CURRENT_COMP_KD ({})\n".format(config_dict["current_comp_Kd"]))
    
    defines_string_io.write("#define CURRENT_COMP_SCALER ({}u)\n".format(config_dict["current_comp_scaler"]))
    
    defines_string_io.write("#define MOTOR_CONSTANT_UF ({}u)\n".format(config_dict["motorConstantUF"]))
    
    defines_string_io.write("#define STALL_DET_MIN_SPEED ({}u)\n".format(config_dict["stallDetMinSpeed"]))
    
    defines_string_io.write("#define STALL_DET_THRESHOLD ({})\n".format(config_dict["stallDetThrshld"]))
    
    shunt_resistance = int(config_dict["sensParamShuntRes"])
    opa_gain = float(config_dict["sensParamOpaGain"])
    voltage_divider = float(config_dict["sensParamVoltageDivider"])
    
    #
    # Calculation provided by Elmos:
    #
    #                  2.5
    # ---------------------------- x UINT16_MAX
    # 4095 x Voltage Divider Ratio 
    #
    voltage_conversion_factor = int((2.5 / (4095 * voltage_divider)) * UINT16_MAX)
    
    #
    # Calculation provided by Elmos:
    #
    #                  2.5
    # ------------------------------------- x UINT16_MAX
    #                      Shunt Resistance
    # 4095 x Op Amp Gain x ---------------- 
    #                          1000
    #
    current_conversion_factor = int((2.5 / (4095 * opa_gain * (shunt_resistance / 1000.0))) * UINT16_MAX)
    
    defines_string_io.write("#define SENS_PARAM_VOLTAGE_FACTOR ({})\n".format(voltage_conversion_factor))
    
    defines_string_io.write("#define SENS_PARAM_CURRENT_FACTOR ({})\n".format(current_conversion_factor))
    
    defines_string = defines_string_io.getvalue()
    
    defines_string_io.close()
    
args.output.write(motor_tune_header_template.header_file_template.format(defines=defines_string, guard=include_guard, file=outfilename))