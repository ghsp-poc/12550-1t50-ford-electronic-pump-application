#D:\PROGRAMME\Perl\bin\ perl

use strict;
use warnings;
use Cwd;

use constant HEADER_FILE_PATH => "..\\inc\\";

use constant LIST_FILE_NAME_52305 =>  "52305_foc_var.list";
use constant VAR_ADDRESS_FILE_NAME_52305 =>  "52305_foc_var_addresses.map";

use constant LIST_FILE_NAME_52306 =>  "52306_foc_var.list";
use constant VAR_ADDRESS_FILE_NAME_52306 =>  "52306_foc_var_addresses.map";

use constant LIST_FILE_NAME_52352 =>  "52352_foc_var.list";
use constant VAR_ADDRESS_FILE_NAME_52352 =>  "52352_foc_var_addresses.map";

use constant TARGET_PLATFORM_52305 => "52305";
use constant TARGET_PLATFORM_52306 => "52306";
use constant TARGET_PLATFORM_52352 => "52352";

my $linkerMapFile=$ARGV[0];
my $targetPlatform;
my $listFileName;
my $varAddressFileName;

my $usrCfgFileName;

my @files;
my @lines;

my %structs;
my %enums;

# enter the variables for which we want the memory layout here, their respective types must be of the same name with "_t" appended to it
my @target_vars = qw (mc_data foc_ctrl_loop_data hall_data uart_system_info adc_data speed_meas_data app_template_data app_speedpoti_demo_data );

if ($linkerMapFile =~ /foc_e52305a/)
{
	$targetPlatform = TARGET_PLATFORM_52305;
	$listFileName = LIST_FILE_NAME_52305;
	$varAddressFileName = VAR_ADDRESS_FILE_NAME_52305;
}
elsif ($linkerMapFile =~ /foc_e52306a/)
{
	$targetPlatform = TARGET_PLATFORM_52306;
	$listFileName = LIST_FILE_NAME_52306;
	$varAddressFileName = VAR_ADDRESS_FILE_NAME_52306;
}
elsif ($linkerMapFile =~ /foc_e52352a/)
{
	$targetPlatform = TARGET_PLATFORM_52352;
	$listFileName = LIST_FILE_NAME_52352;
	$varAddressFileName = VAR_ADDRESS_FILE_NAME_52352;
}
else
{
	die "ERROR! Could not detect target platform, unknown compiler map file!\n";
}

print "# working in " . cwd() . "\n";
print "# target platform: " . $targetPlatform . "\n\n";

generateVariableListing();
getAtomicDataTypes();
computeMemoryLayout();


########
# SUBS #
########

sub computeMemoryLayout
{	
	open RESULT_FILE, ">" . $varAddressFileName or errorHandler("ERROR! Could not create result file " . $varAddressFileName . ": " . $!);
	
	print "# reading " . $listFileName ." ...\n";
	open FH, "<" . $listFileName or errorHandler("ERROR! Could not open file " . $listFileName . ": " . $!);
	@lines = <FH>;
	
	# create a hash of all enums
	for (my $i = 0; $i <= $#lines; $i++)
	{
		if ($lines[$i] =~ /^typedef enum/i)
		{			
			while (!($lines[$i] =~ /^}\s*[\w]+.*;/))
			{
				# ... "walk" to the end of the typedef
				$i = $i + 1;
			}
			
			if ($lines[$i] =~ /^}\s*([\w]+).*;/)
			{
				$enums{$1} = 1;
			}
		}
		elsif ($lines[$i] =~ /defines\.h/)
		{
			last;
		}
	}
	
	# foreach my $key (keys %enums)
	# {
		# print RESULT_FILE $key . "\n";
	# }
	
	# create a hash of all structs
	for (my $i = 0; $i <= $#lines; $i++)
	{
		if ($lines[$i] =~ /^typedef struct/i)
		{			
			while (!($lines[$i] =~ /^}\s*[\w]+.*;/))
			{
				# ... "walk" to the end of the typedef
				$i = $i + 1;
			}
			
			if ($lines[$i] =~ /^}\s*([\w]+).*;/)
			{
				# add this struct to the list, however the value is only a placeholder for now
				$structs{$1}{"signedness"} = 0;
			}
		}
		elsif ($lines[$i] =~ /defines\.h/)
		{
			last;
		}
	}
	
	# foreach my $key (keys %structs)
	# {
		# print RESULT_FILE $key . "\n";
	# }
	
	print "# trying to resolve all dependencies within the structs ...\n";
	
	# now comes the REALLY tricky part: solve the dependencies of the structs (because they can consist of structs itself) -> recursive solution
	foreach (@target_vars)
	{
		my $targetStruct = $_;
		
		$structs{$targetStruct . "_t"}{'signedness'} = 0;
		$structs{$targetStruct . "_t"}{'atomic'} = 0;
		$structs{$targetStruct . "_t"}{'length'} = getStructLength($targetStruct . "_t", @lines);
		
		print "\tVariable " . $targetStruct . " parsed\n";
	}
	
	# foreach my $key (keys %structs)
	# {
		# print $key . " " . $structs{$key}{'length'} ."\n";
	# }
	
	print "# computing absolute addresses for elements of struct ...\n";
	
	foreach (@target_vars)
	{
		my $targetStruct = $_;
		my $address = getAbsoluteAddressOfVar($targetStruct, @lines);
		if (0 != hex($address))
		{
			print "\tVariable " . $targetStruct . " @" . $address . "\n";
			print RESULT_FILE "\n" . $targetStruct . " @" . $address . ":\n\n";
			
			getAbsoluteAddressesOfElements($targetStruct . "_t", $targetStruct, hex($address), @lines);
		}
	}
	
	print "# Finished successfully ...\n";
	
	close RESULT_FILE;
}

###############
# parse the linker map file as well as the header files (indicated via the script's arguments) and simply copy all relevant information into RESULT_FILE
sub generateVariableListing
{
	my %struct_address;
	my %struct_size;

	open RESULT_FILE, ">" . $listFileName or errorHandler("ERROR! Could not create result file " . $listFileName . ": " . $!);

	print RESULT_FILE "\n================\n";
	print RESULT_FILE "VARIABLE LISTING\n";
	print RESULT_FILE "================\n\n";
	
	print RESULT_FILE "Build date: " . localtime() . "\n";
	print RESULT_FILE "Working directory: " . cwd() . "\n\n";

	open FH, "<" . HEADER_FILE_PATH . "defines.h"  or errorHandler("ERROR! Could not open file " . HEADER_FILE_PATH . "defines.h" . ": " . $!);
	@lines = <FH>;

	for (my $i = 0; $i <= $#lines; $i++)
	{
		#print $lines[$i];
		
		chomp($lines[$i]);
		
		if ($lines[$i] =~ /^#define\s+FIRMWARE_MAJOR_VERSION\s+(\S+)/)
		{
			print RESULT_FILE "Firmware major version: " . $1 . "\n";
		}
		
		if ($lines[$i] =~ /^#define\s+FIRMWARE_MINOR_VERSION\s+(\S+)/)
		{
			print RESULT_FILE "Firmware minor version: " . $1 . "\n\n";
		}
	}
	
	# determine the user configuration file
	my $usrCfgValue = compilerDefineValue("USR_CONFIGURATION", @lines);		
	$usrCfgFileName = getUserCfgFile($usrCfgValue, @lines);
	
	if ($usrCfgFileName eq "")
	{
		errorHandler("ERROR! Could not retrieve user configuration file! USR_CONFIGURATION = " . $usrCfgValue . "\n");
	}
	
	if ($usrCfgFileName !~ /$targetPlatform/)
	{
		errorHandler("target platform (" . $targetPlatform . ") is not part of user configuration file name (" . $usrCfgFileName. ")!");
	}
	
	print RESULT_FILE "USR_CONFIGURATION = " . $usrCfgValue . " -> " . $usrCfgFileName . "\n\n";

	close FH;

	print "# reading linker map file ...\n";
	open FH, "<" . $linkerMapFile or errorHandler("ERROR! Could not open file " . $linkerMapFile . ": " . $!);
	@lines = <FH>;

	for (my $i = 0; $i <= $#lines; $i++)
	{
		#print $lines[$i];
		
		chomp($lines[$i]);
		
		if ($lines[$i] =~ /LIBRARY MODULE/)
		{
			# the content of the map file beneath this line is of no interest to us
			last;
		}
		
		if ($lines[$i] =~ /^DATA16_[I|Z].{0,1}$/)
		{
			my $addressBuffer = "";
			my $sizeBuffer = "";
			my $nameOffset = "";
			
			# get the address of the variable ...
			if ($lines[$i + 1] =~ /address: (\w{4})/)
			{
				$addressBuffer = $1;
			}
			else
			{
				errorHandler("ERROR! Could not parse line: " . $lines[$i + 1] . "\n");
			}
			
			# ... and get the size of it
			if ($lines[$i + 1] =~ /\((\S+)\sbytes\)/)
			{
				$sizeBuffer = $1;
			}
			else
			{
				errorHandler("ERROR! Could not parse line: " . $lines[$i + 1] . "\n");
			}
			
			# now retrieve the name which can be found a number of lines below
			for ($nameOffset = $i + 2; ;$nameOffset++)
			{
				if ($lines[$nameOffset] =~ /=====/)
				{
					$nameOffset = $nameOffset + 1;;
					
					if ($lines[$nameOffset] =~ /\s+(\S+)/)
					{
						$struct_address{$1} = $addressBuffer;
						$struct_size{$1} = $sizeBuffer;
						last;
					}
					else
					{
						errorHandler("ERROR! Could not retrieve variable name in line: " . $lines[$nameOffset] . "\n");
					}
				}
			}
			
		}
	}

	close FH;

	print RESULT_FILE "\n=========\nVariables\n=========\n\n";

	foreach my $key (keys %struct_address)
	{
		print RESULT_FILE $key . " 0x" . $struct_address{$key} . " " . $struct_size{$key} . "\n";
	}

	print RESULT_FILE "\n========\nTypedefs\n========\n\n";

	# now we are going through the given header files and retrieve every struct and every enum
	for (my $i = 1; $i <= $#ARGV; $i++)
	{
	#	print $ARGV[$i] . "\n";

		open FH, "<" . HEADER_FILE_PATH . $ARGV[$i] or errorHandler("ERROR! Could not open file " . HEADER_FILE_PATH . $ARGV[$i] . ": " . $!);

		@lines = <FH>;
		
		for (my $cnt = 0; $cnt <= $#lines; $cnt++)
		{
			#print RESULT_FILE $lines[$cnt];
			
			chomp($lines[$cnt]);
			
			# new typedef found ...
			if ($lines[$cnt] =~ /^\s*typedef\s+[enum|struct]/i)
			{
				while (!($lines[$cnt] =~ /^\s*}[\w|\s]+;/))
				{
					# remove comments and ...
					$lines[$cnt] =~ s/\/\*(.+)\*\///;
					# ... print it to result file
					print RESULT_FILE $lines[$cnt];
					$cnt = $cnt + 1;
				}
				
				print RESULT_FILE $lines[$cnt] . "\n\n";
			}
		}
		
		close FH;
	}
	
	print RESULT_FILE "\n========\nParsed files\n========\n\n";

	print "# reading defines.h ...\n";
	open FH, "<" . HEADER_FILE_PATH . "defines.h"  or errorHandler("ERROR! Could not open file " . HEADER_FILE_PATH . "defines.h" . ": " . $!);
	@lines = <FH>;

	print RESULT_FILE "\n=========\ndefines.h\n=========\n\n";

	for (my $i = 0; $i <= $#lines; $i++)
	{
		#print $lines[$i];
		print RESULT_FILE $lines[$i];
	}
	
	close FH;

	print "# reading " . $usrCfgFileName. " ...\n";
	open FH, "<" . "..\\app_user\\user_configurations\\" . $usrCfgFileName  or errorHandler("ERROR! Could not open file " . HEADER_FILE_PATH . $usrCfgFileName . ": " . $!);
	@lines = <FH>;

	print RESULT_FILE "\n=============\n" . $usrCfgFileName ."\n=============\n\n";

	for (my $i = 0; $i <= $#lines; $i++)
	{
		#print $lines[$i];
		print RESULT_FILE $lines[$i];
	}
	
	close FH;
	
	# now we attach simply all other header files
	for (my $i = 1; $i <= $#ARGV; $i++)
	{
		#	print $ARGV[$i] . "\n";

		print "# reading " . $ARGV[$i] . "...\n";
		open FH, "<" . HEADER_FILE_PATH . $ARGV[$i] or errorHandler("ERROR! Could not open file " . HEADER_FILE_PATH . $ARGV[$i] . ": " . $!);

		@lines = <FH>;
		
		print RESULT_FILE "\n=========\n" . $ARGV[$i] ."\n=========\n\n";
				
		for (my $i = 0; $i <= $#lines; $i++)
		{
			#print $lines[$i];
			print RESULT_FILE $lines[$i];
		}
		
		close FH;
	}
	
	close RESULT_FILE;
}

###############
# determine the length of the given structure in bytes and put it into hash %structs
# NOTE: the length is only true if the structure were to be PACKED (which, by standard configuration of the compiler, it is not!)
# NOTE: function is recursive!
sub getStructLength
{
	my ($typedefString, @listFileContent) = @_;
	
	# print $typedefString;
	my $structLength = 0;
	
	for (my $i = 0; $i <= $#listFileContent; $i++)
	{
		if ($listFileContent[$i] =~ /^defines\.h/)
		{
			# the content of the list file beneath this line is of no interest to us; if we didn't find our typedef until now ...
			errorHandler("ERROR! Could not find typedef " . $typedefString ."!");
		}
		elsif ($listFileContent[$i] =~ /\}\s*$typedefString\s*;/)
		{		
			#print $listFileContent[$i];
			# we have found the bottom of the looked-for typedef, now go back to its top
			
			my $structStartIdx = 0;
			
			for ($structStartIdx = $i; $structStartIdx > 0; $structStartIdx--)
			{
				if ($lines[$structStartIdx] =~ /^\s*typedef\s+struct/i) { $structStartIdx = $structStartIdx + 1; last; }
			}
			
			$structs{$typedefString}{'startIndex'} = $structStartIdx;
			
			if (0 == $structStartIdx) { errorHandler("ERROR! Reached beginning of file while searching for beginning of typedef " . $typedefString . "!"); }
			
			# now iterate through the lines and compute the length
			for (my $cnt = $structStartIdx; $cnt < $i; $cnt++)
			{
				# remove all literals that might come before the element type yet are legal from a programming point of view
				$lines[$cnt] =~ s/volatile//g;

				# check for ifdefs
				if ($lines[$cnt] =~ /^\s*#ifdef\s+(\w+)/i)
				{
					# check whether the define is set or not and increment count to an endif line, if applicable //debug: this will fail in case of nested ifdefs....
					if (!(compilerDefineSet($1, @lines)))
					{
						while (!($lines[$cnt] =~ /^\s*#endif\s*/)) { $cnt = $cnt + 1; }
					}
				}
				# check for ifndefs
				elsif ($lines[$cnt] =~ /^\s*#ifndef\s+(\w+)/i)
				{
					# check whether the define is set or not and increment count to an endif line, if applicable //debug: this will fail in case of nested ifdefs....
					if (compilerDefineSet($1, @lines))
					{
						while (!($lines[$cnt] =~ /^\s*#endif\s*/)) { $cnt = $cnt + 1; }
					}
				}
				# check for #if ( DEFINE == xxx )
				elsif ($lines[$cnt] =~ /^\s*#if\s+\(*\s*(\w+)\s*==\s*(\w+)/i)
				{
					# check the value of the define and increment count to an endif line, if applicable //debug: this will fail in case of nested ifs....
					if ($2 ne compilerDefineValue($1, @lines))
					{
						while (!($lines[$cnt] =~ /^\s*#endif\s*/)) { $cnt = $cnt + 1; }
					}

				}
				# check for #if ( DEFINE != xxx )
				elsif ($lines[$cnt] =~ /^\s*#if\s+\(*\s*(\w+)\s*!=\s*(\w+)/i)
				{
					# check the value of the define and increment count to an endif line, if applicable //debug: this will fail in case of nested ifs....
					if ($2 eq compilerDefineValue($1, @lines))
					{
						while (!($lines[$cnt] =~ /^\s*#endif\s*/)) { $cnt = $cnt + 1; }
					}

				}
				# check whether its empty or commented line
				elsif (($lines[$cnt] =~ /^\s*$/i) || ($lines[$cnt] =~ /^\s*\/[\/|*]/i))
				{
				}
				# check whether it is a pointer
				elsif ($lines[$cnt] =~ /\s*\w+\s\*\s(\w+)/)
				{
					$structLength = $structLength + 2;
				}
				# check for data type of the element
				elsif ($lines[$cnt] =~ /\s*(\w+)\s+(\w+)/)
				{
					my $elementDataType = $1;
					my $elementName = $2;
					my $elementLength = 0; # in bytes
					
					#print "# Found element in " . $typedefString . ": " . $1 . " " . $2 . "\n";
					
					# if the size of this element is known (enum or struct), add the length of it; that's all :)
					if (defined($enums{$elementDataType}))
					{
						$elementLength = 1;
					}
					# in this case that struct is NOT known, so get to know it ...
					elsif (!defined($structs{$elementDataType}{'signedness'}) || !defined($structs{$elementDataType}{'length'}))
					{
						# ... to understand recursion, first you must understand recursion ;)
						# call this very function in order to determine the length of the struct in question
						$elementLength = getStructLength($elementDataType, @lines);
						$structs{$elementDataType}{'signedness'} = 0;
						$structs{$elementDataType}{'length'} = $elementLength;
						$structs{$elementDataType}{'atomic'} = 0;
					}
					else
					{
						$elementLength = $structs{$elementDataType}{'length'};
					}
					
					$structLength = $structLength + $elementLength;
				}
				elsif ($lines[$cnt] =~ /^\s*#endif\s*/)
				{
					# nothing to do; this happens when the corresponding define is set
				}
				else
				{
					#what now ... ?
					errorHandler("ERROR! Can not parse this line of struct " . $typedefString . ": " . $lines[$cnt] . "\n");
				}
			}
			
			last;
		}
	}

	return $structLength;
}

###############
# retrieve the absolute address of a variable (not struct typedef!) from the linker map file
sub getAbsoluteAddressOfVar
{
	my ($varName, @listFileContent) = @_;
	
	for (my $i = 0; $i <= $#listFileContent; $i++)
	{
		if ($listFileContent[$i] =~ /Typedefs/)
		{
			# the content of the list file beneath this line is of no interest to us; if we didn't find our typedef until now ...
			#errorHandler("ERROR! Could not find variable " . $varName ."!");
			showLogMessage("Could not find variable " . $varName ."!");
			return "0x0000";
		}
		elsif ($listFileContent[$i] =~ /^$varName\s(\w+)\s\w+/)
		{
			return $1;
		}
	}	
}

###############
# determine the absolute address of every element of variable (=instance of a struct typedef)
# NOTE: function is recursive!
sub getAbsoluteAddressesOfElements
{
	my ($typedefString, $hierarchyString, $startAddress, @listFileContent) = @_;
	
	#print $typedefString . " = " . $startAddress . "\n";
	
	# this value holds the end address of the structure only at the end of the execution of this function
	my $endAddress = $startAddress;
	
	# now iterate through the lines and compute the absolute address for every element
	for (my $i = $structs{$typedefString}{'startIndex'};; $i++)
	{
		# remove all literals that might come before the element type yet are legal from a programming point of view
		$listFileContent[$i] =~ s/volatile//g;

		# check for ifdefs
		if ($listFileContent[$i] =~ /^\s*#ifdef\s+(\w+)/i)
		{
			# check whether the define is set or not and increment count to an endif line, if applicable //debug: this will fail in case of nested ifdefs....
			if (!(compilerDefineSet($1, @listFileContent)))
			{
				while (!($listFileContent[$i] =~ /^\s*#endif\s*/)) { $i = $i + 1; }
			}
		}
		# check for ifndefs
		elsif ($listFileContent[$i] =~ /^\s*#ifndef\s+(\w+)/i)
		{
			# check whether the define is set or not and increment count to an endif line, if applicable //debug: this will fail in case of nested ifdefs....
			if (compilerDefineSet($1, @listFileContent))
			{
				while (!($listFileContent[$i] =~ /^\s*#endif\s*/)) { $i = $i + 1; }
			}
		}
		# check for #if ( DEFINE == xxx )
		elsif ($listFileContent[$i] =~ /^\s*#if\s+\(*\s*(\w+)\s*==\s*(\w+)/i)
		{
			# check the value of the define and increment count to an endif line, if applicable //debug: this will fail in case of nested ifs....
			if ($2 ne compilerDefineValue($1, @lines))
			{
				while (!($listFileContent[$i] =~ /^\s*#endif\s*/)) { $i = $i + 1; }
			}
		}
		# check for #if ( DEFINE != xxx )
		elsif ($listFileContent[$i] =~ /^\s*#if\s+\(*\s*(\w+)\s*!=\s*(\w+)/i)
		{
			# check the value of the define and increment count to an endif line, if applicable //debug: this will fail in case of nested ifs....
			if ($2 eq compilerDefineValue($1, @lines))
			{
				while (!($listFileContent[$i] =~ /^\s*#endif\s*/)) { $i = $i + 1; }
			}
		}
		# check whether its empty or commented line
		elsif (($listFileContent[$i] =~ /^\s*$/i) || ($listFileContent[$i] =~ /^\s*\/[\/|*]/i))
		{
		}
		# a pointer inside the structure, no need to compute anything, a pointer is always 16 bit
		elsif ($listFileContent[$i] =~ /\s*\w+\s\*\s(\w+)/)
		{
			#print RESULT_FILE $hierarchyString . "." . $1 . " 0x" . sprintf("%04X", $endAddress) . " pointer " . 2 . " " . 0 . "\n";
			if (0 != ($endAddress % 2))
			{
				$endAddress = $endAddress + 3;
			}
			else
			{
				$endAddress = $endAddress + 2;
			}
		}
		# element of structure -> compute its absolute address and print it to the result file
		elsif ($listFileContent[$i] =~ /\s*(\w+)\s+(\w+)/)
		{
			my $elementDataType = $1;
			my $elementName = $2;
			my $elementLength = 0; # in bytes
			my $elementSignedness = 0;
			my $lengthMult = 1;
			
			# if it's an array ...
			if ($listFileContent[$i] =~ /\s*(\w+)\s+(\w+)(\[(.+)\])/)
			{
				my $buf = $4;
				
				# print "# FOUND ARRAY!! " . $hierarchyString . "." . $elementName . " length: " . $4 . "\n";				
				$elementName = $elementName . $3;
				$elementName =~ s/\s//g;
				
				if ($buf =~ /\s*([0-9])\s*/)
				{
					$lengthMult = $1;
				}
				elsif ($buf =~ /\s*(\w+)\s*/)
				{
					$lengthMult = compilerDefineValue($1, @listFileContent);
				}
				else
				{
					errorHandler("ERROR! Could not process array length indicator: " . $buf);
				}
			}

			#print "# Found element: " . $hierarchyString . "." . $elementName . "@" . $endAddress ."\n";
			
			# if it's an enum ...
			if (defined($enums{$elementDataType}))
			{
				$elementLength = 1;				
				$elementSignedness = 0;
				
				print RESULT_FILE $hierarchyString . "." . $elementName . " 0x" . sprintf("%04X", $endAddress) . " enum " . $elementLength . " " . $elementSignedness . "\n";
				
				for (my $cnt = 0; $cnt < $lengthMult; $cnt++)
				{
					$endAddress = $endAddress + $elementLength;
				}
			}
			# if it's u16, s16, ...
			elsif (1 == $structs{$elementDataType}{'atomic'})
			{
				$elementLength = $structs{$elementDataType}{'length'};				
				$elementSignedness = $structs{$elementDataType}{'signedness'};
				
				# increment the address to a multiple of 2 if the length (in bytes) of the element is divisible by 2
				if ((0 != ($endAddress % 2)) && (0 == ($elementLength % 2)))
				{
					$endAddress = $endAddress + 1;
				}
					
				print RESULT_FILE $hierarchyString . "." . $elementName . " 0x" . sprintf("%04X", $endAddress) . " " . $elementDataType . " " . $elementLength . " " . $elementSignedness . "\n";
				
				for (my $cnt = 0; $cnt < $lengthMult; $cnt++)
				{
					# increment the address to a multiple of 2 if the length (in bytes) of the element is divisible by 2
					if ((0 != ($endAddress % 2)) && (0 == ($elementLength % 2)))
					{
						$endAddress = $endAddress + 1;
					}
					
					$endAddress = $endAddress + $elementLength;
				}
			}
			# .. self-defined structure -> recursion for getting memory layout of sub structure //debug: what if there's an array of it ?
			elsif (0 == $structs{$elementDataType}{'atomic'})
			{
				# increment the address to a multiple of 2 (the compiler obviously aligns any struct to even addresses even if the first element's length is only one byte)
				if ((0 != ($endAddress % 2)) && (0 == ($elementLength % 2)))
				{
					$endAddress = $endAddress + 1;
				}
				
				print RESULT_FILE $hierarchyString . "." . $elementName . " 0x" . sprintf("%04X ", $endAddress) . "\n";
				$endAddress = getAbsoluteAddressesOfElements($elementDataType, $hierarchyString . "." . $elementName, $endAddress, @listFileContent);
				
				if ($lengthMult != 1)
				{
					errorHandler("ERROR! Handling of arrays of self-defined structures is not implemented yet!");
				}
				
				next;
			}
			else
			{
				errorHandler("ERROR! Unknown element encountered during computation of absolute addresses: " .  $elementDataType . " " . $elementName . "\n");
			}
		}
		elsif ($listFileContent[$i] =~ /^\s*#endif\s*/)
		{
			# nothing to do; this happens when the corresponding define is set
		}
		elsif ($listFileContent[$i] =~ /\}\s*$typedefString\s*;/)
		{
			last;
		}
		else
		{
			# rem @echo offwhat now ... ?
			errorHandler("ERROR! Can not parse this line of struct " . $typedefString . ": " . $listFileContent[$i] . "\n");
		}
		
		if ($i >= $#listFileContent)
		{
			errorHandler("ERROR! Couldn't get element addresses for struct " . $typedefString ."!");
		}
	}
	
	return $endAddress;
}

###############
# check whether a define is set (1) or not (0)
sub compilerDefineSet
{
	my ($defineName, @listFileContent) = @_;
	
	my $returnValue = 0;
	
	for (my $i = 0; $i <= $#listFileContent; $i++)
	{
		my $searchString = '^\s*#define\s+' . $defineName;
		
		if ($listFileContent[$i] =~ /$searchString/i)
		{
			# print "\n# Define set: " . $listFileContent[$i];
			$returnValue = 1;
			last;
		}
		elsif ($listFileContent[$i] =~ /^\s*\/.*#define\s+$defineName/i)
		{
			# print "\n# Define commented out: " . $listFileContent[$i];
			$returnValue = 0;
			last;			
		}
		
		if ($i == $#listFileContent) { errorHandler("ERROR! Define " . $defineName . " can not be found anywhere!"); }
	}
	
	return $returnValue;	
}

###############
# retrieve the (numerical) value of a compiler define
sub compilerDefineValue
{
	my ($defineName, @listFileContent) = @_;
	
	my $returnValue = "";
	
	# workaround for "sizeof()"; we just can not parse such stuff ...
	if ($defineName =~ /^sizeof$/)
	{
		return $returnValue;
	}
	
	for (my $i = 0; $i <= $#listFileContent; $i++)
	{
		my $searchString = '^\s*#define\s+' . $defineName;
		
		if ($listFileContent[$i] =~ /$searchString/i)
		{
			if ($listFileContent[$i] =~ /$defineName\s+\(*\s*(\S+)/i)
			{
				# print "\n# Define value: " . $listFileContent[$i] . " value=" . $1 . "\n";
				$returnValue = $1;
			}
			else
			{
				errorHandler("ERROR! Define " . $defineName . " has no usable value!");
			}
			
			last;
		}
		elsif ($listFileContent[$i] =~ /^\s*\/.*#define\s+$defineName/i)
		{
			errorHandler("ERROR! Define " . $defineName . " is commented out!");
			last;			
		}
		
		if ($i == $#listFileContent) { errorHandler("ERROR! Define " . $defineName . " can not be found anywhere!"); }
	}
	
	return $returnValue;
}

###############
# retrieve the name of the user configuration file from the value of define USR_CONFIGURATION
sub getUserCfgFile
{
	my ($usrCfgValue, @listFileContent) = @_;
	
	my $returnValue = "";
	
	for (my $i = 0; $i <= $#lines; $i++)
	{		
		if ($lines[$i] =~ /#[el]*if\s+\(\s+USR_CONFIGURATION\s+==\s+$usrCfgValue\s+\)/)
		{
			while ($lines[++$i] !~ /#[el]*if/)
			{
				if ($lines[$i] =~ /^\s*#include\s+<(\S+)>/)
				{
					$returnValue = $1;
					last;
				}
			}
		}
	}
	
	return $returnValue;
}

###############
# initialize hash %structs with all elements that should not be parsed (i.e. basic data types)
sub getAtomicDataTypes
{
	# provide known atomic structures
	$structs{'BOOL'}{'signedness'} = 0;
	$structs{'BOOL'}{'length'} = 1;
	$structs{'BOOL'}{'atomic'} = 1;
	
	$structs{'u8'}{'signedness'} = 0;
	$structs{'u8'}{'length'} = 1;
	$structs{'u8'}{'atomic'} = 1;

	$structs{'s8'}{'signedness'} = 1;
	$structs{'s8'}{'length'} = 1;
	$structs{'s8'}{'atomic'} = 1;

	$structs{'u16'}{'signedness'} = 0;
	$structs{'u16'}{'length'} = 2;
	$structs{'u16'}{'atomic'} = 1;

	$structs{'s16'}{'signedness'} = 1;
	$structs{'s16'}{'length'} = 2;
	$structs{'s16'}{'atomic'} = 1;
	
	$structs{'u32'}{'signedness'} = 0;
	$structs{'u32'}{'length'} = 4;
	$structs{'u32'}{'atomic'} = 1;

	$structs{'s32'}{'signedness'} = 1;
	$structs{'s32'}{'length'} = 4;
	$structs{'s32'}{'atomic'} = 1;
    
    $structs{'saradc_ctrl_list_entry_t *'}{'signedness'} = 0;
	$structs{'saradc_ctrl_list_entry_t *'}{'length'} = 2;
	$structs{'saradc_ctrl_list_entry_t *'}{'atomic'} = 1;

	# if one wants to ommit the parsing of a sub structure, just enter its values here and declare as atomic
	# $structs{'pi_controller_t'}{'signedness'} = 0;
	# $structs{'pi_controller_t'}{'length'} = 10;
	# $structs{'pi_controller_t'}{'atomic'} = 1;
}

sub showLogMessage
{
	my ($logMessage) = @_;
	
	# print $logMessage . "\n";
	print "Warning: " . $logMessage . "\n";
}
###############
# issue the error and stop script
sub errorHandler
{
	my ($errorMessage) = @_;
	
	# print $errorMessage . "\n";
	die $errorMessage . "\n";
}

1;