/***************************************************************************//**
 * @file			vic_VectorInterruptControl.c
 *
 * @creator		sbai
 * @created		08.05.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 * @misra{M3CM Rule-8.9. - PRQA Msg 3218,
 * The interface and callback functions struct are the main junction of a LIN driver module\,
 * so it declared and initialized as uppermost of the file as possible. Eventhough they are
 * only directly accessed once at every 'Initialization' function\, they are thenceforth still
 * accessed via a pointer in the environment data structs.,
 * Properly access to data you should not have.,
 * None}
 * _____
 *
 * @misra{M3CM Dir-1.2. and 11.1. - PRQA Msg 310,
 * The cast between a pointer-to-function or pointer-to-object and (another) pointer-to-object type
 * is dangerous\, but basically they are both pointers. This cast is necessary\, because some parameter
 * for the Elmos LIN Driver have to be determinable during runtime and/or via a concrete values.
 * I.e. the determination of some length values. To save memory\, callbacks to determine such a value
 * during runtime and a pointer to a variable share the same pointer and another parameter configures
 * what is behind this pointer.,
 * Can be dangerous, if the conditions aren't strictly checked.,
 * Alignment and NULL-pointer checks are performed and the functionality of this code
 * is precisely tested.}
 *
 ******************************************************************************/

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "el_helper.h"
#include "CommonInterruptHandler.h"

#include "vic_InterruptHandler.h"

#include <stdint.h>
#include <stddef.h>


/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ************************ MODULE GLOBALE VARIABLES **************************/
/* ****************************************************************************/
/* See justification at the file comment header */
// PRQA S 3218 ++
static vic_sInterruptEnvironmentData_t loc_VicEnvironmentData;
// PRQA S 3218 --

/* ****************************************************************************/
/* ******************** FORWARD DECLARATIONS / PROTOTYPES *********************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ************************** FUNCTION DEFINITIONS ****************************/
/* ****************************************************************************/

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
void InterruptHandlerInitialize(vic_cpInterfaceFunctions_t vicIf)
{
  if ((vicIf != NULL) && (vicIf->InterfaceVersion == VIC_INTERFACE_VERSION))
  {    
    /* Initialize environment data with 0 */
	// PRQA S 310 ++
    el_FillBytes(0u, (uint8_t*) &loc_VicEnvironmentData, sizeof(vic_sInterruptEnvironmentData_t));
    // PRQA S 310 --

    /* Init global IRQ handling */ 
    vicIf->IRQInitialisation(&loc_VicEnvironmentData);
  
    /* Enable all IRQs at VIC level */
    vicIf->EnableMain();
  }
  else { }
}

