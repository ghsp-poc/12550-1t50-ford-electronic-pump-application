/***************************************************************************//**
 * @file			spi_InterruptHandler.c
 *
 * @creator		rpy
 * @created		26.01.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 * @misra{M3CM Dir-8.9. - PRQA Msg 3218,
 * This arrays are register lookup tables for the different SPI modules.
 * Even though they are defined at file scope and only used in one function\,
 * it is more useful to define them at file scope.,
 * None,
 * None}
 *
 ******************************************************************************/

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "io_e52398a.h"
#include <intrinsics.h>

#include "vic_InterruptHandler.h"
#include "spi_InterruptHandler.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************** FORWARD DECLARATIONS / PROTOTYPES *********************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ************************ MODULE GLOBALE VARIABLES **************************/
/* ****************************************************************************/
/* See justification at the file comment header */
// PRQA S 3218 ++
static volatile unsigned short * const loc_SPI_x_IRQ_VENABLE[spi_INSTANCE_CNT]  = 
{
  &SPI_0_IRQ_VENABLE,
  &SPI_1_IRQ_VENABLE
};
// PRQA S 3218 --

static volatile unsigned short * const loc_SPI_x_IRQ_VDISABLE[spi_INSTANCE_CNT]  = 
{
  &SPI_0_IRQ_VDISABLE,
  &SPI_1_IRQ_VDISABLE
};

/* See justification at the file comment header */
// PRQA S 3218 ++
static volatile unsigned short * const loc_SPI_x_IRQ_VMAX[spi_INSTANCE_CNT]  = 
{
  &SPI_0_IRQ_VMAX,
  &SPI_1_IRQ_VMAX
};
// PRQA S 3218 --

/* See justification at the file comment header */
// PRQA S 3218 ++
static volatile unsigned short * const loc_SPI_x_IRQ_VNO[spi_INSTANCE_CNT]  = 
{
  &SPI_0_IRQ_VNO,
  &SPI_1_IRQ_VNO
};
// PRQA S 3218 --

static const vic_eInterruptVectorNum_t loc_SPI_VIC_VNO[spi_INSTANCE_CNT] = 
{
  vic_IRQ_SPI_0,
  vic_IRQ_SPI_1
};

/* See justification at the file comment header */
// PRQA S 3218 ++
static const vic_InterruptCallback_t loc_SPI_InterruptHandler[spi_INSTANCE_CNT] = 
{
  spi_InterruptHandler0,
  spi_InterruptHandler1
};
// PRQA S 3218 --

/* ****************************************************************************/
/* ************************** FUNCTION DEFINITIONS ****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************************* INTERRUPTS *********************************/
/* ****************************************************************************/

/***************************************************************************//**
 * @implementation
 * Simple word write access to IRQ_VENABLE register, but with the difference
 * to spi_SetIRQVEnable, that a special enum (spi_eInterruptVectorNum_t)
 * is passed as parameter, for security reasons and a more concrete interface
 * definition.
 *
 ******************************************************************************/
void spi_InterruptEnable(spi_InstanceNum_t instanceNum, spi_eInterruptVectorNum_t irqsrc)
{
  if (instanceNum < spi_INSTANCE_CNT)
  {
    *(loc_SPI_x_IRQ_VENABLE[instanceNum]) = (uint16_t) irqsrc;
  }
  else {}
}

/***************************************************************************//**
 * @implementation
 * Simple word write access to IRQ_VDISABLE register, but with the difference
 * to spi_SetIRQVDisable, that a special enum (spi_eInterruptVectorNum_t)
 * is passed as parameter, for security reasons and a more concrete interface
 * definition.
 *
 ******************************************************************************/
void spi_InterruptDisable(spi_InstanceNum_t instanceNum, spi_eInterruptVectorNum_t irqsrc)
{
  if (instanceNum < spi_INSTANCE_CNT)
  {
    *(loc_SPI_x_IRQ_VDISABLE[instanceNum]) = (uint16_t) irqsrc;
  }
  else {}
}

/***************************************************************************//**
 * @implementation
 * At first the module number is looked up in a table, because module number
 * and module instance base address have a logical connection. Then the
 * interrupt callback function pointer is saved in module interrupt vector
 * table.
 *
 ******************************************************************************/
void spi_InterruptRegisterCallback(spi_InstanceNum_t instanceNum, spi_eInterruptVectorNum_t irqvecnum, spi_InterruptCallback_t cbfnc)
{
  if (instanceNum < spi_INSTANCE_CNT)
  {
    spi_sInterruptEnvironmentData_t* evironmentData = (spi_sInterruptEnvironmentData_t*) vic_GetPointerToEnvironmentData(loc_SPI_VIC_VNO[instanceNum]);

    if ((irqvecnum < spi_INTERRUPT_VECTOR_CNT) && (evironmentData != NULL))
    {
      evironmentData->InterrupVectorTable[irqvecnum] = cbfnc;
    }
    else {}
  }
  else {}
}

/***************************************************************************//**
 * @implementation
 * Reverse function of "spi_RegisterInterruptCallback". Sets the corresponding
 * entry in the module interrupt vector table back to NULL. Very imported,for
 * security reasons the corresponding interrupt is disabled. Otherwise the
 * program could possibly call NULL.
 *
 ******************************************************************************/
void spi_InterruptDeregisterCallback(spi_InstanceNum_t instanceNum, spi_eInterruptVectorNum_t irqvecnum)
{
  if (instanceNum < spi_INSTANCE_CNT)
  {
    spi_sInterruptEnvironmentData_t* evironmentData = (spi_sInterruptEnvironmentData_t*) vic_GetPointerToEnvironmentData(loc_SPI_VIC_VNO[instanceNum]);

    if ((irqvecnum < spi_INTERRUPT_VECTOR_CNT) && (evironmentData != NULL))
    {
      spi_InterruptDisable(instanceNum,irqvecnum);
      evironmentData->InterrupVectorTable[irqvecnum] = NULL;
    }
    else {}
  }
  else {}
}

/***************************************************************************//**
 * @implementation
 * Every hardware SPI module has it's own interrupt handler, also because each
 * module has it's own interrupt vector. So the module base address is fixed.
 * The interrupt vector table is provided by the user-application from RAM. The
 * module specific vector tables lie behind the VIC interrupt vector table, which
 * address is saved in the "TABLE_BASE". The module interrupt handler
 * gets the address from the function "vic_GetPointerToEviornmentData".
 * The interrupt vector number is saved in a local variable, because the value
 * of IRQ_VECTOR could possible change during processing of
 * spi_SPI0_InterruptHandler. Next the registered interrupt callback function
 * (spi_RegisterInterruptCallback) is copied into a local function pointer,
 * checked if it's not NULL and at least called. At the end the interrupt
 * request flag is cleared, by writing back the interrupt vector number to
 * IRQ_VNO register.
 *
 ******************************************************************************/
static void loc_InterruptHandler(spi_InstanceNum_t instanceNum)
{
  spi_eInterruptVectorNum_t irqvecnum = (spi_eInterruptVectorNum_t) (*(loc_SPI_x_IRQ_VNO[instanceNum]));
  
  if (irqvecnum < spi_INTERRUPT_VECTOR_CNT) /* ensure the IRQ trigger is not already gone away until processing starts */
  {
    spi_sInterruptEnvironmentData_t* evironmentData = (spi_sInterruptEnvironmentData_t*) vic_GetPointerToEnvironmentData(loc_SPI_VIC_VNO[instanceNum]);

    spi_InterruptCallback_t fptr;
  
    uint16_t vicVmaxBackup = 0;
    uint16_t spiVmaxBackup = 0;
  
    /* IRQ nesting on VIC level: enabled by default */
    vicVmaxBackup = VIC_IRQ_VMAX;
    VIC_IRQ_VMAX = (uint16_t) loc_SPI_VIC_VNO[instanceNum];

    /* IRQ nesting on module level: by default disabled, handler may override this later on */
    spiVmaxBackup = *(loc_SPI_x_IRQ_VMAX[instanceNum]);
    *(loc_SPI_x_IRQ_VMAX[instanceNum]) = 0;
  
    fptr = evironmentData->InterrupVectorTable[irqvecnum];
          
    if(fptr != NULL)
    {          
      __enable_interrupt();
        
      /* Call interrupt callback function */
      fptr(irqvecnum,  (void*) evironmentData->ContextData);      
      
      __disable_interrupt();
    }
    else 
    {
      /* if there is no handler function for a particular IRQ, disable this IRQ  */ 
      *(loc_SPI_x_IRQ_VDISABLE[instanceNum]) = (uint16_t) irqvecnum;
    }

    /* Clear interrupt request flag */
    *(loc_SPI_x_IRQ_VNO[instanceNum]) = (uint16_t) irqvecnum;
  
    /* IRQ nesting on module level */
    *(loc_SPI_x_IRQ_VMAX[instanceNum]) = spiVmaxBackup;
  
    /* IRQ nesting on VIC level */
    VIC_IRQ_VMAX = vicVmaxBackup;
  }
  else {}  
} 

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
__interrupt void spi_InterruptHandler0(void)
{
  loc_InterruptHandler(spi_MOD_0);
}

__interrupt void spi_InterruptHandler1(void)
{
  loc_InterruptHandler(spi_MOD_1);
}

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
void spi_InterruptInitialisation(spi_InstanceNum_t instanceNum, vic_cpInterfaceFunctions_t vicIf, spi_pInterruptEnvironmentData_t environmentdata, spi_pInterruptContextData_t contextdata)
{
  if ( (vicIf != NULL) &&
       (vicIf->InterfaceVersion == VIC_INTERFACE_VERSION) &&
       (environmentdata != NULL) &&
       (instanceNum < spi_INSTANCE_CNT) )
  {
    environmentdata->ContextData = contextdata;

    /* Register module at interrupt handler */
    vicIf->RegisterModule(loc_SPI_VIC_VNO[instanceNum], loc_SPI_InterruptHandler[instanceNum], environmentdata);
    vicIf->EnableModule(loc_SPI_VIC_VNO[instanceNum]);
  }
  else {}
}
