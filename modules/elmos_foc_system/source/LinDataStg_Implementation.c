/***************************************************************************//**
 * @file			LinDataStg_Implementation.c
 *
 * @creator		sbai
 * @created		27.11.2015
 *
 * @brief  		TODO: Short description of this module
 *
 * $Id: LinDataStg_Implementation.c 1794 2017-01-30 10:47:17Z sbai $
 *
 * $Revision: 1794 $
 *
 * @misra{M3CM Dir-2.7. - PRQA Msg 3206,
 * The Elmos LIN Driver defines interfaces for every module of it. This interfaces consist
 * of typedefinitions of function pointers combined in struct. This struct has to be 
 * implemented by every implementation of every module. Because of this matter of fact\,
 * there can be different implementations for the same module and for the same reason one
 * implementation use and/or write to a parameter defined in the function pointer typedefinition
 * and another one not. So the parameter of the interface function are binding\, but must not
 * be used and/or not written to them.,
 * Unnecessary code.,
 * None}
 * _____
 ******************************************************************************/

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "LinDataStg_Implementation.h"
#include "LinDataStg_Interface.h"
#include "el_helper.h"
#include "LinDataStg_DataValueIDs.h"
//#include "eeprom_access.h"
#include "LinDrvImp_Config.h"
#include <stdint.h>
#include <stdbool.h>

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/
#if LINDRVIMP_AQR_MOD_SZE == 1
#define DATASTG_LAYER_CODE _Pragma("location=\"DATASTG_LAYER_CODE\"")
#define DATASTG_LAYER_DATA _Pragma("location=\"DATASTG_LAYER_DATA\"")
#else
#define DATASTG_LAYER_CODE
#define DATASTG_LAYER_DATA
#endif

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/***************************************************************************//**
 *
 ******************************************************************************/
struct loc_sDataEntry
{
    Lin_uint16_t DataValueId;
    Lin_uint16_t ValidToken;
    Lin_uint16_t PaddedDataLen;
    Lin_uint16_t DataLen;
    Lin_uint8_t  Data[];
};

typedef struct loc_sDataEntry loc_sDataEntry_t;
typedef loc_sDataEntry_t *    loc_pDataEntry_t;


/***************************************************************************//**
 *
 ******************************************************************************/
struct loc_sDataStgEnvironmentData
{
    LinDataStgIf_cpCallbackFunctions_t Callbacks;       /**< TODO: Concrete description. */
    loc_pDataEntry_t                   DataEntryPtr;    /**< TODO: Concrete description. */
    LinDataStgIf_sThis_t               FallbackDataStg; /**< TODO: Concrete description. */
};

typedef struct loc_sDataStgEnvironmentData    loc_sDataStgEnvironmentData_t;  /**<< TODO: Concrete description. */
typedef        loc_sDataStgEnvironmentData_t* loc_pDataStgEnvironmentData_t;  /**<< TODO: Concrete description. */

/***************************************************************************//**
 *
 ******************************************************************************/
enum loc_eMemType
{
  loc_MT_Invalid = 0,
  loc_MT_ROM,
  loc_MT_Flash,
  loc_MT_RAM,
  loc_MT_EEPROM,
  loc_MT_Other,
};

typedef enum loc_eMemType loc_eMemType_t;

/***************************************************************************//**
 *
 ******************************************************************************/
struct loc_sDataValIdTbl
{
    LinDataStgImp_DataValueID_t DataValueId;
    loc_eMemType_t              MemType;
    Lin_pvoid_t                 Adr;
    Lin_BufLength_t                Len;
};

typedef struct loc_sDataValIdTbl loc_sDataValIdTbl_t;

/***************************************************************************//**
 *
 ******************************************************************************/

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/
#if LINDATASTGIMP_EXT_IFFUN_STRCT_ACCESS == 1
DATASTG_LAYER_DATA const LinDataStgIf_sInterfaceFunctions_t LinDataStgImp_InterfaceFunctions =
#else
DATASTG_LAYER_DATA static const LinDataStgIf_sInterfaceFunctions_t LinDataStgImp_InterfaceFunctions =
#endif
{
  .InterfaceVersion  = LINDATASTG_INTERFACE_MODULE_API_VERSION,

  .Initialization  = &LinDataStgImp_Initialization,
  .GetSubInterface = &LinDataStgImp_GetSubInterface,
  .GetDataValue    = &LinDataStgImp_GetDataValue,
  .SetDataValue    = &LinDataStgImp_SetDataValue
};

/* ****************************************************************************/
/* ************************ MODULE GLOBALE VARIABLES **************************/
/* ****************************************************************************/
DATASTG_LAYER_DATA static const loc_sDataValIdTbl_t loc_DataValIdTbl[] =
{
  {.DataValueId = LinDataStgImp_DVID_ConfigCookie,            .MemType = loc_MT_Flash, .Adr = (Lin_uint32_t*) &(((LinDrvImp_cpFLASHCfgData_t) LINDRVIMP_CONFIG_LOCATION)->ConfigCookie),	     .Len = sizeof(((LinDrvImp_cpFLASHCfgData_t) LINDRVIMP_CONFIG_LOCATION)->ConfigCookie)},
  {.DataValueId = LinDataStgIf_DVID_SerialNumber,             .MemType = loc_MT_Flash, .Adr = (Lin_uint32_t*) &(((LinDrvImp_cpFLASHCfgData_t) LINDRVIMP_CONFIG_LOCATION)->SerialNumber),             .Len = sizeof(((LinDrvImp_cpFLASHCfgData_t) LINDRVIMP_CONFIG_LOCATION)->SerialNumber)},
  {.DataValueId = LinDataStgIf_DVID_ProductIdentification,    .MemType = loc_MT_Flash, .Adr = (Lin_uint32_t*) &(((LinDrvImp_cpFLASHCfgData_t) LINDRVIMP_CONFIG_LOCATION)->ProductIdentification),    .Len = sizeof(((LinDrvImp_cpFLASHCfgData_t) LINDRVIMP_CONFIG_LOCATION)->ProductIdentification)},
  {.DataValueId = LinDataStgIf_DVID_NAD,                      .MemType = loc_MT_Flash, .Adr = (Lin_uint32_t*) &(((LinDrvImp_cpFLASHCfgData_t) LINDRVIMP_CONFIG_LOCATION)->InitialNAD),               .Len = sizeof(((LinDrvImp_cpFLASHCfgData_t) LINDRVIMP_CONFIG_LOCATION)->InitialNAD)},
  {.DataValueId = LinDataStgImp_DVID_ADCSampleExt,            .MemType = loc_MT_Flash, .Adr = (Lin_uint32_t*) &(((LinDrvImp_cpFLASHCfgData_t) LINDRVIMP_CONFIG_LOCATION)->ADCSampleExt),             .Len = sizeof(((LinDrvImp_cpFLASHCfgData_t) LINDRVIMP_CONFIG_LOCATION)->ADCSampleExt)},
  {.DataValueId = LinDataStgImp_DVID_ADCSampleExtAA,          .MemType = loc_MT_Flash, .Adr = (Lin_uint32_t*) &(((LinDrvImp_cpFLASHCfgData_t) LINDRVIMP_CONFIG_LOCATION)->ADCSampleExtAA),           .Len = sizeof(((LinDrvImp_cpFLASHCfgData_t) LINDRVIMP_CONFIG_LOCATION)->ADCSampleExtAA)},
  {.DataValueId = LinDataStgImp_DVID_ADCSampleExtVT,          .MemType = loc_MT_Flash, .Adr = (Lin_uint32_t*) &(((LinDrvImp_cpFLASHCfgData_t) LINDRVIMP_CONFIG_LOCATION)->ADCSampleExtVT),           .Len = sizeof(((LinDrvImp_cpFLASHCfgData_t) LINDRVIMP_CONFIG_LOCATION)->ADCSampleExtVT)},
  {.DataValueId = LinDataStgImp_DVID_EnableHighSpeed,         .MemType = loc_MT_Flash, .Adr = (Lin_uint32_t*) &(((LinDrvImp_cpFLASHCfgData_t) LINDRVIMP_CONFIG_LOCATION)->EnableHighSpeed),          .Len = sizeof(((LinDrvImp_cpFLASHCfgData_t) LINDRVIMP_CONFIG_LOCATION)->EnableHighSpeed)},
  {.DataValueId = LinDataStgImp_DVID_DebouncerValue,          .MemType = loc_MT_Flash, .Adr = (Lin_uint32_t*) &(((LinDrvImp_cpFLASHCfgData_t) LINDRVIMP_CONFIG_LOCATION)->DebouncerValue),           .Len = sizeof(((LinDrvImp_cpFLASHCfgData_t) LINDRVIMP_CONFIG_LOCATION)->DebouncerValue)},
  {.DataValueId = LinDataStgImp_DVID_PreInitHook,             .MemType = loc_MT_Flash, .Adr = (Lin_uint32_t*) &(((LinDrvImp_cpFLASHCfgData_t) LINDRVIMP_CONFIG_LOCATION)->PreInitHook),              .Len = sizeof(((LinDrvImp_cpFLASHCfgData_t) LINDRVIMP_CONFIG_LOCATION)->PreInitHook)},
  {.DataValueId = LinDataStgImp_DVID_PostInitHook,            .MemType = loc_MT_Flash, .Adr = (Lin_uint32_t*) &(((LinDrvImp_cpFLASHCfgData_t) LINDRVIMP_CONFIG_LOCATION)->PostInitHook),             .Len = sizeof(((LinDrvImp_cpFLASHCfgData_t) LINDRVIMP_CONFIG_LOCATION)->PostInitHook)},
  {.DataValueId = LinDataStgImp_DVID_TaskHook,                .MemType = loc_MT_Flash, .Adr = (Lin_uint32_t*) &(((LinDrvImp_cpFLASHCfgData_t) LINDRVIMP_CONFIG_LOCATION)->TaskHook),                 .Len = sizeof(((LinDrvImp_cpFLASHCfgData_t) LINDRVIMP_CONFIG_LOCATION)->TaskHook)},
  {.DataValueId = LinDataStgImp_DVID_InitialBaudrate,         .MemType = loc_MT_Flash, .Adr = (Lin_uint32_t*) &(((LinDrvImp_cpFLASHCfgData_t) LINDRVIMP_CONFIG_LOCATION)->InitialBaudrate),          .Len = sizeof(((LinDrvImp_cpFLASHCfgData_t) LINDRVIMP_CONFIG_LOCATION)->InitialBaudrate)},
  {.DataValueId = LinDataStgImp_DVID_EECfgData,               .MemType = loc_MT_Flash, .Adr = (Lin_uint32_t*) LINDRVIMP_CONFIG_LOCATION, .Len = sizeof(LinDrvImp_FLASHCfgData_t)},
  {.DataValueId = LinDataStgIf_DVID_INVALID},
};

/* ****************************************************************************/
/* ******************** FORWARD DECLARATIONS / PROTOTYPES *********************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ************************** FUNCTION DEFINITIONS ****************************/
/* ****************************************************************************/

/***************************************************************************//**
 *
 ******************************************************************************/
/* See justification at the file comment header */
// PRQA S 3206 ++
DATASTG_LAYER_CODE Lin_Bool_t LinDataStgImp_Initialization(LinDataStgIf_pGenericEnvData_t     genericDataStgEnvData, LinDataStgIf_EnvDataSze_t        dataStgEnvDataSze,
                                                           LinDataStgIf_cpCallbackFunctions_t dataStgCbFuns,         LinDataStgIf_pGenericCbCtxData_t genericdataStgCbCtxData,
                                                           LinDataStgIf_pGenericImpCfgData_t  genericImpCfgData)
// PRQA S 3206 --
{
  Lin_Bool_t retVal = LIN_FALSE;

  if ((genericDataStgEnvData != LIN_NULL) &&
      (dataStgEnvDataSze >= sizeof(loc_sDataStgEnvironmentData_t)) &&
      (dataStgCbFuns != LIN_NULL) &&
      (dataStgCbFuns->CallbackVersion == LINDATASTG_INTERFACE_MODULE_API_VERSION))
  {
    loc_pDataStgEnvironmentData_t dataStgEnvData = (loc_pDataStgEnvironmentData_t)  genericDataStgEnvData;

    /* Initialize LIN Data Storage driver environment data with 0 */
    el_FillBytes(0x00, (uint8_t*) dataStgEnvData, sizeof(loc_sDataStgEnvironmentData_t));

    dataStgEnvData->Callbacks = dataStgCbFuns;

    /* Register fallback data storage if present */
    if(genericImpCfgData != LIN_NULL)
    {
      LinDataStgImp_pCfgData_t impCfgData = (LinDataStgImp_pCfgData_t) genericImpCfgData;

      dataStgEnvData->FallbackDataStg.EnvData = impCfgData->FallbackDataStorage.EnvData;
      dataStgEnvData->FallbackDataStg.IfFunsTbl = impCfgData->FallbackDataStorage.IfFunsTbl;
    }
    
    retVal = LIN_TRUE;
  }

  return(retVal);
}

/***************************************************************************//**
 *
 ******************************************************************************/
/* See justification at the file comment header */
// PRQA S 3206 ++
DATASTG_LAYER_CODE Lin_Bool_t LinDataStgImp_GetSubInterface(LinDataStgIf_pGenericEnvData_t genericDataStgEnvData, Lin_eInterfaceIds_t interfaceId,  Lin_pThis_t ifThisPtr)
/* See justification at the file comment header */
// PRQA S 3206 --
{
  return(LIN_FALSE);
}

/***************************************************************************//**
 *
 ******************************************************************************/
/* See justification at the file comment header */
// PRQA S 3206 ++
DATASTG_LAYER_CODE LinDataStgIf_eErrorCodes_t LinDataStgImp_GetDataValue(LinDataStgIf_pGenericEnvData_t genericDataStgEnvData, LinDataStgIf_DataValueID_t dataValueId,
                                                                         LinDataStgIf_pData_t           buffer,                LinDataStgIf_Length_t      bufferLen,
                                                                         LinDataStgIf_pLength_t         pDataValLen)
/* See justification at the file comment header */
// PRQA S 3206 --
{
  LinDataStgIf_eErrorCodes_t retVal = LinDataStgIf_ERR_UNKOWN_DATA_VALUE_ID;

  if (genericDataStgEnvData != LIN_NULL)
  {
    loc_pDataStgEnvironmentData_t dataStgEnvData = (loc_pDataStgEnvironmentData_t) genericDataStgEnvData;
    
    uint16_t i = 0;

    while(loc_DataValIdTbl[i].DataValueId != LinDataStgIf_DVID_INVALID)
    {
      if(loc_DataValIdTbl[i].DataValueId == dataValueId)
      {
        switch(loc_DataValIdTbl[i].MemType)
        {
          case loc_MT_ROM:
          case loc_MT_Flash:
          {
            if(bufferLen >= loc_DataValIdTbl[i].Len)
            {
              el_CopyBytes((uint8_t *) loc_DataValIdTbl[i].Adr, buffer, loc_DataValIdTbl[i].Len);
              retVal = LinDataStgIf_ERR_NO_ERROR;
            }
            else
            {
              retVal = LinDataStgIf_ERR_DATA_VALUE_LEN_MISMATCH;
            }
          }
          case loc_MT_RAM:
          case loc_MT_Other:
          {

          }
          break;

          case loc_MT_EEPROM:
          {
            if(bufferLen >= loc_DataValIdTbl[i].Len)
            {
              el_CopyBytes((uint8_t *) loc_DataValIdTbl[i].Adr, buffer, loc_DataValIdTbl[i].Len);
              retVal = LinDataStgIf_ERR_NO_ERROR;
            }
            else
            {
              retVal = LinDataStgIf_ERR_DATA_VALUE_LEN_MISMATCH;
            }
          }
          break;

          default:
          case loc_MT_Invalid:
          {
            retVal = LinDataStgIf_ERR_DATA_VAL_DEF_MISS;
          }
          break;
        }
      }
      else{}
      
      i++;
    }
    
    if(retVal == LinDataStgIf_ERR_UNKOWN_DATA_VALUE_ID)
    {
      if((dataStgEnvData->FallbackDataStg.IfFunsTbl != LIN_NULL) && (dataStgEnvData->FallbackDataStg.EnvData != LIN_NULL))
      {
        retVal = dataStgEnvData->FallbackDataStg.IfFunsTbl->GetDataValue(dataStgEnvData->FallbackDataStg.EnvData, dataValueId,
                                                                         buffer, bufferLen, pDataValLen);
      }
      else
      {
        retVal = LinDataStgIf_ERR_DATA_VALUE_ACCESS_FAILED;
      }
    }
  }
  else{}

  return(retVal);
}

/***************************************************************************//**
 *
 ******************************************************************************/
/* See justification at the file comment header */
// PRQA S 3206 ++
LinDataStgIf_eErrorCodes_t LinDataStgImp_SetDataValue(LinDataStgIf_pGenericEnvData_t genericDataStgEnvData, LinDataStgIf_DataValueID_t dataValueId,
                                                      LinDataStgIf_pData_t           buffer,                LinDataStgIf_Length_t      bufferLen)
// PRQA S 3206 --
{
  LinDataStgIf_eErrorCodes_t retVal = LinDataStgIf_ERR_UNKOWN_DATA_VALUE_ID;

  if (genericDataStgEnvData != LIN_NULL)
  {
    uint16_t i = 0;

    while(loc_DataValIdTbl[i].DataValueId != LinDataStgIf_DVID_INVALID)
    {
      if(loc_DataValIdTbl[i].DataValueId == dataValueId)
      {
        switch(loc_DataValIdTbl[i].MemType)
        {
          case loc_MT_Flash:
          {
            retVal = LinDataStgIf_ERR_READ_ONLY;
          }
          break;
          case loc_MT_ROM:
          case loc_MT_RAM:
          case loc_MT_Other:
          {

          }
          break;

          case loc_MT_EEPROM:
          {
            uint8_t lockBits = 0xFFu;
            if(loc_DataValIdTbl[i].Len % 16 == 0)
            {
              lockBits = (lockBits >> ((sizeof(lockBits) * 8) - (loc_DataValIdTbl[i].Len / 16))) << (((Lin_uintptr_t) loc_DataValIdTbl[i].Adr - 0x700) / 16);
            }
            else
            {
              lockBits = (lockBits >> ((sizeof(lockBits) * 8) - ((loc_DataValIdTbl[i].Len / 16) + 1))) << (((Lin_uintptr_t) loc_DataValIdTbl[i].Adr - 0x700) / 16);
            }
            
						if(1)
            //if(eeprom_Write(EE_PASSWORD, buffer, (uint16_t *) loc_DataValIdTbl[i].Adr, loc_DataValIdTbl[i].Len, (~lockBits), 0xFF) != TRUE)
            //if(eeprom_Write(EE_PASSWORD, buffer, (uint16_t *) loc_DataValIdTbl[i].Adr, loc_DataValIdTbl[i].Len, 0x00, 0x00) != TRUE)
            {
              retVal = LinDataStgIf_ERR_DATA_VALUE_ACCESS_FAILED;
            }
            else
            {
              retVal = LinDataStgIf_ERR_NO_ERROR;
            }
          }
          break;

          default:
          case loc_MT_Invalid:
          {
            retVal = LinDataStgIf_ERR_DATA_VAL_DEF_MISS;
          }
          break;
        }
      }
      else{}

      i++;
    }
  }
  else{}

  return(retVal);
}
