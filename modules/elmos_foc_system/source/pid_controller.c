/**
 * @ingroup PI
 *
 * @{
 */

/**********************************************************************/
/*! @file pid_controller.c
 * @brief standard PID control algorithm
 *
 * @details by now, the PID controller has been thrown out and
 * only the PI controller for a S16-value is implemented<br>
 * module prefix: pid_
 *
 * @author       JBER
 */
/**********************************************************************/
/* Demo Code Usage Restrictions:
 * Elmos Semiconductor AG provides this source code file simply and solely for IC evaluation purposes in laboratory and
 * this file must not be used for other purposes or within non laboratory environments. Especially, the use or the integration in
 * production systems, appliances or other installations is prohibited.
 *
 * Disclaimer:
 * Elmos Semiconductor AG shall not be liable for any damages arising out of defects resulting from (1) delivered hardware or software,
 * (2) non observance of instructions contained in this document, or (3) misuse, abuse, use under abnormal conditions or alteration by anyone
 * other than Elmos Semiconductor AG. To the extend permitted by law Elmos Semiconductor AG hereby expressively disclaims and user expressively
 * waives any and all warranties of merchantability and of fitness for a particular purpose, statutory warranty of non-infringement and any other
 * warranty or product liability that may arise by reason of usage of trade, custom or course of dealing.
 */
/**********************************************************************/

#include <pid_controller.h>
#include <math_utils.h>
#include <debug.h>

/*! @brief initialize PI controller structure
 *
 * @details
 * the structure of type pi_controller_t must be provided from the caller. All
 * element variables are set to their init values and the controller is parameterized via Kp and Ki
 *
 * @param     int16_t Kp P-amplification (scaled by 2^PI_CONTROLLER_KI_KP_SCALE_BITS), uint16_t Ki I-amplification (scaled by 2^PI_CONTROLLER_KI_KP_SCALE_BITS)
 * @return
 */
void pi_controller_init( pi_controller_t * controller, const int16_t Kp, const uint16_t Ki )
{
  controller->Kp        = Kp;
  controller->Ki        = Ki;
  controller->set_value = (int16_t) 0;
  controller->err_sum   = (int32_t) 0;

  controller->saturation_recovery_ratio = PI_CONTROLLER_SATURATION_RECOVER_RATIO_INIT;
}

/*! @brief execute PI controller
 * @param     int16_t target (setpoint) value, int16_t current value
 */
INLINE void pi_controller_exec_unsafe ( pi_controller_t * controller, const int16_t target_value, const int16_t current_value )
{
  #if ( DBG_PIN3_FUNCTION == DBG_PI_EXEC_DURATION )
    dbg_pin3_set();
  #endif

  int16_t loc_error   = target_value - current_value;
  int32_t loc_err_sum = controller->err_sum + (int32_t) loc_error;

  if( loc_err_sum < -PI_CONTROLLER_MAX_ERR_SUM )
  {
    loc_err_sum = -PI_CONTROLLER_MAX_ERR_SUM;
    #if ( DBG_PIN3_FUNCTION == DBG_PI_SHOW_OVERFLOW )
      dbg_pin3_set();
      dbg_pin3_clear();
    #endif
  }
  else if( loc_err_sum > PI_CONTROLLER_MAX_ERR_SUM )
  {
    loc_err_sum = PI_CONTROLLER_MAX_ERR_SUM;
    #if ( DBG_PIN3_FUNCTION == DBG_PI_SHOW_OVERFLOW )
      dbg_pin3_set();
      dbg_pin3_clear();
    #endif
  }
  else
  {
/*Message 2013*/
  }
  math_mulU16_S32_unsafe( controller->Ki, loc_err_sum );
  math_mulS16_acc_unsafe( loc_error, controller->Kp );

  #if ( DBG_PIN3_FUNCTION == DBG_PI_SHOW_OVERFLOW )
    int16_t res_hi = READ_REG_S16( MATH_H430MUL_REG_ADDR_RESHI );
    if(( res_hi < -128 ) || ( res_hi > 127 ))
    {
      dbg_pin3_set();
      dbg_pin3_clear();
    }
  #endif

  math_mul_shiftright_result( PI_CONTROLLER_KI_KP_SCALE_BITS );

  controller->set_value = (int16_t) math_mul_get_result( False );

  controller->err_sum = loc_err_sum;

  #if ( DBG_PIN3_FUNCTION == DBG_PI_EXEC_DURATION )
    dbg_pin3_clear();
  #endif
}

INLINE void pi_controller_exec_w_saturation_unsafe ( pi_controller_t * controller, const int16_t target_value, const int16_t current_value, const int16_t output_max, const int16_t output_min )
{
  #if ( DBG_PIN3_FUNCTION == DBG_PI_EXEC_DURATION )
    dbg_pin3_set();
  #endif

  int16_t loc_error   = target_value - current_value;
  int32_t loc_err_sum = controller->err_sum + (int32_t) loc_error;

  if( loc_err_sum < -PI_CONTROLLER_MAX_ERR_SUM )
  {
    loc_err_sum = -PI_CONTROLLER_MAX_ERR_SUM;
    #if ( DBG_PIN3_FUNCTION == DBG_PI_SHOW_OVERFLOW )
      dbg_pin3_set();
      dbg_pin3_clear();
    #endif
  }
  else if( loc_err_sum > PI_CONTROLLER_MAX_ERR_SUM )
  {
    loc_err_sum = PI_CONTROLLER_MAX_ERR_SUM;
    #if ( DBG_PIN3_FUNCTION == DBG_PI_SHOW_OVERFLOW )
      dbg_pin3_set();
      dbg_pin3_clear();
    #endif
  }
  else
  {
/*Message 2013*/
  }

  /* compute the actual PI output */
  math_mulU16_S32_unsafe( controller->Ki, loc_err_sum );
  math_mulS16_acc_unsafe( loc_error, controller->Kp );

  #if ( DBG_PIN3_FUNCTION == DBG_PI_SHOW_OVERFLOW )
    int16_t res_hi = READ_REG_S16( MATH_H430MUL_REG_ADDR_RESHI );
    if(( res_hi < -128 ) || ( res_hi > 127 ))
    {
      dbg_pin3_set();
      dbg_pin3_clear();
    }
  #endif

  math_mul_shiftright_result( PI_CONTROLLER_KI_KP_SCALE_BITS );

  controller->set_value = (int16_t) math_mul_get_result( False );

  /* check for saturation of the PI output */
  #if ( PI_CONTROLLER_SATURATION_TYPE == PI_CONTROLLER_SATURATION_COMPUTATIONAL )

    if( controller->set_value > output_max )
    {
      math_mulS16_unsafe( controller->set_value - output_max, controller->saturation_recovery_ratio );
      controller->err_sum -= math_mul_get_result( True );

      controller->set_value = output_max;
    }
    else if( controller->set_value < output_min )
    {
      math_mulS16_unsafe( controller->set_value - output_min, controller->saturation_recovery_ratio );
      controller->err_sum -= math_mul_get_result( True );

      controller->set_value = output_min;
    }
    else
    {
      controller->err_sum = loc_err_sum;
    }

  #elif ( PI_CONTROLLER_SATURATION_TYPE == PI_CONTROLLER_SATURATION_INCREMENTAL )

    if( controller->set_value > output_max )
    {
      controller->set_value = output_max;

      if( loc_error > 0 )
      {
        /* in this branch, we are in the saturation and the error indicates that, without saturation, the controller output should indeed increase beyond 'output_max' */
        controller->err_sum = controller->err_sum - ( controller->err_sum >> 8u );
      }
      else
      {
        /* in this branch, we are in the saturation although the error indicates that we should get out of it -> do that swiftly :) */
        controller->err_sum = loc_err_sum + ((int32_t) loc_error << 1u );
      }
    }
    else if( controller->set_value < output_min )
    {
      controller->set_value = output_min;

      if( loc_error < 0 )
      {
        /* in this branch, we are in the saturation and the error indicates that, without saturation, the controller output should indeed increase beyond 'output_min' */
        controller->err_sum = controller->err_sum - ( controller->err_sum >> 8u );
      }
      else
      {
        /* in this branch, we are in the saturation although the error indicates that we should get out of it -> do that swiftly :) */
        controller->err_sum = loc_err_sum + ((int32_t) loc_error << 1u );
      }
    }
    else
    {
      controller->err_sum = loc_err_sum;
    }

  #else
  #error "no saturation type has been set for the PI controller!"
  #endif

  #if ( DBG_PIN3_FUNCTION == DBG_PI_EXEC_DURATION )
    dbg_pin3_clear();
  #endif
}

/*! @brief reset PI controller structure
 * @details
 * reset controller without clearing Kp and Ki
 */
INLINE void pi_controller_reset ( pi_controller_t * controller )
{
  controller->set_value = (int16_t) 0;
  controller->err_sum   = (int32_t) 0;
}

/* }@
 */
