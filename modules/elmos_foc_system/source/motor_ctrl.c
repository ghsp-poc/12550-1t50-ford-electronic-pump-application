/**
 * @ingroup MC
 *
 * @{
 */

/**********************************************************************/
/*! @file motor_ctrl.c
 * @brief top-level module for controlling the commmutation of the motor
 *
 * @details
 * contains all functionalities for controlling the rotation of the
 * motor, including the top-level state machine for commutation<br>
 * module prefix: mc_
 *
 * @author JBER
 */
/**********************************************************************/
/* Demo Code Usage Restrictions:
 * Elmos Semiconductor AG provides this source code file simply and solely for IC evaluation purposes in laboratory and
 * this file must not be used for other purposes or within non laboratory environments. Especially, the use or the integration in
 * production systems, appliances or other installations is prohibited.
 *
 * Disclaimer:
 * Elmos Semiconductor AG shall not be liable for any damages arising out of defects resulting from (1) delivered hardware or software,
 * (2) non observance of instructions contained in this document, or (3) misuse, abuse, use under abnormal conditions or alteration by anyone
 * other than Elmos Semiconductor AG. To the extend permitted by law Elmos Semiconductor AG hereby expressively disclaims and user expressively
 * waives any and all warranties of merchantability and of fitness for a particular purpose, statutory warranty of non-infringement and any other
 * warranty or product liability that may arise by reason of usage of trade, custom or course of dealing.
 */
/**********************************************************************/

#include <stdlib.h>
#include <string.h>
#include <pwm_ctrl.h>
#include <motor_ctrl.h>
#include <foc.h>
#include <rotor_pos_detect.h>
#include <adc.h>
#include <adc_lists.h>
#include <math_utils.h>
#include <systime.h>
#include <speed_meas.h>

#if ( SCI_USAGE_TYPE == SCI_DATALOGGER )
 #include <uart.h>
#endif

#include <52307_driver.h>
#include "user_application.h"

/* -------------------
   local defines
   ------------------- */
typedef enum
{
  BEMF_MEAS_RESULT_UNDEF     = 0u,
  BEMF_DETECTED_AND_VALID    = 1u,
  BEMF_FAIL_EXCESSIVE_BEMF   = 2u,
  BEMF_FAIL_REVERSE_ROTATION = 4u
} mc_bemf_meas_result_t;

/** @var mc_data
 * @brief struct that holds all parameters, flags and process values of module motor control */
static mc_data_t mc_data;

/*! @brief set-function for 'sync_permission' */
void mc_set_sync_permission( bool_t sync_permission )
{
  mc_data.sync_data.sync_permission = sync_permission;
}

/*! @brief get-function for pointer towards 'mc_data' */
mc_data_t const * mc_get_data ( void )
{
  return &mc_data;
}

/*! @brief get-function for direction of rotor rotation */
INLINE motor_direction_t mc_get_direction ( void )
{
  return mc_data.direction;
}

/*! @brief set-function for direction of rotor rotation */
INLINE void mc_set_direction ( motor_direction_t direction )
{
  mc_data.direction = direction;
}

/*! @brief set-function for enable-flag 'short-circuit motor phases when idle (i.e. target_speed == 0)' */
INLINE void mc_set_active_breaking_when_idle( const bool_t enable )
{
  mc_data.enable_active_breaking_when_idle = enable;
}

/*! @brief get-function for IBAT (which is only computed when MC_COMPUTE_SUPPLY_CURRENT ist set) */
INLINE int16_t mc_get_supply_current ( void )
{
  return mc_data.supply_current;
}

/*! @brief get-function for the error flags of the gate driver IC (registers IRQSTAT1 and IRQSTAT1) */
INLINE uint16_t mc_get_irqstat( void )
{
  return (((uint16_t) mc_data.irqstat2 << 8 ) | (uint16_t) mc_data.irqstat1 );
}

/*! @brief preload (the integral part of the) speed controller
 *
 * @details
 * preload the speed controller in a way that with its first execution (at the given speeds), the output of the controller equals i_q_ref_target<br>
 * this is mainly used when changing from open loop to closed loop operation so as not to produce a step in the target value (foc->i_q_ref)<br>
 * the computation is simply the backward version of the PI controller .. <br>
 * err_sum_preload = ((i_q_ref_target - ((Kp * (target_speed - current_speed)) >> PI_CONTROLLER_KI_KP_SCALE_BITS) ) << PI_CONTROLLER_KI_KP_SCALE_BITS) / Ki
 */
INLINE static void mc_calc_speed_ctrl_preload_value ( const uint16_t current_speed, const uint16_t target_speed, const int16_t i_q_ref_target )
{
  s32 loc_buf = 0;

  __disable_interrupt();
  /* "i_q_ref_target - (Kp * (target_speed - current_speed)" */
  math_mulS16_unsafe((int16_t) (uint16_t) (( target_speed >> 1 ) - ( current_speed >> 1 )), mc_data.controller_rotor_speed.Kp );
  math_mul_shiftright_result( PI_CONTROLLER_KI_KP_SCALE_BITS );
  loc_buf = (int16_t) ( i_q_ref_target - (int16_t) math_mul_get_result( false ));
  __enable_interrupt();

  __disable_interrupt();
  /* shift left by PI_CONTROLLER_KI_KP_SCALE_BITS (MUL is faster here)*/
  math_mulS16_unsafe((int16_t) loc_buf, (int16_t) ((uint16_t) 1 << PI_CONTROLLER_KI_KP_SCALE_BITS ));
  loc_buf = math_mul_get_result( True );
  __enable_interrupt();

  __disable_interrupt();
  /* divide by Ki, finished */
  mc_data.speed_ctrl_err_sum_preload = math_divS32_unsafe( loc_buf, (int16_t) mc_data.controller_rotor_speed.Ki, True );
  __enable_interrupt();
}

/*! @brief initialize the compare values of the MotCU's PWM generator in a way that the output is constantly "LS on", i.e. phase driven to GND */
static void mc_init_pwm_compare_values( void )
{
  #if ( USR_SHUNT_MEASUREMENT_TYPE == USR_SHUNT_MEAS_TYPE_SINGLE )

    /* initialize with 0% output duty cycle (for allowing the bootstrap circuit to charge up upon start) */
    __disable_interrupt();
    pwm_set_c1_values( 0u, 0u, 0u );                /* falling edges */
    pwm_set_c0_values( U16_MAX, U16_MAX, U16_MAX ); /* rising edges */

    __enable_interrupt();

  #elif (( USR_SHUNT_MEASUREMENT_TYPE == USR_SHUNT_MEAS_TYPE_DUAL_U_V ) || ( USR_SHUNT_MEASUREMENT_TYPE == USR_SHUNT_MEAS_TYPE_DUAL_V_W ))

    /* initialize with 0% output duty cycle (for allowing the bootstrap circuit to charge up upon start) */
    __disable_interrupt();
    pwm_set_c1_values( 0u, 0u, 0u );                /* falling edges */
    pwm_set_c0_values( U16_MAX, U16_MAX, U16_MAX ); /* rising edges */

    __enable_interrupt();

  #else
  #error USR_SHUNT_MEASUREMENT_TYPE not supported!
  #endif
}

/*! @brief handler function for a failed attempt of synchronizing to a turning rotor
 *
 * @details
 * function gets passed the reason for the failure and needs to return the state to which the <br>
 * motor control state machine will change with its next iteration. Additional actions are also allowed. <br> <br>
 *
 * NOTE: This is rather just an example. The customer is supposed to override this function.
 */
#if ( USR_OVERRIDE_SYNC_TO_TURNING_ROTOR_FAILED_HANDLER != 1 )
  mc_motor_state_t mc_sync_to_turning_rotor_failed_handler ( const mc_sync_fail_reason_t fail_reason )
  {
    mc_motor_state_t loc_return_value = MC_IDLE;

    switch( fail_reason )
    {
      case MC_SYNC_FAIL_EXCESSIVE_BEMF: {loc_return_value = MC_IDLE; break; }
      case MC_SYNC_FAIL_REVERSE_ROTATION: {loc_return_value = MC_IDLE; break; }
      case MC_SYNC_FAIL_TIMEOUT: {loc_return_value =
                                    #if ( USR_EN_INITIAL_POSITION_DETECTION == 1 )
                                      ( mc_data.enable_initial_pos_detect ) ? MC_DETECT_ROTOR_POSITION :
                                    #endif
                                    MC_RAMP_CURRENT;

                                  #if ( FOC_EN_HALL_SENS == 1 )
                                    loc_return_value = ( FOC_HALL_SENSOR == foc_get_rotor_angle_meas_mode()) ? MC_RUNNING : loc_return_value;
                                  #endif
                                  break; }

      default: {loc_return_value = MC_IDLE; break; }
    }

    return loc_return_value;
  }
#endif

/*! @brief reset the PI controller for the rotor speed and preload its error sum with 'speed_ctrl_err_sum_preload' */
INLINE static void mc_reset_speed_controller ( void )
{
  pi_controller_reset( &( mc_data.controller_rotor_speed ));

  mc_data.controller_rotor_speed.err_sum = mc_data.speed_ctrl_err_sum_preload;
  mc_data.rotor_speed_ctrl_timestamp     = systime_getU16();
}

/*! @brief execute PI controller for rotor speed
 *
 * @details
 * This function is called by the ISR if speed-controlled operation is selected.<br>
 * Every 'rotor_speed_ctrl_time_base' systime ticks, the PI controller (with saturation) is iterated and <br>
 * its output is assigned to the target value of i_q. <br>
 * Also, if the direction measured by module 'spdmeas' does not fit the user setting in 'mc_data.direction', a stalled rotor is assumed. <br>
 * Since the PI controller is designed for S16 but the speed is U16, both speeds (momentary and target) are halved and casted to S16 -> loss of precision of 1 bit [eRPM]
 */
INLINE static void mc_control_speed_unsafe ( void )
{
  uint16_t loc_cur_timestamp = systime_getU16();

  if(( loc_cur_timestamp - mc_data.rotor_speed_ctrl_timestamp ) >= mc_data.rotor_speed_ctrl_time_base )
  {
    mc_data.rotor_speed_ctrl_timestamp = loc_cur_timestamp;

    if( mc_get_direction() != spdmeas_get_cur_direction())
    {
      mc_data.wrong_direction = True;

      #if ( FOC_EN_HALL_SENS == 1 )
        /* disregard the direction error when using hall sensors */
        if( FOC_HALL_SENSOR == foc_get_data()->rotor_angle_meas_mode )
        {
          mc_data.wrong_direction = false;
        }
      #endif
    }

    uint16_t loc_cur_speed = spdmeas_get_cur_speed();

    #if (( FOC_EN_HALL_SENS == 1 ) && ( USR_USE_HALL_SPEED_CALC == 1 ))
      static bool_t loc_use_hall_speed_calc = false;

      if( FOC_HALL_SENSOR == foc_get_data()->rotor_angle_meas_mode )
      {
        if( hall_get_cur_speed() < USR_HALL_SPEED_CALC_THRSHLD_HYS_LOWER )
        {
          loc_use_hall_speed_calc = True;
        }
        else if( hall_get_cur_speed() > USR_HALL_SPEED_CALC_THRSHLD_HYS_UPPER )
        {
          loc_use_hall_speed_calc = false;
        }
        else
        {
          /*Message 2013*/
        }
        if( loc_use_hall_speed_calc )
        {
          loc_cur_speed = hall_get_cur_speed();
        }
      }
    #endif

    pi_controller_exec_w_saturation_unsafe( &( mc_data.controller_rotor_speed ), (int16_t) (uint16_t) ( mc_data.target_rotor_speed >> 1 ), (int16_t) (uint16_t) ( loc_cur_speed >> 1 ), mc_data.speed_ctrl_output_max, mc_data.speed_ctrl_output_min );

    if( mc_get_direction() == FORWARD )
    {
      foc_set_i_q_ref( mc_data.controller_rotor_speed.set_value );
    }
    else
    {
      foc_set_i_q_ref( -mc_data.controller_rotor_speed.set_value );
    }
  }
}

/*! @brief check whether a gate driver error has occurred
 *
 * @details
 * Depending on what IC is used, the HV die (i.e. gate driver) is queried for error conditions. <br>
 * Additionally to the function returning "True" in case of an error condition, 'mc_data.irqstat1' & 'mc_data.irqstat2' <br>
 * hold a value != 0x00 in case of an error. <br>
 * 523.05: the register values of IRQSTATx are held by the variables <br>
 * 523.52: mc_data.irqstat1[0] = ERR1; mc_data.irqstat1[1] = ERR2
 */
INLINE static bool_t mc_check_for_driver_error ( void )
{
    bool_t loc_return_value = false;

    d52307_driver_error_get( &mc_data.irqstat1, &mc_data.irqstat2 );

    if(( 0u != mc_data.irqstat1 ) || ( 0u != mc_data.irqstat2 ))
    {
        loc_return_value = True;
    }

    return loc_return_value;
}

/*! @brief compute the DC link current (supply current) that is drawn by the system
 *
 * @details
 * Simply put, the DC link current is the average of the current that is flowing through the sum shunt. <br>
 * By retrieving the momentary phase voltages (in PWM ticks) and the phase currents (in Amps) from the <br>
 * FOC module, the sum current is computed (computation is commented in line with the source code).
 */
#if ( MC_COMPUTE_SUPPLY_CURRENT == 1 )
  INLINE static void mc_supply_current_calc_unsafe( void )
  {
    #if ( DBG_PIN3_FUNCTION == DBG_SHOW_SUPPLY_CURRENT_CALC_DURATION )
      dbg_pin3_set();
    #endif

    foc_ibat_calc_data_t loc_ibat_calc_data = {0};
    loc_ibat_calc_data = foc_get_ibat_calc_data_unsafe();

    /* function is called on interrupt level, therefore MUL/DIV may be used */
    /* ibat = ((i1 * (v_mid - vlow)) + (i2 * (v_max - v_mid)) / vbat ; since the voltages are represented in compare timer
     * ticks, "vbat" equals the counter ticks per PWM period */
    math_mulS16_unsafe( -loc_ibat_calc_data.i1, loc_ibat_calc_data.v_val_mid - loc_ibat_calc_data.v_val_low );
    math_mulS16_acc_unsafe( loc_ibat_calc_data.i2, loc_ibat_calc_data.v_val_max - loc_ibat_calc_data.v_val_mid );

    mc_data.supply_current = (int16_t) math_divS32_unsafe( math_mul_get_result( True ), (int16_t) PWM_PERIOD_DURATION_TICKS, false );

    #if ( DBG_PIN3_FUNCTION == DBG_SHOW_SUPPLY_CURRENT_CALC_DURATION )
      dbg_pin3_clear();
    #endif
  }
#endif

/* PRQA S 3206 ++ */ /* justification: all state machine handler functions should look the same -> argument not necessarily used */

/*! @brief handler function for state IDLE
 *
 * @details
 * This state is only left if a target_speed != 0 is set by the user. <br>
 * Apart from doing nothing, upon starting request, possibly existing gate driver errors are <br>
 * cleared so that the startup always begins with an initialized gate driver.
 */
INLINE static mc_motor_state_t mc_handle_state_IDLE ( const uint32_t state_duration )
{
  mc_motor_state_t loc_return_val = MC_IDLE;

  mc_data.commutation_mode = MC_OPEN_LOOP;

  __disable_interrupt();
  mc_data.foc_enable      = false;
  mc_data.outputs_enable  = false;
  mc_data.meas_bemf       = false;
  mc_data.exec_speed_ctrl = false;
  mc_data.exec_pos_detect = false;
  __enable_interrupt();

  if( mc_data.target_rotor_speed > 0u )
  {
    if( mc_check_for_driver_error())
    {
      loc_return_val = MC_CLEAR_DRIVER_ERROR;
    }
    else
    {
      loc_return_val = MC_START_COMMUTATION;
    }
  }
  else if( mc_data.enable_active_breaking_when_idle )
  {
    mc_init_pwm_compare_values();

    if( mc_check_for_driver_error())
    {
      loc_return_val = MC_CLEAR_DRIVER_ERROR;
    }
    else
    {
      loc_return_val = MC_APPLY_MOTOR_PHASE_SC;
    }
  }
  #if ( USR_EN_INITIAL_POSITION_DETECTION == 1 )
    else if( mc_data.pos_detect_data.meas_permanently )
    {
      if( mc_check_for_driver_error())
      {
        loc_return_val = MC_CLEAR_DRIVER_ERROR;
      }
      else
      {
        loc_return_val = MC_DETECT_ROTOR_POSITION;
      }
    }
  #endif
  else
  {
/*Message 2013*/
  }
  return loc_return_val;
}

/*! @brief handler function for state START_COMMUTATION
 *
 * @details
 * Basically, everything (FOC, MC) is reset to initial defaults to always start in a defined state. <br>
 * This handler is executed only once, the state machine then changes to either the current ramp or the attempt to <br>
 * synchronize to a turning rotor.
 */
INLINE static mc_motor_state_t mc_handle_state_START_COMMUTATION ( const uint32_t state_duration )
{
  mc_motor_state_t loc_return_val = MC_START_COMMUTATION;

  mc_data.commutation_mode = MC_OPEN_LOOP;

  __disable_interrupt();
  mc_data.foc_enable      = false;
  mc_data.outputs_enable  = false;
  mc_data.meas_bemf       = false;
  mc_data.exec_speed_ctrl = false;
  mc_data.exec_pos_detect = false;
  __enable_interrupt();

  mc_data.startup.angle_incr         = mc_data.startup.speed_ramp_start;
  mc_data.startup.i_q_ref_start_ramp = 0;

  __disable_interrupt();
  mc_data.stall_detected            = false;
  mc_data.wrong_direction           = false;
  mc_data.pwm_cmp_val_overflow_flag = false;
  __enable_interrupt();

  foc_reset_data( True );
  foc_set_i_d_ref( 0 );
  foc_set_i_q_ref( mc_data.startup.i_q_ref_start_ramp );

  mc_init_pwm_compare_values();

  if( FOC_HALL_SENSOR == foc_get_data()->rotor_angle_meas_mode )
  {
    __disable_interrupt();
    mc_reset_speed_controller();
    __enable_interrupt();

    loc_return_val = ( mc_data.enable_sync_to_rotor ) ? MC_SYNC_ROTOR_DELAY : MC_RUNNING;
  }
  #if ( USR_EN_INITIAL_POSITION_DETECTION == 1 )
    else if( mc_data.pos_detect_data.run_calibration )
    {
      foc_set_rotor_angle_startup( mc_data.pos_detect_data.calibration_forced_angle );
      loc_return_val = MC_RAMP_CURRENT;
    }
  #endif
  else if( mc_data.enable_sync_to_rotor )
  {
    loc_return_val = MC_SYNC_ROTOR_DELAY;
  }
  #if ( USR_EN_INITIAL_POSITION_DETECTION == 1 )
    else if( mc_data.enable_initial_pos_detect )
    {
      loc_return_val = MC_DETECT_ROTOR_POSITION;
    }
  #endif
  else
  {
    loc_return_val = MC_RAMP_CURRENT;
  }

  return loc_return_val;
}

/*! @brief handler function for state RAMP_CURRENT
 *
 * @details
 * With the output and the FOC calculation being enabled yet the commutation operating in open loop, the <br>
 * phase current vector is linearly increased up to the value set by the user (mc_data.startup.i_q_ref) with a <br>
 * gradient chosen also by the user (mc_data.startup.current_slope) <br>
 * This state can also be used for rotor alignment when the slope of the ramp is low enough.
 */
/* PRQA S 3206 */
INLINE static mc_motor_state_t mc_handle_state_RAMP_CURRENT ( const uint32_t state_duration )
{
  static int16_t loc_slope_cnt        = 0;
  mc_motor_state_t loc_return_val = MC_RAMP_CURRENT;

  #if ( DBG_PIN3_FUNCTION == DBG_SHOW_CURRENT_RAMP )
    dbg_pin3_set();
  #endif

  mc_data.commutation_mode = MC_OPEN_LOOP;

  __disable_interrupt();
  mc_data.foc_enable      = True;
  mc_data.outputs_enable  = True;
  mc_data.meas_bemf       = false;
  mc_data.exec_speed_ctrl = false;
  mc_data.exec_pos_detect = false;
  __enable_interrupt();

  if( mc_data.startup.current_slope < 0 )
  {
    loc_slope_cnt--;

    if( loc_slope_cnt < mc_data.startup.current_slope )
    {
      mc_data.startup.i_q_ref_start_ramp++;
      loc_slope_cnt = 0;
    }
  }
  else
  {
    mc_data.startup.i_q_ref_start_ramp += mc_data.startup.current_slope;
  }

  if( mc_get_direction() == FORWARD )
  {
    foc_set_i_q_ref( mc_data.startup.i_q_ref_start_ramp );
  }
  else
  {
    foc_set_i_q_ref( -mc_data.startup.i_q_ref_start_ramp );
  }

  if( mc_check_for_driver_error())
  {
    loc_return_val = MC_DRIVER_ERROR;
  }
  else if( 0u == mc_data.target_rotor_speed )
  {
    #if ( DBG_PIN3_FUNCTION == DBG_SHOW_CURRENT_RAMP )
      dbg_pin3_clear();
    #endif

    loc_return_val = MC_IDLE;
  }
  else if( mc_data.startup.i_q_ref_start_ramp >= mc_data.startup.i_q_ref )
  {
    #if ( DBG_PIN3_FUNCTION == DBG_SHOW_CURRENT_RAMP )
      dbg_pin3_clear();
    #endif

    loc_return_val = MC_ALIGN_ROTOR;
  }
  else
  {
/*Message 2013*/
  }

  return loc_return_val;
}

/*! @brief handler function for state ALIGN_ROTOR
 *
 * @details
 * In this state, the phase current vector (which had been reached at the end of state RAMP_CURRENT) is <br>
 * simply held for a user-defined amount of time (mc_data.startup.align_rotor_duration) to give the <br>
 * rotor the time to align to the applied current vector (albeit this can actually be achieved more efficiently by <br>
 * a slow-enough current ramp).
 */
INLINE static mc_motor_state_t mc_handle_state_ALIGN_ROTOR ( const uint32_t state_duration )
{
  mc_motor_state_t loc_return_val = MC_ALIGN_ROTOR;

  mc_data.commutation_mode = MC_OPEN_LOOP;

  __disable_interrupt();
  mc_data.foc_enable      = True;
  mc_data.outputs_enable  = True;
  mc_data.meas_bemf       = false;
  mc_data.exec_speed_ctrl = false;
  mc_data.exec_pos_detect = false;
  __enable_interrupt();

  if( state_duration >= (uint32_t) mc_data.startup.align_rotor_duration )
  {
    loc_return_val = MC_RAMP_SPEED;

    #if ( USR_EN_INITIAL_POSITION_DETECTION == 1 )
      if( mc_data.pos_detect_data.run_calibration )
      {
        loc_return_val = MC_DETECT_ROTOR_POSITION;
      }
    #endif
  }

  if( 0u == mc_data.target_rotor_speed )
  {
    loc_return_val = MC_IDLE;
  }

  return loc_return_val;
}

/*! @brief handler function for state RAMP_SPEED
 *
 * @details
 * Using the phase current amplitude which has been reached at the end of state RAMP_CURRENT, an <br>
 * rotating field with increasing speed is applied to the rotor, starting at 'mc_data.startup.speed_ramp_end', <br>
 * increasing with a gradient of 'mc_data.startup.speed_ramp_slope' until 'mc_data.startup.speed_ramp_end'. <br>
 * Then, the system changes into closed loop operation (for debugging and commissioning a new motor, flag <br>
 * 'mc_data.remain_in_open_loop' can prevent this; the open loop commutation is then simply continued at 'mc_data.startup.speed_ramp_end').
 */
INLINE static mc_motor_state_t mc_handle_state_RAMP_SPEED ( const uint32_t state_duration )
{
  mc_motor_state_t loc_return_val = MC_RAMP_SPEED;

  #if ( DBG_PIN3_FUNCTION == DBG_SHOW_SPEED_RAMP )
    dbg_pin3_set();
  #endif

  mc_data.commutation_mode = MC_OPEN_LOOP;

  __disable_interrupt();
  mc_data.foc_enable      = True;
  mc_data.outputs_enable  = True;
  mc_data.meas_bemf       = false;
  mc_data.exec_speed_ctrl = false;
  mc_data.exec_pos_detect = false;
  __enable_interrupt();

  if(( !mc_data.remain_in_open_loop ) && ( mc_data.startup.angle_incr >= mc_data.startup.speed_ramp_end ))
  {
    __disable_interrupt();
    /* convert angle increments to eRPM */
    math_mulU16_unsafe( mc_data.startup.speed_ramp_end, (uint16_t) (( 60.0 * (dbl) SYSTIME_TICKS_PER_SEC ) / (dbl) MATH_ANGLE_MAX ));
    uint16_t loc_speed_ramp_end_rpm = (uint16_t) math_mul_get_result( false );
    __enable_interrupt();

    mc_calc_speed_ctrl_preload_value( loc_speed_ramp_end_rpm, mc_data.target_rotor_speed, mc_data.startup.i_q_ref );
    __disable_interrupt();
    mc_reset_speed_controller();
    __enable_interrupt();

    if( mc_data.enable_fast_startup )
    {
      loc_return_val = MC_FAST_STARTUP;
    }
    else
    {
      loc_return_val = MC_RUNNING;
    }

    #if ( DBG_PIN3_FUNCTION == DBG_SHOW_SPEED_RAMP )
      dbg_pin3_clear();
    #endif
  }

  if( mc_check_for_driver_error())
  {
    loc_return_val = MC_DRIVER_ERROR;

    #if ( DBG_PIN3_FUNCTION == DBG_SHOW_SPEED_RAMP )
      dbg_pin3_clear();
    #endif
  }
  else if( 0u == mc_data.target_rotor_speed )
  {
    loc_return_val = MC_IDLE;

    #if ( DBG_PIN3_FUNCTION == DBG_SHOW_SPEED_RAMP )
      dbg_pin3_clear();
    #endif
  }
  else
  {
/*Message 2013*/
  }

  return loc_return_val;
}

/*! @brief handler function for state RUNNING
 *
 * @details
 * This state implements the closed-loop operation mode. Until the occurrence of a gate driver error or a <br>
 * stalled rotor detection, the system remains in that state. The user can also stop the commutation by setting a <br>
 * target speed of 0.
 */
INLINE static mc_motor_state_t mc_handle_state_RUNNING ( const uint32_t state_duration )
{
  mc_motor_state_t loc_return_val = MC_RUNNING;

  mc_data.commutation_mode = MC_CLOSED_LOOP;

  __disable_interrupt();
  mc_data.foc_enable      = True;
  mc_data.outputs_enable  = True;
  mc_data.meas_bemf       = false;
  mc_data.exec_speed_ctrl = True;
  mc_data.exec_pos_detect = false;
  __enable_interrupt();

  if( mc_check_for_driver_error())
  {
    loc_return_val = MC_DRIVER_ERROR;
  }
  else if(( mc_data.stall_detected ) || ( mc_data.wrong_direction ))
  {
    loc_return_val = MC_STALLED;
  }
  else if( 0u == mc_data.target_rotor_speed )
  {
    loc_return_val = MC_IDLE;
  }
  else
  {
/*Message 2013*/
  }

  return loc_return_val;
}

/*! @brief handler function for state STALLED
 *
 * @details
 * There's three independent stall detection mechanism, only one of which needs to <br>
 * trigger in order change to state STALLED, where the commutation is stopped and the gate <br>
 * drivers are switched high-ohmic. <br>
 * The user or the application needs to acknowledge this error by setting the target speed to <br>
 * 0. The system then returns to state IDLE and the motor can be started again.
 */
INLINE static mc_motor_state_t mc_handle_state_STALLED ( const uint32_t state_duration )
{
  mc_motor_state_t loc_return_val = MC_STALLED;

  mc_data.commutation_mode = MC_OPEN_LOOP;

  __disable_interrupt();
  mc_data.foc_enable      = false;
  mc_data.outputs_enable  = false;
  mc_data.meas_bemf       = false;
  mc_data.exec_speed_ctrl = false;
  mc_data.exec_pos_detect = false;
  __enable_interrupt();

  if( 0u == mc_data.target_rotor_speed )
  {
    loc_return_val = MC_IDLE;
  }

  return loc_return_val;
}

/*! @brief handler function for state DRIVER_ERROR
 *
 * @details
 * If an error condition is detected by the gate driver, the system changes to state DRIVER_ERROR. Variables <br>
 * 'mc_data.irqstat1' and 'mc_data.irqstat2' then hold the type of the error. <br>
 * The user or the application needs to acknowledge this error by setting the target speed to <br>
 * 0. The system then returns to state IDLE and the motor can be started again.
 */
INLINE static mc_motor_state_t mc_handle_state_DRIVER_ERROR ( const uint32_t state_duration )
{
  mc_motor_state_t loc_return_val = MC_DRIVER_ERROR;

  #if ( DBG_PIN3_FUNCTION == DBG_SHOW_DRIVER_ERROR )
    dbg_pin3_set();
  #endif

  mc_data.commutation_mode = MC_OPEN_LOOP;

  __disable_interrupt();
  mc_data.foc_enable      = false;
  mc_data.outputs_enable  = false;
  mc_data.meas_bemf       = false;
  mc_data.exec_speed_ctrl = false;
  mc_data.exec_pos_detect = false;
  __enable_interrupt();

  if(( 0u == mc_data.target_rotor_speed ) && ( !mc_data.enable_active_breaking_when_idle ))
  {
    #if ( DBG_PIN3_FUNCTION == DBG_SHOW_DRIVER_ERROR )
      dbg_pin3_clear();
    #endif

    loc_return_val = MC_IDLE;
  }

  return loc_return_val;
}

/*! @brief handler function for state CLEAR_DRIVER_ERROR
 *
 * @details
 * When this state is entered, any error conditions detected by the gate driver are reset. The <br>
 * system executes this handler function only once and then changes back to state IDLE.
 */
INLINE static mc_motor_state_t mc_handle_state_CLEAR_DRIVER_ERROR ( const uint32_t state_duration )
{
  mc_motor_state_t loc_return_val = MC_CLEAR_DRIVER_ERROR;

  mc_data.commutation_mode = MC_OPEN_LOOP;

  __disable_interrupt();
  mc_data.foc_enable      = false;
  mc_data.outputs_enable  = false;
  mc_data.meas_bemf       = false;
  mc_data.exec_speed_ctrl = false;
  mc_data.exec_pos_detect = false;
  __enable_interrupt();

  d52307_driver_error_clear();
  loc_return_val = MC_IDLE;

  return loc_return_val;
}

/*! @brief handler function for state SYNC_ROTOR_DELAY
 *
 * @details
 * DEPRECATED! Absolutely nothing is done here.
 */
INLINE static mc_motor_state_t mc_handle_state_SYNC_ROTOR_DELAY ( const uint32_t state_duration )
{
  mc_motor_state_t loc_return_val = MC_SYNC_ROTOR_DELAY;

  mc_data.commutation_mode = MC_OPEN_LOOP;

  __disable_interrupt();
  mc_data.foc_enable      = false;
  mc_data.outputs_enable  = false;
  mc_data.meas_bemf       = false;
  mc_data.exec_speed_ctrl = false;
  mc_data.exec_pos_detect = false;
  __enable_interrupt();

  loc_return_val = MC_SYNC_TO_TURNING_ROTOR;

  return loc_return_val;
}

/*! @brief handler function for state SYNC_TO_TURNING_ROTOR
 *
 * @details
 * This state implements the request for synchronizing to a turning rotor. <br>
 * Actually, there's two parts to this process but since the switching between these two <br>
 * needs to be precisely at a certain moment in time, this is implemented in the ISR (see mc_sync_state_machine_exec() ). <br>
 * Flag 'mc_data.meas_bemf' actually only serves this purpose, that's why it is set to "True" only in this state.
 */
INLINE static mc_motor_state_t mc_handle_state_SYNC_TO_TURNING_ROTOR ( const uint32_t state_duration )
{
  mc_motor_state_t loc_return_val = MC_SYNC_TO_TURNING_ROTOR;

  mc_data.commutation_mode = MC_CLOSED_LOOP;

  __disable_interrupt();
  mc_data.foc_enable      = True;
  mc_data.outputs_enable  = True;
  mc_data.meas_bemf       = True;
  mc_data.exec_speed_ctrl = false;
  mc_data.exec_pos_detect = false;
  __enable_interrupt();

  #if ( DBG_PIN3_FUNCTION == DBG_SHOW_SYNC_TO_TURNING_ROTOR )
    dbg_pin3_set();
  #endif

  if( 0u == mc_data.target_rotor_speed )
  {
    loc_return_val = MC_IDLE;
  }
  else if( MC_SYNC_STATE_FINISHED == mc_data.sync_data.sync_state )
  {
    #if ( DBG_PIN3_FUNCTION == DBG_SHOW_SYNC_TO_TURNING_ROTOR )
      dbg_pin3_clear();
    #endif

    __disable_interrupt();
    mc_data.speed_ctrl_err_sum_preload = (s32) 0L;
    mc_reset_speed_controller();
    __enable_interrupt();

    loc_return_val = MC_RUNNING;
  }
  else if( MC_SYNC_STATE_FAILED == mc_data.sync_data.sync_state )
  {
    #if ( DBG_PIN3_FUNCTION == DBG_SHOW_SYNC_TO_TURNING_ROTOR )
      dbg_pin3_clear();
    #endif

    loc_return_val = mc_sync_to_turning_rotor_failed_handler( mc_data.sync_data.sync_fail_reason );
  }
  else if(( state_duration > mc_data.sync_data.sync_timeout ) && ( MC_SYNC_STATE_COMMUTATION_ACTIVE != mc_data.sync_data.sync_state ))
  {
    #if ( DBG_PIN3_FUNCTION == DBG_SHOW_SYNC_TO_TURNING_ROTOR )
      dbg_pin3_clear();
    #endif

    #if ( FOC_EN_HALL_SENS == 1 )
      if( FOC_HALL_SENSOR == foc_get_rotor_angle_meas_mode())
      {
        hall_reset_data();
      }
    #endif

    loc_return_val = mc_sync_to_turning_rotor_failed_handler( MC_SYNC_FAIL_TIMEOUT );
  }
  else
  {
/*Message 2013*/
  }

  return loc_return_val;
}

/*! @brief NOT YET RELEASED!! WORK IN PROGRESS!! handler function for state FAST_STARTUP
 *
 * @details
 * ATTENTION: output voltage overflow is currently not handled when operating in this state!!
 */
INLINE static mc_motor_state_t mc_handle_state_FAST_STARTUP ( const uint32_t state_duration )
{
  #if ( DBG_PIN3_FUNCTION == DBG_SHOW_FAST_STARTUP )
    dbg_pin3_set();
  #endif

  mc_motor_state_t loc_return_val = MC_FAST_STARTUP;

  mc_data.commutation_mode = MC_CLOSED_LOOP;

  __disable_interrupt();
  mc_data.foc_enable      = True;
  mc_data.outputs_enable  = True;
  mc_data.meas_bemf       = false;
  mc_data.exec_speed_ctrl = false;
  mc_data.exec_pos_detect = false;
  __enable_interrupt();

  /* set target value of i_q according to user-defined "fast startup" parameter */
  if( FORWARD == mc_data.direction )
  {
    foc_set_i_q_ref( mc_data.fast_startup_data.i_q_ref );
  }
  else
  {
    foc_set_i_q_ref( -mc_data.fast_startup_data.i_q_ref );
  }

  if( mc_check_for_driver_error())
  {
    #if ( DBG_PIN3_FUNCTION == DBG_SHOW_FAST_STARTUP )
      dbg_pin3_clear();
    #endif

    loc_return_val = MC_DRIVER_ERROR;
  }
  else if(( mc_data.stall_detected ) || ( mc_data.wrong_direction ))
  {
    #if ( DBG_PIN3_FUNCTION == DBG_SHOW_FAST_STARTUP )
      dbg_pin3_clear();
    #endif

    loc_return_val = MC_STALLED;
  }
  else if( 0u == mc_data.target_rotor_speed )
  {
    #if ( DBG_PIN3_FUNCTION == DBG_SHOW_FAST_STARTUP )
      dbg_pin3_clear();
    #endif

    loc_return_val = MC_IDLE;
  }
  else if(( spdmeas_get_cur_speed() >= mc_data.target_rotor_speed ) || ( state_duration > mc_data.fast_startup_data.timeout ))
  {
    #if ( DBG_PIN3_FUNCTION == DBG_SHOW_FAST_STARTUP )
      dbg_pin3_clear();
    #endif

    mc_calc_speed_ctrl_preload_value ( spdmeas_get_cur_speed(), mc_data.target_rotor_speed, foc_get_i_q_ref());
    __disable_interrupt();
    mc_reset_speed_controller();
    __enable_interrupt();

    loc_return_val = MC_RUNNING;
  }
  else
  {
/*Message 2013*/
  }

  return loc_return_val;
}

/*! @brief handler function for state APPLY_MOTOR_PHASE_SC
 *
 * @details
 * This state implements the active short-circuiting of the three motor phases <br>
 * using a duty cycle of 50% for all three phases. This might be used to brake a motor that <br<
 * is rotated by an external force (-> windmill effect) when the rotor's speed is below the value <br>
 * where proper closed-loop operation is possible. <br>
 * When flag 'mc_data.enable_active_breaking_when_idle' is set, the motor control state machine changes to this state <br>
 * immediately when 'mc_data.target_speed' is set to 0. By either clearing the flag or setting target_speed != 0, the state machine <br>
 * goes back to MC_IDLE.
 * <b>NOTE: The state relies on the fact that mc_init_pwm_compare_values() is called when changing from MC_IDLE to this state!</b>
 */
INLINE static mc_motor_state_t mc_handle_state_APPLY_MOTOR_PHASE_SC ( const uint32_t state_duration )
{
  mc_motor_state_t loc_return_val = MC_APPLY_MOTOR_PHASE_SC;

  mc_data.commutation_mode = MC_OPEN_LOOP;

  __disable_interrupt();
  mc_data.foc_enable      = false;
  mc_data.outputs_enable  = True;
  mc_data.meas_bemf       = false;
  mc_data.exec_speed_ctrl = false;
  mc_data.exec_pos_detect = false;
  __enable_interrupt();

  if( mc_check_for_driver_error())
  {
    loc_return_val = MC_DRIVER_ERROR;
  }
  else if(( 0u != mc_data.target_rotor_speed ) || ( !mc_data.enable_active_breaking_when_idle ))
  {
    loc_return_val = MC_IDLE;
  }
  else
  {
/*Message 2013*/
  }

  return loc_return_val;
}

#if ( USR_EN_INITIAL_POSITION_DETECTION == 1 )
/*! @brief handler function for state DETECT_ROTOR_POSITION
 *
 * @details
 * todo <br>
 * todo <br>
 */
  INLINE static mc_motor_state_t mc_handle_state_DETECT_ROTOR_POSITION( const uint32_t loc_duration )
  {
    mc_motor_state_t loc_return_val = MC_DETECT_ROTOR_POSITION;

    mc_data.commutation_mode = MC_OPEN_LOOP;

    __disable_interrupt();
    mc_data.foc_enable      = false;
    mc_data.outputs_enable  = false;
    mc_data.meas_bemf       = false;
    mc_data.exec_speed_ctrl = false;
    mc_data.exec_pos_detect = True;
    __enable_interrupt();

    pos_detect_state_t loc_pos_detect_state = pos_get_current_state();

    if( mc_check_for_driver_error())
    {
      loc_return_val = MC_DRIVER_ERROR;
    }
    else if( mc_data.pos_detect_data.meas_permanently )
    {
      if( POS_DETECT_STATE_FINISHED == pos_get_current_state())
      {
        if( loc_duration > SYSTIME_MS_TO_SYSTIME_TICKS( 100u ))
        {
          loc_return_val = MC_IDLE;
        }
      }
    }
    else if( mc_data.pos_detect_data.run_calibration )
    {
      if( loc_duration > SYSTIME_MS_TO_SYSTIME_TICKS( 500u ))
      {
        loc_return_val = MC_IDLE;
      }
    }
    else if( mc_data.enable_initial_pos_detect )
    {
      if( POS_DETECT_STATE_FINISHED == loc_pos_detect_state )
      {
        foc_set_rotor_angle_startup( mc_data.pos_detect_data.rotor_angle_meas );
        loc_return_val = MC_RAMP_CURRENT;
      }
    }
    else if( loc_duration > SYSTIME_MS_TO_SYSTIME_TICKS( 1000u ))
    {
      loc_return_val = MC_IDLE;
    }
    else
    {
      /*Message 2013*/
    }
    return loc_return_val;
  }

#endif

/* PRQA S 3206 -- */ /* justification: all state machine handler functions should look the same -> argument not necessarily used */

/*! @brief main state machine for controlling the motor commutation
 *
 * @details
 * The state machine is a simple switch/case construct with one variable as memory (mc_data.state) and handler <br>
 * functions for every state (f.i. mc_handle_state_IDLE() ), which return the next state upon call (i.e. implementation of the state transitions). <br>
 * The duration of a state is tracked by 'loc_duration' so that the handler function can also react to this information (f.i. change to <br>
 * next state after 50ms). <br>
 * EVERY state must set the flags which are read by the ISR (foc_enable, outputs_enable, meas_bemf, exec_speed_ctrl, exec_pos_detect) in accordance with its purpose.
 */
NO_INLINE void mc_main ( void )
{
  static uint32_t loc_last_timestamp = (uint32_t) 0;                /* time of the last execution of the state machine */
  static uint32_t loc_duration       = (uint32_t) 0;                /* duration (in SYSTIME_TICKS) of the current state */

  #if ( DBG_PIN3_FUNCTION == DBG_MOT_CTRL_DURATION )
    dbg_pin3_set();
  #endif

  uint32_t loc_cur_timestamp = systime_getU32();
  uint32_t loc_diff_time     = loc_cur_timestamp - loc_last_timestamp;

  if( loc_diff_time >= (uint32_t) ( SYSTIME_MS_TO_SYSTIME_TICKS( 1u )))
  {
    mc_motor_state_t loc_next_state = MC_IDLE;

    switch( mc_data.state )
    {
      case MC_IDLE:                   {loc_next_state = mc_handle_state_IDLE( loc_duration ); break; }
      case MC_START_COMMUTATION:      {loc_next_state = mc_handle_state_START_COMMUTATION( loc_duration ); break; }
      case MC_RAMP_CURRENT:           {loc_next_state = mc_handle_state_RAMP_CURRENT( loc_duration ); break; }
      case MC_ALIGN_ROTOR:            {loc_next_state = mc_handle_state_ALIGN_ROTOR( loc_duration ); break; }
      case MC_RAMP_SPEED:             {loc_next_state = mc_handle_state_RAMP_SPEED( loc_duration ); break; }
      case MC_RUNNING:                {loc_next_state = mc_handle_state_RUNNING( loc_duration ); break; }
      case MC_STALLED:                {loc_next_state = mc_handle_state_STALLED( loc_duration ); break; }
      case MC_DRIVER_ERROR:           {loc_next_state = mc_handle_state_DRIVER_ERROR( loc_duration ); break; }
      case MC_CLEAR_DRIVER_ERROR:     {loc_next_state = mc_handle_state_CLEAR_DRIVER_ERROR( loc_duration ); break; }
      case MC_SYNC_ROTOR_DELAY:       {loc_next_state = mc_handle_state_SYNC_ROTOR_DELAY( loc_duration ); break; }
      case MC_SYNC_TO_TURNING_ROTOR:  {loc_next_state = mc_handle_state_SYNC_TO_TURNING_ROTOR( loc_duration ); break; }
      case MC_FAST_STARTUP:           {loc_next_state = mc_handle_state_FAST_STARTUP( loc_duration ); break; }
      case MC_APPLY_MOTOR_PHASE_SC:   {loc_next_state = mc_handle_state_APPLY_MOTOR_PHASE_SC( loc_duration ); break; }
        #if ( USR_EN_INITIAL_POSITION_DETECTION == 1 )
          case MC_DETECT_ROTOR_POSITION:  {loc_next_state = mc_handle_state_DETECT_ROTOR_POSITION( loc_duration ); break; }
        #endif

      default:                        {loc_next_state = MC_IDLE; break; }
    }

    if( loc_next_state != mc_data.state )
    {
      loc_duration  = (uint32_t) 0;
      mc_data.state = loc_next_state;

      #ifdef MC_STATE_HISTORY_LENGTH
        #if ( MC_STATE_HISTORY_LENGTH > 0 )
          mc_data.state_history[ mc_data.state_history_idx ] = mc_data.state;
          mc_data.state_history_idx++;

          if( mc_data.state_history_idx >= (uint16_t) MC_STATE_HISTORY_LENGTH )
          {
            mc_data.state_history_idx = 0u;
          }
        #endif
      #endif
    }
    else
    {
      uint32_t new_duration = loc_duration + loc_diff_time;

      /* disallow arithmetic overflow */
      if( new_duration > loc_duration )
      {
        loc_duration = new_duration;
      }
    }

    loc_last_timestamp = loc_cur_timestamp;
  }

  #if ( DBG_PIN3_FUNCTION == DBG_SHOW_CLOSED_LOOP )
    if( MC_CLOSED_LOOP == mc_get_comm_mode()){dbg_pin3_set(); }
    else {dbg_pin3_clear(); }
  #endif

  #if ( DBG_PIN3_FUNCTION == DBG_MOT_CTRL_DURATION )
    dbg_pin3_clear();
  #endif
}

/*! @brief initialize motor control module
 *
 * @details
 * Initialize storage structure, set some parameters to sensible start values & switch
 * all gate drivers to high-ohmic state.
 */
NO_INLINE void mc_init ( void )
{
  (void) memset((void *) &mc_data, 0, sizeof( mc_data )); /* PRQA S 0314 */ /* justification: come on, it's memset ... */

  pi_controller_init( &mc_data.controller_rotor_speed, (int16_t) MC_CTRL_ROT_SPEED_INIT_KP, (uint16_t) MC_CTRL_ROT_SPEED_INIT_KI );

  mc_data.speed_ctrl_err_sum_preload = (s32) 0L;
  mc_set_startup_i_q_ref( 0 );

  mc_data.enable_sync_to_rotor      = false;
  mc_data.sync_data.bemf_ampl_min   = (int16_t) S16_MAX;
  mc_data.sync_data.sync_permission = True;
  mc_data.sync_data.sync_duration   = 0u;
  mc_data.sync_data.sync_timeout    = 0u;
  mc_data.sync_data.bemf_ampl_gain  = 256;

  mc_data.enable_fast_startup       = false;
  mc_data.fast_startup_data.timeout = (uint32_t) 0;

  mc_data.sync_data.sync_state        = MC_SYNC_STATE_DISABLED;
  mc_data.sync_data.crossing_evt_cntr = 0u;

  mc_data.direction = FORWARD;

  mc_data.stall_detected                   = false;
  mc_data.wrong_direction                  = false;
  mc_data.remain_in_open_loop              = false;
  mc_data.enable_active_breaking_when_idle = false;

  mc_data.commutation_mode = MC_OPEN_LOOP;

  mc_data.foc_enable      = false;
  mc_data.outputs_enable  = false;
  mc_data.meas_bemf       = false;
  mc_data.exec_speed_ctrl = false;

  #if ( USR_EN_INITIAL_POSITION_DETECTION == 1 )
    mc_data.pos_detect_data.test_pulse_length = (uint16_t) MC_POS_DETECT_PULSE_LENGTH_INIT;
    mc_data.pos_detect_data.meas_time         = (uint16_t) MC_POS_DETECT_MEAS_TIME_INIT;
    mc_data.pos_detect_data.meas_delay        = (uint16_t) MC_POS_DETECT_MEAS_DELAY_INIT;
    mc_data.pos_detect_data.freerun_duration  = 1;
  #endif

  #if ( USR_EN_SINGLE_SHUNT_SILENT_MODE != 1 )
    mc_data.force_fixed_meas_window = false;
  #endif

  mc_data.state = MC_IDLE;
}

void mc_generate_struct_signature ( void )
{
  mc_data.struct_signature = U16PTR_TO_U16( &mc_data.struct_signature );
}

/* todo: docu */
INLINE static mc_bemf_meas_result_t mc_measure_bemf ( void )
{
  mc_bemf_meas_result_t loc_return_value = BEMF_MEAS_RESULT_UNDEF;

  /* disable the pwm outputs */
  pwm_set_all_outputs( false );

    while( adc_get_data()->vsup == (uint16_t) U16_MAX )
    {
    }

  mc_data.sync_data.bemf_starpoint = (int16_t) math_divS32_unsafe((s32) (uint16_t) ( adc_get_data()->phase_voltage_u + adc_get_data()->phase_voltage_v + adc_get_data()->phase_voltage_w ), 3, false );

  mc_data.sync_data.bemf_u = (int16_t) adc_get_data()->phase_voltage_u - mc_data.sync_data.bemf_starpoint;
  mc_data.sync_data.bemf_v = (int16_t) adc_get_data()->phase_voltage_v - mc_data.sync_data.bemf_starpoint;
  mc_data.sync_data.bemf_w = (int16_t) adc_get_data()->phase_voltage_w - mc_data.sync_data.bemf_starpoint;

  /* True: check direction of rotation; false: check for BEMF crossing */
  static bool_t sync_bemf_state = True;

  if( sync_bemf_state )
  {
    if( mc_data.sync_data.bemf_u > 0 )
    {
      sync_bemf_state = false;

      if( mc_data.sync_data.bemf_v < 0 )
      {
        mc_data.sync_data.rotor_direction = FORWARD;
      }
      else
      {
        mc_data.sync_data.rotor_direction = BACKWARD;
      }
    }
  }
  else
  {
    if( mc_data.sync_data.rotor_direction == FORWARD )
    {
      if(( mc_data.sync_data.bemf_w > mc_data.sync_data.bemf_v ) && ( mc_data.sync_data.bemf_v > 0 ) /* "&& ( mc_data.sync_data.bemf_w > 0 )"*/ )
      {
        sync_bemf_state = True;

        if( mc_data.sync_data.bemf_w > ADC_NOISE_AMPL )
        {
          mc_data.sync_data.bemf_ampl = ( mc_data.sync_data.bemf_v + mc_data.sync_data.bemf_w );
          mc_data.sync_data.crossing_evt_cntr++;
        }
        else
        {
          mc_data.sync_data.crossing_evt_cntr = 0u;
        }
      }
    }
    else /* "mc_data.sync_data.rotor_direction == BACKWARD" */
    {
      if(( mc_data.sync_data.bemf_w < mc_data.sync_data.bemf_v ) && ( mc_data.sync_data.bemf_v > 0 ) && ( mc_data.sync_data.bemf_w > 0 ))
      {
        sync_bemf_state = True;

        if( mc_data.sync_data.bemf_v > ADC_NOISE_AMPL )
        {
          mc_data.sync_data.bemf_ampl = ( mc_data.sync_data.bemf_v + mc_data.sync_data.bemf_w );
          mc_data.sync_data.crossing_evt_cntr++;
        }
        else
        {
          mc_data.sync_data.crossing_evt_cntr = 0u;
        }
      }
    }
  }

  spdmeas_set_voltage_uvw( mc_data.sync_data.bemf_u, mc_data.sync_data.bemf_v, mc_data.sync_data.bemf_w );

  /* evaluate/interpret the crossing events after a minimum amount of them having occurred */
  if(( spdmeas_get_cur_speed() > 0u ) && ( mc_data.sync_data.crossing_evt_cntr >= (u8) MC_ROTOR_SYNC_BEMF_CROSSING_MIN_CNT ))
  {
    /* one can scale up/down the amplitude value; however not recommended */
    math_mulS16_unsafe( mc_data.sync_data.bemf_ampl, mc_data.sync_data.bemf_ampl_gain );
    math_mul_shiftright_result( 8u );
    mc_data.sync_data.bemf_ampl = (int16_t) math_mul_get_result( false );

    mc_data.sync_data.crossing_evt_cntr = 0u;

    if( mc_data.sync_data.sync_permission )
    {
      /* rotor position must be detected sufficiently precisely (-> BEMF amplitude must be high enough) */
      if( mc_data.sync_data.bemf_ampl > mc_data.sync_data.bemf_ampl_min )
      {
        /* the BEMF amplitude must not be greater than half of the supply voltage in order to allow for successful sync */
        if(((uint16_t) abs( mc_data.sync_data.bemf_ampl ) < ( adc_get_data()->vsup >> 1 )))
        {
          if( mc_data.sync_data.rotor_direction == mc_data.direction )
          {
            loc_return_value = BEMF_DETECTED_AND_VALID;
          }
          else
          {
            loc_return_value = BEMF_FAIL_REVERSE_ROTATION;
          }
        }
        else
        {
          loc_return_value = BEMF_FAIL_EXCESSIVE_BEMF;
        }
      }
      else
      {
        /* BEMF detected but insufficient amplitude -> just let the control continue the monitoring until timeout */
      }
    }
    else
    {
      /* sync permission denied ->  just let the control continue the monitoring until timeout */
    }
  }

  return loc_return_value;
}

/* todo: docu
   when return value == false, the ADC list needs to be set here, otherwise the system is stuck (and consequently the watchdog will reset it) */
INLINE static bool_t mc_sync_state_machine_exec( void )
{
  static uint16_t loc_timestamp = 0u;
  bool_t loc_return_value    = false;

  switch( mc_data.sync_data.sync_state )
  {
    case ( MC_SYNC_STATE_DISABLED ):
    {
      if( !mc_data.meas_bemf )
      {
        loc_return_value = True;
      }
      else
      {
        #if ( FOC_EN_HALL_SENS == 1 )
          if( FOC_HALL_SENSOR == foc_get_rotor_angle_meas_mode())
          {
            mc_data.sync_data.sync_state       = MC_SYNC_STATE_WAIT_FOR_HALL_SIGNAL;
            mc_data.sync_data.sync_fail_reason = MC_SYNC_NO_FAIL;

            /* set new adc_list */
            adc_measurement_list_set_and_skip( adc_lists_get_commutation_list());
          }
          else if( FOC_SENSORLESS == foc_get_rotor_angle_meas_mode())
          {
        #endif
        mc_data.sync_data.sync_state        = MC_SYNC_STATE_MEASUREMENT_BEMF_ACTIVE;
        mc_data.sync_data.sync_fail_reason  = MC_SYNC_NO_FAIL;
        mc_data.sync_data.crossing_evt_cntr = 0u;

        /* set new adc_list */
        adc_clear_data();
        adc_measurement_list_set_and_skip( adc_lists_get_meas_bemf_list());

        #if ( FOC_EN_HALL_SENS == 1 )
      }
      else
      {
        /*Message 2013*/
      }
        #endif

        loc_return_value = false;
      }

      break;
    }
    case ( MC_SYNC_STATE_MEASUREMENT_BEMF_ACTIVE ):
    {
      /* set new adc_list */
      adc_clear_data();
      adc_measurement_list_set_and_skip( adc_lists_get_meas_bemf_list());

      /* mc_measure_bemf() returns a result != BEMF_MONITOR_RESULT_UNDEF when a BEMF signal had been detected */
      mc_bemf_meas_result_t loc_bemf_meas_result = mc_measure_bemf();

      if( !mc_data.meas_bemf )
      {
        mc_data.sync_data.sync_state = MC_SYNC_STATE_DISABLED;
      }
      else if( BEMF_DETECTED_AND_VALID == loc_bemf_meas_result )
      {
        foc_init_data_for_sync((uint16_t) mc_data.sync_data.bemf_ampl, (int16_t) adc_get_data()->vsup, mc_data.sync_data.rotor_direction );

        mc_data.sync_data.sync_state = MC_SYNC_STATE_COMMUTATION_ACTIVE;
        loc_timestamp                = systime_getU16();
      }
      else if( BEMF_FAIL_EXCESSIVE_BEMF == loc_bemf_meas_result )
      {
        mc_data.sync_data.sync_fail_reason = MC_SYNC_FAIL_EXCESSIVE_BEMF;
        mc_data.sync_data.sync_state       = MC_SYNC_STATE_FAILED;
      }
      else if( BEMF_FAIL_REVERSE_ROTATION == loc_bemf_meas_result )
      {
        mc_data.sync_data.sync_fail_reason = MC_SYNC_FAIL_REVERSE_ROTATION;
        mc_data.sync_data.sync_state       = MC_SYNC_STATE_FAILED;
      }
      else
      {
        /*Message 2013*/
      }

      loc_return_value = false;
      break;
    }
      #if ( FOC_EN_HALL_SENS == 1 )
        case ( MC_SYNC_STATE_WAIT_FOR_HALL_SIGNAL ):
        {
          /* set new adc_list */
          adc_measurement_list_set_and_skip( adc_lists_get_commutation_list());

          /* just let the hall module do its job without the outputs being enabled; once hall_interpolation_valid() is True, the rotor angle information is reliable */
          hall_calc_rotor_angle_unsafe();

          if( !mc_data.meas_bemf )
          {
            mc_data.sync_data.sync_state = MC_SYNC_STATE_DISABLED;

            spdmeas_reset_data();
          }
          else if( hall_interpolation_valid())
          {
            /* retrieve the rotor direction and convert it for the ensuing comparison */
            motor_direction_t loc_buf = ( hall_get_cur_direction() > 0 ) ? ( FORWARD ) : ( BACKWARD );

            if( loc_buf != mc_get_direction())
            {
              /* rotor is turning in the wrong direction .. */
              mc_data.sync_data.sync_fail_reason = MC_SYNC_FAIL_REVERSE_ROTATION;
              mc_data.sync_data.sync_state       = MC_SYNC_STATE_FAILED;
            }
            else
            {
              /* all OK - prepare to start commutating.. */
              /* 1) calc BEMF amplitude */
              math_mulU16_unsafe( hall_get_cur_speed(), foc_get_data()->param_motor_constant );
              math_mul_shiftright_result( 8u );

              /* 2) preload i_q controller so that the output voltage amplitude immediately matches the BEMF voltage amplitude and no current is flowing initially */
              foc_calc_i_q_ctrl_err_sum_preload_value((int16_t) math_mul_get_result( false ), loc_buf );

              mc_data.sync_data.sync_state = MC_SYNC_STATE_COMMUTATION_ACTIVE;
              loc_timestamp                = systime_getU16();
            }

            spdmeas_reset_data();
          }
          else
          {
            /*Message 2013*/
          }

          loc_return_value = false;

          break;
        }
      #endif
    case ( MC_SYNC_STATE_COMMUTATION_ACTIVE ):
    {
      if( !mc_data.meas_bemf )
      {
        mc_data.sync_data.sync_state = MC_SYNC_STATE_DISABLED;
      }
      else if(( systime_getU16() - loc_timestamp ) >= mc_data.sync_data.sync_duration )
      {
        mc_data.sync_data.sync_state = MC_SYNC_STATE_FINISHED;
      }
      else
      {
/*Message 2013*/
      }
      loc_return_value = True;
      break;
    }
    case ( MC_SYNC_STATE_FINISHED ):
    {
      if( !mc_data.meas_bemf )
      {
        mc_data.sync_data.sync_state = MC_SYNC_STATE_DISABLED;
      }

      loc_return_value = True;
      break;
    }
    case ( MC_SYNC_STATE_FAILED ):
    {
      /* set new adc_list */
      adc_clear_data();
      adc_measurement_list_set_and_skip( adc_lists_get_meas_bemf_list());

      if( !mc_data.meas_bemf )
      {
        mc_data.sync_data.sync_state = MC_SYNC_STATE_DISABLED;
      }

      loc_return_value = false;
      break;
    }
    default:
    {
      mc_data.sync_data.sync_state = MC_SYNC_STATE_DISABLED;
      loc_return_value             = True;
      break;
    }
  }

  return loc_return_value;
}

/*! @brief the ISR which prepares all input data for the FOC, executes the FOC and applies the output of the FOC to the various modules
 *
 * @details
 * This is the heart of the whole application! It is executed when an ADC list has been finished but since the trigger points in the ADC lists <br>
 * are PWM counter values, the execution is also synchronous to the output PWM. The execution of one ADC list always takes as long as one PWM <br>
 * period, thus this ISR also executed with the frequency of the output PWM. This is also used to generate a timebase (module 'systime'). <br>
 * It's no accident that the ISR is implemented right here, in module motor control, because it is teethed with the motor control state machine (see mc_main() ). <br>
 * Since the this state machine has a timebase of one millisecond (and is NOT on interrupt level), the interactions between ISR and state machine are <br>
 * very carefully woven. There are four flags (foc_enable, outputs_enable, meas_bemf, exec_speed_ctrl) which are ONLY read by the ISR and are ONLY written by the <br>
 * state machine. You should also never use 'mc_data.state' for conditional expressions in the ISR (unless you _really_ know what you are doing there). <br>
 * So, this is what's happening here (not necessarily in chronological order, since that can differ between single and dual shunt operation): <br>
 * - increment 'systime' counter <br>
 * - execute state machine for synchronizing to turning rotor <br>
 * - set new ADC list (thereby (re)activating the ADC) <br>
 * - retrieve recent phase current measurements <br>
 * - pass current output voltages to module 'spdmeas' <br>
 * - evaluate phase current offset/supply voltage measurements and update, if valid <br>
 * - compute FOC algorithm <br>
 * - apply FOC output voltages to PWM module (convert the FOC output to PWM compare values), thereby checking for over-/underflow <br>
 * - execute UART data logger, if enabled <br>
 * - execute speed controller, if enabled <br>
 * - compute DC link current, if enabled <br>
 * <br>
 * One little reminder with regard to the PWM output: always keep in mind which shunt configuration you are currently working with! In single shunt mode, the falling <br>
 * edges (pwm_c1) are at _fixed_ values at the beginning of the PWM period and the phase voltage is only produced by moving the rising edges (pwm_c0). In dual shunt mode, <br>
 * both values change and they are symmetrical with regard to PWMMAX / 2 = 1200 (in case of an output frequency of 20 kHz). <br>
 * <br>
 * Note: there's loads of inline documentation in the source code. Read it.
 */
INLINE void mc_isr_handler ( void )
{
  #if ( DBG_PIN3_FUNCTION == DBG_ISR_DURATION )
    dbg_pin3_set();
  #endif

  systime_incr();

  bool_t loc_exec_calculations = mc_sync_state_machine_exec();
  #if ( USR_EN_INITIAL_POSITION_DETECTION == 1 )
    _Pragma( "diag_suppress=Pm128" )                                                                                                                                                        /* cast internal int16_t_bool to bool_t */
    loc_exec_calculations = ( pos_detect_state_machine_exec( mc_data.exec_pos_detect )) && loc_exec_calculations;
    _Pragma( "diag_default=Pm128" )
  #endif

  if( loc_exec_calculations )
  {
    /* set new adc_list */
    adc_measurement_list_set_and_skip( adc_lists_get_commutation_list());

    #if ( USR_SHUNT_MEASUREMENT_TYPE == USR_SHUNT_MEAS_TYPE_SINGLE )

      /* enable or disable the pwm outputs (this is done here to always charge the bootstrap circuit upon start; all low-side switches are ON now) */
      pwm_set_all_outputs( mc_data.outputs_enable );

      /* save adc results: 620ns vs 887..1011ns */
      #if ( USR_EN_SINGLE_SHUNT_SILENT_MODE == 1 )
        /* order of falling edges: U -> V -> W: i1 = -i_u, i2 = i_w */
        foc_set_i_u( mc_data.curr1_offset_val - (int16_t) adc_get_data()->phase_curr1 );
        foc_set_i_w((int16_t) adc_get_data()->phase_curr2 - mc_data.curr1_offset_val );
        foc_set_i_v( -( foc_get_i_w() + foc_get_i_u()));
      #else
        if( MC_PHASE_U == mc_data.highest_phase )
        {
          /* order of falling edges: V -> W -> U: i1 = -i_v, i2 = i_u */
          foc_set_i_v( mc_data.curr1_offset_val - (int16_t) adc_get_data()->phase_curr1 );
          foc_set_i_u((int16_t) adc_get_data()->phase_curr2 - mc_data.curr1_offset_val );
          foc_set_i_w( -( foc_get_i_v() + foc_get_i_u()));
        }
        else if( MC_PHASE_V == mc_data.highest_phase )
        {
          /* order of falling edges: W -> U -> V: i1 = -i_w, i2 = i_v */
          foc_set_i_w( mc_data.curr1_offset_val - (int16_t) adc_get_data()->phase_curr1 );
          foc_set_i_v((int16_t) adc_get_data()->phase_curr2 - mc_data.curr1_offset_val );
          foc_set_i_u( -( foc_get_i_v() + foc_get_i_w()));
        }
        else                                                                                                                                                                                /* "MC_PHASE_W == mc_data.highest_phase" */
        {
          /* order of falling edges: U -> V -> W: i1 = -i_u, i2 = i_w */
          foc_set_i_u( mc_data.curr1_offset_val - (int16_t) adc_get_data()->phase_curr1 );
          foc_set_i_w((int16_t) adc_get_data()->phase_curr2 - mc_data.curr1_offset_val );
          foc_set_i_v( -( foc_get_i_w() + foc_get_i_u()));
        }
      #endif

    #elif ( USR_SHUNT_MEASUREMENT_TYPE == USR_SHUNT_MEAS_TYPE_DUAL_U_V )

      /* enable or disable the pwm outputs (this is done here to always charge the bootstrap circuit upon start; all low-side switches are ON now) */
      pwm_set_all_outputs( mc_data.outputs_enable );

      if( mc_data.outputs_enable )
      {
        /* set values for next edges */
        pwm_set_c1_reload_values(( uint16_t ) mc_data.pwm0_cmp_val, (uint16_t) mc_data.pwm1_cmp_val, (uint16_t) mc_data.pwm2_cmp_val );                                                                    /* falling edges */
        pwm_set_c0_reload_values((uint16_t) (int16_t) ((int16_t) PWM_PERIOD_DURATION_TICKS - mc_data.pwm0_cmp_val ),
                                 (uint16_t) (int16_t) ((int16_t) PWM_PERIOD_DURATION_TICKS - mc_data.pwm1_cmp_val ),
                                 (uint16_t) (int16_t) ((int16_t) PWM_PERIOD_DURATION_TICKS - mc_data.pwm2_cmp_val ));                                                                                    /* rising edges */
      }

      foc_set_i_u( -((int16_t) adc_get_data()->phase_curr1 - mc_data.curr1_offset_val ));
      foc_set_i_v( -((int16_t) adc_get_data()->phase_curr2 - mc_data.curr2_offset_val ));
      foc_set_i_w( -( foc_get_i_u() + foc_get_i_v()));

    #elif  ( USR_SHUNT_MEASUREMENT_TYPE == USR_SHUNT_MEAS_TYPE_DUAL_V_W )

      /* enable or disable the pwm outputs (this is done here to always charge the bootstrap circuit upon start; all low-side switches are ON now) */
      pwm_set_all_outputs( mc_data.outputs_enable );

      if( mc_data.outputs_enable )
      {
        /* set values for next edges */
        pwm_set_c1_reload_values((uint16_t) mc_data.pwm0_cmp_val, (uint16_t) mc_data.pwm1_cmp_val, (uint16_t) mc_data.pwm2_cmp_val );                                                                      /* falling edges */
        pwm_set_c0_reload_values((uint16_t) (int16_t) ((int16_t) PWM_PERIOD_DURATION_TICKS - mc_data.pwm0_cmp_val ),
                                 (uint16_t) (int16_t) ((int16_t) PWM_PERIOD_DURATION_TICKS - mc_data.pwm1_cmp_val ),
                                 (uint16_t) (int16_t) ((int16_t) PWM_PERIOD_DURATION_TICKS - mc_data.pwm2_cmp_val ));                                                                                    /* rising edges */
      }

      foc_set_i_v( -((int16_t) adc_get_data()->phase_curr1 - mc_data.curr1_offset_val ));
      foc_set_i_w( -((int16_t) adc_get_data()->phase_curr2 - mc_data.curr2_offset_val ));
      foc_set_i_u( -( foc_get_i_v() + foc_get_i_w()));

    #else
    #error USR_SHUNT_MEASUREMENT_TYPE not supported!
    #endif

    /* the order of PWM thresholds (i.e. position in the space vector hexagon) is tracked for measuring the rotor speed */
    spdmeas_set_voltage_uvw( foc_get_v_u(), foc_get_v_v(), foc_get_v_w());

    /* supply voltage and current measurement offset value(s) are evaluated in the following block */
    if( mc_data.outputs_enable )
    {

        #if ( DBG_CONST_VBAT_MV != 0u )
          foc_set_supply_voltage_unsafe((int16_t) ((s64) USR_PHASE_VOLTAGE_DIVIDER_RATIO * (s64) ADC_MAX_VAL * (s64) DBG_CONST_VBAT_MV / (s64) ADC_REF_VOLTAGE_MV ));
        #else
          foc_set_supply_voltage_unsafe((int16_t) adc_get_data()->vsup );
        #endif

      /* retrieve the current measurement offset values, if valid (i.e. no current was flowing through the respective shunt resistor at the time of measurement) */
      /* first up: phase 1 (dual shunt) or sum current (single shunt) */
      #if ( USR_SHUNT_MEASUREMENT_TYPE == USR_SHUNT_MEAS_TYPE_SINGLE )
        if( mc_data.max_cmp_val <= (int16_t) ( PWM_SINGLE_SHUNT_MAX_CMP_VAL - ( INT64_C( 4 ) * ( ADC_CONVERSION_CLK_CYCLES + ADC_SAMPLE_CLK_CYCLES )) - ADC_SAMPLE_DELAY_SHUNT_VOLTAGE_TICKS )) /* ensuring that all phases are switched on so that no current is flowing through the sum shunt; obviously, the value depends on the measurement timing in adc_lists_single_shunt */
      #elif ( USR_SHUNT_MEASUREMENT_TYPE == USR_SHUNT_MEAS_TYPE_DUAL_U_V )
        if( mc_data.pwm0_cmp_val >= ((int16_t) ( PWM_PERIOD_DURATION_TICKS ) / 4 ))                                                                                                             /* phase U has a high duty cycle, therefore plenty of time to measure */
      #elif ( USR_SHUNT_MEASUREMENT_TYPE == USR_SHUNT_MEAS_TYPE_DUAL_V_W )
        if( mc_data.pwm1_cmp_val >= ((int16_t) ( PWM_PERIOD_DURATION_TICKS ) / 4 ))                                                                                                             /* phase V has a high duty cycle, therefore plenty of time to measure */
      #else
      #error USR_SHUNT_MEASUREMENT_TYPE not supported!
      #endif
      {
        /* the offset of the current measurement circuitry varies mainly over temperature -> very slowly ... */
        if( mc_data.curr1_offset_val > (int16_t) adc_get_data()->phase_curr1_offset ){mc_data.curr1_offset_val--; }
        else if( mc_data.curr1_offset_val < (int16_t) adc_get_data()->phase_curr1_offset ){mc_data.curr1_offset_val++; }
        else{ /*Message 2013*/}
      }

      /* secondly: phase 2 (dual shunt) */
      #if ( USR_SHUNT_MEASUREMENT_TYPE == USR_SHUNT_MEAS_TYPE_SINGLE )
      #elif ( USR_SHUNT_MEASUREMENT_TYPE == USR_SHUNT_MEAS_TYPE_DUAL_U_V )
        if( mc_data.pwm1_cmp_val >= ((int16_t) ( PWM_PERIOD_DURATION_TICKS ) / 4 ))                                                                                                             /* phase V has a high duty cycle, therefore plenty of time to measure */
      #elif ( USR_SHUNT_MEASUREMENT_TYPE == USR_SHUNT_MEAS_TYPE_DUAL_V_W )
        if( mc_data.pwm2_cmp_val >= ((int16_t) ( PWM_PERIOD_DURATION_TICKS ) / 4 ))                                                                                                             /* phase W has a high duty cycle, therefore plenty of time to measure */
      #else
      #error USR_SHUNT_MEASUREMENT_TYPE not supported!
      #endif
      {
        /* the offset of the current measurement circuitry varies mainly over temperature -> very slowly ... */
        if( mc_data.curr2_offset_val > (int16_t) adc_get_data()->phase_curr2_offset ){mc_data.curr2_offset_val--; }
        else if( mc_data.curr2_offset_val < (int16_t) adc_get_data()->phase_curr2_offset ){mc_data.curr2_offset_val++; }
        else{ /*Message 2013*/}
      }
    }
    else
    {
      mc_data.curr1_offset_val = (int16_t) adc_get_data()->phase_curr1_offset;
      mc_data.curr2_offset_val = (int16_t) adc_get_data()->phase_curr2_offset;

        #if ( DBG_CONST_VBAT_MV != 0u )
          foc_set_supply_voltage_unsafe((int16_t) ((s64) USR_PHASE_VOLTAGE_DIVIDER_RATIO * (s64) ( ADC_MAX_VAL ) *(s64) DBG_CONST_VBAT_MV / (s64) ADC_REF_VOLTAGE_MV ));
        #else
          foc_set_supply_voltage_unsafe((int16_t) adc_get_data()->vsup );
        #endif
    }

    /* the actual, mathematical computation of the FOC algorithm... */
    if( mc_data.foc_enable )
    {
      foc_full_foc_calc();
    }
    else
    {
      foc_set_v_u( 0 );
      foc_set_v_v( 0 );
      foc_set_v_w( 0 );
    }

    /* now configure the PWM output according to the results of the FOC computation */
    #if ( USR_SHUNT_MEASUREMENT_TYPE == USR_SHUNT_MEAS_TYPE_SINGLE )
      #if ( USR_EN_SINGLE_SHUNT_SILENT_MODE == 1 )
        /* this way, the system acts as usual... */
        const int16_t loc_offset_u_rising = (int16_t) ( ADC_SECOND_PWM_EDGE - ADC_FIRST_PWM_EDGE ),
                  loc_offset_v_rising = 0,
                  loc_offset_w_rising = -(int16_t) ( ADC_THIRD_PWM_EDGE - ADC_SECOND_PWM_EDGE );
      #else
        /* ... and here, the measurement window is adaptive for maximizing the amplitude */
        /* first off, determine the highest phase voltage; 300 .. 800ns -> 300ns except for 6 times per electrical revolutions (-> hexagon sector change) */
        static int16_t loc_last_voltage_order_change_evnt_cntr = 0;

        if(( loc_last_voltage_order_change_evnt_cntr != spdmeas_get_data()->phase_voltage_order_change_evnt_cntr ) &&
           ( !mc_data.force_fixed_meas_window ))
        {
          u8 loc_current_ph_volt_order = (u8) ( spdmeas_get_volt_uvw_pattern() & (uint16_t) 0x07 );

          if(( 2u == loc_current_ph_volt_order ) || ( 3u == loc_current_ph_volt_order ))
          {
            mc_data.highest_phase = MC_PHASE_W;
          }
          else if(( 1u == loc_current_ph_volt_order ) || ( 5u == loc_current_ph_volt_order ))
          {
            mc_data.highest_phase = MC_PHASE_V;
          }
          else
          {
            mc_data.highest_phase = MC_PHASE_U;
          }

          loc_last_voltage_order_change_evnt_cntr = spdmeas_get_data()->phase_voltage_order_change_evnt_cntr;
        }

        /* now set the measurement window in a way that the highest phase voltage is the last falling edge of the measurement window */
        int16_t loc_offset_u_rising, loc_offset_v_rising, loc_offset_w_rising;
        uint16_t loc_cmp_u_falling, loc_cmp_v_falling, loc_cmp_w_falling;

        if( MC_PHASE_U == mc_data.highest_phase )
        {
          /* order of falling edges: V -> W -> U */
          loc_cmp_u_falling = (uint16_t) ADC_THIRD_PWM_EDGE;
          loc_cmp_v_falling = (uint16_t) ADC_FIRST_PWM_EDGE;
          loc_cmp_w_falling = (uint16_t) ADC_SECOND_PWM_EDGE;

          loc_offset_v_rising = (int16_t) ( ADC_SECOND_PWM_EDGE - ADC_FIRST_PWM_EDGE );
          loc_offset_w_rising = 0;
          loc_offset_u_rising = -(int16_t) ( ADC_THIRD_PWM_EDGE - ADC_SECOND_PWM_EDGE );
        }
        else if( MC_PHASE_V == mc_data.highest_phase )
        {
          /* order of falling edges: W -> U -> V */
          loc_cmp_u_falling = (uint16_t) ADC_SECOND_PWM_EDGE;
          loc_cmp_v_falling = (uint16_t) ADC_THIRD_PWM_EDGE;
          loc_cmp_w_falling = (uint16_t) ADC_FIRST_PWM_EDGE;

          loc_offset_w_rising = (int16_t) ( ADC_SECOND_PWM_EDGE - ADC_FIRST_PWM_EDGE );
          loc_offset_u_rising = 0;
          loc_offset_v_rising = -(int16_t) ( ADC_THIRD_PWM_EDGE - ADC_SECOND_PWM_EDGE );
        }
        else                                                                                                                                                                                /* MC_PHASE_W == mc_data.highest_phase  */
        {
          /* order of falling edges: U -> V -> W */
          loc_cmp_u_falling = (uint16_t) ADC_FIRST_PWM_EDGE;
          loc_cmp_v_falling = (uint16_t) ADC_SECOND_PWM_EDGE;
          loc_cmp_w_falling = (uint16_t) ADC_THIRD_PWM_EDGE;

          loc_offset_u_rising = (int16_t) ( ADC_SECOND_PWM_EDGE - ADC_FIRST_PWM_EDGE );
          loc_offset_v_rising = 0;
          loc_offset_w_rising = -(int16_t) ( ADC_THIRD_PWM_EDGE - ADC_SECOND_PWM_EDGE );
        }
      #endif

      /* don't get confused -> the higher the voltage value, the lower is the compare value (because the compare value describes the rising edge and the counter is running forward...) */
      /* the second term of this subtraction describes the offsets for the measurement windows which have been created with the falling edges -> compensate them with the rising edges */
      mc_data.pwm0_cmp_val = (int16_t) ( PWM_SINGLE_SHUNT_CMP_MEDIAN ) -foc_get_v_u() - loc_offset_u_rising;
      mc_data.min_cmp_val  = mc_data.pwm0_cmp_val;
      mc_data.max_cmp_val  = mc_data.pwm0_cmp_val;

      mc_data.pwm1_cmp_val = (int16_t) ( PWM_SINGLE_SHUNT_CMP_MEDIAN ) -foc_get_v_v() - loc_offset_v_rising;                                                                                    /* PRQA S 2985 */ /* justification: loc_offset_v_rising is a variable when (USR_EN_SINGLE_SHUNT_SILENT_MODE == 0) */
      if( mc_data.pwm1_cmp_val < mc_data.min_cmp_val ){mc_data.min_cmp_val = mc_data.pwm1_cmp_val; }
      if( mc_data.pwm1_cmp_val > mc_data.max_cmp_val ){mc_data.max_cmp_val = mc_data.pwm1_cmp_val; }

      mc_data.pwm2_cmp_val = (int16_t) ( PWM_SINGLE_SHUNT_CMP_MEDIAN ) -foc_get_v_w() - loc_offset_w_rising;
      if( mc_data.pwm2_cmp_val < mc_data.min_cmp_val ){mc_data.min_cmp_val = mc_data.pwm2_cmp_val; }
      if( mc_data.pwm2_cmp_val > mc_data.max_cmp_val ){mc_data.max_cmp_val = mc_data.pwm2_cmp_val; }

      /* check if compare values violate their limits (which are imposed by the PWM module and the ADC current measurements) */
      if( mc_data.min_cmp_val < (int16_t) PWM_SINGLE_SHUNT_MIN_CMP_VAL )
      {
        int16_t loc_underflow_ticks = (int16_t) PWM_SINGLE_SHUNT_MIN_CMP_VAL - mc_data.min_cmp_val;

        mc_data.pwm0_cmp_val += loc_underflow_ticks;
        if( mc_data.pwm0_cmp_val > (int16_t) ( PWM_SINGLE_SHUNT_MAX_CMP_VAL )){mc_data.pwm0_cmp_val = (int16_t) ( PWM_SINGLE_SHUNT_MAX_CMP_VAL ); mc_data.pwm_cmp_val_overflow_flag = True; }

        mc_data.pwm1_cmp_val += loc_underflow_ticks;
        if( mc_data.pwm1_cmp_val > (int16_t) ( PWM_SINGLE_SHUNT_MAX_CMP_VAL )){mc_data.pwm1_cmp_val = (int16_t) ( PWM_SINGLE_SHUNT_MAX_CMP_VAL ); mc_data.pwm_cmp_val_overflow_flag = True; }

        mc_data.pwm2_cmp_val += loc_underflow_ticks;
        if( mc_data.pwm2_cmp_val > (int16_t) ( PWM_SINGLE_SHUNT_MAX_CMP_VAL )){mc_data.pwm2_cmp_val = (int16_t) ( PWM_SINGLE_SHUNT_MAX_CMP_VAL ); mc_data.pwm_cmp_val_overflow_flag = True; }
      }
      else if( mc_data.max_cmp_val > (int16_t) ( PWM_SINGLE_SHUNT_MAX_CMP_VAL ))
      {
        int16_t loc_overflow_ticks = mc_data.max_cmp_val - (int16_t) PWM_SINGLE_SHUNT_MAX_CMP_VAL;

        mc_data.pwm0_cmp_val -= loc_overflow_ticks;
        if( mc_data.pwm0_cmp_val < (int16_t) ( PWM_SINGLE_SHUNT_MIN_CMP_VAL )){mc_data.pwm0_cmp_val = (int16_t) ( PWM_SINGLE_SHUNT_MIN_CMP_VAL ); mc_data.pwm_cmp_val_overflow_flag = True; }

        mc_data.pwm1_cmp_val -= loc_overflow_ticks;
        if( mc_data.pwm1_cmp_val < (int16_t) ( PWM_SINGLE_SHUNT_MIN_CMP_VAL )){mc_data.pwm1_cmp_val = (int16_t) ( PWM_SINGLE_SHUNT_MIN_CMP_VAL ); mc_data.pwm_cmp_val_overflow_flag = True; }

        mc_data.pwm2_cmp_val -= loc_overflow_ticks;
        if( mc_data.pwm2_cmp_val < (int16_t) ( PWM_SINGLE_SHUNT_MIN_CMP_VAL )){mc_data.pwm2_cmp_val = (int16_t) ( PWM_SINGLE_SHUNT_MIN_CMP_VAL ); mc_data.pwm_cmp_val_overflow_flag = True; }
      }
      else{ /*Message 2013*/}

      if( mc_data.outputs_enable )
      {
        /* set c0 reload values for next rising edges (after CNT has reached 0 again...) */
        pwm_set_c0_reload_values((uint16_t) mc_data.pwm0_cmp_val, (uint16_t) mc_data.pwm1_cmp_val, (uint16_t) mc_data.pwm2_cmp_val );

        /* setting measurement window */
        #if ( USR_EN_SINGLE_SHUNT_SILENT_MODE == 1 )
          /* order of falling edges: U -> V -> W */
          pwm_set_c1_reload_values((uint16_t) ADC_FIRST_PWM_EDGE, (uint16_t) ADC_SECOND_PWM_EDGE, (uint16_t) ADC_THIRD_PWM_EDGE );
        #else
          pwm_set_c1_reload_values( loc_cmp_u_falling, loc_cmp_v_falling, loc_cmp_w_falling );
        #endif
      }

    #elif (( USR_SHUNT_MEASUREMENT_TYPE == USR_SHUNT_MEAS_TYPE_DUAL_U_V ) || ( USR_SHUNT_MEASUREMENT_TYPE == USR_SHUNT_MEAS_TYPE_DUAL_V_W ))

      /* here, everything is quite intuitive: the higer the voltage, the higher the compare value; the voltages are halved so that they are scaled in the same way as
       * in the single-shunt version (thereby, the configuration of the i_d/i_q-controller behaves in the same way, no matter what shunt configuration you use) */
      mc_data.pwm0_cmp_val = SHR_S16( foc_get_v_u(), 1u ) + (int16_t) ( PWM_DUAL_SHUNT_CMP_MEDIAN );
      mc_data.min_cmp_val  = (int16_t) mc_data.pwm0_cmp_val;
      mc_data.max_cmp_val  = (int16_t) mc_data.pwm0_cmp_val;

      mc_data.pwm1_cmp_val = SHR_S16( foc_get_v_v(), 1u ) + (int16_t) ( PWM_DUAL_SHUNT_CMP_MEDIAN );
      if( mc_data.pwm1_cmp_val < mc_data.min_cmp_val ){mc_data.min_cmp_val = (int16_t) mc_data.pwm1_cmp_val; }
      if( mc_data.pwm1_cmp_val > mc_data.max_cmp_val ){mc_data.max_cmp_val = (int16_t) mc_data.pwm1_cmp_val; }

      mc_data.pwm2_cmp_val = SHR_S16( foc_get_v_w(), 1u ) + (int16_t) ( PWM_DUAL_SHUNT_CMP_MEDIAN );
      if( mc_data.pwm2_cmp_val < mc_data.min_cmp_val ){mc_data.min_cmp_val = (int16_t) mc_data.pwm2_cmp_val; }
      if( mc_data.pwm2_cmp_val > mc_data.max_cmp_val ){mc_data.max_cmp_val = (int16_t) mc_data.pwm2_cmp_val; }

      /* check if compare values violate their limits (which are imposed by the PWM module and the ADC current measurements) */
      if( mc_data.max_cmp_val > (int16_t) ( PWM_DUAL_SHUNT_MAX_CMP_VAL ))
      {
        int16_t loc_overflow_ticks = mc_data.max_cmp_val - (int16_t) PWM_DUAL_SHUNT_MAX_CMP_VAL;

        mc_data.pwm0_cmp_val -= loc_overflow_ticks;
        if( mc_data.pwm0_cmp_val < (int16_t) PWM_DUAL_SHUNT_MIN_CMP_VAL ){mc_data.pwm0_cmp_val = (int16_t) PWM_DUAL_SHUNT_MIN_CMP_VAL; mc_data.pwm_cmp_val_overflow_flag = True; }

        mc_data.pwm1_cmp_val -= loc_overflow_ticks;
        if( mc_data.pwm1_cmp_val < (int16_t) PWM_DUAL_SHUNT_MIN_CMP_VAL ){mc_data.pwm1_cmp_val = (int16_t) PWM_DUAL_SHUNT_MIN_CMP_VAL; mc_data.pwm_cmp_val_overflow_flag = True; }

        mc_data.pwm2_cmp_val -= loc_overflow_ticks;
        if( mc_data.pwm2_cmp_val < (int16_t) PWM_DUAL_SHUNT_MIN_CMP_VAL ){mc_data.pwm2_cmp_val = (int16_t) PWM_DUAL_SHUNT_MIN_CMP_VAL; mc_data.pwm_cmp_val_overflow_flag = True; }
      }
      else if( mc_data.min_cmp_val < (int16_t) PWM_DUAL_SHUNT_MIN_CMP_VAL )
      {
        int16_t loc_underflow_ticks = (int16_t) PWM_DUAL_SHUNT_MIN_CMP_VAL - mc_data.min_cmp_val;

        mc_data.pwm0_cmp_val += loc_underflow_ticks;
        if( mc_data.pwm0_cmp_val > (int16_t) PWM_DUAL_SHUNT_MAX_CMP_VAL ){mc_data.pwm0_cmp_val = (int16_t) PWM_DUAL_SHUNT_MAX_CMP_VAL; mc_data.pwm_cmp_val_overflow_flag = True; }

        mc_data.pwm1_cmp_val += loc_underflow_ticks;
        if( mc_data.pwm1_cmp_val > (int16_t) PWM_DUAL_SHUNT_MAX_CMP_VAL ){mc_data.pwm1_cmp_val = (int16_t) PWM_DUAL_SHUNT_MAX_CMP_VAL; mc_data.pwm_cmp_val_overflow_flag = True; }

        mc_data.pwm2_cmp_val += loc_underflow_ticks;
        if( mc_data.pwm2_cmp_val > (int16_t) PWM_DUAL_SHUNT_MAX_CMP_VAL ){mc_data.pwm2_cmp_val = (int16_t) PWM_DUAL_SHUNT_MAX_CMP_VAL; mc_data.pwm_cmp_val_overflow_flag = True; }
      }
      else
      {
        /*Message 2013*/
      }
    #endif

    if( mc_data.pwm_cmp_val_overflow_flag )
    {
      #if ( USR_OVERRIDE_VOLTAGE_OVERFLOW_HANDLER == 1 )
        usr_output_voltage_overflow_handler();
      #else
        mc_output_voltage_overflow_handler();
      #endif
    }
  }

  #if ( SCI_USAGE_TYPE == SCI_DATALOGGER )
    uart_datalogger_exec();
    /* 1.74us */
  #endif

  /* re-initialize the flag for next iteration of this ISR */
  mc_data.pwm_cmp_val_overflow_flag = false;

  if( mc_data.exec_speed_ctrl )
  {
    mc_control_speed_unsafe();
    /* 2.6us */
  }

  #if ( MC_COMPUTE_SUPPLY_CURRENT == 1 )
    /* compute the current drawn from VBAT */
    mc_supply_current_calc_unsafe();
    /* 2.57us */
  #endif

  #if ( DBG_PIN3_FUNCTION == DBG_ISR_DURATION )
    dbg_pin3_clear();
  #endif
}

/*! @brief handler function for FOC output voltage overflow
 *
 * @details
 * The FOC calulation can produce output voltages (i.e. PWM duty cycles) which are impossible to <br>
 * produce, therefore this handler provides a countermeasure. It's always called when this condition <br>
 * occurs and hence needs to implement measures (decreasing i_d/i_q, decreasing target speed/torque, ...) which <br>
 * prevent the condition from persisting. <br>
 * By setting define 'USR_OVERRIDE_VOLTAGE_OVERFLOW_HANDLER' you can (and should!) implement a handler function yourself.<br>
 * NOTE: this function is called on interrupt level!
 */
INLINE void mc_output_voltage_overflow_handler ( void )
{
  /* decrease output of speed controller and let it roll again */
  mc_data.controller_rotor_speed.err_sum -= SHR_S32( mc_data.controller_rotor_speed.err_sum, 8u );

  int16_t loc_buf = SHR_S16( mc_data.controller_rotor_speed.set_value, 8u );

  if( mc_data.controller_rotor_speed.set_value > 0 )
  {
    mc_data.controller_rotor_speed.set_value -= ( 0 == loc_buf ) ? ( 1 ) : ( loc_buf );
  }
  else
  {
    mc_data.controller_rotor_speed.set_value -= ( 0 == loc_buf ) ? ( -1 ) : ( loc_buf );
  }

  if( mc_get_direction() == FORWARD )
  {
    foc_set_i_q_ref( mc_data.controller_rotor_speed.set_value );
  }
  else
  {
    foc_set_i_q_ref( -mc_data.controller_rotor_speed.set_value );
  }

}

/*! @brief handler function for FOC detection of a stalled rotor
 *
 * @details
 * The FOC continuously checks for a stalled rotor by comparing the BEMF amplitude with the value computed from <br>
 * speed and motor constant. If this mechanism triggers, this handler function is called to set <br>
 * flag 'mc_data.stall_detected', which can be processed (read-only) by the main loop execution level.
 * NOTE: this function is called on interrupt level!
 */
INLINE void mc_stalled_rotor_condition_handler ( void )
{
  #if ( DBG_PIN3_FUNCTION == DBG_SHOW_STALLED_ROTOR )
    dbg_pin3_set();
  #endif

  mc_data.stall_detected = True;
}

/*! @brief get-function for state of motor control state machine */
INLINE mc_motor_state_t mc_get_motor_state ( void )
{
  return mc_data.state;
}

/*! @brief get-function for momentary commutation mode (open/closed loop) */
INLINE mc_motor_comm_mode_t mc_get_comm_mode ( void )
{
  return mc_data.commutation_mode;
}

/*! @brief calculation of the angle increments during the speed ramp (open loop)
 *
 * @details
 * The FOC calls this function everytime (i.e. every 1/(20 kHz) = 50us) during state SPEED_RAMP in <br>
 * order to calculate the angle increment for the open-loop speed ramp. <br>
 * The value that is returned by this function is simply added to the momentary angle which is used to <br>
 * calculate the FOC. By constantly increasing this return value, the speed ramp is generated. <br>
 * If 'startup.speed_slope' is smaller than 0, the value acts as an prescaler, i.e. every 'startup.speed_slope' <br>
 * calls to this function, the angle increment is increased by 1. If 'startup.speed_slope' is greater than 0, the angle increment <br>
 * is increased by this value with every call to this function. (brain teaser: 1 and -1 produce the same speed ramp ;-) )
 */
INLINE uint16_t mc_get_startup_angle_incr ( void )
{
  static int16_t loc_cnt_slope = 0;

  if( mc_data.startup.speed_slope < 0 )
  {
    /* this branch means that startup_speed_slope describes a prescaler -> every startup_speed_slope calls of this function, startup_angle_incr is increased by 1 */
    loc_cnt_slope--;

    /* note: the obvious case of overflowing 'startup_angle_inc' is out of question here because that would mean a full electrical revolution during one PWM period ... */
    if( loc_cnt_slope <= mc_data.startup.speed_slope )
    {
      mc_data.startup.angle_incr++;
      loc_cnt_slope = 0;
    }
  }
  else
  {
    /* this branch means that startup_speed_slope describes an increment value -> with every call of this function, startup_angle_incr is increased by startup_speed_slope */

    /* note: the obvious case of overflowing 'startup_angle_inc' is out of question here because that would mean a full electrical revolution during one PWM period ... */
    mc_data.startup.angle_incr += (uint16_t) mc_data.startup.speed_slope;
  }

  if(( mc_data.remain_in_open_loop ) && ( mc_data.startup.angle_incr > (uint16_t) mc_data.startup.speed_ramp_end ))
  {
    mc_data.startup.angle_incr = (uint16_t) mc_data.startup.speed_ramp_end;
  }

  return mc_data.startup.angle_incr;
}

/*! @brief set-function for 'target_speed' */
INLINE void mc_set_target_rotor_speed ( uint16_t target_speed )
{
  mc_data.target_rotor_speed = target_speed;
}

void mc_set_speed_ctrl_kp(int16_t speed_control_kp)
{
    mc_data.controller_rotor_speed.Kp = speed_control_kp;
}

void mc_set_speed_ctrl_ki(uint16_t speed_control_ki)
{
    mc_data.controller_rotor_speed.Ki = speed_control_ki;
}

void mc_set_speed_ctrl_timebase(uint16_t speed_control_timebase)
{
    mc_data.rotor_speed_ctrl_time_base = speed_control_timebase;
}

/*! @brief set-function for output saturation (minimum) of the speed controller */
INLINE void mc_set_speed_ctrl_output_min( int16_t output_min )
{
  mc_data.speed_ctrl_output_min = output_min;
}

/*! @brief set-function for output saturation (maximum) of the speed controller */
INLINE void mc_set_speed_ctrl_output_max( int16_t output_max )
{
  mc_data.speed_ctrl_output_max = output_max;
}

/*! @brief set-function for 'i_q_ref' during startup phase */
INLINE void mc_set_startup_i_q_ref ( int16_t startup_i_q_ref )
{
  mc_data.startup.i_q_ref = startup_i_q_ref;
}

void mc_set_startup_current_slope(int16_t startup_current_slope)
{
    mc_data.startup.current_slope = startup_current_slope;
}

void mc_set_startup_speed_slope(int16_t startup_speed_slope)
{
    mc_data.startup.speed_slope = startup_speed_slope;
}

void mc_set_startup_speed_ramp_start(uint16_t speed_ramp_start)
{
    mc_data.startup.speed_ramp_start = speed_ramp_start;
}

void mc_set_startup_speed_ramp_end(uint16_t speed_ramp_end)
{
    mc_data.startup.speed_ramp_end = speed_ramp_end;
}

void mc_set_startup_align_rotor_duration(uint16_t align_rotor_duration)
{
    mc_data.startup.align_rotor_duration = align_rotor_duration;
}

void mc_set_remain_in_open_loop(bool_t enable)
{
    mc_data.remain_in_open_loop = enable;
}

/*! @brief set-function for 'force_fixed_meas_window' which controls the switching between fixed and adaptive measurement window when USR_EN_SINGLE_SHUNT_SILENT_MODE != 1
 *
 * @details
 * The ELMOS single shunt FOC (Field Oriented Control) has two modulation methods:<br>
 * For USR_EN_SINGLE_SHUNT_SILENT_MODE=1 each of the three left-aligned PWM output voltages has a fixed delay. This allows for fixed measurement windows for the motor phase currents and enables silent operation of the PMSM motor.<br>
 * For USR_EN_SINGLE_SHUNT_SILENT_MODE=0 the order of the output voltages is changed every 120 degrees electrical. This enables full modulation of the highest phase voltage at any given moment, providing ca. 10% more output voltage amplitude to the PMSM motor.<br>
 * However, swapping the PWM switching order as per USR_EN_SINGLE_SHUNT_SILENT_MODE=0 creates disturbances of the phase current waveform and thus acoustic noise. It is therefore desirable to switch between the two operation modes depending on the motor load and speed.<br>
 * This is possible via mc_data.force_fixed_meas_window which, when set, inhibits the swapping of the PWM switching order when USR_EN_SINGLE_SHUNT_SILENT_MODE=0.
 * <br>
 * For commercial usage of the software feature 'mc_data.force_fixed_meas_window' a license regarding the patent family EP1589650B2 is mandatory. For the usage of our products E523.06A and / or E523.52 without this software feature such a license is not required.<br>
 */
INLINE void mc_force_fixed_meas_window ( bool_t enable ) /* PRQA S 3206 */ /* justification: parameter is only available when USR_EN_SINGLE_SHUNT_SILENT_MODE is set */
{
  #if ( USR_EN_SINGLE_SHUNT_SILENT_MODE != 1 )
    mc_data.force_fixed_meas_window = enable;
  #endif
}

/* }@
 */
