/**
 * @ingroup HW
 *
 * @{
 */

/**********************************************************************/
/*! @file adc_lists.c
 * @brief ADC module lists
 *
 * @details
 * contains the lists that configure the ADC measurements<br>
 * module prefix: adc_
 *
 * @author       JBER
 */
/**********************************************************************/
/* Demo Code Usage Restrictions:
 * Elmos Semiconductor AG provides this source code file simply and solely for IC evaluation purposes in laboratory and
 * this file must not be used for other purposes or within non laboratory environments. Especially, the use or the integration in
 * production systems, appliances or other installations is prohibited.
 *
 * Disclaimer:
 * Elmos Semiconductor AG shall not be liable for any damages arising out of defects resulting from (1) delivered hardware or software,
 * (2) non observance of instructions contained in this document, or (3) misuse, abuse, use under abnormal conditions or alteration by anyone
 * other than Elmos Semiconductor AG. To the extend permitted by law Elmos Semiconductor AG hereby expressively disclaims and user expressively
 * waives any and all warranties of merchantability and of fitness for a particular purpose, statutory warranty of non-infringement and any other
 * warranty or product liability that may arise by reason of usage of trade, custom or course of dealing.
 */
/**********************************************************************/

#include "adc.h"
#include "adc_lists.h"
#include "debug.h"
#include "global.h"

/* -------------------
   global declarations
   ------------------- */

#define ADC_500_NANO_SECONDS ((uint16_t) ( 0.5 * (dbl) (uint16_t) F_CPU_MHZ ))

/* PRQA S 3218 ++ */ /* justification: tables would decrease readability if they were included in functions */
/* PRQA S 0306 ++ */ /* justification: cast from pointer to integral type -> needed for setting target addresses of AD conversions */
_Pragma( "diag_suppress=Pm140,Pm152" )

/**
   @var adc_lists_meas_bemf
   @brief list is used during synchronization to a turning rotor
 */
static saradc_ctrl_list_entry_t adc_lists_meas_bemf[] =
{
  #if ( ADC_USER_CHANNEL_CNT != 4 )
  #error ADC_USER_CHANNEL_CNT != 4  ->  adapt adc_lists_meas_bemf !
  #endif

  {0u, 0u, 0u, ADC_PHASE_U, 0u, 1u, ADC_LISTCMD_IMMEDIATE_SAMPLE},                                                                                                                                                                                     /*  0 */

  {ADC_500_NANO_SECONDS, 0u, 0u, ADC_PHASE_U, 0u, 1u, ADC_LISTCMD_WAIT},                                                                                                                                                                               /*  1 */
  {( uint16_t ) & adc_data.phase_voltage_u, 0u, 1u, ADC_PHASE_V, 0u, 1u, ADC_LISTCMD_IMMEDIATE_SAMPLE},                                                                                                                                                     /*  2 */

  {ADC_500_NANO_SECONDS, 0u, 0u, ADC_PHASE_V, 0u, 1u, ADC_LISTCMD_WAIT},                                                                                                                                                                               /*  3 */
  {( uint16_t ) & adc_data.phase_voltage_v, 0u, 1u, ADC_PHASE_W, 0u, 1u, ADC_LISTCMD_IMMEDIATE_SAMPLE},                                                                                                                                                     /*  4 */

  {ADC_500_NANO_SECONDS, 0u, 0u, ADC_PHASE_W, 0u, 1u, ADC_LISTCMD_WAIT},                                                                                                                                                                               /*  5 */
  {( uint16_t ) & adc_data.phase_voltage_w, 0u, 1u, ADC_VBAT_MEASURE_CH, 0u, 1u, ADC_LISTCMD_IMMEDIATE_SAMPLE},                                                                                                                                             /*  6 */

  {ADC_500_NANO_SECONDS, 0u, 0u, ADC_VBAT_MEASURE_CH, 0u, 1u, ADC_LISTCMD_WAIT},                                                                                                                                                                       /*  7 */
  {( uint16_t ) & adc_data.vsup, 0u, 1u, ADC_USR_CHAN_INIT /*usr_0*/, 0u, 1u, ADC_LISTCMD_IMMEDIATE_SAMPLE},                                                                                                                                                /*  8 (user_channel_0)*/

  #define ADC_USER_CH_0_BEMF_LIST_IDX_0 8
  #define ADC_USER_CH_0_BEMF_LIST_IDX_1 9

  {ADC_500_NANO_SECONDS, 0u, 0u, ADC_USR_CHAN_INIT /*usr_0*/, 0u, 1u, ADC_LISTCMD_WAIT},                                                                                                                                                               /*  9 (user_channel_0)*/
  {( uint16_t ) & adc_data.usr_result[ 0 ], 0u, 1u, ADC_USR_CHAN_INIT /*usr_1*/, 0u, 1u, ADC_LISTCMD_IMMEDIATE_SAMPLE},                                                                                                                                     /* 10 (user_channel_1)*/

  #define ADC_USER_CH_1_BEMF_LIST_IDX_0 10
  #define ADC_USER_CH_1_BEMF_LIST_IDX_1 11

  {ADC_500_NANO_SECONDS, 0u, 0u, ADC_USR_CHAN_INIT /*usr_1*/, 0u, 1u, ADC_LISTCMD_WAIT},                                                                                                                                                               /* 11 (user_channel_1)*/
  {( uint16_t ) & adc_data.usr_result[ 1 ], 0u, 1u, ADC_USR_CHAN_INIT /*usr_2*/, 0u, 1u, ADC_LISTCMD_IMMEDIATE_SAMPLE},                                                                                                                                     /* 12 (user_channel_2)*/

  #define ADC_USER_CH_2_BEMF_LIST_IDX_0 12
  #define ADC_USER_CH_2_BEMF_LIST_IDX_1 13

  {ADC_500_NANO_SECONDS, 0u, 0u, ADC_USR_CHAN_INIT /*usr_2*/, 0u, 1u, ADC_LISTCMD_WAIT},                                                                                                                                                               /* 13 (user_channel_2)*/
  {( uint16_t ) & adc_data.usr_result[ 2 ], 0u, 1u, ADC_USR_CHAN_INIT /*usr_3*/, 0u, 1u, ADC_LISTCMD_IMMEDIATE_SAMPLE},                                                                                                                                     /* 14 (user_channel_3)*/

  #define ADC_USER_CH_3_BEMF_LIST_IDX_0 14
  #define ADC_USER_CH_3_BEMF_LIST_IDX_1 15

  {ADC_500_NANO_SECONDS, 0u, 0u, ADC_USR_CHAN_INIT /*usr_3*/, 0u, 1u, ADC_LISTCMD_WAIT},                                                                                                                                                               /* 15 (user_channel_3)*/
  {( uint16_t ) & adc_data.usr_result[ 3 ], 0u, 1u, ADC_PHASE_U, 0u, 1u, ADC_LISTCMD_IMMEDIATE_SAMPLE},                                                                                                                                                     /* 16 */

  {( uint16_t ) & adc_data.dummy, 0u, 0u, ADC_PHASE_U, 0u, 0u, (uint16_t) PWM_PERIOD_DURATION_TICKS / 2u},                                                                                                                                                       /* 17 this is necessary because the ADC interrupt is the timebase for the whole system */

  {0u, 0u, 0u, ADC_PHASE_U, 0u, 1u, ADC_LISTCMD_END}

};
_Pragma( "diag_default=Pm140,Pm152" )
/* PRQA S 0306 -- */ /* justification: cast from pointer to integral type -> needed for setting target addresses of AD conversions */
/* PRQA S 3218 -- */ /* justification: tables would decrease readability if they were included in functions */

/* PRQA S 3218 ++ */ /* justification: tables would decrease readability if they were included in functions */
/* PRQA S 0306 ++ */ /* justification: cast from pointer to integral type -> needed for setting target addresses of AD conversions */
_Pragma( "diag_suppress=Pm140,Pm152" )
#if ( USR_EN_INITIAL_POSITION_DETECTION == 1 )
/**
   @var adc_lists_pos_detect
   @brief list is used when measuring the rotor position via test pulses
 */
  static saradc_ctrl_list_entry_t adc_lists_pos_detect[] =
  {
    #define ADC_POS_DETECT_LIST_FIRST_MEAS_IDX_0 0
    #define ADC_POS_DETECT_LIST_FIRST_MEAS_IDX_1 1

    {( uint16_t ) & adc_data.dummy, 0u, 1u, ADC_USR_CHAN_INIT /*usr_1*/, 0u, 0u, (uint16_t) PWM_PERIOD_DURATION_TICKS / 2u},
    {( uint16_t ) & adc_data.pos_detect_uv[ 0 ], 0u, 1u, ADC_USR_CHAN_INIT /*usr_1*/, 0u, 1u, ADC_LISTCMD_IMMEDIATE_SAMPLE},

    #define ADC_POS_DETECT_LIST_SECOND_MEAS_IDX_0 2
    #define ADC_POS_DETECT_LIST_SECOND_MEAS_IDX_1 3

    {ADC_500_NANO_SECONDS, 0u, 0u, ADC_USR_CHAN_INIT /*usr_1*/, 0u, 1u, ADC_LISTCMD_WAIT},
    {( uint16_t ) & adc_data.pos_detect_uv[ 1 ], 0u, 1u, ADC_USR_CHAN_INIT /*usr_1*/, 0u, 1u, ADC_LISTCMD_IMMEDIATE_SAMPLE},

    #define ADC_POS_DETECT_LIST_THIRD_MEAS_IDX_0 4
    #define ADC_POS_DETECT_LIST_THIRD_MEAS_IDX_1 5

    {ADC_500_NANO_SECONDS, 0u, 0u, ADC_USR_CHAN_INIT /*usr_1*/, 0u, 1u, ADC_LISTCMD_WAIT},
    {( uint16_t ) & adc_data.pos_detect_uv[ 2 ], 0u, 1u, ADC_USR_CHAN_INIT /*usr_1*/, 0u, 1u, ADC_LISTCMD_IMMEDIATE_SAMPLE},

    {0u, 0u, 0u, ADC_USR_CHAN_INIT, 0u, 1u, ADC_LISTCMD_END}
  };
  _Pragma( "diag_default=Pm140,Pm152" )
/* PRQA S 0306 -- */ /* justification: cast from pointer to integral type -> needed for setting target addresses of AD conversions */
/* PRQA S 3218 -- */ /* justification: tables would decrease readability if they were included in functions */

#endif

#if  ( USR_SHUNT_MEASUREMENT_TYPE == USR_SHUNT_MEAS_TYPE_SINGLE )

/* PRQA S 3218 ++ */ /* justification: tables would decrease readability if they were included in functions */
/* PRQA S 0306 ++ */ /* justification: cast from pointer to integral type -> needed for setting target addresses of AD conversions */
  _Pragma( "diag_suppress=Pm140,Pm152" )

/**
   @var adc_lists_single_shunt
   @brief list is permanently used with the exception of synchronization to a turning rotor and rotor position detection
 */
  static saradc_ctrl_list_entry_t adc_lists_single_shunt[] =
  {
    #if ( ADC_USER_CHANNEL_CNT != 4 )
    #error ADC_USER_CHANNEL_CNT != 4  ->  adapt adc_lists_single_shunt !
    #endif

    {0u, 0u, 0u, ADC_USR_CHAN_INIT /*usr_0*/, 0u, 1u, ADC_LISTCMD_IMMEDIATE_SAMPLE},                                                                                                                                                                   /*  0 (user_channel_0)*/

    #define ADC_USER_CH_0_COMM_LIST_IDX_0 0
    #define ADC_USER_CH_0_COMM_LIST_IDX_1 0

    {( uint16_t ) & adc_data.usr_result[ 0 ], 0u, 1u, ADC_USR_CHAN_INIT /*usr_1*/, 0u, 0u, (uint16_t) ( ADC_THIRD_PWM_EDGE + ADC_USR_MEAS_DLY )},                                                                                                                /*  1 (user_channel_1)*/

    #define ADC_USER_CH_1_COMM_LIST_IDX_0 1
    #define ADC_USER_CH_1_COMM_LIST_IDX_1 2

    {ADC_500_NANO_SECONDS, 0u, 0u, ADC_USR_CHAN_INIT /*usr_1*/, 0u, 1u, ADC_LISTCMD_WAIT},                                                                                                                                                             /*  2 (user_channel_1)*/
    {( uint16_t ) & adc_data.usr_result[ 1 ], 0u, 1u, ADC_USR_CHAN_INIT /*usr_2*/, 0u, 1u, ADC_LISTCMD_IMMEDIATE_SAMPLE},                                                                                                                                   /*  3 (user_channel_2)*/

    #define ADC_USER_CH_2_COMM_LIST_IDX_0 3
    #define ADC_USER_CH_2_COMM_LIST_IDX_1 4

    {ADC_500_NANO_SECONDS, 0u, 0u, ADC_USR_CHAN_INIT /*usr_2*/, 0u, 1u, ADC_LISTCMD_WAIT},                                                                                                                                                             /*  4 (user_channel_2)*/
    {( uint16_t ) & adc_data.usr_result[ 2 ], 0u, 1u, ADC_USR_CHAN_INIT /*usr_3*/, 0u, 1u, ADC_LISTCMD_IMMEDIATE_SAMPLE},                                                                                                                                   /*  5 (user_channel_3)*/

    #define ADC_USER_CH_3_COMM_LIST_IDX_0 5
    #define ADC_USER_CH_3_COMM_LIST_IDX_1 6

    {ADC_500_NANO_SECONDS, 0u, 0u, ADC_USR_CHAN_INIT /*usr_3*/, 0u, 1u, ADC_LISTCMD_WAIT},                                                                                                                                                             /*  6 (user_channel_3)*/
    {( uint16_t ) & adc_data.usr_result[ 3 ], 0u, 1u, ADC_SHUNT_SUM /*offs_1*/, 0u, 1u, ADC_LISTCMD_IMMEDIATE_SAMPLE},                                                                                                                                      /*  7 */

    {( uint16_t ) & adc_data.phase_curr1_offset, 0u, 1u, ADC_VBAT_MEASURE_CH /*vsup*/, 0u, 0u, (uint16_t) ( PWM_SINGLE_SHUNT_MAX_CMP_VAL - (( INT64_C( 2 ) * ( ADC_CONVERSION_CLK_CYCLES + ADC_SAMPLE_CLK_CYCLES )) + ADC_GATE_DRIVER_PROPAGATION_DLY_TICKS ))}, /*  8 */
    {( uint16_t ) & adc_data.vsup, 0u, 1u, ADC_SHUNT_SUM /*curr_1*/, 0u, 1u, ADC_LISTCMD_IMMEDIATE_SAMPLE},                                                                                                                                                 /*  9 */

    {( uint16_t ) & adc_data.phase_curr1, 0u, 1u, ADC_SHUNT_SUM /*curr_2*/, 0u, 0u, (uint16_t) ADC_FIRST_CURRENT_SAMPLE},                                                                                                                                        /* 10 */
    {( uint16_t ) & adc_data.phase_curr2, 0u, 1u, ADC_USR_CHAN_INIT /*usr_0*/, 0u, 0u, (uint16_t) ADC_SECOND_CURRENT_SAMPLE},                                                                                                                                    /* 11 */

    {0u, 0u, 0u, ADC_USR_CHAN_INIT /*usr_0*/, 0u, 1u, ADC_LISTCMD_END}                                                                                                                                                                                 /* 12 (user_channel_0)*/
  };
  _Pragma( "diag_default=Pm140,Pm152" )
/* PRQA S 0306 -- */ /* justification: cast from pointer to integral type -> needed for setting target addresses of AD conversions */
/* PRQA S 3218 -- */ /* justification: tables would decrease readability if they were included in functions */

/*! @brief returns a pointer to the "standard" list depending on the configuration (single shunt or dual shunt) */
  INLINE saradc_ctrl_list_entry_t * adc_lists_get_commutation_list ( void )
  {
    return adc_lists_single_shunt;
  }

#elif (( USR_SHUNT_MEASUREMENT_TYPE == USR_SHUNT_MEAS_TYPE_DUAL_U_V ) || ( USR_SHUNT_MEASUREMENT_TYPE == USR_SHUNT_MEAS_TYPE_DUAL_V_W ))

/* PRQA S 3218 ++ */ /* justification: tables would decrease readability if they were included in functions */
/* PRQA S 0306 ++ */ /* justification: cast from pointer to integral type -> needed for setting target addresses of AD conversions */
  _Pragma( "diag_suppress=Pm140,Pm152" )

/**
   @var adc_lists_dual_shunt
   @brief list is permanently used with the exception of synchronization to a turning rotor
 */
  static saradc_ctrl_list_entry_t adc_lists_dual_shunt[] =
  {
    #if ( ADC_USER_CHANNEL_CNT != 4 )
    #error ADC_USER_CHANNEL_CNT != 4  ->  adapt adc_lists_dual_shunt !
    #endif

    {0u, 0u, 0u, ADC_VBAT_MEASURE_CH, 0u, 1u, ADC_LISTCMD_IMMEDIATE_SAMPLE},                                                                                                                                                                           /*  0 */

    {( uint16_t ) & adc_data.vsup, 0u, 1u, ADC_SHUNT_1 /*offs_1*/, 0u, 0u, (uint16_t) ADC_GATE_DRIVER_PROPAGATION_DLY_TICKS},                                                                                                                                    /*  1 */

    {ADC_500_NANO_SECONDS, 0u, 0u, ADC_SHUNT_1 /*offs_1*/, 0u, 1u, ADC_LISTCMD_WAIT},                                                                                                                                                                  /*  2 */
    {( uint16_t ) & adc_data.phase_curr1_offset, 0u, 1u, ADC_SHUNT_2 /*offs_2*/, 0u, 1u, ADC_LISTCMD_IMMEDIATE_SAMPLE},                                                                                                                                     /*  3 */

    {ADC_500_NANO_SECONDS, 0u, 0u, ADC_SHUNT_2 /*offs_2*/, 0u, 1u, ADC_LISTCMD_WAIT},                                                                                                                                                                  /*  4 */
    {( uint16_t ) & adc_data.phase_curr2_offset, 0u, 1u, ADC_USR_CHAN_INIT /*usr_0*/, 0u, 1u, ADC_LISTCMD_IMMEDIATE_SAMPLE},                                                                                                                                /*  5 (user_channel_0)*/

    #define ADC_USER_CH_0_COMM_LIST_IDX_0 5
    #define ADC_USER_CH_0_COMM_LIST_IDX_1 5

    {( uint16_t ) & adc_data.usr_result[ 0 ], 0u, 1u, ADC_USR_CHAN_INIT /*usr_1*/, 0u, 0u, (uint16_t) ADC_USR_MEAS_DLY},                                                                                                                                         /*  6 (user_channel_1)*/

    #define ADC_USER_CH_1_COMM_LIST_IDX_0 6
    #define ADC_USER_CH_1_COMM_LIST_IDX_1 7

    {ADC_500_NANO_SECONDS, 0u, 0u, ADC_USR_CHAN_INIT /*usr_1*/, 0u, 1u, ADC_LISTCMD_WAIT},                                                                                                                                                             /*  7 (user_channel_1)*/
    {( uint16_t ) & adc_data.usr_result[ 1 ], 0u, 1u, ADC_USR_CHAN_INIT /*usr_2*/, 0u, 1u, ADC_LISTCMD_IMMEDIATE_SAMPLE},                                                                                                                                   /*  8 (user_channel_2)*/

    #define ADC_USER_CH_2_COMM_LIST_IDX_0 8
    #define ADC_USER_CH_2_COMM_LIST_IDX_1 9

    {ADC_500_NANO_SECONDS, 0u, 0u, ADC_USR_CHAN_INIT /*usr_2*/, 0u, 1u, ADC_LISTCMD_WAIT},                                                                                                                                                             /*  9 (user_channel_2)*/
    {( uint16_t ) & adc_data.usr_result[ 2 ], 0u, 1u, ADC_USR_CHAN_INIT /*usr_3*/, 0u, 1u, ADC_LISTCMD_IMMEDIATE_SAMPLE},                                                                                                                                   /* 10 (user_channel_3)*/

    #define ADC_USER_CH_3_COMM_LIST_IDX_0 10
    #define ADC_USER_CH_3_COMM_LIST_IDX_1 11

    {ADC_500_NANO_SECONDS, 0u, 0u, ADC_USR_CHAN_INIT /*usr_3*/, 0u, 1u, ADC_LISTCMD_WAIT},                                                                                                                                                             /* 11 (user_channel_3)*/
    {( uint16_t ) & adc_data.usr_result[ 3 ], 0u, 1u, ADC_SHUNT_1 /*curr_1*/, 0u, 1u, ADC_LISTCMD_IMMEDIATE_SAMPLE},                                                                                                                                        /* 12 */

    {( uint16_t ) & adc_data.phase_curr1, 0u, 1u, ADC_SHUNT_2 /*curr_2*/, 0u, 0u, (uint16_t) ( PWM_DUAL_SHUNT_MAX_CMP_VAL + PWM_DEADTIME_INITVAL_TICKS + ADC_SAMPLE_DELAY_SHUNT_VOLTAGE_TICKS + ADC_GATE_DRIVER_PROPAGATION_DLY_TICKS )},                        /* 13 */
    {( uint16_t ) & adc_data.phase_curr2, 0u, 1u, ADC_VBAT_MEASURE_CH, 0u, 1u, ADC_LISTCMD_IMMEDIATE_SAMPLE},                                                                                                                                               /* 14 */

    {0u, 0u, 0u, ADC_VBAT_MEASURE_CH, 0u, 1u, ADC_LISTCMD_END}
  };
  _Pragma( "diag_default=Pm140,Pm152" )
/* PRQA S 0306 -- */ /* justification: cast from pointer to integral type -> needed for setting target addresses of AD conversions */
/* PRQA S 3218 -- */ /* justification: tables would decrease readability if they were included in functions */

/*! @brief returns a pointer to the "standard" list depending on the configuration (single shunt or dual shunt) */
  INLINE saradc_ctrl_list_entry_t * adc_lists_get_commutation_list ( void )
  {
    return adc_lists_dual_shunt;
  }

#else

 #error USR_SHUNT_MEASUREMENT_TYPE not supported!

#endif

/*! @brief returns a pointer to list adc_lists_meas_bemf */
INLINE saradc_ctrl_list_entry_t * adc_lists_get_meas_bemf_list ( void )
{
  return adc_lists_meas_bemf;
}

#if ( USR_EN_INITIAL_POSITION_DETECTION == 1 )
/*! @brief returns a pointer to adc_lists_pos_detect */
  INLINE saradc_ctrl_list_entry_t * adc_lists_get_pos_detect_list ( void )
  {
    return adc_lists_pos_detect;
  }
#endif

/*! @brief only needed for MISRA compatibility */
static saradc_ctrl_list_entry_t * adc_lists_get_element( saradc_ctrl_list_entry_t * list, uint16_t index )
{
  _Pragma( "diag_suppress=Pm140,Pm152" )
  return &( list[ index ] );                                                                                                                                                                                                                           /* PRQA S 0492 */ /*  justification: use list pointer with array indexing -> permissible because argument 'index' is always a literal, not a variable */
  _Pragma( "diag_default=Pm140,Pm152" )
}

/*! @brief set the signal source for one of the 4 user measurements */
void adc_lists_set_user_channel( const uint8_t idx, const uint8_t adc_channel )
{
  #if ( ADC_USER_CHANNEL_CNT != 4 )
  #error ADC_USER_CHANNEL_CNT != 4  ->  adapt adc_lists_set_user_channel() !
  #endif

  if( adc_channel <= 31u )
  {
    switch( idx )
    {
      case 0u:
      {
        adc_lists_get_element( adc_lists_get_meas_bemf_list(), (uint16_t) ADC_USER_CH_0_BEMF_LIST_IDX_0 )->ch_no   = adc_channel;
        adc_lists_get_element( adc_lists_get_meas_bemf_list(), (uint16_t) ADC_USER_CH_0_BEMF_LIST_IDX_1 )->ch_no   = adc_channel;
        adc_lists_get_element( adc_lists_get_commutation_list(), (uint16_t) ADC_USER_CH_0_COMM_LIST_IDX_0 )->ch_no = adc_channel;
        adc_lists_get_element( adc_lists_get_commutation_list(), (uint16_t) ADC_USER_CH_0_COMM_LIST_IDX_1 )->ch_no = adc_channel;
        break;
      }

      case 1u:
      {
        adc_lists_get_element( adc_lists_get_meas_bemf_list(), (uint16_t) ADC_USER_CH_1_BEMF_LIST_IDX_0 )->ch_no   = adc_channel;
        adc_lists_get_element( adc_lists_get_meas_bemf_list(), (uint16_t) ADC_USER_CH_1_BEMF_LIST_IDX_1 )->ch_no   = adc_channel;
        adc_lists_get_element( adc_lists_get_commutation_list(), (uint16_t) ADC_USER_CH_1_COMM_LIST_IDX_0 )->ch_no = adc_channel;
        adc_lists_get_element( adc_lists_get_commutation_list(), (uint16_t) ADC_USER_CH_1_COMM_LIST_IDX_1 )->ch_no = adc_channel;
        break;
      }

      case 2u:
      {
        adc_lists_get_element( adc_lists_get_meas_bemf_list(), (uint16_t) ADC_USER_CH_2_BEMF_LIST_IDX_0 )->ch_no   = adc_channel;
        adc_lists_get_element( adc_lists_get_meas_bemf_list(), (uint16_t) ADC_USER_CH_2_BEMF_LIST_IDX_1 )->ch_no   = adc_channel;
        adc_lists_get_element( adc_lists_get_commutation_list(), (uint16_t) ADC_USER_CH_2_COMM_LIST_IDX_0 )->ch_no = adc_channel;
        adc_lists_get_element( adc_lists_get_commutation_list(), (uint16_t) ADC_USER_CH_2_COMM_LIST_IDX_1 )->ch_no = adc_channel;
        break;
      }

      case 3u:
      {
        adc_lists_get_element( adc_lists_get_meas_bemf_list(), (uint16_t) ADC_USER_CH_3_BEMF_LIST_IDX_0 )->ch_no   = adc_channel;
        adc_lists_get_element( adc_lists_get_meas_bemf_list(), (uint16_t) ADC_USER_CH_3_BEMF_LIST_IDX_1 )->ch_no   = adc_channel;
        adc_lists_get_element( adc_lists_get_commutation_list(), (uint16_t) ADC_USER_CH_3_COMM_LIST_IDX_0 )->ch_no = adc_channel;
        adc_lists_get_element( adc_lists_get_commutation_list(), (uint16_t) ADC_USER_CH_3_COMM_LIST_IDX_1 )->ch_no = adc_channel;
        break;
      }

      default:
      {
/* Message 2016 */
        break;
      }
    }
  }
}

_Pragma( "diag_default = Pm152" )

#if ( USR_EN_INITIAL_POSITION_DETECTION == 1 )

  _Pragma( "diag_suppress = Pm152" )                                                                                                                                                                                                                   /* disable rule "array indexing shall only be applied to objects defined as an array type (MISRA C 2004 rule 17.4) */

/*! @brief set the phase to be measured during next test pulse as well as the time of measurement and where the result should be put (adc_data.pos_detect_vec1/_vec2/...) */
  NO_INLINE void adc_lists_set_pos_detect_channel( const uint8_t adc_channel, const uint8_t vec_no, const uint16_t trig_time )
  {
    adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_FIRST_MEAS_IDX_0 )->trigger = trig_time;

    adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_FIRST_MEAS_IDX_0 )->ch_no = adc_channel;
    adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_FIRST_MEAS_IDX_1 )->ch_no = adc_channel;

    adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_SECOND_MEAS_IDX_0 )->ch_no = adc_channel;
    adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_SECOND_MEAS_IDX_1 )->ch_no = adc_channel;

    adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_THIRD_MEAS_IDX_0 )->ch_no = adc_channel;
    adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_THIRD_MEAS_IDX_1 )->ch_no = adc_channel;

    switch( vec_no )
    {
      case 1u:
      {
        adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_FIRST_MEAS_IDX_1 )->target_adr  = U16PTR_TO_U16( &adc_data.pos_detect_uv[ 0 ] );
        adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_SECOND_MEAS_IDX_1 )->target_adr = U16PTR_TO_U16( &adc_data.pos_detect_uv[ 1 ] );
        adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_THIRD_MEAS_IDX_1 )->target_adr  = U16PTR_TO_U16( &adc_data.pos_detect_uv[ 2 ] );
        break;
      }

      case 2u:
      {
        adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_FIRST_MEAS_IDX_1 )->target_adr  = U16PTR_TO_U16( &adc_data.pos_detect_vu[ 0 ] );
        adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_SECOND_MEAS_IDX_1 )->target_adr = U16PTR_TO_U16( &adc_data.pos_detect_vu[ 1 ] );
        adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_THIRD_MEAS_IDX_1 )->target_adr  = U16PTR_TO_U16( &adc_data.pos_detect_vu[ 2 ] );
        break;
      }

      case 3u:
      {
        adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_FIRST_MEAS_IDX_1 )->target_adr  = U16PTR_TO_U16( &adc_data.pos_detect_uw[ 0 ] );
        adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_SECOND_MEAS_IDX_1 )->target_adr = U16PTR_TO_U16( &adc_data.pos_detect_uw[ 1 ] );
        adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_THIRD_MEAS_IDX_1 )->target_adr  = U16PTR_TO_U16( &adc_data.pos_detect_uw[ 2 ] );
        break;
      }

      case 4u:
      {
        adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_FIRST_MEAS_IDX_1 )->target_adr  = U16PTR_TO_U16( &adc_data.pos_detect_wu[ 0 ] );
        adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_SECOND_MEAS_IDX_1 )->target_adr = U16PTR_TO_U16( &adc_data.pos_detect_wu[ 1 ] );
        adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_THIRD_MEAS_IDX_1 )->target_adr  = U16PTR_TO_U16( &adc_data.pos_detect_wu[ 2 ] );
        break;
      }

      case 5u:
      {
        adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_FIRST_MEAS_IDX_1 )->target_adr  = U16PTR_TO_U16( &adc_data.pos_detect_vw[ 0 ] );
        adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_SECOND_MEAS_IDX_1 )->target_adr = U16PTR_TO_U16( &adc_data.pos_detect_vw[ 1 ] );
        adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_THIRD_MEAS_IDX_1 )->target_adr  = U16PTR_TO_U16( &adc_data.pos_detect_vw[ 2 ] );
        break;
      }

      case 6u:
      {
        adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_FIRST_MEAS_IDX_1 )->target_adr  = U16PTR_TO_U16( &adc_data.pos_detect_wv[ 0 ] );
        adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_SECOND_MEAS_IDX_1 )->target_adr = U16PTR_TO_U16( &adc_data.pos_detect_wv[ 1 ] );
        adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_THIRD_MEAS_IDX_1 )->target_adr  = U16PTR_TO_U16( &adc_data.pos_detect_wv[ 2 ] );
        break;
      }

      default:
      {
        adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_FIRST_MEAS_IDX_1 )->target_adr  = U16PTR_TO_U16( &adc_data.dummy );
        adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_SECOND_MEAS_IDX_1 )->target_adr = U16PTR_TO_U16( &adc_data.dummy );
        adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_THIRD_MEAS_IDX_1 )->target_adr  = U16PTR_TO_U16( &adc_data.dummy );
        break;
      }
    }
  }

/*! @brief set delay between first, second and third measurements (init value is ADC_500_NANOSECONDS) */
  INLINE void adc_lists_set_pos_detect_meas_dly( const uint16_t meas_delay )
  {
    adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_SECOND_MEAS_IDX_0 )->target_adr = meas_delay;
    adc_lists_get_element( adc_lists_get_pos_detect_list(), (uint16_t) ADC_POS_DETECT_LIST_THIRD_MEAS_IDX_0 )->target_adr  = meas_delay;
  }

  _Pragma( "diag_default = Pm152" )
#endif

/* }@
 */
