/***************************************************************************//**
 * @file			linsci_InterruptHandler.c
 *
 * @creator		sbai
 * @created		28.04.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 ******************************************************************************/

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "io_e52398a.h"
#include <intrinsics.h>

#include "vic_InterruptHandler.h"
#include "linsci_InterruptHandler.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************** FORWARD DECLARATIONS / PROTOTYPES *********************/
/* ****************************************************************************/

/***************************************************************************//**
 * Handles the SCI related interrupt requests.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         TODO: Add description of precondition
 *
 * TODO: A more detailed description.
 *
 ******************************************************************************/
 //__interrupt static void loc_InterruptHandler(void);

/* ****************************************************************************/
/* ************************ MODULE GLOBALE VARIABLES **************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ************************** FUNCTION DEFINITIONS ****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************************* INTERRUPTS *********************************/
/* ****************************************************************************/

/***************************************************************************//**
 * @implementation
 * Simple word write access to IRQ_VENABLE register, but with the difference
 * to sci_SetIRQVEnable, that a special enum (sci_eInterruptVectorNum_t)
 * is passed as parameter, for security reasons and a more concrete interface
 * definition.
 *
 ******************************************************************************/
void linsci_InterruptEnable(linsci_eInterruptVectorNum_t irqsrc)
{
  LINSCI_IRQ_VENABLE = (uint16_t) irqsrc;
}

/***************************************************************************//**
 * @implementation
 * Simple word write access to IRQ_VDISABLE register, but with the difference
 * to sci_SetIRQVDisable, that a special enum (sci_eInterruptVectorNum_t)
 * is passed as parameter, for security reasons and a more concrete interface
 * definition.
 *
 ******************************************************************************/
void linsci_InterruptDisable(linsci_eInterruptVectorNum_t irqsrc)
{
  LINSCI_IRQ_VDISABLE = (uint16_t) irqsrc;
}

/***************************************************************************//**
 * @implementation
 * At first the module number is looked up in a table, because module number
 * and module instance base address have a logical connection. Then the
 * interrupt callback function pointer is saved in module interrupt vector
 * table.
 *
 ******************************************************************************/
void linsci_InterruptRegisterCallback(linsci_eInterruptVectorNum_t irqvecnum, linsci_InterruptCallback_t cbfnc)
{
  linsci_sInterruptEnvironmentData_t* evironmentData = (linsci_sInterruptEnvironmentData_t*) vic_GetPointerToEnvironmentData(vic_IRQ_SCI);
  
  if (irqvecnum < linsci_INTERRUPT_VECTOR_CNT && evironmentData != NULL) 
  {
    evironmentData->InterrupVectorTable[irqvecnum] = cbfnc;
  }
  else {}
}

/***************************************************************************//**
 * @implementation
 * Reverse function of "sci_RegisterInterruptCallback". Sets the corresponding
 * entry in the module interrupt vector table back to NULL. Very imported,for
 * security reasons the corresponding interrupt is disabled. Otherwise the
 * program could possibly call NULL.
 *
 ******************************************************************************/
void linsci_InterruptDeregisterCallback(linsci_eInterruptVectorNum_t irqvecnum)
{
  linsci_sInterruptEnvironmentData_t* evironmentData = (linsci_sInterruptEnvironmentData_t*) vic_GetPointerToEnvironmentData(vic_IRQ_SCI);

  if (irqvecnum < linsci_INTERRUPT_VECTOR_CNT && evironmentData != NULL) 
  {
    linsci_InterruptDisable(irqvecnum);
    evironmentData->InterrupVectorTable[irqvecnum] = NULL;
  }
  else {}
}

/***************************************************************************//**
 * @implementation
 * Every hardware SCI module has it's own interrupt handler, also because each
 * module has it's own interrupt vector. So the module base address is fixed.
 * The interrupt vector table is provided by the user-application from RAM. The
 * module specific vector tables lie behind the VIC interrupt vector table, which
 * address is saved in the "TABLE_BASE". The module interrupt handler
 * gets the address from the function "vic_GetPointerToEviornmentData".
 * The interrupt vector number is saved in a local variable, because the value
 * of IRQ_VECTOR could possible change during processing of
 * sci_SCI0_InterruptHandler. Next the registered interrupt callback function
 * (sci_RegisterInterruptCallback) is copied into a local function pointer,
 * checked if it's not NULL and at least called. At the end the interrupt
 * request flag is cleared, by writing back the interrupt vector number to
 * IRQ_VNO register.
 *
 ******************************************************************************/
 __interrupt void linsci_InterruptHandler(void)
{
  linsci_eInterruptVectorNum_t irqvecnum = (linsci_eInterruptVectorNum_t) LINSCI_IRQ_VNO;

  if (irqvecnum < linsci_INTERRUPT_VECTOR_CNT) /* ensure the IRQ trigger is not already gone away until processing starts */
  {  
    linsci_sInterruptEnvironmentData_t* evironmentData = (linsci_sInterruptEnvironmentData_t*) vic_GetPointerToEnvironmentData(vic_IRQ_SCI);

    linsci_InterruptCallback_t fptr;
  
    uint16_t vicVmaxBackup = 0;
    uint16_t sciVmaxBackup = 0;
  
    /* IRQ nesting on VIC level */
    vicVmaxBackup = VIC_IRQ_VMAX;
    VIC_IRQ_VMAX = vic_IRQ_SCI;

    /* IRQ nesting on module level */
    sciVmaxBackup = LINSCI_IRQ_VMAX;
    LINSCI_IRQ_VMAX = 0;
  
    fptr = evironmentData->InterrupVectorTable[irqvecnum];
      
    if(fptr != NULL)
    {
      __enable_interrupt();
        
      /* Call interrupt callback function */
      fptr(irqvecnum,  (void*) evironmentData->ContextData);      
      
      __disable_interrupt();    
    }
    else 
    {
      /* if there is no handler function for a particular IRQ, disable this IRQ  */ 
      LINSCI_IRQ_VDISABLE = (uint16_t) irqvecnum;
    }
  
    /* Clear interrupt request flag */
    LINSCI_IRQ_VNO = (uint16_t) irqvecnum;
  
    /* IRQ nesting on mocule level */
    LINSCI_IRQ_VMAX = sciVmaxBackup;
  
    /* IRQ nesting on VIC level */
    VIC_IRQ_VMAX = vicVmaxBackup;
  }
  else {}
      
  /* RETI will reenable IRQs here */ 
}

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
void linsci_InterruptInitialisation(vic_cpInterfaceFunctions_t vicIf, linsci_pInterruptEnvironmentData_t environmentdata, linsci_pInterruptContextData_t contextdata)
{
  if (vicIf != NULL && vicIf->InterfaceVersion == VIC_INTERFACE_VERSION && environmentdata != NULL)
  {
    environmentdata->ContextData = contextdata;

    /* Register module at interrupt handler */
    vicIf->RegisterModule(vic_IRQ_SCI, &linsci_InterruptHandler, environmentdata);
    vicIf->EnableModule(vic_IRQ_SCI);
  }
  else {}
  
}
