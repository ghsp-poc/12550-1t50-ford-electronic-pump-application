/**
 * @ingroup HW
 *
 * @{
 */

/**********************************************************************/
/*! @file pwm_ctrl.c
 * @brief configuration and control functions for PWMN block
 *
 * @details
 * PWM0..2 go towards the gate driver, PWM3 can be used for debugging (output at PB7)<br>
 * module prefix: pwm_
 *
 * @author       JBER
 */
/**********************************************************************/
/* Demo Code Usage Restrictions:
 * Elmos Semiconductor AG provides this source code file simply and solely for IC evaluation purposes in laboratory and
 * this file must not be used for other purposes or within non laboratory environments. Especially, the use or the integration in
 * production systems, appliances or other installations is prohibited.
 *
 * Disclaimer:
 * Elmos Semiconductor AG shall not be liable for any damages arising out of defects resulting from (1) delivered hardware or software,
 * (2) non observance of instructions contained in this document, or (3) misuse, abuse, use under abnormal conditions or alteration by anyone
 * other than Elmos Semiconductor AG. To the extend permitted by law Elmos Semiconductor AG hereby expressively disclaims and user expressively
 * waives any and all warranties of merchantability and of fitness for a particular purpose, statutory warranty of non-infringement and any other
 * warranty or product liability that may arise by reason of usage of trade, custom or course of dealing.
 */
/**********************************************************************/

#include <defines.h>
#include <pwm_ctrl.h>
#include <debug.h>
#include <motor_ctrl.h>
#include <adc.h>
#include <systime.h>

#if ( SCI_USAGE_TYPE == SCI_DATALOGGER )
 #include <uart.h>
#endif

/*! @brief disable the output of the PWMN module for all phases (gate driver is then high-ohmic) */
INLINE void pwm_disable_all_outputs( void )
{
  PWMN_PWM0_ON = 0u;
  PWMN_PWM1_ON = 0u;
  PWMN_PWM2_ON = 0u;

  #if ( DBG_PIN3_FUNCTION == DBG_ENABLE_PWM3_PB7 )
    PWMN_PWM3_ON = 0u;
  #endif
}

/*! @brief enable the output of the PWMN module for all phases */
INLINE void pwm_enable_all_outputs( void )
{
  PWMN_PWM0_ON = 3u;
  PWMN_PWM1_ON = 3u;
  PWMN_PWM2_ON = 3u;

  #if ( DBG_PIN3_FUNCTION == DBG_ENABLE_PWM3_PB7 )
    PWMN_PWM3_ON = 3u;
  #endif
}

/*! @brief enable/disable the output of the PWMN module for all three phases separately with the next PWM period */
INLINE void pwm_set_outputs_reload( const BOOL enable_pwm0, const BOOL enable_pwm1, const BOOL enable_pwm2 )
{
  if( enable_pwm0 ){PWMN_PWM0_ON_RELOAD = 3u; }
  else {PWMN_PWM0_ON_RELOAD = 0u; }

  if( enable_pwm1 ){PWMN_PWM1_ON_RELOAD = 3u; }
  else {PWMN_PWM1_ON_RELOAD = 0u; }

  if( enable_pwm2 ){PWMN_PWM2_ON_RELOAD = 3u; }
  else {PWMN_PWM2_ON_RELOAD = 0u; }
}

/*! @brief deprecated */
INLINE void pwm_charge_bootstrap( void )
{
  pwm_set_c0_values( 0u, 0u, 0u );
  pwm_set_c0_reload_values( 0u, 0u, 0u );

  PWMN_PWM0_ON = 3u;
  PWMN_PWM1_ON = 3u;
  PWMN_PWM2_ON = 3u;

  #if ( DBG_PIN3_FUNCTION == DBG_ENABLE_PWM3_PB7 )
    PWMN_PWM3_ON = 3u;
  #endif
}

/*! @brief enable/disable the output of the PWMN module for all phases */
INLINE void pwm_set_all_outputs( const BOOL enable )
{
  if( enable )
  {
    pwm_enable_all_outputs();
  }
  else
  {
    pwm_disable_all_outputs();
  }
}

/*! @brief set the compare values (C0) for all phases */
INLINE void pwm_set_c0_values ( const u16 pwm0_c0, const u16 pwm1_c0, const u16 pwm2_c0 )
{
  PWMN_PWM0_C0 = pwm0_c0;
  PWMN_PWM1_C0 = pwm1_c0;
  PWMN_PWM2_C0 = pwm2_c0;

  #if ( DBG_PIN3_FUNCTION == DBG_ENABLE_PWM3_PB7 )
    PWMN_PWM3_C0 = pwm1_c0;
  #endif
}

/*! @brief set the compare values (C1) for all phases */
INLINE void pwm_set_c1_values ( const u16 pwm0_c1, const u16 pwm1_c1, const u16 pwm2_c1 )
{
  PWMN_PWM0_C1 = pwm0_c1;
  PWMN_PWM1_C1 = pwm1_c1;
  PWMN_PWM2_C1 = pwm2_c1;

  #if ( DBG_PIN3_FUNCTION == DBG_ENABLE_PWM3_PB7 )
    PWMN_PWM3_C1 = pwm1_c1;
  #endif
}

/*! @brief set the reload values (C0) for all phases
 * @details
 * NOTE: the reload values are fed to the C0 registers at next PWMNCNT = 0 or = MAX (when PWMN_CFG_bit.middle_rl = 1 and SAWTOOTH)
 */
INLINE void pwm_set_c0_reload_values ( const u16 pwm0_c0_reload, const u16 pwm1_c0_reload, const u16 pwm2_c0_reload )
{
  PWMN_PWM0_C0_RELOAD = pwm0_c0_reload;
  PWMN_PWM1_C0_RELOAD = pwm1_c0_reload;
  PWMN_PWM2_C0_RELOAD = pwm2_c0_reload;

  #if ( DBG_PIN3_FUNCTION == DBG_ENABLE_PWM3_PB7 )
    PWMN_PWM3_C0_RELOAD = pwm1_c0_reload;
  #endif
}

/*! @brief set the reload values (C1) for all phases
 * @details
 * NOTE: the reload values are fed to the C1 registers at next PWMNCNT = 0 or = MAX (when PWMN_CFG_bit.middle_rl = 1 and SAWTOOTH)
 */
INLINE void pwm_set_c1_reload_values ( const u16 pwm0_c1_reload, const u16 pwm1_c1_reload, const u16 pwm2_c1_reload )
{
  PWMN_PWM0_C1_RELOAD = pwm0_c1_reload;
  PWMN_PWM1_C1_RELOAD = pwm1_c1_reload;
  PWMN_PWM2_C1_RELOAD = pwm2_c1_reload;

  #if ( DBG_PIN3_FUNCTION == DBG_ENABLE_PWM3_PB7 )
    PWMN_PWM3_C1_RELOAD = pwm1_c1_reload;
  #endif
}

/*! @brief set the reload value for CNT_MAX
 * @details
 * NOTE: the reload values are fed to the COUNT_MAX registers at next PWMNCNT = 0 or = MAX (when PWMN_CFG_bit.middle_rl = 1 and SAWTOOTH)
 */
INLINE void pwm_set_cnt_max_reload_value ( const u16 cnt_max_reload )
{
  PWMN_CNT_MAX_RELOAD = cnt_max_reload;
}

/*! @brief explicitely set the counter value of PWMN module; YOU SHOULD REALLY THINK TWICE BEFORE USING THIS FUNCTION! */
INLINE void pwm_set_cnt_value ( const u16 pwm_cnt )
{
  PWMN_CNT = pwm_cnt;
}

/*! @brief get the counter value of PWMN module */
INLINE u16 pwm_get_cnt_value ( void )
{
  return PWMN_CNT;
}

/*! @brief init/configure PWM module
 *
 * @details
 * configure PWMN of MotCU
 */
NO_INLINE void pwm_init( pwm_cnt_mode_t cnt_mode, pwm_deadtime_mode_t deadtime_mode, u8 deadtime )
{
  /* CFG */
  PWMN_CFG_bit.cnt_mode        = (u16) cnt_mode;
  PWMN_CFG_bit.middle_rl       = 0u;
  PWMN_CFG_bit.dead_time_mode  = (u16) deadtime_mode;
  PWMN_CFG_bit.oc_asyn_en      = 1u;
  PWMN_CFG_bit.oc_syn_en       = 0u;
  PWMN_CFG_bit.oc_intf         = 0u;
  PWMN_CFG_bit.restart_en      = 1u;
  PWMN_CFG_bit.restart_cap_en  = 1u;
  PWMN_CFG_bit.dt_evt_continue = 0u;

  /* DEAD_TIME & DEAD_TIME_RELOAD */
  PWMN_DEAD_TIME_RELOAD = (u16) deadtime;
  PWMN_DEAD_TIME        = (u16) deadtime;

  /* CNT_MAX & CNT_MAX_RELOAD */
  PWMN_CNT_MAX_RELOAD = (u16) PWM_PERIOD_DURATION_TICKS;
  PWMN_CNT_MAX        = (u16) PWM_PERIOD_DURATION_TICKS;
  PWMN_CNT            = 0u;

  /* PRESCALER & PRESCALER_RELOAD */
  PWMN_PRESCALER_RELOAD = 0u;
  PWMN_PRESCALER        = 0u;

  /* NTH_START & NTH_START_RELOAD */
  PWMN_NTH_START_RELOAD = 0u;
  PWMN_NTH_START        = 0u;

  /* PWMx_CFG */
  PWMN_PWM0_CFG                   = 0u;
  PWMN_PWM0_CFG_bit.cmp_mode      = 2u; /* use xx_c0 AND xx_c1 as compare values */
  PWMN_PWM0_CFG_bit.ign_start_evt = 1u;

  PWMN_PWM1_CFG                   = 0u;
  PWMN_PWM1_CFG_bit.cmp_mode      = 2u; /* use xx_c0 AND xx_c1 as compare values */
  PWMN_PWM1_CFG_bit.ign_start_evt = 1u;

  PWMN_PWM2_CFG                   = 0u;
  PWMN_PWM2_CFG_bit.cmp_mode      = 2u; /* use xx_c0 AND xx_c1 as compare values */
  PWMN_PWM2_CFG_bit.ign_start_evt = 1u;

  PWMN_PWM3_CFG                   = 0u;
  PWMN_PWM3_CFG_bit.cmp_mode      = 2u; /* use xx_c0 AND xx_c1 as compare values */
  PWMN_PWM3_CFG_bit.ign_start_evt = 1u;

  /* PWMx CURRENT DUTY CYCLES*/
  PWMN_PWM0_C0_RELOAD = 0u;
  PWMN_PWM0_C1_RELOAD = 0u;

  PWMN_PWM1_C0_RELOAD = 0u;
  PWMN_PWM1_C1_RELOAD = 0u;

  PWMN_PWM2_C0_RELOAD = 0u;
  PWMN_PWM2_C1_RELOAD = 0u;

  PWMN_PWM3_C0_RELOAD = 0u;
  PWMN_PWM3_C1_RELOAD = 0u;

  /* PWMx_ON & PWMx_ON_RELOAD */
  PWMN_PWM0_ON               = 0u;
  PWMN_PWM0_ON_RELOAD        = 0u;
  PWMN_PWM0_ON_RELOAD_bit.ls = 0u;
  PWMN_PWM0_ON_RELOAD_bit.hs = 0u;
  PWMN_PWM0_ON_bit.ls        = 0u;
  PWMN_PWM0_ON_bit.hs        = 0u;

  PWMN_PWM1_ON               = 0u;
  PWMN_PWM1_ON_RELOAD_bit.ls = 0u;
  PWMN_PWM1_ON_RELOAD_bit.hs = 0u;
  PWMN_PWM1_ON_bit.ls        = 0u;
  PWMN_PWM1_ON_bit.hs        = 0u;

  PWMN_PWM2_ON               = 0u;
  PWMN_PWM2_ON_RELOAD_bit.ls = 0u;
  PWMN_PWM2_ON_RELOAD_bit.hs = 0u;
  PWMN_PWM2_ON_bit.ls        = 0u;
  PWMN_PWM2_ON_bit.hs        = 0u;

  PWMN_PWM3_ON               = 0u;
  PWMN_PWM3_ON_RELOAD_bit.ls = 0u;
  PWMN_PWM3_ON_RELOAD_bit.hs = 0u;
  PWMN_PWM3_ON_bit.ls        = 0u;
  PWMN_PWM3_ON_bit.hs        = 0u;

  /* CMD */
  PWMN_CMD_bit.run = 1u;
}

/*! @brief retrieve the information whether the PWM counter is counting up or down (when in triangle mode)
 *
 * @param     none
 * @return    True = counting up; False = counting down
 */
INLINE BOOL pwm_cnt_in_first_half ( void )
{
  return U16_TO_BOOL( PWMN_PWM_PHASE & 0x8000u );
}

/* }@
 */
