/***************************************************************************//**
 * @file			cctimer_InterruptHandler.c
 *
 * @creator		rpy
 * @created		26.01.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 * @misra{M3CM Dir-8.9. - PRQA Msg 3218,
 * This arrays are register lookup tables for the different CC-Timer modules.
 * Even though they are defined at file scope and only used in one function\,
 * it is more useful to define them at file scope.,
 * None,
 * None}
 *
 ******************************************************************************/

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "io_e52398a.h"
#include <intrinsics.h>

#include "vic_InterruptHandler.h"
#include "cctimer_InterruptHandler.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************** FORWARD DECLARATIONS / PROTOTYPES *********************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ************************ MODULE GLOBALE VARIABLES **************************/
/* ****************************************************************************/
/* See justification at the file comment header */
// PRQA S 3218 ++
static volatile unsigned short * const loc_CCTIMER_x_IRQ_VENABLE[cctimer_INSTANCE_CNT]  = 
{
  &CCTIMER_0_IRQ_VENABLE,
  &CCTIMER_1_IRQ_VENABLE,
  &CCTIMER_2_IRQ_VENABLE,
  &CCTIMER_3_IRQ_VENABLE
};
// PRQA S 3218 --

static volatile unsigned short * const loc_CCTIMER_x_IRQ_VDISABLE[cctimer_INSTANCE_CNT]  = 
{
  &CCTIMER_0_IRQ_VDISABLE,
  &CCTIMER_1_IRQ_VDISABLE,
  &CCTIMER_2_IRQ_VDISABLE,
  &CCTIMER_3_IRQ_VDISABLE
};

/* See justification at the file comment header */
// PRQA S 3218 ++
static volatile unsigned short * const loc_CCTIMER_x_IRQ_VMAX[cctimer_INSTANCE_CNT]  = 
{
  &CCTIMER_0_IRQ_VMAX,
  &CCTIMER_1_IRQ_VMAX,
  &CCTIMER_2_IRQ_VMAX,
  &CCTIMER_3_IRQ_VMAX
};
// PRQA S 3218 --

/* See justification at the file comment header */
// PRQA S 3218 ++
static volatile unsigned short * const loc_CCTIMER_x_IRQ_VNO[cctimer_INSTANCE_CNT]  = 
{
  &CCTIMER_0_IRQ_VNO,
  &CCTIMER_1_IRQ_VNO,
  &CCTIMER_2_IRQ_VNO,
  &CCTIMER_3_IRQ_VNO
};
// PRQA S 3218 --

static const vic_eInterruptVectorNum_t loc_CCTIMER_VIC_VNO[cctimer_INSTANCE_CNT] = 
{
  vic_IRQ_CCTIMER_0,
  vic_IRQ_CCTIMER_1,
  vic_IRQ_CCTIMER_2,
  vic_IRQ_CCTIMER_3
};

/* See justification at the file comment header */
// PRQA S 3218 ++
static const vic_InterruptCallback_t loc_CCTIMER_InterruptHandler[cctimer_INSTANCE_CNT] = 
{
  cctimer_InterruptHandler0,
  cctimer_InterruptHandler1,
  cctimer_InterruptHandler2,
  cctimer_InterruptHandler3
};
// PRQA S 3218 --

/* ****************************************************************************/
/* ************************** FUNCTION DEFINITIONS ****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************************* INTERRUPTS *********************************/
/* ****************************************************************************/

/***************************************************************************//**
 * @implementation
 * Simple word write access to IRQ_VENABLE register, but with the difference
 * to cctimer_SetIRQVEnable, that a special enum (cctimer_eInterruptVectorNum_t)
 * is passed as parameter, for security reasons and a more concrete interface
 * definition.
 *
 ******************************************************************************/
void cctimer_InterruptEnable(cctimer_InstanceNum_t instanceNum, cctimer_eInterruptVectorNum_t irqsrc)
{
  if (instanceNum < cctimer_INSTANCE_CNT)
  {
    *(loc_CCTIMER_x_IRQ_VENABLE[instanceNum]) = (uint16_t) irqsrc;
  }
  else {}
}

/***************************************************************************//**
 * @implementation
 * Simple word write access to IRQ_VDISABLE register, but with the difference
 * to cctimer_SetIRQVDisable, that a special enum (cctimer_eInterruptVectorNum_t)
 * is passed as parameter, for security reasons and a more concrete interface
 * definition.
 *
 ******************************************************************************/
void cctimer_InterruptDisable(cctimer_InstanceNum_t instanceNum, cctimer_eInterruptVectorNum_t irqsrc)
{
  if (instanceNum < cctimer_INSTANCE_CNT)
  {
    *(loc_CCTIMER_x_IRQ_VDISABLE[instanceNum]) = (uint16_t) irqsrc;
  }
  else {}
}

/***************************************************************************//**
 * @implementation
 * At first the module number is looked up in a table, because module number
 * and module instance base address have a logical connection. Then the
 * interrupt callback function pointer is saved in module interrupt vector
 * table.
 *
 ******************************************************************************/
void cctimer_InterruptRegisterCallback(cctimer_InstanceNum_t instanceNum, cctimer_eInterruptVectorNum_t irqvecnum, cctimer_InterruptCallback_t cbfnc)
{
  if (instanceNum < cctimer_INSTANCE_CNT)
  {
    cctimer_sInterruptEnvironmentData_t* evironmentData = (cctimer_sInterruptEnvironmentData_t*) vic_GetPointerToEnvironmentData(loc_CCTIMER_VIC_VNO[instanceNum]);

    if ((irqvecnum < cctimer_INTERRUPT_VECTOR_CNT) && (evironmentData != NULL))
    {
      evironmentData->InterrupVectorTable[irqvecnum] = cbfnc;
    }
    else {}
  }
  else {}
}

/***************************************************************************//**
 * @implementation
 * Reverse function of "cctimer_RegisterInterruptCallback". Sets the corresponding
 * entry in the module interrupt vector table back to NULL. Very imported,for
 * security reasons the corresponding interrupt is disabled. Otherwise the
 * program could possibly call NULL.
 *
 ******************************************************************************/
void cctimer_InterruptDeregisterCallback(cctimer_InstanceNum_t instanceNum, cctimer_eInterruptVectorNum_t irqvecnum)
{
  if (instanceNum < cctimer_INSTANCE_CNT)
  {
    cctimer_sInterruptEnvironmentData_t* evironmentData = (cctimer_sInterruptEnvironmentData_t*) vic_GetPointerToEnvironmentData(loc_CCTIMER_VIC_VNO[instanceNum]);

    if ((irqvecnum < cctimer_INTERRUPT_VECTOR_CNT) && (evironmentData != NULL))
    {
      cctimer_InterruptDisable(instanceNum,irqvecnum);
      evironmentData->InterrupVectorTable[irqvecnum] = NULL;
    }
    else {}
  }
  else {}
}

/***************************************************************************//**
 * @implementation
 * Every hardware CCTIMER module has it's own interrupt handler, also because each
 * module has it's own interrupt vector. So the module base address is fixed.
 * The interrupt vector table is provided by the user-application from RAM. The
 * module specific vector tables lie behind the VIC interrupt vector table, which
 * address is saved in the "TABLE_BASE". The module interrupt handler
 * gets the address from the function "vic_GetPointerToEviornmentData".
 * The interrupt vector number is saved in a local variable, because the value
 * of IRQ_VECTOR could possible change during processing of
 * cctimer_CCTIMER0_InterruptHandler. Next the registered interrupt callback function
 * (cctimer_RegisterInterruptCallback) is copied into a local function pointer,
 * checked if it's not NULL and at least called. At the end the interrupt
 * request flag is cleared, by writing back the interrupt vector number to
 * IRQ_VNO register.
 *
 ******************************************************************************/
static void loc_InterruptHandler(cctimer_InstanceNum_t instanceNum)
{
  cctimer_eInterruptVectorNum_t irqvecnum = (cctimer_eInterruptVectorNum_t) (*(loc_CCTIMER_x_IRQ_VNO[instanceNum]));
  
  if (irqvecnum < cctimer_INTERRUPT_VECTOR_CNT) /* ensure the IRQ trigger is not already gone away until processing starts */
  {
    cctimer_sInterruptEnvironmentData_t* evironmentData = (cctimer_sInterruptEnvironmentData_t*) vic_GetPointerToEnvironmentData(loc_CCTIMER_VIC_VNO[instanceNum]);

    cctimer_InterruptCallback_t fptr;
  
    uint16_t vicVmaxBackup = 0;
    uint16_t cctimerVmaxBackup = 0;
  
    /* IRQ nesting on VIC level: enabled by default */
    vicVmaxBackup = VIC_IRQ_VMAX;
    VIC_IRQ_VMAX = (uint16_t) loc_CCTIMER_VIC_VNO[instanceNum];

    /* IRQ nesting on module level: by default disabled, handler may override this later on */
    cctimerVmaxBackup = *(loc_CCTIMER_x_IRQ_VMAX[instanceNum]);
    *(loc_CCTIMER_x_IRQ_VMAX[instanceNum]) = 0;
  
    fptr = evironmentData->InterrupVectorTable[irqvecnum];
          
    if(fptr != NULL)
    {          
      __enable_interrupt();
        
      /* Call interrupt callback function */
      fptr(irqvecnum,  (void*) evironmentData->ContextData);      
      
      __disable_interrupt();
    }
    else 
    {
      /* if there is no handler function for a particular IRQ, disable this IRQ  */ 
      *(loc_CCTIMER_x_IRQ_VDISABLE[instanceNum]) = (uint16_t) irqvecnum;
    }

    /* Clear interrupt request flag */
    *(loc_CCTIMER_x_IRQ_VNO[instanceNum]) = (uint16_t) irqvecnum;
  
    /* IRQ nesting on module level */
    *(loc_CCTIMER_x_IRQ_VMAX[instanceNum]) = cctimerVmaxBackup;
  
    /* IRQ nesting on VIC level */
    VIC_IRQ_VMAX = vicVmaxBackup;
  }
  else {}  
} 

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
__interrupt void cctimer_InterruptHandler0(void)
{
  loc_InterruptHandler(cctimer_MOD_0);
}

__interrupt void cctimer_InterruptHandler1(void)
{
  loc_InterruptHandler(cctimer_MOD_1);
}

__interrupt void cctimer_InterruptHandler2(void)
{
  loc_InterruptHandler(cctimer_MOD_2);
}

__interrupt void cctimer_InterruptHandler3(void)
{
  loc_InterruptHandler(cctimer_MOD_3);
}

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
void cctimer_InterruptInitialisation(cctimer_InstanceNum_t instanceNum, vic_cpInterfaceFunctions_t vicIf, cctimer_pInterruptEnvironmentData_t environmentdata, cctimer_pInterruptContextData_t contextdata)
{
  if ( (vicIf != NULL) &&
       (vicIf->InterfaceVersion == VIC_INTERFACE_VERSION) &&
       (environmentdata != NULL) &&
       (instanceNum < cctimer_INSTANCE_CNT) )
  {
    environmentdata->ContextData = contextdata;

    /* Register module at interrupt handler */
    vicIf->RegisterModule(loc_CCTIMER_VIC_VNO[instanceNum], loc_CCTIMER_InterruptHandler[instanceNum], environmentdata);
    vicIf->EnableModule(loc_CCTIMER_VIC_VNO[instanceNum]);
  }
  else {}
}
