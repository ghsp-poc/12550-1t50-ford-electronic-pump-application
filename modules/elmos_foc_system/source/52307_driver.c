/**
 * @ingroup HW
 *
 * @{
 */

/**********************************************************************/
/*! @file 52307_driver.c
 * @brief module that interfaces IC 523.07
 *
 * @details
 * contains all functions to access the functionalities of
 * the gate driver IC 523.07<br>
 * module prefix: d52307_
 *
 * @author       RHA
 */
/**********************************************************************/
/* Demo Code Usage Restrictions:
 * Elmos Semiconductor AG provides this source code file simply and solely for IC evaluation purposes in laboratory and
 * this file must not be used for other purposes or within non laboratory environments. Especially, the use or the integration in
 * production systems, appliances or other installations is prohibited.
 *
 * Disclaimer:
 * Elmos Semiconductor AG shall not be liable for any damages arising out of defects resulting from (1) delivered hardware or software,
 * (2) non observance of instructions contained in this document, or (3) misuse, abuse, use under abnormal conditions or alteration by anyone
 * other than Elmos Semiconductor AG. To the extend permitted by law Elmos Semiconductor AG hereby expressively disclaims and user expressively
 * waives any and all warranties of merchantability and of fitness for a particular purpose, statutory warranty of non-infringement and any other
 * warranty or product liability that may arise by reason of usage of trade, custom or course of dealing.
 */
/**********************************************************************/

#include <52307_driver.h>

#include <adc.h>
#include <systime.h>
#include <debug.h>

/* -------------
   local defines
   ------------- */


/* ------------------
   local declarations
   ------------------ */

#define SPI_FIFO_DEPTH 4

/*! @brief transmit 16 bits via SPI */
static bool_t d52307_spi_0_transmit( uint16_t data, bool_t keep_nss, bool_t blocking )
{
  bool_t result = False;

  do {}  while(( SPI_0_FIFO_LEVELS_bit.tx_fifo_level >= (uint16_t) SPI_FIFO_DEPTH ) && blocking );

  if( SPI_0_FIFO_LEVELS_bit.tx_fifo_level < (uint16_t) SPI_FIFO_DEPTH )
  {
    if( keep_nss )
    {
      SPI_0_DATA_KEEP_NSS = data;
    }
    else
    {
      SPI_0_DATA = data;
    }

    result = True;
  }

  return result;
}

/*! @brief read out the SPI RX buffer */
NO_INLINE static bool_t d52307_spi_0_receive( uint16_t * data, bool_t blocking )
{
  bool_t result = False;

  do {} while(( SPI_0_FIFO_LEVELS_bit.rx_fifo_level < 1u ) && blocking );

  if( SPI_0_FIFO_LEVELS_bit.rx_fifo_level >= 1u )
  {
    *data  = SPI_0_DATA;
    result = True;
  }

  return result;
}

/*! @brief init/configure 52307_driver module
 *
 * @details
 * initialize SPI interface and the IO registers of 523.07
 */
void d52307_init( void )
{
  spi_config_bf_t spi_config =
  {
    .order        = 1u,                                                          /* MSB_First */
    .phase        = 0u,                                                          /* phase = 0 --> 2nd edge sample */
    .polarity     = 0u,                                                          /* polarity = 0 --> clock_off _level 0 */
    .slave        = 0u,                                                          /* !MASTER */
    .length       = 16u - 1u,
    .slave_high_z = 0u,                                                          /* high_z = 0 --> doesn't matter (master_mode) */
    .invert_nss   = 0u,
    .invert_data  = 0u,
    .pause_nss    = 1u,
    .sdi_irq_pol  = 0u,
    .swap_sdi_sdo = 0u,
    .enable       = 1u,
  };

  SPI_0_CONFIG_bit     = spi_config;
  SPI_0_BAUD_CONFIG    = (uint16_t) ((u32) F_CPU_MHZ / 2uL / (u32) ( SPI0_CLK_MHZ )); /* Divider = f_clk/(2*baudrate) */
  SPI_0_TIMEOUT_CONFIG = 0xFFFFu;

  d52307_write_reg( CHIPCTRL, CHIPCTRL_INITVAL );
  systime_delay_blocking( 3u );
  d52307_write_reg( SLEEPCTRL, SLEEPCTRL_INITVAL );
  systime_delay_blocking( 3u );
  d52307_write_reg( SAFECTRL, SAFECTRL_INITVAL );
  systime_delay_blocking( 3u );
  d52307_write_reg( IOCFG, IOCFG_INITVAL );
  systime_delay_blocking( 3u );
  d52307_write_reg( VREGCTRL, VREGCTRL_INITVAL );
  systime_delay_blocking( 3u );
  d52307_write_reg( SCPCTRL, SCPCTRL_INITVAL );
  systime_delay_blocking( 3u );
  d52307_write_reg( WDCTRL, WDCTRL_INITVAL );
  systime_delay_blocking( 3u );
  d52307_write_reg( IRQMSK1, IRQMSK1_INITVAL );
  systime_delay_blocking( 3u );
  d52307_write_reg( IRQSTAT1, IRQSTAT1_INITVAL );
  systime_delay_blocking( 3u );
  d52307_write_reg( IRQMSK2, IRQMSK2_INITVAL );
  systime_delay_blocking( 3u );
  d52307_write_reg( IRQSTAT2, IRQSTAT2_INITVAL );
  systime_delay_blocking( 3u );
  d52307_write_reg( IOCOMPTHR, IOCOMPTHR_INITVAL );
  systime_delay_blocking( 3u );
  d52307_write_reg( AMUX, AMUX_INITVAL );
  systime_delay_blocking( 3u );
  d52307_write_reg( DMUX, DMUX_INITVAL );
  systime_delay_blocking( 3u );
  d52307_write_reg( IOCOMPDEB, IOCOMPDEB_INITVAL );
  systime_delay_blocking( 3u );
  d52307_write_reg( SCMT, SCMT_INITVAL );
  systime_delay_blocking( 3u );
  d52307_write_reg( SCTH_HS, SCTH_HS_INITVAL );
  systime_delay_blocking( 3u );
  d52307_write_reg( SCTH_LS, SCTH_LS_INITVAL );
  systime_delay_blocking( 3u );
  d52307_write_reg( SCEN, SCEN_INITVAL );
  systime_delay_blocking( 3u );
  d52307_write_reg( SCDEB, SCDEB_INITVAL );
  systime_delay_blocking( 3u );
  d52307_write_reg( OFFSET_CFG, OFFSET_CFG_INITVAL );
  systime_delay_blocking( 3u );
  d52307_write_reg( OFFSET_VAL, OFFSET_VAL_INITVAL );
  systime_delay_blocking( 3u );

  /* clear IRQSTAT1 and IRQSTAT2 */
  d52307_driver_error_clear();

  /* set AMUX to VBAT voltage divider */
  d52307_set_bemf_channel( D52307_BEMF_CHANNEL_VSUP );
}

/*! @brief create the 16bit data packet from its bitfields */
INLINE static uint16_t d52307_build_spi_packet( const uint8_t adr, const uint8_t rw, const uint8_t data )
{
  uint16_t ret_val = ((uint16_t) (uint8_t) ( adr & MSK_523_07_ADR )) << SFT_523_07_ADR;
  ret_val |= ((uint16_t) (uint8_t) ( rw & MSK_523_07_RW ) << (uint16_t) SFT_523_07_RW ) | ((uint16_t) (uint8_t) data );
  return ret_val;
}

/*! @brief write an IO register of 523.07 via SPI
 *
 * @param     adr (address in 523.07 IO space), data (value to be written)
 */
NO_INLINE static bool_t d52307_write_reg_raw( uint8_t adr, uint8_t data, bool_t blocking )
{
  bool_t ret_val = False;
  uint16_t spi_packet;

  #if ( DBG_PIN3_FUNCTION == DBG_SHOW_SPI_FRAME )
    dbg_pin3_set();
  #endif

  if( 0u == SPI_0_FIFO_LEVELS_bit.tx_fifo_level )
  {
    /* construct SPI frame out of arguments, transmit and wait for process to be finished */
    spi_packet = d52307_build_spi_packet( adr, 0u, data );

    (void) d52307_spi_0_transmit( spi_packet, False, blocking );

    if( True == blocking )
    {
      if( d52307_spi_0_receive( &spi_packet, True ) == True )
      {
        ret_val = True;
      }
      else
      {
        ret_val = False;
      }
    }
    else
    {
      ret_val = True;
    }
  }
  else
  {
    ret_val = False;
  }

  #if ( DBG_PIN3_FUNCTION == DBG_SHOW_SPI_FRAME )
    dbg_pin3_clear();
  #endif

  return ret_val;
}

/*! @brief write an IO register of 523.07 via SPI
 *
 * @param     adr (address in 523.07 IO space), data (value to be written)
 */
void d52307_write_reg( uint8_t adr, uint8_t data )
{
  (void) d52307_write_reg_raw( adr, data, True );
}

/*! @brief read an IO register of 523.07 via SPI
 *
 * @details
 * reads the value of a given (->adr) register to a uint8_t-variable passed as a pointer (->*data)
 *
 * @param     adr (address in 523.07 IO space), *data (holds content of register after function execution)
 */
NO_INLINE void d52307_read_reg( uint8_t adr, uint8_t * const data )
{
  uint16_t spi_packet;

  #if ( DBG_PIN3_FUNCTION == DBG_SHOW_SPI_FRAME )
    dbg_pin3_set();
  #endif

  if( 0u == SPI_0_FIFO_LEVELS_bit.tx_fifo_level )
  {
    spi_packet = d52307_build_spi_packet( adr, 1u, 0u );

    SPI_0_FIFO_CLEAR = (uint16_t) ( E_SPI_FIFO_CLEAR_RX_FIFO_CLEAR ) | (uint16_t) ( E_SPI_FIFO_CLEAR_TX_FIFO_CLEAR );

    (void) d52307_spi_0_transmit( spi_packet, False, True );

    if( d52307_spi_0_receive( &spi_packet, True ))
    {
      *data = (uint8_t) ( spi_packet & 0xFFu );
    }
    else
    {
      *data = 0xFFu;
    }
  }
  else
  {
  }

  #if ( DBG_PIN3_FUNCTION == DBG_SHOW_SPI_FRAME )
    dbg_pin3_clear();
  #endif
}

/*! @brief read 523.07 driver error
 *
 * @details
 * read registers IRQSTAT1 and IRQSTAT2
 *
 * @param     (uint8_t *) content of IRQSTAT1, (uint8_t *) content of IRQSTAT2
 */
void d52307_driver_error_get( uint8_t * const irqstat1_val, uint8_t * const irqstat2_val )
{
  d52307_read_reg( IRQSTAT1, irqstat1_val );
  systime_delay_blocking( 3u );
  d52307_read_reg( IRQSTAT2, irqstat2_val );
  systime_delay_blocking( 3u );
}

/*! @brief clear 523.07 driver error
 *
 * @details
 * clear all flags in IRQSTAT1 and IRQSTAT2
 */
void d52307_driver_error_clear( void )
{
  d52307_write_reg( IRQSTAT1, 0xFFu );
  systime_delay_blocking( 3u );
  d52307_write_reg( IRQSTAT2, 0xFFu );
  systime_delay_blocking( 3u );
}

/*! @brief get status of pin S (read register DMON2 and wevaluate MSB) */
bool_t d52307_pin_S_status_get( void )
{
  uint8_t loc_register_value = 0u;

  d52307_read_reg( DMON2, &loc_register_value );
  systime_delay_blocking( 3u );

  return U8_TO_BOOL( loc_register_value & (uint8_t) BITPOS_S );
}

/*! @brief enter deep sleep mode */
void d52307_go_to_sleep( void )
{
  d52307_write_reg( SLEEPCTRL, 0x03u );
}

/*! @brief trigger watchdog of 523.07
 *
 * @details
 * depending on what is written to register WDCTRL, this function must be called before
 * the watchdog overflows
 */
void d52307_kick_the_dog( void )
{
  static uint8_t wdtrig_val         = 0u;
  static uint16_t wdtrig_time_stamp = 0u;

  uint16_t cur_time = systime_getU16();

  if(( cur_time - wdtrig_time_stamp ) >= (uint16_t) WD_TRIG_CYCLE_TIME_TICKS )
  {
    #if ( DBG_PIN3_FUNCTION == DBG_SHOW_WD_TRIGGER_ACCESS )
      dbg_pin3_set();
    #endif

    wdtrig_val = ( ~wdtrig_val ) & 1u;
    d52307_write_reg( WDTRIG, wdtrig_val );

    wdtrig_time_stamp = cur_time;

    #if ( DBG_PIN3_FUNCTION == DBG_SHOW_WD_TRIGGER_ACCESS )
      dbg_pin3_clear();
    #endif
  }
}

/*! @brief configure output of 523.07 AMUX (BEMF_U, BEMF_V, BEMF_W or VBAT) */
INLINE void d52307_set_bemf_channel( d52307_bemf_channel_t channel )
{
  d52307_write_reg( AMUX, (uint8_t) channel | 0x80u );
}

/* }@
 */
