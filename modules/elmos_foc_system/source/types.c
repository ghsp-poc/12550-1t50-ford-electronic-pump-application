/**
 * @ingroup HELP
 *
 * @{
 */

/**********************************************************************/
/*! @file types.c
 * @brief todo
 *
 * @details
 * encapsulated, todoeasy-to-use math functionalities<br>
 * module prefix: xxx
 *
 * @author       JBER
 */
/**********************************************************************/
/* Demo Code Usage Restrictions:
 * Elmos Semiconductor AG provides this source code file simply and solely for IC evaluation purposes in laboratory and
 * this file must not be used for other purposes or within non laboratory environments. Especially, the use or the integration in
 * production systems, appliances or other installations is prohibited.
 *
 * Disclaimer:
 * Elmos Semiconductor AG shall not be liable for any damages arising out of defects resulting from (1) delivered hardware or software,
 * (2) non observance of instructions contained in this document, or (3) misuse, abuse, use under abnormal conditions or alteration by anyone
 * other than Elmos Semiconductor AG. To the extend permitted by law Elmos Semiconductor AG hereby expressively disclaims and user expressively
 * waives any and all warranties of merchantability and of fitness for a particular purpose, statutory warranty of non-infringement and any other
 * warranty or product liability that may arise by reason of usage of trade, custom or course of dealing.
 */
/**********************************************************************/

#include <types.h>

#include <debug.h>

/*-------------------
   local defines
   -------------------*/

/* -------------------
   local declarations
   ------------------- */

/*! @brief todo */
u8 GET_BIT_POS( u16 val )
{
  u8 result = 255u;

  if( val == ( 1uL << 0 )){result = 0u; }
  if( val == ( 1uL << 1 )){result = 1u; }
  if( val == ( 1uL << 2 )){result = 2u; }
  if( val == ( 1uL << 3 )){result = 3u; }
  if( val == ( 1uL << 4 )){result = 4u; }
  if( val == ( 1uL << 5 )){result = 5u; }
  if( val == ( 1uL << 6 )){result = 6u; }
  if( val == ( 1uL << 7 )){result = 7u; }
  if( val == ( 1uL << 8 )){result = 8u; }
  if( val == ( 1uL << 9 )){result = 9u; }
  if( val == ( 1uL << 10 )){result = 10u; }
  if( val == ( 1uL << 11 )){result = 11u; }
  if( val == ( 1uL << 12 )){result = 12u; }
  if( val == ( 1uL << 13 )){result = 13u; }
  if( val == ( 1uL << 14 )){result = 14u; }
  if( val == ( 1uL << 15 )){result = 15u; }

  return result;
}

/*! @brief todo */
volatile u16 * U16_TO_U16PTR ( const volatile u16 addr )
{
  _Pragma( "diag_suppress = Pm140" )                   /* justification: cast from u16 to ptr-type */
  return (volatile u16 *) addr;                        /* PRQA S 0303 */  /* justification: cast from u16 to ptr-type */
  _Pragma( "diag_default = Pm140" )
}

/*! @brief todo */
u16 U16PTR_TO_U16 ( const volatile u16 * ptr )
{
  _Pragma( "diag_suppress = Pm140" )                   /* justification: cast from ptr-type to u16*/
  return (u16) ptr;                                    /* PRQA S 0303 */  /* justification: cast from ptr-type to u16*/
  _Pragma( "diag_default = Pm140" )
}

/*! @brief todo */
s16 SHR_S16 ( const s16 value, const u16 places )
{
  _Pragma( "diag_suppress = Pm031" )
  return ( value >> places );                          /* PRQA S 4533 */  /* justification: tested with IAR + EL16 */
  _Pragma( "diag_default = Pm031" )
}

/*! @brief todo */
s16 SHL_S16 ( const s16 value, const u16 places )
{
  _Pragma( "diag_suppress = Pm031" )
  return ( value << places );                          /* PRQA S 4533 */  /* justification: tested with IAR + EL16 */
  _Pragma( "diag_default = Pm031" )
}

/*! @brief todo */
s32 SHR_S32 ( const s32 value, const u16 places )
{
  _Pragma( "diag_suppress = Pm031" )
  return ( value >> places );                          /* PRQA S 4533 */  /* justification: tested with IAR + EL16 */
  _Pragma( "diag_default = Pm031" )
}

/*! @brief todo */
s32 SHL_S32 ( const s32 value, const u16 places )
{
  _Pragma( "diag_suppress = Pm031" )
  return ( value << places );                          /* PRQA S 4533 */  /* justification: tested with IAR + EL16 */
  _Pragma( "diag_default = Pm031" )
}

/*! @brief todo */
void WRITE_REG_U16 ( volatile u16 * const addr, const u16 data )
{
  *addr = data;
}

/*! @brief todo */
void WRITE_REG_S16 ( volatile u16 * const addr, const s16 data )
{
  WRITE_REG_U16( addr, (u16) data );
}

/*! @brief todo */
u16 READ_REG_U16 ( volatile u16 const * addr )
{
  return *addr;
}

/*! @brief todo */
s16 READ_REG_S16 ( volatile u16 const * addr )
{
  return ( s16 ) READ_REG_U16( addr );
}

/*! @brief todo */
void WRITE_REG_U32( volatile u16 * const addr, const u32 data )
{
  WRITE_REG_U16( addr, LOWORD( data ));
  WRITE_REG_U16( addr + 1u, HIWORD( data ));
}

/*! @brief todo */
void WRITE_REG_S32( volatile u16 * const addr, const s32 data )
{
  WRITE_REG_U32( addr, (u32) data );
}

/*! @brief todo */
u32 READ_REG_U32( volatile u16 const * addr )
{
  u32 loc_result = (u32) READ_REG_U16( addr );
  return ( loc_result | (((u32) READ_REG_U16( addr + 1 )) << 16 ));
}

/*! @brief todo */
s32 READ_REG_S32( volatile u16 const * addr )
{
  return (s32) READ_REG_U32( addr );
}

/*! @brief todo */
BOOL U8_TO_BOOL( const u8 val )
{
  BOOL loc_retVal = False;

  if( val != 0u )
  {
    loc_retVal = True;
  }

  return loc_retVal;
}

/*! @brief todo */
BOOL S16_TO_BOOL( const s16 val )
{
  BOOL loc_retVal = False;

  if( val != 0 )
  {
    loc_retVal = True;
  }

  return loc_retVal;
}

/*! @brief todo */
BOOL U16_TO_BOOL( const u16 val )
{
  BOOL loc_retVal = False;

  if( val != 0u )
  {
    loc_retVal = True;
  }

  return loc_retVal;
}

/*! @brief todo */
u16 LOWORD( const u32 val )
{
  return (u16) val;
}

/*! @brief todo */
u16 HIWORD( const u32 val )
{
  return (u16) ( val >> 16 );
}

/*! @brief todo */
void GPIO_C_SET( const u16 pin )
{
  GPIO_C_DATA_OUT |= pin;
}

/*! @brief todo */
void GPIO_C_CLEAR( const u16 pin )
{
  GPIO_C_DATA_OUT &= ~pin;
}


/*! @brief todo */
void GPIO_B_SET( const u16 pin )
{
  GPIO_B_DATA_OUT |= pin;
}

/*! @brief todo */
void GPIO_B_CLEAR( const u16 pin )
{
  GPIO_B_DATA_OUT &= ~pin;
}

/* }@
 */
