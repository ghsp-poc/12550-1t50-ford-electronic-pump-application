#include <stdint.h>
#include "flash_api.h"
#include "flash_api_ram.h"
/* todo: changed! */
 #include <defines.h>

static void flash_protect_all( void );
static void flash_unprotect_main( void );

/**
 * @brief protect all flash areas and disable write access
 *
 * This function enables the write protection of the flash peripheral.
 */
static void flash_protect_all( void )
{
  FLASH_CTRL_AREA_MAIN_L = 0xA5FFu;
  FLASH_CTRL_AREA_MAIN_H = 0xA5FFu;
}

/**
 * @brief remove protection of all flash areas
 *
 * This function removes the write protection of the flash peripheral.
 */
static void flash_unprotect_main( void )
{
  FLASH_CTRL_AREA_MAIN_L = 0xA500u;
  FLASH_CTRL_AREA_MAIN_H = 0xA500u;
}

/**
 * @brief initialize the flash api
 *
 * As the MOTCU contains a single flash, the main flash operations
 * have to be conducted from SRAM (flash_write, flash_page_erase). Thus the
 * functions have to be copied to the SRAM before they can be used.
 *
 * @return this implementation returns flash_ok in any case.
 */
flash_status_t flash_init ( void ) {

  init_ram_code();
  return flash_ok;
}


/**
 * @brief read a flash location to a buffer
 *
 * The function reads word_count words from the flash address given to a buffer
 * and returns the address of that buffer. When the memory is directly accessible
 * (like in this case), the reference may point directly to the address required
 * to read.
 *
 * The function has to assert that the buffer passed by ref is really readable.
 * The flash may not be busy with some other operation (e.g. erase) when flash_ok is returned.
 * When the flash is busy with some other operation, is must return flash_operation_pending.
 *
 * The function may use an static internal sram buffer to hold the read data, but as
 * the eeprom emulation api will not copy the data but pass on the reference,
 * validity will be lost when a successive call to the emulation api is made.
 *
 * @param addr flash address to read
 * @param ref reference to point to the buffer where the read data is available.
 * @param word_count number of words to read.
 *
 * @return this implementation returns flash_ok in any case.
 */
flash_status_t flash_read ( uint16_t addr, const uint16_t * * ref, uint16_t word_count ) {
  if( ref != NULL ){
    *ref = (const void *) addr;
  }
  return flash_ok;
}

/**
 * @brief writes a buffer to a flash location
 *
 * The function writes a word-based buffer of word_count words to the address given. The function must
 * be able to handle non-row aligned writes ( must split-up the write-operation to two or more row writes)
 * but can rely on even word_counts (32 bit writes).
 * As the underlying flash requires 32 bit writes, the address must be 32 bit aligned and word_count must
 * be even.
 * Removal of the write protection is done within the function.
 *
 * @param addr target address of the write operation
 * @param word_buf buffer to contain the words to write
 * @param word_count number of words to write
 *
 * @return flash_ok or flash_failed when switching to programming mode fails
 */
flash_status_t flash_write ( uint16_t addr, const uint16_t * word_buf, uint16_t word_count ) {
  flash_status_t status;

/*	debug("Write 0x%04x (%d)\n", addr, word_count ); */
  flash_unprotect_main();
  status = ram_write_words_to_flash ( FLASH_MAIN, addr, word_buf, word_count );
  flash_protect_all();
  return status;
}

/**
 * @brief erases a flash page
 *
 * The function erases the given flash page. On MOTCU, this operation has to be
 * performed in SRAM, so flash_init is required to be executed once before
 * this function is called.
 *
 * @param addr target address of the page to erase
 *
 * @return flash_ok or flash_failed when switching to erase mode fails
 */
flash_status_t flash_page_erase ( uint16_t addr ) {
  flash_status_t status;

/*	debug("Erase 0x%04x\n", addr); */
  flash_unprotect_main();
  status = ram_flash_page_erase ( FLASH_MAIN, addr );
  flash_protect_all();
  return status;
}
