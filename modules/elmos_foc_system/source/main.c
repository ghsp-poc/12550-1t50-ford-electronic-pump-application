/**********************************************************************/
/*! @file main.c
 * @brief main file FOC Firmware
 *
 * @mainpage
 * FOC firmware for 523.05/523.06/523.52 <br>
 * (dual-die: motCU + 523.01/523.07/523.50) <br>
 * <br>
 * The two most crucial functions (and a good point to get started, if you have to) are: <br>
 * mc_isr_handler() - the sole ISR associated with motor control which, amongst other things, also calls the following function... <br>
 * foc_full_foc_calc() - the actual mathematics of the FOC <br>
 * You might also wanna have a look at the state machine governing the motor control -> mc_main() <br>
 * <br>
 * The second and final advice would be: keep in mind that the call graphs produced by GraphWiz don't seem <br>
 * to be 100% accurate. Therefore please rather rely on your skill to read source code than sheepishly follow the call graphs. <br>
 * <br>
 * Demo Code Usage Restrictions:<br>
 * Elmos Semiconductor AG provides this source code file simply and solely for IC evaluation purposes in laboratory and this file must not be used for other purposes or within non laboratory environments. Especially, the use or the integration in production systems, appliances or other installations is prohibited.<br>
 *
 * Disclaimer:<br>
 * Elmos Semiconductor AG shall not be liable for any damages arising out of defects resulting from (1) delivered hardware or software, (2) non observance of instructions contained in this document, or (3) misuse, abuse, use under abnormal conditions or alteration by anyone  other than Elmos Semiconductor AG. To the extend permitted by law Elmos Semiconductor AG hereby expressively disclaims and user expressively waives any and all warranties of merchantability and of fitness for a particular purpose, statutory warranty of non-infringement and any other warranty or product liability that may arise by reason of usage of trade, custom or course of dealing.<br>
 *
 * @author       JBER
 *
 * @version      4.1
 */
/**********************************************************************/

#include <defines.h>
#include <debug.h>

#include <adc.h>
#include <adc_lists.h>
#include <pwm_ctrl.h>
#include <diag_pwm_in.h>
#include <diag_pwm_out.h>
#include <motor_ctrl.h>
#include <foc.h>
#include <speed_meas.h>
#include <rotor_pos_detect.h>

#if ( SCI_USAGE_TYPE == SCI_DATALOGGER )
 #include <uart.h>
#endif

#include <52307_driver.h>
#include "CommonInterruptHandler.h"
#include "vic_InterruptHandler.h"
#include "LinDrvImp_Config.h"
#include "el_types.h"
#include "rom_API.h"

#include "user_application.h"

/* "cookies" - special program memory locations that the bootloader evaluates during startup, for instance for
 * deciding whether to execute the ROM bootloader or jump to main() */

_Pragma( "location=0xFFF2u" )
static __root const uint16_t active_cookie = 0x0000u;

_Pragma( "location=0xFFF4u" )
static __root const uint16_t kill_cookie = 0xFFFFu;

_Pragma( "location=0xFFE8u" )
static __root const uint64_t customer_signature = 0xFFFFFFFFFFFFFFFFu;


/* ---------------------
   function declarations
   --------------------- */
static void sys_init( void );

/*! @brief mother of all
 *
 * @details
 * call to init-procedures and endless loop
 */
s16 main( void )
{
  /************************************************************************/
  /************************* IMPORTANT HINT *******************************/
  /************************************************************************/
  /* The generation of the doxygen documentation is completely automated: */
  /* - go to subfolder {workspace}\tools\doxygen */
  /* - execute doxygen_run.bat (recommended) or doxygen_wiz.bat */
  /* - navigate to subfolder {workspace}\doxygen\html\ */
  /* - open index.html and enjoy */
  /************************************************************************/
  /************************************************************************/

  /* do the initialization-limbo */
  (void) sys_init();

  /* endless loop - contains call to functions that are meant to be executed regularly without a specified timing */
  while( True )
  {


    #if ( DBG_PIN3_FUNCTION == DBG_MAIN_LOOP_DURATION )
      dbg_pin3_set();
    #endif

    #if ( DEBUG == 1 )
      dbg_main();
    #endif

    #if ( SCI_USAGE_TYPE == SCI_DATALOGGER )
      /* main function of UART module */
      uart_main();
    #elif ( SCI_USAGE_TYPE == SCI_ELMOS_LIN )
      LinDrvImp_ProtoIfFuns->Task((LinProtoIf_pGenericEnvData_t) LinDrvImp_ProtoEnvData );                                     /* PRQA S 0314 */ /* justification: ESW LIN stack */
      LinDrvImp_TransIfFuns->Task((LinTransIf_pGenericEnvData_t) LinDrvImp_TransEnvData );                                     /* PRQA S 0314 */ /* justification: ESW LIN stack */
    #elif ( SCI_USAGE_TYPE == SCI_UNUSED )
    #else
    #error "SCI_USAGE_TYPE not defined!"
    #endif

    #if ( USE_PWM_DIAG_OUTPUT == 1 )
      diago_main();
    #endif

    /* main function of the motor control module */
#ifndef ENABLE_UNLOADED_CPU_CHRONOMETRICS
    mc_main();
#endif
    /* main state machine for the user application */
    app_main();

    /* kick the dog before it bites! */
    d52307_kick_the_dog();

    #if ( DEBUG == 1 )
      static u16 loc_dbg_iomux_restore    = U16_MAX;
      static BOOL loc_last_pb7_dbg_signal = False;

      if(( adc_get_data()->enable_pb7_dbg_signal != False ) && ( loc_last_pb7_dbg_signal == False ))
      {
        loc_dbg_iomux_restore = IOMUX_CTRL_PB_IO_SEL_bit.sel_7;

        #if ( DBG_PIN3_FUNCTION == DBG_ENABLE_PWM3_PB7 )
          IOMUX_CTRL_PB_IO_SEL_bit.sel_7 = 2u;                                                                                 /* IOMUX_CTRL_FUNC_B7_PWMN X_P */
        #else
          IOMUX_CTRL_PB_IO_SEL_bit.sel_7 = 3u;                                                                                 /* IOMUX_CTRL_FUNC_B7_SARADC_DBG_SMPL */
        #endif

        loc_last_pb7_dbg_signal = adc_get_data()->enable_pb7_dbg_signal;
      }
      else if(( !( adc_get_data()->enable_pb7_dbg_signal )) && ( loc_last_pb7_dbg_signal ))
      {
        IOMUX_CTRL_PB_IO_SEL_bit.sel_7 = loc_dbg_iomux_restore;
        loc_last_pb7_dbg_signal        = adc_get_data()->enable_pb7_dbg_signal;
      }
      else
      {
/*Message 2013*/
      }
    #endif

    #if ( DBG_PIN3_FUNCTION == DBG_MAIN_LOOP_DURATION )
      dbg_pin3_clear();
    #endif
  }
}

/*! @brief initialize CPU and its peripherals
 *
 * @details
 * - init VIC (vector interrupt controller)<br>
 * - set system clock<br>
 * - init GPIO, SPI<br>
 * - init timers<br>
 * - init 523.01/523.50 gate driver<br>
 * - init ADC<br>
 * - init PWM generation module<br>
 * - init UART/LIN module<br>
 * - init motor control module<br>
 * - init FOC module<br>
 */
static void sys_init( void )
{
  /* function pointer into ROM for VIC interface functions */
  static vic_cpInterfaceFunctions_t vic_IfFuns;

  /* get VIC interface functions (enable, register callback etc.) */
  _Pragma( "diag_suppress=Pm140,Pm141" )
  if( romIf_MainInterface.Interface_Get((u16) ROMIF_API_VIC, (romIf_cpGenericInterface_t *) &vic_IfFuns, LIN_NULL ) == FALSE ) /* PRQA S 0310 */ /* justification: ESW LIN stack */
  {
    while( 1 )
    {
    }
  }
  _Pragma( "diag_default=Pm140,Pm141" )

  /* initialize all VIC handlers to NULL */
  InterruptHandlerInitialize( vic_IfFuns );

  /* set system clock and enable the peripherals needed */
  #if ( F_CPU_MHZ == INT64_C( 48 ))
    SYS_STATE_CONTROL_bit.sys_clk_sel = (u16) SYS_STATE_CLK_SEL_48_MHz;
  #elif  ( F_CPU_MHZ == INT64_C( 24 ))
  #error "F_CPU_MHZ must be 48 in order to run the FOC!"
    SYS_STATE_CONTROL_bit.sys_clk_sel = (u16) SYS_STATE_CLK_SEL_24_MHz;
  #else
  #error "Define F_CPU_MHZ is not set or has an invalid value!"
  #endif

  SYS_STATE_MODULE_ENABLE_bit.gpio_a      = 1u;
  SYS_STATE_MODULE_ENABLE_bit.gpio_b      = 1u;
  SYS_STATE_MODULE_ENABLE_bit.gpio_c      = 1u;
  SYS_STATE_MODULE_ENABLE_bit.pwmn        = 1u;
  SYS_STATE_MODULE_ENABLE_bit.saradc_ctrl = 1u;

  SYS_STATE_MODULE_ENABLE_bit.spi_0 = 1u;
  SYS_STATE_MODULE_ENABLE_bit.spi_1 = 1u;

  /* configure GPIOs */
  /* every GPIO has a MUX before it that selects the function of the pin (i.e. the digital signal it is connected to) */
  /* the following lines configure those MUXs */

  /* Port A - SPI, clock signal for 523.01, lowside driver signals */

  IOMUX_CTRL_PA_IO_SEL =
    (
        IOMUX_CTRL_FUNC_A0_SPI_0_SCK |                                                                                         /* internal   -> spi      -> sclk       */
        IOMUX_CTRL_FUNC_A1_SPI_0_SDI |                                                                                         /* internal   -> spi      -> so         */
        IOMUX_CTRL_FUNC_A2_SPI_0_SDO |                                                                                         /* internal   -> spi      -> si         */
        IOMUX_CTRL_FUNC_A3_SPI_0_NSS |                                                                                         /* internal   -> spi      -> csb        */
        IOMUX_CTRL_FUNC_A4_GPIO |                                                                                              /* not bonded ->                        */
        IOMUX_CTRL_FUNC_A5_PWMN_U_N |                                                                                          /* internal   -> pwm      -> pwm_l_1    */
        IOMUX_CTRL_FUNC_A6_PWMN_V_N |                                                                                          /* internal   -> pwm      -> pwm_l_2    */
        IOMUX_CTRL_FUNC_A7_PWMN_W_N                                                                                            /* internal   -> pwm      -> pwm_l_3    */
    );

  /* Port B - GPIOs, highside driver signals, LIN and UART SCI (for communicating with PC via FTDI) */

  IOMUX_CTRL_PB_IO_SEL =
    (
        #if ( SCI_USAGE_TYPE == SCI_ELMOS_LIN )
          IOMUX_CTRL_FUNC_B0_SCI_RXD |                                                                                         /* internal   -> lin      -> rxd        */
          IOMUX_CTRL_FUNC_B1_SCI_TXD |                                                                                         /* internal   -> lin      -> txd        */
        #else
          IOMUX_CTRL_FUNC_B0_GPIO |                                                                                            /* unused                               */
          IOMUX_CTRL_FUNC_B1_GPIO |                                                                                            /* unused                               */
        #endif

        IOMUX_CTRL_FUNC_B2_PWMN_U_P |                                                                                          /* internal   -> pwm      -> pwm_h_1    */
        IOMUX_CTRL_FUNC_B3_PWMN_V_P |                                                                                          /* internal   -> pwm      -> pwm_h_2    */
        IOMUX_CTRL_FUNC_B4_PWMN_W_P |                                                                                          /* internal   -> pwm      -> pwm_h_3    */
        IOMUX_CTRL_FUNC_B5_GPIO |                                                                                              /**< output of the current sensing circuit is routed to PB5 */

        #if (( SCI_USAGE_TYPE == SCI_UNUSED ) || ( SCI_USAGE_TYPE == SCI_ELMOS_LIN ))
          IOMUX_CTRL_FUNC_B6_GPIO |
          IOMUX_CTRL_FUNC_B7_GPIO
        #elif ( SCI_USAGE_TYPE == SCI_DATALOGGER )
          IOMUX_CTRL_FUNC_B6_SCI_RXD |
          IOMUX_CTRL_FUNC_B7_SCI_TXD
        #else
        #error "SCI_USAGE_TYPE not defined!"
        #endif
    );


/*
    Set up SCI_1 to be able to communicate with external eeprorm on PC bits 2 - 4 : 

      
  Func.     signal      uP 
    1       NSS        PC5   
    2       SCK        PC2   
    3       SDO MOSI   PC3   
    4       SDI MISO   PC4    

*/

#if ( ENABLE_SPI_1_EEPROM == 1 )
  IOMUX_CTRL_PB_IO_SEL_bit.sel_6 = 0u;    /* Using PB6 for BEMF_W, set as GPIO input */
  IOMUX_CTRL_PB_IO_SEL_bit.sel_7 = 0u;    /* Using PP7 for BEMF_U to select GPIO input (IO7) */  
  (GPIO_B_DATA_OE &= ~(0xC0u));           /* Make PB6 and 7 inputs for use as BEMF pins. */
  IOMUX_CTRL_PC0123_IO_SEL = 0x3200u;
  IOMUX_CTRL_PC4567_IO_SEL = 0x0014u;  
#endif  


/*  #endif  */

  /* Enable Spread Spectrum system clock */
  OSC_CTRL_OSC_CONFIG_bit.spread_enable = 1u;

  #if ( DEBUG == 1 )
    dbg_init();

    _Pragma( "diag_suppress = Pm085" )                                                                                         /* disable rule "identifiers in pre-processor directives should be defined before use (MISRA C 2004 rule 19.11)" */

    #if ( DBG_EN_PIN3 == 1 )
      #if ( DBG_PIN3 == GPIO_IO_5 )
      #warning DEBUG_SIGNAL enabled on PIN PC5  /*  PRQA S 1008, 3115 */
      #elif ( DBG_PIN3 == GPIO_IO_6 )
      #warning DEBUG_SIGNAL enabled on PIN PC6  /*  PRQA S 1008, 3115 */
      #elif ( DBG_PIN3 == GPIO_IO_7 )
      #warning DEBUG_SIGNAL enabled on PIN PC7  /*  PRQA S 1008, 3115 */
      #endif
    #endif

    _Pragma( "diag_default = Pm085" )

  #endif

    /* initialize SPI interface and the 523.07 IO registers */
    d52307_init();

  /* initialize ADC module */
  adc_init();

  /* initialize PWM generation module */
  pwm_init( PWM_SAWTOOTH, PWM_DT_GEN_FIRST_EDGE, (u8) PWM_DEADTIME_INITVAL_TICKS );

  /* initialize serial communication interface */
  #if ( SCI_USAGE_TYPE == SCI_ELMOS_LIN )
    (void) LinDrvImp_Init( vic_IfFuns );
  #elif ( SCI_USAGE_TYPE == SCI_DATALOGGER )
    uart_init();
  #elif ( SCI_USAGE_TYPE == SCI_UNUSED )
  #warning "SCI_USAGE_TYPE == SCI_UNUSED!" /*  PRQA S 1008, 3115 */
  #else
  #error "SCI_USAGE_TYPE not defined!"
  #endif

  /* initialize motor control module */
  mc_init();

  /* initialize FOC computation module */
  foc_init();

  /* initialize FOC speed measurement module */
  spdmeas_init();

  #if ( USE_PWM_DIAG_INPUT == 1 )
    /* initialize diagnostic input PWM interface */
    diagi_init( PWM_DIAG_IN_PRESCALER );
  #endif

  #if ( USE_PWM_DIAG_OUTPUT == 1 )
    /* initialize diagnostic output PWM interface */
    diago_init( PWM_DIAG_OUT_PRESCALER );
  #endif

  #if ( USR_EN_INITIAL_POSITION_DETECTION == 1 )
    /* initialize rotor position detection interface */
    pos_init();
  #endif

  /* initialize user application module */
  app_init();

  #if ( USR_OVERRIDE_VOLTAGE_OVERFLOW_HANDLER != 1 )
  #warning It is strongly recommended to implement usr_output_voltage_overflow_handler()!  /*  PRQA S 1008, 3115 */
  #endif

  /* generate signatures of the various module's signatures (they are verified upon connecting via data logger) */
  #if ( SCI_USAGE_TYPE == SCI_DATALOGGER )
    hall_generate_struct_signature();
    adc_generate_struct_signature();
    foc_generate_struct_signature();
    mc_generate_struct_signature();
    spdmeas_generate_struct_signature();
    app_generate_struct_signature();
  #endif

  __enable_interrupt();

  adc_measurement_list_set_and_skip( adc_lists_get_commutation_list());
}
