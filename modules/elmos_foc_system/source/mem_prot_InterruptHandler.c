/***************************************************************************//**
 * @file			mem_prot_InterruptHandler.c
 *
 * @creator		rpy
 * @created		26.01.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 ******************************************************************************/

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "io_e52398a.h"
#include <intrinsics.h>

#include "vic_InterruptHandler.h"
#include "mem_prot_InterruptHandler.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************** FORWARD DECLARATIONS / PROTOTYPES *********************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ************************ MODULE GLOBALE VARIABLES **************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ************************** FUNCTION DEFINITIONS ****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************************* INTERRUPTS *********************************/
/* ****************************************************************************/

/***************************************************************************//**
 * @implementation
 * Simple word write access to IRQ_VENABLE register, but with the difference
 * to mem_prot_SetIRQVEnable, that a special enum (mem_prot_eInterruptVectorNum_t)
 * is passed as parameter, for security reasons and a more concrete interface
 * definition.
 *
 ******************************************************************************/
void mem_prot_InterruptEnable(mem_prot_eInterruptVectorNum_t irqsrc)
{
  MEM_PROT_IRQ_VENABLE = (uint16_t) irqsrc;
}

/***************************************************************************//**
 * @implementation
 * Simple word write access to IRQ_VDISABLE register, but with the difference
 * to mem_prot_SetIRQVDisable, that a special enum (mem_prot_eInterruptVectorNum_t)
 * is passed as parameter, for security reasons and a more concrete interface
 * definition.
 *
 ******************************************************************************/
void mem_prot_InterruptDisable(mem_prot_eInterruptVectorNum_t irqsrc)
{
  MEM_PROT_IRQ_VDISABLE = (uint16_t) irqsrc;
}

/***************************************************************************//**
 * @implementation
 * At first the module number is looked up in a table, because module number
 * and module instance base address have a logical connection. Then the
 * interrupt callback function pointer is saved in module interrupt vector
 * table.
 *
 ******************************************************************************/
void mem_prot_InterruptRegisterCallback(mem_prot_eInterruptVectorNum_t irqvecnum, mem_prot_InterruptCallback_t cbfnc)
{
  mem_prot_sInterruptEnvironmentData_t* evironmentData = (mem_prot_sInterruptEnvironmentData_t*) vic_GetPointerToEnvironmentData(vic_IRQ_MEM_PROT);

  if ((irqvecnum < mem_prot_INTERRUPT_VECTOR_CNT) && (evironmentData != NULL))
  {
    evironmentData->InterrupVectorTable[irqvecnum] = cbfnc;
  }
  else {}
}

/***************************************************************************//**
 * @implementation
 * Reverse function of "mem_prot_RegisterInterruptCallback". Sets the corresponding
 * entry in the module interrupt vector table back to NULL. Very imported,for
 * security reasons the corresponding interrupt is disabled. Otherwise the
 * program could possibly call NULL.
 *
 ******************************************************************************/
void mem_prot_InterruptDeregisterCallback(mem_prot_eInterruptVectorNum_t irqvecnum)
{
  mem_prot_sInterruptEnvironmentData_t* evironmentData = (mem_prot_sInterruptEnvironmentData_t*) vic_GetPointerToEnvironmentData(vic_IRQ_MEM_PROT);

  if ((irqvecnum < mem_prot_INTERRUPT_VECTOR_CNT) && (evironmentData != NULL))
  {
    mem_prot_InterruptDisable(irqvecnum);
    evironmentData->InterrupVectorTable[irqvecnum] = NULL;
  }
  else {}
}

/***************************************************************************//**
 * @implementation
 * Every hardware MEM_PROT module has it's own interrupt handler, also because each
 * module has it's own interrupt vector. So the module base address is fixed.
 * The interrupt vector table is provided by the user-application from RAM. The
 * module specific vector tables lie behind the VIC interrupt vector table, which
 * address is saved in the "TABLE_BASE". The module interrupt handler
 * gets the address from the function "vic_GetPointerToEviornmentData".
 * The interrupt vector number is saved in a local variable, because the value
 * of IRQ_VECTOR could possible change during processing of
 * mem_prot_MEM_PROT0_InterruptHandler. Next the registered interrupt callback function
 * (mem_prot_RegisterInterruptCallback) is copied into a local function pointer,
 * checked if it's not NULL and at least called. At the end the interrupt
 * request flag is cleared, by writing back the interrupt vector number to
 * IRQ_VNO register.
 *
 ******************************************************************************/
 __interrupt void mem_prot_InterruptHandler(void)
{
  mem_prot_eInterruptVectorNum_t irqvecnum = (mem_prot_eInterruptVectorNum_t) MEM_PROT_IRQ_VNO;
  
  if (irqvecnum < mem_prot_INTERRUPT_VECTOR_CNT) /* ensure the IRQ trigger is not already gone away until processing starts */
  {
    mem_prot_sInterruptEnvironmentData_t* evironmentData = (mem_prot_sInterruptEnvironmentData_t*) vic_GetPointerToEnvironmentData(vic_IRQ_MEM_PROT);

    mem_prot_InterruptCallback_t fptr;
  
    uint16_t vicVmaxBackup = 0;
    uint16_t mem_protVmaxBackup = 0;
  
    /* IRQ nesting on VIC level: enabled by default */
    vicVmaxBackup = VIC_IRQ_VMAX;
    VIC_IRQ_VMAX = (uint16_t) vic_IRQ_MEM_PROT;

    /* IRQ nesting on module level: by default disabled, handler may override this later on */
    mem_protVmaxBackup = MEM_PROT_IRQ_VMAX;
    MEM_PROT_IRQ_VMAX = 0;
  
    fptr = evironmentData->InterrupVectorTable[irqvecnum];
          
    if(fptr != NULL)
    {          
      __enable_interrupt();
        
      /* Call interrupt callback function */
      fptr(irqvecnum,  (void*) evironmentData->ContextData);      
      
      __disable_interrupt();
    }
    else 
    {
      /* if there is no handler function for a particular IRQ, disable this IRQ  */ 
      MEM_PROT_IRQ_VDISABLE = (uint16_t) irqvecnum;
    }

    /* Clear interrupt request flag */
    MEM_PROT_IRQ_VNO = (uint16_t) irqvecnum;
  
    /* IRQ nesting on module level */
    MEM_PROT_IRQ_VMAX = mem_protVmaxBackup;
  
    /* IRQ nesting on VIC level */
    VIC_IRQ_VMAX = vicVmaxBackup;
  }
  else {}
  
  /* RETI will reenable IRQs here */ 
} 

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
void mem_prot_InterruptInitialisation(vic_cpInterfaceFunctions_t vicIf, mem_prot_pInterruptEnvironmentData_t environmentdata, mem_prot_pInterruptContextData_t contextdata)
{
  if ((vicIf != NULL) && (vicIf->InterfaceVersion == VIC_INTERFACE_VERSION) && (environmentdata != NULL))
  {
    environmentdata->ContextData = contextdata;

    /* Register module at interrupt handler */
    vicIf->RegisterModule(vic_IRQ_MEM_PROT, &mem_prot_InterruptHandler, environmentdata);
    vicIf->EnableModule(vic_IRQ_MEM_PROT);
  }
  else {}
}
