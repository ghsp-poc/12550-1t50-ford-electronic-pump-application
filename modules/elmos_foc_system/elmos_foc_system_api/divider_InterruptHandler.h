/***************************************************************************//**
 * @file			divider_InterruptHandler.h
 *
 * @creator		rpy
 * @created		04.09.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 ******************************************************************************/

#ifndef DIVIDER_INTERRUPTHANDLER_H_          
#define DIVIDER_INTERRUPTHANDLER_H_

#pragma once

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "el_types.h"
#include "vic_Interface.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/***************************************************************************//**
 * DIVIDER IRQ vector numbers
 ******************************************************************************/
typedef enum {
  divider_IRQ_EVT_DIV_BY_ZERO        =           0u,

  divider_INTERRUPT_VECTOR_CNT       =           1u  /**< Number of available interrupt vectors */
} divider_eInterruptVectorNum_t;

/***************************************************************************//**
 * Pointer to DIVIDER context data
 ******************************************************************************/
typedef void * divider_pInterruptContextData_t;

/***************************************************************************//**
 * Callback function pointer type
 ******************************************************************************/
typedef void (*divider_InterruptCallback_t) (divider_eInterruptVectorNum_t irqsrc, divider_pInterruptContextData_t contextdata);

/***************************************************************************//**
 * DIVIDER environment data
 ******************************************************************************/
typedef struct divider_sInterruptEnvironmentData
{
    /** Interrupt vector table of this module */
    divider_InterruptCallback_t InterrupVectorTable[divider_INTERRUPT_VECTOR_CNT];
    
    /** DIVIDER module context data */
    divider_pInterruptContextData_t ContextData;
    
} divider_sInterruptEnvironmentData_t;

/***************************************************************************//**
 * Pointer to DIVIDER environment data
 ******************************************************************************/
typedef divider_sInterruptEnvironmentData_t * divider_pInterruptEnvironmentData_t;

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************************* INTERRUPTS *********************************/
/* ****************************************************************************/

/***************************************************************************//**
 * @brief     Enables an IRQ.
 *
 * @param[in] modulBaseAddress  Pointer to DIVIDER module base address
 * @param[in] irqsrc            IRQ to be enabled
 *
 * @pre       A call back function to the related interrupt should have been
 *            registered with divider_RegisterInterruptCallback().
 *
 * @post      The related call back function will be called if the desired
 *            interrupt occurs.
 *
 * @detaildesc
 * The DIVIDER IRQ_VENABLE register will be set the related IRQ number and therefore
 * the interrupt will be activated.
 *
 ******************************************************************************/
void divider_InterruptEnable(divider_eInterruptVectorNum_t irqsrc);

/***************************************************************************//**
 * @brief Disables an IRQ.
 *
 * @param[in] modulBaseAddress  Pointer to DIVIDER module base address
 * @param[in] irqsrc            IRQ to be disable
 *
 * @post      The interrupt will be disabled and the related callback function
 *            will no longer be called from the interrupt handler.
 *
 * @detaildesc
 * The DIVIDER IRQ_VDISABLE register will be set the related IRQ number and therefore
 * the interrupt will be deactivated.
 *
 ******************************************************************************/
void divider_InterruptDisable(divider_eInterruptVectorNum_t irqsrc);

/***************************************************************************//**
 * @brief Registers/Adds callback function to the module interrupt vector table.
 *
 * @param modulBaseAddress  Pointer to DIVIDER module base address
 * @param irqsrc            IRQ number
 * @param cbfnc             Pointer to desired callback function
 *
 * @pre     (optional) Which are the conditions to call this function? i.e. none
 *
 * @post    If the interrupt will be activated the registered callback function
 *          will be called if the IRQ occurs.
 *
 * @detaildesc
 * Registers the callback function at interrupt vector handling. It sets the
 * entry in the interrupt vector table to passed function pointer.
 *
 ******************************************************************************/
void divider_InterruptRegisterCallback(divider_eInterruptVectorNum_t irqvecnum, divider_InterruptCallback_t cbfnc);

/***************************************************************************//**
 * @brief Deregisters/deletes callback function from module interrupt vector table.
 *
 * @param modulBaseAddress  Pointer to DIVIDER module base address
 * @param irqvecnum         IRQ number
 * @param cbfnc             Pointer to desired callback function
 *
 * @pre   The related IRQ should be disabled.
 *
 * @post  The entry in the module interrupt vector table will point to NULL and
 *        the related IRQ will be disabled.
 *
 * @detaildesc
 * Deregisters the callback function from interrupt vector handling. It sets the
 * entry in the interrupt vector table to NULL and disables the related interrupt.
 *
 ******************************************************************************/
void divider_InterruptDeregisterCallback(divider_eInterruptVectorNum_t irqvecnum);

/***************************************************************************//**
 * Handles the DIVIDER related interrupt requests.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         TODO: Add description of precondition
 *
 * TODO: A more detailed description.
 *
 ******************************************************************************/
 __interrupt void divider_InterruptHandler(void);

/***************************************************************************//**
 * @brief Initialize DIVIDER module
 *
 * @param environmentdata  Pointer to Environment data for DIVIDER module in
 *                         user RAM
 *
 * @pre        VIC (vic_VectorInterruptControl) and DIVIDER (divider_SystemStateModule)
 *             have to presented and initialized.
 *
 * @post       DIVIDER module is configured for use.
 *
 * @detaildesc
 * Initializes the DIVIDER module interrupt vector table. 
 *
 ******************************************************************************/
void divider_InterruptInitialisation(vic_cpInterfaceFunctions_t vicIf, divider_pInterruptEnvironmentData_t environmentdata, divider_pInterruptContextData_t contextdata);


#endif /* LIN_CTRL_INTERRUPTHANDLER_H_ */

