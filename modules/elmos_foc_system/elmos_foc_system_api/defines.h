/**
 * @defgroup CFG Configuration files
 * @ingroup CFG
 *
 * @{
 */

/*****************************************************************************/
/*! @file defines.h
 * @brief global parameters and defines which are relevant for compilation and
 * FOC/motor control algorithm
 *
 * @author       JBER
 */
/*****************************************************************************/
#ifndef DEFINES_H
#define DEFINES_H

#include <types.h>

/* supported ELMOS ICs */
#define E52305A ( 1 )
#define E52306A ( 2 )
#define E52352A ( 3 )

/* include the IO header of the respective MCU */
#include <io_e52398a.h>
#include <io_e52398a_extend.h>

/* ----------------------------------------------------------------------------
   macro definitions (do not change)
   ---------------------------------------------------------------------------- */

/* current measurement type */
#define USR_SHUNT_MEAS_TYPE_SINGLE   ( 1 )
#define USR_SHUNT_MEAS_TYPE_DUAL_U_V ( 2 )
#define USR_SHUNT_MEAS_TYPE_DUAL_V_W ( 3 )

/* SCI usage type */
#define SCI_UNUSED     ( 0 )
#define SCI_DATALOGGER ( 1 )
#define SCI_ELMOS_LIN  ( 2 )

/* user configuration */
#define USR_CFG_DEMOBOARD_52305_SINGLE_SHUNT ( 1 )
#define USR_CFG_DEMOBOARD_52305_DUAL_SHUNT   ( 2 )
#define USR_CFG_DEMOBOARD_52306_SINGLE_SHUNT ( 3 )
#define USR_CFG_DEMOBOARD_52306_DUAL_SHUNT   ( 4 )
#define USR_CFG_DEMOBOARD_52352_SINGLE_SHUNT ( 5 )
#define USR_CFG_DEMOBOARD_52352_DUAL_SHUNT   ( 6 )

#define USR_CFG_DEMOBOARD_52305_SINGLE_SHUNT_SPEEDPOTI_DEMO ( 12 )
#define USR_CFG_DEMOBOARD_52306_SINGLE_SHUNT_SPEEDPOTI_DEMO ( 13 )
#define USR_CFG_DEMOBOARD_52352_SINGLE_SHUNT_SPEEDPOTI_DEMO ( 14 )

#define USR_CFG_DEMOBOARD_52305_SINGLE_SHUNT_VDA_BLOWER     ( 15 )
#define USR_CFG_DEMOBOARD_52306_SINGLE_SHUNT_VDA_BLOWER     ( 16 )


/*******************************************************************************/
/*******************************************************************************/

/* ----------------------------------------------------------------------------
   USER APPLICATION CONFIGURATION
   ---------------------------------------------------------------------------- */

#define USR_CONFIGURATION USR_CFG_DEMOBOARD_52306_SINGLE_SHUNT_VDA_BLOWER

/* ----------------------------------------------------------------------------
   firmware revision
   ---------------------------------------------------------------------------- */
#define  FIRMWARE_MAJOR_ID      ( 0 )
#define  FIRMWARE_MINOR_ID      ( 0 )
#define  FIRMWARE_MAJOR_VERSION ( 4 )
#define  FIRMWARE_MINOR_VERSION ( 2 )

#define  UART_PROTOCOL_MAJOR_VERSION ( 1 )
#define  UART_PROTOCOL_MINOR_VERSION ( 0 )

/* ----------------------------------------------------------------------------
   debugging
   ---------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------
   CPU clock
   ---------------------------------------------------------------------------- */
#ifndef F_CPU
  #define F_CPU INT64_C( 48000000 )                                                                             /**< core clock frequency [Hz] */
#endif

#ifndef F_CPU_MHZ
  #define F_CPU_MHZ ( F_CPU / INT64_C( 1000000 ))                                                               /**< core clock frequency [MHz] */
#endif

/* ----------------------------------------------------------------------------
   watchdog of the gate driver die
   ---------------------------------------------------------------------------- */
#define WD_TRIG_CYCLE_TIME       INT64_C( 50 )                                                                  /**< every WD_TRIG_CYCLE_TIME milliseconds the 523.01/.07 watchdog is triggered */
#define WD_TRIG_CYCLE_TIME_TICKS ( SYSTIME_TICKS_PER_SEC * WD_TRIG_CYCLE_TIME / INT64_C( 1000 ))

/* ----------------------------------------------------------------------------
   SPI module
   ---------------------------------------------------------------------------- */
#define SPI0_CLK_MHZ INT64_C( 4 )                                                                               /* SPI speed [MHz] */

/* ----------------------------------------------------------------------------
   PWM generation
   ---------------------------------------------------------------------------- */

#define PWM_OUT_FREQ     INT64_C( 20000 )                                                                       /**< frequency of PWM output signal [Hz] */
#define PWM_OUT_FREQ_KHZ ( PWM_OUT_FREQ / INT64_C( 1000 ))                                                      /**< frequency of PWM output signal [kHz] */

#define PWM_DEADTIME_INITVAL       INT64_C( 1000 )                                                              /**< PWM deadtime in nanoseconds */
#define PWM_DEADTIME_INITVAL_TICKS (( F_CPU_MHZ * PWM_DEADTIME_INITVAL ) / INT64_C( 1000 ))                     /**< deadtime between HS and LS switch [register DEAD_TIME of block PWMN] */

#define PWM_PERIOD_DURATION_TICKS (( F_CPU_MHZ * INT64_C( 1000 )) / PWM_OUT_FREQ_KHZ )                          /**< amount of timer ticks per PWM period */

#define PWM_SINGLE_SHUNT_MAX_CMP_VAL ( PWM_PERIOD_DURATION_TICKS - INT64_C( 1 ))                                /**< maximum timer compare value (single shunt operation) */
#define PWM_SINGLE_SHUNT_MIN_CMP_VAL ( ADC_THIRD_PWM_EDGE + INT64_C( 1 ))                                       /**< minimum timer compare value (single shunt operation) */

#define PWM_SINGLE_SHUNT_CMP_MEDIAN ((((( PWM_SINGLE_SHUNT_MAX_CMP_VAL - PWM_SINGLE_SHUNT_MIN_CMP_VAL ) \
                                        * INT64_C( 11547 )) / INT64_C( 10000 )) \
                                      / INT64_C( 2 )) + PWM_SINGLE_SHUNT_MIN_CMP_VAL )                          /**< todo */


#define PWM_DUAL_SHUNT_MAX_CMP_VAL (( PWM_PERIOD_DURATION_TICKS / INT64_C( 2 )) \
                                    - ((( PWM_DEADTIME_INITVAL_TICKS + ADC_SAMPLE_DELAY_SHUNT_VOLTAGE_TICKS ) \
                                        + ( INT64_C( 2 ) * ADC_SAMPLE_CLK_CYCLES ) \
                                        + ADC_CONVERSION_CLK_CYCLES ) / INT64_C( 2 )))                          /**< maximum timer compare value (dual shunt operation) */
#define PWM_DUAL_SHUNT_MIN_CMP_VAL INT64_C( 1 )                                                                 /**< minimum timer compare value (single shunt operation) */
#define PWM_DUAL_SHUNT_CMP_MEDIAN  ((( PWM_DUAL_SHUNT_MAX_CMP_VAL - PWM_DUAL_SHUNT_MIN_CMP_VAL ) / INT64_C( 2 )) \
                                    + PWM_DUAL_SHUNT_MIN_CMP_VAL )                                              /**< middle between PWM_DUAL_SHUNT_MAX_CMP_VAL and PWM_DUAL_SHUNT_MIN_CMP_VAL */

/* ----------------------------------------------------------------------------
   MOTOR_CTRL module
   ---------------------------------------------------------------------------- */
#define MC_CTRL_ROT_SPEED_INIT_KP INT64_C( 1000 )
#define MC_CTRL_ROT_SPEED_INIT_KI INT64_C( 8 )

#define MC_ROTOR_SYNC_BEMF_CROSSING_MIN_CNT INT64_C( 3 )                                                        /**< amount of BEMF crossings that need to occur before the control synchronizes to a turning rotor */

#define MC_52305_VBAT_NOMINAL INT64_C( 13500 )                                                                  /**< since the 523.05 can not measure the supply voltage when the gate driver is switched off, this value is assumed to be it when sync'ing to a turning rotor [V] */

#define MC_COMPUTE_SUPPLY_CURRENT ( 1 )                                                                         /**< when this define is set, the current drawn from the battery (i.e. DC link current) is computed */

#define MC_POS_DETECT_PULSE_LENGTH_INIT INT64_C( 1900 )                                                         /**< duration that one test pulse; the value modulo 2400 (or PWM_PERIOD_DURATION_TICKS) must be different from 0! [CPU clock ticks] */
#define MC_POS_DETECT_MEAS_TIME_INIT    INT64_C( 800 )                                                          /**< time of the first of the three measurements [CPU clock ticks] */
#define MC_POS_DETECT_MEAS_DELAY_INIT   INT64_C( 400 )                                                          /**< delay between the first/second and second/third measurement -> the higher this value, the better the evaluation of the gradients for polarity deduction [CPU clock ticks] */

/* ----------------------------------------------------------------------------
   ADC module
   ---------------------------------------------------------------------------- */
#define ADC_GATE_DRIVER_PROPAGATION_DLY INT64_C( 300 )                                                        /**< propagation delay through the gate driver, i.e. delay between the MotCU PWM signals and the actual gate driver output [ns] */


#define ADC_GATE_DRIVER_PROPAGATION_DLY_TICKS (( ADC_GATE_DRIVER_PROPAGATION_DLY * F_CPU_MHZ ) / INT64_C( 1000 ))

#define ADC_SAMPLE_EXTENSION_CLK_CYCLES INT64_C( 3 )                                                            /**< the sampling time of the ADC is extended by that amount of (ADC) clock cycles / 2 (6 MHz) -> only for all IMMEDIATE_SAMPLES (user measurements and VSUP) */

#define ADC_SAMPLE_DELAY_SHUNT_VOLTAGE INT64_C( 1210 )                                                          /**< time that is waited before a shunt voltage is sampled [ns] */

#define ADC_SAMPLE_DURATION     INT64_C( 166 )                                                                  /**< duration of an ADC sample phase during phase current measurement [ns], only used for calculation of measurement timings -> ADC_SAMPLE_EXTENSION_CLK_CYCLES does _not_ have an impact here! */
#define ADC_CONVERSION_DURATION INT64_C( 1438 )                                                                 /**< duration of an AD conversion, until next conversion can be started [ns] */

#define ADC_SAMPLE_DELAY_SHUNT_VOLTAGE_TICKS (( ADC_SAMPLE_DELAY_SHUNT_VOLTAGE * F_CPU_MHZ ) / INT64_C( 1000 )) /**< time that is waited before a shunt voltage is sampled [PWM ticks] */

#define ADC_SAMPLE_CLK_CYCLES     (( ADC_SAMPLE_DURATION * F_CPU_MHZ ) / INT64_C( 1000 ))                       /**< duration of an ADC sample phase during phase current measurement [PWM ticks], only used for calculation of measurement timings -> ADC_SAMPLE_EXTENSION_CLK_CYCLES does _not_ have an impact here! */
#define ADC_CONVERSION_CLK_CYCLES (( ADC_CONVERSION_DURATION * F_CPU_MHZ ) / INT64_C( 1000 ))                   /**< duration of an AD conversion, until next conversion can be started [PWM ticks] */

/* single shunt */
#define ADC_FIRST_PWM_EDGE        INT64_C( 0 )
#define ADC_FIRST_CURRENT_SAMPLE  (( ADC_GATE_DRIVER_PROPAGATION_DLY_TICKS + PWM_DEADTIME_INITVAL_TICKS ) + ADC_SAMPLE_DELAY_SHUNT_VOLTAGE_TICKS )
#define ADC_SECOND_PWM_EDGE       (( ADC_FIRST_CURRENT_SAMPLE - ADC_GATE_DRIVER_PROPAGATION_DLY_TICKS ) + ADC_SAMPLE_CLK_CYCLES )
#define ADC_SECOND_CURRENT_SAMPLE ((( ADC_SECOND_PWM_EDGE + ADC_GATE_DRIVER_PROPAGATION_DLY_TICKS ) + PWM_DEADTIME_INITVAL_TICKS ) + ADC_SAMPLE_DELAY_SHUNT_VOLTAGE_TICKS )
#define ADC_THIRD_PWM_EDGE        ( ADC_SECOND_PWM_EDGE * INT64_C( 2 ))

/* user measurements */
#define ADC_USR_MEAS_DLY INT64_C( 400 )

#define SYSTIME_TICKS_PER_SEC ( PWM_OUT_FREQ )

#define ADC_NOISE_AMPL INT64_C( 8 )

/* ----------------------------------------------------------------------------
   FOC module
   ---------------------------------------------------------------------------- */
#define FOC_USE_SVM 0                                                                                           /**< when this define is set, the FOC uses a real SVM instead of the inverse Clark transformation; BEWARE: execution time 7.6us instead of 0.7us */

#define FOC_CONTROLLLER_I_D_INIT_KP INT64_C( 600 )
#define FOC_CONTROLLLER_I_D_INIT_KI INT64_C( 3 )

#define FOC_CONTROLLLER_I_Q_INIT_KP FOC_CONTROLLLER_I_D_INIT_KP
#define FOC_CONTROLLLER_I_Q_INIT_KI FOC_CONTROLLLER_I_D_INIT_KI

#define FOC_I_MODEL_SCALE_BITS_INIT INT64_C( 9 )

#define FOC_OPEN_TO_CLOSED_LOOP_SMOOTHING_COEFF_INIT INT64_C( 50 )

/* ----------------------------------------------------------------------------
   HALL module
   ---------------------------------------------------------------------------- */
#define FOC_HALL_EDGE_AMOUNT_BEFORE_INTERPOLATION INT64_C( 24 )

#define FOC_HALL_SIGNAL_SMOOTHING             ( 1 )
#define FOC_HALL_INTERPOLATION_MIN_SPEED_INIT INT64_C( 300 )

/* todo: docu */
#define HALL_CCT_DISABLED                ( 1 )
#define HALL_CCT_ENABLED_STD             ( 2 )
#define HALL_CCT_ENABLED_W_BLANKING_TIME ( 3 )

#define HALL_CCT_MODE HALL_CCT_ENABLED_W_BLANKING_TIME

#define HALL_HIGHSPEED_ENABLE_THRSHLD_HYS_UPPER INT64_C( 10000 )
#define HALL_HIGHSPEED_ENABLE_THRSHLD_HYS_LOWER INT64_C( 9000 )

#define HALL_CCT_MAX_ERPM INT64_C( 60000 )

#define HALL_CCT_PRESCALER           ( F_CPU * INT64_C( 60 ) * INT64_C( 2 ) / (s64) ( U16_MAX ) / HALL_HIGHSPEED_ENABLE_THRSHLD_HYS_LOWER )
#define HALL_CCT_BLANKING_TIME_TICKS ((( SYSTIME_TICKS_PER_SEC * INT64_C( 60 )) / INT64_C( 2 )) / HALL_CCT_MAX_ERPM )

/* ----------------------------------------------------------------------------
   ROTOR POSITION DETECTION module
   ---------------------------------------------------------------------------- */
#define POS_REV_POLARITY_FET_CHARGE_DURATION_INIT INT64_C( 65 )

#define POS_DETECT_ADC_SAMPLING_EXTENSION_VALUE INT64_C( 20 )

/* ----------------------------------------------------------------------------
   data logger module-related
   ---------------------------------------------------------------------------- */
#define UART_BAUD_RATE INT64_C( 3000000 )

/* ----------------------------------------------------------------------------
   including the user configuration file specified via USR_CONFIGURATION
   ---------------------------------------------------------------------------- */

#include "usr_cfg.h"

#endif /* _#ifndef DEFINES_H_ */

/* }@
 */
