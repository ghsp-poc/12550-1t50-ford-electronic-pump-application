/**
 * @ingroup CFG
 *
 * @{
 */

#ifndef IO_E52399A_EXTEND_H
#define IO_E52399A_EXTEND_H

enum {
  SYS_STATE_CLK_SEL_4_MHz = 0,
  SYS_STATE_CLK_SEL_8_MHz = 1,
  SYS_STATE_CLK_SEL_12_MHz= 2,
  SYS_STATE_CLK_SEL_24_MHz= 3,
  SYS_STATE_CLK_SEL_48_MHz= 4
};

/********************************/
/* port A+B constants           */
/*                              */
/* 1st function : GPIO          */
/* 2nd function : 523.01 + misc */
/* 3rd function : 523.50 + misc */
/* 4th function : misc          */
/********************************/

/* port A constants */

#define GPIO_A 1u
#define GPIO_B 2u
#define GPIO_C 3u

#define  GPIO_IO_0 0x0001u
#define  GPIO_IO_1 0x0002u
#define  GPIO_IO_2 0x0004u
#define  GPIO_IO_3 0x0008u
#define  GPIO_IO_4 0x0010u
#define  GPIO_IO_5 0x0020u
#define  GPIO_IO_6 0x0040u
#define  GPIO_IO_7 0x0080u

typedef uint16_t iomux_func_a_t;

#define IOMUX_CTRL_FUNC_A0_GPIO         ((iomux_func_a_t) (((u16) 0u ) << 0 ))
#define IOMUX_CTRL_FUNC_A0_SPI_0_SCK    ((iomux_func_a_t) (((u16) 1u ) << 0 ))
#define IOMUX_CTRL_FUNC_A0_PWMN_NALLOFF ((iomux_func_a_t) (((u16) 2u ) << 0 ))
#define IOMUX_CTRL_FUNC_A0_SCI_RXD      ((iomux_func_a_t) (((u16) 3u ) << 0 ))

#define IOMUX_CTRL_FUNC_A1_GPIO           ((iomux_func_a_t) (((u16) 0u ) << 2 ))
#define IOMUX_CTRL_FUNC_A1_SPI_0_SDI      ((iomux_func_a_t) (((u16) 1u ) << 2 ))
#define IOMUX_CTRL_FUNC_A1_CCTIMER_0_MEAS ((iomux_func_a_t) (((u16) 2u ) << 2 ))
#define IOMUX_CTRL_FUNC_A1_SCI_TXD        ((iomux_func_a_t) (((u16) 3u ) << 2 ))

#define IOMUX_CTRL_FUNC_A2_GPIO             ((iomux_func_a_t) (((u16) 0u ) << 4 ))
#define IOMUX_CTRL_FUNC_A2_SPI_0_SDO        ((iomux_func_a_t) (((u16) 1u ) << 4 ))
#define IOMUX_CTRL_FUNC_A2_SARADC_CTRL_MUX0 ((iomux_func_a_t) (((u16) 2u ) << 4 ))
#define IOMUX_CTRL_FUNC_A2_PWMN_X_N         ((iomux_func_a_t) (((u16) 3u ) << 4 ))

#define IOMUX_CTRL_FUNC_A3_GPIO             ((iomux_func_a_t) (((u16) 0u ) << 6 ))
#define IOMUX_CTRL_FUNC_A3_SPI_0_NSS        ((iomux_func_a_t) (((u16) 1u ) << 6 ))
#define IOMUX_CTRL_FUNC_A3_SARADC_CTRL_MUX1 ((iomux_func_a_t) (((u16) 2u ) << 6 ))
#define IOMUX_CTRL_FUNC_A3_SYS_STATE_CLK    ((iomux_func_a_t) (((u16) 3u ) << 6 ))

#define IOMUX_CTRL_FUNC_A4_GPIO          ((iomux_func_a_t) (((u16) 0u ) << 8 ))
#define IOMUX_CTRL_FUNC_A4_SYS_STATE_CLK ((iomux_func_a_t) (((u16) 1u ) << 8 ))
#define IOMUX_CTRL_FUNC_A4_PWMN_U_N      ((iomux_func_a_t) (((u16) 2u ) << 8 ))
#define IOMUX_CTRL_FUNC_A4_PWMN_X_P      ((iomux_func_a_t) (((u16) 3u ) << 8 ))

#define IOMUX_CTRL_FUNC_A5_GPIO          ((iomux_func_a_t) (((u16) 0u ) << 10 ))
#define IOMUX_CTRL_FUNC_A5_PWMN_U_N      ((iomux_func_a_t) (((u16) 1u ) << 10 ))
#define IOMUX_CTRL_FUNC_A5_PWMN_V_N      ((iomux_func_a_t) (((u16) 2u ) << 10 ))
#define IOMUX_CTRL_FUNC_A5_CCTIMER_0_PWM ((iomux_func_a_t) (((u16) 3u ) << 10 ))

#define IOMUX_CTRL_FUNC_A6_GPIO          ((iomux_func_a_t) (((u16) 0u ) << 12 ))
#define IOMUX_CTRL_FUNC_A6_PWMN_V_N      ((iomux_func_a_t) (((u16) 1u ) << 12 ))
#define IOMUX_CTRL_FUNC_A6_PWMN_W_N      ((iomux_func_a_t) (((u16) 2u ) << 12 ))
#define IOMUX_CTRL_FUNC_A6_CCTIMER_1_PWM ((iomux_func_a_t) (((u16) 3u ) << 12 ))

#define IOMUX_CTRL_FUNC_A7_GPIO             ((iomux_func_a_t) (((u16) 0u ) << 14 ))
#define IOMUX_CTRL_FUNC_A7_PWMN_W_N         ((iomux_func_a_t) (((u16) 1u ) << 14 ))
#define IOMUX_CTRL_FUNC_A7_CCTIMER_1_MEAS   ((iomux_func_a_t) (((u16) 2u ) << 14 ))
#define IOMUX_CTRL_FUNC_A7_SARADC_CTRL_MUX0 ((iomux_func_a_t) (((u16) 3u ) << 14 ))

/* port B constants */

typedef uint16_t iomux_func_b_t;

#define IOMUX_CTRL_FUNC_B0_GPIO             ((iomux_func_b_t) (((u16) 0u ) << 0 ))
#define IOMUX_CTRL_FUNC_B0_SCI_RXD          ((iomux_func_b_t) (((u16) 1u ) << 0 ))
#define IOMUX_CTRL_FUNC_B0_CCTIMER_2_MEAS   ((iomux_func_b_t) (((u16) 2u ) << 0 ))
#define IOMUX_CTRL_FUNC_B0_SARADC_CTRL_MUX1 ((iomux_func_b_t) (((u16) 3u ) << 0 ))

#define IOMUX_CTRL_FUNC_B1_GPIO             ((iomux_func_b_t) (((u16) 0u ) << 2 ))
#define IOMUX_CTRL_FUNC_B1_SCI_TXD          ((iomux_func_b_t) (((u16) 1u ) << 2 ))
#define IOMUX_CTRL_FUNC_B1_PWMN_U_P         ((iomux_func_b_t) (((u16) 2u ) << 2 ))
#define IOMUX_CTRL_FUNC_B1_SARADC_CTRL_MUX0 ((iomux_func_b_t) (((u16) 3u ) << 2 ))

#define IOMUX_CTRL_FUNC_B2_GPIO     ((iomux_func_b_t) (((u16) 0u ) << 4 ))
#define IOMUX_CTRL_FUNC_B2_PWMN_U_P ((iomux_func_b_t) (((u16) 1u ) << 4 ))
#define IOMUX_CTRL_FUNC_B2_PWMN_V_P ((iomux_func_b_t) (((u16) 2u ) << 4 ))
#define IOMUX_CTRL_FUNC_B2_SCI_RXD  ((iomux_func_b_t) (((u16) 3u ) << 4 ))

#define IOMUX_CTRL_FUNC_B3_GPIO          ((iomux_func_b_t) (((u16) 0u ) << 6 ))
#define IOMUX_CTRL_FUNC_B3_PWMN_V_P      ((iomux_func_b_t) (((u16) 1u ) << 6 ))
#define IOMUX_CTRL_FUNC_B3_PWMN_W_P      ((iomux_func_b_t) (((u16) 2u ) << 6 ))
#define IOMUX_CTRL_FUNC_B3_CCTIMER_2_PWM ((iomux_func_b_t) (((u16) 3u ) << 6 ))

#define IOMUX_CTRL_FUNC_B4_GPIO         ((iomux_func_b_t) (((u16) 0u ) << 8 ))
#define IOMUX_CTRL_FUNC_B4_PWMN_W_P     ((iomux_func_b_t) (((u16) 1u ) << 8 ))
#define IOMUX_CTRL_FUNC_B4_PWMN_NALLOFF ((iomux_func_b_t) (((u16) 2u ) << 8 ))
#define IOMUX_CTRL_FUNC_B4_SCI_TXD      ((iomux_func_b_t) (((u16) 3u ) << 8 ))

#define IOMUX_CTRL_FUNC_B5_GPIO            ((iomux_func_b_t) (((u16) 0u ) << 10 ))
#define IOMUX_CTRL_FUNC_B5_PWMN_NALLOFF    ((iomux_func_b_t) (((u16) 1u ) << 10 ))
#define IOMUX_CTRL_FUNC_B5_SARADC_DBG_SMPL ((iomux_func_b_t) (((u16) 2u ) << 10 ))
#define IOMUX_CTRL_FUNC_B5_SYNC_W          ((iomux_func_b_t) (((u16) 3u ) << 10 ))

#define IOMUX_CTRL_FUNC_B6_GPIO     ((iomux_func_b_t) (((u16) 0u ) << 12 ))
#define IOMUX_CTRL_FUNC_B6_SCI_RXD  ((iomux_func_b_t) (((u16) 1u ) << 12 ))
#define IOMUX_CTRL_FUNC_B6_PWMN_X_N ((iomux_func_b_t) (((u16) 2u ) << 12 ))
#define IOMUX_CTRL_FUNC_B6_SYNC_W   ((iomux_func_b_t) (((u16) 3u ) << 12 ))

#define IOMUX_CTRL_FUNC_B7_GPIO            ((iomux_func_b_t) (((u16) 0u ) << 14 ))
#define IOMUX_CTRL_FUNC_B7_SCI_TXD         ((iomux_func_b_t) (((u16) 1u ) << 14 ))
#define IOMUX_CTRL_FUNC_B7_PWMN_X_P        ((iomux_func_b_t) (((u16) 2u ) << 14 ))
#define IOMUX_CTRL_FUNC_B7_SARADC_DBG_SMPL ((iomux_func_b_t) (((u16) 3u ) << 14 ))

#define IOMUX_CTRL_FUNC_C_GPIO            0
#define IOMUX_CTRL_FUNC_C_SPI_1_NSS       1
#define IOMUX_CTRL_FUNC_C_SPI_1_SCK       2
#define IOMUX_CTRL_FUNC_C_SPI_1_SDO       3
#define IOMUX_CTRL_FUNC_C_SPI_1_SDI       4
#define IOMUX_CTRL_FUNC_C_CCTIMER_0_MEAS  5
#define IOMUX_CTRL_FUNC_C_CCTIMER_0_PWM   6
#define IOMUX_CTRL_FUNC_C_CCTIMER_1_MEAS  7
#define IOMUX_CTRL_FUNC_C_CCTIMER_1_PWM   8
#define IOMUX_CTRL_FUNC_C_CCTIMER_2_MEAS  9
#define IOMUX_CTRL_FUNC_C_CCTIMER_2_PWM   10
#define IOMUX_CTRL_FUNC_C_CCTIMER_3_MEAS  11
#define IOMUX_CTRL_FUNC_C_CCTIMER_3_PWM   12
#define IOMUX_CTRL_FUNC_C_SYNC_U          13
#define IOMUX_CTRL_FUNC_C_SYNC_V          14
#define IOMUX_CTRL_FUNC_C_SARADC_SYNC_OUT 15

#define FLASH_CTRL_MODE_ADDR              ( 0x01C4u )
#define FLASH_INFO_PAGE_BASE_ADDR         ( 0xFE00u )
#define FLASH_INFO_PAGE_ROM_MASK_REV      ( 0x0202u )
#define FLASH_INFO_PAGE_ROM_MASK_REV_ADDR ( FLASH_INFO_PAGE_BASE_ADDR + 0u )
#define FLASH_INFO_PAGE_IC_MAJOR_ID_ADDR  ( FLASH_INFO_PAGE_BASE_ADDR + 38u )
#define FLASH_INFO_PAGE_IC_MINOR_ID_ADDR  ( FLASH_INFO_PAGE_BASE_ADDR + 40u )


#define CCTIMER_CONFIG_CLK_SEL__SYS_CLK 0u

#define CCTIMER_CONFIG_RESTART_SEL__POS_EDGE   2u
#define CCTIMER_CONFIG_RESTART_SEL__NEG_EDGE   3u
#define CCTIMER_CONFIG_RESTART_SEL__SYS_SIGNAL 4u

#define CCTIMER_CONFIG_CAPTURE_SEL__POS_EDGE   1u
#define CCTIMER_CONFIG_CAPTURE_SEL__NEG_EDGE   2u
#define CCTIMER_CONFIG_CAPTURE_SEL__SYS_SIGNAL 3u

#define CCTIMER_CONFIG_MODE__COMPARE      0u
#define CCTIMER_CONFIG_MODE__COMPARE_ONCE 1u
#define CCTIMER_CONFIG_MODE__COMPARE_LOOP 2u
#define CCTIMER_CONFIG_MODE__CAPTURE      3u

#endif /* "#define IO_E52399A_EXTEND_H" */

/* }@
 */
