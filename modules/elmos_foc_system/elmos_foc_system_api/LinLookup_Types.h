/***************************************************************************//**
 * @file			LinLookup_Types.h
 *
 * @creator		sbai
 * @created		25.03.2015
 *
 * @brief  		Definitions of basic data types for the 'LIN LOOKUP Layer'.
 *
 * $Id$
 *
 * $Revision$
 *
 ******************************************************************************/

#ifndef LINLOOKUP_TYPES_H_
#define LINLOOKUP_TYPES_H_

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "Lin_Basictypes.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

typedef Lin_pvoid_t      LinLookupIf_pGenericEnvData_t;           /**< Generic pointer to environment data of the LOOKUP layer module. */
typedef Lin_EnvDataSze_t LinLookupIf_EnvDataSze_t;                /**< LIN Lookup layer data type for the environment data length. */
typedef Lin_pvoid_t      LinLookupIf_pGenericFrameDescLst_t;      /**< Generic pointer to a 'Frame Description List'. **/
typedef Lin_cpvoid_t     LinLookupIf_cpGenericFrameDescLst_t;     /**< Generic constant pointer to a 'Frame Description List'. **/
typedef Lin_pvoid_t      LinLookupIf_pGenericImpCfgData_t;        /**< Generic pointer to configuration parameter of the specific LOOKUP layer implementation. */
typedef Lin_pvoid_t      LinLookupIf_pGenericCbCtxData_t;         /**< Pointer to LOOKUP callback context data. */
typedef Lin_pvoid_t      LinLookupIf_pGenericFrmDescLstEnvData_t; /**< Generic pointer to environment data per 'Frame Description List' of the LOOKUP layer module. */

typedef Lin_uint8_t          LinLookupIf_FrameIdx_t;                /**< LIN Frame Index @see LIN 2.2 Specification - Chapter 4.2.5.5 Assign frame ID range **/
typedef LinBusIf_pFrameID_t  LinLookupIf_pAssignFrameIDRangeLst_t;  /**< Pointer to a list of assigned 'Frame IDs'. @see LIN 2.2 Specification - Chapter 4.2.5.5 Assign frame ID range **/
typedef LinBusIf_cpFrameID_t LinLookupIf_cpAssignFrameIDRangeLst_t; /**< Pointer to a constant list of assigned 'Frame IDs'. @see LIN 2.2 Specification - Chapter 4.2.5.5 Assign frame ID range **/

typedef Lin_uint8_t LinLookupIf_FrameIdLstLength_t;   /**< */

typedef Lin_uint8_t* LinLookupIf_pAssignmentBuffer_t; /**< Pointer to an 'Assignment Buffer'. **/

struct         LinLookupIf_sInterfaceFunctions;                                        /* Forward declaration of LIN LOOKUP layer interface functions. */
typedef struct LinLookupIf_sInterfaceFunctions     LinLookupIf_sInterfaceFunctions_t;  /**< Typedef for LinLookupIf_sInterfaceFunctions. */
typedef        LinLookupIf_sInterfaceFunctions_t*  LinLookupIf_pInterfaceFunctions_t;  /**< Typedef of pointer to LinLookupIf_sInterfaceFunctions. */
typedef const  LinLookupIf_sInterfaceFunctions_t*  LinLookupIf_cpInterfaceFunctions_t; /**< Typedef of constant pointer to LinLookupIf_sInterfaceFunctions. */

struct         LinLookupIf_sThis;                        /* Forward declaration of LIN BUS layer This-Pointer. */
typedef struct LinLookupIf_sThis    LinLookupIf_sThis_t; /**< Typedef for LinLookupIf_sThis. */
typedef        LinLookupIf_sThis_t* LinLookupIf_pThis_t; /**< Typedef of pointer to LinLookupIf_sThis. */

struct         LinLookupIf_sInitParam;                             /* Forward declaration of LIN LOOKUP layer initialization parameter struct. */
typedef struct LinLookupIf_sInitParam    LinLookupIf_sInitParam_t; /**< Typedef for LinLookupIf_sInitParam */
typedef        LinLookupIf_sInitParam_t* LinLookupIf_pInitParam_t; /**< Typedef of pointer to LinLookupIf_sInitParam */

/***************************************************************************//**
 * @brief LIN LOOKUP assignments command types.
 ******************************************************************************/
enum LinLookupIf_eAssignFrameIDRangeCmds
{
  LinLookupIf_AssignFrameIDRangeCmd_UNASSIGN   = 0xFE, /**< Unassign the 'Frame Description' **/
  LinLookupIf_AssignFrameIDRangeCmd_DONOTCARE  = 0xFF  /**< "Do not care" about this 'Frame Description' **/
};

typedef enum LinLookupIf_eAssignFrameIDRangeCmds LinLookupIf_eAssignFrameIDRangeCmds_t;

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

#endif /* LINLOOKUP_TYPES_H_ */
