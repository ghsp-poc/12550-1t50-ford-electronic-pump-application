/***************************************************************************//**
 * @file			LinLookup_Implementation_Sequential.h
 *
 * @creator		sbai
 * @created		13.01.2015
 *
 * @brief  		LIN 'Fixed' LOOKUP Layer implementation.
 *
 * $Id: $
 *
 * $Revision: $
 *
 ******************************************************************************/

#ifndef LINLOOKUP_IMPLEMENTATION_Sequential_H_
#define LINLOOKUP_IMPLEMENTATION_Sequential_H_

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "LinLookup_Interface.h"
#include "LinProto_Interface.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/
#define LINLOOKUPIMP_SEQ_VERSION               0x0110u            /**< LIN LOOKUP SEQ implementation version */

#define LINLOOKUPIMP_SEQ_CONFIG_DATA_VERSION   0x0100             /* Expected config data version */

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/
#if LINLOOKUPIMP_SEQUENTIAL_EXT_IFFUN_STRCT_ACCESS == 1
extern const LinLookupIf_sInterfaceFunctions_t LinLookupImp_InterfaceFunctions_Sequential;
#endif /* LINLOOKUPIMP_EXT_IFFUN_STRCT_ACCESS == 1 */

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

/** @addtogroup LinLookupImpInterfaceFunctions */
/**@{*/

/***************************************************************************//**
 * @brief 'Fixed Lookup' Implementation of LIN LOOKUP layer 'Initialization'
 *        function.
 *
 * @copydetails LinLookupIf_InitializationIfFun_t
 *
 ******************************************************************************/
Lin_Bool_t LinLookupImp_Initialization_Sequential(LinLookupIf_pGenericEnvData_t    genericLookupEnvData, LinLookupIf_EnvDataSze_t lookupEnvDataSze,
                                                  LinLookupIf_pGenericImpCfgData_t genericLookupImpCfgData);

/***************************************************************************//**
 * @brief 'Fixed Lookup' Implementation of LIN LOOKUP layer 'Get Sub-Interface'
 *        function.
 *
 * @copydetails LinLookupIf_GetSubInterfaceIfFun_t
 *
 ******************************************************************************/
Lin_Bool_t LinLookupImp_GetSubInterface_Sequential(LinLookupIf_pGenericEnvData_t genericLookupEnvData, Lin_eInterfaceIds_t interfaceId, Lin_pThis_t ifThisPtr);

/***************************************************************************//**
 * @brief 'Fixed Lookup' Implementation of LIN LOOKUP layer 'Assign Frame ID'
 *        function.
 *
 * @copydetails LinLookupIf_AssignFrameIDIfFun_t
 *
 ******************************************************************************/
Lin_Bool_t LinLookupImp_AssignFrameID_Sequential(LinLookupIf_pGenericEnvData_t genericLookupEnvData, LinProtoIf_MsgID_t msgID, LinBusIf_FrameID_t frameID);

/***************************************************************************//**
 * @brief 'Fixed Lookup' Implementation of LIN LOOKUP layer 'Assign Frame ID
 *        Range' function.
 *
 * @copydetails LinLookupIf_AssignFrameIDRangeIfFun_t
 *
 ******************************************************************************/
Lin_Bool_t LinLookupImp_AssignFrameIDRange_Sequential(LinLookupIf_pGenericEnvData_t         genericLookupEnvData, LinLookupIf_FrameIdx_t         frameIdx,
                                                      LinLookupIf_cpAssignFrameIDRangeLst_t frameIdLst,           LinLookupIf_FrameIdLstLength_t frameIdLstLen);

/***************************************************************************//**
 * @brief 'Fixed Lookup' Implementation of LIN LOOKUP layer 'Get Frame
 *        Description' function.
 *
 * @copydetails LinLookupIf_GetFrameDescriptionIfFun_t
 *
 ******************************************************************************/
LinProtoIf_cpFrameDescription_t LinLookupImp_GetFrameDescription_Sequential(LinLookupIf_pGenericEnvData_t genericLookupEnvData, LinBusIf_FrameID_t frameID);

/***************************************************************************//**
 * @brief 'Fixed Lookup' Implementation of LIN LOOKUP layer 'Get Frame ID
 *        Assignment' function.
 *
 * @copydetails LinLookupIf_GetFrameIdAssignmentIfFun_t
 *
 ******************************************************************************/
LinBusIf_cpFrameID_t LinLookupImp_GetFrameIdAssignment_Sequential(LinLookupIf_pGenericEnvData_t genericLookupEnvData, LinLookupIf_cpGenericFrameDescLst_t genericFrmDscLst);

/***************************************************************************//**
 * @brief 'Fixed Lookup' Implementation of LIN LOOKUP layer 'Add Frame
 *        Description List' function.
 *
 * @copydetails LinLookupIf_AddFrameDescriptionListIfFun_t
 *
 ******************************************************************************/
Lin_Bool_t LinLookupImp_AddFrameDescLst_Sequential(LinLookupIf_pGenericEnvData_t genericLookupEnvData, LinLookupIf_cpGenericFrameDescLst_t     genericFrmDscLst,
                                                   Lin_Bool_t                    ldfRelevance,         LinLookupIf_pGenericFrmDescLstEnvData_t genericFrmDescLstEnvData,
                                                   LinLookupIf_EnvDataSze_t      frmDescLstEnvDataSze, LinLookupIf_pGenericCbCtxData_t         perFrmDescLstCbCtxData);

/***************************************************************************//**
 * @brief 'Fixed Lookup'Implementation of LIN LOOKUP layer 'Remove Frame
 *        Description List function.
 *
 * @copydetails LinLookupIf_RemoveFrameDescriptionListIfFun_t
 *
 ******************************************************************************/
Lin_Bool_t LinLookupImp_RmvFrameDescLst_Sequential(LinLookupIf_pGenericEnvData_t genericLookupEnvData, LinLookupIf_cpGenericFrameDescLst_t genericFrmDscLst);

/***************************************************************************//**
 * @brief 'Fixed Lookup'Implementation of LIN LOOKUP layer 'Get Frame
 *        Description Count' function.
 *
 * @copydetails LinLookupIf_GetFrameDescriptionCountIfFun_t
 *
 ******************************************************************************/
Lin_Bool_t LinLookupImp_GetFrameDescCnt_Sequential(LinLookupIf_pGenericEnvData_t genericLookupEnvData);

/***************************************************************************//**
 * @brief 'Fixed Lookup' Implementation of LIN LOOKUP layer 'Get per Frame
 *        Description List Context Data' function.
 *
 * @copydetails LinLookupIf_GetPerFrameDescLstCbCtxDataIfFun_t
 *
 ******************************************************************************/
LinLookupIf_pGenericCbCtxData_t LinLookupImp_GetPerFrameDescLstCbCtxData_Sequential(LinLookupIf_pGenericEnvData_t genericLookupEnvData, LinLookupIf_cpGenericFrameDescLst_t frameDescLstAdr);

/**@} LinLookupImpInterfaceFunctions */

#endif /* LINLOOKUP_IMPLEMENTATION_Sequential_H_ */
