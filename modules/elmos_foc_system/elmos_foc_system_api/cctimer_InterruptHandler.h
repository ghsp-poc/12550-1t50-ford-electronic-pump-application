/***************************************************************************//**
 * @file			cctimer_InterruptHandler.h
 *
 * @creator		rpy
 * @created		04.09.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 ******************************************************************************/

#ifndef CCTIMER_INTERRUPTHANDLER_H_          
#define CCTIMER_INTERRUPTHANDLER_H_

#pragma once

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "el_types.h"
#include "vic_Interface.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/***************************************************************************//**
 * CCTIMER IRQ vector numbers
 ******************************************************************************/
typedef enum {  
  cctimer_IRQ_RESTART_P              = 0u,
  cctimer_IRQ_OVERFLOW_A             = 1u,
  cctimer_IRQ_OVERFLOW_B             = 2u,
  cctimer_IRQ_RESTART_A              = 3u,
  cctimer_IRQ_RESTART_B              = 4u,
  cctimer_IRQ_CAPTURE_A              = 5u,
  cctimer_IRQ_CAPTURE_B              = 6u,
  cctimer_IRQ_COMPARE_A              = 7u,
  cctimer_IRQ_COMPARE_B              = 8u,

  cctimer_INTERRUPT_VECTOR_CNT       = 9u  /**< Number of available interrupt vectors */
} cctimer_eInterruptVectorNum_t;


/***************************************************************************//**
 * Pointer to CCTIMER context data
 ******************************************************************************/
typedef void * cctimer_pInterruptContextData_t;

/***************************************************************************//**
 * Callback function pointer type
 ******************************************************************************/
typedef void (*cctimer_InterruptCallback_t) (cctimer_eInterruptVectorNum_t irqsrc, cctimer_pInterruptContextData_t contextdata);

/***************************************************************************//**
 * Callback function pointer type
 ******************************************************************************/
typedef enum
{
  cctimer_MOD_0         = 0u,
  cctimer_MOD_1         = 1u,
  cctimer_MOD_2         = 2u,
  cctimer_MOD_3         = 3u,

  cctimer_INSTANCE_CNT  = 4u  /**< Number of cctimes implemented */

} cctimer_InstanceNum_t;


/***************************************************************************//**
 * CCTIMER environment data
 ******************************************************************************/
typedef struct cctimer_sInterruptEnvironmentData
{
    /** Interrupt vector table of this module */
    cctimer_InterruptCallback_t InterrupVectorTable[cctimer_INTERRUPT_VECTOR_CNT];
    
    /** CCTIMER module context data */
    cctimer_pInterruptContextData_t ContextData;
        
} cctimer_sInterruptEnvironmentData_t;

/***************************************************************************//**
 * Pointer to CCTIMER environment data
 ******************************************************************************/
typedef cctimer_sInterruptEnvironmentData_t * cctimer_pInterruptEnvironmentData_t;

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************************* INTERRUPTS *********************************/
/* ****************************************************************************/

/***************************************************************************//**
 * @brief     Enables an IRQ.
 *
 * @param[in] modulBaseAddress  Pointer to CCTIMER module base address
 * @param[in] irqsrc            IRQ to be enabled
 *
 * @pre       A call back function to the related interrupt should have been
 *            registered with cctimer_RegisterInterruptCallback().
 *
 * @post      The related call back function will be called if the desired
 *            interrupt occurs.
 *
 * @detaildesc
 * The CCTIMER IRQ_VENABLE register will be set the related IRQ number and therefore
 * the interrupt will be activated.
 *
 ******************************************************************************/
void cctimer_InterruptEnable(cctimer_InstanceNum_t instanceNum, cctimer_eInterruptVectorNum_t irqsrc);

/***************************************************************************//**
 * @brief Disables an IRQ.
 *
 * @param[in] modulBaseAddress  Pointer to CCTIMER module base address
 * @param[in] irqsrc            IRQ to be disable
 *
 * @post      The interrupt will be disabled and the related callback function
 *            will no longer be called from the interrupt handler.
 *
 * @detaildesc
 * The CCTIMER IRQ_VDISABLE register will be set the related IRQ number and therefore
 * the interrupt will be deactivated.
 *
 ******************************************************************************/
void cctimer_InterruptDisable(cctimer_InstanceNum_t instanceNum, cctimer_eInterruptVectorNum_t irqsrc);

/***************************************************************************//**
 * @brief Registers/Adds callback function to the module interrupt vector table.
 *
 * @param modulBaseAddress  Pointer to CCTIMER module base address
 * @param irqsrc            IRQ number
 * @param cbfnc             Pointer to desired callback function
 *
 * @pre     (optional) Which are the conditions to call this function? i.e. none
 *
 * @post    If the interrupt will be activated the registered callback function
 *          will be called if the IRQ occurs.
 *
 * @detaildesc
 * Registers the callback function at interrupt vector handling. It sets the
 * entry in the interrupt vector table to passed function pointer.
 *
 ******************************************************************************/
void cctimer_InterruptRegisterCallback(cctimer_InstanceNum_t instanceNum, cctimer_eInterruptVectorNum_t irqvecnum, cctimer_InterruptCallback_t cbfnc);

/***************************************************************************//**
 * @brief Deregisters/deletes callback function from module interrupt vector table.
 *
 * @param modulBaseAddress  Pointer to CCTIMER module base address
 * @param irqvecnum         IRQ number
 * @param cbfnc             Pointer to desired callback function
 *
 * @pre   The related IRQ should be disabled.
 *
 * @post  The entry in the module interrupt vector table will point to NULL and
 *        the related IRQ will be disabled.
 *
 * @detaildesc
 * Deregisters the callback function from interrupt vector handling. It sets the
 * entry in the interrupt vector table to NULL and disables the related interrupt.
 *
 ******************************************************************************/
void cctimer_InterruptDeregisterCallback(cctimer_InstanceNum_t instanceNum, cctimer_eInterruptVectorNum_t irqvecnum);

/***************************************************************************//**
 * Handles the CCTIMER 0 related interrupt requests.
 *
 ******************************************************************************/
 __interrupt void cctimer_InterruptHandler0(void);
 
 /***************************************************************************//**
 * Handles the CCTIMER 1 related interrupt requests.
 *
 ******************************************************************************/
 __interrupt void cctimer_InterruptHandler1(void);
 
 /***************************************************************************//**
 * Handles the CCTIMER 2 related interrupt requests.
 *
 ******************************************************************************/
 __interrupt void cctimer_InterruptHandler2(void);
 
 /***************************************************************************//**
 * Handles the CCTIMER 3 related interrupt requests.
 *
 ******************************************************************************/
 __interrupt void cctimer_InterruptHandler3(void);

/***************************************************************************//**
 * @brief Initialize CCTIMER module
 *
 * @param environmentdata  Pointer to Environment data for CCTIMER module in
 *                         user RAM
 *
 * @pre        VIC (vic_VectorInterruptControl) and CCTIMER (cctimer_SystemStateModule)
 *             have to presented and initialized.
 *
 * @post       CCTIMER module is configured for use.
 *
 * @detaildesc
 * Initializes the CCTIMER software and hardware module, including the module
 * interrupt vector table. Configures if IRQ nesting is active and if IO2 and
 * IO3 are used as CCTIMERs or not.
 *
 ******************************************************************************/
void cctimer_InterruptInitialisation(cctimer_InstanceNum_t instanceNum, vic_cpInterfaceFunctions_t vicIf, cctimer_pInterruptEnvironmentData_t environmentdata, cctimer_pInterruptContextData_t contextdata);


#endif /* LIN_CTRL_INTERRUPTHANDLER_H_ */

