/***************************************************************************//**
 * @file      LinUDS_Types.h
 *
 * @creator   sbai
 * @created   25.03.2015
 *
 * @brief     Definitions of basic data types for the 'LIN UDS Layer'.
 *
 * $Id: LinUDS_Types.h 2417 2017-05-11 09:11:45Z sbai $
 *
 * $Revision: 2417 $
 *
 ******************************************************************************/

#ifndef LINUDS_TYPES_H_
#define LINUDS_TYPES_H_

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "LinTrans_Types.h"

/* ****************************************************************************/
/* *********** GENERAL DEFINES, ENUMS, STRUCTS, DATA TYPES, ETC. **************/
/* ****************************************************************************/
typedef Lin_pvoid_t      LinUDSIf_pGenericEnvData_t;    /**< TODO: Add description */
typedef Lin_EnvDataSze_t LinUDSIf_EnvDataSze_t;         /**< LIN UDS layer data type for the environment data length. */
typedef Lin_pvoid_t      LinUDSIf_pGenericImpCfgData_t; /**< Generic pointer to configuration parameter of the specific UDS Layer implementation */
typedef Lin_pvoid_t      LinUDSIf_pGenericCbCtxData_t;  /**< TODO: Add description */

typedef Lin_Error_t  LinUDSIf_Error_t;              /**< mapping for bus error codes to generic error type */

typedef Lin_uint8_t       LinUDSIf_Data_t;   /**< TODO: Add description */
typedef LinUDSIf_Data_t*  LinUDSIf_pData_t;  /**< TODO: Add description */
typedef LinUDSIf_Data_t** LinUDSIf_ppData_t; /**< TODO: Add description */

typedef Lin_BufLength_t     LinUDSIf_Length_t;   /**< TODO: Add description */
typedef LinUDSIf_Length_t*  LinUDSIf_pLength_t;  /**< TODO: Add description */
typedef LinUDSIf_Length_t** LinUDSIf_ppLength_t; /**< TODO: Add description */

struct         LinUDSIf_sCallbackFunctions;
typedef struct LinUDSIf_sCallbackFunctions     LinUDSIf_sCallbackFunctions_t;
typedef        LinUDSIf_sCallbackFunctions_t*  LinUDSIf_pCallbackFunctions_t;
typedef const  LinUDSIf_sCallbackFunctions_t*  LinUDSIf_cpCallbackFunctions_t;

struct         LinUDSIf_sInterfaceFunctions;                                      /**< TODO: Add description */
typedef struct LinUDSIf_sInterfaceFunctions     LinUDSIf_sInterfaceFunctions_t;  /**< TODO: Add description */
typedef        LinUDSIf_sInterfaceFunctions_t*  LinUDSIf_pInterfaceFunctions_t;  /**< TODO: Add description */
typedef const  LinUDSIf_sInterfaceFunctions_t*  LinUDSIf_cpInterfaceFunctions_t; /**< TODO: Add description */

struct         LinUDSIf_sThis;                      /**< TODO: Add description */
typedef struct LinUDSIf_sThis    LinUDSIf_sThis_t; /**< TODO: Add description */
typedef        LinUDSIf_sThis_t* LinUDSIf_pThis_t; /**< TODO: Add description */

struct         LinUDSIf_sInitParam;                           /**< TODO: Add description */
typedef struct LinUDSIf_sInitParam    LinUDSIf_sInitParam_t; /**< TODO: Add description */
typedef        LinUDSIf_sInitParam_t* LinUDSIf_pInitParam_t; /**< TODO: Add description */

typedef LinTransIf_NAD_t LinUDSIf_NAD_t;
typedef LinTransIf_SID_t LinUDSIf_SID_t;

typedef Lin_uint32_t LinUDSIf_Timeout_t;

/***************************************************************************//**
 *
 ******************************************************************************/
enum LinUDSIf_eNADType
{
  LinUDSIf_NADType_NORMAL  = 0,
  LinUDSIf_NADType_INITIAL = 1
};

typedef enum LinUDSIf_eNADType LinUDSIf_eNADType_t;

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param[in]  enclosing_method_argument TODO: Parameter description
 *
 * @return         TODO: return description
 *
 ******************************************************************************/
typedef Lin_Bool_t (*LinUDSIf_AppendDataToMsgBuffer_t) (LinUDSIf_pGenericEnvData_t genericUdsEnvData, LinUDSIf_pData_t data, LinUDSIf_Length_t dataLen);

/***************************************************************************//**
 * LIN UDS Layer error enumerator
 *
 * @misra{M3CM Dir-10.1. - PRQA Msg 4521,
 * The Elmos LIN Driver defines areas of error codes for every module and sub-areas between
 * general defined interface error codes and implementation specific error codes. To link
 * between this areas some arithmetic offset calculation has to be done between different
 * enum types.,
 * Conflicts in  signedness.,
 * Always make sure unsigned values ate defined and used.}
 ******************************************************************************/
// PRQA S 4521 ++
typedef enum LinUDSIf_eErrorCodes
{
  LinUDSIf_ERR_NO_ERROR                                 = Lin_NO_ERROR,              /**< No error at all. */
  LinUDSIf_ERR_INIT                                     = Lin_ERROR_AREA_DIAG_UDS + 0u,  /**< Initialization error */
  LinUDSIf_ERR_INVALID_NAD                              = Lin_ERROR_AREA_DIAG_UDS + 1u,  /**< invalid NAD specified */
  LinUDSIf_ERR_IN_CORRECT_MESSAGE_LENGHT_INVALID_FORMAT = Lin_ERROR_AREA_DIAG_UDS + 2u,  /**< The data length mismatch for the service. */
  LinUDSIf_ERR_RBI_DEF                                  = Lin_ERROR_AREA_DIAG_UDS + 3u,  /**< Error at "Read by Identifier" definition. */
  LinUDSIf_ERR_RBI_NOT_FOUND                            = Lin_ERROR_AREA_DIAG_UDS + 4u,  /**< No entry for this identifier is found. */
  LinUDSIf_ERR_RBI_DATA_LEN                             = Lin_ERROR_AREA_DIAG_UDS + 5u,
  LinUDSIf_ERR_RDI_DEF                                  = Lin_ERROR_AREA_DIAG_UDS + 6u,  /**< Error at "Record Data Identifier" definition. */
  LinUDSIf_ERR_RDI_NOT_FOUND                            = Lin_ERROR_AREA_DIAG_UDS + 7u,  /**< No entry for this "Record Data Identifier" is found. */
  LinUDSIf_ERR_RDI_DATA_LEN                             = Lin_ERROR_AREA_DIAG_UDS + 8u,  /**< Record Data Identifier length mismatch. */
  LinUDSIf_ERR_SEED_LEN_MISMATCH                        = Lin_ERROR_AREA_DIAG_UDS + 9u,
  LinUDSIf_ERR_TRANS_LAYER                              = Lin_ERROR_AREA_DIAG_UDS + 10u, /**< Error using Transport Layer. */
  LinUDSIf_ERR_ROUTINEID_DEF                            = Lin_ERROR_AREA_DIAG_UDS + 11u, /**< Error at "Routine Identifier" definition. */
  LinUDSIf_ERR_ROUTINEID_DATA_LEN                       = Lin_ERROR_AREA_DIAG_UDS + 12u, /**< Routine Identifier length mismatch. */
  LinUDSIf_ERR_IMPL_ERROR_AREA                          = Lin_ERROR_AREA_DIAG_UDS + (LIN_ERROR_AREA_SIZE/2u) /**< Any additional implementation specific error codes start here. */
} LinUDSIf_eErrorCodes_t;
// PRQA S 4521 --

#define LINUDSIF_RSID_OFFSET         0x40u

#define LINUDSIF_SUPPLIER_ID_WILDCARD  0x7FFFu
#define LINUDSIF_FUNCTION_ID_WILDCARD  0xFFFFu

enum LinUDSIf_ServiceID
{
  /* LIN 2.2 specification services. */
  LinUDSIf_SID_AssignNAD                      = 0xB0u,
  LinUDSIf_SID_AssignFrameID                  = 0xB1u,
  LinUDSIf_SID_ReadByIdentifier               = 0xB2u,
  LinUDSIf_SID_ConditionalChangeNAD           = 0xB3u,
  LinUDSIf_SID_SNPD                           = 0xB5u,
  LinUDSIf_SID_SaveConfiguration              = 0xB6u,
  LinUDSIf_SID_AssignFrameIDRange             = 0xB7u,
  /* Diagnostic and Communication Management functional unit  */
  LinUDSIf_SID_DiagnosticSessionControl       = 0x10u,
  LinUDSIf_SID_ECUReset                       = 0x11u,
  LinUDSIf_SID_SecurityAccess                 = 0x27u,
  LinUDSIf_SID_CommunicationControl           = 0x28u,
  LinUDSIf_SID_TesterPresent                  = 0x3Eu,
  LinUDSIf_SID_ControlDTCSetting              = 0x85u,
  LinUDSIf_SID_ResponseOnEvent                = 0x86u,
  /* Data Transmission functional unit */
  LinUDSIf_SID_ReadDataByIdentifier           = 0x22u,
  LinUDSIf_SID_WriteDataByIdentifier          = 0x2Eu,
  /* Stored data transmission functional unit */
  LinUDSIf_SID_ClearDiagnosticInformation     = 0x14u,
  LinUDSIf_SID_ReadDTCInformation             = 0x19u,
  /* Input/Output control functional unit */
  LinUDSIf_SID_InputOutputControlByIdentifier = 0x2Fu,
  /* Remote activation of routine functional unit */
  LinUDSIf_SID_RoutineControl                 = 0x31u,
  /* Upload/Download functional unit */
  LinUDSIf_SID_RequestDownload                = 0x34u,
  LinUDSIf_SID_RequestUpload                  = 0x35u,
  LinUDSIf_SID_TransferData                   = 0x36u,
  LinUDSIf_SID_RequestTransferExit            = 0x37u,
};

typedef enum LinUDSIf_ServiceID LinUDSIf_ServiceID_t;

/***************************************************************************//**
 * Negative Response Codes
 ******************************************************************************/
#define LINUDSIF_NEG_RESP_RSID                                         0x7Fu
#define LINUDSIF_NEG_RESP_LEN                                          2u

typedef Lin_uint16_t     LinUDSIf_NRC_t;
typedef LinUDSIf_NRC_t* LinUDSIf_pNRC_t;

enum LinUDSIf_eNegativeResponseCode
{
  LinUDIf_NRC_General_Reject                                           = 0x10u,
  LinUDIf_NRC_Service_Not_Supported                                    = 0x11u,
  LinUDIf_NRC_Sub_Function_Not_Supported                               = 0x12u,
  LinUDIf_NRC_In_Correct_Message_Lenght_Invalid_Format                 = 0x13u,
  LinUDIf_NRC_Response_Too_Long                                        = 0x14u,
  LinUDIf_NRC_Busy_Repeat_Request                                      = 0x21u,
  LinUDIf_NRC_Conditions_Not_Correct                                   = 0x22u,
  LinUDIf_NRC_Request_Sequence_Error                                   = 0x24u,
  LinUDIf_NRC_No_Response_From_Subnet_Component                        = 0x25u,
  LinUDIf_NRC_Failure_Prevents_Execution_Of_Requested_Action           = 0x26u,
  LinUDIf_NRC_Request_Out_Of_Range                                     = 0x31u,
  LinUDIf_NRC_Security_Access_Denied                                   = 0x33u,
  LinUDIf_NRC_Invalid_Key                                              = 0x35u,
  LinUDIf_NRC_Exceed_Number_Of_Attempts                                = 0x36u,
  LinUDIf_NRC_Required_Time_Delay_Not_Expired                          = 0x37u,
  LinUDIf_NRC_Upload_Download_Not_Accepted                             = 0x70u,
  LinUDIf_NRC_Transfer_Data_Suspended                                  = 0x71u,
  LinUDIf_NRC_General_Programming_Failure                              = 0x72u,
  LinUDIf_NRC_Wrong_Block_Sequence_Counter                             = 0x73u,
  LinUDIf_NRC_Request_Correctly_Received_Response_Pending              = 0x78u,
  LinUDIf_NRC_Sub_Function_Not_Supported_In_Active_Diagnostic_Session  = 0x7Eu,
  LinUDIf_NRC_Service_Not_Supported_In_Active_Diagnostic_Session       = 0x7Fu,
  LinUDIf_NRC_Rpm_Too_High                                             = 0x81u,
  LinUDIf_NRC_Rpm_Too_Low                                              = 0x82u,
  LinUDIf_NRC_Engine_Is_Running                                        = 0x83u,
  LinUDIf_NRC_Engine_Is_Not_Running                                    = 0x84u,
  LinUDIf_NRC_Engine_Run_Time_Too_Low                                  = 0x85u,
  LinUDIf_NRC_Temperature_Too_High                                     = 0x86u,
  LinUDIf_NRC_Temperature_Too_Low                                      = 0x87u,
  LinUDIf_NRC_Vehicle_Speed_Too_High                                   = 0x88u,
  LinUDIf_NRC_Vehicle_Speed_Too_Low                                    = 0x89u,
  LinUDIf_NRC_Throttle_Pedal_Too_High                                  = 0x8Au,
  LinUDIf_NRC_Throttle_Pedal_Too_Low                                   = 0x8Bu,
  LinUDIf_NRC_Transmission_Range_Not_In_Neutral                        = 0x8Cu,
  LinUDIf_NRC_Transmission_Rang_Not_In_Gear                            = 0x8Du,
  LinUDIf_NRC_Brake_Switches_Not_Closed                                = 0x8Fu,
  LinUDIf_NRC_Shifter_Lever_Not_In_Park                                = 0x90u,
  LinUDIf_NRC_Torque_Converter_Clutch_Locked                           = 0x91u,
  LinUDIf_NRC_Voltage_Too_High                                         = 0x92u,
  LinUDIf_NRC_Voltage_Too_Low                                          = 0x93u,
  LinUDIf_NRC_Basic_Setting_Not_Started                                = 0xF0u,
  LinUDIf_NRC_Not_Configured                                           = 0xF1u,
  LinUDIf_NRC_Ecu_Specific_Conditions_Not_Correct                      = 0xFAu
};

typedef enum LinUDSIf_eNegativeResponseCode LinUDSIf_eNegativeResponseCode_t;

/* ****************************************************************************/
/* ****************************** LIN SERVICES ********************************/
/* ****************************************************************************/

#define LINUDSIF_ASSIGNNAD_REQLEN             5u
#define LINUDSIF_ASSIGNFRAMEID_REQLEN         5u
#define LINUDSIF_READBYIDENTIFIER_REQLEN      5u
#define LINUDSIF_CONDITIONALCHANGENAD_REQLEN  5u
#define LINUDSIF_ASSIGNFRAMEIDRANGE_REQLEN    5u
#define LINUDSIF_SNPD_REQLEN                  5u

/* LIN 2.2 Read by identifier - identifier */

#define LINUDSIF_RB_IDENTIFIER_PROD_IDENT      0x00u /*<< Read By Identifier for 'Product Identification'. */
#define LINUDSIF_RB_IDENTIFIER_PROD_IDENT_LEN  5u

#define LINUDSIF_RB_IDENTIFIER_SERIAL_NUM      0x01u /*<< Read By Identifier for 'Serial number'. */
#define LINUDSIF_RB_IDENTIFIER_SERIAL_NUM_LEN  4u

typedef Lin_uint16_t LinUDSIf_SupplierID_t;       /**< TODO: Add description */
typedef Lin_uint16_t LinUDSIf_FunctionID_t;       /**< TODO: Add description */
typedef Lin_uint8_t  LinUDSIf_VariantID_t;        /**< TODO: Add description */
typedef Lin_uint32_t LinUDSIf_SerialNumber_t;     /**< TODO: Add description */

typedef Lin_uint8_t LinUDSIf_RbIdentifier_t; /**< TODO: Add description */
typedef Lin_pvoid_t LinUDSIf_RbiLenght_t;    /**< TODO: Add description */

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param[in]  enclosing_method_argument TODO: Parameter description
 *
 * @return         TODO: return description
 *
 ******************************************************************************/
typedef LinUDSIf_Length_t (*LinUDSIf_RbILenCbFun_t) (LinUDSIf_pGenericEnvData_t genericUdsEnvData, LinUDSIf_cpInterfaceFunctions_t udsIfFuns,
                                                     LinUDSIf_RbIdentifier_t    rbIdentifier);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param[in]  enclosing_method_argument TODO: Parameter description
 *
 * @return         TODO: return description
 *
 ******************************************************************************/
typedef Lin_Bool_t (*LinUDSIf_RbIdentifierCbFun_t) (LinUDSIf_pGenericEnvData_t   genericUdsEnvData, LinUDSIf_cpInterfaceFunctions_t  udsIfFuns,
                                                    LinUDSIf_RbIdentifier_t      rbIdentifier,      LinUDSIf_AppendDataToMsgBuffer_t appendDataToMsgBufFun,
                                                    LinUDSIf_pGenericCbCtxData_t genericUdsCbCtxData);

/***************************************************************************//**
 *
 ******************************************************************************/
typedef enum LinUDSIf_eRbiLenType
{
  LinUDSIf_RbiLenType_Value    = 0,  /**< TODO: Add description */
  LinUDSIf_RbiLenType_Callback = 1   /**< TODO: Add description */
}LinUDSIf_eRbiLenType_t;

/***************************************************************************//**
 *
 ******************************************************************************/
struct LinUDSIf_sRbiLookupEntry
{
  LinUDSIf_RbIdentifier_t      FirstRbIdentifier;
  LinUDSIf_RbIdentifier_t      LastRbIdentifier;
  LinUDSIf_RbIdentifierCbFun_t Callback;
  LinUDSIf_eRbiLenType_t       LengthType;
  LinUDSIf_RbiLenght_t         Length;
  LinUDSIf_pGenericCbCtxData_t CbCtxData;
};

typedef struct LinUDSIf_sRbiLookupEntry LinUDSIf_sRbiLookupEntry_t;
typedef LinUDSIf_sRbiLookupEntry_t*     LinUDSIf_pRbiLookupEntry_t;

/***************************************************************************//**
 *
 ******************************************************************************/
struct LinUDSIf_sProductIdentification
{
  LinUDSIf_SupplierID_t SupplierID;
  LinUDSIf_FunctionID_t FunctionID;
  LinUDSIf_VariantID_t  VariantID;
};

typedef struct LinUDSIf_sProductIdentification    LinUDSIf_sProductIdentification_t;
typedef        LinUDSIf_sProductIdentification_t* LinUDSIf_pProductIdentification_t;

/***************************************************************************//**
 *
 ******************************************************************************/
struct LinUDSIf_sConditionalChangeNADData
{
  LinUDSIf_RbIdentifier_t Id;     /**< TODO: Add description */
  LinUDSIf_Data_t         Byte;   /**< TODO: Add description */
  LinUDSIf_Data_t         Mask;   /**< TODO: Add description */
  LinUDSIf_Data_t         Invert; /**< TODO: Add description */
  LinUDSIf_NAD_t          NewNAD; /**< TODO: Add description */
};

typedef struct LinUDSIf_sConditionalChangeNADData    LinUDSIf_sConditionalChangeNADData_t;  /**< TODO: Add description */
typedef        LinUDSIf_sConditionalChangeNADData_t* LinUDSIf_pConditionalChangeNADData_t;  /**< TODO: Add description */

/* ****************************************************************************/
/* ****************************** UDS SERVICES ********************************/
/* ****************************************************************************/
#define LINUDSIF_DIAGNOSTICSESSIONCONTROL_REQLEN            1u
#define LINUDSIF_ECURESET_REQLEN                            1u
#define LINUDSIF_SECURITYACCESS_REQLEN_MIN                  1u
#define LINUDSIF_COMMUNICATIONCONTROL_REQLEN_MIN            2u
#define LINUDSIF_COMMUNICATIONCONTROL_REQLEN_MAX            4u
#define LINUDSIF_TESTERPRESENT_REQLEN                       1u
#define LINUDSIF_CLEARDIAGNOSTICINFORMATION_REQLEN_MIN      1u
#define LINUDSIF_RESPONSEONEVENT_REQLEN_MIN                 1u
#define LINUDSIF_READDATABYIDENTIFIER_REQLEN_MAX            LINUDS_MAX_PHYADR_RDBI_LST_LEN * sizeof(LinUDSIf_DID_t)
#define LINUDSIF_WRITEDATABYIDENTIFIER_REQLEN_MIN           3u
#define LINUDSIF_CLEARDIAGNOSTICINFORMATION_REQLEN          3u
#define LINUDSIF_READDTCINFORMATION_REQLEN_MIN              2u
#define LINUDSIF_INPUTOUTPUTCONTROLBYIDENTIFIER_REQLEN_MIN  3u
#define LINUDSIF_ROUTINECONTROL_REQLEN_MIN                  3u
#define LINUDSIF_REQUESTDOWNLOAD_REQLEN_MIN                 4u
#define LINUDSIF_REQUESTUPLOAD_REQLEN_MIN                   4u
#define LINUDSIF_TRANSFERDATA_REQLEN_MIN                    2u
#define LINUDSIF_REQUESTTRANSFEREXIT_REQLEN_MIN             0u

#define LINUDSIF_DIAGNOSTICSESSIONCONTROL_RESPLEN 5u
#define LINUDSIF_ECURESET_RESPLEN_MIN             1u
#define LINUDSIF_SECURITYACCESS_RESPLEN_MIN       1u
#define LINUDSIF_SECURITYACCESS_SEED_LEN_MIN      2u

/* ****************************************************************************/
/* ********* DIAGNOSTIC AND COMMUNICATION MANAGEMENT FUNCTIONAL UNIT **********/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************* DiagnosticSessionControl Service ***********************/
/* ****************************************************************************/
typedef Lin_uint16_t  LinUDSIf_P2SrvMax_t;
typedef Lin_uint16_t* LinUDSIf_pP2SrvMax_t;
typedef Lin_uint16_t  LinUDSIf_P2StarSrvMax_t;
typedef Lin_uint16_t* LinUDSIf_pP2StarSrvMax_t;

typedef Lin_uint8_t LinUDSIf_DiagnosticSessionType_t;

/***************************************************************************//**
 * DiagnosticSessionControl (0x10) Service - Sub-Function 'diagnosticSessionType' - LEV_DS_
 ******************************************************************************/
enum LinUDSIf_eSubFunc_diagnosticSessionType
{
  LinUDSIf_SUBFUNC_DS_ISOSAEReserved_0x00               = 0x00u, /**< ISOSAEReserved - LEV_DS_ISOSAERESRVD */
  LinUDSIf_SUBFUNC_DS_defaultSession                    = 0x01u, /**< defaultSession - LEV_DS_DS */
  LinUDSIf_SUBFUNC_DS_ProgrammingSession                = 0x02u, /**< ProgrammingSession - LEV_DS_PRGS */
  LinUDSIf_SUBFUNC_DS_extendedDiagnosticSession         = 0x03u, /**< extendedDiagnosticSession - LEV_DS_EXTDS */
  LinUDSIf_SUBFUNC_DS_safetySystemDiagnosticSession     = 0x04u, /**< safetySystemDiagnosticSession - LEV_DS_SSDS */
  LinUDSIf_SUBFUNC_DS_ISOSAEReserved_Start_0x05         = 0x05u, /**< ISOSAEReserved Area Start - LEV_DS_ISOSAERESRVD */
  LinUDSIf_SUBFUNC_DS_ISOSAEReserved_End_0x3F           = 0x3Fu, /**< ISOSAEReserved Area End - LEV_DS_ISOSAERESRVD */
  LinUDSIf_SUBFUNC_DS_vehicleManufacturerSpecific_Start = 0x40u, /**< vehicleManufacturerSpecific Area Start - LEV_DS_VMS  */
  LinUDSIf_SUBFUNC_DS_vehicleManufacturerSpecific_End   = 0x5Fu, /**< vehicleManufacturerSpecific Area End - LEV_DS_VMS */
  LinUDSIf_SUBFUNC_DS_systemSupplierSpecific_Start      = 0x60u, /**< systemSupplierSpecific Area Start - LEV_DS_SSS */
  LinUDSIf_SUBFUNC_DS_systemSupplierSpecific_End        = 0x7Eu, /**< systemSupplierSpecific Area End - LEV_DS_SSS */
  LinUDSIf_SUBFUNC_DS_ISOSAEReserved_0x7F               = 0x7Fu  /**< ISOSAEReserved - LEV_DS_ISOSAERESRVD */
};

typedef enum LinUDSIf_eSubFunc_diagnosticSessionType LinUDSIf_eSubFunc_diagnosticSessionType_t;

/* ****************************************************************************/
/* **************************** ECUReset Service ******************************/
/* ****************************************************************************/
typedef Lin_uint8_t               LinUDSIf_ResetType_t;
typedef Lin_uint8_t               LinUDSIf_PowerDownTime_t;
typedef LinUDSIf_PowerDownTime_t* LinUDSIf_pPowerDownTime_t;

/***************************************************************************//**
 * ECUReset (0x11) Service - Sub-Function 'resetType' - LEV_RT_
 ******************************************************************************/
enum LinUDSIf_eSubFunc_resetType
{
  LinUDSIf_SUBFUNC_RT_ISOSAEReserved_0x00               = 0x00u, /**< ISOSAEReserved - LEV_RT_ISOSAERESRVD */
  LinUDSIf_SUBFUNC_RT_hardReset                         = 0x01u, /**< hardReset - LEV_RT_HR */
  LinUDSIf_SUBFUNC_RT_keyOffOnReset                     = 0x02u, /**< keyOffOnReset - LEV_RT_KOFFONR */
  LinUDSIf_SUBFUNC_RT_softReset                         = 0x03u, /**< softReset - LEV_RT_SR */
  LinUDSIf_SUBFUNC_RT_enableRapidPowerShutDown          = 0x04u, /**< enableRapidPowerShutDown - LEV_RT_ERPSD */
  LinUDSIf_SUBFUNC_RT_disableRapidPowerShutDown         = 0x05u, /**< disableRapidPowerShutDown - LEV_RT_DRPSD */
  LinUDSIf_SUBFUNC_RT_ISOSAEReserved_Start_0x06         = 0x06u, /**< ISOSAEReserved Area Start - LEV_RT_ISOSAERESRVD */
  LinUDSIf_SUBFUNC_RT_ISOSAEReserved_End_0x3F           = 0x3Fu, /**< ISOSAEReserved Area End - LEV_RT_ISOSAERESRVD */
  LinUDSIf_SUBFUNC_RT_vehicleManufacturerSpecific_Start = 0x40u, /**< vehicleManufacturerSpecific Area Start - LEV_RT_VMS  */
  LinUDSIf_SUBFUNC_RT_vehicleManufacturerSpecific_End   = 0x5Fu, /**< vehicleManufacturerSpecific Area End - LEV_RT_VMS */
  LinUDSIf_SUBFUNC_RT_systemSupplierSpecific_Start      = 0x60u, /**< systemSupplierSpecific Area Start - LEV_RT_SSS */
  LinUDSIf_SUBFUNC_RT_systemSupplierSpecific_End        = 0x7Eu, /**< systemSupplierSpecific Area End - LEV_RT_SSS */
  LinUDSIf_SUBFUNC_RT_ISOSAEReserved_0x7F               = 0x7Fu  /**< ISOSAEReserved - LEV_RT_ISOSAERESRVD */
};

typedef enum LinUDSIf_eSubFunc_resetType LinUDSIf_eSubFunc_resetType_t;

/* ****************************************************************************/
/* ************************ SecurityAccess Service ****************************/
/* ****************************************************************************/
typedef Lin_uint8_t                     LinUDSIf_SecurityAccessType_t;
typedef LinUDSIf_SecurityAccessType_t*  LinUDSIf_pSecurityAccessType_t;
typedef Lin_uint8_t*                    LinUDSIf_pSecurityAccessDataRecord_t;
typedef Lin_uint8_t*                    LinUDSIf_pSeed_t;
typedef Lin_uint8_t*                    LinUDSIf_pKey_t;

/***************************************************************************//**
 * SecurityAccess (0x27) - Sub-Function 'securityAccessType' - LEV_SAT_
 ******************************************************************************/
enum LinUDSIf_eSubFunc_securityAccessType
{
  LinUDSIf_SUBFUNC_SAT_ISOSAEReserved_0x00           = 0x00u, /**< ISOSAEReserved - LEV_SAT_ISOSAERESRVD */
  LinUDSIf_SUBFUNC_SAT_requestSeed                   = 0x01u, /**< requestSeed - LEV_SAT_RSD */
  LinUDSIf_SUBFUNC_SAT_sendKey                       = 0x02u, /**< sendKey - LEV_SAT_SK */
  LinUDSIf_SUBFUNC_SAT_requestSeed_Start             = 0x03u, /**< requestSeed - LEV_SAT_RSD - Uneven numbers are for 'requestSeed' */
  LinUDSIf_SUBFUNC_SAT_requestSeed_End               = 0x41u, /**< requestSeed - LEV_SAT_RSD - Uneven numbers are for 'requestSeed' */
  LinUDSIf_SUBFUNC_SAT_sendKey_Start                 = 0x04u, /**< sendKey - LEV_SAT_SK - Even numbers are for 'sendKey' */
  LinUDSIf_SUBFUNC_SAT_sendKey_End                   = 0x42u, /**< sendKey - LEV_SAT_SK - Even numbers are for 'sendKey' */
  LinUDSIf_SUBFUNC_SAT_ISOSAEReserved_Start_0x05     = 0x43u, /**< ISOSAEReserved Area Start - LEV_SAT_ISOSAERESRVD */
  LinUDSIf_SUBFUNC_SAT_ISOSAEReserved_End_0x3F       = 0x5Eu, /**< ISOSAEReserved Area End - LEV_SAT_ISOSAERESRVD */
  LinUDSIf_SUBFUNC_SAT_ISO26021_2_requestSeed_values = 0x5Fu, /**< ISO26021-2 requestSeed values - LEV_SAT_SK */
  LinUDSIf_SUBFUNC_SAT_ISO26021_2_sendKey_values     = 0x60u, /**< ISO26021-2 sendKey values - LEV_SAT_ISOSAERESRVD */
  LinUDSIf_SUBFUNC_SAT_systemSupplierSpecific_Start  = 0x60u, /**< systemSupplierSpecific Area Start - LEV_SAT_SSS */
  LinUDSIf_SUBFUNC_SAT_systemSupplierSpecific_End    = 0x7Eu  /**< systemSupplierSpecific Area End - LEV_SAT_SSS */
};

typedef enum LinUDSIf_eSubFunc_securityAccessType LinUDSIf_eSubFunc_securityAccessType_t;

/* ****************************************************************************/
/* ********************* CommunicationControl Service *************************/
/* ****************************************************************************/
typedef Lin_uint8_t  LinUDSIf_ComCtrlType_t;
typedef Lin_uint8_t  LinUDSIf_CommunicationType_t;
typedef Lin_uint16_t LinUDSIf_NIN_t;

/***************************************************************************//**
 *
 ******************************************************************************/
struct LinUDSIf_sCommunicationType
{
    Lin_uint8_t Bit_0_1  :2;
    Lin_uint8_t Reserved :2;
    Lin_uint8_t Bit_4_7  :4;
};

typedef struct LinUDSIf_sCommunicationType    LinUDSIf_sCommunicationType_t;
typedef        LinUDSIf_sCommunicationType_t* LinUDSIf_pCommunicationType_t;

/***************************************************************************//**
 * CommunicationControl (0x28) - Sub-Function 'controlType' - LEV_CTRLTP_
 ******************************************************************************/
enum LinUDSIf_eSubFunc_controlType
{
 LinUDSIf_SUBFUNC_CTRLTP_enableRxAndTx                                      = 0x00u, /**< enableRxAndTx - LEV_CTRLTP_ERXTX */
 LinUDSIf_SUBFUNC_CTRLTP_enableRxAndDisableTx                               = 0x01u, /**< enableRxAndDisableTx - LEV_CTRLTP_ERXDTX */
 LinUDSIf_SUBFUNC_CTRLTP_disableRxAndEnableTx                               = 0x02u, /**< disableRxAndEnableTx - LEV_CTRLTP_DRXETX */
 LinUDSIf_SUBFUNC_CTRLTP_disableRxAndTx                                     = 0x03u, /**< disableRxAndTx - LEV_CTRLTP_DRXTX */
 LinUDSIf_SUBFUNC_CTRLTP_enableRxAndDisableTxWithEnhancedAddressInformation = 0x04u, /**< enableRxAndDisableTxWithEnhancedAddressInformation - LEV_CTRLTP_ERXDTXWEAI */
 LinUDSIf_SUBFUNC_CTRLTP_enableRxAndTxWithEnhancedAddressInformation        = 0x05u, /**< enableRxAndTxWithEnhancedAddressInformation - LEV_CTRLTP_ERXTXWEAI */
 LinUDSIf_SUBFUNC_CTRLTP_ISOSAEReserved_Start_0x05                          = 0x06u, /**< ISOSAEReserved Area Start - LEV_CTRLTP_ISOSAERESRVD */
 LinUDSIf_SUBFUNC_CTRLTP_ISOSAEReserved_End_0x3F                            = 0x3Fu, /**< ISOSAEReserved Area End - LEV_CTRLTP_ISOSAERESRVD */
 LinUDSIf_SUBFUNC_CTRLTP_vehicleManufacturerSpecific_Start                  = 0x40u, /**< vehicleManufacturerSpecific Area Start - LEV_CTRLTP_VMS  */
 LinUDSIf_SUBFUNC_CTRLTP_vehicleManufacturerSpecific_End                    = 0x5Fu, /**< vehicleManufacturerSpecific Area End - LEV_CTRLTP_VMS */
 LinUDSIf_SUBFUNC_CTRLTP_systemSupplierSpecific_Start                       = 0x60u, /**< systemSupplierSpecific Area Start - LEV_CTRLTP_SS */
 LinUDSIf_SUBFUNC_CTRLTP_systemSupplierSpecific_End                         = 0x7Eu, /**< systemSupplierSpecific Area End - LEV_CTRLTP_DS_SSS */
 LinUDSIf_SUBFUNC_CTRLTP_ISOSAEReserved_0x7F                                = 0x7Fu  /**< ISOSAEReserved - LEV_CTRLTP_ISOSAERESRVD */
};

typedef enum LinUDSIf_eSubFunc_controlType LinUDSIf_eSubFunc_controlType_t;

/***************************************************************************//**
 * CommunicationControl (0x28) - communicationType parameter - CTP
 ******************************************************************************/
#define LINUDSIF_CTP_BIT_0_1_MSK     0x03u  /**< communicationType - mask for bit 0 to 1 */
#define LINUDSIF_CTP_BIT_4_7_MSK     0xF0u  /**< communicationType - mask for bit 4 to 7 */

#define LINUDSIF_CTP_EXTRACT_BIT_0_1(CTP) ((CTP) & LINUDSIF_CTP_BIT_0_1_MSK)          /**< communicationType - bit 0 to 1 extraction macro. */
#define LINUDSIF_CTP_EXTRACT_BIT_4_7(CTP) (((CTP) & LINUDSIF_CTP_BIT_4_7_MSK) >> 4u)  /**< communicationType - bit 4 to 7 extraction macro. */

enum LinUDSIf_communicationType_Bit_0_1
{
  LinUDSIf_CTP_Bit_0_1_ISOSAEReserved                                                         = 0x0u, /**< ISOSAEReserved */
  LinUDSIf_CTP_Bit_0_1_normalCommunicationMessages                                            = 0x1u, /**< normalCommunicationMessages - NCM */
  LinUDSIf_CTP_Bit_0_1_networkManagementCommunicationMessages                                 = 0x2u, /**< networkManagementCommunicationMessages - NWMCM */
  LinUDSIf_CTP_Bit_0_1_networkManagementCommunicationMessages_and_normalCommunicationMessages = 0x3u  /**< networkManagementCommunicationMessages and normalCommunicationMessages  - NWMCM-NCM */
};

typedef enum LinUDSIf_eCommunicationType_Bit_0_1 LinUDSIf_eCommunicationType_Bit_0_1_t;

enum LinUDSIf_eCommunicationType_Bit_4_7
{
  LinUDSIf_CTP_Bit_4_7_Disable_Enable_specified_communicationType                     = 0x00u, /**< Disable / Enable specified communicationType - DISENSCT */
  LinUDSIf_CTP_Bit_4_7_Disable_Enable_specific_subnet_identified_by_subnet_number_1   = 0x01u, /**< Disable / Enable specific subnet identified by subnet number - subnet 1 - DISENSSIVSN */
  LinUDSIf_CTP_Bit_4_7_Disable_Enable_specific_subnet_identified_by_subnet_number_2   = 0x02u, /**< Disable / Enable specific subnet identified by subnet number - subnet 2 - DISENSSIVSN */
  LinUDSIf_CTP_Bit_4_7_Disable_Enable_specific_subnet_identified_by_subnet_number_3   = 0x03u, /**< Disable / Enable specific subnet identified by subnet number - subnet 3 - DISENSSIVSN */
  LinUDSIf_CTP_Bit_4_7_Disable_Enable_specific_subnet_identified_by_subnet_number_4   = 0x04u, /**< Disable / Enable specific subnet identified by subnet number - subnet 4 - DISENSSIVSN */
  LinUDSIf_CTP_Bit_4_7_Disable_Enable_specific_subnet_identified_by_subnet_number_5   = 0x05u, /**< Disable / Enable specific subnet identified by subnet number - subnet 5 - DISENSSIVSN */
  LinUDSIf_CTP_Bit_4_7_Disable_Enable_specific_subnet_identified_by_subnet_number_6   = 0x06u, /**< Disable / Enable specific subnet identified by subnet number - subnet 6 - DISENSSIVSN */
  LinUDSIf_CTP_Bit_4_7_Disable_Enable_specific_subnet_identified_by_subnet_number_7   = 0x07u, /**< Disable / Enable specific subnet identified by subnet number - subnet 7 - DISENSSIVSN */
  LinUDSIf_CTP_Bit_4_7_Disable_Enable_specific_subnet_identified_by_subnet_number_8   = 0x08u, /**< Disable / Enable specific subnet identified by subnet number - subnet 8 - DISENSSIVSN */
  LinUDSIf_CTP_Bit_4_7_Disable_Enable_specific_subnet_identified_by_subnet_number_9   = 0x09u, /**< Disable / Enable specific subnet identified by subnet number - subnet 9 - DISENSSIVSN */
  LinUDSIf_CTP_Bit_4_7_Disable_Enable_specific_subnet_identified_by_subnet_number_10  = 0x0Au, /**< Disable / Enable specific subnet identified by subnet number - subnet 10 - DISENSSIVSN */
  LinUDSIf_CTP_Bit_4_7_Disable_Enable_specific_subnet_identified_by_subnet_number_11  = 0x0Bu, /**< Disable / Enable specific subnet identified by subnet number - subnet 11 - DISENSSIVSN */
  LinUDSIf_CTP_Bit_4_7_Disable_Enable_specific_subnet_identified_by_subnet_number_12  = 0x0Cu, /**< Disable / Enable specific subnet identified by subnet number - subnet 12 - DISENSSIVSN */
  LinUDSIf_CTP_Bit_4_7_Disable_Enable_specific_subnet_identified_by_subnet_number_13  = 0x0Du, /**< Disable / Enable specific subnet identified by subnet number - subnet 13 - DISENSSIVSN */
  LinUDSIf_CTP_Bit_4_7_Disable_Enable_specific_subnet_identified_by_subnet_number_14  = 0x0Eu, /**< Disable / Enable specific subnet identified by subnet number - subnet 14 - DISENSSIVSN */
  LinUDSIf_CTP_Bit_4_7_Disable_Enable_network_which_request_is_received_on            = 0x0Fu, /**< Disable/Enable  network  which  request  is  received  on  (Receiving node (server)) - DENWRIRO */
};

typedef enum LinUDSIf_eCommunicationType_Bit_4_7 LinUDSIf_eCommunicationType_Bit_4_7_t;

/* ****************************************************************************/
/* ************************* TesterPresent Service ****************************/
/* ****************************************************************************/
typedef Lin_uint8_t LinUDSIf_ZeroSubFunction_t;

/* ****************************************************************************/
/* *********************** ControlDTCSetting Service **************************/
/* ****************************************************************************/
typedef Lin_uint8_t  LinUDSIf_DTCSettingType_t;
typedef Lin_uint8_t* LinUDSIf_pDTCSetCtrlOptRcrd_t;

typedef Lin_uint8_t  LinUDSIf_DtcReportType_t;

typedef Lin_uint8_t* LinUDSIf_pDtcParameter_t;
typedef Lin_uint16_t LinUDSIf_DtcParamLen_t;
typedef Lin_uint8_t  LinUDSIf_GroupOfDTC_t[3];

/***************************************************************************//**
 * ControlDTCSetting (0x85) - Sub-Function 'DTCSettingType' - LEV_ETP_
 ******************************************************************************/
enum LinUDSIf_eSubFunc_DTCSettingType
{
  LinUDSIf_SUBFUNC_DTCSTP_ISOSAEReserved_0x00               = 0x00u, /**< ISOSAEReserved - LEV_DTCSTP_ISOSAERESRVD */
  LinUDSIf_SUBFUNC_DTCSTP_on                                = 0x01u, /**< on - LEV_DTCSTP_ON */
  LinUDSIf_SUBFUNC_DTCSTP_off                               = 0x02u, /**< off - LEV_DTCSTP_OFF */
  LinUDSIf_SUBFUNC_DTCSTP_ISOSAEReserved_Start_0x03         = 0x03u, /**< ISOSAEReserved Area Start - LEV_DTCSTP_ISOSAERESRVD */
  LinUDSIf_SUBFUNC_DTCSTP_ISOSAEReserved_End_0x3F           = 0x3Fu, /**< ISOSAEReserved Area End - LEV_DTCSTP_ISOSAERESRVD */
  LinUDSIf_SUBFUNC_DTCSTP_vehicleManufacturerSpecific_Start = 0x40u, /**< vehicleManufacturerSpecific Area Start - LEV_DTCSTP_VMS  */
  LinUDSIf_SUBFUNC_DTCSTP_vehicleManufacturerSpecific_End   = 0x5Fu, /**< vehicleManufacturerSpecific Area End - LEV_DTCSTP_VMS */
  LinUDSIf_SUBFUNC_DTCSTP_systemSupplierSpecific_Start      = 0x60u, /**< systemSupplierSpecific Area Start - LEV_DTCSTP_SSS */
  LinUDSIf_SUBFUNC_DTCSTP_systemSupplierSpecific_End        = 0x7Eu, /**< systemSupplierSpecific Area End - LEV_DTCSTP_SSS */
  LinUDSIf_SUBFUNC_DTCSTP_ISOSAEReserved_0x7F               = 0x7Fu  /**< ISOSAEReserved - LEV_DTCSTP_ISOSAERESRVD */
};

typedef enum LinUDSIf_eSubFunc_DTCSettingType LinUDSIf_eSubFunc_DTCSettingType_t;

/* ****************************************************************************/
/* ************************ ResponseOnEvent Service ***************************/
/* ****************************************************************************/
typedef Lin_uint8_t            LinUDSIf_EventType_t;
typedef LinUDSIf_EventType_t*  LinUDSIf_pEventType_t;

typedef Lin_uint8_t   LinUDSIf_EventWindowTime_t;
typedef Lin_uint8_t*  LinUDSIf_pEventTypeRecord_t;
typedef Lin_uint8_t*  LinUDSIf_pServiceToRespondToRecord_t;

/***************************************************************************//**
 *
 ******************************************************************************/
struct LinUDSIf_sEventType
{
    Lin_uint8_t Bit_0_5           :6;
    Lin_uint8_t Bit_6             :1;
    Lin_uint8_t SuppressPosRspMsg :1;
};

typedef struct LinUDSIf_sEventType    LinUDSIf_sEventType_t;
typedef        LinUDSIf_sEventType_t* LinUDSIf_psEventType_t;

/***************************************************************************//**
 *
 ******************************************************************************/
struct LinUDSIf_sETR_Localization
{
  Lin_uint16_t Offset  :10;
  Lin_uint16_t Length  :5;
  Lin_uint16_t Sign    :1;
};

typedef struct LinUDSIf_sETR_Localization     LinUDSIf_sETR_Localization_t;
typedef        LinUDSIf_sETR_Localization_t*  LinUDSIf_pETR_Localization_t;

/***************************************************************************//**
 *
 ******************************************************************************/
struct LinUDSIf_sETR_OCOV
{
    Lin_uint8_t                  DID[2];
    Lin_uint8_t                  ComparisonLogic;
    Lin_uint8_t                  RawReference[3];
    Lin_uint8_t                  HysteresisValue;
    LinUDSIf_sETR_Localization_t Localization;
};

typedef struct LinUDSIf_sETR_OCOV LinUDSIf_sETR_OCOV_t;
typedef LinUDSIf_sETR_OCOV_t* LinUDSIf_pETR_OCOV_t;

/***************************************************************************//**
 * ResponseOnEvent (0x86) - Sub-Function 'eventType' - LEV_ETP_
 ******************************************************************************/
#define LINUDSIF_ET_BIT_0_5_MSK   0x3Fu /**< eventType - mask for bit 0 to 5 */
#define LINUDSIF_ET_BIT_6_MSK     0x40u /**< eventType - mask for bit 6 */

#define LINUDSIF_ET_EXTRACT_BIT_0_5(ET) ((ET) & LINUDSIF_ET_BIT_0_5_MSK)     /**< eventType - bit 0 to 5 extraction macro. */
#define LINUDSIF_ET_EXTRACT_BIT_6(ET)   ((ET) & LINUDSIF_ET_BIT_6_MSK) >> 6u /**< eventType - bit 6 extraction macro. */

enum LinUDSIf_eSubFunc_eventType_Bit_0_5
{
  LinUDSIf_SUBFUNC_ETP_Bit_0_5_stopResponseOnEvent               = 0x00u, /**< stopResponseOnEvent - LEV_ETP_STPROE */
  LinUDSIf_SUBFUNC_ETP_Bit_0_5_onDTCStatusChange                 = 0x01u, /**< onDTCStatusChange - LEV_ETP_ONDTCS */
  LinUDSIf_SUBFUNC_ETP_Bit_0_5_onTimerInterrupt                  = 0x02u, /**< onTimerInterrupt - LEV_ETP_OTI */
  LinUDSIf_SUBFUNC_ETP_Bit_0_5_onChangeOfDataIdentifier          = 0x03u, /**< onChangeOfDataIdentifier - LEV_ETP_OCODID */
  LinUDSIf_SUBFUNC_ETP_Bit_0_5_reportActivatedEvents             = 0x04u, /**< reportActivatedEvents - LEV_ETP_RAE */
  LinUDSIf_SUBFUNC_ETP_Bit_0_5_startResponseOnEvent              = 0x05u, /**< startResponseOnEvent - LEV_ETP_STRTROE */
  LinUDSIf_SUBFUNC_ETP_Bit_0_5_clearResponseOnEvent              = 0x06u, /**< clearResponseOnEvent - LEV_ETP_CLRROE */
  LinUDSIf_SUBFUNC_ETP_Bit_0_5_onComparisonOfValues              = 0x07u, /**< onComparisonOfValues - LEV_ETP_OCOV */
  LinUDSIf_SUBFUNC_ETP_Bit_0_5_ISOSAEReserved_Start_0x08         = 0x08u, /**< ISOSAEReserved Area Start - LEV_ETP_ISOSAERESRVD */
  LinUDSIf_SUBFUNC_ETP_Bit_0_5_ISOSAEReserved_End_0x1F           = 0x1Fu, /**< ISOSAEReserved Area End - LEV_ETP_ISOSAERESRVD */
  LinUDSIf_SUBFUNC_ETP_Bit_0_5_vehicleManufacturerSpecific_Start = 0x20u, /**< vehicleManufacturerSpecific Area Start - LEV_ETP_VMS  */
  LinUDSIf_SUBFUNC_ETP_Bit_0_5_vehicleManufacturerSpecific_End   = 0x2Fu, /**< vehicleManufacturerSpecific Area End - LEV_ETP_VMS */
  LinUDSIf_SUBFUNC_ETP_Bit_0_5_systemSupplierSpecific_Start      = 0x30u, /**< systemSupplierSpecific Area Start - LEV_ETP_SSS */
  LinUDSIf_SUBFUNC_ETP_Bit_0_5_systemSupplierSpecific_End        = 0x3Eu, /**< systemSupplierSpecific Area End - LEV_ETP_SSS */
  LinUDSIf_SUBFUNC_ETP_Bit_0_5_ISOSAEReserved_0x3F               = 0x3Fu  /**< ISOSAEReserved - LEV_ETP_ISOSAERESRVD */
};

typedef enum LinUDSIf_eSubFunc_eventType_Bit_0_5 LinUDSIf_eSubFunc_eventType_Bit_0_5_t;

enum LinUDSIf_eSubFunc_eventType_Bit_6
{
  LinUDSIf_SUBFUNC_ETP_Bit_6_doNotStoreEvent = 0x00u, /**< doNotStoreEvent - LEV_ETP_DNSE */
  LinUDSIf_SUBFUNC_ETP_Bit_6_storeEvent      = 0x01u, /**< storeEvent - LEV_ETP_SE */
};

typedef enum LinUDSIf_eSubFunc_eventType_Bit_6 LinUDSIf_eSubFunc_eventType_Bit_6_t;

/***************************************************************************//**
 * ResponseOnEvent (0x86) - eventWindowTime parameter - EWT
 ******************************************************************************/
enum LinUDSIf_eEventWindowTime
{
  LinUDSIf_EWT_ISOSAEReserved_0x00               = 0x00u, /**< ISOSAEReserved - ISOSAERESRVD*/
  LinUDSIf_EWT_ISOSAEReserved_0x01               = 0x01u, /**< ISOSAEReserved - ISOSAERESRVD*/
  LinUDSIf_EWT_infiniteTimeToResponse            = 0x02u, /**< infiniteTimeToResponse - ITTR */
  LinUDSIf_EWT_vehicleManufacturerSpecific_Start = 0x03u, /**< vehicleManufacturerSpecific - VMS */
  LinUDSIf_EWT_vehicleManufacturerSpecific_End   = 0x7Fu, /**< vehicleManufacturerSpecific - VMS */
  LinUDSIf_EWT_ISOSAEReserved_Start_0x80         = 0x80u, /**< ISOSAEReserved - ISOSAERESRVD */
  LinUDSIf_EWT_ISOSAEReserved_Start_0xFF         = 0xFFu  /**< ISOSAEReserved - ISOSAERESRVD */
};

typedef enum LinUDSIf_eEventWindowTime LinUDSIf_eEventWindowTime_t;

/* ****************************************************************************/
/* ****************** DATA TRANSMISSION FUNCTIONAL UNIT ***********************/
/* ****************************************************************************/
typedef Lin_uint16_t     LinUDSIf_DID_t;  /**< TODO: Add description */
typedef LinUDSIf_DID_t* LinUDSIf_pDID_t; /**< TODO: Add description */

typedef Lin_uint16_t                      LinUDSIf_InputOutputIdentifier_t; /**< TODO: Add description */
typedef LinUDSIf_InputOutputIdentifier_t* LinUDSIf_pInputOutputIdentifier_t; /**< TODO: Add description */

typedef Lin_pvoid_t  LinUDSIf_DidLength_t; /**< TODO: Add description */

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param[in]  enclosing_method_argument TODO: Parameter description
 *
 * @return         TODO: return description
 *
 * Precondition:   (optional) Which are the conditions to call this function? i.e. none
 *
 * Postcondition:  (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see            (optional) Crossreference
 *
 ******************************************************************************/
typedef Lin_Bool_t (*LinUDSIf_RdDataByIdCbFun_t) (LinUDSIf_pGenericEnvData_t genericUdsEnvData,  LinUDSIf_cpInterfaceFunctions_t  udsIfFuns,
                                                  LinUDSIf_DID_t             did,                LinUDSIf_AppendDataToMsgBuffer_t appendDataToMsgBufFun,
                                                  LinUDSIf_pNRC_t            pNcr,               LinUDSIf_pGenericCbCtxData_t     genericUdsCbCtxData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param[in]  enclosing_method_argument TODO: Parameter description
 *
 * @return         TODO: return description
 *
 * Precondition:   (optional) Which are the conditions to call this function? i.e. none
 *
 * Postcondition:  (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see            (optional) Crossreference
 *
 ******************************************************************************/
typedef Lin_Bool_t (*LinUDSIf_WrtDataByIdCbFun_t) (LinUDSIf_pGenericEnvData_t   genericUdsEnvData,  LinUDSIf_cpInterfaceFunctions_t udsIfFuns,
                                                   LinUDSIf_DID_t               did,                LinUDSIf_pData_t                data,
                                                   LinUDSIf_Length_t            dataLen,            LinUDSIf_pNRC_t                 pNcr,
                                                   LinUDSIf_pGenericCbCtxData_t genericUdsCbCtxData);

/* ****************************************************************************/
/* ******************** ReadDataByIdentifier Service **************************/
/* ****************************************************************************/

/***************************************************************************//**
 * ReadDataByIdentifier (0x22) - DID
 ******************************************************************************/
#define LINUDSIF_DID_ADSDID      0xF186u
#define LINUDSIF_DID_VMSPNDID    0xF187u
#define LINUDSIF_DID_VMECUSVNDID 0xF189u
#define LINUDSIF_DID_VMECUHNDID  0xF191u
#define LINUDSIF_DID_SNOETDID    0xF197u
#define LINUDSIF_DID_ODXFDID     0xF19Eu

/* ****************************************************************************/
/* ************** STORED DATA TRANSMISSION FUNCTIONAL UNIT ********************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ***************** ClearDiagnosticInformation Service ***********************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ********************** ReadDTCInformation Service **************************/
/* ****************************************************************************/

/***************************************************************************//**
 * ReadDTCInformation (0x19) - DTCStatusMask - DTCSM - DTC status bit definitions
 ******************************************************************************/
enum LinUDSIf_eDTCStatusMask
{
  LinUDSIf_DTCSM_testFailed                         = 0x01u, /**< Bit 0 - testFailed - DTCSM_TF **/
  LinUDSIf_DTCSM_testFailedThisOperationCycle       = 0x02u, /**< Bit 1 - testFailedThisOperationCycle - DTCSM_TFTOC **/
  LinUDSIf_DTCSM_pendingDTC                         = 0x04u, /**< Bit 2 - pendingDTC - DTCSM_PDTC **/
  LinUDSIf_DTCSM_confirmedDTC                       = 0x08u, /**< Bit 3 - confirmedDTC - DTCSM_CDTC **/
  LinUDSIf_DTCSM_testNotCompletedSinceLastClear     = 0x10u, /**< Bit 4 - testNotCompletedSinceLastClear - DTCSM_TNCSLC **/
  LinUDSIf_DTCSM_testFailedSinceLastClear           = 0x20u, /**< Bit 5 - testFailedSinceLastClear - DTCSM_TFSLC **/
  LinUDSIf_DTCSM_testNotCompletedThisOperationCycle = 0x40u, /**< Bit 6 - testNotCompletedThisOperationCycle - DTCSM_TNCTOC **/
  LinUDSIf_DTCSM_warningIndicatorRequested          = 0x80u  /**< Bit 7 - warningIndicatorRequested - DTCSM_WIR **/
};

typedef enum LinUDSIf_eDTCStatusMask LinUDSIf_eDTCStatusMask_t;

/***************************************************************************//**
 *  ReadDTCInformation - DTCStatusMask - DTCSM
 ******************************************************************************/
struct LinUDSIf_sDTCStatusMask
{
    Lin_uint8_t TF     :1; /**< Bit 0 - testFailed                         **/
    Lin_uint8_t TFTOC  :1; /**< Bit 1 - testFailedThisOperationCycle       **/
    Lin_uint8_t PDTC   :1; /**< Bit 2 - pendingDTC                         **/
    Lin_uint8_t CDTC   :1; /**< Bit 3 - confirmedDTC                       **/
    Lin_uint8_t TNCSLC :1; /**< Bit 4 - testNotCompletedSinceLastClear     **/
    Lin_uint8_t TFSLC  :1; /**< Bit 5 - testFailedSinceLastClear           **/
    Lin_uint8_t TNCTOC :1; /**< Bit 6 - testNotCompletedThisOperationCycle **/
    Lin_uint8_t WIR    :1; /**< Bit 7 - warningIndicatorRequested          **/
};

/***************************************************************************//**
 * ReadDTCInformation (0x19) - Sub-Function 'reportType'
 ******************************************************************************/
enum LinUDSIf_eSubFunc_reportType
{
  LinUDSIf_SUBFUNC_RPT_ISOSAEReserved_0x00                             = 0x00u, /**< ISOSAEReserved - ISOSAERESRVD */
  LinUDSIf_SUBFUNC_RPT_reportNumberOfDTCByStatusMask                   = 0x01u, /**< reportNumberOfDTCByStatusMask - RNODTCBSM */
  LinUDSIf_SUBFUNC_RPT_reportDTCByStatusMask                           = 0x02u, /**< reportDTCByStatusMask - RDTCBSM */
  LinUDSIf_SUBFUNC_RPT_reportDTCSnapshotIdentification                 = 0x03u, /**< reportDTCSnapshotIdentification - RDTCSSI */
  LinUDSIf_SUBFUNC_RPT_reportDTCSnapshotRecordByDTCNumber              = 0x04u, /**< reportDTCSnapshotRecordByDTCNumber - RDTCSSBDTC */
  LinUDSIf_SUBFUNC_RPT_reportDTCStoredDataByRecordNumber               = 0x05u, /**< reportDTCStoredDataByRecordNumber - RDTCSDBRN */
  LinUDSIf_SUBFUNC_RPT_reportDTCExtDataRecordByDTCNumber               = 0x06u, /**< reportDTCExtDataRecordByDTCNumber - RDTCEDRBDN */
  LinUDSIf_SUBFUNC_RPT_reportNumberOfDTCBySeverityMaskRecord           = 0x07u, /**< reportNumberOfDTCBySeverityMaskRecord - RNODTCBSMR */
  LinUDSIf_SUBFUNC_RPT_reportDTCBySeverityMaskRecord                   = 0x08u, /**< reportDTCBySeverityMaskRecord - RDTCBSMR */
  LinUDSIf_SUBFUNC_RPT_reportSeverityInformationOfDTC                  = 0x09u, /**< reportSeverityInformationOfDTC - RSIODTC */
  LinUDSIf_SUBFUNC_RPT_reportSupportedDTC                              = 0x0Au, /**< reportSupportedDTC - RSUPDTC */
  LinUDSIf_SUBFUNC_RPT_reportFirstTestFailedDTC                        = 0x0Bu, /**< reportFirstTestFailedDTC - RFTFDTC */
  LinUDSIf_SUBFUNC_RPT_reportFirstConfirmedDTC                         = 0x0Cu, /**< reportFirstConfirmedDTC - RFCDTC */
  LinUDSIf_SUBFUNC_RPT_reportMostRecentTestFailedDTC                   = 0x0Du, /**< reportMostRecentTestFailedDTC - RMRTFDTC */
  LinUDSIf_SUBFUNC_RPT_reportMostRecentConfirmedDTC                    = 0x0Eu, /**< reportMostRecentConfirmedDTC - RMRCDTC */
  LinUDSIf_SUBFUNC_RPT_reportMirrorMemoryDTCByStatusMask               = 0x0Fu, /**< reportMirrorMemoryDTCByStatusMask - RMMDTCBSM */
  LinUDSIf_SUBFUNC_RPT_reportMirrorMemoryDTCExtDataRecordByDTCNumber   = 0x10u, /**< reportMirrorMemoryDTCExtDataRecordByDTCNumber - RMMDEDRBDN */
  LinUDSIf_SUBFUNC_RPT_reportNumberOfMirrorMemoryDTCByStatusMask       = 0x11u, /**< reportNumberOfMirrorMemoryDTCByStatusMask - RNOMMDTCBSM */
  LinUDSIf_SUBFUNC_RPT_reportNumberOfEmissionsOBDDTCByStatusMask       = 0x12u, /**< reportNumberOfEmissionsOBDDTCByStatusMask - RNOOEOBDDTCBSM */
  LinUDSIf_SUBFUNC_RPT_reportEmissionsOBDDTCByStatusMask               = 0x13u, /**< reportEmissionsOBDDTCByStatusMask - ROBDDTCBSM */
  LinUDSIf_SUBFUNC_RPT_reportDTCFaultDetectionCounter                  = 0x14u, /**< reportDTCFaultDetectionCounter - RDTCFDC */
  LinUDSIf_SUBFUNC_RPT_reportDTCWithPermanentStatus                    = 0x15u, /**< reportDTCWithPermanentStatus - RDTCWPS */
  LinUDSIf_SUBFUNC_RPT_reportDTCExtDataRecordByRecordNumber            = 0x16u, /**< reportDTCExtDataRecordByRecordNumber - RDTCEDBR */
  LinUDSIf_SUBFUNC_RPT_reportUserDefMemoryDTCByStatusMask              = 0x17u, /**< reportUserDefMemoryDTCByStatusMask - RUDMDTCBSM */
  LinUDSIf_SUBFUNC_RPT_reportUserDefMemoryDTCSnapshotRecordByDTCNumber = 0x18u, /**< reportUserDefMemoryDTCSnapshotRecordByDTCNumber - RUDMDTCSSBDTC */
  LinUDSIf_SUBFUNC_RPT_reportUserDefMemoryDTCExtDataRecordByDTCNumber  = 0x19u, /**< reportUserDefMemoryDTCExtDataRecordByDTCNumber - RUDMDTCEDRBDN */
  LinUDSIf_SUBFUNC_RPT_ISOSAEReserved_Start_0x1A                       = 0x1Au, /**< ISOSAEReserved - ISOSAERESRVD */
  LinUDSIf_SUBFUNC_RPT_ISOSAEReserved_End_0x41                         = 0x41u, /**< ISOSAEReserved - ISOSAERESRVD */
  LinUDSIf_SUBFUNC_RPT_reportWWHOBDDTCByMaskRecord                     = 0x42u, /**< reportWWHOBDDTCByMaskRecord - RWWHOBDDTCBMR */
  LinUDSIf_SUBFUNC_RPT_ISOSAEReserved_Start_0x43                       = 0x43u, /**< ISOSAEReserved - ISOSAERESRVD */
  LinUDSIf_SUBFUNC_RPT_ISOSAEReserved_End_0x54                         = 0x54u, /**< ISOSAEReserved - ISOSAERESRVD */
  LinUDSIf_SUBFUNC_RPT_reportWWHOBDDTCWithPermanentStatus              = 0x55u, /**< reportWWHOBDDTCWithPermanentStatus - RWWHOBDDTCWPS */
  LinUDSIf_SUBFUNC_RPT_ISOSAEReserved_Start_0x56                       = 0x56u, /**< ISOSAEReserved - ISOSAERESRVD */
  LinUDSIf_SUBFUNC_RPT_ISOSAEReserved_End_0x7F                         = 0x7Fu, /**< ISOSAEReserved - ISOSAERESRVD */
};

typedef enum LinUDSIf_eSubFunc_reportType LinUDSIf_SubFunc_reportType_t;

/* ****************************************************************************/
/* ****************** INPUT/OUTPUT CONTROL FUNCTIONAL UNIT ********************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ***************** InputOutputControlByIdentifier Service *******************/
/* ****************************************************************************/
typedef Lin_uint8_t LinUDSIf_IOCtrlParameter_t;

/***************************************************************************//**
 * InputOutputControlByIdentifier (0x2F) -  inputOutputControlParameter parameter - IOCP
 ******************************************************************************/
enum LinUDSIf_eInputOutputControlParameter
{
  LinUDSIf_IOCP_returnControlToECU        = 0x00u, /**< returnControlToECU - RCTECU*/
  LinUDSIf_IOCP_resetToDefault            = 0x01u, /**< resetToDefault - RTD*/
  LinUDSIf_IOCP_freezeCurrentState        = 0x02u, /**< freezeCurrentState - FCS */
  LinUDSIf_IOCP_shortTermAdjustment       = 0x03u, /**< shortTermAdjustment - STA */
  LinUDSIf_IOCP_ISOSAEReserved_Start_0x04 = 0x80u, /**< ISOSAEReserved - ISOSAERESRVD */
  LinUDSIf_IOCP_ISOSAEReserved_Start_0xFF = 0xFFu  /**< ISOSAEReserved - ISOSAERESRVD */
};

typedef enum LinUDSIf_eInputOutputControlParameter LinUDSIf_eInputOutputControlParameter_t;

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param[in]  enclosing_method_argument TODO: Parameter description
 *
 * @return         TODO: return description
 *
 ******************************************************************************/
typedef Lin_Bool_t (*LinUDSIf_IOCtrlCbFun_t) (LinUDSIf_pGenericEnvData_t       genericUdsEnvData,     LinUDSIf_cpInterfaceFunctions_t udsIfFuns,
                                              LinUDSIf_DID_t                   did,                   LinUDSIf_IOCtrlParameter_t      ioCtrlParam,
                                              LinUDSIf_pData_t                 ctrlStateData,         LinUDSIf_Length_t               ctrlStateDataLen,
                                              LinUDSIf_pData_t                 ctrlEnMsk,             LinUDSIf_Length_t               ctrlEnMskLen,
                                              LinUDSIf_AppendDataToMsgBuffer_t appendDataToMsgBufFun, LinUDSIf_pNRC_t                 pNcr,
                                              LinUDSIf_pGenericCbCtxData_t     genericUdsCbCtxData);

/* ****************************************************************************/
/* ************* REMOTE ACTIVATION OF ROUTINE FUNCTIONAL UNIT *****************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ************************ RoutineControl Service ****************************/
/* ****************************************************************************/
typedef Lin_uint8_t  LinUDSIf_RoutineCtrlType_t;
typedef Lin_uint16_t LinUDSIf_RoutineIdentifier_t;
typedef Lin_uint8_t* LinUDSIf_pRoutineCtrlOptRcrd_t;

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param[in]  enclosing_method_argument TODO: Parameter description
 *
 * @return         TODO: return description
 *
 ******************************************************************************/
typedef LinUDSIf_Length_t (*LinUDSIf_RoutineIdLenCbFun_t) (LinUDSIf_pGenericEnvData_t   genericUdsEnvData,  LinUDSIf_cpInterfaceFunctions_t udsIfFuns,
                                                           LinUDSIf_RoutineIdentifier_t routineId,          LinUDSIf_RoutineCtrlType_t      routineCtrlType,
                                                           LinUDSIf_pGenericCbCtxData_t genericUdsCbCtxData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param[in]  enclosing_method_argument TODO: Parameter description
 *
 * @return         TODO: return description
 *
 ******************************************************************************/
typedef Lin_Bool_t (*LinUDSIf_RoutineControlCbFun_t) (LinUDSIf_pGenericEnvData_t   genericUdsEnvData,     LinUDSIf_cpInterfaceFunctions_t  udsIfFuns,
                                                      Lin_Bool_t                   suppressPosRspMsg,     LinUDSIf_RoutineCtrlType_t       routineCtrlType,
                                                      LinUDSIf_RoutineIdentifier_t routineId,             LinUDSIf_pRoutineCtrlOptRcrd_t   pRoutineCtrlOptRcrd,
                                                      LinUDSIf_Length_t            routineCtrlOptRcrdLen, LinUDSIf_AppendDataToMsgBuffer_t appendDataToMsgBufFun,
                                                      LinUDSIf_pNRC_t              pNrc,                  LinUDSIf_pGenericCbCtxData_t     genericUdsCbCtxData);

/***************************************************************************//**
 *
 ******************************************************************************/
struct LinUDSIf_sRoutineIdLookupEntry
{
  LinUDSIf_RoutineIdentifier_t   FirstRoutineId;
  LinUDSIf_RoutineIdentifier_t   LastRoutineId;
  LinUDSIf_RoutineControlCbFun_t RoutineIdCbFun;
  LinUDSIf_RoutineIdLenCbFun_t   LengthCbFun;
  LinUDSIf_pGenericCbCtxData_t   CbCtxData;
};

typedef struct LinUDSIf_sRoutineIdLookupEntry    LinUDSIf_sRoutineIdLookupEntry_t;
typedef LinUDSIf_sRoutineIdLookupEntry_t*        LinUDSIf_pRoutineIdLookupEntry_t;
typedef const LinUDSIf_sRoutineIdLookupEntry_t*  LinUDSIf_cpRoutineIdLookupEntry_t;

/***************************************************************************//**
 *  RoutineControl (0x31) - Sub-Function 'routineControlType' - LEV_RCTP_
 ******************************************************************************/
enum LinUDSIf_eSubFunc_SubroutineControlType
{
  LinUDSIf_SUBFUNC_RCTP_ISOSAEReserved_0x00       = 0x00u, /**< ISOSAEReserved - LEV_RCTP_ISOSAERESRVD */
  LinUDSIf_SUBFUNC_RCTP_startRoutine              = 0x01u, /**< startRoutine - LEV_RCTP_STR */
  LinUDSIf_SUBFUNC_RCTP_stopRoutine               = 0x02u, /**< stopRoutine - LEV_RCTP_STPR */
  LinUDSIf_SUBFUNC_RCTP_requestRoutineResults     = 0x03u, /**< requestRoutineResults - LEV_RCTP_RRR */
  LinUDSIf_SUBFUNC_RCTP_ISOSAEReserved_Start_0x04 = 0x04u, /**< ISOSAEReserved - LEV_RCTP_ISOSAERESRVD */
  LinUDSIf_SUBFUNC_RCTP_ISOSAEReserved_End_0x7F   = 0x7Fu, /**< ISOSAEReserved - LEV_RCTP_ISOSAERESRVD */
};

typedef enum LinUDSIf_eSubFunc_SubroutineControlType LinUDSIf_eSubFunc_SubroutineControlType_t;

/* ****************************************************************************/
/* ******************* UPLOAD/DOWNLOAD FUNCTIONAL UNIT ************************/
/* ****************************************************************************/
typedef Lin_uint8_t  LinUDSIf_CompressionMethod_t;
typedef Lin_uint8_t  LinUDSIf_EncryptionMethod_t;
typedef Lin_uint8_t  LinUDSIf_MemSizeParamLen_t;
typedef Lin_uint8_t  LinUDSIf_MemAddressParamLen_t;
typedef Lin_uint8_t* LinUDSIf_pMemoryAddress_t;
typedef Lin_uint8_t* LinUDSIf_pMemorySize_t;
typedef Lin_uint8_t  LinUDSIf_MaxNumOfBlockLenParamLen_t;
typedef Lin_uint8_t* LinUDSIf_pMaxNumberOfBlockLength_t;
typedef Lin_uint8_t  LinUDSIf_BlkSeqCnt_t;
typedef Lin_uint8_t* LinUDSIf_TransParamRec_t;
typedef Lin_uint16_t LinUDSIf_TransParamRecLen_t;
typedef LinUDSIf_TransParamRecLen_t* LinUDSIf_pTransParamRecLen_t;

/***************************************************************************//**
 *
 ******************************************************************************/
enum LinUDSIf_eTransDirection
{
  LinUDSIf_TD_Upload   = 0u,
  LinUDSIf_TD_Download = 1u
};

typedef enum LinUDSIf_eTransDirection LinUDSIf_eTransDirection_t;

/***************************************************************************//**
 *
 ******************************************************************************/
typedef struct LinUDSIf_sAddressAndLengthFormatIdentifier
{
  LinUDSIf_MemAddressParamLen_t MemAddressParamLen :4;
  LinUDSIf_MemSizeParamLen_t    MemSizeParamLen    :4;
}LinUDSIf_sAddressAndLengthFormatIdentifier_t;

typedef LinUDSIf_sAddressAndLengthFormatIdentifier_t* LinUDSIf_pAddressAndLengthFormatIdentifier_t;

/***************************************************************************//**
 *
 ******************************************************************************/
struct LinUDSIf_sReqDataTransRecParam
{
  LinUDSIf_EncryptionMethod_t   EncryptionMethod;
  LinUDSIf_CompressionMethod_t  CompressionMethod;
  LinUDSIf_MemAddressParamLen_t MemAdrParamLen;
  LinUDSIf_MemSizeParamLen_t    MemSzeParamLen;
  LinUDSIf_pMemoryAddress_t     MemoryAddressPtr;
  LinUDSIf_pMemorySize_t        MemorySizePtr;
};

typedef struct LinUDSIf_sReqDataTransRecParam LinUDSIf_sReqDataTransRecParam_t;
typedef LinUDSIf_sReqDataTransRecParam_t*     LinUDSIf_pReqDataTransRecParam_t;

/***************************************************************************//**
 *
 ******************************************************************************/
struct LinUDSIf_sReqDataTransRetParam
{
  LinUDSIf_MaxNumOfBlockLenParamLen_t MaxNumOfBlockLenParamLen;
  LinUDSIf_pMaxNumberOfBlockLength_t  MaxNumberOfBlockLengthPtr;
  Lin_uint8_t                         MaxNumOfBlkLenBufLen;
};

typedef struct LinUDSIf_sReqDataTransRetParam LinUDSIf_sReqDataTransRetParam_t;
typedef LinUDSIf_sReqDataTransRetParam_t*     LinUDSIf_pReqDataTransRetParam_t;

/***************************************************************************//**
 *
 ******************************************************************************/
struct LinUDSIf_sTransParamRec
{
    LinUDSIf_TransParamRec_t    TransParamRec;
    LinUDSIf_TransParamRecLen_t TransParamRecLen;
};

typedef struct LinUDSIf_sTransParamRec LinUDSIf_sTransParamRec_t;
typedef LinUDSIf_sTransParamRec_t*     LinUDSIf_pTransParamRec_t;

/* ****************************************************************************/
/* ***************************** DID LOOKUP ***********************************/
/* ****************************************************************************/

/***************************************************************************//**
 *
 ******************************************************************************/
typedef enum LinUDSIf_eDidLenType
{
  LinUDSIf_DidLenType_Value    = 0,  /**< TODO: Add description */
  LinUDSIf_DidLenType_Callback = 1   /**< TODO: Add description */
}LinUDSIf_eDidLenType_t;

/***************************************************************************//**
 *
 ******************************************************************************/
struct LinUDSIf_sDidLookupEntry
{
  LinUDSIf_DID_t               FirstDid;
  LinUDSIf_DID_t               LastDid;
  LinUDSIf_RdDataByIdCbFun_t   RdCbFun;
  LinUDSIf_WrtDataByIdCbFun_t  WrtCbFun;
  LinUDSIf_IOCtrlCbFun_t       IOCtrlCbFun;
  LinUDSIf_eDidLenType_t       LengthType;
  LinUDSIf_DidLength_t         Length;
  LinUDSIf_pGenericCbCtxData_t CbCtxData;
};

typedef struct LinUDSIf_sDidLookupEntry    LinUDSIf_sDidLookupEntry_t;
typedef LinUDSIf_sDidLookupEntry_t*        LinUDSIf_pDidLookupEntry_t;
typedef const LinUDSIf_sDidLookupEntry_t*  LinUDSIf_cpDidLookupEntry_t;

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

#endif /* LINUDS_TYPES_H_ */
