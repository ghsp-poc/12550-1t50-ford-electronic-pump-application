
#ifndef __IO_E52398A_H
#define __IO_E52398A_H

#ifdef __IAR_SYSTEMS_ICC__
  #include "io_e52398a_macros.h"

  #ifndef _SYSTEM_BUILD
    #pragma system_include
  #endif
  #pragma language=save
  #pragma language=extended

  #ifdef __ICCARM__
    #if __LITTLE_ENDIAN__ == 0
      #error This file should only be compiled in little endian mode
    #endif
  #endif
#endif

/* ============================================================================== */
/* VIC module register definitions */
/* ============================================================================== */

/* ------------------------------------------------------------------------------ */
/* TABLE_BASE register ... */

__IO_REG16(VIC_TABLE_BASE, 0x00000040u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* TABLE_TYPE register ... */

__IO_REG16(VIC_TABLE_TYPE, 0x00000042u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* MAIN_ENABLE register ... */

__IO_REG16(VIC_MAIN_ENABLE, 0x00000044u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_STATUS0 register ... */

/* IRQ_STATUS0 bitfield type ... */

typedef struct {
  __REG16 mem_prot  :  1;       /* [0] */
  __REG16 sys_state :  1;       /* [1] */
  __REG16 wdog      :  1;       /* [2] */
  __REG16 divider   :  1;       /* [3] */
  __REG16 adc_ctrl  :  1;       /* [4] */
  __REG16 pwmn      :  1;       /* [5] */
  __REG16 pre_pwm   :  1;       /* [6] */
  __REG16 spi_0     :  1;       /* [7] */
  __REG16 spi_1     :  1;       /* [8] */
  __REG16 sci       :  1;       /* [9] */
  __REG16 cctimer_0 :  1;       /* [10] */
  __REG16 cctimer_1 :  1;       /* [11] */
  __REG16 cctimer_2 :  1;       /* [12] */
  __REG16 cctimer_3 :  1;       /* [13] */
  __REG16 gpio_a    :  1;       /* [14] */
  __REG16 gpio_b    :  1;       /* [15] */
} vic_irq_status0_bf_t;

/* IRQ_STATUS0 single bit enum ... */

enum {
  E_VIC_IRQ_STATUS0_MEM_PROT  = 0x0001u,
  E_VIC_IRQ_STATUS0_SYS_STATE = 0x0002u,
  E_VIC_IRQ_STATUS0_WDOG      = 0x0004u,
  E_VIC_IRQ_STATUS0_DIVIDER   = 0x0008u,
  E_VIC_IRQ_STATUS0_ADC_CTRL  = 0x0010u,
  E_VIC_IRQ_STATUS0_PWMN      = 0x0020u,
  E_VIC_IRQ_STATUS0_PRE_PWM   = 0x0040u,
  E_VIC_IRQ_STATUS0_SPI_0     = 0x0080u,
  E_VIC_IRQ_STATUS0_SPI_1     = 0x0100u,
  E_VIC_IRQ_STATUS0_SCI       = 0x0200u,
  E_VIC_IRQ_STATUS0_CCTIMER_0 = 0x0400u,
  E_VIC_IRQ_STATUS0_CCTIMER_1 = 0x0800u,
  E_VIC_IRQ_STATUS0_CCTIMER_2 = 0x1000u,
  E_VIC_IRQ_STATUS0_CCTIMER_3 = 0x2000u,
  E_VIC_IRQ_STATUS0_GPIO_A    = 0x4000u,
  E_VIC_IRQ_STATUS0_GPIO_B    = 0x8000u
};

/* IRQ_STATUS0 address mapping ... */

__IO_REG16_BIT(VIC_IRQ_STATUS0, 0x00000070u, __READ_WRITE, vic_irq_status0_bf_t);

/* ------------------------------------------------------------------------------ */
/* IRQ_STATUS1 register ... */

__IO_REG16(VIC_IRQ_STATUS1, 0x00000072u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_MASK0 register ... */

__IO_REG16(VIC_IRQ_MASK0, 0x00000074u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_MASK1 register ... */

__IO_REG16(VIC_IRQ_MASK1, 0x00000076u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VENABLE register ... */

__IO_REG16(VIC_IRQ_VENABLE, 0x00000078u, __WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VDISABLE register ... */

__IO_REG16(VIC_IRQ_VDISABLE, 0x0000007Au, __WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VMAX register ... */

__IO_REG16(VIC_IRQ_VMAX, 0x0000007Cu, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VNO register ... */

__IO_REG16(VIC_IRQ_VNO, 0x0000007Eu, __READ_WRITE);

/* ============================================================================== */
/* WDOG module register definitions */
/* ============================================================================== */

/* ------------------------------------------------------------------------------ */
/* CONTROL register ... */

/* CONTROL bitfield type ... */

typedef struct {
  __REG16 run_enable :  1;       /* [0] */
  __REG16 restart    :  1;       /* [1] */
  __REG16            :  6;
  __REG16 password   :  8;       /* [15:8] */
} wdog_control_bf_t;

/* CONTROL single bit enum ... */

enum {
  E_WDOG_CONTROL_RUN_ENABLE = 0x0001u,
  E_WDOG_CONTROL_RESTART    = 0x0002u
};

/* CONTROL address mapping ... */

__IO_REG16_BIT(WDOG_CONTROL, 0x00000080u, __READ_WRITE, wdog_control_bf_t);

/* ------------------------------------------------------------------------------ */
/* WINDOW register ... */

/* WINDOW bitfield type ... */

typedef struct {
  __REG16 size   :  4;       /* [3:0] */
  __REG16 enable :  1;       /* [4] */
  __REG16        : 11;
} wdog_window_bf_t;

/* WINDOW single bit enum ... */

enum {
  E_WDOG_WINDOW_ENABLE = 0x0010u
};

/* WINDOW address mapping ... */

__IO_REG16_BIT(WDOG_WINDOW, 0x00000082u, __READ_WRITE, wdog_window_bf_t);

/* ------------------------------------------------------------------------------ */
/* PRESCALER register ... */

__IO_REG16(WDOG_PRESCALER, 0x00000084u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* RELOAD register ... */

__IO_REG16(WDOG_RELOAD, 0x00000086u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* COUNTER register ... */

__IO_REG16(WDOG_COUNTER, 0x00000088u, __READ);

/* ------------------------------------------------------------------------------ */
/* IRQ_STATUS register ... */

__IO_REG16(WDOG_IRQ_STATUS, 0x000000B0u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_MASK register ... */

__IO_REG16(WDOG_IRQ_MASK, 0x000000B4u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VENABLE register ... */

__IO_REG16(WDOG_IRQ_VENABLE, 0x000000B8u, __WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VDISABLE register ... */

__IO_REG16(WDOG_IRQ_VDISABLE, 0x000000BAu, __WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VMAX register ... */

__IO_REG16(WDOG_IRQ_VMAX, 0x000000BCu, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VNO register ... */

__IO_REG16(WDOG_IRQ_VNO, 0x000000BEu, __READ_WRITE);

/* ============================================================================== */
/* MEM_PROT module register definitions */
/* ============================================================================== */

/* ------------------------------------------------------------------------------ */
/* EXEC_ENABLE registers ... */

__IO_REG16(MEM_PROT_EXEC_ENABLE_0, 0x000000C0u, __READ_WRITE);
__IO_REG16(MEM_PROT_EXEC_ENABLE_1, 0x000000C2u, __READ_WRITE);
__IO_REG16(MEM_PROT_EXEC_ENABLE_2, 0x000000C4u, __READ_WRITE);
__IO_REG16(MEM_PROT_EXEC_ENABLE_3, 0x000000C6u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* STACK_ENABLE register ... */

__IO_REG16(MEM_PROT_STACK_ENABLE, 0x000000C8u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* SRAM_WR_ENABLE register ... */

__IO_REG16(MEM_PROT_SRAM_WR_ENABLE, 0x000000CAu, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* ACCESS_ADDR register ... */

__IO_REG16(MEM_PROT_ACCESS_ADDR, 0x000000D0u, __READ);

/* ------------------------------------------------------------------------------ */
/* ACCESS_PC register ... */

__IO_REG16(MEM_PROT_ACCESS_PC, 0x000000D2u, __READ);

/* ------------------------------------------------------------------------------ */
/* ACCESS_TYPE register ... */

/* ACCESS_TYPE bitfield type ... */

typedef struct {
  __REG16 type     :  3;       /* [2:0] */
  __REG16          :  1;
  __REG16 bus      :  3;       /* [6:4] */
  __REG16          :  9;
} mem_prot_access_type_bf_t;

/* ACCESS_TYPE address mapping ... */

__IO_REG16_BIT(MEM_PROT_ACCESS_TYPE, 0x000000D4u, __READ, mem_prot_access_type_bf_t);

/* ------------------------------------------------------------------------------ */
/* ACCESS_CLEAR register ... */

__IO_REG16(MEM_PROT_ACCESS_CLEAR, 0x000000D6u, __WRITE);

/* ------------------------------------------------------------------------------ */
/* SYSROM_RD_ENABLE register ... */

__IO_REG16(MEM_PROT_SYSROM_RD_ENABLE, 0x000000E0u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* SYSROM_RD_ENABLE_LOW register ... */

__IO_REG16(MEM_PROT_SYSROM_RD_ENABLE_LOW, 0x000000E2u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_STATUS register ... */

/* IRQ_STATUS bitfield type ... */

typedef struct {
  __REG16 evt_undefined   :  1;       /* [0] */
  __REG16 evt_dmisaligned :  1;       /* [1] */
  __REG16                 : 14;
} mem_prot_irq_status_bf_t;

/* IRQ_STATUS single bit enum ... */

enum {
  E_MEM_PROT_IRQ_STATUS_EVT_UNDEFINED   = 0x0001u,
  E_MEM_PROT_IRQ_STATUS_EVT_DMISALIGNED = 0x0002u
};

/* IRQ_STATUS address mapping ... */

__IO_REG16_BIT(MEM_PROT_IRQ_STATUS, 0x000000F0u, __READ_WRITE, mem_prot_irq_status_bf_t);

/* ------------------------------------------------------------------------------ */
/* IRQ_MASK register ... */

__IO_REG16(MEM_PROT_IRQ_MASK, 0x000000F4u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VENABLE register ... */

__IO_REG16(MEM_PROT_IRQ_VENABLE, 0x000000F8u, __WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VDISABLE register ... */

__IO_REG16(MEM_PROT_IRQ_VDISABLE, 0x000000FAu, __WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VMAX register ... */

__IO_REG16(MEM_PROT_IRQ_VMAX, 0x000000FCu, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VNO register ... */

__IO_REG16(MEM_PROT_IRQ_VNO, 0x000000FEu, __READ_WRITE);

/* ============================================================================== */
/* H430_MUL module register definitions */
/* ============================================================================== */

/* ------------------------------------------------------------------------------ */
/* MPY register ... */

__IO_REG16(H430_MUL_MPY, 0x00000130u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* MPYS register ... */

__IO_REG16(H430_MUL_MPYS, 0x00000132u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* MAC register ... */

__IO_REG16(H430_MUL_MAC, 0x00000134u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* MACS register ... */

__IO_REG16(H430_MUL_MACS, 0x00000136u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* OP2 register ... */

__IO_REG16(H430_MUL_OP2, 0x00000138u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* RESLO register ... */

__IO_REG16(H430_MUL_RESLO, 0x0000013Au, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* RESHI register ... */

__IO_REG16(H430_MUL_RESHI, 0x0000013Cu, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* SUMEXT register ... */

__IO_REG16(H430_MUL_SUMEXT, 0x0000013Eu, __READ);

/* ------------------------------------------------------------------------------ */
/* LAST_MODE register ... */

__IO_REG16(H430_MUL_LAST_MODE, 0x00000100u, __READ);

/* ------------------------------------------------------------------------------ */
/* OP2U register ... */

__IO_REG16(H430_MUL_OP2U, 0x00000102u, __WRITE);

/* ------------------------------------------------------------------------------ */
/* OP2S register ... */

__IO_REG16(H430_MUL_OP2S, 0x00000104u, __WRITE);

/* ------------------------------------------------------------------------------ */
/* OP2SN register ... */

__IO_REG16(H430_MUL_OP2SN, 0x00000106u, __WRITE);

/* ------------------------------------------------------------------------------ */
/* ACCU_EXT register ... */

__IO_REG16(H430_MUL_ACCU_EXT, 0x00000108u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* RESHI_TC register ... */

__IO_REG16(H430_MUL_RESHI_TC, 0x0000010Au, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* RESHI_Q1_15 register ... */

__IO_REG16(H430_MUL_RESHI_Q1_15, 0x0000010Cu, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* SHIFT_RIGHT register ... */

__IO_REG16(H430_MUL_SHIFT_RIGHT, 0x0000010Eu, __WRITE);

/* ------------------------------------------------------------------------------ */
/* SHIFT_LEFT register ... */

__IO_REG16(H430_MUL_SHIFT_LEFT, 0x00000110u, __WRITE);

/* ============================================================================== */
/* DIVIDER module register definitions */
/* ============================================================================== */

/* ------------------------------------------------------------------------------ */
/* OP1 registers ... */

__IO_REG16(DIVIDER_OP1LO, 0x00000140u, __READ_WRITE);
__IO_REG16(DIVIDER_OP1HI, 0x00000142u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* OP2 register ... */

__IO_REG16(DIVIDER_OP2, 0x00000144u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* OP2S register ... */

__IO_REG16(DIVIDER_OP2S, 0x00000146u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* RESULT registers ... */

__IO_REG16(DIVIDER_RESULTLO, 0x00000148u, __READ_WRITE);
__IO_REG16(DIVIDER_RESULTHI, 0x0000014Au, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* REMAINDER register ... */

__IO_REG16(DIVIDER_REMAINDER, 0x0000014Cu, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_STATUS register ... */

__IO_REG16(DIVIDER_IRQ_STATUS, 0x00000170u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_MASK register ... */

__IO_REG16(DIVIDER_IRQ_MASK, 0x00000174u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VENABLE register ... */

__IO_REG16(DIVIDER_IRQ_VENABLE, 0x00000178u, __WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VDISABLE register ... */

__IO_REG16(DIVIDER_IRQ_VDISABLE, 0x0000017Au, __WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VMAX register ... */

__IO_REG16(DIVIDER_IRQ_VMAX, 0x0000017Cu, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VNO register ... */

__IO_REG16(DIVIDER_IRQ_VNO, 0x0000017Eu, __READ_WRITE);

/* ============================================================================== */
/* SYS_STATE module register definitions */
/* ============================================================================== */

/* ------------------------------------------------------------------------------ */
/* MODULE_ENABLE register ... */

/* MODULE_ENABLE bitfield type ... */

typedef struct {
  __REG16             :  1;
  __REG16             :  1;
  __REG16 spi_0       :  1;       /* [2] */
  __REG16 spi_1       :  1;       /* [3] */
  __REG16 gpio_a      :  1;       /* [4] */
  __REG16 gpio_b      :  1;       /* [5] */
  __REG16 gpio_c      :  1;       /* [6] */
  __REG16 cctimer_0   :  1;       /* [7] */
  __REG16 cctimer_1   :  1;       /* [8] */
  __REG16 cctimer_2   :  1;       /* [9] */
  __REG16 cctimer_3   :  1;       /* [10] */
  __REG16 saradc_ctrl :  1;       /* [11] */
  __REG16 pre_pwm     :  1;       /* [12] */
  __REG16 pwmn        :  1;       /* [13] */
  __REG16             :  2;
} sys_state_module_enable_bf_t;

/* MODULE_ENABLE single bit enum ... */

enum {
  E_SYS_STATE_MODULE_ENABLE_RESERVED1   = 0x0001u,
  E_SYS_STATE_MODULE_ENABLE_RESERVED2   = 0x0002u,
  E_SYS_STATE_MODULE_ENABLE_SPI_0       = 0x0004u,
  E_SYS_STATE_MODULE_ENABLE_SPI_1       = 0x0008u,
  E_SYS_STATE_MODULE_ENABLE_GPIO_A      = 0x0010u,
  E_SYS_STATE_MODULE_ENABLE_GPIO_B      = 0x0020u,
  E_SYS_STATE_MODULE_ENABLE_GPIO_C      = 0x0040u,
  E_SYS_STATE_MODULE_ENABLE_CCTIMER_0   = 0x0080u,
  E_SYS_STATE_MODULE_ENABLE_CCTIMER_1   = 0x0100u,
  E_SYS_STATE_MODULE_ENABLE_CCTIMER_2   = 0x0200u,
  E_SYS_STATE_MODULE_ENABLE_CCTIMER_3   = 0x0400u,
  E_SYS_STATE_MODULE_ENABLE_SARADC_CTRL = 0x0800u,
  E_SYS_STATE_MODULE_ENABLE_PRE_PWM     = 0x1000u,
  E_SYS_STATE_MODULE_ENABLE_PWMN        = 0x2000u
};

/* MODULE_ENABLE address mapping ... */

__IO_REG16_BIT(SYS_STATE_MODULE_ENABLE, 0x00000180u, __READ_WRITE, sys_state_module_enable_bf_t);

/* ------------------------------------------------------------------------------ */
/* CONTROL register ... */

/* CONTROL bitfield type ... */

typedef struct {
  __REG16 sys_clk_sel  :  3;       /* [2:0] */
  __REG16 drv_strength :  1;       /* [3] */
  __REG16 target_clk_l :  3;       /* [6:4] */
  __REG16 target_clk_h :  3;       /* [9:7] */
  __REG16              :  6;
} sys_state_control_bf_t;

/* CONTROL single bit enum ... */

enum {
  E_SYS_STATE_CONTROL_DRV_STRENGTH = 0x0008u
};

/* CONTROL address mapping ... */

__IO_REG16_BIT(SYS_STATE_CONTROL, 0x00000182u, __READ_WRITE, sys_state_control_bf_t);

/* ------------------------------------------------------------------------------ */
/* RESET_STATUS register ... */

/* RESET_STATUS bitfield type ... */

typedef struct {
  __REG16 por            :  1;       /* [0] */
  __REG16 vddio_ok       :  1;       /* [1] */
  __REG16 vddc_ok        :  1;       /* [2] */
  __REG16 nrsti          :  1;       /* [3] */
  __REG16 sys_clk_fail   :  1;       /* [4] */
  __REG16 watchdog       :  1;       /* [5] */
  __REG16 sw_reset       :  1;       /* [6] */
  __REG16 cpu_parity     :  1;       /* [7] */
  __REG16 sram_parity    :  1;       /* [8] */
  __REG16 flash_2bit_err :  1;       /* [9] */
  __REG16 flash_1bit_err :  1;       /* [10] */
  __REG16 sram_wr_prot   :  1;       /* [11] */
  __REG16 stack_prot     :  1;       /* [12] */
  __REG16 exec_prot      :  1;       /* [13] */
  __REG16 sw_bl_reset_0  :  1;       /* [14] */
  __REG16 sw_bl_reset_1  :  1;       /* [15] */
} sys_state_reset_status_bf_t;

/* RESET_STATUS single bit enum ... */

enum {
  E_SYS_STATE_RESET_STATUS_POR            = 0x0001u,
  E_SYS_STATE_RESET_STATUS_VDDIO_OK       = 0x0002u,
  E_SYS_STATE_RESET_STATUS_VDDC_OK        = 0x0004u,
  E_SYS_STATE_RESET_STATUS_NRSTI          = 0x0008u,
  E_SYS_STATE_RESET_STATUS_SYS_CLK_FAIL   = 0x0010u,
  E_SYS_STATE_RESET_STATUS_WATCHDOG       = 0x0020u,
  E_SYS_STATE_RESET_STATUS_SW_RESET       = 0x0040u,
  E_SYS_STATE_RESET_STATUS_CPU_PARITY     = 0x0080u,
  E_SYS_STATE_RESET_STATUS_SRAM_PARITY    = 0x0100u,
  E_SYS_STATE_RESET_STATUS_FLASH_2BIT_ERR = 0x0200u,
  E_SYS_STATE_RESET_STATUS_FLASH_1BIT_ERR = 0x0400u,
  E_SYS_STATE_RESET_STATUS_SRAM_WR_PROT   = 0x0800u,
  E_SYS_STATE_RESET_STATUS_STACK_PROT     = 0x1000u,
  E_SYS_STATE_RESET_STATUS_EXEC_PROT      = 0x2000u,
  E_SYS_STATE_RESET_STATUS_SW_BL_RESET_0  = 0x4000u,
  E_SYS_STATE_RESET_STATUS_SW_BL_RESET_1  = 0x8000u
};

/* RESET_STATUS address mapping ... */

__IO_REG16_BIT(SYS_STATE_RESET_STATUS, 0x00000184u, __READ, sys_state_reset_status_bf_t);

/* ------------------------------------------------------------------------------ */
/* _RESET_STATUS_CLEAR register ... */

__IO_REG16(SYS_STATE_RESET_STATUS_CLEAR, 0x00000186u, __WRITE);

/* ------------------------------------------------------------------------------ */
/* RESET_ENABLE register ... */

/* RESET_ENABLE bitfield type ... */

typedef struct {
  __REG16 watchdog       :  1;       /* [0] */
  __REG16 sw_reset       :  1;       /* [1] */
  __REG16 cpu_parity     :  1;       /* [2] */
  __REG16 sram_parity    :  1;       /* [3] */
  __REG16 flash_2bit_err :  1;       /* [4] */
  __REG16 flash_1bit_err :  1;       /* [5] */
  __REG16 sram_wr_prot   :  1;       /* [6] */
  __REG16 stack_prot     :  1;       /* [7] */
  __REG16 exec_prot      :  1;       /* [8] */
  __REG16 sw_bl_reset_0  :  1;       /* [9] */
  __REG16 sw_bl_reset_1  :  1;       /* [10] */
  __REG16                :  5;
} sys_state_reset_enable_bf_t;

/* RESET_ENABLE single bit enum ... */

enum {
  E_SYS_STATE_RESET_ENABLE_WATCHDOG       = 0x0001u,
  E_SYS_STATE_RESET_ENABLE_SW_RESET       = 0x0002u,
  E_SYS_STATE_RESET_ENABLE_CPU_PARITY     = 0x0004u,
  E_SYS_STATE_RESET_ENABLE_SRAM_PARITY    = 0x0008u,
  E_SYS_STATE_RESET_ENABLE_FLASH_2BIT_ERR = 0x0010u,
  E_SYS_STATE_RESET_ENABLE_FLASH_1BIT_ERR = 0x0020u,
  E_SYS_STATE_RESET_ENABLE_SRAM_WR_PROT   = 0x0040u,
  E_SYS_STATE_RESET_ENABLE_STACK_PROT     = 0x0080u,
  E_SYS_STATE_RESET_ENABLE_EXEC_PROT      = 0x0100u,
  E_SYS_STATE_RESET_ENABLE_SW_BL_RESET_0  = 0x0200u,
  E_SYS_STATE_RESET_ENABLE_SW_BL_RESET_1  = 0x0400u
};

/* RESET_ENABLE address mapping ... */

__IO_REG16_BIT(SYS_STATE_RESET_ENABLE, 0x00000188u, __READ_WRITE, sys_state_reset_enable_bf_t);

/* ------------------------------------------------------------------------------ */
/* SW_RESET register ... */

/* SW_RESET bitfield type ... */

typedef struct {
  __REG16 por_flag      :  1;       /* [0] */
  __REG16 sw_reset      :  1;       /* [1] */
  __REG16 sw_bl_reset_0 :  1;       /* [2] */
  __REG16 sw_bl_reset_1 :  1;       /* [3] */
  __REG16               : 12;
} sys_state_sw_reset_bf_t;

/* SW_RESET single bit enum ... */

enum {
  E_SYS_STATE_SW_RESET_POR_FLAG      = 0x0001u,
  E_SYS_STATE_SW_RESET_SW_RESET      = 0x0002u,
  E_SYS_STATE_SW_RESET_SW_BL_RESET_0 = 0x0004u,
  E_SYS_STATE_SW_RESET_SW_BL_RESET_1 = 0x0008u
};

/* SW_RESET address mapping ... */

__IO_REG16_BIT(SYS_STATE_SW_RESET, 0x0000018Au, __READ_WRITE, sys_state_sw_reset_bf_t);

/* ------------------------------------------------------------------------------ */
/* ENABLE_JTAG register ... */

/* ENABLE_JTAG bitfield type ... */

typedef struct {
  __REG16 enable           :  1;       /* [0] */
  __REG16 exit_boot_loader :  1;       /* [1] */
  __REG16                  : 14;
} sys_state_enable_jtag_bf_t;

/* ENABLE_JTAG single bit enum ... */

enum {
  E_SYS_STATE_ENABLE_JTAG_ENABLE           = 0x0001u,
  E_SYS_STATE_ENABLE_JTAG_EXIT_BOOT_LOADER = 0x0002u
};

/* ENABLE_JTAG address mapping ... */

__IO_REG16_BIT(SYS_STATE_ENABLE_JTAG, 0x0000018Cu, __READ_WRITE, sys_state_enable_jtag_bf_t);

/* ------------------------------------------------------------------------------ */
/* DEVICE_ID register ... */

__IO_REG16(SYS_STATE_DEVICE_ID, 0x0000018Eu, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* SIGNATURE registers ... */

__IO_REG16(SYS_STATE_SIGNATURE_0, 0x00000190u, __READ_WRITE);
__IO_REG16(SYS_STATE_SIGNATURE_1, 0x00000192u, __READ_WRITE);
__IO_REG16(SYS_STATE_SIGNATURE_2, 0x00000194u, __READ_WRITE);
__IO_REG16(SYS_STATE_SIGNATURE_3, 0x00000196u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* CALIBRATION register ... */

/* CALIBRATION bitfield type ... */

typedef struct {
  __REG16 cal_vref   :  3;       /* [2:0] */
  __REG16            :  1;
  __REG16 cal_bgap   :  4;       /* [7:4] */
  __REG16 msb_offset :  4;       /* [11:8] */
  __REG16            :  3;
  __REG16 lock       :  1;       /* [15] */
} sys_state_calibration_bf_t;

/* CALIBRATION single bit enum ... */

enum {
  E_SYS_STATE_CALIBRATION_RESERVED   = 0x0008u,
  E_SYS_STATE_CALIBRATION_LOCK       = 0x8000u
};

/* CALIBRATION address mapping ... */

__IO_REG16_BIT(SYS_STATE_CALIBRATION, 0x00000198u, __READ_WRITE, sys_state_calibration_bf_t);

/* ------------------------------------------------------------------------------ */
/* IRQ_STATUS register ... */

/* IRQ_STATUS bitfield type ... */

typedef struct {
  __REG16 watchdog       :  1;       /* [0] */
  __REG16 sw_reset       :  1;       /* [1] */
  __REG16 cpu_parity     :  1;       /* [2] */
  __REG16 sram_parity    :  1;       /* [3] */
  __REG16 flash_2bit_err :  1;       /* [4] */
  __REG16 flash_1bit_err :  1;       /* [5] */
  __REG16 sram_wr_prot   :  1;       /* [6] */
  __REG16 stack_prot     :  1;       /* [7] */
  __REG16 exec_prot      :  1;       /* [8] */
  __REG16 sw_bl_reset_0  :  1;       /* [9] */
  __REG16 sw_bl_reset_1  :  1;       /* [10] */
  __REG16                :  5;
} sys_state_irq_status_bf_t;

/* IRQ_STATUS single bit enum ... */

enum {
  E_SYS_STATE_IRQ_STATUS_WATCHDOG       = 0x0001u,
  E_SYS_STATE_IRQ_STATUS_SW_RESET       = 0x0002u,
  E_SYS_STATE_IRQ_STATUS_CPU_PARITY     = 0x0004u,
  E_SYS_STATE_IRQ_STATUS_SRAM_PARITY    = 0x0008u,
  E_SYS_STATE_IRQ_STATUS_FLASH_2BIT_ERR = 0x0010u,
  E_SYS_STATE_IRQ_STATUS_FLASH_1BIT_ERR = 0x0020u,
  E_SYS_STATE_IRQ_STATUS_SRAM_WR_PROT   = 0x0040u,
  E_SYS_STATE_IRQ_STATUS_STACK_PROT     = 0x0080u,
  E_SYS_STATE_IRQ_STATUS_EXEC_PROT      = 0x0100u,
  E_SYS_STATE_IRQ_STATUS_SW_BL_RESET_0  = 0x0200u,
  E_SYS_STATE_IRQ_STATUS_SW_BL_RESET_1  = 0x0400u
};

/* IRQ_STATUS address mapping ... */

__IO_REG16_BIT(SYS_STATE_IRQ_STATUS, 0x000001B0u, __READ_WRITE, sys_state_irq_status_bf_t);

/* ------------------------------------------------------------------------------ */
/* IRQ_MASK register ... */

__IO_REG16(SYS_STATE_IRQ_MASK, 0x000001B4u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VENABLE register ... */

__IO_REG16(SYS_STATE_IRQ_VENABLE, 0x000001B8u, __WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VDISABLE register ... */

__IO_REG16(SYS_STATE_IRQ_VDISABLE, 0x000001BAu, __WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VMAX register ... */

__IO_REG16(SYS_STATE_IRQ_VMAX, 0x000001BCu, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VNO register ... */

__IO_REG16(SYS_STATE_IRQ_VNO, 0x000001BEu, __READ_WRITE);

/* ============================================================================== */
/* FLASH_CTRL module register definitions */
/* ============================================================================== */

/* ------------------------------------------------------------------------------ */
/* AREA_MAIN_L register ... */

/* AREA_MAIN_L bitfield type ... */

typedef struct {
  __REG16 area :  8;       /* [7:0] */
  __REG16 pass :  8;       /* [15:8] */
} flash_ctrl_area_main_l_bf_t;

/* AREA_MAIN_L address mapping ... */

__IO_REG16_BIT(FLASH_CTRL_AREA_MAIN_L, 0x000001C0u, __READ_WRITE, flash_ctrl_area_main_l_bf_t);

/* ------------------------------------------------------------------------------ */
/* AREA_MAIN_H register ... */

/* AREA_MAIN_H bitfield type ... */

typedef struct {
  __REG16 area :  8;       /* [7:0] */
  __REG16 pass :  8;       /* [15:8] */
} flash_ctrl_area_main_h_bf_t;

/* AREA_MAIN_H address mapping ... */

__IO_REG16_BIT(FLASH_CTRL_AREA_MAIN_H, 0x000001C2u, __READ_WRITE, flash_ctrl_area_main_h_bf_t);

/* ------------------------------------------------------------------------------ */
/* MODE register ... */

/* MODE bitfield type ... */

typedef struct {
  __REG16 mode :  8;       /* [7:0] */
  __REG16 pass :  8;       /* [15:8] */
} flash_ctrl_mode_bf_t;

/* MODE address mapping ... */

__IO_REG16_BIT(FLASH_CTRL_MODE, 0x000001C4u, __READ_WRITE, flash_ctrl_mode_bf_t);

/* ------------------------------------------------------------------------------ */
/* STATUS register ... */

/* STATUS bitfield type ... */

typedef struct {
  __REG16 busy        :  1;       /* [0] */
  __REG16 incomplete  :  1;       /* [1] */
  __REG16 write_error :  1;       /* [2] */
  __REG16 recall_busy :  1;       /* [3] */
  __REG16             : 12;
} flash_ctrl_status_bf_t;

/* STATUS single bit enum ... */

enum {
  E_FLASH_CTRL_STATUS_BUSY        = 0x0001u,
  E_FLASH_CTRL_STATUS_INCOMPLETE  = 0x0002u,
  E_FLASH_CTRL_STATUS_WRITE_ERROR = 0x0004u,
  E_FLASH_CTRL_STATUS_RECALL_BUSY = 0x0008u
};

/* STATUS address mapping ... */

__IO_REG16_BIT(FLASH_CTRL_STATUS, 0x000001C6u, __READ, flash_ctrl_status_bf_t);

/* ------------------------------------------------------------------------------ */
/* AREA_INFO register ... */

/* AREA_INFO bitfield type ... */

typedef struct {
  __REG16 area     :  2;       /* [1:0] */
  __REG16          :  6;
  __REG16 pass     :  8;       /* [15:8] */
} flash_ctrl_area_info_bf_t;

/* AREA_INFO address mapping ... */

__IO_REG16_BIT(FLASH_CTRL_AREA_INFO, 0x000001C8u, __READ_WRITE, flash_ctrl_area_info_bf_t);

/* ------------------------------------------------------------------------------ */
/* READ_INFO register ... */

/* READ_INFO bitfield type ... */

typedef struct {
  __REG16 area     :  2;       /* [1:0] */
  __REG16          :  6;
  __REG16 pass     :  8;       /* [15:8] */
} flash_ctrl_read_info_bf_t;

/* READ_INFO address mapping ... */

__IO_REG16_BIT(FLASH_CTRL_READ_INFO, 0x000001CAu, __READ_WRITE, flash_ctrl_read_info_bf_t);

/* ------------------------------------------------------------------------------ */
/* BIT_ERROR_ADDR register ... */

__IO_REG16(FLASH_CTRL_BIT_ERROR_ADDR, 0x000001CCu, __READ);

/* ------------------------------------------------------------------------------ */
/* WORD_CONFIG register ... */

/* WORD_CONFIG bitfield type ... */

typedef struct {
  __REG16 config   :  6;       /* [5:0] */
  __REG16          :  2;
  __REG16 pass     :  8;       /* [15:8] */
} flash_ctrl_word_config_bf_t;

/* WORD_CONFIG address mapping ... */

__IO_REG16_BIT(FLASH_CTRL_WORD_CONFIG, 0x000001CEu, __READ_WRITE, flash_ctrl_word_config_bf_t);

/* ------------------------------------------------------------------------------ */
/* AREA_MAIN_L_LOCK register ... */

/* AREA_MAIN_L_LOCK bitfield type ... */

typedef struct {
  __REG16 area :  8;       /* [7:0] */
  __REG16 pass :  8;       /* [15:8] */
} flash_ctrl_area_main_l_lock_bf_t;

/* AREA_MAIN_L_LOCK address mapping ... */

__IO_REG16_BIT(FLASH_CTRL_AREA_MAIN_L_LOCK, 0x000001E0u, __READ_WRITE, flash_ctrl_area_main_l_lock_bf_t);

/* ------------------------------------------------------------------------------ */
/* AREA_MAIN_H_LOCK register ... */

/* AREA_MAIN_H_LOCK bitfield type ... */

typedef struct {
  __REG16 area :  8;       /* [7:0] */
  __REG16 pass :  8;       /* [15:8] */
} flash_ctrl_area_main_h_lock_bf_t;

/* AREA_MAIN_H_LOCK address mapping ... */

__IO_REG16_BIT(FLASH_CTRL_AREA_MAIN_H_LOCK, 0x000001E2u, __READ_WRITE, flash_ctrl_area_main_h_lock_bf_t);

/* ============================================================================== */
/* IOMUX_CTRL module register definitions */
/* ============================================================================== */

/* ------------------------------------------------------------------------------ */
/* PAB_IO_SEL registers ... */

/* PAB_IO_SEL bitfield type ... */

typedef struct {
  __REG16 sel_0 :  2;       /* [1:0] */
  __REG16 sel_1 :  2;       /* [3:2] */
  __REG16 sel_2 :  2;       /* [5:4] */
  __REG16 sel_3 :  2;       /* [7:6] */
  __REG16 sel_4 :  2;       /* [9:8] */
  __REG16 sel_5 :  2;       /* [11:10] */
  __REG16 sel_6 :  2;       /* [13:12] */
  __REG16 sel_7 :  2;       /* [15:14] */
} iomux_ctrl_pab_io_sel_bf_t;

/* PAB_IO_SEL address mapping ... */

__IO_REG16_BIT(IOMUX_CTRL_PA_IO_SEL, 0x00000200u, __READ_WRITE, iomux_ctrl_pab_io_sel_bf_t);

__IO_REG16_BIT(IOMUX_CTRL_PB_IO_SEL, 0x00000202u, __READ_WRITE, iomux_ctrl_pab_io_sel_bf_t);

/* ------------------------------------------------------------------------------ */
/* PC0123_IO_SEL register ... */

/* PC0123_IO_SEL bitfield type ... */

typedef struct {
  __REG16 sel_0 :  4;       /* [3:0] */
  __REG16 sel_1 :  4;       /* [7:4] */
  __REG16 sel_2 :  4;       /* [11:8] */
  __REG16 sel_3 :  4;       /* [15:12] */
} iomux_ctrl_pc0123_io_sel_bf_t;

/* PC0123_IO_SEL address mapping ... */

__IO_REG16_BIT(IOMUX_CTRL_PC0123_IO_SEL, 0x00000204u, __READ_WRITE, iomux_ctrl_pc0123_io_sel_bf_t);

/* ------------------------------------------------------------------------------ */
/* PC4567_IO_SEL register ... */

/* PC4567_IO_SEL bitfield type ... */

typedef struct {
  __REG16 sel_4 :  4;       /* [3:0] */
  __REG16 sel_5 :  4;       /* [7:4] */
  __REG16 sel_6 :  4;       /* [11:8] */
  __REG16 sel_7 :  4;       /* [15:12] */
} iomux_ctrl_pc4567_io_sel_bf_t;

/* PC4567_IO_SEL address mapping ... */

__IO_REG16_BIT(IOMUX_CTRL_PC4567_IO_SEL, 0x00000206u, __READ_WRITE, iomux_ctrl_pc4567_io_sel_bf_t);

/* ------------------------------------------------------------------------------ */
/* PX_LOCK registers ... */

/* PX_LOCK bitfield type ... */

typedef struct {
  __REG16 lock :  8;       /* [7:0] */
  __REG16 pass :  8;       /* [15:8] */
} iomux_ctrl_px_lock_bf_t;

/* PX_LOCK address mapping ... */

__IO_REG16_BIT(IOMUX_CTRL_PA_LOCK, 0x00000208u, __READ_WRITE, iomux_ctrl_px_lock_bf_t);

__IO_REG16_BIT(IOMUX_CTRL_PB_LOCK, 0x0000020Au, __READ_WRITE, iomux_ctrl_px_lock_bf_t);

__IO_REG16_BIT(IOMUX_CTRL_PC_LOCK, 0x0000020Cu, __READ_WRITE, iomux_ctrl_px_lock_bf_t);

/* ============================================================================== */
/* SPI module register definitions */
/* ============================================================================== */

/* ------------------------------------------------------------------------------ */
/* DATA register (2 module instances) ... */

__IO_REG16(SPI_1_DATA, 0x00000280u, __READ_WRITE);
__IO_REG16(SPI_0_DATA, 0x00000240u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* DATA_KEEP_NSS register (2 module instances) ... */

__IO_REG16(SPI_1_DATA_KEEP_NSS, 0x00000282u, __WRITE);
__IO_REG16(SPI_0_DATA_KEEP_NSS, 0x00000242u, __WRITE);

/* ------------------------------------------------------------------------------ */
/* CONFIG register (2 module instances) ... */

/* CONFIG bitfield type ... */

typedef struct {
  __REG16 order        :  1;       /* [0] */
  __REG16 phase        :  1;       /* [1] */
  __REG16 polarity     :  1;       /* [2] */
  __REG16 slave        :  1;       /* [3] */
  __REG16 length       :  4;       /* [7:4] */
  __REG16 slave_high_z :  1;       /* [8] */
  __REG16 invert_nss   :  1;       /* [9] */
  __REG16 invert_data  :  1;       /* [10] */
  __REG16 pause_nss    :  1;       /* [11] */
  __REG16 sdi_irq_pol  :  1;       /* [12] */
  __REG16 swap_sdi_sdo :  1;       /* [13] */
  __REG16              :  1;
  __REG16 enable       :  1;       /* [15] */
} spi_config_bf_t;

/* CONFIG single bit enum ... */

enum {
  E_SPI_CONFIG_ORDER        = 0x0001u,
  E_SPI_CONFIG_PHASE        = 0x0002u,
  E_SPI_CONFIG_POLARITY     = 0x0004u,
  E_SPI_CONFIG_SLAVE        = 0x0008u,
  E_SPI_CONFIG_SLAVE_HIGH_Z = 0x0100u,
  E_SPI_CONFIG_INVERT_NSS   = 0x0200u,
  E_SPI_CONFIG_INVERT_DATA  = 0x0400u,
  E_SPI_CONFIG_PAUSE_NSS    = 0x0800u,
  E_SPI_CONFIG_SDI_IRQ_POL  = 0x1000u,
  E_SPI_CONFIG_SWAP_SDI_SDO = 0x2000u,
  E_SPI_CONFIG_RESERVED     = 0x4000u,
  E_SPI_CONFIG_ENABLE       = 0x8000u
};

/* CONFIG address mapping ... */

__IO_REG16_BIT(SPI_1_CONFIG, 0x00000284u, __READ_WRITE, spi_config_bf_t);

__IO_REG16_BIT(SPI_0_CONFIG, 0x00000244u, __READ_WRITE, spi_config_bf_t);

/* ------------------------------------------------------------------------------ */
/* BAUD_CONFIG register (2 module instances) ... */

__IO_REG16(SPI_1_BAUD_CONFIG, 0x00000286u, __READ_WRITE);
__IO_REG16(SPI_0_BAUD_CONFIG, 0x00000246u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* TIMEOUT_CONFIG register (2 module instances) ... */

__IO_REG16(SPI_1_TIMEOUT_CONFIG, 0x00000288u, __READ_WRITE);
__IO_REG16(SPI_0_TIMEOUT_CONFIG, 0x00000248u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* RX_FIFO_TIMEOUT register (2 module instances) ... */

__IO_REG16(SPI_1_RX_FIFO_TIMEOUT, 0x0000028Au, __READ_WRITE);
__IO_REG16(SPI_0_RX_FIFO_TIMEOUT, 0x0000024Au, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* FIFO_CLEAR register (2 module instances) ... */

/* FIFO_CLEAR bitfield type ... */

typedef struct {
  __REG16 rx_fifo_clear :  1;       /* [0] */
  __REG16 tx_fifo_clear :  1;       /* [1] */
  __REG16 reset         :  1;       /* [2] */
  __REG16               : 13;
} spi_fifo_clear_bf_t;

/* FIFO_CLEAR single bit enum ... */

enum {
  E_SPI_FIFO_CLEAR_RX_FIFO_CLEAR = 0x0001u,
  E_SPI_FIFO_CLEAR_TX_FIFO_CLEAR = 0x0002u,
  E_SPI_FIFO_CLEAR_RESET         = 0x0004u
};

/* FIFO_CLEAR address mapping ... */

__IO_REG16_BIT(SPI_1_FIFO_CLEAR, 0x0000028Cu, __WRITE, spi_fifo_clear_bf_t);

__IO_REG16_BIT(SPI_0_FIFO_CLEAR, 0x0000024Cu, __WRITE, spi_fifo_clear_bf_t);

/* ------------------------------------------------------------------------------ */
/* FIFO_LEVELS register (2 module instances) ... */

/* FIFO_LEVELS bitfield type ... */

typedef struct {
  __REG16 rx_fifo_level      :  3;       /* [2:0] */
  __REG16                    :  1;
  __REG16 tx_fifo_level      :  3;       /* [6:4] */
  __REG16                    :  1;
  __REG16 rx_fifo_high_water :  3;       /* [10:8] */
  __REG16                    :  1;
  __REG16 tx_fifo_low_water  :  3;       /* [14:12] */
  __REG16                    :  1;
} spi_fifo_levels_bf_t;

/* FIFO_LEVELS address mapping ... */

__IO_REG16_BIT(SPI_1_FIFO_LEVELS, 0x0000028Eu, __READ_WRITE, spi_fifo_levels_bf_t);

__IO_REG16_BIT(SPI_0_FIFO_LEVELS, 0x0000024Eu, __READ_WRITE, spi_fifo_levels_bf_t);

/* ------------------------------------------------------------------------------ */
/* IRQ_STATUS register (2 module instances) ... */

/* IRQ_STATUS bitfield type ... */

typedef struct {
  __REG16 evt_rx_fifo_ov_err :  1;       /* [0] */
  __REG16 evt_rx_fifo_ur_err :  1;       /* [1] */
  __REG16 evt_tx_fifo_ov_err :  1;       /* [2] */
  __REG16 evt_tx_fifo_ur_err :  1;       /* [3] */
  __REG16 evt_sot            :  1;       /* [4] */
  __REG16 evt_eot            :  1;       /* [5] */
  __REG16 evt_sdi            :  1;       /* [6] */
  __REG16 evt_sclk_timeout   :  1;       /* [7] */
  __REG16 evt_shift_done     :  1;       /* [8] */
  __REG16 rx_fifo_nempty     :  1;       /* [9] */
  __REG16 rx_fifo_timeout    :  1;       /* [10] */
  __REG16 rx_fifo_high_water :  1;       /* [11] */
  __REG16 rx_fifo_full       :  1;       /* [12] */
  __REG16 tx_fifo_empty      :  1;       /* [13] */
  __REG16 tx_fifo_low_water  :  1;       /* [14] */
  __REG16 tx_fifo_nfull      :  1;       /* [15] */
} spi_irq_status_bf_t;

/* IRQ_STATUS single bit enum ... */

enum {
  E_SPI_IRQ_STATUS_EVT_RX_FIFO_OV_ERR = 0x0001u,
  E_SPI_IRQ_STATUS_EVT_RX_FIFO_UR_ERR = 0x0002u,
  E_SPI_IRQ_STATUS_EVT_TX_FIFO_OV_ERR = 0x0004u,
  E_SPI_IRQ_STATUS_EVT_TX_FIFO_UR_ERR = 0x0008u,
  E_SPI_IRQ_STATUS_EVT_SOT            = 0x0010u,
  E_SPI_IRQ_STATUS_EVT_EOT            = 0x0020u,
  E_SPI_IRQ_STATUS_EVT_SDI            = 0x0040u,
  E_SPI_IRQ_STATUS_EVT_SCLK_TIMEOUT   = 0x0080u,
  E_SPI_IRQ_STATUS_EVT_SHIFT_DONE     = 0x0100u,
  E_SPI_IRQ_STATUS_RX_FIFO_NEMPTY     = 0x0200u,
  E_SPI_IRQ_STATUS_RX_FIFO_TIMEOUT    = 0x0400u,
  E_SPI_IRQ_STATUS_RX_FIFO_HIGH_WATER = 0x0800u,
  E_SPI_IRQ_STATUS_RX_FIFO_FULL       = 0x1000u,
  E_SPI_IRQ_STATUS_TX_FIFO_EMPTY      = 0x2000u,
  E_SPI_IRQ_STATUS_TX_FIFO_LOW_WATER  = 0x4000u,
  E_SPI_IRQ_STATUS_TX_FIFO_NFULL      = 0x8000u
};

/* IRQ_STATUS address mapping ... */

__IO_REG16_BIT(SPI_1_IRQ_STATUS, 0x000002A0u, __READ_WRITE, spi_irq_status_bf_t);

__IO_REG16_BIT(SPI_0_IRQ_STATUS, 0x00000260u, __READ_WRITE, spi_irq_status_bf_t);

/* ------------------------------------------------------------------------------ */
/* IRQ_MASK register (2 module instances) ... */

__IO_REG16(SPI_1_IRQ_MASK, 0x000002A4u, __READ_WRITE);
__IO_REG16(SPI_0_IRQ_MASK, 0x00000264u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VENABLE register (2 module instances) ... */

__IO_REG16(SPI_1_IRQ_VENABLE, 0x000002A8u, __WRITE);
__IO_REG16(SPI_0_IRQ_VENABLE, 0x00000268u, __WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VDISABLE register (2 module instances) ... */

__IO_REG16(SPI_1_IRQ_VDISABLE, 0x000002AAu, __WRITE);
__IO_REG16(SPI_0_IRQ_VDISABLE, 0x0000026Au, __WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VMAX register (2 module instances) ... */

__IO_REG16(SPI_1_IRQ_VMAX, 0x000002ACu, __READ_WRITE);
__IO_REG16(SPI_0_IRQ_VMAX, 0x0000026Cu, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VNO register (2 module instances) ... */

__IO_REG16(SPI_1_IRQ_VNO, 0x000002AEu, __READ_WRITE);
__IO_REG16(SPI_0_IRQ_VNO, 0x0000026Eu, __READ_WRITE);

/* ============================================================================== */
/* LINSCI module register definitions */
/* ============================================================================== */

/* ------------------------------------------------------------------------------ */
/* BAUD_RATE register ... */

/* BAUD_RATE bitfield type ... */

typedef struct {
  __REG16 frac :  5;       /* [4:0] */
  __REG16 div  : 11;       /* [15:5] */
} linsci_baud_rate_bf_t;

/* BAUD_RATE address mapping ... */

__IO_REG16_BIT(LINSCI_BAUD_RATE, 0x000002C0u, __READ_WRITE, linsci_baud_rate_bf_t);

/* ------------------------------------------------------------------------------ */
/* UART_CONFIG register ... */

/* UART_CONFIG bitfield type ... */

typedef struct {
  __REG16 re           :  1;       /* [0] */
  __REG16 te           :  1;       /* [1] */
  __REG16 parity       :  2;       /* [3:2] */
  __REG16 stop         :  1;       /* [4] */
  __REG16 mask_brk_err :  1;       /* [5] */
  __REG16 clk_src      :  1;       /* [6] */
  __REG16              :  1;
  __REG16 txd_mask     :  1;       /* [8] */
  __REG16 txd_val      :  1;       /* [9] */
  __REG16 rxd_mask     :  1;       /* [10] */
  __REG16 rxd_val      :  1;       /* [11] */
  __REG16 debounce     :  3;       /* [14:12] */
  __REG16              :  1;
} linsci_uart_config_bf_t;

/* UART_CONFIG single bit enum ... */

enum {
  E_LINSCI_UART_CONFIG_RE           = 0x0001u,
  E_LINSCI_UART_CONFIG_TE           = 0x0002u,
  E_LINSCI_UART_CONFIG_STOP         = 0x0010u,
  E_LINSCI_UART_CONFIG_MASK_BRK_ERR = 0x0020u,
  E_LINSCI_UART_CONFIG_CLK_SRC      = 0x0040u,
  E_LINSCI_UART_CONFIG_RESERVED     = 0x0080u,
  E_LINSCI_UART_CONFIG_TXD_MASK     = 0x0100u,
  E_LINSCI_UART_CONFIG_TXD_VAL      = 0x0200u,
  E_LINSCI_UART_CONFIG_RXD_MASK     = 0x0400u,
  E_LINSCI_UART_CONFIG_RXD_VAL      = 0x0800u
};

/* UART_CONFIG address mapping ... */

__IO_REG16_BIT(LINSCI_UART_CONFIG, 0x000002C2u, __READ_WRITE, linsci_uart_config_bf_t);

/* ------------------------------------------------------------------------------ */
/* LIN_CONFIG register ... */

/* LIN_CONFIG bitfield type ... */

typedef struct {
  __REG16 autobaud          :  1;       /* [0] */
  __REG16 collision         :  1;       /* [1] */
  __REG16 break_thd         :  1;       /* [2] */
  __REG16 header_processing :  1;       /* [3] */
  __REG16 filter_pid        :  1;       /* [4] */
  __REG16 chksum_enable     :  1;       /* [5] */
  __REG16 chksum_type       :  1;       /* [6] */
  __REG16 chksum_insert     :  1;       /* [7] */
  __REG16 suppress_tx_fb    :  1;       /* [8] */
  __REG16 sync_validation   :  1;       /* [9] */
  __REG16 dma_rx_skip_last  :  1;       /* [10] */
  __REG16                   :  5;
} linsci_lin_config_bf_t;

/* LIN_CONFIG single bit enum ... */

enum {
  E_LINSCI_LIN_CONFIG_AUTOBAUD          = 0x0001u,
  E_LINSCI_LIN_CONFIG_COLLISION         = 0x0002u,
  E_LINSCI_LIN_CONFIG_BREAK_THD         = 0x0004u,
  E_LINSCI_LIN_CONFIG_HEADER_PROCESSING = 0x0008u,
  E_LINSCI_LIN_CONFIG_FILTER_PID        = 0x0010u,
  E_LINSCI_LIN_CONFIG_CHKSUM_ENABLE     = 0x0020u,
  E_LINSCI_LIN_CONFIG_CHKSUM_TYPE       = 0x0040u,
  E_LINSCI_LIN_CONFIG_CHKSUM_INSERT     = 0x0080u,
  E_LINSCI_LIN_CONFIG_SUPPRESS_TX_FB    = 0x0100u,
  E_LINSCI_LIN_CONFIG_SYNC_VALIDATION   = 0x0200u,
  E_LINSCI_LIN_CONFIG_DMA_RX_SKIP_LAST  = 0x0400u
};

/* LIN_CONFIG address mapping ... */

__IO_REG16_BIT(LINSCI_LIN_CONFIG, 0x000002C4u, __READ_WRITE, linsci_lin_config_bf_t);

/* ------------------------------------------------------------------------------ */
/* LIN_CONTROL register ... */

/* LIN_CONTROL bitfield type ... */

typedef struct {
  __REG16 abort_rx  :  1;       /* [0] */
  __REG16 abort_tx  :  1;       /* [1] */
  __REG16 tx_chksum :  1;       /* [2] */
  __REG16 rx_sleep  :  1;       /* [3] */
  __REG16           : 12;
} linsci_lin_control_bf_t;

/* LIN_CONTROL single bit enum ... */

enum {
  E_LINSCI_LIN_CONTROL_ABORT_RX  = 0x0001u,
  E_LINSCI_LIN_CONTROL_ABORT_TX  = 0x0002u,
  E_LINSCI_LIN_CONTROL_TX_CHKSUM = 0x0004u,
  E_LINSCI_LIN_CONTROL_RX_SLEEP  = 0x0008u
};

/* LIN_CONTROL address mapping ... */

__IO_REG16_BIT(LINSCI_LIN_CONTROL, 0x000002C6u, __READ_WRITE, linsci_lin_control_bf_t);

/* ------------------------------------------------------------------------------ */
/* STATUS register ... */

/* STATUS bitfield type ... */

typedef struct {
  __REG16 rx_idle          :  1;       /* [0] */
  __REG16 tx_idle          :  1;       /* [1] */
  __REG16 rx_fifo_full     :  1;       /* [2] */
  __REG16 tx_fifo_full     :  1;       /* [3] */
  __REG16 rx_header_state  :  2;       /* [5:4] */
  __REG16 rx_chksum_valid  :  1;       /* [6] */
  __REG16 rx_post_pid_edge :  1;       /* [7] */
  __REG16                  :  8;
} linsci_status_bf_t;

/* STATUS single bit enum ... */

enum {
  E_LINSCI_STATUS_RX_IDLE          = 0x0001u,
  E_LINSCI_STATUS_TX_IDLE          = 0x0002u,
  E_LINSCI_STATUS_RX_FIFO_FULL     = 0x0004u,
  E_LINSCI_STATUS_TX_FIFO_FULL     = 0x0008u,
  E_LINSCI_STATUS_RX_CHKSUM_VALID  = 0x0040u,
  E_LINSCI_STATUS_RX_POST_PID_EDGE = 0x0080u
};

/* STATUS address mapping ... */

__IO_REG16_BIT(LINSCI_STATUS, 0x000002C8u, __READ, linsci_status_bf_t);

/* ------------------------------------------------------------------------------ */
/* ERROR register ... */

/* ERROR bitfield type ... */

typedef struct {
  __REG16 frame_err      :  1;       /* [0] */
  __REG16 parity_err     :  1;       /* [1] */
  __REG16 sync_err       :  1;       /* [2] */
  __REG16 sync_ov        :  1;       /* [3] */
  __REG16 sync_invalid   :  1;       /* [4] */
  __REG16 pid_parity_err :  1;       /* [5] */
  __REG16 bus_collision  :  1;       /* [6] */
  __REG16 concurrent_brk :  1;       /* [7] */
  __REG16 txd_timeout    :  1;       /* [8] */
  __REG16 rx_overflow    :  1;       /* [9] */
  __REG16                :  6;
} linsci_error_bf_t;

/* ERROR single bit enum ... */

enum {
  E_LINSCI_ERROR_FRAME_ERR      = 0x0001u,
  E_LINSCI_ERROR_PARITY_ERR     = 0x0002u,
  E_LINSCI_ERROR_SYNC_ERR       = 0x0004u,
  E_LINSCI_ERROR_SYNC_OV        = 0x0008u,
  E_LINSCI_ERROR_SYNC_INVALID   = 0x0010u,
  E_LINSCI_ERROR_PID_PARITY_ERR = 0x0020u,
  E_LINSCI_ERROR_BUS_COLLISION  = 0x0040u,
  E_LINSCI_ERROR_CONCURRENT_BRK = 0x0080u,
  E_LINSCI_ERROR_TXD_TIMEOUT    = 0x0100u,
  E_LINSCI_ERROR_RX_OVERFLOW    = 0x0200u
};

/* ERROR address mapping ... */

__IO_REG16_BIT(LINSCI_ERROR, 0x000002CAu, __READ_WRITE, linsci_error_bf_t);

/* ------------------------------------------------------------------------------ */
/* LIN_CONFIGURATION register ... */

/* LIN_CONFIGURATION bitfield type ... */

typedef struct {
  __REG16 cbm             :  1;       /* [0] */
  __REG16 txd_timeout_reg :  1;       /* [1] */
  __REG16 timer           :  1;       /* [2] */
  __REG16 dma             :  1;       /* [3] */
  __REG16 version         :  4;       /* [7:4] */
  __REG16                 :  8;
} linsci_lin_configuration_bf_t;

/* LIN_CONFIGURATION single bit enum ... */

enum {
  E_LINSCI_LIN_CONFIGURATION_CBM             = 0x0001u,
  E_LINSCI_LIN_CONFIGURATION_TXD_TIMEOUT_REG = 0x0002u,
  E_LINSCI_LIN_CONFIGURATION_TIMER           = 0x0004u,
  E_LINSCI_LIN_CONFIGURATION_DMA             = 0x0008u
};

/* LIN_CONFIGURATION address mapping ... */

__IO_REG16_BIT(LINSCI_LIN_CONFIGURATION, 0x000002CCu, __READ, linsci_lin_configuration_bf_t);

/* ------------------------------------------------------------------------------ */
/* DATA register ... */

/* DATA bitfield type ... */

typedef struct {
  __REG16 data       :  8;       /* [7:0] */
  __REG16 send_break :  1;       /* [8] */
  __REG16            :  7;
} linsci_data_bf_t;

/* DATA single bit enum ... */

enum {
  E_LINSCI_DATA_SEND_BREAK = 0x0100u
};

/* DATA address mapping ... */

__IO_REG16_BIT(LINSCI_DATA, 0x000002CEu, __READ_WRITE, linsci_data_bf_t);

/* ------------------------------------------------------------------------------ */
/* TBIT2_LENGTH register ... */

__IO_REG16(LINSCI_TBIT2_LENGTH, 0x000002D0u, __READ);

/* ------------------------------------------------------------------------------ */
/* LIN_PID register ... */

__IO_REG16(LINSCI_LIN_PID, 0x000002D2u, __READ);

/* ------------------------------------------------------------------------------ */
/* LIN_CHECKSUM register ... */

/* LIN_CHECKSUM bitfield type ... */

typedef struct {
  __REG16 chksum_val :  8;       /* [7:0] */
  __REG16 initialize :  1;       /* [8] */
  __REG16            :  7;
} linsci_lin_checksum_bf_t;

/* LIN_CHECKSUM single bit enum ... */

enum {
  E_LINSCI_LIN_CHECKSUM_INITIALIZE = 0x0100u
};

/* LIN_CHECKSUM address mapping ... */

__IO_REG16_BIT(LINSCI_LIN_CHECKSUM, 0x000002D4u, __READ_WRITE, linsci_lin_checksum_bf_t);

/* ------------------------------------------------------------------------------ */
/* TIMER register ... */

/* TIMER bitfield type ... */

typedef struct {
  __REG16 enable        :  1;       /* [0] */
  __REG16 clk_src       :  1;       /* [1] */
  __REG16 prepare       :  2;       /* [3:2] */
  __REG16 break_restart :  1;       /* [4] */
  __REG16 txd_timeout_e :  1;       /* [5] */
  __REG16 capture_e     :  1;       /* [6] */
  __REG16 pid_cmp_val_e :  1;       /* [7] */
  __REG16               :  8;
} linsci_timer_bf_t;

/* TIMER single bit enum ... */

enum {
  E_LINSCI_TIMER_ENABLE        = 0x0001u,
  E_LINSCI_TIMER_CLK_SRC       = 0x0002u,
  E_LINSCI_TIMER_BREAK_RESTART = 0x0010u,
  E_LINSCI_TIMER_TXD_TIMEOUT_E = 0x0020u,
  E_LINSCI_TIMER_CAPTURE_E     = 0x0040u,
  E_LINSCI_TIMER_PID_CMP_VAL_E = 0x0080u
};

/* TIMER address mapping ... */

__IO_REG16_BIT(LINSCI_TIMER, 0x000002D6u, __READ_WRITE, linsci_timer_bf_t);

/* ------------------------------------------------------------------------------ */
/* TIMER_COUNTER register ... */

__IO_REG16(LINSCI_TIMER_COUNTER, 0x000002D8u, __READ);

/* ------------------------------------------------------------------------------ */
/* TIMER_COMPARE register ... */

__IO_REG16(LINSCI_TIMER_COMPARE, 0x000002DAu, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* TIMER_CAPTURE register ... */

__IO_REG16(LINSCI_TIMER_CAPTURE, 0x000002DCu, __READ);

/* ------------------------------------------------------------------------------ */
/* DMA_TX_ADDRESS register ... */

__IO_REG16(LINSCI_DMA_TX_ADDRESS, 0x000002E0u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* DMA_TX_ADDRESS_HIGH register ... */

__IO_REG16(LINSCI_DMA_TX_ADDRESS_HIGH, 0x000002E2u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* DMA_TX_LENGTH register ... */

__IO_REG16(LINSCI_DMA_TX_LENGTH, 0x000002E4u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* DMA_RX_ADDRESS register ... */

__IO_REG16(LINSCI_DMA_RX_ADDRESS, 0x000002E8u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* DMA_RX_ADDRESS_HIGH register ... */

__IO_REG16(LINSCI_DMA_RX_ADDRESS_HIGH, 0x000002EAu, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* DMA_RX_LENGTH register ... */

__IO_REG16(LINSCI_DMA_RX_LENGTH, 0x000002ECu, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_STATUS register ... */

/* IRQ_STATUS bitfield type ... */

typedef struct {
  __REG16 rxd_falling     :  1;       /* [0] */
  __REG16 rxd_rising      :  1;       /* [1] */
  __REG16 sci_timer_cmp   :  1;       /* [2] */
  __REG16 sci_timer_ov    :  1;       /* [3] */
  __REG16 bus_err         :  1;       /* [4] */
  __REG16 receiver_err    :  1;       /* [5] */
  __REG16 header_err      :  1;       /* [6] */
  __REG16 break_evt       :  1;       /* [7] */
  __REG16 sync_evt        :  1;       /* [8] */
  __REG16 pid_evt         :  1;       /* [9] */
  __REG16 rx_fifo_full    :  1;       /* [10] */
  __REG16 rx_dma_finished :  1;       /* [11] */
  __REG16 tx_fifo_empty   :  1;       /* [12] */
  __REG16 tx_dma_finished :  1;       /* [13] */
  __REG16 tx_finish_evt   :  1;       /* [14] */
  __REG16 tick_1ms        :  1;       /* [15] */
} linsci_irq_status_bf_t;

/* IRQ_STATUS single bit enum ... */

enum {
  E_LINSCI_IRQ_STATUS_RXD_FALLING     = 0x0001u,
  E_LINSCI_IRQ_STATUS_RXD_RISING      = 0x0002u,
  E_LINSCI_IRQ_STATUS_SCI_TIMER_CMP   = 0x0004u,
  E_LINSCI_IRQ_STATUS_SCI_TIMER_OV    = 0x0008u,
  E_LINSCI_IRQ_STATUS_BUS_ERR         = 0x0010u,
  E_LINSCI_IRQ_STATUS_RECEIVER_ERR    = 0x0020u,
  E_LINSCI_IRQ_STATUS_HEADER_ERR      = 0x0040u,
  E_LINSCI_IRQ_STATUS_BREAK_EVT       = 0x0080u,
  E_LINSCI_IRQ_STATUS_SYNC_EVT        = 0x0100u,
  E_LINSCI_IRQ_STATUS_PID_EVT         = 0x0200u,
  E_LINSCI_IRQ_STATUS_RX_FIFO_FULL    = 0x0400u,
  E_LINSCI_IRQ_STATUS_RX_DMA_FINISHED = 0x0800u,
  E_LINSCI_IRQ_STATUS_TX_FIFO_EMPTY   = 0x1000u,
  E_LINSCI_IRQ_STATUS_TX_DMA_FINISHED = 0x2000u,
  E_LINSCI_IRQ_STATUS_TX_FINISH_EVT   = 0x4000u,
  E_LINSCI_IRQ_STATUS_TICK_1MS        = 0x8000u
};

/* IRQ_STATUS address mapping ... */

__IO_REG16_BIT(LINSCI_IRQ_STATUS, 0x000002F0u, __READ_WRITE, linsci_irq_status_bf_t);

/* ------------------------------------------------------------------------------ */
/* IRQ_MASK register ... */

__IO_REG16(LINSCI_IRQ_MASK, 0x000002F4u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VENABLE register ... */

__IO_REG16(LINSCI_IRQ_VENABLE, 0x000002F8u, __WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VDISABLE register ... */

__IO_REG16(LINSCI_IRQ_VDISABLE, 0x000002FAu, __WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VMAX register ... */

__IO_REG16(LINSCI_IRQ_VMAX, 0x000002FCu, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VNO register ... */

__IO_REG16(LINSCI_IRQ_VNO, 0x000002FEu, __READ_WRITE);

/* ============================================================================== */
/* GPIO module register definitions */
/* ============================================================================== */

/* ------------------------------------------------------------------------------ */
/* DATA_OUT register (3 module instances) ... */

__IO_REG16(GPIO_C_DATA_OUT, 0x00000380u, __READ_WRITE);
__IO_REG16(GPIO_B_DATA_OUT, 0x00000340u, __READ_WRITE);
__IO_REG16(GPIO_A_DATA_OUT, 0x00000300u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* DATA_OE register (3 module instances) ... */

__IO_REG16(GPIO_C_DATA_OE, 0x00000382u, __READ_WRITE);
__IO_REG16(GPIO_B_DATA_OE, 0x00000342u, __READ_WRITE);
__IO_REG16(GPIO_A_DATA_OE, 0x00000302u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* DATA_IN register (3 module instances) ... */

__IO_REG16(GPIO_C_DATA_IN, 0x00000384u, __READ_WRITE);
__IO_REG16(GPIO_B_DATA_IN, 0x00000344u, __READ_WRITE);
__IO_REG16(GPIO_A_DATA_IN, 0x00000304u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* DATA_IE register (3 module instances) ... */

__IO_REG16(GPIO_C_DATA_IE, 0x00000386u, __READ_WRITE);
__IO_REG16(GPIO_B_DATA_IE, 0x00000346u, __READ_WRITE);
__IO_REG16(GPIO_A_DATA_IE, 0x00000306u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* DIRECTION_LOCK register (3 module instances) ... */

__IO_REG16(GPIO_C_DIRECTION_LOCK, 0x00000388u, __READ_WRITE);
__IO_REG16(GPIO_B_DIRECTION_LOCK, 0x00000348u, __READ_WRITE);
__IO_REG16(GPIO_A_DIRECTION_LOCK, 0x00000308u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_STATUS register (3 module instances) ... */

/* IRQ_STATUS bitfield type ... */

typedef struct {
  __REG16 evt_pos_0 :  1;       /* [0] */
  __REG16 evt_neg_0 :  1;       /* [1] */
  __REG16 evt_pos_1 :  1;       /* [2] */
  __REG16 evt_neg_1 :  1;       /* [3] */
  __REG16 evt_pos_2 :  1;       /* [4] */
  __REG16 evt_neg_2 :  1;       /* [5] */
  __REG16 evt_pos_3 :  1;       /* [6] */
  __REG16 evt_neg_3 :  1;       /* [7] */
  __REG16 evt_pos_4 :  1;       /* [8] */
  __REG16 evt_neg_4 :  1;       /* [9] */
  __REG16 evt_pos_5 :  1;       /* [10] */
  __REG16 evt_neg_5 :  1;       /* [11] */
  __REG16 evt_pos_6 :  1;       /* [12] */
  __REG16 evt_neg_6 :  1;       /* [13] */
  __REG16 evt_pos_7 :  1;       /* [14] */
  __REG16 evt_neg_7 :  1;       /* [15] */
} gpio_irq_status_bf_t;

/* IRQ_STATUS single bit enum ... */

enum {
  E_GPIO_IRQ_STATUS_EVT_POS_0 = 0x0001u,
  E_GPIO_IRQ_STATUS_EVT_NEG_0 = 0x0002u,
  E_GPIO_IRQ_STATUS_EVT_POS_1 = 0x0004u,
  E_GPIO_IRQ_STATUS_EVT_NEG_1 = 0x0008u,
  E_GPIO_IRQ_STATUS_EVT_POS_2 = 0x0010u,
  E_GPIO_IRQ_STATUS_EVT_NEG_2 = 0x0020u,
  E_GPIO_IRQ_STATUS_EVT_POS_3 = 0x0040u,
  E_GPIO_IRQ_STATUS_EVT_NEG_3 = 0x0080u,
  E_GPIO_IRQ_STATUS_EVT_POS_4 = 0x0100u,
  E_GPIO_IRQ_STATUS_EVT_NEG_4 = 0x0200u,
  E_GPIO_IRQ_STATUS_EVT_POS_5 = 0x0400u,
  E_GPIO_IRQ_STATUS_EVT_NEG_5 = 0x0800u,
  E_GPIO_IRQ_STATUS_EVT_POS_6 = 0x1000u,
  E_GPIO_IRQ_STATUS_EVT_NEG_6 = 0x2000u,
  E_GPIO_IRQ_STATUS_EVT_POS_7 = 0x4000u,
  E_GPIO_IRQ_STATUS_EVT_NEG_7 = 0x8000u
};

/* IRQ_STATUS address mapping ... */

__IO_REG16_BIT(GPIO_C_IRQ_STATUS, 0x000003B0u, __READ_WRITE, gpio_irq_status_bf_t);

__IO_REG16_BIT(GPIO_B_IRQ_STATUS, 0x00000370u, __READ_WRITE, gpio_irq_status_bf_t);

__IO_REG16_BIT(GPIO_A_IRQ_STATUS, 0x00000330u, __READ_WRITE, gpio_irq_status_bf_t);

/* ------------------------------------------------------------------------------ */
/* IRQ_MASK register (3 module instances) ... */

__IO_REG16(GPIO_C_IRQ_MASK, 0x000003B4u, __READ_WRITE);
__IO_REG16(GPIO_B_IRQ_MASK, 0x00000374u, __READ_WRITE);
__IO_REG16(GPIO_A_IRQ_MASK, 0x00000334u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VENABLE register (3 module instances) ... */

__IO_REG16(GPIO_C_IRQ_VENABLE, 0x000003B8u, __WRITE);
__IO_REG16(GPIO_B_IRQ_VENABLE, 0x00000378u, __WRITE);
__IO_REG16(GPIO_A_IRQ_VENABLE, 0x00000338u, __WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VDISABLE register (3 module instances) ... */

__IO_REG16(GPIO_C_IRQ_VDISABLE, 0x000003BAu, __WRITE);
__IO_REG16(GPIO_B_IRQ_VDISABLE, 0x0000037Au, __WRITE);
__IO_REG16(GPIO_A_IRQ_VDISABLE, 0x0000033Au, __WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VMAX register (3 module instances) ... */

__IO_REG16(GPIO_C_IRQ_VMAX, 0x000003BCu, __READ_WRITE);
__IO_REG16(GPIO_B_IRQ_VMAX, 0x0000037Cu, __READ_WRITE);
__IO_REG16(GPIO_A_IRQ_VMAX, 0x0000033Cu, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VNO register (3 module instances) ... */

__IO_REG16(GPIO_C_IRQ_VNO, 0x000003BEu, __READ_WRITE);
__IO_REG16(GPIO_B_IRQ_VNO, 0x0000037Eu, __READ_WRITE);
__IO_REG16(GPIO_A_IRQ_VNO, 0x0000033Eu, __READ_WRITE);

/* ============================================================================== */
/* CCTIMER module register definitions */
/* ============================================================================== */

/* ------------------------------------------------------------------------------ */
/* PRESCALER register (4 module instances) ... */

__IO_REG16(CCTIMER_3_PRESCALER, 0x00000480u, __READ_WRITE);
__IO_REG16(CCTIMER_2_PRESCALER, 0x00000440u, __READ_WRITE);
__IO_REG16(CCTIMER_1_PRESCALER, 0x00000400u, __READ_WRITE);
__IO_REG16(CCTIMER_0_PRESCALER, 0x000003C0u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* CONTROL register (4 module instances) ... */

/* CONTROL bitfield type ... */

typedef struct {
  __REG16 enable_a  :  1;       /* [0] */
  __REG16 enable_b  :  1;       /* [1] */
  __REG16 restart_a :  1;       /* [2] */
  __REG16 restart_b :  1;       /* [3] */
  __REG16 restart_p :  1;       /* [4] */
  __REG16           : 11;
} cctimer_control_bf_t;

/* CONTROL single bit enum ... */

enum {
  E_CCTIMER_CONTROL_ENABLE_A  = 0x0001u,
  E_CCTIMER_CONTROL_ENABLE_B  = 0x0002u,
  E_CCTIMER_CONTROL_RESTART_A = 0x0004u,
  E_CCTIMER_CONTROL_RESTART_B = 0x0008u,
  E_CCTIMER_CONTROL_RESTART_P = 0x0010u
};

/* CONTROL address mapping ... */

__IO_REG16_BIT(CCTIMER_3_CONTROL, 0x00000482u, __READ_WRITE, cctimer_control_bf_t);

__IO_REG16_BIT(CCTIMER_2_CONTROL, 0x00000442u, __READ_WRITE, cctimer_control_bf_t);

__IO_REG16_BIT(CCTIMER_1_CONTROL, 0x00000402u, __READ_WRITE, cctimer_control_bf_t);

__IO_REG16_BIT(CCTIMER_0_CONTROL, 0x000003C2u, __READ_WRITE, cctimer_control_bf_t);

/* ------------------------------------------------------------------------------ */
/* CONFIG registers (4 module instances) ... */

/* CONFIG bitfield type ... */

typedef struct {
  __REG16 clk_sel     :  2;       /* [1:0] */
  __REG16 restart_sel :  3;       /* [4:2] */
  __REG16 capture_sel :  2;       /* [6:5] */
  __REG16             :  1;
  __REG16 mode        :  2;       /* [9:8] */
  __REG16             :  6;
} cctimer_config_bf_t;

/* CONFIG address mapping ... */

__IO_REG16_BIT(CCTIMER_3_CONFIG_A, 0x00000484u, __READ_WRITE, cctimer_config_bf_t);

__IO_REG16_BIT(CCTIMER_3_CONFIG_B, 0x00000486u, __READ_WRITE, cctimer_config_bf_t);

__IO_REG16_BIT(CCTIMER_2_CONFIG_A, 0x00000444u, __READ_WRITE, cctimer_config_bf_t);

__IO_REG16_BIT(CCTIMER_2_CONFIG_B, 0x00000446u, __READ_WRITE, cctimer_config_bf_t);

__IO_REG16_BIT(CCTIMER_1_CONFIG_A, 0x00000404u, __READ_WRITE, cctimer_config_bf_t);

__IO_REG16_BIT(CCTIMER_1_CONFIG_B, 0x00000406u, __READ_WRITE, cctimer_config_bf_t);

__IO_REG16_BIT(CCTIMER_0_CONFIG_A, 0x000003C4u, __READ_WRITE, cctimer_config_bf_t);

__IO_REG16_BIT(CCTIMER_0_CONFIG_B, 0x000003C6u, __READ_WRITE, cctimer_config_bf_t);

/* ------------------------------------------------------------------------------ */
/* CAPCMP registers (4 module instances) ... */

__IO_REG16(CCTIMER_3_CAPCMP_A, 0x00000488u, __READ_WRITE);
__IO_REG16(CCTIMER_3_CAPCMP_B, 0x0000048Au, __READ_WRITE);
__IO_REG16(CCTIMER_2_CAPCMP_A, 0x00000448u, __READ_WRITE);
__IO_REG16(CCTIMER_2_CAPCMP_B, 0x0000044Au, __READ_WRITE);
__IO_REG16(CCTIMER_1_CAPCMP_A, 0x00000408u, __READ_WRITE);
__IO_REG16(CCTIMER_1_CAPCMP_B, 0x0000040Au, __READ_WRITE);
__IO_REG16(CCTIMER_0_CAPCMP_A, 0x000003C8u, __READ_WRITE);
__IO_REG16(CCTIMER_0_CAPCMP_B, 0x000003CAu, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* COUNTER registers (4 module instances) ... */

__IO_REG16(CCTIMER_3_COUNTER_A, 0x0000048Cu, __READ);
__IO_REG16(CCTIMER_3_COUNTER_B, 0x0000048Eu, __READ);
__IO_REG16(CCTIMER_2_COUNTER_A, 0x0000044Cu, __READ);
__IO_REG16(CCTIMER_2_COUNTER_B, 0x0000044Eu, __READ);
__IO_REG16(CCTIMER_1_COUNTER_A, 0x0000040Cu, __READ);
__IO_REG16(CCTIMER_1_COUNTER_B, 0x0000040Eu, __READ);
__IO_REG16(CCTIMER_0_COUNTER_A, 0x000003CCu, __READ);
__IO_REG16(CCTIMER_0_COUNTER_B, 0x000003CEu, __READ);

/* ------------------------------------------------------------------------------ */
/* PRESEL registers (4 module instances) ... */

/* PRESEL bitfield type ... */

typedef struct {
  __REG16 clk_sel     :  4;       /* [3:0] */
  __REG16 restart_sel :  4;       /* [7:4] */
  __REG16 capture_sel :  4;       /* [11:8] */
  __REG16             :  4;
} cctimer_presel_bf_t;

/* PRESEL address mapping ... */

__IO_REG16_BIT(CCTIMER_3_PRESEL_A, 0x00000490u, __READ_WRITE, cctimer_presel_bf_t);

__IO_REG16_BIT(CCTIMER_3_PRESEL_B, 0x00000492u, __READ_WRITE, cctimer_presel_bf_t);

__IO_REG16_BIT(CCTIMER_2_PRESEL_A, 0x00000450u, __READ_WRITE, cctimer_presel_bf_t);

__IO_REG16_BIT(CCTIMER_2_PRESEL_B, 0x00000452u, __READ_WRITE, cctimer_presel_bf_t);

__IO_REG16_BIT(CCTIMER_1_PRESEL_A, 0x00000410u, __READ_WRITE, cctimer_presel_bf_t);

__IO_REG16_BIT(CCTIMER_1_PRESEL_B, 0x00000412u, __READ_WRITE, cctimer_presel_bf_t);

__IO_REG16_BIT(CCTIMER_0_PRESEL_A, 0x000003D0u, __READ_WRITE, cctimer_presel_bf_t);

__IO_REG16_BIT(CCTIMER_0_PRESEL_B, 0x000003D2u, __READ_WRITE, cctimer_presel_bf_t);

/* ------------------------------------------------------------------------------ */
/* IRQ_STATUS register (4 module instances) ... */

/* IRQ_STATUS bitfield type ... */

typedef struct {
  __REG16 restart_p  :  1;       /* [0] */
  __REG16 overflow_a :  1;       /* [1] */
  __REG16 overflow_b :  1;       /* [2] */
  __REG16 restart_a  :  1;       /* [3] */
  __REG16 restart_b  :  1;       /* [4] */
  __REG16 capture_a  :  1;       /* [5] */
  __REG16 capture_b  :  1;       /* [6] */
  __REG16 compare_a  :  1;       /* [7] */
  __REG16 compare_b  :  1;       /* [8] */
  __REG16            :  7;
} cctimer_irq_status_bf_t;

/* IRQ_STATUS single bit enum ... */

enum {
  E_CCTIMER_IRQ_STATUS_RESTART_P  = 0x0001u,
  E_CCTIMER_IRQ_STATUS_OVERFLOW_A = 0x0002u,
  E_CCTIMER_IRQ_STATUS_OVERFLOW_B = 0x0004u,
  E_CCTIMER_IRQ_STATUS_RESTART_A  = 0x0008u,
  E_CCTIMER_IRQ_STATUS_RESTART_B  = 0x0010u,
  E_CCTIMER_IRQ_STATUS_CAPTURE_A  = 0x0020u,
  E_CCTIMER_IRQ_STATUS_CAPTURE_B  = 0x0040u,
  E_CCTIMER_IRQ_STATUS_COMPARE_A  = 0x0080u,
  E_CCTIMER_IRQ_STATUS_COMPARE_B  = 0x0100u
};

/* IRQ_STATUS address mapping ... */

__IO_REG16_BIT(CCTIMER_3_IRQ_STATUS, 0x000004B0u, __READ_WRITE, cctimer_irq_status_bf_t);

__IO_REG16_BIT(CCTIMER_2_IRQ_STATUS, 0x00000470u, __READ_WRITE, cctimer_irq_status_bf_t);

__IO_REG16_BIT(CCTIMER_1_IRQ_STATUS, 0x00000430u, __READ_WRITE, cctimer_irq_status_bf_t);

__IO_REG16_BIT(CCTIMER_0_IRQ_STATUS, 0x000003F0u, __READ_WRITE, cctimer_irq_status_bf_t);

/* ------------------------------------------------------------------------------ */
/* IRQ_MASK register (4 module instances) ... */

__IO_REG16(CCTIMER_3_IRQ_MASK, 0x000004B4u, __READ_WRITE);
__IO_REG16(CCTIMER_2_IRQ_MASK, 0x00000474u, __READ_WRITE);
__IO_REG16(CCTIMER_1_IRQ_MASK, 0x00000434u, __READ_WRITE);
__IO_REG16(CCTIMER_0_IRQ_MASK, 0x000003F4u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VENABLE register (4 module instances) ... */

__IO_REG16(CCTIMER_3_IRQ_VENABLE, 0x000004B8u, __WRITE);
__IO_REG16(CCTIMER_2_IRQ_VENABLE, 0x00000478u, __WRITE);
__IO_REG16(CCTIMER_1_IRQ_VENABLE, 0x00000438u, __WRITE);
__IO_REG16(CCTIMER_0_IRQ_VENABLE, 0x000003F8u, __WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VDISABLE register (4 module instances) ... */

__IO_REG16(CCTIMER_3_IRQ_VDISABLE, 0x000004BAu, __WRITE);
__IO_REG16(CCTIMER_2_IRQ_VDISABLE, 0x0000047Au, __WRITE);
__IO_REG16(CCTIMER_1_IRQ_VDISABLE, 0x0000043Au, __WRITE);
__IO_REG16(CCTIMER_0_IRQ_VDISABLE, 0x000003FAu, __WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VMAX register (4 module instances) ... */

__IO_REG16(CCTIMER_3_IRQ_VMAX, 0x000004BCu, __READ_WRITE);
__IO_REG16(CCTIMER_2_IRQ_VMAX, 0x0000047Cu, __READ_WRITE);
__IO_REG16(CCTIMER_1_IRQ_VMAX, 0x0000043Cu, __READ_WRITE);
__IO_REG16(CCTIMER_0_IRQ_VMAX, 0x000003FCu, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VNO register (4 module instances) ... */

__IO_REG16(CCTIMER_3_IRQ_VNO, 0x000004BEu, __READ_WRITE);
__IO_REG16(CCTIMER_2_IRQ_VNO, 0x0000047Eu, __READ_WRITE);
__IO_REG16(CCTIMER_1_IRQ_VNO, 0x0000043Eu, __READ_WRITE);
__IO_REG16(CCTIMER_0_IRQ_VNO, 0x000003FEu, __READ_WRITE);

/* ============================================================================== */
/* OSC_CTRL module register definitions */
/* ============================================================================== */

/* ------------------------------------------------------------------------------ */
/* CONTROL register ... */

__IO_REG16(OSC_CTRL_CONTROL, 0x000004C0u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* SYS_CLK_CONFIG register ... */

/* SYS_CLK_CONFIG bitfield type ... */

typedef struct {
  __REG16 sys_clk_delay :  5;       /* [4:0] */
  __REG16 adc_clk_phase :  1;       /* [5] */
  __REG16               : 10;
} osc_ctrl_sys_clk_config_bf_t;

/* SYS_CLK_CONFIG single bit enum ... */

enum {
  E_OSC_CTRL_SYS_CLK_CONFIG_ADC_CLK_PHASE = 0x0020u
};

/* SYS_CLK_CONFIG address mapping ... */

__IO_REG16_BIT(OSC_CTRL_SYS_CLK_CONFIG, 0x000004C2u, __READ_WRITE, osc_ctrl_sys_clk_config_bf_t);

/* ------------------------------------------------------------------------------ */
/* OSC_TRIM1 register ... */

/* OSC_TRIM1 bitfield type ... */

typedef struct {
  __REG16 osc_trim_fine :  8;       /* [7:0] */
  __REG16 osc_trim_raw  :  4;       /* [11:8] */
  __REG16 cyc_offset    :  4;       /* [15:12] */
} osc_ctrl_osc_trim1_bf_t;

/* OSC_TRIM1 address mapping ... */

__IO_REG16_BIT(OSC_CTRL_OSC_TRIM1, 0x000004C4u, __READ_WRITE, osc_ctrl_osc_trim1_bf_t);

/* ------------------------------------------------------------------------------ */
/* OSC_TRIM2 register ... */

/* OSC_TRIM2 bitfield type ... */

typedef struct {
  __REG16 osc_trim_fine :  8;       /* [7:0] */
  __REG16 osc_trim_raw  :  4;       /* [11:8] */
  __REG16 cyc_offset    :  4;       /* [15:12] */
} osc_ctrl_osc_trim2_bf_t;

/* OSC_TRIM2 address mapping ... */

__IO_REG16_BIT(OSC_CTRL_OSC_TRIM2, 0x000004C6u, __READ_WRITE, osc_ctrl_osc_trim2_bf_t);

/* ------------------------------------------------------------------------------ */
/* OSC_CONFIG register ... */

/* OSC_CONFIG bitfield type ... */

typedef struct {
  __REG16 osc_freq_sel  :  1;       /* [0] */
  __REG16 spread_enable :  1;       /* [1] */
  __REG16               : 14;
} osc_ctrl_osc_config_bf_t;

/* OSC_CONFIG single bit enum ... */

enum {
  E_OSC_CTRL_OSC_CONFIG_OSC_FREQ_SEL  = 0x0001u,
  E_OSC_CTRL_OSC_CONFIG_SPREAD_ENABLE = 0x0002u
};

/* OSC_CONFIG address mapping ... */

__IO_REG16_BIT(OSC_CTRL_OSC_CONFIG, 0x000004C8u, __READ_WRITE, osc_ctrl_osc_config_bf_t);

/* ------------------------------------------------------------------------------ */
/* SPREAD_CONFIG register ... */

/* SPREAD_CONFIG bitfield type ... */

typedef struct {
  __REG16 halflength        :  4;       /* [3:0] */
  __REG16 cyc_base          :  8;       /* [11:4] */
  __REG16 double_cyc_offset :  1;       /* [12] */
  __REG16 double_step       :  1;       /* [13] */
  __REG16 double_peak       :  1;       /* [14] */
  __REG16 osc_mask_disable  :  1;       /* [15] */
} osc_ctrl_spread_config_bf_t;

/* SPREAD_CONFIG single bit enum ... */

enum {
  E_OSC_CTRL_SPREAD_CONFIG_DOUBLE_CYC_OFFSET = 0x1000u,
  E_OSC_CTRL_SPREAD_CONFIG_DOUBLE_STEP       = 0x2000u,
  E_OSC_CTRL_SPREAD_CONFIG_DOUBLE_PEAK       = 0x4000u,
  E_OSC_CTRL_SPREAD_CONFIG_OSC_MASK_DISABLE  = 0x8000u
};

/* SPREAD_CONFIG address mapping ... */

__IO_REG16_BIT(OSC_CTRL_SPREAD_CONFIG, 0x000004CAu, __READ_WRITE, osc_ctrl_spread_config_bf_t);

/* ------------------------------------------------------------------------------ */
/* SPREAD_STEP1 register ... */

__IO_REG16(OSC_CTRL_SPREAD_STEP1, 0x000004CCu, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* SPREAD_STEP2 register ... */

__IO_REG16(OSC_CTRL_SPREAD_STEP2, 0x000004CEu, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* SPREAD_STEP3 register ... */

__IO_REG16(OSC_CTRL_SPREAD_STEP3, 0x000004D0u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* WD_TIMING register ... */

__IO_REG16(OSC_CTRL_WD_TIMING, 0x000004D2u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* FREQ_COUNT register ... */

__IO_REG16(OSC_CTRL_FREQ_COUNT, 0x000004D4u, __READ);

/* ------------------------------------------------------------------------------ */
/* FREQ register ... */

__IO_REG16(OSC_CTRL_FREQ, 0x000004D6u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* FREQ_MEAS_CTRL register ... */

/* FREQ_MEAS_CTRL bitfield type ... */

typedef struct {
  __REG16 start_freq_meas  :  1;       /* [0] */
  __REG16 freq_meas_status :  1;       /* [1] */
  __REG16                  : 14;
} osc_ctrl_freq_meas_ctrl_bf_t;

/* FREQ_MEAS_CTRL single bit enum ... */

enum {
  E_OSC_CTRL_FREQ_MEAS_CTRL_START_FREQ_MEAS  = 0x0001u,
  E_OSC_CTRL_FREQ_MEAS_CTRL_FREQ_MEAS_STATUS = 0x0002u
};

/* FREQ_MEAS_CTRL address mapping ... */

__IO_REG16_BIT(OSC_CTRL_FREQ_MEAS_CTRL, 0x000004D8u, __READ_WRITE, osc_ctrl_freq_meas_ctrl_bf_t);

/* ============================================================================== */
/* SARADC_CTRL module register definitions */
/* ============================================================================== */

/* ------------------------------------------------------------------------------ */
/* CFG register ... */

/* CFG bitfield type ... */

typedef struct {
  __REG16 adc_on       :  1;       /* [0] */
  __REG16 adc_reset    :  2;       /* [2:1] */
  __REG16 mux_sel      :  1;       /* [3] */
  __REG16 ext_sel      :  5;       /* [8:4] */
  __REG16 missed_dt_e  :  1;       /* [9] */
  __REG16 store_dt_evt :  1;       /* [10] */
  __REG16 adcloop_up   :  1;       /* [11] */
  __REG16              :  4;
} saradc_ctrl_cfg_bf_t;

/* CFG single bit enum ... */

enum {
  E_SARADC_CTRL_CFG_ADC_ON       = 0x0001u,
  E_SARADC_CTRL_CFG_MUX_SEL      = 0x0008u,
  E_SARADC_CTRL_CFG_MISSED_DT_E  = 0x0200u,
  E_SARADC_CTRL_CFG_STORE_DT_EVT = 0x0400u,
  E_SARADC_CTRL_CFG_ADCLOOP_UP   = 0x0800u
};

/* CFG address mapping ... */

__IO_REG16_BIT(SARADC_CTRL_CFG, 0x00000500u, __READ_WRITE, saradc_ctrl_cfg_bf_t);

/* ------------------------------------------------------------------------------ */
/* CFG_SAR_TIMING register ... */

/* CFG_SAR_TIMING bitfield type ... */

typedef struct {
  __REG16 adc_clk_div        :  3;       /* [2:0] */
  __REG16 mux_phase          :  5;       /* [7:3] */
  __REG16 sampling_extension :  8;       /* [15:8] */
} saradc_ctrl_cfg_sar_timing_bf_t;

/* CFG_SAR_TIMING address mapping ... */

__IO_REG16_BIT(SARADC_CTRL_CFG_SAR_TIMING, 0x00000502u, __READ_WRITE, saradc_ctrl_cfg_sar_timing_bf_t);

/* ------------------------------------------------------------------------------ */
/* DEAD_TIME_DELAY register ... */

__IO_REG16(SARADC_CTRL_DEAD_TIME_DELAY, 0x00000504u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* DEAD_TIME_WAIT registers ... */

__IO_REG16(SARADC_CTRL_DEAD_TIME_WAIT0, 0x00000506u, __READ_WRITE);
__IO_REG16(SARADC_CTRL_DEAD_TIME_WAIT1, 0x00000522u, __READ_WRITE);
__IO_REG16(SARADC_CTRL_DEAD_TIME_WAIT2, 0x00000524u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* SYNC_OUT_CFG register ... */

/* SYNC_OUT_CFG bitfield type ... */

typedef struct {
  __REG16 length :  8;       /* [7:0] */
  __REG16 pol    :  1;       /* [8] */
  __REG16 src    :  2;       /* [10:9] */
  __REG16        :  5;
} saradc_ctrl_sync_out_cfg_bf_t;

/* SYNC_OUT_CFG single bit enum ... */

enum {
  E_SARADC_CTRL_SYNC_OUT_CFG_POL    = 0x0100u
};

/* SYNC_OUT_CFG address mapping ... */

__IO_REG16_BIT(SARADC_CTRL_SYNC_OUT_CFG, 0x00000508u, __READ_WRITE, saradc_ctrl_sync_out_cfg_bf_t);

/* ------------------------------------------------------------------------------ */
/* SYNC_OUT_TRIG register ... */

__IO_REG16(SARADC_CTRL_SYNC_OUT_TRIG, 0x0000050Au, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* CMD register ... */

/* CMD bitfield type ... */

typedef struct {
  __REG16 list_skip :  1;       /* [0] */
  __REG16 seq_skip  :  1;       /* [1] */
  __REG16           : 14;
} saradc_ctrl_cmd_bf_t;

/* CMD single bit enum ... */

enum {
  E_SARADC_CTRL_CMD_LIST_SKIP = 0x0001u,
  E_SARADC_CTRL_CMD_SEQ_SKIP  = 0x0002u
};

/* CMD address mapping ... */

__IO_REG16_BIT(SARADC_CTRL_CMD, 0x0000050Cu, __WRITE, saradc_ctrl_cmd_bf_t);

/* ------------------------------------------------------------------------------ */
/* WADR_MIN register ... */

/* WADR_MIN bitfield type ... */

typedef struct {
  __REG16          :  5;
  __REG16 wadr_min : 11;       /* [15:5] */
} saradc_ctrl_wadr_min_bf_t;

/* WADR_MIN address mapping ... */

__IO_REG16_BIT(SARADC_CTRL_WADR_MIN, 0x0000050Eu, __READ_WRITE, saradc_ctrl_wadr_min_bf_t);

/* ------------------------------------------------------------------------------ */
/* WADR_MAX register ... */

/* WADR_MAX bitfield type ... */

typedef struct {
  __REG16          :  5;
  __REG16 wadr_max : 11;       /* [15:5] */
} saradc_ctrl_wadr_max_bf_t;

/* WADR_MAX address mapping ... */

__IO_REG16_BIT(SARADC_CTRL_WADR_MAX, 0x00000510u, __READ_WRITE, saradc_ctrl_wadr_max_bf_t);

/* ------------------------------------------------------------------------------ */
/* SADR_NEW register ... */

/* SADR_NEW bitfield type ... */

typedef struct {
  __REG16 list_skip :  1;       /* [0] */
  __REG16 start_adr : 15;       /* [15:1] */
} saradc_ctrl_sadr_new_bf_t;

/* SADR_NEW single bit enum ... */

enum {
  E_SARADC_CTRL_SADR_NEW_LIST_SKIP = 0x0001u
};

/* SADR_NEW address mapping ... */

__IO_REG16_BIT(SARADC_CTRL_SADR_NEW, 0x00000512u, __WRITE, saradc_ctrl_sadr_new_bf_t);

/* ------------------------------------------------------------------------------ */
/* SADR_CURRENT register ... */

__IO_REG16(SARADC_CTRL_SADR_CURRENT, 0x00000514u, __READ);

/* ------------------------------------------------------------------------------ */
/* L0_CURRENT register ... */

__IO_REG16(SARADC_CTRL_L0_CURRENT, 0x00000516u, __READ);

/* ------------------------------------------------------------------------------ */
/* L1_CURRENT register ... */

/* L1_CURRENT bitfield type ... */

typedef struct {
  __REG16 no_sample        :  4;       /* [3:0] */
  __REG16 no_sum           :  5;       /* [8:4] */
  __REG16 ch_no            :  5;       /* [13:9] */
  __REG16 trigger_type_ext :  1;       /* [14] */
  __REG16 trigger_type     :  1;       /* [15] */
} saradc_ctrl_l1_current_bf_t;

/* L1_CURRENT single bit enum ... */

enum {
  E_SARADC_CTRL_L1_CURRENT_TRIGGER_TYPE_EXT = 0x4000u,
  E_SARADC_CTRL_L1_CURRENT_TRIGGER_TYPE     = 0x8000u
};

/* L1_CURRENT address mapping ... */

__IO_REG16_BIT(SARADC_CTRL_L1_CURRENT, 0x00000518u, __READ, saradc_ctrl_l1_current_bf_t);

/* ------------------------------------------------------------------------------ */
/* L2_CURRENT register ... */

__IO_REG16(SARADC_CTRL_L2_CURRENT, 0x0000051Au, __READ);

/* ------------------------------------------------------------------------------ */
/* ADR_NEXT register ... */

__IO_REG16(SARADC_CTRL_ADR_NEXT, 0x0000051Cu, __READ);

/* ------------------------------------------------------------------------------ */
/* SADR_DONE register ... */

__IO_REG16(SARADC_CTRL_SADR_DONE, 0x0000051Eu, __READ);

/* ------------------------------------------------------------------------------ */
/* STATE register ... */

__IO_REG16(SARADC_CTRL_STATE, 0x00000520u, __READ);

/* ------------------------------------------------------------------------------ */
/* TADR register ... */

__IO_REG16(SARADC_CTRL_TADR, 0x00000526u, __READ);

/* ------------------------------------------------------------------------------ */
/* AUTOCHANNEL register ... */

/* AUTOCHANNEL bitfield type ... */

typedef struct {
  __REG16 auto_ch_u :  5;       /* [4:0] */
  __REG16 auto_ch_v :  5;       /* [9:5] */
  __REG16 auto_ch_w :  5;       /* [14:10] */
  __REG16           :  1;
} saradc_ctrl_autochannel_bf_t;

/* AUTOCHANNEL address mapping ... */

__IO_REG16_BIT(SARADC_CTRL_AUTOCHANNEL, 0x00000528u, __READ_WRITE, saradc_ctrl_autochannel_bf_t);

/* ------------------------------------------------------------------------------ */
/* IRQ_STATUS register ... */

/* IRQ_STATUS bitfield type ... */

typedef struct {
  __REG16 dma_read_err     :  1;       /* [0] */
  __REG16 dma_write_err    :  1;       /* [1] */
  __REG16 sum_written_evt  :  1;       /* [2] */
  __REG16 list_done_evt    :  1;       /* [3] */
  __REG16 sadr_done_nempty :  1;       /* [4] */
  __REG16 sadr_new_nfull   :  1;       /* [5] */
  __REG16 oor0             :  1;       /* [6] */
  __REG16 oor1             :  1;       /* [7] */
  __REG16 oor2             :  1;       /* [8] */
  __REG16 oor3             :  1;       /* [9] */
  __REG16 oor4             :  1;       /* [10] */
  __REG16 oor5             :  1;       /* [11] */
  __REG16 oor6             :  1;       /* [12] */
  __REG16 oor7             :  1;       /* [13] */
  __REG16 oor8             :  1;       /* [14] */
  __REG16 oor9             :  1;       /* [15] */
} saradc_ctrl_irq_status_bf_t;

/* IRQ_STATUS single bit enum ... */

enum {
  E_SARADC_CTRL_IRQ_STATUS_DMA_READ_ERR     = 0x0001u,
  E_SARADC_CTRL_IRQ_STATUS_DMA_WRITE_ERR    = 0x0002u,
  E_SARADC_CTRL_IRQ_STATUS_SUM_WRITTEN_EVT  = 0x0004u,
  E_SARADC_CTRL_IRQ_STATUS_LIST_DONE_EVT    = 0x0008u,
  E_SARADC_CTRL_IRQ_STATUS_SADR_DONE_NEMPTY = 0x0010u,
  E_SARADC_CTRL_IRQ_STATUS_SADR_NEW_NFULL   = 0x0020u,
  E_SARADC_CTRL_IRQ_STATUS_OOR0             = 0x0040u,
  E_SARADC_CTRL_IRQ_STATUS_OOR1             = 0x0080u,
  E_SARADC_CTRL_IRQ_STATUS_OOR2             = 0x0100u,
  E_SARADC_CTRL_IRQ_STATUS_OOR3             = 0x0200u,
  E_SARADC_CTRL_IRQ_STATUS_OOR4             = 0x0400u,
  E_SARADC_CTRL_IRQ_STATUS_OOR5             = 0x0800u,
  E_SARADC_CTRL_IRQ_STATUS_OOR6             = 0x1000u,
  E_SARADC_CTRL_IRQ_STATUS_OOR7             = 0x2000u,
  E_SARADC_CTRL_IRQ_STATUS_OOR8             = 0x4000u,
  E_SARADC_CTRL_IRQ_STATUS_OOR9             = 0x8000u
};

/* IRQ_STATUS address mapping ... */

__IO_REG16_BIT(SARADC_CTRL_IRQ_STATUS, 0x00000570u, __READ_WRITE, saradc_ctrl_irq_status_bf_t);

/* ------------------------------------------------------------------------------ */
/* IRQ_MASK register ... */

__IO_REG16(SARADC_CTRL_IRQ_MASK, 0x00000574u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VENABLE register ... */

__IO_REG16(SARADC_CTRL_IRQ_VENABLE, 0x00000578u, __WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VDISABLE register ... */

__IO_REG16(SARADC_CTRL_IRQ_VDISABLE, 0x0000057Au, __WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VMAX register ... */

__IO_REG16(SARADC_CTRL_IRQ_VMAX, 0x0000057Cu, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VNO register ... */

__IO_REG16(SARADC_CTRL_IRQ_VNO, 0x0000057Eu, __READ_WRITE);

/* ============================================================================== */
/* PRE_PWM module register definitions */
/* ============================================================================== */

/* ------------------------------------------------------------------------------ */
/* CFG register ... */

/* CFG bitfield type ... */

typedef struct {
  __REG16 inc_en       :  1;       /* [0] */
  __REG16 dma_en       :  1;       /* [1] */
  __REG16 on_state     :  2;       /* [3:2] */
  __REG16 swap_uv      :  1;       /* [4] */
  __REG16 switch_gd_td :  1;       /* [5] */
  __REG16              : 10;
} pre_pwm_cfg_bf_t;

/* CFG single bit enum ... */

enum {
  E_PRE_PWM_CFG_INC_EN       = 0x0001u,
  E_PRE_PWM_CFG_DMA_EN       = 0x0002u,
  E_PRE_PWM_CFG_SWAP_UV      = 0x0010u,
  E_PRE_PWM_CFG_SWITCH_GD_TD = 0x0020u
};

/* CFG address mapping ... */

__IO_REG16_BIT(PRE_PWM_CFG, 0x00000580u, __READ_WRITE, pre_pwm_cfg_bf_t);

/* ------------------------------------------------------------------------------ */
/* BP_MAX register ... */

__IO_REG16(PRE_PWM_BP_MAX, 0x00000582u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* ADDR_BASE register ... */

__IO_REG16(PRE_PWM_ADDR_BASE, 0x00000584u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* CMD register ... */

/* CMD bitfield type ... */

typedef struct {
  __REG16 bps0      :  1;       /* [0] */
  __REG16 u_reload  :  1;       /* [1] */
  __REG16 v_reload  :  1;       /* [2] */
  __REG16 w_reload  :  1;       /* [3] */
  __REG16 u_dma     :  1;       /* [4] */
  __REG16 v_dma     :  1;       /* [5] */
  __REG16 w_dma     :  1;       /* [6] */
  __REG16 uvw_scale :  1;       /* [7] */
  __REG16           :  8;
} pre_pwm_cmd_bf_t;

/* CMD single bit enum ... */

enum {
  E_PRE_PWM_CMD_BPS0      = 0x0001u,
  E_PRE_PWM_CMD_U_RELOAD  = 0x0002u,
  E_PRE_PWM_CMD_V_RELOAD  = 0x0004u,
  E_PRE_PWM_CMD_W_RELOAD  = 0x0008u,
  E_PRE_PWM_CMD_U_DMA     = 0x0010u,
  E_PRE_PWM_CMD_V_DMA     = 0x0020u,
  E_PRE_PWM_CMD_W_DMA     = 0x0040u,
  E_PRE_PWM_CMD_UVW_SCALE = 0x0080u
};

/* CMD address mapping ... */

__IO_REG16_BIT(PRE_PWM_CMD, 0x00000586u, __WRITE, pre_pwm_cmd_bf_t);

/* ------------------------------------------------------------------------------ */
/* INC register ... */

__IO_REG16(PRE_PWM_INC, 0x00000588u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* ACC register ... */

__IO_REG16(PRE_PWM_ACC, 0x0000058Au, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* BASE_PHASE register ... */

__IO_REG16(PRE_PWM_BASE_PHASE, 0x0000058Cu, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* SCALE register ... */

/* SCALE bitfield type ... */

typedef struct {
  __REG16 scale       : 12;       /* [11:0] */
  __REG16 scale_shift :  3;       /* [14:12] */
  __REG16             :  1;
} pre_pwm_scale_bf_t;

/* SCALE address mapping ... */

__IO_REG16_BIT(PRE_PWM_SCALE, 0x0000058Eu, __READ_WRITE, pre_pwm_scale_bf_t);

/* ------------------------------------------------------------------------------ */
/* SCALE_OFFSET registers ... */

__IO_REG16(PRE_PWM_SCALE_OFFSET0, 0x00000590u, __READ_WRITE);
__IO_REG16(PRE_PWM_SCALE_OFFSET1, 0x0000059Eu, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* LIMIT_LOW register ... */

__IO_REG16(PRE_PWM_LIMIT_LOW, 0x000005A0u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* LIMIT_LOW_SET register ... */

__IO_REG16(PRE_PWM_LIMIT_LOW_SET, 0x000005A2u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* LIMIT_HIGH register ... */

__IO_REG16(PRE_PWM_LIMIT_HIGH, 0x000005A4u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* LIMIT_HIGH_SET register ... */

__IO_REG16(PRE_PWM_LIMIT_HIGH_SET, 0x000005A6u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* BP_SYNC registers ... */

__IO_REG16(PRE_PWM_BP_SYNC0, 0x00000592u, __READ_WRITE);
__IO_REG16(PRE_PWM_BP_SYNC1, 0x00000594u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* SYNC_EN register ... */

/* SYNC_EN bitfield type ... */

typedef struct {
  __REG16 bps0 :  2;       /* [1:0] */
  __REG16 bps1 :  2;       /* [3:2] */
  __REG16      : 12;
} pre_pwm_sync_en_bf_t;

/* SYNC_EN address mapping ... */

__IO_REG16_BIT(PRE_PWM_SYNC_EN, 0x00000596u, __READ_WRITE, pre_pwm_sync_en_bf_t);

/* ------------------------------------------------------------------------------ */
/* SYNC_EXT_CFG register ... */

/* SYNC_EXT_CFG bitfield type ... */

typedef struct {
  __REG16 src      :  2;       /* [1:0] */
  __REG16 intf     :  8;       /* [9:2] */
  __REG16 intf_set :  2;       /* [11:10] */
  __REG16 edge     :  1;       /* [12] */
  __REG16          :  3;
} pre_pwm_sync_ext_cfg_bf_t;

/* SYNC_EXT_CFG single bit enum ... */

enum {
  E_PRE_PWM_SYNC_EXT_CFG_EDGE     = 0x1000u
};

/* SYNC_EXT_CFG address mapping ... */

__IO_REG16_BIT(PRE_PWM_SYNC_EXT_CFG, 0x00000598u, __READ_WRITE, pre_pwm_sync_ext_cfg_bf_t);

/* ------------------------------------------------------------------------------ */
/* SCALE_SYNC register ... */

/* SCALE_SYNC bitfield type ... */

typedef struct {
  __REG16 scale       : 12;       /* [11:0] */
  __REG16 scale_shift :  3;       /* [14:12] */
  __REG16             :  1;
} pre_pwm_scale_sync_bf_t;

/* SCALE_SYNC address mapping ... */

__IO_REG16_BIT(PRE_PWM_SCALE_SYNC, 0x0000059Au, __READ_WRITE, pre_pwm_scale_sync_bf_t);

/* ------------------------------------------------------------------------------ */
/* INC_SYNC register ... */

__IO_REG16(PRE_PWM_INC_SYNC, 0x0000059Cu, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* ADDR registers ... */

__IO_REG16(PRE_PWM_U_ADDR, 0x000005B0u, __READ_WRITE);
__IO_REG16(PRE_PWM_V_ADDR, 0x000005C0u, __READ_WRITE);
__IO_REG16(PRE_PWM_W_ADDR, 0x000005D0u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* LW registers ... */

/* LW bitfield type ... */

typedef struct {
  __REG16 duration :  9;       /* [8:0] */
  __REG16 slope    :  7;       /* [15:9] */
} pre_pwm_lw_bf_t;

/* LW address mapping ... */

__IO_REG16_BIT(PRE_PWM_U_LW, 0x000005B2u, __READ_WRITE, pre_pwm_lw_bf_t);

__IO_REG16_BIT(PRE_PWM_V_LW, 0x000005C2u, __READ_WRITE, pre_pwm_lw_bf_t);

__IO_REG16_BIT(PRE_PWM_W_LW, 0x000005D2u, __READ_WRITE, pre_pwm_lw_bf_t);

/* ------------------------------------------------------------------------------ */
/* HW registers ... */

/* HW bitfield type ... */

typedef struct {
  __REG16 slope_acc  : 15;       /* [14:0] */
  __REG16 use_cfg_on :  1;       /* [15] */
} pre_pwm_hw_bf_t;

/* HW single bit enum ... */

enum {
  E_PRE_PWM_HW_USE_CFG_ON = 0x8000u
};

/* HW address mapping ... */

__IO_REG16_BIT(PRE_PWM_U_HW, 0x000005B4u, __READ_WRITE, pre_pwm_hw_bf_t);

__IO_REG16_BIT(PRE_PWM_V_HW, 0x000005C4u, __READ_WRITE, pre_pwm_hw_bf_t);

__IO_REG16_BIT(PRE_PWM_W_HW, 0x000005D4u, __READ_WRITE, pre_pwm_hw_bf_t);

/* ------------------------------------------------------------------------------ */
/* LW_RLD registers ... */

/* LW_RLD bitfield type ... */

typedef struct {
  __REG16 duration :  9;       /* [8:0] */
  __REG16 slope    :  7;       /* [15:9] */
} pre_pwm_lw_rld_bf_t;

/* LW_RLD address mapping ... */

__IO_REG16_BIT(PRE_PWM_U_LW_RLD, 0x000005B6u, __READ_WRITE, pre_pwm_lw_rld_bf_t);

__IO_REG16_BIT(PRE_PWM_V_LW_RLD, 0x000005C6u, __READ_WRITE, pre_pwm_lw_rld_bf_t);

__IO_REG16_BIT(PRE_PWM_W_LW_RLD, 0x000005D6u, __READ_WRITE, pre_pwm_lw_rld_bf_t);

/* ------------------------------------------------------------------------------ */
/* HW_RLD registers ... */

/* HW_RLD bitfield type ... */

typedef struct {
  __REG16 slope_acc  : 15;       /* [14:0] */
  __REG16 use_cfg_on :  1;       /* [15] */
} pre_pwm_hw_rld_bf_t;

/* HW_RLD single bit enum ... */

enum {
  E_PRE_PWM_HW_RLD_USE_CFG_ON = 0x8000u
};

/* HW_RLD address mapping ... */

__IO_REG16_BIT(PRE_PWM_U_HW_RLD, 0x000005B8u, __READ_WRITE, pre_pwm_hw_rld_bf_t);

__IO_REG16_BIT(PRE_PWM_V_HW_RLD, 0x000005C8u, __READ_WRITE, pre_pwm_hw_rld_bf_t);

__IO_REG16_BIT(PRE_PWM_W_HW_RLD, 0x000005D8u, __READ_WRITE, pre_pwm_hw_rld_bf_t);

/* ------------------------------------------------------------------------------ */
/* IRQ_STATUS register ... */

/* IRQ_STATUS bitfield type ... */

typedef struct {
  __REG16 bps0_evt         :  1;       /* [0] */
  __REG16 bps1_evt         :  1;       /* [1] */
  __REG16 u_reload_evt     :  1;       /* [2] */
  __REG16 v_reload_evt     :  1;       /* [3] */
  __REG16 w_reload_evt     :  1;       /* [4] */
  __REG16 reload_error_evt :  1;       /* [5] */
  __REG16 u_dma_rdy        :  1;       /* [6] */
  __REG16 v_dma_rdy        :  1;       /* [7] */
  __REG16 w_dma_rdy        :  1;       /* [8] */
  __REG16                  :  7;
} pre_pwm_irq_status_bf_t;

/* IRQ_STATUS single bit enum ... */

enum {
  E_PRE_PWM_IRQ_STATUS_BPS0_EVT         = 0x0001u,
  E_PRE_PWM_IRQ_STATUS_BPS1_EVT         = 0x0002u,
  E_PRE_PWM_IRQ_STATUS_U_RELOAD_EVT     = 0x0004u,
  E_PRE_PWM_IRQ_STATUS_V_RELOAD_EVT     = 0x0008u,
  E_PRE_PWM_IRQ_STATUS_W_RELOAD_EVT     = 0x0010u,
  E_PRE_PWM_IRQ_STATUS_RELOAD_ERROR_EVT = 0x0020u,
  E_PRE_PWM_IRQ_STATUS_U_DMA_RDY        = 0x0040u,
  E_PRE_PWM_IRQ_STATUS_V_DMA_RDY        = 0x0080u,
  E_PRE_PWM_IRQ_STATUS_W_DMA_RDY        = 0x0100u
};

/* IRQ_STATUS address mapping ... */

__IO_REG16_BIT(PRE_PWM_IRQ_STATUS, 0x000005F0u, __READ_WRITE, pre_pwm_irq_status_bf_t);

/* ------------------------------------------------------------------------------ */
/* IRQ_MASK register ... */

__IO_REG16(PRE_PWM_IRQ_MASK, 0x000005F4u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VENABLE register ... */

__IO_REG16(PRE_PWM_IRQ_VENABLE, 0x000005F8u, __WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VDISABLE register ... */

__IO_REG16(PRE_PWM_IRQ_VDISABLE, 0x000005FAu, __WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VMAX register ... */

__IO_REG16(PRE_PWM_IRQ_VMAX, 0x000005FCu, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VNO register ... */

__IO_REG16(PRE_PWM_IRQ_VNO, 0x000005FEu, __READ_WRITE);

/* ============================================================================== */
/* PWMN module register definitions */
/* ============================================================================== */

/* ------------------------------------------------------------------------------ */
/* CFG register ... */

/* CFG bitfield type ... */

typedef struct {
  __REG16 cnt_mode        :  2;       /* [1:0] */
  __REG16 middle_rl       :  1;       /* [2] */
  __REG16 dead_time_mode  :  1;       /* [3] */
  __REG16 oc_asyn_en      :  1;       /* [4] */
  __REG16 oc_syn_en       :  1;       /* [5] */
  __REG16 oc_intf         :  7;       /* [12:6] */
  __REG16 restart_en      :  1;       /* [13] */
  __REG16 restart_cap_en  :  1;       /* [14] */
  __REG16 dt_evt_continue :  1;       /* [15] */
} pwmn_cfg_bf_t;

/* CFG single bit enum ... */

enum {
  E_PWMN_CFG_MIDDLE_RL       = 0x0004u,
  E_PWMN_CFG_DEAD_TIME_MODE  = 0x0008u,
  E_PWMN_CFG_OC_ASYN_EN      = 0x0010u,
  E_PWMN_CFG_OC_SYN_EN       = 0x0020u,
  E_PWMN_CFG_RESTART_EN      = 0x2000u,
  E_PWMN_CFG_RESTART_CAP_EN  = 0x4000u,
  E_PWMN_CFG_DT_EVT_CONTINUE = 0x8000u
};

/* CFG address mapping ... */

__IO_REG16_BIT(PWMN_CFG, 0x00000600u, __READ_WRITE, pwmn_cfg_bf_t);

/* ------------------------------------------------------------------------------ */
/* CMD register ... */

/* CMD bitfield type ... */

typedef struct {
  __REG16 run             :  1;       /* [0] */
  __REG16 oc              :  1;       /* [1] */
  __REG16 restart         :  1;       /* [2] */
  __REG16 intf_sync_clear :  1;       /* [3] */
  __REG16                 : 12;
} pwmn_cmd_bf_t;

/* CMD single bit enum ... */

enum {
  E_PWMN_CMD_RUN             = 0x0001u,
  E_PWMN_CMD_OC              = 0x0002u,
  E_PWMN_CMD_RESTART         = 0x0004u,
  E_PWMN_CMD_INTF_SYNC_CLEAR = 0x0008u
};

/* CMD address mapping ... */

__IO_REG16_BIT(PWMN_CMD, 0x00000602u, __WRITE, pwmn_cmd_bf_t);

/* ------------------------------------------------------------------------------ */
/* STATE register ... */

/* STATE bitfield type ... */

typedef struct {
  __REG16 run :  1;       /* [0] */
  __REG16 oc  :  1;       /* [1] */
  __REG16     : 14;
} pwmn_state_bf_t;

/* STATE single bit enum ... */

enum {
  E_PWMN_STATE_RUN = 0x0001u,
  E_PWMN_STATE_OC  = 0x0002u
};

/* STATE address mapping ... */

__IO_REG16_BIT(PWMN_STATE, 0x00000604u, __READ, pwmn_state_bf_t);

/* ------------------------------------------------------------------------------ */
/* CNT_RESTART register ... */

__IO_REG16(PWMN_CNT_RESTART, 0x00000616u, __READ);

/* ------------------------------------------------------------------------------ */
/* CNT register ... */

__IO_REG16(PWMN_CNT, 0x00000606u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* PWM_PHASE register ... */

__IO_REG16(PWMN_PWM_PHASE, 0x00000608u, __READ);

/* ------------------------------------------------------------------------------ */
/* PRESCALER register ... */

__IO_REG16(PWMN_PRESCALER, 0x0000060Au, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* CNT_MAX register ... */

__IO_REG16(PWMN_CNT_MAX, 0x0000060Cu, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* NTH_START register ... */

__IO_REG16(PWMN_NTH_START, 0x00000618u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* DEAD_TIME register ... */

__IO_REG16(PWMN_DEAD_TIME, 0x0000060Eu, __WRITE);

/* ------------------------------------------------------------------------------ */
/* PRESCALER_RELOAD register ... */

__IO_REG16(PWMN_PRESCALER_RELOAD, 0x00000610u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* CNT_MAX_RELOAD register ... */

__IO_REG16(PWMN_CNT_MAX_RELOAD, 0x00000612u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* NTH_START_RELOAD register ... */

__IO_REG16(PWMN_NTH_START_RELOAD, 0x0000061Au, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* DEAD_TIME_RELOAD register ... */

__IO_REG16(PWMN_DEAD_TIME_RELOAD, 0x00000614u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* DEAD_TIME_RELOAD_CH register ... */

/* DEAD_TIME_RELOAD_CH bitfield type ... */

typedef struct {
  __REG16 reload    :  8;       /* [7:0] */
  __REG16 up_ch0_hl :  1;       /* [8] */
  __REG16 up_ch0_lh :  1;       /* [9] */
  __REG16 up_ch1_hl :  1;       /* [10] */
  __REG16 up_ch1_lh :  1;       /* [11] */
  __REG16 up_ch2_hl :  1;       /* [12] */
  __REG16 up_ch2_lh :  1;       /* [13] */
  __REG16 up_ch3_hl :  1;       /* [14] */
  __REG16 up_ch3_lh :  1;       /* [15] */
} pwmn_dead_time_reload_ch_bf_t;

/* DEAD_TIME_RELOAD_CH single bit enum ... */

enum {
  E_PWMN_DEAD_TIME_RELOAD_CH_UP_CH0_HL = 0x0100u,
  E_PWMN_DEAD_TIME_RELOAD_CH_UP_CH0_LH = 0x0200u,
  E_PWMN_DEAD_TIME_RELOAD_CH_UP_CH1_HL = 0x0400u,
  E_PWMN_DEAD_TIME_RELOAD_CH_UP_CH1_LH = 0x0800u,
  E_PWMN_DEAD_TIME_RELOAD_CH_UP_CH2_HL = 0x1000u,
  E_PWMN_DEAD_TIME_RELOAD_CH_UP_CH2_LH = 0x2000u,
  E_PWMN_DEAD_TIME_RELOAD_CH_UP_CH3_HL = 0x4000u,
  E_PWMN_DEAD_TIME_RELOAD_CH_UP_CH3_LH = 0x8000u
};

/* DEAD_TIME_RELOAD_CH address mapping ... */

__IO_REG16_BIT(PWMN_DEAD_TIME_RELOAD_CH, 0x0000061Eu, __READ_WRITE, pwmn_dead_time_reload_ch_bf_t);

/* ------------------------------------------------------------------------------ */
/* PWM_CFG registers ... */

/* PWM_CFG bitfield type ... */

typedef struct {
  __REG16 cmp_mode      :  2;       /* [1:0] */
  __REG16 c_src         :  2;       /* [3:2] */
  __REG16 on_src        :  1;       /* [4] */
  __REG16 ign_start_evt :  1;       /* [5] */
  __REG16               : 10;
} pwmn_pwm_cfg_bf_t;

/* PWM_CFG single bit enum ... */

enum {
  E_PWMN_PWM_CFG_ON_SRC        = 0x0010u,
  E_PWMN_PWM_CFG_IGN_START_EVT = 0x0020u
};

/* PWM_CFG address mapping ... */

__IO_REG16_BIT(PWMN_PWM0_CFG, 0x00000620u, __READ_WRITE, pwmn_pwm_cfg_bf_t);

__IO_REG16_BIT(PWMN_PWM1_CFG, 0x00000630u, __READ_WRITE, pwmn_pwm_cfg_bf_t);

__IO_REG16_BIT(PWMN_PWM2_CFG, 0x00000640u, __READ_WRITE, pwmn_pwm_cfg_bf_t);

__IO_REG16_BIT(PWMN_PWM3_CFG, 0x00000650u, __READ_WRITE, pwmn_pwm_cfg_bf_t);

/* ------------------------------------------------------------------------------ */
/* PWM_C registers ... */

__IO_REG16(PWMN_PWM0_C0, 0x00000622u, __READ_WRITE);
__IO_REG16(PWMN_PWM0_C1, 0x00000624u, __READ_WRITE);
__IO_REG16(PWMN_PWM1_C0, 0x00000632u, __READ_WRITE);
__IO_REG16(PWMN_PWM1_C1, 0x00000634u, __READ_WRITE);
__IO_REG16(PWMN_PWM2_C0, 0x00000642u, __READ_WRITE);
__IO_REG16(PWMN_PWM2_C1, 0x00000644u, __READ_WRITE);
__IO_REG16(PWMN_PWM3_C0, 0x00000652u, __READ_WRITE);
__IO_REG16(PWMN_PWM3_C1, 0x00000654u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* PWM_ON registers ... */

/* PWM_ON bitfield type ... */

typedef struct {
  __REG16 ls :  1;       /* [0] */
  __REG16 hs :  1;       /* [1] */
  __REG16    : 14;
} pwmn_pwm_on_bf_t;

/* PWM_ON single bit enum ... */

enum {
  E_PWMN_PWM_ON_LS = 0x0001u,
  E_PWMN_PWM_ON_HS = 0x0002u
};

/* PWM_ON address mapping ... */

__IO_REG16_BIT(PWMN_PWM0_ON, 0x00000626u, __READ_WRITE, pwmn_pwm_on_bf_t);

__IO_REG16_BIT(PWMN_PWM1_ON, 0x00000636u, __READ_WRITE, pwmn_pwm_on_bf_t);

__IO_REG16_BIT(PWMN_PWM2_ON, 0x00000646u, __READ_WRITE, pwmn_pwm_on_bf_t);

__IO_REG16_BIT(PWMN_PWM3_ON, 0x00000656u, __READ_WRITE, pwmn_pwm_on_bf_t);

/* ------------------------------------------------------------------------------ */
/* PWM_C_RELOAD registers ... */

__IO_REG16(PWMN_PWM0_C0_RELOAD, 0x00000628u, __READ_WRITE);
__IO_REG16(PWMN_PWM0_C1_RELOAD, 0x0000062Au, __READ_WRITE);
__IO_REG16(PWMN_PWM1_C0_RELOAD, 0x00000638u, __READ_WRITE);
__IO_REG16(PWMN_PWM1_C1_RELOAD, 0x0000063Au, __READ_WRITE);
__IO_REG16(PWMN_PWM2_C0_RELOAD, 0x00000648u, __READ_WRITE);
__IO_REG16(PWMN_PWM2_C1_RELOAD, 0x0000064Au, __READ_WRITE);
__IO_REG16(PWMN_PWM3_C0_RELOAD, 0x00000658u, __READ_WRITE);
__IO_REG16(PWMN_PWM3_C1_RELOAD, 0x0000065Au, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* PWM_ON_RELOAD registers ... */

/* PWM_ON_RELOAD bitfield type ... */

typedef struct {
  __REG16 ls :  1;       /* [0] */
  __REG16 hs :  1;       /* [1] */
  __REG16    : 14;
} pwmn_pwm_on_reload_bf_t;

/* PWM_ON_RELOAD single bit enum ... */

enum {
  E_PWMN_PWM_ON_RELOAD_LS = 0x0001u,
  E_PWMN_PWM_ON_RELOAD_HS = 0x0002u
};

/* PWM_ON_RELOAD address mapping ... */

__IO_REG16_BIT(PWMN_PWM0_ON_RELOAD, 0x0000062Cu, __READ_WRITE, pwmn_pwm_on_reload_bf_t);

__IO_REG16_BIT(PWMN_PWM1_ON_RELOAD, 0x0000063Cu, __READ_WRITE, pwmn_pwm_on_reload_bf_t);

__IO_REG16_BIT(PWMN_PWM2_ON_RELOAD, 0x0000064Cu, __READ_WRITE, pwmn_pwm_on_reload_bf_t);

__IO_REG16_BIT(PWMN_PWM3_ON_RELOAD, 0x0000065Cu, __READ_WRITE, pwmn_pwm_on_reload_bf_t);

/* ------------------------------------------------------------------------------ */
/* PWM_DEAD_TIME registers ... */

/* PWM_DEAD_TIME bitfield type ... */

typedef struct {
  __REG16 high_to_low :  8;       /* [7:0] */
  __REG16 low_to_high :  8;       /* [15:8] */
} pwmn_pwm_dead_time_bf_t;

/* PWM_DEAD_TIME address mapping ... */

__IO_REG16_BIT(PWMN_PWM0_DEAD_TIME, 0x0000062Eu, __READ_WRITE, pwmn_pwm_dead_time_bf_t);

__IO_REG16_BIT(PWMN_PWM1_DEAD_TIME, 0x0000063Eu, __READ_WRITE, pwmn_pwm_dead_time_bf_t);

__IO_REG16_BIT(PWMN_PWM2_DEAD_TIME, 0x0000064Eu, __READ_WRITE, pwmn_pwm_dead_time_bf_t);

__IO_REG16_BIT(PWMN_PWM3_DEAD_TIME, 0x0000065Eu, __READ_WRITE, pwmn_pwm_dead_time_bf_t);

/* ------------------------------------------------------------------------------ */
/* IRQ_STATUS register ... */

/* IRQ_STATUS bitfield type ... */

typedef struct {
  __REG16 oc              :  1;       /* [0] */
  __REG16 start_evt       :  1;       /* [1] */
  __REG16 middle_evt      :  1;       /* [2] */
  __REG16 dead_time_evt_0 :  1;       /* [3] */
  __REG16 dead_time_evt_1 :  1;       /* [4] */
  __REG16 dead_time_evt_2 :  1;       /* [5] */
  __REG16 dead_time_evt_3 :  1;       /* [6] */
  __REG16 dead_time_evt_4 :  1;       /* [7] */
  __REG16 dead_time_evt_5 :  1;       /* [8] */
  __REG16 dead_time_evt_6 :  1;       /* [9] */
  __REG16 dead_time_evt_7 :  1;       /* [10] */
  __REG16                 :  5;
} pwmn_irq_status_bf_t;

/* IRQ_STATUS single bit enum ... */

enum {
  E_PWMN_IRQ_STATUS_OC              = 0x0001u,
  E_PWMN_IRQ_STATUS_START_EVT       = 0x0002u,
  E_PWMN_IRQ_STATUS_MIDDLE_EVT      = 0x0004u,
  E_PWMN_IRQ_STATUS_DEAD_TIME_EVT_0 = 0x0008u,
  E_PWMN_IRQ_STATUS_DEAD_TIME_EVT_1 = 0x0010u,
  E_PWMN_IRQ_STATUS_DEAD_TIME_EVT_2 = 0x0020u,
  E_PWMN_IRQ_STATUS_DEAD_TIME_EVT_3 = 0x0040u,
  E_PWMN_IRQ_STATUS_DEAD_TIME_EVT_4 = 0x0080u,
  E_PWMN_IRQ_STATUS_DEAD_TIME_EVT_5 = 0x0100u,
  E_PWMN_IRQ_STATUS_DEAD_TIME_EVT_6 = 0x0200u,
  E_PWMN_IRQ_STATUS_DEAD_TIME_EVT_7 = 0x0400u
};

/* IRQ_STATUS address mapping ... */

__IO_REG16_BIT(PWMN_IRQ_STATUS, 0x00000670u, __READ_WRITE, pwmn_irq_status_bf_t);

/* ------------------------------------------------------------------------------ */
/* IRQ_MASK register ... */

__IO_REG16(PWMN_IRQ_MASK, 0x00000674u, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VENABLE register ... */

__IO_REG16(PWMN_IRQ_VENABLE, 0x00000678u, __WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VDISABLE register ... */

__IO_REG16(PWMN_IRQ_VDISABLE, 0x0000067Au, __WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VMAX register ... */

__IO_REG16(PWMN_IRQ_VMAX, 0x0000067Cu, __READ_WRITE);

/* ------------------------------------------------------------------------------ */
/* IRQ_VNO register ... */

__IO_REG16(PWMN_IRQ_VNO, 0x0000067Eu, __READ_WRITE);

/* ============================================================================== */

#ifdef __IAR_SYSTEMS_ICC__
  #pragma language=restore
#endif

#endif  /* __IO_E52398A__ */

