/**
 * @ingroup MC
 *
 * @{
 */

#ifndef ROTOR_POS_DETECT_H
#define ROTOR_POS_DETECT_H

#include <defines.h>

typedef enum
{
  POS_DETECT_STATE_INVALID            = 0u,
  POS_DETECT_STATE_DISABLED           = 1u,
  POS_DETECT_STATE_CHARGE_BOOTSTRAPS  = 2u,
  POS_DETECT_STATE_CHARGE_REV_POL_FET = 3u,
  POS_DETECT_STATE_PULSE_UV           = 4u,
  POS_DETECT_STATE_PULSE_VU           = 5u,
  POS_DETECT_STATE_PULSE_UW           = 6u,
  POS_DETECT_STATE_PULSE_WU           = 7u,
  POS_DETECT_STATE_PULSE_VW           = 8u,
  POS_DETECT_STATE_PULSE_WV           = 9u,
  POS_DETECT_STATE_CALC               = 10u,
  POS_DETECT_STATE_FINISHED           = 11u

} pos_detect_state_t;

typedef enum
{
  POS_PWM_INVALID                       = 0u,
  POS_PWM_ALL_PHASES_HIGH_Z             = 1u,
  POS_PWM_ALL_LS_ON                     = 2u,
  POS_PWM_ALL_HS_ON                     = 3u,
  POS_PWM_ALL_PHASES_ON_W_COMPARE_VALUE = 4u,
  POS_PWM_PULSE_UV                      = 5u,
  POS_PWM_PULSE_UW                      = 6u,
  POS_PWM_PULSE_VW                      = 7u

} pos_pwm_period_type_t;

typedef enum
{
  POS_PULSE_POL_POSITIVE = 1u,
  POS_PULSE_POL_NEGATIVE = 2u

} pulse_polarity_t;

typedef struct
{
  pos_detect_state_t state;

  s16 rev_pol_fet_charge_duration;
  s16 freerun_duration;

  s16 pulse_length_div_period;
  s16 pulse_length_mod_period;

  s16 meas_time_div_period;
  s16 meas_time_mod_period;

  s16 pwm_period_cnt;

  s16 adc_sample_extension_restore;

  u16 struct_signature;

} pos_data_t;

/* -------------------
   global declarations
   ------------------- */

void pos_generate_struct_signature ( void );

#if ( USR_EN_INITIAL_POSITION_DETECTION == 1 )
  void pos_init( void );

  BOOL pos_detect_state_machine_exec( const BOOL exec_pos_detect );
  pos_detect_state_t pos_get_current_state ( void );
#endif

#endif /* _#ifndef ROTOR_POS_DETECT_H */

/* }@
 */
