/***************************************************************************//**
 * @file			LinDrvImp_Callbacks.h
 *
 * @creator		sbai
 * @created		16.07.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 ******************************************************************************/

#ifndef LINDRVIMP_CALLBACKS_H_
#define LINDRVIMP_CALLBACKS_H_

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "Lin_Basictypes.h"
#include "LinBus_Interface.h"
#include "LinDataStg_Interface.h"
#include "LinDiag_Interface.h"
#include "LinLookup_Interface.h"
#include "LinProto_Interface.h"
#include "LinTrans_Interface.h"
#include "LinBus_Implementation.h"


/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

/* ****************************************************************************/
/*                               BUS CALLBACKS                                */
/* ****************************************************************************/

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
Lin_uint32_t LinDrvImp_BusGetMillisecondsFun(void);

/* ****************************************************************************/
/*                              PROTO CALLBACKS                               */
/* ****************************************************************************/

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
void LinDrvImp_ProtoErrorCbFun(LinProtoIf_pGenericEnvData_t    genericProtoEnvData, LinProtoIf_cpInterfaceFunctions_t protoIfFuns,
                               LinProtoIf_Error_t              error,               LinBusIf_FrameID_t                frameID,
                               LinProtoIf_cpFrameDescription_t frameDesc,           LinProtoIf_pGenericCbCtxData_t    genericProtoCbCtxData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
void LinDrvImp_ProtoRestartCbFun(LinProtoIf_pGenericEnvData_t   genericProtoEnvData, LinProtoIf_cpInterfaceFunctions_t protoIfFuns,
                                 LinProtoIf_pGenericCbCtxData_t genericProtoCbCtxData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
LinProtoIf_eMeasDoneRetVal_t LinDrvImp_ProtoMeasDoneCbFun(LinProtoIf_pGenericEnvData_t   genericProtoEnvData, LinProtoIf_cpInterfaceFunctions_t protoIfFuns,
                                                          LinProtoIf_Baudrate_t          exp_baudrate,        LinProtoIf_Baudrate_t             prev_baudrate,
                                                          LinProtoIf_Baudrate_t          meas_baudrate,       LinProtoIf_BreakLen_t             meas_break_len,
                                                          LinProtoIf_pGenericCbCtxData_t genericProtoCbCtxData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
LinProtoIf_eIdleAction_t LinDrvImp_ProtoIdleCbFun(LinProtoIf_pGenericEnvData_t   genericProtoEnvData, LinProtoIf_cpInterfaceFunctions_t protoIfFuns,
                                                  LinProtoIf_pGenericCbCtxData_t genericProtoCbCtxData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
void LinDrvImp_ProtoWakupCbFun(LinProtoIf_pGenericEnvData_t   genericProtoEnvData, LinProtoIf_cpInterfaceFunctions_t protoIfFuns,
                               LinProtoIf_pGenericCbCtxData_t genericProtoCbCtxData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
LinProtoIf_eMsgAction_t LinDrvImp_ProtoFrameIdProcCbFun(LinProtoIf_pGenericEnvData_t   genericProtoEnvData, LinProtoIf_cpInterfaceFunctions_t ifFunctions,
                                                        LinBusIf_FrameID_t             frameID,             LinProtoIf_cpFrameDescription_t   frameDesc,
                                                        LinProtoIf_Error_t             error,               LinProtoIf_pGenericCbCtxData_t    genericProtoCbCtxData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
LinProtoIf_eMsgAction_t LinDrvImp_ResponseErrorFrameProcCbFun(LinProtoIf_pGenericEnvData_t   genericProtoEnvData, LinProtoIf_cpInterfaceFunctions_t ifFunctions,
                                                              LinBusIf_FrameID_t             frameID,             LinProtoIf_cpFrameDescription_t   frameDesc,
                                                              LinProtoIf_Error_t             error,               LinProtoIf_pGenericCbCtxData_t    genericProtoCbCtxData);
										   
/* ****************************************************************************/
/*                              TRANS CALLBACKS                               */
/* ****************************************************************************/

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
void LinDrvImp_TransErrorCbFun(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_cpInterfaceFunctions_t ifFunctions,
                               LinTransIf_Error_t           error,               LinTransIf_SID_t                  SID,
                               LinBusIf_pGenericCbCtxData_t genericTransCbCtxData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
void LinDrvImp_TransSIDProcessedCbFun(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_cpInterfaceFunctions_t ifFunctions,
                                      LinTransIf_SID_t             sid,                 LinBusIf_pGenericCbCtxData_t      genericTransCbCtxData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
LinTransIf_eCheckNADResult_t LinDrvImp_TransCheckNADCbFun(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_cpInterfaceFunctions_t ifFunctions,
                                                          LinTransIf_NAD_t             nad,                 LinTransIf_SID_t             sid,
                                                          LinBusIf_pGenericCbCtxData_t genericTransCbCtxData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
void LinDrvImp_TransGoToSleepCbFun(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_cpInterfaceFunctions_t ifFunctions,
                                   LinBusIf_pGenericCbCtxData_t genericTransCbCtxData);


/* ****************************************************************************/
/*                           DATA STORAGE CALLBACKS                           */
/* ****************************************************************************/

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
void LinDrvImp_DataStgErrorCbFun(LinDataStgIf_pGenericEnvData_t genericDataStgEnvData, LinDataStgIf_cpInterfaceFunctions_t protoIfFuns,
                                 LinDataStgIf_Error_t           error,                 LinDataStgIf_pGenericCbCtxData_t    genericDataStgCbCtxData);


/* ****************************************************************************/
/*                               DIAG CALLBACKS                               */
/* ****************************************************************************/

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
void LinDrvImp_DiagErrorCbFun(LinDiagIf_pGenericEnvData_t   genericDiagEnvData, LinDiagIf_cpInterfaceFunctions_t ifFunctions,
                              LinDiagIf_Error_t             error,              LinTransIf_SID_t                 SID,
                              LinDiagIf_pGenericCbCtxData_t genericDiagCbCtxData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre				  (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
void LinDrvImp_DiagGoToSleepCbFun(LinDiagIf_pGenericEnvData_t   genericDiagEnvData, LinDiagIf_cpInterfaceFunctions_t ifFunctions,
                                  LinDiagIf_pGenericCbCtxData_t genericDiagCbCtxData);





#endif /* LINDRVIMP_CALLBACKS_H_ */
