/***************************************************************************//**
 * @file			LinTrans_Implementation.h
 *
 * @creator		sbai
 * @created		13.02.2015
 *
 * @brief  		Implementation of the LIN Driver TRANS layer.
 *
 * $Id$
 *
 * $Revision$
 *
 ******************************************************************************/

#ifndef LINTRANS_IMPLEMENTATION_H_
#define LINTRANS_IMPLEMENTATION_H_

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "LinProto_Interface.h"
#include "LinTrans_Interface.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/
#define LINTRANSIMP_VERSION                       0x0110u  /**< LIN TRANS implementation version */

#define LINTRANSIMP_CONFIG_DATA_VERSION           0x0100u  /* Expected config data version */

#define LINTRANSIMP_NUMBER_OF_FRAME_DESCRIPTIONS  2u  /**< Count of used 'Frame Description Lists' of PROTO LAYER in LIN TRANS Layer. */

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/***************************************************************************//**
 * @brief Struct for TRANS layer implementation specific configuration data.
 ******************************************************************************/
struct LinTransImp_sCfgData
{
  Lin_Version_t       Version;                /* Config data struct version */

  LinProtoIf_sThis_t  LinProtoInsThisPointer; /**<< @copydoc LinProtoIf_sThis_t */
};

typedef struct LinTransImp_sCfgData    LinTransImp_sCfgData_t; /**< Typedef of LinTransImp_sCfgData. */
typedef        LinTransImp_sCfgData_t* LinTransImp_pCfgData_t; /**< Typedef of pointer to LinTransImp_sCfgData. */

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/
#if LINTRANSIMP_EXT_IFFUN_STRCT_ACCESS == 1
extern const LinTransIf_sInterfaceFunctions_t LinTransImp_InterfaceFunctions;
#endif /* LINTRANSIMP_EXT_IFFUN_STRCT_ACCESS == 1 */

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

/***************************************************************************//**
 * @brief Implementation of LIN TRANS layer 'Initialization' function.
 *
 * @copydetails LinTransIf_InitializationIfFun_t
 *
 ******************************************************************************/
Lin_Bool_t LinTransImp_Initialization(LinTransIf_pGenericEnvData_t             genericTransEnvData, LinTransIf_EnvDataSze_t         transEnvDataSze,
																			LinTransIf_cpCallbackFunctions_t         transCbFuns,         LinBusIf_pGenericCbCtxData_t    genericTransCbCtxData,
																			LinTransIf_cpProtoFrameDescriptionInfo_t protoFrmDescInfo,    LinTransIf_NasTimeout_t         nasTimeout,
																			LinTransIf_NcrTimeout_t                  ncrTimeout,          LinTransIf_pGenericImpCfgData_t genericTransImpCfgData);

/***************************************************************************//**
 * @brief Implementation of LIN TRANS layer 'Task' function.
 *
 * @copydetails LinTransIf_TaskIfFun_t
 *
 ******************************************************************************/
void LinTransImp_Task(LinTransIf_pGenericEnvData_t genericProtoEnvData);

/***************************************************************************//**
 * @brief Implementation of LIN TRANS layer 'Get Sub Interface' function.
 *
 * @copydetails LinTransIf_GetSubInterfaceIfFun_t
 *
 ******************************************************************************/
Lin_Bool_t LinTransImp_GetSubInterface(LinTransIf_pGenericEnvData_t genericTransEnvData, Lin_eInterfaceIds_t interfaceId, Lin_pThis_t ifThisPtr);

/***************************************************************************//**
 * @brief Implementation of LIN TRANS layer 'Init Response' function.
 *
 * @copydetails LinTransIf_InitResponseIfFun_t
 *
 ******************************************************************************/
Lin_Bool_t LinTransImp_InitResponse(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_NAD_t       responseNAD,
																		LinTransIf_SID_t             rsid,                LinTransIf_PDUMsgLen_t dataLen);

/***************************************************************************//**
 * @brief Implementation of LIN TRANS layer 'Append Data to Message Buffer' function.
 *
 * @copydetails LinTransIf_AppendDataToMsgBufferIfFun_t
 *
 ******************************************************************************/
Lin_Bool_t LinTransImp_AppendDataToMsgBuffer(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_cpData_t data,
                                             LinTransIf_BufLength_t       dataLen);

/***************************************************************************//**
 * @brief Implementation of LIN TRANS layer 'Add SID Description List' function.
 *
 * @copydetails LinTransIf_AddSidDescLstIfFun_t
 *
 ******************************************************************************/
Lin_Bool_t LinTransImp_AddSIDDescriptionList(LinTransIf_pGenericEnvData_t   genericTransEnvData, LinTransIf_cpSIDDescription_t genericSidDescLst,
																						 LinTransIf_pGenericCbCtxData_t genericTransPerSidDescLstCbCtxData);

/***************************************************************************//**
 * @brief Implementation of LIN TRANS layer 'Remove SID Description List' function.
 *
 * @copydetails LinTransIf_RmvSidDescLstIfFun_t
 *
 ******************************************************************************/
Lin_Bool_t LinTransImp_RemoveSIDDescriptionList(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_cpSIDDescription_t genericSidDescLst);

/***************************************************************************//**
 * @brief Implementation of LIN TRANS layer 'Assign Frame ID' function.
 *
 * @copydetails LinTransIf_AssignFrameIDIfFun_t
 *
 ******************************************************************************/
Lin_Bool_t LinTransImp_AssignFrameID(LinTransIf_pGenericEnvData_t genericTransEnvData, LinProtoIf_MsgID_t msgID, LinBusIf_FrameID_t frameID);

/***************************************************************************//**
 * @brief Implementation of LIN TRANS layer 'Assign Frame ID Range' function.
 *
 * @copydetails LinTransIf_AssignFrameIDRangeIfFun_t
 *
 ******************************************************************************/
Lin_Bool_t LinTransImp_AssignFrameIDRange(LinTransIf_pGenericEnvData_t genericTransEnvData, LinLookupIf_FrameIdx_t frameIdx, LinLookupIf_pAssignFrameIDRangeLst_t pidLst,
                                          Lin_uint8_t                  pidLstLen);

/***************************************************************************//**
 * @brief Implementation of LIN TRANS layer 'Get Status' function.
 *
 * @copydetails LinTransIf_SetTimeoutIfFun_t
 *
 ******************************************************************************/
LinTransIf_eComStatus_t LinTransImp_GetStatus(const LinTransIf_pGenericEnvData_t genericTransEnvData);

/***************************************************************************//**
 * @brief Implementation of LIN TRANS layer 'Set Timeout' function.
 *
 * @copydetails LinTransIf_GetStatusIfFun_t
 *
 ******************************************************************************/
void LinTransImp_SetTimeout(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_eTimeoutType_t timeoutType, LinTransIf_Timeout_t timeout);

#endif /* LINTRANS_IMPLEMENTATION_H_ */

