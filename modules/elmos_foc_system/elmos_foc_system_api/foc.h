/**
 * @defgroup FOC FOC algorithm
 * @ingroup FOC
 *
 * @{
 */

/**********************************************************************/
/*! @file foc.h
 * @brief computation of the FOC algorithm
 *
 * @author       JBER
 */
/**********************************************************************/

#ifndef FOC_H_
#define FOC_H_

#include "adc.h"
#include "debug.h"
#include "global.h"
#include "motor_ctrl.h"
#include "pid_controller.h"
#include <string.h>

typedef enum
{
  FOC_SENSORLESS  = 0,
  FOC_HALL_SENSOR = 1
} foc_sens_meas_mode_t;

typedef struct
{
  int16_t i1;                                     /**< the current that is flowing between v_val_min and v_val_mid (think of it as timer compare values) */
  int16_t i2;                                     /**< the current that is flowing between v_val_mid and v_val_max (think of it as timer compare values)*/
  int16_t v_val_low;                              /**< the value of the voltage with the smallest compare value */
  int16_t v_val_mid;                              /**< the value of the voltage whose corresponding compare value lies between v_val_low and v_val_max */
  int16_t v_val_max;                              /**< the value of the voltage with the greatest compare value */

} foc_ibat_calc_data_t;

typedef struct
{
  int16_t K;
  int16_t K_shift_val;

  int32_t output;

} foc_low_pass_filter_t;

/**
   @struct foc_luenberger_data_t
   @brief parameters and process values of the rotor angle estimator (Luenberger observer)
 */
typedef struct
{
  int16_t motor_model_coeff_a;                    /**< first coefficient for the electrical model of the motor ((1 - T_pwm_period * R_phase / L_phase) << motor_model_coeff_scaler) */
  int16_t motor_model_coeff_b;                    /**< second coeffictient for the electrical model of the motor ((T_pwm_period / L_phase) << motor_model_coeff_scaler) */
  uint16_t motor_model_coeff_scaler;               /**< shift value (to the left) for both, coeff_a and coeff_b, as their value is between 0 and 1 and therefore not suited for integer operation */

  int16_t i_alpha_real;
  int16_t i_beta_real;

  int16_t i_alpha_model;                          /**< value of i_alpha of the electrical motor model [A/256] */
  int16_t i_beta_model;                           /**< value of i_beta of the electrical motor model [A/256] */

  int16_t diff_i_alpha;                           /**< error between measured current and estimated current [A/256] */
  int16_t diff_i_beta;                            /**< error between measured current and estimated current [A/256] */

  int16_t diff_i_alpha_last;
  int16_t diff_i_beta_last;

  int16_t current_comp_Kp;                        /**< amplification factor of the compensator for estimated currents from measured currents */
  int16_t current_comp_Kd;                        /**< amplification factor of the compensator for estimated currents from measured currents */
  uint16_t current_comp_scaler;                    /**< shift value (to the right) of the compensator output for estimated currents from measured currents */

  int16_t corr_alpha;                             /**< output of compensated model for computing the next BEMF value, alpha-component [V/256] */
  int16_t corr_beta;                              /**< output of compensated model for computing the next BEMF value, beta-component [V/256] */

  int16_t e_alpha_model_raw;                      /**< value of e_alpha of the electrical motor model, thus the BEMF value of the model [V/256] */
  int16_t e_beta_model_raw;                       /**< value of e_beta of the electrical motor model, thus the BEMF value of the model [V/256]*/

  int16_t lpf_gain_T;                             /**< time-constant-defining gain of the LPF for e_lpha/e_beta */

  foc_low_pass_filter_t lpf_e_alpha;          /**< low-pass filter for e_alpha; must be configured to produce a phase lag of 45 degrees at all times while producing the same output amplitude as the input one */
  foc_low_pass_filter_t lpf_e_beta;           /**< low-pass filter for e_alpha; must be configured to produce a phase lag of 45 degrees at all times while producing the same output amplitude as the input one */

  int16_t e_alpha_model_filtered;                 /**< e_alpha_model_raw after lowpass filter; used for computing the rotor angle [V/256] */
  int16_t e_beta_model_filtered;                  /**< e_beta_model_raw after lowpass filter; used for computing the rotor angle [V/256]*/

  uint16_t rotor_angle_estimated;

} foc_luenberger_data_t;

/**
   @struct foc_ctrl_loop_data_t
   @brief top-level storage structure of module "FOC algorithm"
 */
typedef struct                                /* PRQA S 3630 */ /* justification: typedef necessary for exporting function foc_get_data() */
{
  int16_t i_u;                                    /**< momentary value of current through motor phase U [ADC bits] */
  int16_t i_v;                                    /**< momentary value of current through motor phase V [ADC bits] */
  int16_t i_w;                                    /**< momentary value of current through motor phase W [ADC bits] */

  int16_t i_alpha;                                /**< momentary value of current in alpha-beta-coordinates, alpha [ADC bits] */
  int16_t i_beta;                                 /**< momentary value of current in alpha-beta-coordinates, beta [ADC bits] */

  int16_t i_d;                                    /**< momentary value of current in d-q-coordinates, d [ADC bits] */
  int16_t i_q;                                    /**< momentary value of current in d-q-coordinates, q [ADC bits] */

  int16_t i_d_ref;                                /**< target value for i_d [A/256] */
  int16_t i_q_ref;                                /**< target value for i_q [A/256] */

  int16_t v_d;                                    /**< momentary value of output voltage vector in d-q-coordinates, d [PWM counter value] */
  int16_t v_q;                                    /**< momentary value of output voltage vector in d-q-coordinates, q [PWM counter value] */

  int16_t v_alpha;                                /**< momentary value of output voltage vector in alpha-beta-coordinates, alpha [PWM counter value] */
  int16_t v_beta;                                 /**< momentary value of output voltage vector in alpha-beta-coordinates, beta [PWM counter value] */

  int16_t v_u;                                    /**< momentary value of output voltage at motor phase U [PWM counter value]*/
  int16_t v_v;                                    /**< momentary value of output voltage at motor phase U [PWM counter value]*/
  int16_t v_w;                                    /**< momentary value of output voltage at motor phase U [PWM counter value]*/

  int16_t svm_length;
  uint16_t svm_phi;

  pi_controller_t controller_i_d;             /**< (adjustable) controller for i_d*/
  pi_controller_t controller_i_q;             /**< (adjustable) controller for i_q*/

  int16_t voltage_conversion_factor;
  int16_t current_conversion_factor;

  uint16_t rotor_angle_startup;                    /**< the rotor angle which is forced on the FOC algorithm during startup (open loop operation) */
  uint16_t rotor_angle_open_to_closed_loop_error;  /**< when switching from open loop to closed loop operation, the error between the impressed angle (open loop) and the real rotor angle is reduced smoothly */
  uint16_t open_to_closed_loop_smoothing_coeff;    /**< the lower this value is, the smoother is the transition from open loop to closed loop; however during this transition phase the efficiency is low and the stall detection is disabled */

  uint16_t current_rotor_angle;                    /**< the angle that is actually used for the FOC calculation (which can origin from different sources.. impressed angle during startup, Luenberger observer, hall sensor, ...) */
  int16_t sine_of_current_rotor_angle;            /**< sine( current_rotor_angle )*/
  int16_t cosine_of_current_rotor_angle;          /**< cosine( current_rotor_angle )*/

  int16_t v_alpha_history[ 2 ];                   /**< the last two values of v_alpha (necessary for the Luenberger observer) [V/256] */
  int16_t v_beta_history[ 2 ];                    /**< the last two values of v_beta (necessary for the Luenberger observer) [V/256] */

  uint16_t param_motor_constant;                   /**< U/f coefficient of the motor (necessary for stall detection) [V/(65536*eRPM)] */
  uint16_t stall_det_min_rotor_speed;              /**< stall detection is only active above that speed [eRPM] */
  int16_t stall_det_thrshld;                      /**< threshold (i.e. maximum allowable deviation of the BEMF amplitude) for stall detection [1/256] */
  int16_t stall_det_bemf_amplitude_ref;           /**< expected BEMF amplitude (computed from rotor speed) [V/256] */
  int16_t stall_det_bemf_amplitude_current;       /**< momentary value of BEMF amplitude [V/256] */

  int16_t vbat;                                   /**< momentary value of supply voltage [V/256] */

  foc_sens_meas_mode_t rotor_angle_meas_mode; /**< defining which computation of the rotor angle is used for the FOC algorithm */

  foc_luenberger_data_t luenberger_data;

  /* ------------------ */

  uint16_t struct_signature;

} foc_ctrl_loop_data_t;

/* --------------------
   global declarations
   -------------------- */
void foc_init( void );
void foc_generate_struct_signature ( void );
void foc_reset_data ( const bool_t reset_spdmeas_module );
void foc_init_data_for_sync( const uint16_t bemf_ampl, const int16_t vsup_adc, const motor_direction_t direction );
void foc_full_foc_calc( void );

void foc_set_i_u( const int16_t i_u );
int16_t foc_get_i_u( void );
void foc_set_i_v( const int16_t i_v );
int16_t foc_get_i_v( void );
void foc_set_i_w( const int16_t i_w );
int16_t foc_get_i_w( void );

void foc_set_v_u( const int16_t v_u );
int16_t foc_get_v_u( void );
void foc_set_v_v( const int16_t v_v );
int16_t foc_get_v_v( void );
void foc_set_v_w( const int16_t v_w );
int16_t foc_get_v_w( void );

const foc_ctrl_loop_data_t * foc_get_data( void );

void foc_set_rotor_angle_startup ( uint16_t val );
void foc_set_i_q_ref( int16_t val );
void foc_set_i_d_ref( int16_t val );
int16_t foc_get_i_q_ref( void );
int16_t foc_get_i_d_ref( void );
void foc_set_rotor_angle_meas_mode( foc_sens_meas_mode_t meas_mode );
foc_sens_meas_mode_t foc_get_rotor_angle_meas_mode ( void );
int16_t foc_get_current_i_d( void );
int16_t foc_get_current_i_q( void );

void foc_set_supply_voltage_unsafe ( int16_t val );
int16_t foc_get_supply_voltage( void );

INLINE void foc_calc_i_q_ctrl_err_sum_preload_value( const int16_t v_q_out, const motor_direction_t direction );
foc_ibat_calc_data_t foc_get_ibat_calc_data_unsafe ( void );

void foc_set_stall_detect_min_rotor_speed(uint16_t stall_detect_min_rotor_speed);
void foc_set_motor_constant(uint16_t param_motor_constant);

#endif /*_#ifndef FOC_H_ */

/**
 * }@
 */
