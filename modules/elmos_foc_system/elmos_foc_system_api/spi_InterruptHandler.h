/***************************************************************************//**
 * @file			spi_InterruptHandler.h
 *
 * @creator		rpy
 * @created		04.09.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 ******************************************************************************/

#ifndef SPI_INTERRUPTHANDLER_H_          
#define SPI_INTERRUPTHANDLER_H_

#pragma once

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "el_types.h"
#include "vic_Interface.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/***************************************************************************//**
 * SPI IRQ vector numbers
 ******************************************************************************/
typedef enum {  
  spi_IRQ_EVT_RX_FIFO_OV_ERR       = 0u,
  spi_IRQ_EVT_RX_FIFO_UR_ERR       = 1u,
  spi_IRQ_EVT_TX_FIFO_OV_ERR       = 2u,
  spi_IRQ_EVT_TX_FIFO_UR_ERR       = 3u,
  spi_IRQ_EVT_SOT                  = 4u,
  spi_IRQ_EVT_EOT                  = 5u,
  spi_IRQ_EVT_SDI                  = 6u,
  spi_IRQ_EVT_SCLK_TIMEOUT         = 7u,
  spi_IRQ_EVT_SHIFT_DONE           = 8u,
  spi_IRQ_RX_FIFO_NEMPTY           = 9u,
  spi_IRQ_RX_FIFO_TIMEOUT          = 10u,
  spi_IRQ_RX_FIFO_HIGH_WATER       = 11u,
  spi_IRQ_RX_FIFO_FULL             = 12u,
  spi_IRQ_TX_FIFO_EMPTY            = 13u,
  spi_IRQ_TX_FIFO_LOW_WATER        = 14u,
  spi_IRQ_TX_FIFO_NFULL            = 15u,

  spi_INTERRUPT_VECTOR_CNT         = 16u  /**< Number of available interrupt vectors */
} spi_eInterruptVectorNum_t;


/***************************************************************************//**
 * Pointer to SPI context data
 ******************************************************************************/
typedef void * spi_pInterruptContextData_t;

/***************************************************************************//**
 * Callback function pointer type
 ******************************************************************************/
typedef void (*spi_InterruptCallback_t) (spi_eInterruptVectorNum_t irqsrc, spi_pInterruptContextData_t contextdata);

/***************************************************************************//**
 * Callback function pointer type
 ******************************************************************************/
typedef enum
{
  spi_MOD_0         = 0u,
  spi_MOD_1         = 1u,

  spi_INSTANCE_CNT  = 2u  /**< Number of spi modules implemented */

} spi_InstanceNum_t;


/***************************************************************************//**
 * SPI environment data
 ******************************************************************************/
typedef struct spi_sInterruptEnvironmentData
{
    /** Interrupt vector table of this module */
    spi_InterruptCallback_t InterrupVectorTable[spi_INTERRUPT_VECTOR_CNT];
    
    /** SPI module context data */
    spi_pInterruptContextData_t ContextData;
        
} spi_sInterruptEnvironmentData_t;

/***************************************************************************//**
 * Pointer to SPI environment data
 ******************************************************************************/
typedef spi_sInterruptEnvironmentData_t * spi_pInterruptEnvironmentData_t;

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************************* INTERRUPTS *********************************/
/* ****************************************************************************/

/***************************************************************************//**
 * @brief     Enables an IRQ.
 *
 * @param[in] modulBaseAddress  Pointer to SPI module base address
 * @param[in] irqsrc            IRQ to be enabled
 *
 * @pre       A call back function to the related interrupt should have been
 *            registered with spi_RegisterInterruptCallback().
 *
 * @post      The related call back function will be called if the desired
 *            interrupt occurs.
 *
 * @detaildesc
 * The SPI IRQ_VENABLE register will be set the related IRQ number and therefore
 * the interrupt will be activated.
 *
 ******************************************************************************/
void spi_InterruptEnable(spi_InstanceNum_t instanceNum, spi_eInterruptVectorNum_t irqsrc);

/***************************************************************************//**
 * @brief Disables an IRQ.
 *
 * @param[in] modulBaseAddress  Pointer to SPI module base address
 * @param[in] irqsrc            IRQ to be disable
 *
 * @post      The interrupt will be disabled and the related callback function
 *            will no longer be called from the interrupt handler.
 *
 * @detaildesc
 * The SPI IRQ_VDISABLE register will be set the related IRQ number and therefore
 * the interrupt will be deactivated.
 *
 ******************************************************************************/
void spi_InterruptDisable(spi_InstanceNum_t instanceNum, spi_eInterruptVectorNum_t irqsrc);

/***************************************************************************//**
 * @brief Registers/Adds callback function to the module interrupt vector table.
 *
 * @param modulBaseAddress  Pointer to SPI module base address
 * @param irqsrc            IRQ number
 * @param cbfnc             Pointer to desired callback function
 *
 * @pre     (optional) Which are the conditions to call this function? i.e. none
 *
 * @post    If the interrupt will be activated the registered callback function
 *          will be called if the IRQ occurs.
 *
 * @detaildesc
 * Registers the callback function at interrupt vector handling. It sets the
 * entry in the interrupt vector table to passed function pointer.
 *
 ******************************************************************************/
void spi_InterruptRegisterCallback(spi_InstanceNum_t instanceNum, spi_eInterruptVectorNum_t irqvecnum, spi_InterruptCallback_t cbfnc);

/***************************************************************************//**
 * @brief Deregisters/deletes callback function from module interrupt vector table.
 *
 * @param modulBaseAddress  Pointer to SPI module base address
 * @param irqvecnum         IRQ number
 * @param cbfnc             Pointer to desired callback function
 *
 * @pre   The related IRQ should be disabled.
 *
 * @post  The entry in the module interrupt vector table will point to NULL and
 *        the related IRQ will be disabled.
 *
 * @detaildesc
 * Deregisters the callback function from interrupt vector handling. It sets the
 * entry in the interrupt vector table to NULL and disables the related interrupt.
 *
 ******************************************************************************/
void spi_InterruptDeregisterCallback(spi_InstanceNum_t instanceNum, spi_eInterruptVectorNum_t irqvecnum);

/***************************************************************************//**
 * Handles the SPI related interrupt requests.
 *
 ******************************************************************************/
 __interrupt void spi_InterruptHandler0(void);
 
 /***************************************************************************//**
 * Handles the SPI related interrupt requests.
 *
 ******************************************************************************/
 __interrupt void spi_InterruptHandler1(void);

/***************************************************************************//**
 * @brief Initialize SPI module
 *
 * @param environmentdata  Pointer to Environment data for SPI module in
 *                         user RAM
 *
 * @pre        VIC (vic_VectorInterruptControl) and SPI (spi_SystemStateModule)
 *             have to presented and initialized.
 *
 * @post       SPI module is configured for use.
 *
 * @detaildesc
 * Initializes the SPI software and hardware module, including the module
 * interrupt vector table. Configures if IRQ nesting is active and if IO2 and
 * IO3 are used as SPIs or not.
 *
 ******************************************************************************/
void spi_InterruptInitialisation(spi_InstanceNum_t instanceNum, vic_cpInterfaceFunctions_t vicIf, spi_pInterruptEnvironmentData_t environmentdata, spi_pInterruptContextData_t contextdata);


#endif /* LIN_CTRL_INTERRUPTHANDLER_H_ */

