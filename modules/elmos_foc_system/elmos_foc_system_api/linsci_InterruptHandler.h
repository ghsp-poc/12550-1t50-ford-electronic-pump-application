/***************************************************************************//**
 * @file			linsci_InterruptHandler.h
 *
 * @creator		rpy
 * @created		04.09.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 ******************************************************************************/

#ifndef LINSCI_INTERRUPTHANDLER_H_          
#define LINSCI_INTERRUPTHANDLER_H_

#pragma once

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "el_types.h"
#include "vic_Interface.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/***************************************************************************//**
 * SCI IRQ vector numbers
 ******************************************************************************/
typedef enum {
  linsci_IRQ_RXD_FALLING                   = 0u,
  linsci_IRQ_RXD_RISING                    = 1u,
  linsci_IRQ_SCI_TIMER_CMP                 = 2u,
  linsci_IRQ_SCI_TIMER_OV                  = 3u,
  linsci_IRQ_BUS_ERR                       = 4u,
  linsci_IRQ_RECEIVER_ERR                  = 5u,
  linsci_IRQ_HEADER_ERR                    = 6u,
  linsci_IRQ_BREAK_EVT                     = 7u,
  linsci_IRQ_SYNC_EVT                      = 8u,
  linsci_IRQ_PID_EVT                       = 9u,
  linsci_IRQ_RX_FIFO_FULL                  = 10u,
  linsci_IRQ_RX_DMA_FINISHED               = 11u,
  linsci_IRQ_TX_FIFO_EMPTY                 = 12u,
  linsci_IRQ_TX_DMA_FINISHED               = 13u,
  linsci_IRQ_TX_FINISH_EVT                 = 14u,
  linsci_IRQ_TICK_1MS                      = 15u,
  
  linsci_INTERRUPT_VECTOR_CNT              = 16u  /**< Number of available interrupt vectors */
         
} linsci_eInterruptVectorNum_t;

/***************************************************************************//**
 * Pointer to SCI context data
 ******************************************************************************/
typedef void * linsci_pInterruptContextData_t;

/***************************************************************************//**
 * Callback function pointer type
 ******************************************************************************/
typedef void (*linsci_InterruptCallback_t) (linsci_eInterruptVectorNum_t irqsrc, linsci_pInterruptContextData_t contextdata);

/***************************************************************************//**
 * SCI environment data
 ******************************************************************************/
typedef struct linsci_sInterruptEnvironmentData
{
    /** Interrupt vector table of this module             */
    linsci_InterruptCallback_t InterrupVectorTable[linsci_INTERRUPT_VECTOR_CNT];
    
    /** SCI module context data */
    linsci_pInterruptContextData_t ContextData;
    
} linsci_sInterruptEnvironmentData_t;

/***************************************************************************//**
 * Pointer to SCI environment data
 ******************************************************************************/
typedef linsci_sInterruptEnvironmentData_t * linsci_pInterruptEnvironmentData_t;

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************************* INTERRUPTS *********************************/
/* ****************************************************************************/

/***************************************************************************//**
 * @brief     Enables an IRQ.
 *
 * @param[in] modulBaseAddress  Pointer to SCI module base address
 * @param[in] irqsrc            IRQ to be enabled
 *
 * @pre       A call back function to the related interrupt should have been
 *            registered with sci_RegisterInterruptCallback().
 *
 * @post      The related call back function will be called if the desired
 *            interrupt occurs.
 *
 * @detaildesc
 * The SCI IRQ_VENABLE register will be set the related IRQ number and therefore
 * the interrupt will be activated.
 *
 ******************************************************************************/
void linsci_InterruptEnable(linsci_eInterruptVectorNum_t irqsrc);

/***************************************************************************//**
 * @brief Disables an IRQ.
 *
 * @param[in] modulBaseAddress  Pointer to SCI module base address
 * @param[in] irqsrc            IRQ to be disable
 *
 * @post      The interrupt will be disabled and the related callback function
 *            will no longer be called from the interrupt handler.
 *
 * @detaildesc
 * The SCI IRQ_VDISABLE register will be set the related IRQ number and therefore
 * the interrupt will be deactivated.
 *
 ******************************************************************************/
void linsci_InterruptDisable(linsci_eInterruptVectorNum_t irqsrc);

/***************************************************************************//**
 * @brief Registers/Adds callback function to the module interrupt vector table.
 *
 * @param modulBaseAddress  Pointer to SCI module base address
 * @param irqsrc            IRQ number
 * @param cbfnc             Pointer to desired callback function
 *
 * @pre     (optional) Which are the conditions to call this function? i.e. none
 *
 * @post    If the interrupt will be activated the registered callback function
 *          will be called if the IRQ occurs.
 *
 * @detaildesc
 * Registers the callback function at interrupt vector handling. It sets the
 * entry in the interrupt vector table to passed function pointer.
 *
 ******************************************************************************/
void linsci_InterruptRegisterCallback(linsci_eInterruptVectorNum_t irqsrc, linsci_InterruptCallback_t cbfnc);

/***************************************************************************//**
 * @brief Deregisters/deletes callback function from module interrupt vector table.
 *
 * @param modulBaseAddress  Pointer to SCI module base address
 * @param irqvecnum         IRQ number
 * @param cbfnc             Pointer to desired callback function
 *
 * @pre   The related IRQ should be disabled.
 *
 * @post  The entry in the module interrupt vector table will point to NULL and
 *        the related IRQ will be disabled.
 *
 * @detaildesc
 * Deregisters the callback function from interrupt vector handling. It sets the
 * entry in the interrupt vector table to NULL and disables the related interrupt.
 *
 ******************************************************************************/
void linsci_InterruptDeregisterCallback(linsci_eInterruptVectorNum_t irqvecnum);

/***************************************************************************//**
 * Handles the SCI related interrupt requests.
 *
 ******************************************************************************/
 __interrupt void linsci_InterruptHandler(void);

/***************************************************************************//**
 * @brief Initialize SCI module
 *
 * @param environmentdata  Pointer to Environment data for SCI module in
 *                         user RAM
 *
 * @pre        VIC (vic_VectorInterruptControl) and SCI (sci_SystemStateModule)
 *             have to presented and initialized.
 *
 * @post       SCI module is configured for use.
 *
 * @detaildesc
 * Initializes the SCI software and hardware module, including the module
 * interrupt vector table. Configures if IRQ nesting is active and if IO2 and
 * IO3 are used as SCIs or not.
 *
 ******************************************************************************/
void linsci_InterruptInitialisation(vic_cpInterfaceFunctions_t vicIf, linsci_pInterruptEnvironmentData_t environmentdata, linsci_pInterruptContextData_t contextdata);


#endif /* LIN_CTRL_INTERRUPTHANDLER_H_ */

