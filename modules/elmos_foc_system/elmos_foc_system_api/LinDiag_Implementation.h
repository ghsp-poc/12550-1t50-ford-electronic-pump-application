/***************************************************************************//**
 * @file			LinDiag_Implementation.h
 *
 * @creator		sbai
 * @created		12.02.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id$
 *
 * $Revision$
 *
 ******************************************************************************/

#ifndef LINDIAG_IMPLEMENTATION_H_
#define LINDIAG_IMPLEMENTATION_H_

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "LinDrvImp_CompilationConfig.h"
#include "LinDiag_Interface.h"
#if LINDIAG_SUP_DATASTG == 1
#include "LinDataStg_Interface.h"
#endif

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/
#define LINDIAGIMP_VERSION             0x0110u /**< LIN DIAG implementation version */

#define LINDIAGIMP_CONFIG_DATA_VERSION 0x0100  /* Expected config data version */

#define LINDIAG_SUP_CLASS_1            1
#define LINDIAG_SUP_CLASS_2            0
#define LINDIAG_SUP_CLASS_3            0

#define ELMOS_BLTEST                   0

#define LINDIAG_SNPD_REQ_MSG_LEN       5

#define LINDIAG_SNPD_NAD_POS           4

#define LINDIAG_SNPD_CMD_POS           2

#define LINDIAG_SNPD_CMD_MEASURE       1
#define LINDIAG_SNPD_CMD_KEEP_NAD      2
#define LINDIAG_SNPD_CMD_STORE_NAD     3
#define LINDIAG_SNPD_CMD_DISABLE       4

/* ************************************ ************************************/
/* *********************** STRUCTS, ENU ND TYPEDEFS ************************/
/* ****************************************************************************/
/***************************************************************************//**
 * TODO: Concrete description.
 *
 ******************************************************************************/
struct LinDiagImp_sCfgData
{
    Lin_Version_t                      Version;                /* Config data struct version */

    LinTransIf_sInitParam_t            LinTransIfInitParam;
    LinBusIf_sThis_t                   LinBusIfThisPointer;
    LinLookupIf_sThis_t                LinLookupIfThisPointer;
#if LINDIAG_SUP_DATASTG == 1
    LinDataStgIf_sThis_t               LinDataStgIfThisPointer;
#else
    LinDiagIf_sProductIdentification_t InitialProdIdent;
    LinDiagIf_NAD_t                    InitialNad;
    LinDiagIf_SerialNumber_t           InitialSerialNumber;
#endif
#if LINDIAG_SUP_SNPD == 1    
    LinSNPDIf_sThis_t                  LinSNPDIfThisPointer;
    Lin_Bool_t                         ImmediateNADUpdate;
#endif    
};

typedef struct LinDiagImp_sCfgData    LinDiagImp_sCfgData_t;
typedef        LinDiagImp_sCfgData_t* LinDiagImp_pCfgData_t;

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/
#if LINDIAGIMP_EXT_IFFUN_STRCT_ACCESS == 1
extern const LinDiagIf_sInterfaceFunctions_t LinDiagImp_InterfaceFunctions;
#endif /* LINDIAGIMP_EXT_IFFUN_STRCT_ACCESS == 1 */

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
 Lin_Bool_t LinDiagImp_Initialization(LinDiagIf_pGenericEnvData_t     genericDiagEnvData,      LinDiagIf_EnvDataSze_t         diagEnvDataSze,
                                      LinDiagIf_cpCallbackFunctions_t diagCbFuns,              LinDiagIf_pGenericCbCtxData_t  genericDiagCbCtxData,
                                      Lin_Bool_t                      invalidReadByIDAnswered, LinDiagIf_pGenericImpCfgData_t genericDiagImpCfgData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
void LinDiagImp_Task(LinDiagIf_pGenericEnvData_t genericDiagEnvData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
Lin_Bool_t LinDiagImp_GetSubInterface(LinDiagIf_pGenericEnvData_t genericDiagEnvData, Lin_eInterfaceIds_t interfaceId, Lin_pThis_t ifThisPtr);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
LinTransIf_NAD_t LinDiagImp_GetNad(LinDiagIf_pGenericEnvData_t genericDiagEnvData, LinDiagIf_eNADType_t nadtype);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
void LinDiagImp_SetNad(LinDiagIf_pGenericEnvData_t genericDiagEnvData, LinTransIf_NAD_t nad);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
Lin_Bool_t LinDiagImp_AddRbiTable(LinDiagIf_pGenericEnvData_t genericDiagEnvData, LinDiagIf_pRbiLookupEntry_t rbiTbl);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
Lin_Bool_t LinDiagImp_RmvRbiTable(LinDiagIf_pGenericEnvData_t genericDiagEnvData, LinDiagIf_pRbiLookupEntry_t rbiTbl);

#endif /* LINDIAG_IMPLEMENTATION_H_ */

