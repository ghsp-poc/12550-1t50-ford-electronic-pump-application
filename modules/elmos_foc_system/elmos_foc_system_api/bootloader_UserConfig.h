/***************************************************************************//**
 * @file			bootloader_UserConfig.h
 *
 * @creator		sbai
 * @created		15.06.2015
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 ******************************************************************************/

#ifndef BOOTLOADER_USERCONFIG_H_
#define BOOTLOADER_USERCONFIG_H_

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/
typedef uint8_t  bldConf_NAD_t;

struct bldConf_sBootloaderEntryMode
{
  uint16_t   FBLPinEnabled              : 1;
  uint16_t   FBLPinPolarity             : 1;
  uint16_t   FBLPinGPIOModuleNumber     : 2;
  uint16_t   FBLPinNumber               : 3;
  uint16_t   CheckAppCRC                : 1;  // if 0 -> check cookie only, 1 -> check cookie & crc
  
  uint16_t   TimeWindow                 : 3;  // -> 0 = off, 1 .. 7  = 1=20ms ... 2=30 ... 3=40 ... 7=80ms
  uint16_t   EnableSoftResetEntry       : 1;  // -> 0 = off, 1 = use software reset codes to enter bootloader from application.  
  uint16_t   StayInBootloaderNoApp      : 1;  // -> 0 = if threre is no valid application, and the TW has expired go to save state, 1 = reactivate bootloader permanently.  
};

typedef struct bldConf_sBootloaderEntryMode bldConf_BootloaderEntryMode_t;

typedef uint32_t bldConf_Baudrate_t;

struct bldConf_sSerial // keep in sync with bldIf_pSerial_t!
{
  uint8_t Data[4];
};

typedef struct bldConf_sSerial  bldConf_Serial_t;
typedef bldConf_Serial_t *      bldConf_pSerial_t;

struct bldConf_sProductInfo // keep in sync with bldIf_sProductInfo
{
  uint16_t SupplierID;
  uint16_t FunctionID;
  uint8_t  VariantID;
  uint8_t  Stepping;
};

typedef struct bldConf_sProductInfo bldConf_ProductInfo_t;

struct bldConf_sKey
{
  uint8_t Data[8];
};


typedef struct bldConf_sKey  bldConf_Key_t;
typedef bldConf_Key_t *      bldConf_pKey_t;

typedef uint16_t bldConf_Lock_t;

struct bldConf_sConfigData
{
  bldConf_NAD_t                         NodeAddress;
  bldConf_BootloaderEntryMode_t         EntryMode;
  bldConf_Baudrate_t                    Baudrate;
  bldConf_Serial_t                      Serial;
  bldConf_ProductInfo_t                 ProductInfo;
  bldConf_Key_t                         Key;
  bldConf_Lock_t                        Lock;
};

typedef struct bldConf_sConfigData    bldConf_ConfigData_t;
typedef       bldConf_ConfigData_t *  bldConf_pConfigData_t;
typedef const bldConf_ConfigData_t *  bldConf_cpConfigData_t;

struct bldConf_sDeviceSerial // keep in sync with bldIf_DeviceSerial_t!
{
  uint8_t Data[6];
};

typedef struct bldConf_sDeviceSerial  bldConf_DeviceSerial_t;
typedef bldConf_DeviceSerial_t *      bldConf_pDeviceSerial_t;

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

#endif /* BOOTLOADER_USERCONFIG_H_ */
