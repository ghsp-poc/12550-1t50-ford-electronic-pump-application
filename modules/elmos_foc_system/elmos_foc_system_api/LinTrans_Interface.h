/***************************************************************************//**
 * @file			LinTrans_Interface.h
 *
 * @creator		sbai
 * @created		13.02.2015
 *
 * @brief  		Definition of the interface for the 'TRANS Layer'.
 *
 * $Id$
 *
 * $Revision$
 *
 ******************************************************************************/

#ifndef LINTRANS_INTERFACE_H_
#define LINTRANS_INTERFACE_H_

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "Lin_Basictypes.h"
#include "LinProto_FrameDescription.h"
#if LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1
#include "LinProto_Signal.h"
#endif
#include "LinTrans_Types.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/
#define LINTRANS_INTERFACE_MODULE_API_VERSION         0x0104u /**< LIN TRANS interface version */

/***************************************************************************//**
 * @brief Macro for adding a service to 'SID List'.
 ******************************************************************************/
#define LINTRANS_INTERFACE_ADD_SERVICE(SERVICEID,         \
                                       CB_FUN,      \
                                       CB_CTX_TYPE, \
                                       CB_CTX)      \
                                       \
                                       {.SID             = (SERVICEID),   \
                                        .CallbackFnc     = (CB_FUN),      \
                                        .CallbackCtxType = (CB_CTX_TYPE), \
                                        .CallbackCtx     = (CB_CTX)}      \

/***************************************************************************//**
 * @brief Macro to mark the end of a 'SID List'.
 ******************************************************************************/
#define LINTRANS_INTERFACE_ADD_SERVICE_LIST_END  {.CallbackFnc =  LIN_NULL}

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/***************************************************************************//**
 * @brief Defines the callback context data which shall be passed to the
 *        'SID callback' callback.
 ******************************************************************************/
enum LinTransIf_eSIDDescLstCbCtxType
{
  LinTransIf_SIDDescLstCbCtxType_PER_SID  = 0, /**< The callback context of the SID description in the 'SID List'
                                                    will be passed to the callback function. @see LinTransIf_sSIDDescription */
  LinTransIf_SIDDescLstCbCtxType_PER_LST  = 1  /**< The callback context attached to the 'SID List'
                                                    will be passed to the callback function.*/
};

typedef enum LinTransIf_eSIDDescLstCbCtxType LinTransIf_eSIDDescLstCbCtxType_t; /**< Typedef of LinTransIf_eSIDDescLstCbCtxType */

/***************************************************************************//**
 * @brief Configuration data for the TRANS Layer to setup a
 *        'Frame ID Descriptions' for an 'Request-/Response-Channel' in
 *        PROTO layer.
 ******************************************************************************/
struct LinTransIf_sProtoFrameInfo
{
  LinBusIf_FrameID_t       MsgID;   /**< 'Message ID' of the TRANS Layer channel. */
  LinProtoIf_FrameLenght_t Length;  /**< Length of the LIN Frame used by the TRANS module layer. */
  LinProtoIf_eCrcType_t    CrcType; /**< CRC calculation type of the TRANS Layer channel. */
};

typedef struct LinTransIf_sProtoFrameInfo     LinTransIf_sProtoFrameInfo_t;   /**< Typedef of LinTransIf_sProtoFrameInfo */
typedef        LinTransIf_sProtoFrameInfo_t * LinTransIf_pProtoFrameInfo_t;   /**< Typedef of pointer to LinTransIf_sProtoFrameInfo. */
typedef const  LinTransIf_sProtoFrameInfo_t * LinTransIf_cpProtoFrameInfo_t;  /**< Typedef of constant pointer to LinTransIf_sProtoFrameInfo. */

/***************************************************************************//**
 * @brief Complete configuration of the TRANS Layer to setup
 *        the PROTO layer module.
 ******************************************************************************/
struct LinTransIf_sProtoFrameDescriptionInfo
{
  LinTransIf_sProtoFrameInfo_t            ReqFrameInfo;             /**< Request-Channel configuration. */
  LinTransIf_sProtoFrameInfo_t            RespFrameInfo;            /**< Response-Channel configuration. */
  
  LinProtoIf_pGenericFrmDescLstEnvData_t  FrameDescListEnvData;     /**< Environment data per 'Frame Description List' (See also: LIN_LOOKUP_FRM_DESC_ENV_DATA_SZE) */
  LinProtoIf_EnvDataSze_t                 FrameDescListEnvDataSze;  /**< Size of reserved environment data per 'Frame Description List'.  */

  Lin_Bool_t                              LdfRelevance;             /**< LIN_TRUE if the Response-/Request-Channel can be assigned/unassigned. */
};

typedef struct LinTransIf_sProtoFrameDescriptionInfo     LinTransIf_sProtoFrameDescriptionInfo_t;   /**< Typedef of LinTransIf_sProtoFrameDescriptionInfo */
typedef        LinTransIf_sProtoFrameDescriptionInfo_t * LinTransIf_pProtoFrameDescriptionInfo_t;   /**< Typedef of pointer to LinTransIf_sProtoFrameDescriptionInfo. */
typedef const  LinTransIf_sProtoFrameDescriptionInfo_t * LinTransIf_cpProtoFrameDescriptionInfo_t;  /**< Typedef of constant pointer to LinTransIf_sProtoFrameDescriptionInfo. */

/* ******************************************************************************
 * TRANS Layer interface function types
 *******************************************************************************/

/** @addtogroup LinTransIfIfFunDefs */
/**@{*/

/***************************************************************************//**
 * @brief Typedef of TRANS layer 'Initialization' interface function.
 *
 * @param genericTransEnvData[in]     Pointer to reserved TRANS Layer environment data.
 * @param transEnvDataSze[in]         Size of the reserved RAM for TRANS environment
 *                                    data.
 * @param transCbFuns[in]             Pointer to TRANS Layer callback function.
 *                                    struct. Implemented in the higher layer or
 *                                    user application.
 *                                    (LinTransIf_sCallbackFunctions).
 * @param genericTransCbCtxData[in]   Pointer to TRANS Layer callback context
 *                                    data.
 * @param protoFrmDescInfo[in]        Pointer to the 'Frame Description List' used
 *                                    by the TRANS Layer for its Request and Response
 *                                    Channel.
 * @param nasTimeout[in]              N_As timeout.
 *                                    (See also: LIN 2.2a Specification -
 *                                    Chapter 3.2.5 - TIMING CONSTRAINTS)
 * @param ncrTimeout[in]              N_Cr timeout.
 *                                    (See also: LIN 2.2a Specification -
 *                                    Chapter 3.2.5 - TIMING CONSTRAINTS)
 * @param genericTransImpCfgData[in]  Pointer to implementation dependent
 *                                    configuration data for the TRANS Layer.
 *                                    (LinTransImp_sCfgData)
 *
 * @return LIN_TRUE if the initialization was successfully LIN_FALSE if not.
 *
 * Initializes the TRANS Layer.
 *
 ******************************************************************************/
typedef Lin_Bool_t (*LinTransIf_InitializationIfFun_t) (LinTransIf_pGenericEnvData_t             genericTransEnvData, LinTransIf_EnvDataSze_t         transEnvDataSze,
                                                        LinTransIf_cpCallbackFunctions_t         transCbFuns,         LinBusIf_pGenericCbCtxData_t    genericTransCbCtxData,
                                                        LinTransIf_cpProtoFrameDescriptionInfo_t protoFrmDescInfo,    LinTransIf_NasTimeout_t         nasTimeout,
                                                        LinTransIf_NcrTimeout_t                  ncrTimeout,          LinTransIf_pGenericImpCfgData_t genericTransImpCfgData);

/***************************************************************************//**
 * @brief Typedef of TRANS Layer 'Task' interface function.
 *
 * @param genericTransEnvData[in] Pointer to reserved TRANS Layer environment data.
 *
 * The 'Task' function has to be called periodically to process scheduled task of
 * the TRANS Layer.
 *
 ******************************************************************************/
typedef void (*LinTransIf_TaskIfFun_t) (LinTransIf_pGenericEnvData_t genericTransEnvData);

/***************************************************************************//**
 * @brief Typedef of TRANS Layer 'Get Sub-Interface' interface function.
 *
 * @param genericTransEnvData[in] Pointer to reserved TRANS Layer environment data.
 * @param interfaceId[in]         Sub-Interface ID.
 * @param ifThisPtr[out]          Variable (pointer) which will be set to the
 *                                address of the This-Pointer of the desired
 *                                sub-interface.
 *
 * @return  LIN_TRUE if the desired interface is available and the parameter
 *          @p ifThisPtr could be set.
 *
 * Acquires the desired sub-interface (@p interfaceId) and writes the address
 * of its This-Pointer to @p ifThisPtr.
 *
 ******************************************************************************/
typedef Lin_Bool_t (*LinTransIf_GetSubInterfaceIfFun_t) (LinTransIf_pGenericEnvData_t genericTransEnvData, Lin_eInterfaceIds_t interfaceId, Lin_pThis_t ifThisPtr);

/***************************************************************************//**
 * @brief Typedef of TRANS Layer 'Get milliseconds' interface function.
 *
 * @param genericTransEnvData[in] Pointer to reserved TRANS Layer environment data.
 *
 * @return The milliseconds counter value.
 *
 * Get the  milliseconds counter value.
 *
 ******************************************************************************/
typedef LinTransIf_Tick_t (*LinTransIf_GetMillisecondsIfFun_t) (LinTransIf_pGenericEnvData_t genericTransEnvData);

/***************************************************************************//**
 * @brief Typedef of TRANS Layer 'Init Response' interface function.
 *
 * @param genericTransEnvData[in] Pointer to reserved TRANS Layer environment data.
 * @param responseNAD[in]         NAD which shall be used in the 'Response'.
 * @param rsid[in]                Response SID, normally SID + 0x40.
 *                                (See also: LIN 2.2a Specification -
 *                                Chapter 4.2.3.5 RSID)
 * @param dataLen[in]             Length of data to response with.
 *
 * @return LIN_TRUE if all parameter are correct.
 *
 * If the user application or a higher layer module receives a 'LIN Transport
 * Layer Request'. This function initializes the 'Response' to send.
 *
 ******************************************************************************/
typedef Lin_Bool_t (*LinTransIf_InitResponseIfFun_t) (LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_NAD_t       responseNAD,
                                                      LinTransIf_SID_t             rsid,                LinTransIf_PDUMsgLen_t dataLen);

/***************************************************************************//**
 * @brief Typedef of TRANS Layer 'Append Data to Message Buffer' interface function.
 *
 * @param genericTransEnvData[in] Pointer to reserved TRANS Layer environment data.
 * @param data[in]                Pointer to data to append to message buffer.
 * @param dataLen[in]             Length of data to append.
 *
 * @return LIN_TRUE if the data has been appended successfully to the
 *         message buffer.
 *
 * This function is necessary to deserialize the collection of data and
 * the actual process of sending them back onto the bus through a
 * 'LIN Transport Layer Response'. Not every device has the RAM to buffer
 * the full amount of data, which is possible to send with the 'LIN Transport
 * Layer'.
 *
 * @see LIN 2.2a Specification - Chapter 3.2.1.4 LEN
 *
 ******************************************************************************/
typedef Lin_Bool_t (*LinTransIf_AppendDataToMsgBufferIfFun_t) (LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_cpData_t data,
                                                               LinTransIf_BufLength_t       dataLen);

/***************************************************************************//**
 * @brief Typedef of TRANS Layer 'Add SID Description List' interface function.
 *
 * @param genericTransEnvData[in]             Pointer to reserved TRANS Layer
 *                                            environment data.
 * @param sidDescLst[in]                      Pointer to the 'SID Description List
 *                                            to add to the TRANS Layer.
 * @param genericTransPerSidLstCbCtxData[in]  Callback context data connected to this
 *                                            'SID List'.
 *                                            (See also: @ref LinTransIf_eSIDDescLstCbCtxType)
 *
 * @return LIN_TRUE, if the list has been successfully added.
 *
 * Adds a 'SID List' to the TRANS Layer on which SIDs it should react on.
 *
 ******************************************************************************/
typedef Lin_Bool_t (*LinTransIf_AddSidDescLstIfFun_t) (LinTransIf_pGenericEnvData_t   genericTransEnvData, LinTransIf_cpSIDDescription_t sidDescLst,
                                                       LinTransIf_pGenericCbCtxData_t genericTransPerSidLstCbCtxData);

/***************************************************************************//**
 * @brief Typedef of TRANS Layer 'Remove SID Description List' interface function.
 *
 * @param genericTransEnvData[in] Pointer to reserved TRANS Layer environment data.
 * @param sidDescLst[in]          Pointer to the 'SID Description List
 *                                to remove from the TRANS Layer.
 *
 * @return LIN_TRUE, if the list has been successfully removed.
 *
 * Removes a list of 'SIDs' from the TRANS Layer.
 *
 ******************************************************************************/
typedef Lin_Bool_t (*LinTransIf_RmvSidDescLstIfFun_t) (LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_cpSIDDescription_t sidDescLst);

/***************************************************************************//**
 * @brief Typedef of TRANS Layer 'Get Status' interface function.
 *
 * @param genericTransEnvData[in] Pointer to reserved TRANS Layer environment data.
 *
 * @return The current status of the TRANS Layer.
 *
 * Acquire the current status of the TRANS Layer.
 *
 ******************************************************************************/
typedef LinTransIf_eComStatus_t (*LinTransIf_GetStatusIfFun_t) (LinTransIf_pGenericEnvData_t genericTransEnvData);

/***************************************************************************//**
 * @brief Typedef of TRANS Layer 'Set Timeout' interface function.
 *
 * @param genericTransEnvData[in] Pointer to reserved TRANS Layer environment data.
 * @param timeoutType[in]         Timeout type to set. (@ref LinTransIf_eTimeoutType_t)
 * @param timeout[in]             Timeout value.
 *
 * Set the N_As or N_Cr timeout.
 *
 ******************************************************************************/
typedef void (*LinTransIf_SetTimeoutIfFun_t) (LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_eTimeoutType_t timeoutType, LinTransIf_Timeout_t timeout);

/**@} LinTransIfIfFunDefs */

/***************************************************************************//**
 * @brief TRANS Layer interface functions struct.
 *
 * Collective struct for the TRANS Layer interface functions.
 ******************************************************************************/
struct LinTransIf_sInterfaceFunctions
{
    Lin_Version_t                            InterfaceVersion;             /**< @brief TRANS Layer interface version.*/

    LinTransIf_InitializationIfFun_t         Initialization;               /**< @brief Pointer to TRANS Layer 'Initialization' interface function. (@ref LinTransIf_InitializationIfFun_t) @copydetails LinTransIf_InitializationIfFun_t */
    LinTransIf_TaskIfFun_t                   Task;                         /**< @brief Pointer to TRANS Layer 'Task' interface function. (@ref LinTransIf_TaskIfFun_t) @copydetails LinTransIf_TaskIfFun_t */
    LinTransIf_GetSubInterfaceIfFun_t        GetSubInterface;              /**< @brief Pointer to TRANS Layer 'Get Sub Interface' interface function. (@ref LinTransIf_GetSubInterfaceIfFun_t) @copydetails LinTransIf_GetSubInterfaceIfFun_t */
    LinTransIf_InitResponseIfFun_t           InitResponse;                 /**< @brief Pointer to TRANS Layer 'Init Response' interface function. (@ref LinTransIf_InitResponseIfFun_t) @copydetails LinTransIf_InitResponseIfFun_t */
    LinTransIf_AppendDataToMsgBufferIfFun_t  AppendDataToMsgBuffer;        /**< @brief Pointer to TRANS Layer 'Append Data to Message Buffer' interface function. (@ref LinTransIf_AppendDataToMsgBufferIfFun_t) @copydetails LinTransIf_AppendDataToMsgBufferIfFun_t */
    LinTransIf_AddSidDescLstIfFun_t          AddSIDDescriptionList;        /**< @brief Pointer to TRANS Layer 'Add SID Description List' interface function. (@ref LinTransIf_AddSidDescLstIfFun_t) @copydetails LinTransIf_AddSidDescLstIfFun_t */
    LinTransIf_RmvSidDescLstIfFun_t          RemoveSIDDescriptionList;     /**< @brief Pointer to TRANS Layer 'Remove SID Description List' interface function. (@ref LinTransIf_RmvSidDescLstIfFun_t) @copydetails LinTransIf_RmvSidDescLstIfFun_t */
    LinTransIf_GetStatusIfFun_t              GetStatus;                    /**< @brief Pointer to TRANS Layer 'Get Status' interface function. (@ref LinTransIf_GetStatusIfFun_t) @copydetails LinTransIf_GetStatusIfFun_t */
    LinTransIf_SetTimeoutIfFun_t             SetTimeout;                   /**< @brief Pointer to TRANS Layer 'Set Timeout' interface function. (@ref LinTransIf_SetTimeoutIfFun_t) @copydetails LinTransIf_SetTimeoutIfFun_t */
};

/* *****************************************************************************
 * TRANS Layer callback function types.
 ******************************************************************************/

/** @addtogroup LinTransIfCbFunDefs */
/**@{*/

/***************************************************************************//**
 * @brief Typedef of TRANS Layer 'SID' callback function.
 *
 * @param genericTransEnvData[in]   Pointer to reserved TRANS Layer environment data.
 * @param nad[in]                   LIN Transport Layer Node Address (See also:
 *                                  LIN 2.2a Specification - Chapter 4.2.3.2 NAD)
 * @param sid[in]                   LIN Transport Layer Service ID. (See also:
 *                                  LIN 2.2a Specification - Chapter 3.2.1.5 SID)
 * @param data[in]                  Pointer to data of the 'Service Request'.
 * @param dataLen[in]               Length of the data.
 * @param errorFlag[in]             LIN_TRUE, if an error occurred during the
 *                                  reception/transmission.
 * @param genericTransCbCtxData[in] Pointer to TRANS Layer callback context
 *                                  data.
 *
 * @return How the TRANS Layer should react to this service request.
 *         (@ref LinTransIf_eSIDReaction)
 *
 * This callback is called on reception of a SID.
 *
 ******************************************************************************/
typedef LinTransIf_eSIDReaction_t (*LinTransIf_SIDCbFun_t) (LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_NAD_t       nad,
                                                            LinTransIf_SID_t             sid,                 LinTransIf_pData_t     data,
                                                            LinTransIf_BufLength_t       dataLen,             LinTransIf_ErrorFlag_t errorFlag,
                                                            LinBusIf_pGenericCbCtxData_t genericTransCbCtxData);

/***************************************************************************//**
 * @brief Typedef of TRANS Layer 'Error' callback function.
 *
 * @param genericTransEnvData[in]   Pointer to reserved TRANS Layer environment data.
 * @param transIfFuns[in]           Pointer to the TRANS Layer interface function struct.
 * @param error[in]                 Occurred error.
 * @param sid[in]                   Related Service ID.
 * @param genericTransCbCtxData[in] Pointer to TRANS Layer callback context
 *                                  data.
 *
 * Indicates an error in the TRANS Layer and underlying layers of the LIN
 * Driver.
 *
 * @par "Call Description:"
 * @mscfile msc_trans_errorcbcalled.dox
 * @n
 *
 ******************************************************************************/
typedef void (*LinTransIf_ErrorCbFun_t) (LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_cpInterfaceFunctions_t transIfFuns,
                                         LinTransIf_Error_t           error,               LinTransIf_SID_t                  sid,
                                         LinBusIf_pGenericCbCtxData_t genericTransCbCtxData);

/***************************************************************************//**
 * @brief Typedef of TRANS Layer 'SID Processed' callback function.
 *
 * @param genericTransEnvData[in]   Pointer to reserved TRANS Layer environment data.
 * @param transIfFuns[in]           Pointer to the TRANS Layer interface function struct.
 * @param sid[in]                   Related Service ID.
 * @param errorFlag[in]             LIN_TRUE, if an error occurred during the
 *                                  reception/transmission.
 * @param genericTransCbCtxData[in] Pointer to TRANS Layer callback context
 *                                  data.
 *
 * This callback is called after a 'Service ID' has been processed.
 *
 * @par "Call Description:"
 * @mscfile msc_trans_sidprocessedcbcalled.dox
 * @n
 *
 ******************************************************************************/
typedef void (*LinTransIf_SIDProcessedCbFun_t) (LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_cpInterfaceFunctions_t transIfFuns,
                                                LinTransIf_SID_t             sid,                 LinTransIf_ErrorFlag_t            errorFlag,
                                                LinBusIf_pGenericCbCtxData_t genericTransCbCtxData);

/***************************************************************************//**
 * @brief Typedef of TRANS Layer 'Check NAD' callback function.
 *
 * @param genericTransEnvData[in]   Pointer to reserved TRANS Layer environment data.
 * @param transIfFuns[in]           Pointer to the TRANS Layer interface function struct.
 * @param nad[in]                   Node address to check.
 * @param sid[in]                   Related Service ID.
 * @param genericTransCbCtxData[in] Pointer to TRANS Layer callback context
 *                                  data.
 *
 * @return If the NAD should be accepted or declined.
 *
 * Callback function to let the user/higher layer decided, if the NAD
 * should be accepted or not.
 *
 * @par "Call Description:"
 * @mscfile msc_trans_checknadcbcalled.dox
 * @n
 *
 ******************************************************************************/
typedef LinTransIf_eCheckNADResult_t (*LinTransIf_CheckNADCbFun_t) (LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_cpInterfaceFunctions_t transIfFuns,
                                                                    LinTransIf_NAD_t             nad,                 LinTransIf_SID_t                  sid,
                                                                    LinBusIf_pGenericCbCtxData_t genericTransCbCtxData);

/***************************************************************************//**
 * @brief Typedef of TRANS Layer 'Go To Sleep' callback function.
 *
 * @param genericTransEnvData[in]   Pointer to reserved TRANS Layer environment data.
 * @param transIfFuns[in]           Pointer to the TRANS Layer interface function struct.
 * @param genericTransCbCtxData[in] Pointer to TRANS Layer callback context
 *                                  data.
 *
 * This callback is called if the TRANS Layer receives a 'Go to sleep'
 * command PDU.
 *
 * @par "Call Description:"
 * @mscfile msc_trans_gotosleepcbcalled.dox
 * @n
 *
 *
 ******************************************************************************/
typedef void (*LinTransIf_GoToSleepCbFun_t) (LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_cpInterfaceFunctions_t transIfFuns,
                                             LinBusIf_pGenericCbCtxData_t genericTransCbCtxData);

/**@} LinTransIfCbFunDefs */

/***************************************************************************//**
 * @brief TRANS Layer callback functions struct.
 *
 * The set of callbacks which should be invoked in the higher layers.
 ******************************************************************************/
struct LinTransIf_sCallbackFunctions
{
    Lin_Version_t                   CallbackVersion;  /**< @brief TRANS Layer Callback Version.*/
  
    LinTransIf_ErrorCbFun_t         Error;            /**< @brief Pointer to TRANS Layer 'Error' callback function. (@ref LinTransIf_ErrorCbFun_t) @copydetails LinTransIf_ErrorCbFun_t */
    LinTransIf_SIDProcessedCbFun_t  SIDProcessed;     /**< @brief Pointer to TRANS Layer 'SID Processed' callback function. (@ref LinTransIf_SIDProcessedCbFun_t) @copydetails LinTransIf_SIDProcessedCbFun_t */
    LinTransIf_CheckNADCbFun_t      CheckNAD;         /**< @brief Pointer to TRANS Layer 'Check NAD' callback function. (@ref LinTransIf_CheckNADCbFun_t) @copydetails LinTransIf_CheckNADCbFun_t */
    LinTransIf_GoToSleepCbFun_t     GoToSleep;        /**< @brief Pointer to TRANS Layer 'Go To Sleep' callback function. (@ref LinTransIf_GoToSleepCbFun_t) @copydetails LinTransIf_GoToSleepCbFun_t */
};

/**@} LinTransIfIfFunDefs */

/***************************************************************************//**
 * @brief Object-like This-pointer to connect the TRANS Layer to higher layers
 *        or application.
 ******************************************************************************/
struct LinTransIf_sThis
{
    LinTransIf_cpInterfaceFunctions_t IfFunsTbl; /**< @brief Pointer to the TRANS Layer interface function struct. (LinTransIf_sInterfaceFunctions) */
    LinTransIf_pGenericEnvData_t      EnvData;   /**< @brief Pointer to reserved TRANS Layer environment data. */
};

/***************************************************************************//**
 * @brief TRANS Layer interface configuration parameter.
 *
 * Data needed for initialization of the TRANS Layer.
 ******************************************************************************/
struct LinTransIf_sInitParam
{
    LinTransIf_cpInterfaceFunctions_t        IfFunsTbl;         /**< @brief Pointer to the constant TRANS Layer interface function struct. */
    LinTransIf_pGenericEnvData_t             EnvData;           /**< @brief Pointer to reserved TRANS Layer environment data. */
    LinTransIf_EnvDataSze_t                  EnvDataLen;        /**< @brief Size of the reserved RAM for TRANS Layer environment data. */
    LinTransIf_cpProtoFrameDescriptionInfo_t ProtoFrmDescInfo;  /**< @brief Pointer to the 'Frame Description List' used by the TRANS Layer for its Request and Response Channel. */
    LinTransIf_NasTimeout_t                  NasTimeout;        /**< @brief N_As timeout (See also: LIN 2.2a Specification - Chapter 3.2.5 - TIMING CONSTRAINTS) */
    LinTransIf_NcrTimeout_t                  NcrTimeout;        /**< @brief N_Cr timeout (See also: LIN 2.2a Specification - Chapter 3.2.5 - TIMING CONSTRAINTS) */
    LinTransIf_pGenericImpCfgData_t          ImpCfgData;        /**< @brief Pointer to implementation dependent configuration data for the TRANS Layer. (LinTransImp_sCfgData) */
};

/***************************************************************************//**
 * @brief TRANS Layer 'SID Description' struct.
 ******************************************************************************/
struct LinTransIf_sSIDDescription
{
    LinTransIf_SID_t                  SID;             /**< @brief Service ID (SID). */
    LinTransIf_SIDCbFun_t             CallbackFnc;     /**< @brief Pointer to SID callback function. */
    LinTransIf_eSIDDescLstCbCtxType_t CallbackCtxType; /**< @brief Type of the context data pointer for the SID callback. (See also: @ref LinTransIf_eSIDDescLstCbCtxType) */
    LinTransIf_SIDCbCtx_t             CallbackCtx;     /**< @brief Pointer to callback context data. (See also: @ref LinTransIf_SIDDescLstCbCtxType_PER_SID) */
};

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

#endif /* LINTRANS_INTERFACE_H_ */
