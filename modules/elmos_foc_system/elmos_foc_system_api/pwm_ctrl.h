/**
 * @ingroup HW
 *
 * @{
 */

#ifndef PWM_CTRL_
#define PWM_CTRL_

#include <defines.h>

#define PWM_PHASE_U ( 0u )
#define PWM_PHASE_V ( 1u )
#define PWM_PHASE_W ( 2u )

typedef enum
{
  PWM_SAWTOOTH     = 0x00,
  PWM_INV_SAWTOOTH = 0x01,
  PWM_TRIANGLE     = 0x02,
  PWM_INV_TRIANGLE = 0x03
} pwm_cnt_mode_t;

typedef enum
{
  PWM_DT_GEN_FIRST_EDGE = 0x00,
  PWM_DT_GEN_BOTH_EDGES = 0x01
} pwm_deadtime_mode_t;

typedef enum
{
  HS_OFF = 0x00,
  HS_ON  = 0x01
} pwm_hs_enable_t;

typedef enum
{
  LS_OFF = 0x00,
  LS_ON  = 0x01
} pwm_ls_enable_t;

/* --------------------
   global declarations
   -------------------- */
void pwm_init( pwm_cnt_mode_t cnt_mode, pwm_deadtime_mode_t deadtime_mode, u8 deadtime );
BOOL pwm_cnt_in_first_half( void );

void pwm_disable_all_outputs( void );
void pwm_enable_all_outputs( void );
void pwm_charge_bootstrap( void );
void pwm_set_all_outputs( const BOOL enable );
void pwm_set_outputs_reload( const BOOL enable_pwm0, const BOOL enable_pwm1, const BOOL enable_pwm2 );

void pwm_set_c0_values ( const u16 pwm0_c0, const u16 pwm1_c0, const u16 pwm2_c0 );
void pwm_set_c1_values ( const u16 pwm0_c1, const u16 pwm1_c1, const u16 pwm2_c1 );
void pwm_set_c0_reload_values ( const u16 pwm0_c0_reload, const u16 pwm1_c0_reload, const u16 pwm2_c0_reload );
void pwm_set_c1_reload_values ( const u16 pwm0_c1_reload, const u16 pwm1_c1_reload, const u16 pwm2_c1_reload );
void pwm_set_cnt_max_reload_value ( const u16 cnt_max_reload );
void pwm_set_cnt_value ( const u16 pwm_cnt );
u16 pwm_get_cnt_value ( void );

#endif /* _#ifndef PWM_CTRL_H_ */

/* }@
 */
