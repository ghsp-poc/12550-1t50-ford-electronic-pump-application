/***************************************************************************//**
 * @file		LinDataStg_Implementation.c
 *
 * @creator		sbai
 * @created		25.03.2015
 *
 * @brief  		Definitions of basic data types for the 'LIN DIAG Layer'.
 *
 * $Id$
 *
 * $Revision$
 *
 ******************************************************************************/

#ifndef LINDIAG_TYPES_H_
#define LINDIAG_TYPES_H_

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "LinTrans_Types.h"

/* ****************************************************************************/
/* *********** GENERAL DEFINES, ENUMS, STRUCTS, DATA TYPES, ETC. **************/
/* ****************************************************************************/
typedef Lin_pvoid_t      LinDiagIf_pGenericEnvData_t;    /**< TODO: Add description */
typedef Lin_EnvDataSze_t LinDiagIf_EnvDataSze_t;         /**< LIN Diagnose layer data type for the environment data length. */
typedef Lin_pvoid_t      LinDiagIf_pGenericImpCfgData_t; /**< Generic pointer to configuration parameter of the specific diag layer implementation */
typedef Lin_pvoid_t      LinDiagIf_pGenericCbCtxData_t;  /**< TODO: Add description */

typedef Lin_Error_t  LinDiagIf_Error_t;              /**< mapping for bus error codes to generic error type */

typedef Lin_uint8_t        LinDiagIf_Data_t;   /**< TODO: Add description */
typedef LinDiagIf_Data_t*  LinDiagIf_pData_t;  /**< TODO: Add description */
typedef LinDiagIf_Data_t** LinDiagIf_ppData_t; /**< TODO: Add description */

typedef Lin_BufLength_t      LinDiagIf_Length_t;   /**< TODO: Add description */
typedef LinDiagIf_Length_t*  LinDiagIf_pLength_t;  /**< TODO: Add description */
typedef LinDiagIf_Length_t** LinDiagIf_ppLength_t; /**< TODO: Add description */

struct         LinDiagIf_sCallbackFunctions;
typedef struct LinDiagIf_sCallbackFunctions     LinDiagIf_sCallbackFunctions_t;
typedef        LinDiagIf_sCallbackFunctions_t*  LinDiagIf_pCallbackFunctions_t;
typedef const  LinDiagIf_sCallbackFunctions_t*  LinDiagIf_cpCallbackFunctions_t;

struct         LinDiagIf_sInterfaceFunctions;                                      /**< TODO: Add description */
typedef struct LinDiagIf_sInterfaceFunctions     LinDiagIf_sInterfaceFunctions_t;  /**< TODO: Add description */
typedef        LinDiagIf_sInterfaceFunctions_t*  LinDiagIf_pInterfaceFunctions_t;  /**< TODO: Add description */
typedef const  LinDiagIf_sInterfaceFunctions_t*  LinDiagIf_cpInterfaceFunctions_t; /**< TODO: Add description */

struct         LinDiagIf_sThis;                      /**< TODO: Add description */
typedef struct LinDiagIf_sThis    LinDiagIf_sThis_t; /**< TODO: Add description */
typedef        LinDiagIf_sThis_t* LinDiagIf_pThis_t; /**< TODO: Add description */

struct         LinDiagIf_sInitParam;                           /**< TODO: Add description */
typedef struct LinDiagIf_sInitParam    LinDiagIf_sInitParam_t; /**< TODO: Add description */
typedef        LinDiagIf_sInitParam_t* LinDiagIf_pInitParam_t; /**< TODO: Add description */

typedef LinTransIf_NAD_t LinDiagIf_NAD_t;
typedef LinTransIf_SID_t LinDiagIf_SID_t;

typedef Lin_uint32_t LinDiagIf_Timeout_t;

/***************************************************************************//**
 *
 ******************************************************************************/
enum LinDiagIf_eNADType
{
  LinDiagIf_NADType_NORMAL  = 0,
  LinDiagIf_NADType_INITIAL = 1
};

typedef enum LinDiagIf_eNADType LinDiagIf_eNADType_t;

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param[in]  enclosing_method_argument TODO: Parameter description
 *
 * @return         TODO: return description
 *
 ******************************************************************************/
typedef Lin_Bool_t (*LinDiagIf_AppendDataToMsgBuffer_t) (LinDiagIf_pGenericEnvData_t genericDiagEnvData, LinDiagIf_pData_t data, LinDiagIf_Length_t dataLen);

/***************************************************************************//**
 * LIN diagnostic layer error enumerator
 *
 * @misra{M3CM Dir-10.1. - PRQA Msg 4521,
 * The Elmos LIN Driver defines areas of error codes for every module and sub-areas between
 * general defined interface error codes and implementation specific error codes. To link
 * between this areas some arithmetic offset calculation has to be done between different
 * enum types.,
 * Conflicts in  signedness.,
 * Always make sure unsigned values ate defined and used.}
 ******************************************************************************/
// PRQA S 4521 ++
typedef enum LinDiagIf_eErrorCodes
{
  LinDiagIf_ERR_NO_ERROR                                 = Lin_NO_ERROR,             /**< No error at all. */
  LinDiagIf_ERR_INIT                                     = Lin_ERROR_AREA_DIAG_UDS + 0u,  /**< Initialization error */
  LinDiagIf_ERR_INVALID_NAD                              = Lin_ERROR_AREA_DIAG_UDS + 1u,  /**< invalid NAD specified */
  LinDiagIf_ERR_IN_CORRECT_MESSAGE_LENGHT_INVALID_FORMAT = Lin_ERROR_AREA_DIAG_UDS + 2u,  /**< The data length mismatch for the service. */
  LinDiagIf_ERR_RBI_DEF                                  = Lin_ERROR_AREA_DIAG_UDS + 3u,  /**< Error at "Read by Identifier" definition. */
  LinDiagIf_ERR_RBI_NOT_FOUND                            = Lin_ERROR_AREA_DIAG_UDS + 4u,  /**< No entry for this identifier is found. */
  LinDiagIf_ERR_RBI_DATA_LEN                             = Lin_ERROR_AREA_DIAG_UDS + 5u,
  LinDiagIf_ERR_TRANS_LAYER                              = Lin_ERROR_AREA_DIAG_UDS + 10u, /**< Error using Transport Layer. */
  LinDiagIf_ERR_IMPL_ERROR_AREA                          = Lin_ERROR_AREA_DIAG_UDS + (LIN_ERROR_AREA_SIZE/2u) /**< Any additional implementation specific error codes start here. */
} LinDiagIf_eErrorCodes_t;
// PRQA S 4521 --

#define LINDIAGIF_RSID_OFFSET         0x40u

#define LINDIAGIF_SUPPLIER_ID_WILDCARD  0x7FFFu
#define LINDIAGIF_FUNCTION_ID_WILDCARD  0xFFFFu

enum LinDiagIf_ServiceID
{
  /* LIN 2.2 specification services. */
  LinDiagIf_SID_AssignNAD            = 0xB0u,
  LinDiagIf_SID_AssignFrameID        = 0xB1u,
  LinDiagIf_SID_ReadByIdentifier     = 0xB2u,
  LinDiagIf_SID_ConditionalChangeNAD = 0xB3u,
  LinDiagIf_SID_SNPD                 = 0xB5u,
  LinDiagIf_SID_SaveConfiguration    = 0xB6u,
  LinDiagIf_SID_AssignFrameIDRange   = 0xB7u,
};

typedef enum LinDiagIf_ServiceID LinDiagIf_ServiceID_t;

/***************************************************************************//**
 * Negative Response Codes
 ******************************************************************************/
#define LINDIAGIF_NEG_RESP_RSID                                         0x7Fu
#define LINDIAGIF_NEG_RESP_LEN                                          2u

typedef Lin_uint16_t     LinDiagIf_NRC_t;
typedef LinDiagIf_NRC_t* LinDiagIf_pNRC_t;

enum LinDiagIf_eNegativeResponseCode
{
  LinDiagIf_NRC_General_Reject                                           = 0x10u,
  LinDiagIf_NRC_Service_Not_Supported                                    = 0x11u,
  LinDiagIf_NRC_Sub_Function_Not_Supported                               = 0x12u,
};

typedef enum LinDiagIf_eNegativeResponseCode LinDiagIf_eNegativeResponseCode_t;

/* ****************************************************************************/
/* ****************************** LIN SERVICES ********************************/
/* ****************************************************************************/

#define LINDIAGIF_ASSIGNNAD_REQLEN             5u
#define LINDIAGIF_ASSIGNFRAMEID_REQLEN         5u
#define LINDIAGIF_READBYIDENTIFIER_REQLEN      5u
#define LINDIAGIF_CONDITIONALCHANGENAD_REQLEN  5u
#define LINDIAGIF_ASSIGNFRAMEIDRANGE_REQLEN    5u

/* LIN 2.2 Read by identifier - identifier */

#define LINDIAGIF_RB_IDENTIFIER_PROD_IDENT      0x00u /*<< Read By Identifier for 'Product Identification'. */
#define LINDIAGIF_RB_IDENTIFIER_PROD_IDENT_LEN  5u

#define LINDIAGIF_RB_IDENTIFIER_SERIAL_NUM      0x01u /*<< Read By Identifier for 'Serial number'. */
#define LINDIAGIF_RB_IDENTIFIER_SERIAL_NUM_LEN  4u

typedef Lin_uint16_t LinDiagIf_SupplierID_t;       /**< TODO: Add description */
typedef Lin_uint16_t LinDiagIf_FunctionID_t;       /**< TODO: Add description */
typedef Lin_uint8_t  LinDiagIf_VariantID_t;        /**< TODO: Add description */
typedef Lin_uint32_t LinDiagIf_SerialNumber_t;     /**< TODO: Add description */

typedef Lin_uint8_t LinDiagIf_RbIdentifier_t; /**< TODO: Add description */
typedef Lin_pvoid_t LinDiagIf_RbiLenght_t;    /**< TODO: Add description */

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param[in]  enclosing_method_argument TODO: Parameter description
 *
 * @return         TODO: return description
 *
 ******************************************************************************/
typedef LinDiagIf_Length_t (*LinDiagIf_RbILenCbFun_t) (LinDiagIf_pGenericEnvData_t genericDiagEnvData, LinDiagIf_cpInterfaceFunctions_t ifFunctions,
                                                       LinDiagIf_RbIdentifier_t    rbIdentifier);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param[in]  enclosing_method_argument TODO: Parameter description
 *
 * @return         TODO: return description
 *
 ******************************************************************************/
typedef Lin_Bool_t (*LinDiagIf_RbIdentifierCbFun_t) (LinDiagIf_pGenericEnvData_t   genericDiagEnvData, LinDiagIf_cpInterfaceFunctions_t  ifFunctions,
                                                     LinDiagIf_RbIdentifier_t      rbIdentifier,       LinDiagIf_AppendDataToMsgBuffer_t appendDataToMsgBufFun,
                                                     LinDiagIf_pGenericCbCtxData_t genericDiagCbCtxData);

/***************************************************************************//**
 *
 ******************************************************************************/
typedef enum LinDiagIf_eRbiLenType
{
  LinDiagIf_RbiLenType_Value    = 0,  /**< TODO: Add description */
  LinDiagIf_RbiLenType_Callback = 1   /**< TODO: Add description */
}LinDiagIf_eRbiLenType_t;

/***************************************************************************//**
 *
 ******************************************************************************/
struct LinDiagIf_sRbiLookupEntry
{
  LinDiagIf_RbIdentifier_t      FirstRbIdentifier;
  LinDiagIf_RbIdentifier_t      LastRbIdentifier;
  LinDiagIf_RbIdentifierCbFun_t Callback;
  LinDiagIf_eRbiLenType_t       LengthType;
  LinDiagIf_RbiLenght_t         Length;
  LinDiagIf_pGenericCbCtxData_t CbCtxData;
};

typedef struct LinDiagIf_sRbiLookupEntry LinDiagIf_sRbiLookupEntry_t;
typedef LinDiagIf_sRbiLookupEntry_t*     LinDiagIf_pRbiLookupEntry_t;

/***************************************************************************//**
 *
 ******************************************************************************/
struct LinDiagIf_sProductIdentification
{
  LinDiagIf_SupplierID_t SupplierID;
  LinDiagIf_FunctionID_t FunctionID;
  LinDiagIf_VariantID_t  VariantID;
};

typedef struct LinDiagIf_sProductIdentification    LinDiagIf_sProductIdentification_t;
typedef        LinDiagIf_sProductIdentification_t* LinDiagIf_pProductIdentification_t;

/***************************************************************************//**
 *
 ******************************************************************************/
struct LinDiagIf_sConditionalChangeNADData
{
  LinDiagIf_RbIdentifier_t Id;     /**< TODO: Add description */
  LinDiagIf_Data_t         Byte;   /**< TODO: Add description */
  LinDiagIf_Data_t         Mask;   /**< TODO: Add description */
  LinDiagIf_Data_t         Invert; /**< TODO: Add description */
  LinTransIf_NAD_t         NewNAD; /**< TODO: Add description */
};

typedef struct LinDiagIf_sConditionalChangeNADData    LinDiagIf_sConditionalChangeNADData_t;  /**< TODO: Add description */
typedef        LinDiagIf_sConditionalChangeNADData_t* LinDiagIf_pConditionalChangeNADData_t;  /**< TODO: Add description */

#endif /* LINDIAG_TYPES_H_ */
