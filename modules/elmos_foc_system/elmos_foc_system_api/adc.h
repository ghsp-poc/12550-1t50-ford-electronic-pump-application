/**
 * @ingroup HW
 *
 * @{
 */

#ifndef ADC_H
#define ADC_H

#include "global.h"

/* ---------------
   global defines
   --------------- */

/* all ADC input sinals encoded into channel numbers */
#define ADC_CHNO_A0 ( 0u )
#define ADC_CHNO_A1 ( 1u )
#define ADC_CHNO_A2 ( 2u )
#define ADC_CHNO_A3 ( 3u )
#define ADC_CHNO_A4 ( 4u )
#define ADC_CHNO_A5 ( 5u )
#define ADC_CHNO_A6 ( 6u )
#define ADC_CHNO_A7 ( 7u )

#define ADC_CHNO_B0 ( 8u )
#define ADC_CHNO_B1 ( 9u )
#define ADC_CHNO_B2 ( 10u )
#define ADC_CHNO_B3 ( 11u )
#define ADC_CHNO_B4 ( 12u )
#define ADC_CHNO_B5 ( 13u )
#define ADC_CHNO_B6 ( 14u )
#define ADC_CHNO_B7 ( 15u )

#define ADC_CHNO_C0 ( 16u )
#define ADC_CHNO_C1 ( 17u )
#define ADC_CHNO_C2 ( 18u )
#define ADC_CHNO_C3 ( 19u )
#define ADC_CHNO_C4 ( 20u )
#define ADC_CHNO_C5 ( 21u )
#define ADC_CHNO_C6 ( 22u )
#define ADC_CHNO_C7 ( 23u )

#define ADC_CHNO_TEMP ( 24u )

#define ADC_USER_CHANNEL_CNT ( 4 )
#define ADC_USR_CHAN_INIT    ( ADC_CHNO_C7 )

#define CNT_UP_MSK   ( 0x0000u )
#define CNT_DOWN_MSK ( 0x8000u )

/**
   @struct adc_data_t
   @brief top-level storage structure of module "ADC"
 */
typedef struct
{
  uint16_t phase_curr1;                                        /**< voltage over phase shunt 1 (when dual shunt operation) or voltage over sum shunt (when single shunt operation) [ADC bits] */
  uint16_t phase_curr1_offset;                                 /**< OPA offset voltage of phase shunt 1 (when dual shunt operation) or OPA offset voltage of sum shunt (when single shunt operation) [ADC bits] */

  uint16_t phase_curr2;                                        /**< voltage over phase shunt 2 [ADC bits] */
  uint16_t phase_curr2_offset;                                 /**< OPA offset voltage of phase shunt 2 */

  uint16_t usr_result[ ADC_USER_CHANNEL_CNT ];                 /**< user measurements [ADC bits] */

  uint16_t dummy;                                              /**< ... */

  uint16_t vsup;                                               /**< supply voltage [ADC bits] */

  uint16_t phase_voltage_u;                                    /**< voltage of phase U (only valid during sync to turning rotor) [ADC bits] */
  uint16_t phase_voltage_v;                                    /**< voltage of phase V (only valid during sync to turning rotor) [ADC bits] */
  uint16_t phase_voltage_w;                                    /**< voltage of phase W (only valid during sync to turning rotor) [ADC bits] */

  uint16_t pos_detect_uv[ 3 ];
  uint16_t pos_detect_vu[ 3 ];
  uint16_t pos_detect_uw[ 3 ];
  uint16_t pos_detect_wu[ 3 ];
  uint16_t pos_detect_vw[ 3 ];
  uint16_t pos_detect_wv[ 3 ];

  bool_t enable_pb7_dbg_signal;                             /**< when this variable is True, the MotCU-internal ADC_DBG_SAMPLING signal is output at PB7 (which also means that there's no UART communication from the IC to the GUI) */

  uint16_t struct_signature;

} adc_data_t;

#define ADC_REF_VOLTAGE_MV ( 2500 )
#define ADC_RESOLUTION     ( 12u )
#define ADC_MAX_VAL        (( 1u << ADC_RESOLUTION ) - 1u )

enum
{
  ADC_CTRL_STATE_idle                      = 0,
  ADC_CTRL_STATE_waiting_for_trigger       = 1,
  ADC_CTRL_STATE_sampling                  = 2,
  ADC_CTRL_STATE_executing_other_command   = 3,
  ADC_CTRL_STATE_waiting_for_next_list_item= 4,
  ADC_CTRL_STATE_finish_sampling           = 5
};

/* ADC trigger events caused by dead_time_evt */

/** Triggers after DEAD_TIME_WAIT0 cycles if phase U was Rising.*/
#define ADC_TRIG_U_RIS_DT0 ( 0x0402u )

/** Triggers after DEAD_TIME_WAIT0 cycles if phase V was Rising.*/
#define ADC_TRIG_V_RIS_DT0 ( 0x0408u )

/** Triggers after DEAD_TIME_WAIT0 cycles if phase W was Rising.*/
#define ADC_TRIG_W_RIS_DT0 ( 0x0420u )

/** Triggers after DEAD_TIME_WAIT0 cycles if phase U was Falling.*/
#define ADC_TRIG_U_FALL_DT0 ( 0x0401u )

/** Triggers after DEAD_TIME_WAIT0 cycles if phase V was Falling.*/
#define ADC_TRIG_V_FALL_DT0 ( 0x0404u )

/** Triggers after DEAD_TIME_WAIT0 cycles if phase W was Falling.*/
#define ADC_TRIG_W_FALL_DT0 ( 0x0410u )

/** Triggers after DEAD_TIME_WAIT1 cycles if phase U was Rising.*/
#define ADC_TRIG_U_RIS_DT1 ( 0x1402u )

/** Triggers after DEAD_TIME_WAIT1 cycles if phase V was Rising.*/
#define ADC_TRIG_V_RIS_DT1 ( 0x1408u )

/** Triggers after DEAD_TIME_WAIT1 cycles if phase W was Rising.*/
#define ADC_TRIG_W_RIS_DT1 ( 0x1420u )

/** Triggers after DEAD_TIME_WAIT1 cycles if phase U was Falling.*/
#define ADC_TRIG_U_FALL_DT1 ( 0x1401u )

/** Triggers after DEAD_TIME_WAIT1 cycles if phase V was Falling.*/
#define ADC_TRIG_V_FALL_DT1 ( 0x1404u )

/** Triggers after DEAD_TIME_WAIT1 cycles if phase W was Falling.*/
#define ADC_TRIG_W_FALL_DT1 ( 0x1410u )

/** Triggers after DEAD_TIME_WAIT2 cycles if phase U was Rising.*/
#define ADC_TRIG_U_RIS_DT2 ( 0x2402u )

/** Triggers after DEAD_TIME_WAIT2 cycles if phase V was Rising.*/
#define ADC_TRIG_V_RIS_DT2 ( 0x2408u )

/** Triggers after DEAD_TIME_WAIT2 cycles if phase W was Rising.*/
#define ADC_TRIG_W_RIS_DT2 ( 0x2420u )

/** Triggers after DEAD_TIME_WAIT2 cycles if phase U was Falling.*/
#define ADC_TRIG_U_FALL_DT2 ( 0x2401u )

/** Triggers after DEAD_TIME_WAIT2 cycles if phase V was Falling.*/
#define ADC_TRIG_V_FALL_DT2 ( 0x2404u )

/** Triggers after DEAD_TIME_WAIT2 cycles if phase W was Falling.*/
#define ADC_TRIG_W_FALL_DT2 ( 0x2410u )

typedef struct {                                          /* PRQA S 3630 */ /* justification: typedef necessary for exporting function adc_measurement_list_set() */
  volatile uint16_t target_adr;                           /* [15:0]  */
  volatile uint16_t no_sample                     :    4; /* [19:16] */
  volatile uint16_t no_sum                        :    5; /* [24:20] */
  volatile uint16_t ch_no                         :    5; /* [29:25] */
  volatile uint16_t trigger_type_ext              :    1; /* [30]    */
  volatile uint16_t trigger_type                  :    1; /* [31]    */
  volatile uint16_t trigger;                              /* [47:32] */
} saradc_ctrl_list_entry_t;

/* -------------------
   global declarations
   ------------------- */

extern adc_data_t adc_data;

void adc_init( void );
void adc_generate_struct_signature ( void );
const adc_data_t * adc_get_data( void );
void adc_clear_data( void );

void adc_list_ready_irq_enable( void );
void adc_list_ready_irq_disable( void );

void adc_measurement_list_set( saradc_ctrl_list_entry_t const * list_new, bool_t skip_list );
void adc_measurement_list_set_and_skip ( saradc_ctrl_list_entry_t const * list_new );
void adc_deadtime_config( uint8_t deadtime_wait_0, uint8_t deadtime_wait_1, uint8_t deadtime_wait_2 );

uint16_t adc_get_user_data( uint8_t idx );
uint16_t adc_get_supply_voltage(void);

void adc_main( void );

#endif /* _#ifndef ADC_H_ */

/* }@
 */
