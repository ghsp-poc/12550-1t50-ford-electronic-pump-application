/***************************************************************************//**
 * @file			LinSNPD_Types.h
 *
 * @creator		rpy
 * @created		9.9.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id$
 *
 * $Revision$
 *
 ******************************************************************************/

#ifndef LINSNPD_TYPES_H_
#define LINSNPD_TYPES_H_

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "Lin_Basictypes.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

//typedef Lin_uint16_t LinSNPDIf_Thres_t;

typedef Lin_pvoid_t      LinSNPDIf_pGenericEnvData_t;           /**< SNPD environment data type **/
typedef Lin_EnvDataSze_t LinSNPDIf_EnvDataSze_t;                /**< LIN SNPD layer data type for the environment data length. */
typedef Lin_pvoid_t      LinSNPDIf_pGenericImpCfgData_t;        /**< Generic pointer to configuration parameter of the specific SNPD implementation */

typedef Lin_uint16_t LinSNPDIf_MeasCount_t; /**< TODO: Add description */

struct         LinSNPDIf_sInterfaceFunctions;
typedef struct LinSNPDIf_sInterfaceFunctions     LinSNPDIf_sInterfaceFunctions_t;  /**< TODO: Add description **/
typedef        LinSNPDIf_sInterfaceFunctions_t*  LinSNPDIf_pInterfaceFunctions_t;  /**< TODO: Add description **/
typedef const  LinSNPDIf_sInterfaceFunctions_t*  LinSNPDIf_cpInterfaceFunctions_t; /**< TODO: Add description **/

struct         LinSNPDIf_sThis;                        /**< TODO: Add description */
typedef struct LinSNPDIf_sThis    LinSNPDIf_sThis_t;   /**< TODO: Add description */
typedef        LinSNPDIf_sThis_t* LinSNPDIf_pThis_t;   /**< TODO: Add description */

struct         LinSNPDIf_sInitParam;                             /**< TODO: Add description */
typedef struct LinSNPDIf_sInitParam    LinSNPDIf_sInitParam_t;   /**< TODO: Add description */
typedef        LinSNPDIf_sInitParam_t* LinSNPDIf_pInitParam_t;   /**< TODO: Add description */



/***************************************************************************//**
 * SNPD stated
 ******************************************************************************/
enum LinSNPDIf_eState
{
  LinSNPDIf_DISABLED          = 0,  /**<  SNPD state machine is disable call LinBusImp_AutoAddrInitialize() to enable. */
  LinSNPDIf_WAITING           = 1,  /**<  no auto addressing sequence has been performed since last LinBusImp_AutoAddrNext() call */
  LinSNPDIf_DONE_NOT_LAST     = 2,  /**<  AA sequence successfully performed, but this node was not the last */
  LinSNPDIf_DONE_LAST         = 3,  /**<  AA sequence successful, this node is the last one. Store provided NAD*/
  LinSNPDIf_SKIPED            = 4,  /**<  AA sequence successful, this node has already been adressed, so just skip any action. */
  LinSNPDIf_FAILED            = 5,  /**<  i.e. ADC sampling did not finish on time (i.e. oversampling to high, sample extension too long )*/   
};

typedef enum LinSNPDIf_eState LinSNPDIf_eState_t;

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

#endif /* LINSNPD_TYPES_H_ */
