#ifndef FDL_H
#    define FDL_H

/**
 * @file fdl.h
 *
 * API for the Flash Driver Library for the Elmos platform using an external EEPROM.
 *
 *------------------------------------------------------------------------------
 *
 * Copyright 2014 GHSP, Inc., All Rights Reserved.
 * GHSP Confidential
 *
 * NOTICE:
 * All information contained herein is, and remains the property of GHSP, Inc.
 * The intellectual and technical concepts contained herein are proprietary to
 * GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination
 * of this information or reproduction of this material is strictly forbidden
 * unless prior written permission is obtained from GHSP, Inc.
 *
 *------------------------------------------------------------------------------
 *
 * @section DESC DESCRIPTION:
 *
 * API for the Flash Driver Library for the S12Z platform.
 * @section ABBR ABBREVIATIONS:
 *   - None
 *
 * @section TRACE TRACEABILITY INFO:
 *   - Design Document(s):
 *     - None
 *
 *   - Requirements Document(s):
 *     - None
 *
 *   - Applicable Standards (in order of precedence: highest first):
 *     - <a href="http://sharepoint/sandbox/ghspdev/LaunchPad/EngineeringDepartment/Elec_proj_mgmt/Revision%20Controlled%20Library/Software%20Process/Work%20Instructions/Coding%20Standard.docx">
 *       "GHSP Coding Standard" [12-Mar-2006]</a>
 *
 * @section DEV DEVIATIONS FROM STANDARDS:
 *   - None
 *
 * @defgroup fdl_api Flash Driver Library Interface Documentation
 * @{
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "global.h"
#include "fdl_cfg.h"
/*===========================================================================*
 * Exported Preprocessor #define Constants
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor #define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/

typedef enum
{
    FDL_STATUS_SUCCESS = 0,
    FDL_STATUS_BUSY,
    FDL_STATUS_FAIL,
    FDL_STATUS_MAX
} FDL_Status_T;

/*===========================================================================*
 * Exported Const Object Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Function Prototypes
 *===========================================================================*/

/**
 * Initialize the flash driver library.
 *
 * @param none
 *
 * @return
 *  true : FDL configured properly
 *  false : FDL config failed
 */
bool_t FDL_Init(void);

/**
 * Indicate whether the FDL has been initialized.
 * @details
 *  Tracks the initialization state of the FDL.
 *
 * @param None
 *
 * @return
 *  true : FDL_Init() called, FDL initialized.
 *  false : FDL_Init() has not been called, FDL not initialized
 */
bool_t FDL_Is_Initialized(void);

/**
 * Indicates whether or not flash library is busy processing a request.
 *
 * @details
 *  If the flash driver state machine is IDLE and if the memory controller
 *  hardware is not busy, this function returns false. If either the
 *  state machine is not idle or the memory controller is running an
 *  operation this function returns true.
 *
 * @param none
 *
 * @return
 *  true : FDL is busy, don't start a new operation
 *  false : FDL is not busy, ok to start a new operation
 */
bool_t FDL_Is_Busy(void);

/**
 * Poll the flash driver until a running operation completes.
 *
 * @details
 *  NVM operations take many cycles and are therefore broken up into a number
 *  of discrete operations. The sequence is kicked off by calling a function
 *  like FDL_Write, _Read, _Erase, or _Erase_Verify. If those functions return
 *  BUSY (indicating the operation has started), the caller has to periodically
 *  "pump" the state machine by calling FDL_Poll(). The poll will return BUSY
 *  until the sequence succeeds or fails, returning SUCCESS or FAIL.
 *
 * @param none
 *
 * @return
 *  FDL_STATUS_BUSY : the library is still busy, keep calling FDL_Poll()
 *  FDL_STATUS_SUCCESS : the library completed the operation successfully
 *  FDL_STATUS_FAIL : the library completed the operation but failed (not read / written)
 */
FDL_Status_T FDL_Poll(void);

/**
 * Start a write operation.
 *
 * @details
 *  Writing to NVM is a multi-step process that can take some time. This function kicks off the
 *  write sequence. If it returns BUSY, the FDL_Poll() function must be called until it returns
 *  something other than BUSY. You have to have a persistent buffer that contains the data to
 *  write (the buffer can't be on the stack).
 *
 * @param [in] p_src_buffer
 *  - Pointer to buffer containing data to write. Must point to a buffer in persistent memory, the
 *    buffer cannot have been created on the stack.
 *
 * @param [in] dest_addr
 *  - Address to write to in NVM. Must be even address.
 *
 * @param [in] num_bytes
 *  - Number of bytes to write. Must be an even number of bytes.
 *
 * @return
 *  FDL_STATUS_BUSY : the library started the write, keep pumping
 *  FDL_STATUS_SUCCESS : the library completed the write successfully
 *  FDL_STATUS_FAIL : the library completed the operation but failed (not read / written)
 */
FDL_Status_T FDL_Write(uint8_t const *p_src_buffer, uint32_t dest_addr, uint8_t num_bytes);

/**
 * Start an erase operation.
 *
 * @details
 *  Erasing locations in the NVM is a multi-step, long process. Calling this
 *  function kicks off the erase sequence. If this function returns BUSY,
 *  the caller must periodically call FDL_Poll() until it returns something
 *  other than FDL_STATUS_BUSY.
 *
 * @param [in] address
 *  - address to erase. Range must remain in valid NVM memory space.
 *
 * @param [in] num_bytes
 *  - number of bytes to erase.
 *
 * @note
 *
 * @return
 *  FDL_STATUS_BUSY : the library started the erase, keep calling FDL_Poll() until complete
 *  FDL_STATUS_SUCCESS : the library completed the erase operation successfully
 *  FDL_STATUS_FAIL : the library completed the operation but failed (not erased)
 */
FDL_Status_T FDL_Erase(uint32_t address, uint8_t num_bytes);

/**
 * Start a read operation.
 *
 * @details
 *   The memory controller automatically inserts wait states when reading from
 *   NVM addresses. In order to prevent these wait states from stacking up and
 *   affecting interrupt behavior, prevent more than FDL_CFG_MAX_READ_BYTES_ALLOWED
 *   from being read at one time. Instead reads will be broken up into segments.
 *   After kicking the sequence off by calling FDL_Read(), caller needs to
 *   call FDL_Poll() periodically until it returns something other than BUSY.
 *
 * @param [in] p_dest_buffer
 *  - pointer to PERSISTENT BUFFER that will be loaded with data from NVM.
 *    This buffer can't be created on the stack, must be dedicated.
 * @param [in] src_addr
 *  - NVM address to start reading from. Must be even address.
 * @param [in] num_bytes
 *  - The number of bytes to read from NVM. Must be even number of bytes.
 *
 * @note The p_dest_buffer parm MUST BE PERSISTENT MEMORY, CAN'T BE CREATED ON THE STACK.
 *
 * @return
 *  FDL_STATUS_BUSY : the library started the read, keep pumping until complete
 *  FDL_STATUS_SUCCESS : the library completed the read operation successfully
 *  FDL_STATUS_FAIL : the library completed the operation but failed (not read)
 */
FDL_Status_T FDL_Read(uint8_t * const p_dest_buffer, uint32_t src_addr, uint8_t num_bytes);


/*===========================================================================*
 * Exported Inline Function Definitions and #define Function-Like Macros
 *===========================================================================*/

/** @} doxygen end group */
#endif /* FDL_H */
