#ifndef _TEST_
#    error This file is for unit testing only!
#endif

#ifndef ELMOS_52307_TEST_REGS_H
#define ELMOS_52307_TEST_REGS_H

#include "global.h"

#define TEST_REG_END (0xEFFu)

/**
    Define these variable in your unit tests and then you can use any of the regs
    that the regular register file defines. Test_Regs and Test_NV_Regs are arrays
    that through the defines in this file map those registers to their memory
    instead of the CPU registers of the actual micro.

    The following is an example of how to use these in a unit test.
    @code

    // Register _TEST_ mem mapped variable
    byte Test_Regs[TEST_REG_END];

    void setUp(void)
    {
        // ensure register mem map is cleared for each test.
        memset(&Test_Regs[0u], (int8_t)0, (size_t)TEST_REG_END);
    }

    void test_can_driver_enable_should_set_physical_can_enable_reg_bit(void)
    {
        // Ensure known test state

        // Setup expected call chain

        // Call function under test
        can_driver_init();
        // Verify test results
        TEST_ASSERT_EQUAL(1u, CPCR_CPE);
    }

    @endcode
*/
extern uint16_t Test_Regs[TEST_REG_END];

#define REG_BASE 0x0000                /* Base address for the I/O register block */

#define OSC_CTRL_OSC_CONFIG   (Test_Regs[0x4C8u])
#define PWMN_CNT              (Test_Regs[0x606u])
#define PWMN_CNT_MAX          (Test_Regs[0x60Cu])
#define PWMN_DEAD_TIME        (Test_Regs[0x60Eu])
#define PWMN_DEAD_TIME_RELOAD (Test_Regs[0x614u])

#endif /* ELMOS_52307_TEST_REGS_H */
