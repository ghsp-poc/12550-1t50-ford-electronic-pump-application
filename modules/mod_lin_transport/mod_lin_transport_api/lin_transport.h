#ifndef LIN_TRANSPORT_H
#    define LIN_TRANSPORT_H

/**
 * @file lin_transport.h
 *
 * API for the LIN Transport Layer 2.2A
 *
 *------------------------------------------------------------------------------
 *
 * Copyright 2014 GHSP, Inc., All Rights Reserved.
 * GHSP Confidential
 *
 * NOTICE:
 * All information contained herein is, and remains the property of GHSP, Inc.
 * The intellectual and technical concepts contained herein are proprietary to
 * GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination
 * of this information or reproduction of this material is strictly forbidden
 * unless prior written permission is obtained from GHSP, Inc.
 *
 *------------------------------------------------------------------------------
 *
 * @section DESC DESCRIPTION:
 *
 * Template file for the API of a module. Add your detailed description here.
 * @section ABBR ABBREVIATIONS:
 *   - None
 *
 * @section TRACE TRACEABILITY INFO:
 *   - Design Document(s):
 *     - None
 *
 *   - Requirements Document(s):
 *     - None
 *
 *   - Applicable Standards (in order of precedence: highest first):
 *     - <a href="http://sharepoint/sandbox/ghspdev/LaunchPad/EngineeringDepartment/Elec_proj_mgmt/Revision%20Controlled%20Library/Software%20Process/Work%20Instructions/Coding%20Standard.docx">
 *       "GHSP Coding Standard" [12-Mar-2006]</a>
 *
 * @section DEV DEVIATIONS FROM STANDARDS:
 *   - None
 *
 * @defgroup homebrew_lin_trans_api LIN Transport Layer 2.2A Interface Documentation
 * @{
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "serial_data_buffer.h"

/*===========================================================================*
 * Exported Preprocessor #define Constants
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor #define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Const Object Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Function Prototypes
 *===========================================================================*/
void LIN_Diagnostic_Request(Serial_Data_Buffer_T * Rx_Data_Buffer, Serial_Data_Buffer_T * Tx_Data_Buffer);
void LIN_Diagnostic_Response(Serial_Data_Buffer_T * Rx_Data_Buffer, Serial_Data_Buffer_T * Tx_Data_Buffer);

/*===========================================================================*
 * Exported Inline Function Definitions and #define Function-Like Macros
 *===========================================================================*/

/** @} doxygen end group */
#endif /* LIN_TRANSPORT_H */
